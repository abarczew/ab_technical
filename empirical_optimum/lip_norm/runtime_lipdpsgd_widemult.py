'''
Taining of a WideResNet on CIFAR-10 with Lip-DP-SGD, wide resnet and augmentation
multiplicity.
'''


import argparse
from math import ceil
import time
import torch
import torchvision
import torchvision.transforms as transforms
from src.models.wideresnet import WideResNet
import torch.nn as nn
import torch.optim as optim
# from src.opacus_augmented.privacy_engine_augmented import PrivacyEngineAugmented
from opacus.validators import ModuleValidator
from tqdm import tqdm
from torchvision.datasets import CIFAR10
import numpy as np
from opacus.utils.batch_memory_manager import BatchMemoryManager
from torch.utils.data import Subset
from opacus import GradSampleModule
# from EMA import EMA
from src.models.EMA_without_class import create_ema, update
import pickle
# from src.models.augmented_grad_samplers import AugmentationMultiplicity
from math import ceil
from src.utils.utils import (init_distributed_mode, initialize_exp, bool_flag,
                             accuracy, get_noise_from_bs, get_epochs_from_bs, print_params,)
import json
from src.models.prepare_models import prepare_data_cifar, prepare_augmult_cifar
from torch.nn.parallel import DistributedDataParallel as DDP
from opacus.distributed import DifferentiallyPrivateDistributedDataParallel as DPDDP

from lip_dp.normalizer import get_spectral_norm
from model import Conv2dNorm, WideResNetNorm
from lip_dp.optimizers import LipDPParameters, prepare_optimizer,\
    prepare_accountant, prepare_model
from opacus.data_loader import DPDataLoader
from opacus.accountants import RDPAccountant

import warnings

warnings.simplefilter("ignore")

activation_dict = {'tanh': nn.Tanh, 'relu': nn.ReLU}


def train(
    model,
    ema,
    train_loader,
    optimizer,
    epoch,
    max_nb_steps,
    step_freq,
    device,
    accountant,
    K,
    logger,
    losses,
    train_acc,
    epsilons,
    grad_sample_gradients_norms_per_epoch,
    test_loader,
    is_main_worker,
    args,
    nb_steps,
):
    """
    Trains the model for one epoch. If it is the last epoch, it will stop at max_nb_steps iterations.
    If the model is being shadowed for EMA, we update the model at every step.
    """
    # nb_steps = nb_steps
    model.train()

    max_input_norm = {}
    def save_input_norm(name):
        def hook(model, input, output):
            x = input[0]
            batch_max_input_norm = float(torch.linalg.vector_norm(x, dim=[i for i in range(len(x.size())) if i > 0]).max().detach().data)
            if name not in max_input_norm.keys():
                max_input_norm[name] = batch_max_input_norm
            elif batch_max_input_norm > max_input_norm[name]:
                max_input_norm[name] = batch_max_input_norm
        return hook

    def register_hook(model):
        for i, named_module in enumerate(model.named_modules()):
            name, m = named_module
            if 'fc' in name or 'conv' in name:
                m.register_forward_hook(save_input_norm(f'layer.{i}'))

    register_hook(model)

    criterion = nn.CrossEntropyLoss()
    steps_per_epoch = len(train_loader) // step_freq
    if is_main_worker:
        print(f"steps_per_epoch:{steps_per_epoch}")
    losses_epoch, train_acc_epoch, grad_sample_norms, grad_batch_norms = [], [], [], []
    nb_examples_epoch = 0
    optimizer.zero_grad() 
    for i, (images, target) in enumerate(train_loader):
        nb_examples_epoch += len(images)
        images = images.to(device)
        target = target.to(device)
        assert K == args.transform
        l = len(images)
        # Using Augmentation multiplicity
        if K:
            images_duplicates = torch.repeat_interleave(images, repeats=K, dim=0)
            target = torch.repeat_interleave(target, repeats=K, dim=0)
            transform = transforms.Compose([transforms.RandomCrop(
                size=(32, 32), padding=4, padding_mode="reflect"), transforms.RandomHorizontalFlip(p=0.5), ])
            images = transforms.Lambda(lambda x: torch.stack(
                [transform(x_) for x_ in x]))(images_duplicates)
            assert len(images) == args.transform * l

        # compute output
        output = model(images)
        loss = criterion(output, target) * args.loss_lip / step_freq # reduction == 'mean'
        preds = np.argmax(output.detach().cpu().numpy(), axis=1)
        labels = target.detach().cpu().numpy()

        # measure accuracy and record loss
        acc = accuracy(preds, labels)
        losses_epoch.append(loss.item())
        train_acc_epoch.append(acc)

        loss.backward()

        if (i+1) % (step_freq) == 0:
            # Logging gradient statistics on the main worker
            if is_main_worker and args.debug:
                with torch.no_grad():
                    param_norms_prestep = [get_spectral_norm(
                        weight) for weight in model.parameters() if weight.requires_grad]
                    # per_param_norms = [g.grad_sample.view(
                    #     len(g.grad_sample), -1).norm(2, dim=-1) for g in model.parameters() if g.grad_sample is not None]
                    # per_sample_norms = (torch.stack(
                    #     per_param_norms, dim=1).norm(2, dim=1).cpu().tolist())
                    grad_batch_norm = torch.Tensor(
                        [float(g.grad.norm(2)) for g in model.parameters() if g.grad is not None]).norm(2)
                    grad_batch_norms.append(float(grad_batch_norm))
                    # # in case of poisson sampling we dont want the 0s
                    # grad_sample_norms += per_sample_norms[:l]

            nb_steps += 1

            max_input_norm_list = [v for _, v in max_input_norm.items()]
            layer_nb = len(max_input_norm_list)
            # if model has bias
            for i in range(layer_nb):
                max_input_norm_list.insert(2*i+1, 1.)

            optimizer.grad_scales = torch.Tensor(max_input_norm_list[::-1])
            optimizer.step()
            optimizer.zero_grad()
            if ema:
                update(model, ema, nb_steps)

            # reset max_input norm recording
            max_input_norm = {}

            if is_main_worker:
                if args.debug:
                    with torch.no_grad():
                        param_norms = [get_spectral_norm(
                            weight) for weight in model.parameters() if weight.requires_grad]
                losses.append(np.mean(losses_epoch))
                train_acc.append(np.mean(train_acc_epoch))
                # grad_sample_gradients_norms_per_epoch.append(np.mean(grad_sample_norms))
                losses_epoch, train_acc_epoch = [], []
                if nb_steps % args.freq_log == 0:
                    print(f"epoch:{epoch},step:{nb_steps}")
                   
                    # norms2_before_sigma = []
                    # grad_sample_norms = []
                if nb_steps % args.freq_log_val == 0:
                    test_acc_ema, train_acc_ema = (
                        test(ema, test_loader, train_loader, device)
                        if ema
                        else test(model, test_loader, train_loader, device)
                    )
                    print(f"epoch:{epoch},step:{nb_steps}")
                    epsilon = accountant.get_epsilon(args.delta)
                    
        nb_examples_epoch = 0
        if nb_steps >= max_nb_steps:
            break
    epsilon = accountant.get_epsilon(args.delta)
    if is_main_worker:
        epsilons.append(epsilon)
    return nb_steps  # , norms2_before_sigma


def test(model, test_loader, train_loader, device):
    """
    Test the model on the testing set and the training set
    """
    model.eval()
    criterion = nn.CrossEntropyLoss()
    losses = []
    test_top1_acc = []
    train_top1_acc = []

    with torch.no_grad():
        for images, target in test_loader:
            images = images.to(device)
            target = target.to(device)

            output = model(images)
            loss = criterion(output, target)
            preds = np.argmax(output.detach().cpu().numpy(), axis=1)
            labels = target.detach().cpu().numpy()
            acc = accuracy(preds, labels)

            losses.append(loss.item())
            test_top1_acc.append(acc)

    test_top1_avg = np.mean(test_top1_acc)

    # with torch.no_grad():
    #     for images, target in train_loader:
    #         images = images.to(device)
    #         target = target.to(device)

    #         output = model(images)
    #         loss = criterion(output, target)
    #         preds = np.argmax(output.detach().cpu().numpy(), axis=1)
    #         labels = target.detach().cpu().numpy()
    #         acc = accuracy(preds, labels)

    #         # losses.append(loss.item())
    #         train_top1_acc.append(acc)
    # train_top1_avg = np.mean(train_top1_acc)
    # print(f"\tTest set:"f"Loss: {np.mean(losses):.6f} "f"Acc: {top1_avg * 100:.6f} ")
    return (test_top1_avg, 1.0) #train_top1_avg)


def main(batch_size):  # for non poisson, divide bs by world size

    args = parse_args()
    # init_distributed_mode(args)#Handle single and multi-GPU / multi-node]
    init_distributed_mode(args)
    logger = initialize_exp(args)
    activation = activation_dict[args.activation]
    model = WideResNetNorm(args.WRN_depth, 10, args.WRN_k, args.nb_groups,
                           args.init, args.order1, args.order2,
                           args.group_affine, args.max_lip_norm, args.lnorm,
                           activation)

    model.cuda()
    print_params(model)
    if args.multi_gpu:
        print("using multi GPU DPDDP")
        model = DPDDP(model)
    rank = args.global_rank
    is_main_worker = rank == 0
    weights = model.module.parameters() if args.multi_gpu else model.parameters()
    # Creating the datasets and dataloader for cifar10
    actual_batch_size = args.max_physical_batch_size // args.transform \
        if args.transform\
        else args.max_physical_batch_size
    train_dataset, train_loader, test_loader = prepare_data_cifar(
        args.data_root, actual_batch_size, args.proportion)
    # loss and optimizer
    optimizer = optim.SGD(weights, lr=args.lr, momentum=args.momentum,
                          weight_decay=args.weight_decay, nesterov=args.nesterov)
    sigma = get_noise_from_bs(args.batch_size, args.ref_noise, args.ref_B)

    lip_dp_parameters = LipDPParameters(batch_size, sigma,
                                        args.max_lip_norm, args.lnorm,
                                        'CrossEntropyLoss', args.loss_lip, 
                                        model, args.transform)

    train_loader = DPDataLoader.from_data_loader(train_loader)

    optimizer = prepare_optimizer(optimizer, lip_dp_parameters)
    accountant = RDPAccountant()
    accountant = prepare_accountant(accountant)
    # note that accountant takes only optim.noise_multiplier
    #  as noise_multiplier
    # TODO: check if sample rate is ok and if batch_size shouldn't be ref_B
    sample_rate = batch_size / len(train_loader.dataset)
    optimizer.register_step_pre_hook(
        accountant.get_optimizer_hook_fn(sample_rate=sample_rate)
    )
    step_freq = batch_size // actual_batch_size

    ema = None
    # we create a shadow model
    print("shadowing de model with EMA")
    ema = create_ema(model)
    train_acc, test_acc, epsilons, losses, top1_accs, grad_sample_gradients_norms_per_step = (0, 0, [], [
    ], [], [])

    E = 5
    if is_main_worker:
        print(f"E:{E},sigma:{sigma}, BATCH_SIZE:{args.batch_size}, noise_multiplier:{sigma}, EPOCHS:{E}")
    nb_steps = 0
    time_per_epoch = []
    for epoch in range(E):
        if nb_steps >= args.ref_nb_steps:
            break
        start_time = time.time()
        nb_steps = train(
            model,
            ema,
            train_loader,
            optimizer,
            epoch,
            args.ref_nb_steps,
            step_freq,
            rank,
            accountant,
            args.transform,
            logger,
            losses,
            top1_accs,
            epsilons,
            grad_sample_gradients_norms_per_step,
            test_loader,
            is_main_worker,
            args,
            nb_steps
        )
        end_time = time.time()
        time_per_epoch.append(end_time - start_time)
        if is_main_worker:
            print(
                f"epoch:{epoch}, Current loss:{losses[-1]:.2f},nb_steps:{nb_steps}, top1_acc of model (not ema){top1_accs[-1]:.2f}")
    if is_main_worker:
        logger.info("__log:" + json.dumps({
                    "batch_size": batch_size,
                    "time_per_epoch": time_per_epoch
                    }))


def parse_args():
    parser = argparse.ArgumentParser(description="PyTorch CIFAR10 DP Training")

    parser.add_argument("--batch_size", default=256, type=int,
                        help="Batch size for simulated training. It will automatically set the noise $\sigma$ s.t. $B/\sigma = B_{ref}/sigma_{ref}$")
    parser.add_argument("--max_physical_batch_size", default=256, type=int,
                        help="max_physical_batch_size for BatchMemoryManager",)
    parser.add_argument("--WRN_depth", default=16, type=int)
    parser.add_argument("--WRN_k", default=4, type=int, help="k of resnet block",)

    parser.add_argument("--lr", "--learning_rate", default=0.1, type=float,
                        metavar="LR", help="initial learning rate", dest="lr",)

    parser.add_argument("--momentum", default=0, type=float, help="SGD momentum",)
    parser.add_argument("--weight_decay", default=0, type=float, help="SGD weight_decay",)
    parser.add_argument("--nesterov", default=False, type=bool_flag, help="SGD nesterov",)
    parser.add_argument("--experiment", default=0, type=int,
                        help="seed for initializing training. ")
    parser.add_argument("-c", "--max_per_sample_grad_norm", type=float, default=1,
                        metavar="C", help="Clip per-sample gradients to this norm (default 1.0)",)
    parser.add_argument("--delta", type=float, default=1e-5, metavar="D",
                        help="Target delta (default: 1e-5)",)
    parser.add_argument("--ref_noise", type=float, default=3,
                        help="reference noise used with reference batch size and number of steps to create our physical constant",)
    parser.add_argument("--ref_B", type=int, default=4096,
                        help="reference batch size used with reference noise and number of steps to create our physical constant",)
    parser.add_argument("--nb_groups", type=int, default=16,
                        help="number of groups for the group norms",)
    parser.add_argument("--ref_nb_steps", default=2500, type=int,
                        help="reference number of steps used with reference noise and batch size to create our physical constant",)
    parser.add_argument("--data_root", type=str, default="../cifar10",
                        help="Where CIFAR10 is/will be stored",)
    parser.add_argument("--dump_path", type=str, default="", help="Where results will be stored",)
    parser.add_argument("--transform", type=int, default=0, help="using augmentation multiplicity",)

    parser.add_argument("--freq_log", type=int, default=20,
                        help="every each freq_log steps, we log",)

    parser.add_argument("--freq_log_val", type=int, default=100,
                        help="every each freq_log steps, we log val and ema acc",)

    parser.add_argument("--poisson_sampling", type=bool_flag,
                        default=True, help="using Poisson sampling",)

    parser.add_argument("--proportion", default=1, type=float,
                        help="proportion of the training set to use for training",)

    parser.add_argument("--exp_name", type=str, default="bypass")

    parser.add_argument("--init", type=int, default=0)
    parser.add_argument("--order1", type=int, default=0)
    parser.add_argument("--order2", type=int, default=0)
    parser.add_argument("--group_affine", type=bool_flag, default=False,
                        help="define if GroupNorm has learnable parameters")

    parser.add_argument("--local_rank", type=int, default=-1)
    parser.add_argument("--master_port", type=int, default=-1)
    parser.add_argument("--debug_slurm", type=bool_flag, default=False)

    parser.add_argument("--max_lip_norm", default=1, type=float,
                        help="maximum lipschitz value per layer",)
    parser.add_argument("--lnorm", default='2', type=str,
                        help="normalization method",)
    parser.add_argument(
        "--loss_lip", default=0.8, type=float, help="lipschitz value of loss"
    )
    parser.add_argument("--activation", default='relu', type=str,
                        help="name of activation function (relu or tanh)",)
    parser.add_argument("--debug", type=bool_flag, default=False)
    return parser.parse_args()


if __name__ == "__main__":
    batch_sizes = [int(i) for i in np.linspace(1, 20000, 20)][1:]
    for batch_size in batch_sizes:
        main(batch_size)
