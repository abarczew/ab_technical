import argparse
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import pandas as pd
import json

plt.style.use('seaborn-v0_8-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)
markers = ['o', '^', 's', 'P', 'v', '>', '<', '*', '+', 'x', 'D', 2]

methods = ['grad_clip', 'weight_norm']
exp_names = ['mnist', 'fashionmnist', 'cifar',
             'income', 'android', 'breast', 'default_credit',
             'dropout', 'german', 'nursery']

linestyles = ['-', '--']
colors = list(mcolors.TABLEAU_COLORS.keys())


lengend_marker = {"Papernot et al (2021)": 1,
                  "Abadi et al (2016)": 2,
                  "De et al (2022)": 3,
                  "Fu et al (2022)": 4,
                  "Tramèr and Boneh (2021)": 5}


parser = argparse.ArgumentParser()
parser.add_argument('--exp-name', '-e', dest='exp_name', default='mnist',
                    choices=exp_names,
                    help='experiment name: ' + ' | '.join(exp_names) +
                    ' (default: mnist)')
parser.add_argument('--save-dir', '-s', dest='save_dir',
                    help='The directory used to save the results',
                    default='save_temp', type=str)


def parse_log_to_df(filename='/tmp/a9k3rirjl6/train.log', net_type='wide'):
    """
    Read and parse logs from wideresnet to get accuracy and epsilon
    on test data.
    Args:
        filename: string of path to file
    Returns:
        dataframe of acc (with ema) and epsilon
    """
    log_data = open(filename,'r')
    result = []
    for line in log_data:
        if ('"test_acc_ema":' in line and net_type == 'wide') or\
        ('log:' in line and net_type != 'wide'):
            data = line.split('log:')[1]
            result.append(json.loads(data))
        else:
            continue
    df = pd.DataFrame.from_records(result)
    if net_type == 'wide':
        if '40' in filename:
            df = df.loc[(df.epsilon > 4.2)]
        else:
            df = df.loc[df.epsilon > 1.5]
        return df.rename({'test_acc_ema':'test_acc'}, axis=1)
    else:
        df.epsilon = df.epsilon.fillna(method='ffill')
        df = df.dropna(subset='test_acc', axis=0)
        df = df[['epsilon', 'test_acc']].groupby('epsilon').max().reset_index()
        return df

def parse_sota(filename='plots/cifar/sota.txt'):
    log_data = open(filename,'r')
    result = []
    for line in log_data:
        result.append(json.loads(line))
    return result

def plot_logs(filenames_logs,
                    filename_sota='plots/cifar/sota.txt',
                    path_results='plots/cifar/',
                    net_type='wide',
                    metric='test_acc',
                    agg='median'):
    """
    Retrieve data from logs and plots accuracy per epsilon against SOTA.
    Args:
        filenames_logs: list of paths to files where logs are stored
        filename_sota: path to file with sota results
    Returns
        Writes png files with accuracies per epsilon against sota.
    """
    df = pd.DataFrame()
    for filename in filenames_logs:
        df_log = parse_log_to_df(filename, net_type=net_type)
        df_log = df_log.cummax()
        df_log[metric] = df_log[metric].map(lambda x: round(x*100, 1))
        df_log['epsilon'] = df_log['epsilon'].map(lambda x: round(x, 2))
        df = pd.concat([df, df_log])
    df_plot = df[[metric, 'epsilon']].groupby(
            'epsilon').agg([agg, 'std']).reset_index()
    df_plot = df_plot.cummax()

    plt.errorbar(df_plot['epsilon'],
                    df_plot[metric][agg],
                    label='Lip-DP-SGD',
                    yerr=df_plot[metric]['std'],
                    marker=markers[0],
                    color=mcolors.TABLEAU_COLORS[colors[0]])
    
    points_sota = parse_sota(filename_sota)
    for i, point_sota in enumerate(points_sota):
        legend = point_sota['legend']
        idx_format = lengend_marker[legend]
        plt.scatter(*point_sota['coordinate'],
                    label=legend,
                    marker=markers[idx_format], 
                    color=mcolors.TABLEAU_COLORS[colors[idx_format]])

    plt.grid()
    plt.xlabel('Epsilon')
    plt.ylabel("Accuracy (%)")
    plt.legend(loc='lower right')

    plt.savefig(f'{path_results}acc_eps.png', bbox_inches='tight')
    plt.close()


def run():
    # args = parser.parse_args()
    plot_logs(['logs/cifar/train_40_0.log',
               'logs/cifar/train_40.log',
               'logs/cifar/train_40_1.log',
               'logs/cifar/train_16.log'],
               filename_sota='plots/cifar/sota.txt',
                path_results='plots/cifar/',
                net_type='wide')
    plot_logs([
        # 'logs/mnist/train_0.log',
               'logs/mnist/train_1.log',
               'logs/mnist/train_2.log',
            #    'logs/mnist/train_3.log',
            #    'logs/mnist/train_4.log',
               ],
               filename_sota='plots/mnist/sota.txt',
                path_results='plots/mnist/',
                net_type='conv')
    
    plot_logs([
        # 'logs/fashionmnist/train_0.log',
            #    'logs/fashionmnist/train_1.log',
               'logs/fashionmnist/train_2.log',
            #    'logs/fashionmnist/train_3.log',
               'logs/fashionmnist/train_4.log',
               ],
               filename_sota='plots/fashionmnist/sota.txt',
                path_results='plots/fashionmnist/',
                net_type='conv')


if __name__ == '__main__':
    run()
