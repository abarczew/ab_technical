import numpy as np
import pandas as pd
import scipy
from itertools import accumulate
from typing import List, Optional, Tuple

import torch
from torch.utils.data import Dataset, random_split

from sklearn.datasets import load_breast_cancer, fetch_california_housing, \
    load_diabetes
from sklearn.preprocessing import OneHotEncoder


DATADIR = 'data'

class MyDataset(Dataset):
    def __init__(self, X, y, standardize=False):
        if standardize:
            X = (X - X.min(axis=0)) / (X.max(axis=0) - X.min(axis=0))
        if scipy.sparse.issparse(X):
            self.x_data = torch.sparse_csr_tensor(torch.tensor(X.indptr,
                                                               dtype=torch.int64),
                                                  torch.tensor(X.indices,
                                                  dtype=torch.int64),
                                                  torch.tensor(X.data),
                                                  dtype=torch.float32)
        else:
            X = np.array(X, dtype=np.float32)
            self.x_data = torch.tensor(X, dtype=torch.float32)

        self.class_num = len(np.unique(y))
        self.y_data = torch.tensor(y, dtype=torch.int64)
        self.input_dim = X.shape[1]

    def __len__(self):
        return self.x_data.shape[0]

    def __getitem__(self, idx):
        return self.x_data[idx], self.y_data[idx]


def get_data(dataset_name, sample_size=-1):
    if dataset_name == 'framingham':
        df_fram = pd.read_csv(f'{DATADIR}/{dataset_name}.csv').fillna(0)
        X = df_fram[[col for col in df_fram.columns if col != 'TenYearCHD']]
        y = df_fram.TenYearCHD
    elif dataset_name == 'breast':
        data = load_breast_cancer()
        y = data.target
        X = data.data
    elif dataset_name == 'bank-full':
        df_ban = pd.read_csv(f'{DATADIR}/{dataset_name}.csv', sep=';')
        enc = OneHotEncoder(drop='first')
        X = enc.fit_transform(df_ban[[col for col
                                      in df_ban.columns if col != 'y']])
        y = df_ban.y.map(lambda x: 1 if x == 'yes' else 0)
    elif dataset_name == 'diabetes':
        df_dia = pd.read_csv(f'{DATADIR}/{dataset_name}.csv')
        target_name = 'Outcome'
        X = df_dia[[col for col in df_dia.columns if col != target_name]]
        y = df_dia[target_name]
    elif dataset_name == 'android':
        df_dia = pd.read_csv(f'{DATADIR}/{dataset_name}.csv')
        target_name = 'Result'
        X = df_dia[[col for col in df_dia.columns if col != target_name]]
        y = df_dia[target_name]
    elif dataset_name == 'titanic':
        df = pd.read_csv(f'{DATADIR}/{dataset_name}.csv')
        target_name = 'Survived'
        train_data = df.copy()
        train_data["Age"].fillna(df["Age"].median(skipna=True), inplace=True)
        train_data["Embarked"].fillna(df['Embarked'].value_counts().idxmax(), inplace=True)
        train_data.drop('Cabin', axis=1, inplace=True)
        train_data['TravelAlone'] = np.where((train_data["SibSp"] + train_data["Parch"]) > 0, 0, 1)
        train_data.drop('SibSp', axis=1, inplace=True)
        train_data.drop('Parch', axis=1, inplace=True)
        training = pd.get_dummies(train_data, columns=["Pclass", "Embarked", "Sex"])
        training.drop('Sex_female', axis=1, inplace=True)
        training.drop('PassengerId', axis=1, inplace=True)
        training.drop('Name', axis=1, inplace=True)
        training.drop('Ticket', axis=1, inplace=True)
        X = training[[col for col in training.columns if col != target_name]]
        y = training[target_name]
    elif dataset_name == 'income':
        names = ['age', 'workclass', 'fnlwgt', 'education', 'education-num',
                 'marital-status', 'occupation', 'relationship', 'race',
                 'sex', 'capital-gain', 'capital-loss', 'hours-per-week',
                 'native-country', 'income']
        df = pd.read_csv(f"{DATADIR}/adult.data", names=names)
        df = df.applymap(lambda x: np.nan if x == '?' else x)
        target_name = 'income'
        input_features = [col for col in df.columns if col not in ['kfold', target_name]]
        numerical_columns = ['age', 'fnlwgt', 'capital-gain', 'capital-loss', 'hours-per-week']
        categorical_columns = [col for col in input_features if not col in numerical_columns]

        def impute_null_values(data, categorical_columns, numerical_columns):
            for col in categorical_columns:
                data.loc[:, col] = data[col].fillna("NONE")

            for cols in numerical_columns:
                data.loc[:, cols] = data[cols].fillna(data[cols].median())
            return data
        df = impute_null_values(df, categorical_columns, numerical_columns)
        enc = OneHotEncoder(sparse=False, drop='first')
        df_cat = pd.DataFrame(enc.fit_transform(df[categorical_columns]))
        X = pd.concat([df[numerical_columns], df_cat], axis=1)
        y = df[target_name].map({' <=50K': 0, ' >50K': 1})
    elif dataset_name == 'housing':
        data = fetch_california_housing()
        y = data.target
        X = data.data
    elif dataset_name == 'diabetes_reg':
        data = load_diabetes()
        y = data.target
        X = data.data
    elif dataset_name == 'patient_survival':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.csv')
        target_name = 'hospital_death'
        numerical_cat = [
            'elective_surgery',
            'apache_post_operative',
            'arf_apache',
            'gcs_unable_apache',
            'intubated_apache',
            'ventilated_apache',
            'aids',
            'cirrhosis',
            'diabetes_mellitus',
            'hepatic_failure',
            'immunosuppression',
            'leukemia',
            'lymphoma',
            'solid_tumor_with_metastasis']
        categorical = ['ethnicity',
                       'gender',
                       'icu_type',
                       'apache_3j_bodysystem',
                       'apache_2_bodysystem']
        col_drop = ['encounter_id', 'icu_admit_source', 'Unnamed: 83',
                    'icu_id', 'icu_stay_type', 'patient_id', 'hospital_id']
        bmi_col = ['bmi', 'weight', 'height']
        num_col = [col for col in data.columns
                   if col not in numerical_cat+categorical
                   + col_drop+[target_name]]
        data = data.drop(col_drop, axis=1)
        data = data.loc[data[bmi_col].dropna(axis=0).index]
        data = data.loc[data[categorical].dropna(axis=0).index]
        data[numerical_cat] = data[numerical_cat].fillna(0)
        data[num_col] = data[num_col].fillna(data[num_col].mean())
        X = data[[col for col in data.columns if col != target_name]]
        X = pd.get_dummies(X, columns=categorical, drop_first=True)
        y = data[target_name]
    elif dataset_name == 'german':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.data-numeric',
                           header=None, delimiter='\s+')
        y = data.values[:, -1]
        y = np.array(list(map(lambda x: 0 if x == 2 else x, y)))
        X = data.values[:, :-1]
    elif dataset_name == 'dropout':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.csv', delimiter=';')
        target_name = 'Target'
        y = data[target_name]
        y = np.array(list(map(lambda x: 0 if x == 'Graduate' else 1, y)))
        X = data[[col for col in data.columns if col != target_name]]
        categorical = ['Marital status', 'Application mode',
                       'Application order', 'Course',
                       'Previous qualification', 'Nacionality',
                       "Mother's qualification", "Father's qualification",
                       "Mother's occupation", "Father's occupation"]
        X = pd.get_dummies(X, columns=categorical, drop_first=True)
        X = X.applymap(lambda x: int(x) if type(x) == bool else x)
    elif dataset_name == 'nursery':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.data',
                           header=None, delimiter=',')
        y = data.values[:, -1]
        mask = {k: v for v, k in enumerate(np.unique(y))}
        y = np.array(list(map(lambda x: mask[x], y)))
        X = data.values[:, :-1]
        X = pd.get_dummies(pd.DataFrame(X), drop_first=True)
        X = X.applymap(lambda x: int(x) if type(x) == bool else x)
    elif dataset_name == 'shuttle':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.tst',
                           header=None, delimiter='\s+')
        y = data.values[:, -1] - 1
        X = data.values[:, :-1]
    elif dataset_name == 'default_credit':
        data = pd.read_excel(f'{DATADIR}/{dataset_name}.xls', skiprows=1)
        categorical = ['EDUCATION', 'MARRIAGE']
        target = 'default payment next month'
        dropcol = 'ID'
        y = data[target]
        X = data.drop([dropcol, target], axis=1)
        X = pd.get_dummies(X, columns=categorical, drop_first=True)
        X = X.applymap(lambda x: int(x) if type(x) == bool else x)
    elif dataset_name == 'thyroid':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.csv')
        data = data.values[:, 1:]
        y = (data[:, -1] - 1).astype('int')
        X = data[:, :-1]
    elif dataset_name == 'yeast':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.data',
                           header=None, delimiter='\s+')
        data = data.values[:, 1:]
        y = data[:, -1]
        mask = {k: v for v, k in enumerate(np.unique(y))}
        y = np.array(list(map(lambda x: mask[x], y)))
        X = data[:, :-1]
        X = X.astype(float)
    elif dataset_name == 'kddcup':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.data_10_percent_corrected',
                           header=None, delimiter=',')
        y = data[41].values
        mask = {k: v for v, k in enumerate(np.unique(y))}
        y = np.array(list(map(lambda x: mask[x], y)))
        X = data.drop(41, axis=1)
        X = pd.get_dummies(X, drop_first=True).values
    else:
        raise 'Dataset not in datasets list'
    if sample_size == -1:
        return X, y
    else:
        rnd_index = np.random.choice(len(y), sample_size, replace=False)
        return X.loc[rnd_index], y.loc[rnd_index]

def load_tabular_data(exp_name, test_size=0.2, generator=torch.Generator().manual_seed(42)):
    X, y = get_data(exp_name, sample_size=-1)
    dataset = MyDataset(X, np.array(y), standardize=-1)
    class_num = dataset.class_num
    input_dim = dataset.input_dim
    test_set_size = int(len(dataset) * test_size)
    train_set_size = len(dataset) - test_set_size
    train_valid_set, test_set = random_split(dataset,
                                             [train_set_size,
                                              test_set_size],
                                             generator=generator)
    return train_valid_set, test_set, input_dim, class_num