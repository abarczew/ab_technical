import argparse
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import json

plt.style.use('seaborn-v0_8-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)
markers = ['o', '^', 's', 'P', 'v', '>', '<', '*', '+', 'x', 'D', 2]

methods = ['grad_clip', 'weight_norm']
exp_names = ['mnist', 'fashionmnist', 'cifar',
             'income', 'android', 'breast', 'default_credit',
             'dropout', 'german', 'nursery']

linestyles = ['-', '--']
colors = list(mcolors.TABLEAU_COLORS.keys())


parser = argparse.ArgumentParser()
parser.add_argument('--exp-name', '-e', dest='exp_name', default='mnist',
                    choices=exp_names,
                    help='experiment name: ' + ' | '.join(exp_names) +
                    ' (default: mnist)')
parser.add_argument('--save-dir', '-s', dest='save_dir',
                    help='The directory used to save the results',
                    default='save_temp', type=str)


def parse_log_to_df(filename='/tmp/a9k3rirjl6/train.log'):
    """
    Read and parse logs from wideresnet to get accuracy and epsilon
    on test data.
    Args:
        filename: string of path to file
    Returns:
        dataframe of acc (with ema) and epsilon
    """
    log_data = open(filename,'r')
    result = []
    for line in log_data:
        if 'mnist' in filename:
            if ('batch_size' in line) and ('INFO:' in line):
                data = line.split('INFO:')[1]
                data = data.replace("'", '"')
                result.append(json.loads(data))
            else:
                continue
        elif 'cifar' in filename:
            if ('batch_size' in line) and ('log:' in line):
                data = line.split('log:')[1]
                result.append(json.loads(data))
            else:
                continue
    df = pd.DataFrame.from_records(result)
    return df

def plot_logs(filenames, labels, path_results):
    """
    Retrieve data from logs and plots runtime per batchsize.
    Args:
        filenames_logs: list of paths to files where logs are stored
        filename_sota: path to file with sota results
    Returns
        Writes png files with accuracies per epsilon against sota.
    """
    i = 0
    for filename, label in zip(filenames, labels):
        df = parse_log_to_df(filename)
        df['time'] = df['time_per_epoch'].map(np.median)

        plt.plot(df['batch_size'],
                        df['time'],
                        label=label,
                        marker=markers[i],
                        color=mcolors.TABLEAU_COLORS[colors[i]])
        i += 1

    plt.grid()
    plt.xlabel('Batch size')
    plt.ylabel("Median time per epoch")
    plt.legend(loc='lower right')

    plt.savefig(f'{path_results}runtime.png', bbox_inches='tight')
    plt.close()


def run():
    plot_logs(['logs/cifar/runtime/train.log',
               'logs/cifar/runtime_grad/train.log'],
               labels=['Lip-DP-SGD', 'DP-SGD'],
                path_results='plots/cifar/')
    plot_logs([
               'logs/mnist/runtime.log',
               'logs/mnist/runtime_dpsgd.log',
               ],
               labels=['Lip-DP-SGD', 'DP-SGD'],
                path_results='plots/mnist/')


if __name__ == '__main__':
    run()
