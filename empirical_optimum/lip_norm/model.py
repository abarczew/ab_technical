import math
import numpy as np
import sys
from torch.autograd import Variable
import torch.nn.init as init
import torch
import torch.nn as nn
import torch.nn.functional as F
from lip_dp.normalizer import normalization_conv, normalization_linear
from opacus.grad_sample import register_grad_sampler
from opacus.utils.tensor_utils import unfold2d


class ConvNet(nn.Sequential):
    """
    ConvNet: base model with regularization techniques.
    Model from https://github.com/pytorch/opacus/blob/main/examples/cifar10.py
    Note that Papernot et al. 2020 discuss the depth of models with DP
    See https://arxiv.org/pdf/2007.14191.pdf.
    """

    def __init__(self, num_classes=10):
        super(ConvNet, self).__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(3, 32, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten(start_dim=1, end_dim=-1),
        )
        self.linear = nn.Linear(128, num_classes, bias=True)

    def forward(self, x):
        y = self.conv(x)
        return self.linear(y)


class ConvNetGN(nn.Sequential):
    """
    Same model as ConvNet with Group Normalization.
    See https://arxiv.org/pdf/1803.08494.pdf
    """

    def __init__(self, num_classes=10, group_affine=False):
        super(ConvNetGN, self).__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(3, 32, kernel_size=3, stride=1, padding=1),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten(start_dim=1, end_dim=-1),
            nn.Linear(128, num_classes, bias=True),
        )
        self.linear = nn.Linear(128, num_classes, bias=True)

    def forward(self, x):
        y = self.conv(x)
        return self.linear(y)


class Conv2dWeightStd(nn.Conv2d):
    """
    Convolution layer with weight standardization.
    See https://arxiv.org/pdf/1903.10520.pdf
    # TODO: close to the froebinus norm, should we test it?
    """

    def __init__(self, in_channels, out_channels, kernel_size, stride=1,
                 padding=0, dilation=1, groups=1, bias=True):
        super(Conv2dWeightStd, self).__init__(in_channels, out_channels,
                                              kernel_size, stride,
                                              padding, dilation, groups, bias)

    def forward(self, x):
        weight = self.weight
        weight_mean = weight.mean(dim=1, keepdim=True)\
                            .mean(dim=2, keepdim=True)\
                            .mean(dim=3, keepdim=True)
        weight = weight - weight_mean
        std = weight.view(weight.size(0), -1)\
                    .std(dim=1)\
                    .view(-1, 1, 1, 1)\
            + 1e-5
        weight = weight / std.expand_as(weight)
        return F.conv2d(x, weight, self.bias, self.stride,
                        self.padding, self.dilation, self.groups)


class ConvNetGNWeightStd(nn.Sequential):
    """
    Same model as ConvNet with group normalization and Conv2dWeightStd.
    """

    def __init__(self, num_classes=10, group_affine=False):
        super(ConvNetGNWeightStd, self).__init__()
        self.classifier = nn.Sequential(
            Conv2dWeightStd(3, 32, kernel_size=3, stride=1, padding=1),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dWeightStd(32, 64, kernel_size=3, stride=1, padding=1),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dWeightStd(64, 64, kernel_size=3, stride=1, padding=1),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dWeightStd(64, 128, kernel_size=3, stride=1, padding=1),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten(start_dim=1, end_dim=-1),
            nn.Linear(128, num_classes, bias=True),
        )

    def forward(self, x):
        return self.classifier(x)


class Conv2dNorm(nn.Conv2d):
    """
    Convolution layer with weight normalization.
    # TODO: should the lnorm and max_norm_i be initialized?
    # TODO: how can the max_norm_i be a learned parameter?
    """

    def __init__(self, in_channels, out_channels, kernel_size, stride=1,
                 padding=0, dilation=1, groups=1, bias=True, max_norm_i=1,
                 lnorm='inf'):
        super(Conv2dNorm, self).__init__(in_channels, out_channels,
                                         kernel_size, stride,
                                         padding, dilation, groups, bias)
        self.max_norm_i = max_norm_i
        self.lnorm = lnorm

    def forward(self, x):
        weight = normalization_conv(self.weight, lnorm='std')
        weight = normalization_conv(weight, self.max_norm_i, self.lnorm)
        self.weight.data = weight
        return F.conv2d(x, weight, self.bias, self.stride,
                        self.padding, self.dilation, self.groups)


@register_grad_sampler(Conv2dNorm)
def compute_conv_grad_sample(module, activations, backprops):
    n = activations.shape[0]
    activations = unfold2d(
        activations,
        kernel_size=module.kernel_size,  # 3
        padding=module.padding,           # 1
        stride=module.stride,                   # 1
        dilation=module.dilation,              # 1
    )
    backprops = backprops.reshape(n, -1, activations.shape[-1])
    # n=batch_sz; o=num_out_channels; p=(num_in_channels/groups)*kernel_sz
    grad_sample = torch.einsum("noq,npq->nop", backprops, activations)
    # rearrange the above tensor and extract diagonals.
    grad_sample = grad_sample.view(
        n,
        module.groups,
        -1,
        module.groups,
        int(module.in_channels / module.groups),
        np.prod(1),
    )
    grad_sample = torch.einsum("ngrg...->ngr...", grad_sample).contiguous()
    shape = [n] + list(module.weight.shape)

    ret = {module.weight: grad_sample.view(shape)}
    if module.bias is not None:
        ret[module.bias] = torch.sum(backprops, dim=2)

    return ret


class LinearNorm(nn.Linear):
    """
    Linear layer with weight normalization.
    """

    def __init__(self, in_features, out_features, bias=True,
                 max_norm_i=1, lnorm='inf'):
        super(LinearNorm, self).__init__(in_features, out_features, bias)
        self.max_norm_i = max_norm_i
        self.lnorm = lnorm

    def forward(self, x):
        weight = normalization_linear(self.weight, self.max_norm_i, self.lnorm)
        self.weight.data = weight
        return F.linear(x, weight, self.bias)


@register_grad_sampler(LinearNorm)
def compute_linear_grad_sample(
        layer, activations, backprops):
    """
    Computes per sample gradients for ``nn.Linear`` layer
    Args:
        layer: Layer
        activations: Activations
        backprops: Backpropagations
    """
    gs = torch.einsum("n...i,n...j->nij", backprops, activations)
    ret = {layer.weight: gs}
    if layer.bias is not None:
        ret[layer.bias] = torch.einsum("n...k->nk", backprops)

    return ret

# TODO: make max_norm_i learnable with regulizer
# TODO: implement loss lip (learnable?)
# TODO: test with other optimizer
# TODO: use actual norm if norm < max_norm
# TODO: make privacy engine


class ConvNetGNWeightNorm(nn.Sequential):
    """
    Same model as ConvNet with group normalization and Conv2d/LinearNorm.
    """

    def __init__(self, num_classes=10, group_affine=False,
                 max_norm_i=1, lnorm='inf'):
        super(ConvNetGNWeightNorm, self).__init__()
        self.max_norm_i = max_norm_i
        self.lnorm = lnorm
        self.classifier = nn.Sequential(
            Conv2dNorm(3, 32, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dNorm(32, 64, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dNorm(64, 64, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dNorm(64, 128, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten(start_dim=1, end_dim=-1),
            LinearNorm(128, num_classes, bias=True,
                       max_norm_i=max_norm_i, lnorm=lnorm),
        )

    def forward(self, x):
        return self.classifier(x)


class ConvNetGNTanhWeightNorm(nn.Sequential):
    """
    Same model as ConvNet with group normalization and Conv2d/LinearNorm.
    """

    def __init__(self, num_classes=10, group_affine=False,
                 max_norm_i=1, lnorm='inf'):
        super(ConvNetGNTanhWeightNorm, self).__init__()
        self.max_norm_i = max_norm_i
        self.lnorm = lnorm
        self.conv = nn.Sequential(
            Conv2dNorm(3, 32, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.Tanh(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dNorm(32, 64, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.Tanh(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dNorm(64, 64, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.Tanh(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dNorm(64, 128, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.Tanh(),
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten(start_dim=1, end_dim=-1),
        )
        self.linear = LinearNorm(128, num_classes, bias=True,
                                 max_norm_i=max_norm_i, lnorm=lnorm)

    def forward(self, x):
        y = self.conv(x)
        return self.linear(y)


class GroupNormLip(nn.GroupNorm):
    """GroupNorm that only performs the zero mean."""

    def __init__(self, *args, **kwargs):
        super(GroupNormLip, self).__init__(*args, **kwargs)

    def forward(self, input):
        N, C, H, W = input.shape
        G = self.num_groups
        input = torch.reshape(input, [N, G, C // G, H, W])
        mean = input.mean(dim=[2, 3, 4], keepdim=True)
        invstd = torch.sqrt(input.var([2, 3, 4], unbiased=False, keepdim=True))
        invstd = torch.maximum(invstd, torch.ones_like(invstd))
        output = (input - mean) / invstd
        output = torch.reshape(output, [N, C, H, W])
        return output


class LayerNormLip(nn.GroupNorm):
    """Reset layer input norm to 1."""

    def __init__(self, *args, **kwargs):
        super(LayerNormLip, self).__init__(*args, **kwargs)

    def forward(self, input):
        import ipdb
        ipdb.set_trace()
        max_norm = torch.linalg.vector_norm(input, dim=(1, 2, 3), keepdim=True)
        max_norm = torch.maximum(max_norm, torch.ones_like(max_norm))
        return input / max_norm

####### Reproduce SOTA #########


class ConvNetSotaLipTanh(nn.Sequential):
    """
    Same model as in https://arxiv.org/pdf/2007.14191.pdf
    with relu, group normalization and Conv2dWeightStd as
    in https://arxiv.org/pdf/2204.13650.pdf
    """

    def __init__(self, num_classes=10, group_affine=False,
                 max_norm_i=1, lnorm='inf'):
        super(ConvNetSotaLipTanh, self).__init__()
        self.gn = nn.GroupNorm(16, 64, affine=group_affine)
        # self.gn = GroupNormLip(16, 64, affine=group_affine)
        # self.gn = [nn.GroupNorm(16, 64, affine=group_affine),
        # self.ln = LayerNormLip(16, 64, affine=False)
        self.conv = nn.Sequential(
            # self.ln,
            Conv2dNorm(3, 32, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            nn.Tanh(),
            # self.ln,
            Conv2dNorm(32, 32, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            nn.Tanh(),
            # self.ln,
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dNorm(32, 64, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            nn.Tanh(),
            # self.ln,
            Conv2dNorm(64, 64, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            nn.Tanh(),
            # self.ln,
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dNorm(64, 128, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            nn.Tanh(),
            # self.ln,
            Conv2dNorm(128, 128, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            nn.Tanh(),
            # self.ln,
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dNorm(128, 256, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            nn.Tanh(),
            # self.ln,
            Conv2dNorm(256, 10, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            # nn.GroupNorm(16, 64, affine=group_affine),
            nn.Tanh(),
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten(start_dim=1, end_dim=-1),
        )
        self.linear = LinearNorm(10, num_classes, bias=True,
                                 max_norm_i=max_norm_i, lnorm=lnorm)

    def forward(self, x):
        y = self.conv(x)
        return self.linear(y)


class ConvNetSota(nn.Sequential):
    """
    Same model as in https://arxiv.org/pdf/2007.14191.pdf
    with relu, group normalization and Conv2dWeightStd as
    in https://arxiv.org/pdf/2204.13650.pdf
    """

    def __init__(self, num_classes=10, group_affine=False):
        super(ConvNetSota, self).__init__()
        self.classifier = nn.Sequential(
            Conv2dWeightStd(3, 32, kernel_size=3, stride=1, padding=1),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            Conv2dWeightStd(32, 32, kernel_size=3, stride=1, padding=1),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dWeightStd(32, 64, kernel_size=3, stride=1, padding=1),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            Conv2dWeightStd(64, 64, kernel_size=3, stride=1, padding=1),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dWeightStd(64, 128, kernel_size=3, stride=1, padding=1),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            Conv2dWeightStd(128, 128, kernel_size=3, stride=1, padding=1),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dWeightStd(128, 256, kernel_size=3, stride=1, padding=1),
            nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            Conv2dWeightStd(256, 10, kernel_size=3, stride=1, padding=1),
            # nn.GroupNorm(16, 64, affine=group_affine),
            nn.ReLU(),
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten(start_dim=1, end_dim=-1),
            nn.Linear(10, num_classes, bias=True),
        )

    def forward(self, x):
        return self.classifier(x)


def conv3x3(in_planes, out_planes, stride=1):
    return Conv2dWeightStd(in_planes, out_planes, kernel_size=3,
                           stride=stride, padding=1, bias=True)


def conv_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        # TODO: check if not another initialization
        init.xavier_uniform_(m.weight, gain=np.sqrt(2))
        init.constant_(m.bias, 0)
    elif classname.find('BatchNorm') != -1:
        init.constant_(m.weight, 1)
        init.constant_(m.bias, 0)


class wide_basic(nn.Module):
    # no dropout as in De et al.
    def __init__(self, in_planes, planes, dropout_rate, stride=1):
        super(wide_basic, self).__init__()
        self.gn1 = nn.GroupNorm(16, 64, affine=False)
        self.conv1 = Conv2dWeightStd(in_planes, planes, kernel_size=3,
                                     padding=1, bias=True)
        self.dropout = nn.Dropout(p=dropout_rate)
        self.gn2 = nn.GroupNorm(16, 64, affine=False)
        self.conv2 = Conv2dWeightStd(planes, planes, kernel_size=3,
                                     stride=stride, padding=1, bias=True)

        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != planes:
            self.shortcut = nn.Sequential(
                nn.ReLU(),
                nn.GroupNorm(16, 64, affine=False),
                Conv2dWeightStd(in_planes, planes, kernel_size=1,
                                stride=stride, bias=True),
            )

    def forward(self, x):
        out = self.conv1(self.gn1(F.relu(x)))
        out = self.conv2(self.gn2(F.relu(out)))
        out += self.shortcut(x)
        return out


class WideResNetSota(nn.Module):
    """
    Implements a Wide Resnet as introduced by
    https://arxiv.org/pdf/1605.07146.pdf
    which works with dp-sgd as in https://arxiv.org/pdf/2204.13650.pdf
    """

    def __init__(self, depth, widen_factor, dropout_rate, num_classes,
                 block=wide_basic):
        super(WideResNetSota, self).__init__()
        self.in_planes = 16

        assert ((depth-4) % 6 == 0), 'Wide-resnet depth should be 6n+4'
        n = (depth-4)/6
        k = widen_factor

        print('| Wide-Resnet %dx%d' % (depth, k))
        nStages = [16, 16*k, 32*k, 64*k]

        self.conv1 = conv3x3(3, nStages[0])
        self.layer1 = self._wide_layer(wide_basic, nStages[1], n, dropout_rate,
                                       stride=1)
        self.layer2 = self._wide_layer(wide_basic, nStages[2], n, dropout_rate,
                                       stride=2)
        self.layer3 = self._wide_layer(wide_basic, nStages[3], n, dropout_rate,
                                       stride=2)
        self.gn1 = nn.GroupNorm(16, 64, affine=False)
        self.linear = nn.Linear(nStages[3], num_classes)

    def _wide_layer(self, block, planes, num_blocks, dropout_rate, stride):
        strides = [stride] + [1]*(int(num_blocks)-1)
        layers = []
        for stride in strides:
            layers.append(block(self.in_planes, planes, dropout_rate, stride))
            self.in_planes = planes
        return nn.Sequential(*layers)

    def forward(self, x):
        out = self.conv1(x)
        out = self.layer1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.gn1(F.relu(out))
        out = F.avg_pool2d(out, 8)
        out = out.view(out.size(0), -1)
        out = self.linear(out)
        return out


"""
Adapted from timm:
https://github.com/xternalz/WideResNet-pytorch/blob/master/wideresnet.py
"""


class BasicBlockNorm(nn.Module):
    """
    Implements Basic block of WideResNet with Conv2dNorm.
    """

    def __init__(self, in_planes, out_planes, stride, nb_groups, order,
                 group_affine, max_norm_i, lnorm, activation):
        super(BasicBlockNorm, self).__init__()
        self.order = order
        self.bn1 = nn.GroupNorm(nb_groups, in_planes,
                                affine=group_affine) if nb_groups else nn.Identity()
        self.act1 = activation()
        self.conv1 = Conv2dNorm(
            in_planes, out_planes, kernel_size=3, stride=stride, padding=1,
            max_norm_i=max_norm_i, lnorm=lnorm
        )
        self.bn2 = nn.GroupNorm(nb_groups, out_planes,
                                affine=group_affine) if nb_groups else nn.Identity()
        self.act2 = activation()
        self.conv2 = Conv2dNorm(
            out_planes, out_planes, kernel_size=3, stride=1, padding=1,
            max_norm_i=max_norm_i, lnorm=lnorm
        )

        self.equalInOut = in_planes == out_planes
        self.bnShortcut = (
            (not self.equalInOut)
            and nb_groups
            and nn.GroupNorm(nb_groups, in_planes, affine=group_affine)
            or (not self.equalInOut)
            and nn.Identity()
            or None
        )
        self.convShortcut = (
            (not self.equalInOut)
            and Conv2dNorm(
                in_planes, out_planes, kernel_size=1, stride=stride, padding=0,
                max_norm_i=max_norm_i, lnorm=lnorm
            )
        ) or None

    def forward(self, x):
        skip = x
        assert self.order in [0, 1, 2, 3]
        if self.order == 0:  # DM accuracy good
            if not self.equalInOut:
                skip = self.convShortcut(self.bnShortcut(self.act1(x)))
            out = self.conv1(self.bn1(self.act1(x)))
            out = self.conv2(self.bn2(self.act2(out)))
        elif self.order == 1:  # classic accuracy bad
            if not self.equalInOut:
                skip = self.convShortcut(self.act1(self.bnShortcut(x)))
            out = self.conv1(self.act1(self.bn1(x)))
            out = self.conv2(self.act2(self.bn2(out)))
        elif self.order == 2:  # DM IN RESIDUAL, normal other
            if not self.equalInOut:
                skip = self.convShortcut(self.bnShortcut(self.act1(x)))
            out = self.conv1(self.act1(self.bn1(x)))
            out = self.conv2(self.act2(self.bn2(out)))
        elif self.order == 3:  # normal in residualm DM in others
            if not self.equalInOut:
                skip = self.convShortcut(self.act1(self.bnShortcut(x)))
            out = self.conv1(self.bn1(self.act1(x)))
            out = self.conv2(self.bn2(self.act2(out)))
        return torch.add(skip, out)


class NetworkBlockNorm(nn.Module):
    def __init__(
        self, nb_layers, in_planes, out_planes, block, stride, nb_groups, order,
        group_affine, max_norm_i, lnorm, activation
    ):
        super(NetworkBlockNorm, self).__init__()
        self.layer = self._make_layer(
            block, in_planes, out_planes, nb_layers, stride, nb_groups, order,
            group_affine, max_norm_i, lnorm, activation
        )

    def _make_layer(
        self, block, in_planes, out_planes, nb_layers, stride, nb_groups, order,
        group_affine, max_norm_i, lnorm, activation
    ):
        layers = []
        for i in range(int(nb_layers)):
            layers.append(
                block(
                    i == 0 and in_planes or out_planes,
                    out_planes,
                    i == 0 and stride or 1,
                    nb_groups,
                    order,
                    group_affine,
                    max_norm_i,
                    lnorm,
                    activation
                )
            )
        return nn.Sequential(*layers)

    def forward(self, x):
        return self.layer(x)


class WideResNetNorm(nn.Module):
    """
    WideResNet with lipschitz constrained layers.
    """

    def __init__(
        self,
        depth,
        num_classes,
        widen_factor=1,
        nb_groups=16,
        init=0,
        order1=0,
        order2=0,
        group_affine=False,
        max_norm_i=1,
        lnorm='2',
        activation=nn.ReLU
    ):
        if order1 == 0:
            print("order1=0: In the blocks: like in DM, BN on top of relu")
        if order1 == 1:
            print("order1=1: In the blocks: not like in DM, relu on top of BN")
        if order1 == 2:
            print(
                "order1=2: In the blocks: BN on top of relu in residual (DM), relu on top of BN ortherplace (clqssique)"
            )
        if order1 == 3:
            print(
                "order1=3: In the blocks:  relu on top of BN  in residual (classic), BN on top of relu otherplace (DM)"
            )
        if order2 == 0:
            print("order2=0: outside the blocks:  like in DM, BN on top of relu")
        if order2 == 1:
            print("order2=1: outside the blocks:  not like in DM, relu on top of BN")
        super(WideResNetNorm, self).__init__()
        nChannels = [16, 16 * widen_factor, 32 * widen_factor, 64 * widen_factor]
        assert (depth - 4) % 6 == 0
        n = (depth - 4) / 6
        block = BasicBlockNorm
        self.max_norm_i = max_norm_i
        self.lnorm = lnorm
        # 1st conv before any network block
        self.conv1 = Conv2dNorm(3, nChannels[0], kernel_size=3, stride=1, padding=1,
                                max_norm_i=max_norm_i, lnorm=lnorm)
        # 1st block
        self.block1 = NetworkBlockNorm(
            n, nChannels[0], nChannels[1], block, 1, nb_groups, order1,
            group_affine, max_norm_i=max_norm_i, lnorm=lnorm, activation=activation
        )
        # 2nd block
        self.block2 = NetworkBlockNorm(
            n, nChannels[1], nChannels[2], block, 2, nb_groups, order1,
            group_affine, max_norm_i=max_norm_i, lnorm=lnorm, activation=activation
        )
        # 3rd block
        self.block3 = NetworkBlockNorm(
            n, nChannels[2], nChannels[3], block, 2, nb_groups, order1,
            group_affine, max_norm_i=max_norm_i, lnorm=lnorm, activation=activation
        )
        # global average pooling and classifier
        self.bn1 = nn.GroupNorm(
            nb_groups, nChannels[3], affine=group_affine) if nb_groups else nn.Identity()
        self.act = activation()
        self.fc = LinearNorm(nChannels[3], num_classes,
                             max_norm_i=max_norm_i, lnorm=lnorm)
        self.nChannels = nChannels[3]
        if init == 0:  # as in Deep Mind's paper
            for m in self.modules():
                if isinstance(m, Conv2dNorm):
                    fan_in, fan_out = nn.init._calculate_fan_in_and_fan_out(m.weight)
                    s = 1 / (max(fan_in, 1)) ** 0.5
                    nn.init.trunc_normal_(m.weight, std=s)
                    m.bias.data.zero_()
                elif isinstance(m, nn.GroupNorm) and group_affine:
                    m.weight.data.fill_(1)
                    m.bias.data.zero_()
                elif isinstance(m, LinearNorm):
                    fan_in, fan_out = nn.init._calculate_fan_in_and_fan_out(m.weight)
                    s = 1 / (max(fan_in, 1)) ** 0.5
                    nn.init.trunc_normal_(m.weight, std=s)
                    m.bias.data.zero_()
        if init == 1:  # old version
            for m in self.modules():
                if isinstance(m, Conv2dNorm):
                    nn.init.kaiming_normal_(
                        m.weight, mode="fan_out", nonlinearity="relu"
                    )
                elif isinstance(m, nn.GroupNorm) and group_affine:
                    m.weight.data.fill_(1)
                    m.bias.data.zero_()
                elif isinstance(m, LinearNorm):
                    m.bias.data.zero_()
        self.order2 = order2

    def forward(self, x):
        out = self.conv1(x)
        out = self.block1(out)
        out = self.block2(out)
        out = self.block3(out)
        out = self.bn1(self.act(out)) if self.order2 == 0 else self.act(self.bn1(out))
        out = F.avg_pool2d(out, 8)
        out = out.view(-1, self.nChannels)
        return self.fc(out)


class ConvNetCifar(nn.Sequential):
    """
    Same model as in https://arxiv.org/pdf/2007.14191.pdf
    with relu or tanh, group normalization and Conv2dWeightStd as
    in https://arxiv.org/pdf/2204.13650.pdf
    """

    def __init__(self, num_classes=10, group_affine=False,
                 max_norm_i=1, lnorm='inf', activation=nn.Tanh):
        super(ConvNetCifar, self).__init__()
        self.gn = nn.GroupNorm(16, 64, affine=group_affine)
        self.act = activation()
        # self.gn = GroupNormLip(16, 64, affine=group_affine)
        # self.gn = [nn.GroupNorm(16, 64, affine=group_affine),
        # self.ln = LayerNormLip(16, 64, affine=False)
        self.conv = nn.Sequential(
            # self.ln,
            Conv2dNorm(3, 32, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            self.act,
            # self.ln,
            Conv2dNorm(32, 32, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            self.act,
            # self.ln,
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dNorm(32, 64, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            self.act,
            # self.ln,
            Conv2dNorm(64, 64, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            self.act,
            # self.ln,
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dNorm(64, 128, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            self.act,
            # self.ln,
            Conv2dNorm(128, 128, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            self.act,
            # self.ln,
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dNorm(128, 256, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            self.act,
            # self.ln,
            Conv2dNorm(256, 10, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            # nn.GroupNorm(16, 64, affine=group_affine),
            self.act,
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten(start_dim=1, end_dim=-1),
        )
        self.linear = nn.Sequential(
            LinearNorm(10, num_classes, bias=True,
                                 max_norm_i=max_norm_i, lnorm=lnorm))

    def forward(self, x):
        y = self.conv(x)
        return self.linear(y)
    

class ConvNetFashion(nn.Sequential):
    """
    Same model as in https://arxiv.org/pdf/2007.14191.pdf
    with relu or tanh, group normalization and Conv2dWeightStd as
    in https://arxiv.org/pdf/2204.13650.pdf
    """

    def __init__(self, num_classes=10, group_affine=False,
                 max_norm_i=1, lnorm='inf', activation=nn.Tanh):
        super(ConvNetFashion, self).__init__()
        self.gn = nn.GroupNorm(16, 64, affine=group_affine)
        self.act = activation()
        # self.gn = GroupNormLip(16, 64, affine=group_affine)
        # self.gn = [nn.GroupNorm(16, 64, affine=group_affine),
        # self.ln = LayerNormLip(16, 64, affine=False)
        self.conv = nn.Sequential(
            # self.ln,
            Conv2dNorm(1, 16, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            self.act,
            # self.ln,
            Conv2dNorm(16, 32, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            self.act,
            # self.ln,
            nn.AvgPool2d(kernel_size=2, stride=2),
            Conv2dNorm(32, 64, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            self.act,
            # self.ln,
            Conv2dNorm(64, 64, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            self.act,
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten(start_dim=1, end_dim=-1),
        )
        probe_tensor = torch.zeros((1, 1, 28, 28))
        out_features = self.conv(probe_tensor).view(-1)
        self.linear = nn.Sequential(
        
        LinearNorm(out_features.shape[0], 128, bias=True,
                max_norm_i=max_norm_i, lnorm=lnorm),
        self.act,
        LinearNorm(128, 256, bias=True,
                max_norm_i=max_norm_i, lnorm=lnorm),
        self.act,
        LinearNorm(256, num_classes, bias=True,
                max_norm_i=max_norm_i, lnorm=lnorm)
        )

    def forward(self, x):
        y = self.conv(x)
        return self.linear(y)
    

class ConvNetMnist(nn.Sequential):
    """
    Same model as in https://arxiv.org/pdf/2007.14191.pdf
    with relu or tanh, group normalization and Conv2dWeightStd as
    in https://arxiv.org/pdf/2204.13650.pdf
    """

    def __init__(self, num_classes=10, group_affine=False,
                 max_norm_i=1, lnorm='inf', activation=nn.Tanh):
        super(ConvNetMnist, self).__init__()
        self.gn = nn.GroupNorm(16, 64, affine=group_affine)
        self.act = activation()
        # self.gn = GroupNormLip(16, 64, affine=group_affine)
        # self.gn = [nn.GroupNorm(16, 64, affine=group_affine),
        # self.ln = LayerNormLip(16, 64, affine=False)
        self.conv = nn.Sequential(
            # self.ln,
            Conv2dNorm(1, 32, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            self.act,
            # self.ln,
            Conv2dNorm(32, 64, kernel_size=3, stride=1, padding=1,
                       max_norm_i=max_norm_i, lnorm=lnorm),
            self.gn,
            self.act,
            # self.ln,
            nn.AvgPool2d(kernel_size=2, stride=2),
            nn.Flatten(start_dim=1, end_dim=-1),
        )
        probe_tensor = torch.zeros((1, 1, 28, 28))
        out_features = self.conv(probe_tensor).view(-1)
        self.linear = nn.Sequential(
        LinearNorm(out_features.shape[0], 128, bias=True,
                max_norm_i=max_norm_i, lnorm=lnorm),
        self.act,
        LinearNorm(128, num_classes, bias=True,
                max_norm_i=max_norm_i, lnorm=lnorm)
        )

    def forward(self, x):
        y = self.conv(x)
        return self.linear(y)
    

class ConvNetMnistGrad(nn.Sequential):
    """
    Same model as in https://arxiv.org/pdf/2007.14191.pdf
    with relu or tanh, group normalization and Conv2d
    """

    def __init__(self, num_classes=10, group_affine=False,
                 max_norm_i=1, lnorm='inf', activation=nn.Tanh):
        super(ConvNetMnistGrad, self).__init__()
        self.gn = nn.GroupNorm(16, 64, affine=group_affine)
        self.act = activation()
        self.conv = nn.Sequential(
            # self.ln,
            nn.Conv2d(1, 32, kernel_size=3, stride=1, padding=1),
            self.gn,
            self.act,
            # self.ln,
            nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1),
            self.gn,
            self.act,
            # self.ln,
            nn.AvgPool2d(kernel_size=2, stride=2),
            nn.Flatten(start_dim=1, end_dim=-1),
        )
        probe_tensor = torch.zeros((1, 1, 28, 28))
        out_features = self.conv(probe_tensor).view(-1)
        self.linear = nn.Sequential(
        nn.Linear(out_features.shape[0], 128, bias=True),
        self.act,
        nn.Linear(128, num_classes, bias=True)
        )

    def forward(self, x):
        y = self.conv(x)
        return self.linear(y)
   

class MLPLip(nn.Sequential):
    """
    Dense network for tabular datasets.
    """

    def __init__(self, input_dim, num_classes=2,
                 max_norm_i=1, lnorm='2', activation=nn.Tanh):
        super(MLPLip, self).__init__()
        self.act = activation()
        self.linear = nn.Sequential(
                LinearNorm(input_dim, 128, bias=True,
                    max_norm_i=max_norm_i, lnorm=lnorm),
                self.act,
                LinearNorm(128, num_classes, bias=True,
                    max_norm_i=max_norm_i, lnorm=lnorm)
            )

    def forward(self, x):
        return self.linear(x)