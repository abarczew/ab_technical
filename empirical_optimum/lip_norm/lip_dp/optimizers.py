import torch
from types import MethodType
import torch.nn.functional as F
import torch.nn as nn
from lip_dp.normalizer import normalization_conv, normalization_linear
import re


def extract_block_name(name, regex_search='(layer\.\d)\.'):
    # raise error if no match
    return re.search(regex_search, name).group(1)


class LipDPParameters(object):
    """LipDPParameters is a class to store data and compute deltas."""

    def __init__(self, expected_batch_size, noise_multiplier, max_lip_norm,
                 lnorm, loss_name, loss_lip, model, K=1.,
                 generator=None, secure_mode=False):
        super(LipDPParameters, self).__init__()
        self.expected_batch_size = expected_batch_size
        self.noise_multiplier = noise_multiplier
        self.max_lip_norm = max_lip_norm
        self.lnorm = lnorm
        self.loss_name = loss_name
        self.loss_lip = loss_lip
        self.K = K
        self.generator = generator
        self.secure_mode = secure_mode
        self.accumulated_iterations = 1.  # quick fix to use dp accountant
        self.model_grad_archi = self.get_model_grad_archi(model)
        self.deltas = self.compute_deltas()
        self.grad_scales = None

    def compute_deltas(self):
        """
        Return l2-sensitivity (or function to compute it with parameters)
        of gradient of each layer.
        # TODO: return function to compute with actual parameters rather
        # than max
        # TODO: investigate how it would work with learnable parameters.
        # TODO: assumes activation to be tempered sigmoid, should apply to
        # other 1-lip activation function
        # TODO: take into account the case without bias

        Args:
            model_grad_archi: dict of model composition of learnable layers.
        Returns:
            tensor of float (or function): l2-sensitivity of gradient layers.
        """
        # gradient are computed based on a loss that is already reduced
        # with reduction "mean".
        deltas = []

        # lip_model_input is already scaled by batch_size
        # does it need to be squared??

        lip_model_input = self.loss_lip / (self.expected_batch_size * self.K) \
            if self.K else self.loss_lip / self.expected_batch_size
        backward_model_grad_archi = self.model_grad_archi[::-1]
        in_short_layer_prev = False
        in_short_block_prev = False
        short_block_name = '##'
        for name, size in backward_model_grad_archi:
            ks = size[-1]
            lip_layer_input = self.get_lip_layer_input()
            lip_layer_param = self.get_lip_layer_param(name, ks)
            in_short_layer = 'Shortcut' in name
            if in_short_layer:
                short_block_name = extract_block_name(name)
            in_short_block = short_block_name in name
            if in_short_block:
                if in_short_layer:
                    if not in_short_layer_prev:
                        deltas.append(lip_model_input*lip_layer_param)
                        lip_short_input = lip_model_input * lip_layer_input
                        in_short_layer_prev = True
                    else:
                        deltas.append(lip_short_input*lip_layer_param)
                        lip_short_input *= lip_layer_input
                elif in_short_layer_prev:
                    deltas.append(lip_short_input*lip_layer_param)
                    lip_layerblock_input = lip_model_input * lip_layer_input
                    in_short_layer_prev = False
                else:
                    deltas.append(lip_layerblock_input*lip_layer_param)
                    lip_layerblock_input *= lip_layer_input
                    in_short_block_prev = True
            elif in_short_block_prev:
                deltas.append(lip_layerblock_input*lip_layer_param)
                lip_model_input = (lip_short_input + lip_layerblock_input) * lip_layer_input
                in_short_block_prev = False
            else:
                deltas.append(lip_model_input*lip_layer_param)
                lip_model_input *= lip_layer_input

        return torch.Tensor(deltas)

    def get_lip_layer_input(self):
        """
        Computes the lipschitz value of a trainable layer regarding the input,
        based on lnorm.

        Args:
            name: name of the layer, specifies whether the layer is dense
                    or convolutionnal.
            size: size of the parameter
        Returns:
            float (or function): lip value of the layer regarding the input.
        """
        return self.max_lip_norm

    def get_lip_layer_param(self, name, ks):
        """
        Computes the lipschitz value of a trainable layer regarding the
        parameters, based on lnorm.

        Args:
            name: name of the layer, specifies whether the layer is dense
                    or convolutionnal.
            size: size of the parameter
        Returns:
            Tuple of float (or function): lip.
        """
        if 'bias' in name:
            return 1.
        if 'conv' in name:
            return float(ks)
        if ('linear' in name) or ('fc' in name):
            return 1.

    def get_model_grad_archi(self, model):
        """
        Get metadata of model about layers ie name and size of
        trainable parameters.

        Args:
            model: nn.Sequential representing a pytorch model
        Returns:
            Tensor of tuple of name and kernel size of trainable parameters
            of model.
        """
        model_grad_archi = []
        for name, param in model.named_parameters():
            if param.requires_grad:
                model_grad_archi.append((name, param.size()))
            else:
                continue
        return model_grad_archi

    def to_dict(self):
        return {key: value for key, value in vars(self)}


def _generate_noise(
    std: float,
    reference: torch.Tensor,
    generator=None,
    secure_mode: bool = False,
) -> torch.Tensor:
    """
    Generates noise according to a Gaussian distribution with mean 0

    Args:
        std: Standard deviation of the noise
        reference: The reference Tensor to get the appropriate shape and device
            for generating the noise
        generator: The PyTorch noise generator
        secure_mode: boolean showing if "secure" noise need to be generated
            (see the notes)
    """
    zeros = torch.zeros(reference.shape, device=reference.device)
    if std == 0:
        return zeros
    if secure_mode:
        torch.normal(
            mean=0,
            std=std,
            size=(1, 1),
            device=reference.device,
            generator=generator,
        )  # generate, but throw away first generated Gaussian sample
        sum = zeros
        for _ in range(4):
            sum += torch.normal(
                mean=0,
                std=std,
                size=reference.shape,
                device=reference.device,
                generator=generator,
            )
        return sum / 2
    else:
        return torch.normal(
            mean=0,
            std=std,
            size=reference.shape,
            device=reference.device,
            generator=generator,
        )


def add_gaussian_noise(optimzer, args, kwargs):
    """
    Add Gaussian noise to gradients to enforce dp for SGD.

    Args:
        self: Lip-DP-SGD optimizer.
    Returns:
        None.
    """
    for group in optimzer.param_groups:
        for p, delta in zip(group['params'], optimzer.deltas):
            noise = _generate_noise(
                std=optimzer.noise_multiplier * delta,
                reference=p.grad,
                generator=optimzer.generator,
                secure_mode=optimzer.secure_mode,
            )
            p.grad = (p.grad + noise).view_as(p)


def modified_gaussian_noise(optimzer, args, kwargs):
    """
    Add Gaussian noise to gradients to enforce dp for SGD.
    Modify gradient to add noise scaled on the sensitivity
    and not on the input norm (idea from De et al.)

    Args:
        self: Lip-DP-SGD optimizer.
    Returns:
        None.
    """
    for group in optimzer.param_groups:
        for p, grad_scale_delta in zip(group['params'],
                                       zip(optimzer.grad_scales,
                                           optimzer.deltas)):
            grad_scale, delta = grad_scale_delta
            noise = _generate_noise(
                std=optimzer.noise_multiplier * delta,
                reference=p.grad,
                generator=optimzer.generator,
                secure_mode=optimzer.secure_mode,
            )
            grad_scale = grad_scale.to(p.grad.device)
            # noise scale is already done in the delta compute
            # noise_scale = optimzer.expected_batch_size * (optimzer.K + 1)
            p.grad = (p.grad / grad_scale + noise).view_as(p)


def prepare_optimizer(optimizer, lip_dp_parameters,
                      dp_mechanism=modified_gaussian_noise):
    """
    Convert an optimzer to enforce Lip-DP-SGD.

    Args:
        lip_dp_parameters: parameters of Lip-DP-SGD
        dp_mechanism: dp mechanism to enforce DP.
    Returns:
        tendor: dp gradients.
    """
    for key, value in vars(lip_dp_parameters).items():
        setattr(optimizer, key, value)

    # TODO: if done with learnable norms then attached to pre_step
    # TODO: if done with actual parameter norm (and not max) hook_pre_step
    # setattr(optimizer, 'deltas', deltas)
    optimizer.register_step_pre_hook(dp_mechanism)

    return optimizer


def get_optimizer_hook_fn(self, sample_rate):
    """
    Returns a callback function which can be used to attach to DPOptimizer
    Args:
        sample_rate: Expected sampling rate used for accounting
    """

    def hook_fn(optim, args, kwargs):
        # This works for Poisson for both single-node and distributed
        # The reason is that the sample rate is the same in both cases (but in
        # distributed mode, each node samples among a subset of the data)
        self.step(
            noise_multiplier=optim.noise_multiplier,
            sample_rate=sample_rate,
        )

    return hook_fn


def prepare_accountant(accountant):
    accountant.get_optimizer_hook_fn = MethodType(get_optimizer_hook_fn,
                                                  accountant)
    return accountant


def get_model_hook_fn(module, max_lip_norm, lnorm):
    """
    Returns a callback function which can be used to attach to nn.Module
    Args:
        lnorm: norm to apply on weights
    """
    if isinstance(module, nn.Conv2d):
        def hook_fn(module, input):
            weight = normalization_conv(module.weight, max_lip_norm, lnorm)
            return F.conv2d(input[0], weight, module.bias, module.stride,
                            module.padding, module.dilation, module.groups)
    elif lnorm == 'std' and isinstance(module, nn.Linear):
        def hook_fn(module, input):
            pass
    elif lnorm in ['1', '2', 'inf'] and isinstance(module, nn.Linear):
        def hook_fn(module, input):
            weight = normalization_linear(module.weight, max_lip_norm, lnorm)
            return F.linear(input[0], weight, module.bias)
    else:
        raise ValueError
    return hook_fn


def prepare_model(model, max_lip_norm=1, lnorm='inf'):
    for name, module in model.named_modules():
        if isinstance(module, nn.Linear) or isinstance(module, nn.Conv2d):
            hook_fn = get_model_hook_fn(module, max_lip_norm, lnorm)
            module.register_forward_pre_hook(hook_fn)
        else:
            continue
    return model
