import torch
from torch.nn.functional import normalize

DEFAULT_NITER_BJORCK = 15
DEFAULT_NITER_SPECTRAL = 5
DEFAULT_NITER_SPECTRAL_INIT = 10
DEFAULT_BETA = 0.5


def bjorck_normalization(
    w: torch.Tensor, niter: int = DEFAULT_NITER_BJORCK, beta: float = DEFAULT_BETA
) -> torch.Tensor:
    r"""
    Apply Bjorck normalization on the kernel as per

    .. math::
        \begin{array}{l}
            W_0 = W \\
            W_{n + 1} = (1 + \beta) W_{n} - \beta W_n W_n^T W_n \\
            \overline{W} = W_{N}
        \end{array}

    where :math:`W` is the kernel of shape :math:`(C, *)`, with :math:`C` the number
    of channels.

    Args:
        w: Weights to normalize. For the normalization to work properly, the greatest
            eigen value of ``w`` must be approximately 1.
        niter: Number of iterations.
        beta: Value of :math:`\beta` to use.

    Returns:
        The weights :math:`\overline{W}` after Bjorck normalization.
    """
    if niter == 0:
        return w
    shape = w.shape
    cout = w.size(0)
    w_mat = w.reshape(cout, -1)
    for i in range(niter):
        w_mat = (1.0 + beta) * w_mat - beta * torch.mm(
            w_mat, torch.mm(w_mat.t(), w_mat)
        )
    w = w_mat.reshape(shape)
    return w


def reshape_weight_to_matrix(weight: torch.Tensor) -> torch.Tensor:
    weight_mat = weight
    height = weight_mat.size(0)
    return weight_mat.reshape(height, -1)


def spectral_normalization(weight,
                           max_weight_norm=1.,
                           fix_sigma=False,
                           n_power_iterations=5,
                           do_power_iteration=True,
                           eps=1e-10):
    with torch.no_grad():
        weight_mat = reshape_weight_to_matrix(weight)

        h, w = weight_mat.size()
        # randomly initialize `u` and `v`
        u = torch.vmap(torch.nn.init.normal_,
                       randomness="different")(weight.new_empty(h))
        v = torch.vmap(torch.nn.init.normal_,
                       randomness="different")(weight.new_empty(w))
        u = normalize(u, dim=0, eps=eps)
        v = normalize(v, dim=0, eps=eps)

    if do_power_iteration:
        with torch.no_grad():
            for _ in range(n_power_iterations):
                v = normalize(torch.mv(weight_mat.t(), u), dim=0, eps=eps,
                              out=v)
                u = normalize(torch.mv(weight_mat, v), dim=0, eps=eps, out=u)
            if n_power_iterations > 0:
                # See above on why we need to clone
                u = u.clone(memory_format=torch.contiguous_format)
                v = v.clone(memory_format=torch.contiguous_format)

    sigma = torch.dot(u, torch.mv(weight_mat, v)).item()
    if fix_sigma or sigma > max_weight_norm:
        weight = weight / sigma * max_weight_norm
        sigma = max_weight_norm
    return weight, sigma


def get_spectral_norm(weight,
                      max_weight_norm=1.,
                      fix_sigma=False,
                      n_power_iterations=5,
                      do_power_iteration=True,
                      eps=1e-10):
    with torch.no_grad():
        weight_mat = reshape_weight_to_matrix(weight)

        h, w = weight_mat.size()
        # randomly initialize `u` and `v`
        u = torch.vmap(torch.nn.init.normal_,
                       randomness="different")(weight.new_empty(h))
        v = torch.vmap(torch.nn.init.normal_,
                       randomness="different")(weight.new_empty(w))
        u = normalize(u, dim=0, eps=eps)
        v = normalize(v, dim=0, eps=eps)

    if do_power_iteration:
        with torch.no_grad():
            for _ in range(n_power_iterations):
                v = normalize(torch.mv(weight_mat.t(), u), dim=0, eps=eps,
                              out=v)
                u = normalize(torch.mv(weight_mat, v), dim=0, eps=eps, out=u)
            if n_power_iterations > 0:
                # See above on why we need to clone
                u = u.clone(memory_format=torch.contiguous_format)
                v = v.clone(memory_format=torch.contiguous_format)

    return torch.dot(u, torch.mv(weight_mat, v)).item()


def normalization_inf_conv(weight, max_norm_i):
    """
    Performs normlization with the inf-norm on
    weights of a 2d convolutionnal layer.
    See Gouk et al. 2020 https://arxiv.org/pdf/1804.04368.pdf
    (see code from gouk https://github.com/henrygouk/keras-lipschitz-networks/\
    blob/master/arch/lipschitz.py)
    Note that not all input are scaled as in
    https://arxiv.org/pdf/2202.08345.pdf.
    """
    absoutputsum = torch.max(torch.abs(weight).sum(dim=[1, 2, 3]))
    scale = torch.min(torch.tensor(1.0), max_norm_i/absoutputsum)
    return weight * scale.expand_as(weight)
    # TODO: FIX dim with matrix to vector product view of convolution


def normalization_1_conv(weight, max_norm_i):
    """
    Performs normlization with the 1-norm on
    weights of a 2d convolutionnal layer.
    See Gouk et al. 2020 https://arxiv.org/pdf/1804.04368.pdf
    (see code from gouk https://github.com/henrygouk/keras-lipschitz-networks/\
    blob/master/arch/lipschitz.py)
    Note that not all input are scaled as in
    https://arxiv.org/pdf/2202.08345.pdf.
    """
    absinputsum = torch.max(torch.abs(weight).sum(dim=[0, 2, 3]))
    scale = torch.min(torch.tensor(1.0), max_norm_i/absinputsum)
    return weight * scale.expand_as(weight)


def normalization_2_conv(weight, max_norm_i):
    """
    Performs l2 normalization, with:
    * spectral normlization
    * orthonormalization
    See Bethune et al. 2022 https://arxiv.org/pdf/2104.05097.pdf
    Note that, with this method every eigen value is scaled.

    Also, this method can be enhanced thanks to Scaman or Gouk
    """
    weight, _ = spectral_normalization(weight)
    weight = bjorck_normalization(weight)
    # TODO:  lconv_norm(self) what is it?
    return weight * max_norm_i


def normalization_std_conv(weight):
    """
    Performs weight standardization as in https://arxiv.org/pdf/1903.10520.pdf
    Code directly taken from
    https://github.com/joe-siyuan-qiao/pytorch-classification/blob/
    e6355f829e85ac05a71b8889f4fff77b9ab95d0b/models/layers.py
    """
    weight_mean = weight.mean(dim=1, keepdim=True)\
                        .mean(dim=2, keepdim=True)\
                        .mean(dim=3, keepdim=True)
    weight = weight - weight_mean
    std = weight.view(weight.size(0), -1)\
                .std(dim=1)\
                .view(-1, 1, 1, 1)\
        + 1e-5
    weight = weight / std.expand_as(weight)
    return weight


def normalization_linear(weight, max_norm_i, lnorm='inf'):
    weight_norm = torch.linalg.matrix_norm(weight, ord=float(lnorm))
    scale = torch.min(torch.tensor(1.0), max_norm_i/weight_norm)
    return weight * scale.expand_as(weight)


def normalization_conv(weight, max_norm_i=1, lnorm='inf'):
    if lnorm == 'inf':
        return normalization_inf_conv(weight, max_norm_i)
    elif lnorm == '1':
        return normalization_1_conv(weight, max_norm_i)
    elif lnorm == '2':
        return normalization_2_conv(weight, max_norm_i)
    elif lnorm == 'std':
        return normalization_std_conv(weight)
