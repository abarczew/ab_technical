# Experiments

This repository contains the Python code used to produce the experimental results
on CIFAR10

## Setup

```
# To create a virtual environment
python -m venv myenv

# To activate the virtual environment
source myenv/bin/activate

# Set TMPDIR to a partition with a lot of space, e.g.,
export TMPDIR=${HOME}/tmp

# Install requirements
pip install -r requirements.txt
```

## Data

This folder is focused on CIFAR10 only for now. The downloading of the dataset is taken care of by the script.

## Usage

To run DP-SGD with state of the art performances:
```
python main_dpsgd.py --device [cpu/gpu]
```
To run Lip-DP-SGD with same model as SOTA DP-SGD:
```
python main_lipdpsgd.py --device [cpu/gpu]
```

For now, one has to change main_lipdpsgd.py to tune the privacy parameters.
