import torch
import json
import os
import argparse

from ax.service.ax_client import AxClient, ObjectiveProperties


exp_names = ['mnist', 'fashionmnist', 'cifar',
             'income', 'android', 'breast', 'default_credit',
             'dropout', 'german', 'nursery', 'thyroid', 'yeast']

parser = argparse.ArgumentParser()
parser.add_argument('--exp-name', '-e', dest='exp_name', default='mnist',
                    choices=exp_names,
                    help='experiment name: ' + ' | '.join(exp_names) +
                    ' (default: mnist)')
parser.add_argument('--runs', '-r', default=50, type=int,
                    help='number of trials to run')
parser.add_argument('--save-dir', '-s', dest='save_dir',
                    help='The directory used to save the trained models',
                    default='save_temp', type=str)


def parse_parameters(parameters):
    
    return parameters_string


def run():
    args = parser.parse_args()
    path = f"search_tab_exp/{args.exp_name}/"
    path_results = f"{path}{args.save_dir}/"

    if not os.path.exists(path_results):
        os.makedirs(path_results)
    
    ax_client = AxClient()

    with open(path+'parameters.json', 'r') as f:
        parameters = json.load(f)

    ax_client.create_experiment(
        name=path.replace('/', '_'),  # The name of the experiment.
        parameters=parameters,
        # The objective name and minimization setting.
        objectives={'acc': ObjectiveProperties(minimize=False),
                    # "epsilon": ObjectiveProperties(minimize=True)
                    },
    )

    def train_evaluate(parameters):
        parameters_string = parse_parameters(parameters)
        result = os.system(f'main_lipdpsgd.py {parameters_string}')
        return result

    for i in range(args.runs):
        parameters, trial_index = ax_client.get_next_trial()
        ax_client.complete_trial(trial_index=trial_index,
                                 raw_data=train_evaluate(parameters))

    # write results
    ax_client.save_to_json_file(filepath=path_results+"results.json")


if __name__ == '__main__':
    run()
