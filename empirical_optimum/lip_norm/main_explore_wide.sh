python main_nodp_widemult.py --debug true --max_per_sample_grad_norm 1e9 --lr 0.1 --dump_path explore/nodp/noclipbestlr_norm_analysis

python main_nodp_widemult.py --debug true --max_per_sample_grad_norm 1e9 --dump_path explore/nodp/noclip_norm_analysis
python main_nodp_widemult.py --debug true --max_per_sample_grad_norm 1.0 --dump_path explore/nodp/clip_norm_analysis
python main_nodp_widemult.py --debug true --max_per_sample_grad_norm 1.0 --lr 0.1 --dump_path explore/nodp/clipbestlr_norm_analysis

python main_dpsgd_widemult.py --debug true --dump_path explore/dpsgd/clip_norm_analysis

python main_lipdpsgd_widemult.py --debug true --dump_path explore/lipdpsgd/plain_norm_analysis
python main_lipdpsgd_widemult.py --debug true --lr 20 --dump_path explore/lipdpsgd/plainbestlr_norm_analysis
python main_lipdpsgd_widemult.py --debug true --lr 20 --ref_noise 0 --dump_path explore/lipdpsgd/nodp_norm_analysis
python main_lipdpsgd_widemult.py --debug true --lr 20 --activation tanh --dump_path explore/lipdpsgd/tanh_norm_analysis
python main_lipdpsgd_widemult.py --debug true --lr 20 --max_lip_norm 10 --activation tanh --dump_path explore/lipdpsgd/bigliptanh_norm_analysis

python main_lipdpsgd_widemult.py --debug true --lr 20 --ref_noise 0 --activation tanh --dump_path explore/lipdpsgd/nodptanh_norm_analysis
python main_lipdpsgd_widemult.py --debug true --lr 0.1 --ref_noise 0 --dump_path explore/lipdpsgd/nodpsmalllr_norm_analysis
