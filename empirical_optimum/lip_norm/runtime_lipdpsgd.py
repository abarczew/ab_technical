#!/usr/bin/env python3
# Copyright (c) Meta Platforms, Inc. and affiliates.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import json
import logging
import os
import shutil
import sys
from datetime import datetime, timedelta

import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data
import torch.utils.data.distributed
import torchvision.transforms as transforms
from opacus import PrivacyEngine
from opacus.distributed import DifferentiallyPrivateDistributedDataParallel as DPDDP
from opacus.grad_sample.functorch import make_functional
from opacus.data_loader import DPDataLoader
from opacus.accountants import RDPAccountant
from torch.func import grad_and_value, vmap
from torch.nn.parallel import DistributedDataParallel as DDP
from torchvision.datasets import CIFAR10
from torchvision.datasets import FashionMNIST
from torchvision.datasets import MNIST
from tqdm import tqdm

from model import ConvNetGNWeightNorm, ConvNetSotaLipTanh, Conv2dNorm,\
    ConvNetMnist, ConvNetFashion, ConvNetCifar, LinearNorm, MLPLip
from lip_dp.optimizers import LipDPParameters, prepare_optimizer,\
    prepare_accountant, prepare_model
from src.models.EMA_without_class import create_ema, update
from src.utils.utils import get_noise_from_bs, get_epochs_from_bs, bool_flag
from utils import load_tabular_data


for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)

logging.basicConfig(
    format="%(asctime)s:%(levelname)s:%(message)s",
    datefmt="%m/%d/%Y %H:%M:%S",
    # stream=sys.stdout,
    filename='logs/mnist/runtime.log',
    filemode='a',
)
logger = logging.getLogger()
logger.setLevel(level=logging.DEBUG)

data_load_dict = {'mnist': (MNIST, [(0.1307,), (0.3081,)]), 
                  'fashionmnist': (FashionMNIST, [(0.286,), (0.353,)]), 
                  'cifar10': (CIFAR10, [(0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)])
                  }

model_load_dict = {'mnist': ConvNetMnist, 'fashionmnist': ConvNetMnist,
                   'cifar10': ConvNetCifar}

activation_dict = {'tanh': nn.Tanh, 'relu': nn.ReLU}

vision_datasets = ['mnist', 'fashionmnist', 'cifar10']


def setup(args):
    if not torch.cuda.is_available():
        raise NotImplementedError(
            "DistributedDataParallel device_ids and output_device arguments \
            only work with single-device GPU modules"
        )

    if sys.platform == "win32":
        raise NotImplementedError("Windows version of multi-GPU is not supported yet.")

    # Initialize the process group on a Slurm cluster
    if os.environ.get("SLURM_NTASKS") is not None:
        rank = int(os.environ.get("SLURM_PROCID"))
        local_rank = int(os.environ.get("SLURM_LOCALID"))
        world_size = int(os.environ.get("SLURM_NTASKS"))
        os.environ["MASTER_ADDR"] = "127.0.0.1"
        os.environ["MASTER_PORT"] = "7440"

        torch.distributed.init_process_group(
            args.dist_backend, rank=rank, world_size=world_size
        )

        logger.debug(
            f"Setup on Slurm: rank={rank}, local_rank={local_rank}, world_size={world_size}"
        )

        return (rank, local_rank, world_size)

    # Initialize the process group through the environment variables
    elif args.local_rank >= 0:
        torch.distributed.init_process_group(
            init_method="env://",
            backend=args.dist_backend,
        )
        rank = torch.distributed.get_rank()
        local_rank = args.local_rank
        world_size = torch.distributed.get_world_size()

        logger.debug(
            f"Setup with 'env://': rank={rank}, local_rank={local_rank}, world_size={world_size}"
        )

        return (rank, local_rank, world_size)

    else:
        logger.debug(f"Running on a single GPU.")
        return (0, 0, 1)


def cleanup():
    torch.distributed.destroy_process_group()


def save_checkpoint(state, is_best, filename="checkpoint.tar"):
    torch.save(state, filename)
    if is_best:
        shutil.copyfile(filename, "model_best.pth.tar")


def accuracy(preds, labels):
    return (preds == labels).mean()


def train(args, model, ema, train_loader, optimizer, accountant, epoch, device, nb_steps):
    start_time = datetime.now()

    model.train()
    max_input_norm = {}
    def save_input_norm(name):
        def hook(model, input, output):
            x = input[0]
            batch_max_input_norm = float(torch.linalg.vector_norm(x, dim=[i for i in range(len(x.size())) if i > 0]).max().detach().data)
            if name not in max_input_norm.keys():
                max_input_norm[name] = batch_max_input_norm
            elif batch_max_input_norm > max_input_norm[name]:
                max_input_norm[name] = batch_max_input_norm
        return hook

    def register_hook(model):
        if hasattr(model, 'conv'):
            for i in range(len(model.conv)):
                if isinstance(model.conv[i], Conv2dNorm):
                    model.conv[i].register_forward_hook(save_input_norm(f'conv.{i}'))
        if hasattr(model, 'linear'):
            for i in range(len(model.linear)):
                if isinstance(model.linear[i], LinearNorm):
                    model.linear.register_forward_hook(save_input_norm(f'linear.{i+1}'))

    register_hook(model)

    criterion = nn.CrossEntropyLoss()

    losses = []
    top1_acc = []

    optimizer.zero_grad()
    for i, (images, target) in enumerate(tqdm(train_loader)):
        images = images.to(device)
        target = target.to(device)

        K = args.transform
        l = len(images)
        # Using Augmentation multiplicity
        if args.transform:
            K = args.transform + 1
            images_duplicates = torch.repeat_interleave(images, repeats=K, dim=0)
            target = torch.repeat_interleave(target, repeats=K, dim=0)
            transform = transforms.Compose([transforms.RandomCrop(
                size=(32, 32), padding=4, padding_mode="reflect"), transforms.RandomHorizontalFlip(p=0.5), ])
            images = transforms.Lambda(lambda x: torch.stack(
                [transform(x_) for x_ in x]))(images_duplicates)
            assert len(images) == K * l

        # compute output
        output = model(images)

        loss = criterion(output, target) / (args.transform+1) * args.loss_lip
        preds = np.argmax(output.detach().cpu().numpy(), axis=1)
        labels = target.detach().cpu().numpy()

        # measure accuracy and record loss
        acc1 = accuracy(preds, labels)
        top1_acc.append(acc1)

        # compute gradient and do SGD step
        loss.backward()

        losses.append(loss.item())

        if (i+1) % (args.transform+1) == 0:
            max_input_norm_list = [v for _, v in max_input_norm.items()]
            layer_nb = len(max_input_norm_list)
            # if model has bias
            for i in range(layer_nb):
                max_input_norm_list.insert(2*i+1, 1.)
            optimizer.grad_scales = torch.Tensor(max_input_norm_list[::-1])
            optimizer.step()
            optimizer.zero_grad()

            nb_steps += 1
            if ema:
                update(model, ema, nb_steps)

            # reset max_input norm recording
            max_input_norm = {}

            if nb_steps % args.print_freq == 0:
                if not args.disable_dp:
                    epsilon = accountant.get_epsilon(delta=args.delta)
                    print(
                        f"\tTrain Epoch: {epoch} \t"
                        f"Loss: {np.mean(losses):.6f} "
                        f"Acc@1: {np.mean(top1_acc):.6f} "
                        f"(ε = {epsilon:.2f}, δ = {args.delta})"
                    )
                else:
                    print(
                        f"\tTrain Epoch: {epoch} \t"
                        f"Loss: {np.mean(losses):.6f} "
                        f"Acc@1: {np.mean(top1_acc):.6f} "
                    )
    train_duration = datetime.now() - start_time
    return train_duration, nb_steps


def test(args, model, test_loader, device):
    model.eval()
    criterion = nn.CrossEntropyLoss()
    losses = []
    top1_acc = []

    with torch.no_grad():
        for images, target in tqdm(test_loader):
            images = images.to(device)
            target = target.to(device)

            output = model(images)
            loss = criterion(output, target)
            preds = np.argmax(output.detach().cpu().numpy(), axis=1)
            labels = target.detach().cpu().numpy()
            acc1 = accuracy(preds, labels)

            losses.append(loss.item())
            top1_acc.append(acc1)

    top1_avg = np.mean(top1_acc)

    print(f"\tTest set:" f"Loss: {np.mean(losses):.6f} " f"Acc@1: {top1_avg :.6f} ")

    return np.mean(top1_acc)


# flake8: noqa: C901
def main(batch_size):
    args = parse_args()

    if args.debug >= 1:
        logger.setLevel(level=logging.DEBUG)

    # Sets `world_size = 1` if you run on a single GPU with `args.local_rank = -1`
    if args.local_rank != -1 or args.device != "cpu":
        rank, local_rank, world_size = setup(args)
        device = local_rank
    else:
        device = "cpu"
        rank = 0
        world_size = 1

    if args.secure_rng:
        try:
            import torchcsprng as prng
        except ImportError as e:
            msg = (
                "To use secure RNG, you must install the torchcsprng package! "
                "Check out the instructions here: https://github.com/pytorch/csprng#installation"
            )
            raise ImportError(msg) from e

        generator = prng.create_random_device_generator("/dev/urandom")

    else:
        generator = None

    if args.data_name in vision_datasets:
        load_data, normalize_args = data_load_dict[args.data_name]
        augmentations = [
            transforms.RandomCrop(size=(32, 32), padding=4, padding_mode="reflect"),
            transforms.RandomHorizontalFlip(p=0.5),
        ]
        normalize = [
            transforms.ToTensor(),
            transforms.Normalize(*normalize_args),
        ]
        train_transform = transforms.Compose(
            augmentations + normalize if args.disable_dp else normalize
        )

        test_transform = transforms.Compose(normalize)

        train_dataset = load_data(
            root=args.data_root, train=True, download=True, transform=train_transform
        )

        test_dataset = load_data(
            root=args.data_root, train=False, download=True, transform=test_transform
        )
    else:
        train_dataset, test_dataset, input_dim, num_class = load_tabular_data(args.data_name)

    physical_batch_size = batch_size // (args.transform+1)
    train_loader = torch.utils.data.DataLoader(
        train_dataset,
        batch_size=physical_batch_size,
        generator=generator,
        num_workers=args.workers,
        pin_memory=True,
    )
    test_loader = torch.utils.data.DataLoader(
        test_dataset,
        batch_size=args.batch_size_test,
        shuffle=False,
        num_workers=args.workers,
    )

    best_acc1 = 0

    activation = activation_dict[args.activation]
    if args.data_name in vision_datasets:
        model_load = model_load_dict[args.data_name]
        model = model_load(num_classes=10, lnorm='2', activation=activation)
    else:
        model = MLPLip(input_dim=input_dim, num_classes=num_class, lnorm='2', activation=activation)
    model = model.to(device)

    # Use the right distributed module wrapper if distributed training is enabled
    if world_size > 1:
        if not args.disable_dp:
            if args.clip_per_layer:
                model = DDP(model, device_ids=[device])
            else:
                model = DPDDP(model)
        else:
            model = DDP(model, device_ids=[device])

    privacy_engine = None
    if args.clip_per_layer:
        # Each layer has the same clipping threshold. The total grad norm is still bounded by `args.max_per_sample_grad_norm`.
        n_layers = len(
            [(n, p) for n, p in model.named_parameters() if p.requires_grad]
        )
        max_grad_norm = [
            args.max_per_sample_grad_norm / np.sqrt(n_layers)
        ] * n_layers
    else:
        max_grad_norm = args.max_per_sample_grad_norm
    # TODO: implement per_layer_norm
    # TODO: implement learnable layer_norm
    max_lip_norm = args.max_lip_norm
    sample_rate = (args.transform + 1) / len(train_loader)
    expected_batch_size = int(len(train_loader.dataset) * sample_rate)
    # TODO: use LipDPParameters in prepare_model
    # model = prepare_model(model, max_lip_norm, 'inf')
    # TODO: should add
    # sigma = get_noise_from_bs(args.batch_size, args.ref_noise, args.ref_B) ?
    lip_dp_parameters = LipDPParameters(expected_batch_size, args.sigma,
                                        max_lip_norm, '2',
                                        'CrossEntropyLoss', args.loss_lip, model,
                                        args.transform)
    # TODO: how to replicate the secure mode without privacy_engine?
    # privacy_engine = PrivacyEngine(
    #     secure_mode=args.secure_rng,
    # )
    if args.optim == "SGD":
        optimizer = optim.SGD(
            model.parameters(),
            lr=args.lr,
            momentum=args.momentum,
            weight_decay=args.weight_decay,
            nesterov=args.nesterov,
        )
    elif args.optim == 'SGD_tunedlr':
        forward_grad_scales = lip_dp_parameters.grad_scales.tolist()[::-1]
        optimizer = optim.SGD(
            [{'params': param, 'lr': args.lr * grad_scale}
             for (name, param), grad_scale
             in zip(model.named_parameters(), forward_grad_scales)]
        )
    elif args.optim == "RMSprop":
        optimizer = optim.RMSprop(model.parameters(), lr=args.lr)
    elif args.optim == "Adam":
        optimizer = optim.Adam(model.parameters(), lr=args.lr)
    else:
        raise NotImplementedError("Optimizer not recognized. Please check spelling")

    clipping = "per_layer" if args.clip_per_layer else "flat"
    train_loader = DPDataLoader.from_data_loader(train_loader)

    optimizer = prepare_optimizer(optimizer, lip_dp_parameters)
    accountant = RDPAccountant()
    accountant = prepare_accountant(accountant)
    # note that accountant takes only optim.noise_multiplier
    #  as noise_multiplier
    optimizer.register_step_pre_hook(
        accountant.get_optimizer_hook_fn(sample_rate=sample_rate)
    )

    # Store some logs
    accuracy_per_epoch = []
    time_per_epoch = []
    nb_steps = 0
    ema = None
    # we create a shadow model
    print("shadowing de model with EMA")
    ema = create_ema(model)
    epochs = 5

    if args.resume != "":
        checkpoint = torch.load(args.resume)
        model.load_state_dict(checkpoint['state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        epoch = checkpoint['epoch']
        print(epoch)

    for epoch in range(epochs):
        if args.lr_schedule == "cos":
            lr = args.lr * 0.5 * (1 + np.cos(np.pi * epoch / (epochs + 1)))
            for param_group in optimizer.param_groups:
                param_group["lr"] = lr

        train_duration, nb_steps = train(
            args, model, ema, train_loader, optimizer, accountant, epoch, device, nb_steps
        )
        top1_acc = test(args, model, test_loader, device)

        top1_acc_ema = test(args, ema, test_loader, device)

        # remember best acc@1 and save checkpoint
        is_best = top1_acc > best_acc1
        best_acc1 = max(top1_acc, best_acc1)

        time_per_epoch.append(train_duration)
        accuracy_per_epoch.append(float(top1_acc))

    if rank == 0:
        time_per_epoch_seconds = [t.total_seconds() for t in time_per_epoch]
        avg_time_per_epoch = sum(time_per_epoch_seconds) / len(time_per_epoch_seconds)
        metrics = {
            "batch_size": batch_size,
            "time_per_epoch": time_per_epoch_seconds,
        }

        logger.info(
            "\nNote:\n- 'total_time' includes the data loading time, training time and testing time.\n- 'time_per_epoch' measures the training time only.\n"
        )
        logger.info(metrics)

    if world_size > 1:
        cleanup()

    return best_acc1


def parse_args():
    parser = argparse.ArgumentParser(description="PyTorch DP Training")
    parser.add_argument("--data_name", type=str, default="cifar10")
    parser.add_argument("--grad_sample_mode", type=str, default="hooks")
    parser.add_argument(
        "-j",
        "--workers",
        default=2,
        type=int,
        metavar="N",
        help="number of data loading workers (default: 2)",
    )
    parser.add_argument("--ref_nb_steps",
                        default=2500,
                        type=int,
                        help="reference number of steps used with reference noise and batch size to create our physical constant",)
    parser.add_argument(
        "-b",
        "--batch-size-test",
        default=256,
        type=int,
        metavar="N",
        help="mini-batch size for test dataset (default: 256), this is the total "
        "batch size of all GPUs on the current node when "
        "using Data Parallel or Distributed Data Parallel",
    )
    parser.add_argument(
        "--batch-size",
        default=2000,
        type=int,
        metavar="N",
        help="approximate bacth size",
    )
    parser.add_argument("--transform", type=int, default=0,
                        help="using augmentation multiplicity",)
    parser.add_argument(
        "--lr",
        "--learning-rate",
        default=0.1,
        type=float,
        metavar="LR",
        help="initial learning rate",
        dest="lr",
    )
    parser.add_argument(
        "--momentum", default=0.9, type=float, metavar="M", help="SGD momentum"
    )
    parser.add_argument(
        "--nesterov", default=False, type=bool_flag, help="SGD nesterov"
    )
    parser.add_argument(
        "--wd",
        "--weight-decay",
        default=0,
        type=float,
        metavar="W",
        help="SGD weight decay",
        dest="weight_decay",
    )
    parser.add_argument(
        "-p",
        "--print-freq",
        default=10,
        type=int,
        metavar="N",
        help="print frequency (default: 10)",
    )
    parser.add_argument(
        "--resume",
        default="",
        type=str,
        metavar="PATH",
        help="path to latest checkpoint (default: none)",
    )
    parser.add_argument(
        "-e",
        "--evaluate",
        dest="evaluate",
        action="store_true",
        help="evaluate model on validation set",
    )
    parser.add_argument(
        "--seed", default=None, type=int, help="seed for initializing training. "
    )

    parser.add_argument(
        "--sigma",
        type=float,
        default=1.5,
        metavar="S",
        help="Noise multiplier (default 1.0)",
    )
    parser.add_argument(
        "--loss_lip", default=0.8, type=float, help="lipschitz value of loss"
    )
    parser.add_argument("--activation", default='relu', type=str,
                        help="name of activation function (relu or tanh)",)
    parser.add_argument(
        "-c",
        "--max-per-sample-grad_norm",
        type=float,
        default=1.0,
        metavar="C",
        help="Clip per-sample gradients to this norm (default 1.0)",
    )
    parser.add_argument(
        "--max_lip_norm",
        type=float,
        default=1.0,
        help="Clip per-layer weights to this norm (default 1.0)",
    )
    parser.add_argument(
        "--disable-dp",
        action="store_true",
        default=False,
        help="Disable privacy training and just train with vanilla SGD",
    )
    parser.add_argument(
        "--secure-rng",
        action="store_true",
        default=False,
        help="Enable Secure RNG to have trustworthy privacy guarantees."
        "Comes at a performance cost. Opacus will emit a warning if secure rng is off,"
        "indicating that for production use it's recommender to turn it on.",
    )
    parser.add_argument(
        "--delta",
        type=float,
        default=1e-5,
        metavar="D",
        help="Target delta (default: 1e-5)",
    )

    parser.add_argument(
        "--checkpoint-file",
        type=str,
        default="checkpoint",
        help="path to save check points",
    )
    parser.add_argument(
        "--data-root",
        type=str,
        default="../cifar10",
        help="Where CIFAR10 is/will be stored",
    )
    parser.add_argument(
        "--log-dir",
        type=str,
        default="/tmp/stat/tensorboard",
        help="Where Tensorboard log will be stored",
    )
    parser.add_argument(
        "--optim",
        type=str,
        default="SGD",
        help="Optimizer to use (Adam, RMSprop, SGD)",
    )
    parser.add_argument(
        "--lr-schedule", type=str, choices=["constant", "cos"], default="constant"
    )

    parser.add_argument(
        "--device", type=str, default="gpu", help="Device on which to run the code."
    )
    parser.add_argument(
        "--local_rank",
        type=int,
        default=-1,
        help="Local rank if multi-GPU training, -1 for single GPU training. Will be overriden by the environment variables if running on a Slurm cluster.",
    )

    parser.add_argument(
        "--dist_backend",
        type=str,
        default="gloo",
        help="Choose the backend for torch distributed from: gloo, nccl, mpi",
    )

    parser.add_argument(
        "--clip_per_layer",
        action="store_true",
        default=False,
        help="Use static per-layer clipping with the same clipping threshold for each layer. Necessary for DDP. If `False` (default), uses flat clipping.",
    )
    parser.add_argument(
        "--debug",
        type=int,
        default=0,
        help="debug level (default: 0)",
    )

    return parser.parse_args()


if __name__ == "__main__":
    batch_sizes = [int(i) for i in np.linspace(1, 20000, 20)][1:]
    for batch_size in batch_sizes:
        main(batch_size)
