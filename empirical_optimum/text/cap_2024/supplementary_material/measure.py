import torch
import os
import argparse
import pandas as pd
import json

from opacus import GradSampleModule
from opacus.data_loader import DPDataLoader

from lip_dp.train import train, evaluate_acc_auc, evaluate_ece

from utils import load_vision_data, load_tabular_data, load_best_params
from model import load_vision_model, load_tabular_model

exp_names = ['mnist', 'fashionmnist', 'cifar',
             'income', 'android', 'breast', 'default_credit',
             'dropout', 'german', 'nursery']

parser = argparse.ArgumentParser()
parser.add_argument('--exp-name', '-e', dest='exp_name', default='mnist',
                    choices=exp_names,
                    help='experiment name: ' + ' | '.join(exp_names) +
                    ' (default: mnist)')
parser.add_argument('-j', '--workers', default=0, type=int, metavar='N',
                    help='number of data loading workers (default: 0)')
# TODO: check how it's used
parser.add_argument('--runs', '-r', default=10, type=int,
                    help='number of trials to run')
parser.add_argument('--save-dir', '-s', dest='save_dir',
                    help='The directory used to save the trained models',
                    default='save_temp', type=str)
parser.add_argument('--imbal', '-i', default=False, type=bool,
                    help='boolean to load imbalanced dataset, \
                    only available for mnist')
parser.add_argument('--fix', '-f', default=False, type=bool,
                    help='boolean to init optimizer with fixed sigma')
parser.add_argument('--ema', action='store_true',
                    help='boolean to set train with parameters averaging')
parser.set_defaults(ema=False)
parser.add_argument('--group', action='store_true')
parser.add_argument('--no-group', dest='group', action='store_false')
parser.set_defaults(group=True)
parser.add_argument('--grad', '-g', default=False, type=bool,
                    help='boolean to print grad')
parser.add_argument('--params', '-p',
                    help='The filname of the best params if any',
                    default='empty.json', type=str)


def run(method):
    global args
    args = parser.parse_args()
    path = f"exp/{args.exp_name}/{method}/"
    if args.group:
        path_results = f"{path}{args.save_dir}_group/"
    else:
        path_results = f"{path}{args.save_dir}_nogroup/"

    if not os.path.exists(path_results):
        os.makedirs(path_results)

    torch.manual_seed(42)
    dtype = torch.float
    os.environ["PYTORCH_CUDA_ALLOC_CONF"] = "max_split_size_mb:512"

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # load best params
    if args.params == 'empty.json':
        parameters = load_best_params(method,
                                      exp_name=args.exp_name,
                                      save_dir=args.save_dir)
    else:
        with open(path+args.params, 'r') as f:
            parameters = json.load(f)
    parameters['method'] = method
    parameters['fix_sigma'] = args.fix
    parameters['ema'] = args.ema
    parameters['print_grad'] = args.grad
    parameters['exp_name'] = args.exp_name
    if not args.group:
        num_group = 0
        alpha = 0
    else:
        num_groups = parameters.get("num_groups")
        alpha = parameters.get("alpha")

    if args.exp_name in ['mnist', 'fashionmnist', 'cifar']:
        train_valid_set, test_set = load_vision_data(args.exp_name)
        class_num = 10
    else:
        train_valid_set, test_set, input_dim, class_num = load_tabular_data(args.exp_name)

    def train_evaluate(parameters):
        """
        Train the model and then compute an evaluation metric.

        """
        if args.exp_name in ['mnist', 'fashionmnist', 'cifar']:
            net = load_vision_model(args.exp_name, num_groups, alpha)
        else:
            net = load_tabular_model(args.exp_name,
                                     input_dim,
                                     class_num,
                                     args.group)

        parameters["operators_lip"] = net.operators_lip
        net = GradSampleModule(net, batch_first=True)

        # Initialize dataloaders
        train_loader = torch.utils.data.DataLoader(
            train_valid_set,
            batch_size=parameters.get("batch_size"),
            shuffle=True,
            pin_memory=(args.exp_name == 'cifar')
        )
        test_loader = torch.utils.data.DataLoader(
            test_set,
            batch_size=parameters.get("batch_size"),
            shuffle=True,
            pin_memory=(args.exp_name == 'cifar')
        )

        train_loader = DPDataLoader.from_data_loader(train_loader,
                                                     distributed=False)

        net, accountant, ema = train(
            net=net,
            train_loader=train_loader,
            parameters=parameters,
            dtype=dtype,
            device=device,
        )
        delta = parameters.get("delta", 1/10000)  # todo: apply actual values
        epsilon = accountant.get_epsilon(delta)
        noise_multiplier = parameters.get("noise_multiplier")

        ece = evaluate_ece(
            net=net,
            data_loader=test_loader,
            dtype=dtype,
            device=device)
        performances = evaluate_acc_auc(
            net=net,
            data_loader=test_loader,
            class_num=class_num,
            # ema=ema,
            dtype=dtype,
            device=device)

        return {'noise_multiplier': noise_multiplier,
                'epsilon': epsilon,
                'delta': delta,
                'ece': ece,
                'accuracy': performances['accuracy'],
                'auc': performances['auc']}

    noise_multiplier_range = [0.45, 0.56, 0.7, 0.9, 1.5, 3.5, 5.3, 5.7]
    results = []
    for _ in range(args.runs):
        for i, noise_multiplier in enumerate(noise_multiplier_range):
            # Attach the trial
            parameters_trial = {"noise_multiplier": noise_multiplier,
                                **parameters}
            # Get the parameters and run the trial
            result = train_evaluate(parameters_trial)
            results.append(result)
            print(result)
    # write results
    print('writing results')
    df = pd.DataFrame.from_records(results)
    df.to_csv(path_results+"trials.csv")


if __name__ == '__main__':
    for method in ['grad_clip', 'weight_norm']:
        run(method)
