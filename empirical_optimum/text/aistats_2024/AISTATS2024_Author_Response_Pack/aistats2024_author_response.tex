\documentclass{article}

\usepackage{aistats2024_author_response}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{xcolor}         % define colors in text
\usepackage{xspace}         % fix spacing around commands
\usepackage{enumitem}
\usepackage{wrapfig}

\begin{document}


\textbf{Author response}\\
\begin{wrapfigure}{r}{7cm}
\begin{tabular}{lll}
  ECE ($\epsilon=1.4, \delta=10^{-5}$)  & CIFAR-10 & MNIST \\
  DP-SGD                                 & $0.41$   & $0.10$ \\
  Lip-DP-SGD                             & $0.03$   & $0.04$\\
  \end{tabular}
\end{wrapfigure}
\textbf{Empirical improvements (R2, R4).} Some reviewers are concerned that not all our experiments show improved performances (R2,R4).  Of course, there is no free lunch and one can't expect to gain performance in all cases.  Nevertheless, our paper shows on most tabular datasets (in Table 1) an improvement of several percent (more than the 1\% mentioned by R2).  It is true that for some image datasets performance improvements are smaller (but still positive).  Still, for these datasets one must not underestimate the bias reduction, which has been pointed out by several other papers already.  Maybe reviewers underestimate this bias reduction, to make the improvement clearer we provide here some numerical results (and could add more in appendix).  It is hard to measure `bias' directly, but expected calibration error (ECE) \url{https://arxiv.org/pdf/1706.04599.pdf} is a good approximation which will detect if predictions are systematically too high/low in certain regions.
%\cite{Naeini, Mahdi Pakdaman, Cooper, Gregory F, and Hauskrecht, Milos. Obtaining well calibrated probabilities using bayesian binning. In AAAI, pp. 2901, 2015.}
% which takes a weighted average of accuracy/confidence difference of the predictions grouped in bins.
The table on the right shows that Lip-DP-SGD is up to 10 times better calibrated than DP-SGD, which supports the fact that Lip-DP-SGD induces less bias.


\textbf{Results of DP-SGD (R2)}
%We validate the accuracy of DP-SGD results as correct and fair.
Unlike what R2 suggests, our DP-SGD results on MNIST are consistent with literature benchmarks, e.g., our DP-SGD implementation has accuracy  $95.16$\% at $\epsilon=1.59$, the $94.63$\% at smaller $\epsilon=1.16$ obtained by Opacus is comparable, despite our use of a more light-weight model.
%e.g., demonstrating an accuracy of $95.16$\% at $\epsilon=1.59$, outperforming the reported $94.63$\% at $\epsilon=1.16$, even though we employed a relatively lightweight model, featuring only three layers with a single convolutional layer.


\textbf{Related works (R1,R4,R10)} We appreciate the reviewers for highlighting related work. R1-[1] illustrates how various techniques such as regularization and choosing a good batch size form alternative ways to improve the performance of DP-SGD.  Still, R1-[1] is not a peer-reviewed paper, it only applies to image datasets (while our method is general), it does not solve the problem of bias (which other papers have already pointed out and we argue above is not negligible), and it is likely that most of the ideas presented in R1-[1] can be combined with our improvements.
%the interaction of regularization techniques can bridge the gap between DP and non-DP SGD, offering potential benefits to Lip-DP-SGD. Indeed, Lip-DP-SGD introduces a fundamental change to the DP-SGD framework, but still it can benefit from the studies initially conducted on DP-SGD.
R4 points to [Bethune et al. 2023], also not a peer-reviewed paper, which suggests a number of ideas in the direction of our Fix-Lip-DP-SGD variant, but has only limited empirical evaluation.  Our paper shows that Lip-DP-SGD outperforms Fix-Lip-DP-SGD (Table 1), and contributes a systematic empirical evaluation showing the idea of bounding weight norms is especially beneficial in non-image datasets.
%it seems closer to the "fixed" version of Lip-DP-SGD (Fix-Lip-DP-SGD), for which we provide results on tabular datasets. Despite [Bethune et al. 2023] not demonstrating significant improvements, our work showcases enhancements on tabular datasets.
R10-[1] indeed shows a prior use of weight clipping, we don't claim to invent weight clipping, our contribution is in the integration in supervised learning.  R10-[1] is on (unsupervised) GAN, where different performance metrics are important and avoiding bias is not the primary motivation of its use.
%Lastly, [Xie et al. 2018] indicates prior use of weight clipping, a popular technique in GANs, from which we adopt this concept. While weight clipping is widely employed, our innovation lies in integrating it into the construction of a private stochastic gradient descent.


\textbf{Code (R4)} We thank R4 for taking the time to look into the code. The code indeed sets $\delta$ to a different value than the one used in our experiments, setting $\delta=10^{-5}$ we can reproduce our experiments from the shared code, we will correct this.
%There was indeed an error on the values of $\delta$ but it seems it appeared after getting the main results since we reran experiments with this value, obtaining the originally displayed results.
Regarding sampling, we confirm that we correctly do Poisson sampling, as stated in the paper, thanks to the DPDataLoader module provided by Opacus (line 112 in main.py).


\textbf{Details on the approach (R10)} R10 has concerns about the privacy of Lip-DP-SGD.  First, $X1$ is the maximum norm over the domain.  All our datasets have a bounded domain (and in preprocessing we normalize the data).  Hence, $X1$ is known and independent from the data.
%First, we confirm that $X1$, the maximum norm over the domain (after normalizing the data a known constant), is independent of the data and applied to all data points thanks to scaling techniques (section 3.3).
Second, [abadi et al. 2016] does perform clipping and noise addition per layer, we tried to make our algorithm as much as possible comparable to this original DP-SGD.  Also the privacy accounting is performed in the same way as in [Abadi et al. 2016].
%Second, it's worth noting that the DP-SGD [Abadi et al. 2016] is clipping the gradient and adding the noise at the layer level (refer to the paragraph "Per layer and time-dependent parameters" in section 3.1). Lip-DP-SGD adds noise in similar fashion, so we derived differential privacy guarantees of Lip-DP-SGD from DP-SGD. Lastly, the privacy budget is monitored thanks to a privacy accountant (section 3.3, paragraph "Privacy accounting") which counts the privacy loss at each iteration.


\textbf{Clearer abstract (R4)} R4 asks to clarify "By leveraging public information concerning the current global model and its location within the search domain" in the abstract. It states that (for noise calibration) we employ the norm of the model parameters which have been made differentially private and can hence be published ("public information regarding the current global model"), and dependending on the current "location within the search domain" of the parameter vector, the maximum possible gradient norm may be smaller than the global maximum.
%enabling the noise to adjust to the current position of the private model parameters throughout the search ("its location within the search domain").


\textbf{Baselines (R1,R2,R4)} Reviewers asked to compare to more baselines.
%noted the absence of certain baselines.
We compare to a single baseline DP-SGD to keep the comparison simple and fair.  Other baselines offer other variations/improvements which are in most cases orthogonal to ours, i.e., one could combine these variations/improvements with the one we propose and get similar effects on performance, fairness and other metrics.
%We exclusively compare with DP-SGD, anticipating that Lip-DP-SGD can leverage similar refinements.
%For instance, adaptive learning rate for DP learning [Kosekela et al. 2020] can be seamlessly incorporated into Lip-DP-SGD, mirroring the enhancements applied to DP-SGD.
Regarding PATE, its scope differs from ours. Our focus is on achieving privacy during optimization, in contrast to PATE, which operates post-optimization. Immediate privacy in optimization is vital, especially in scenarios where the aggregator's trustworthiness is uncertain, as seen in Federated learning setups where parties aim to secure data before sharing with an aggregator.

%\textbf{Typos (R10)}
\textbf{Formal theorem (R1)} Theorem 3.2 refers to Section 2.2 and Algorithm 1, we can make Theorem 3.2 more formal by collecting and repeating the conditions stated in Section 2.2 and Algorithm 1 already, and by making explicit that we assume that we assume the parameters let the algorithm convergence.

% \textbf{Additional comment (R4)}
% While the per-example clipping free may reduce the bias issue in DP-SGD, the noise is scaled to the gradient that has the highest value in each layer, I wonder would this lead to adding more noise.
%
% The lipschitz values analyzed in this paper only include the linear layer and the convolutional layer, it would be better to provide insight on how to extend to other architectures.
%
% In addition, if would further help reproducibility if the authors could report the hyperparameters that achieve best result in the paper instead of the hyperparameter search.
%


\end{document}
