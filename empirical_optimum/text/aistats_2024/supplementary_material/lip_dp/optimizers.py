import torch
from torch.nn.functional import normalize
from typing import Callable, List, Optional, Union
from opacus.optimizers import DPOptimizer
import numpy as np


def _mark_as_processed(obj: Union[torch.Tensor, List[torch.Tensor]]):
    """
    Marks parameters that have already been used in the optimizer step.

    DP-SGD puts certain restrictions on how gradients can be accumulated.
    In particular, no gradient can be used twice - client must
    call .zero_grad() between optimizer steps, otherwise privacy
    guarantees are compromised.
    This method marks tensors that have already been used in optimizer steps
    to then check if zero_grad has been duly called.

    Notes:
          This is used to only mark ``p.grad_sample`` and ``p.summed_grad``

    Args:
        obj: tensor or a list of tensors to be marked
    """

    if isinstance(obj, torch.Tensor):
        obj._processed = True
    elif isinstance(obj, list):
        for x in obj:
            x._processed = True


def _unmark_as_processed(obj: Union[torch.Tensor, List[torch.Tensor]]):

    if isinstance(obj, torch.Tensor):
        if hasattr(obj, "_processed"):
            delattr(obj, "_processed")
    elif isinstance(obj, list):
        for x in obj:
            if hasattr(x, "_processed"):
                delattr(x, "_processed")


def _check_processed_flag_tensor(x: torch.Tensor):
    """
    Checks if this gradient tensor has been used in optimization step.

    See Also:
        :meth:`~opacus.optimizers.optimizer._mark_as_processed`

    Args:
        x: gradient tensor

    Raises:
        ValueError
            If tensor has attribute ``._processed`` previously set by
            ``_mark_as_processed`` method
    """

    if hasattr(x, "_processed"):
        raise ValueError(
            "Gradients haven't been cleared since the last optimizer step. "
            "to obtain privacy guarantees you must call optimizer.zero_grad()"
            "on each step"
        )


def _check_processed_flag(obj: Union[torch.Tensor, List[torch.Tensor]]):
    """
    Checks if this gradient tensor (or a list of tensors) has been previously
    used in optimization step.

    See Also:
        :meth:`~opacus.optimizers.optimizer._mark_as_processed`

    Args:
        x: gradient tensor or a list of tensors

    Raises:
        ValueError
            If tensor (or at least one tensor from the list) has attribute
            ``._processed`` previously set by ``_mark_as_processed`` method
    """

    if isinstance(obj, torch.Tensor):
        _check_processed_flag_tensor(obj)
    elif isinstance(obj, list):
        for x in obj:
            _check_processed_flag_tensor(x)


def _generate_noise(
    std: float,
    reference: torch.Tensor,
    generator=None,
    secure_mode: bool = False,
) -> torch.Tensor:
    """
    Generates noise according to a Gaussian distribution with mean 0

    Args:
        std: Standard deviation of the noise
        reference: The reference Tensor to get the appropriate shape and device
            for generating the noise
        generator: The PyTorch noise generator
        secure_mode: boolean showing if "secure" noise need to be generated
            (see the notes)
    """
    zeros = torch.zeros(reference.shape, device=reference.device)
    if std == 0:
        return zeros
    if secure_mode:
        torch.normal(
            mean=0,
            std=std,
            size=(1, 1),
            device=reference.device,
            generator=generator,
        )  # generate, but throw away first generated Gaussian sample
        sum = zeros
        for _ in range(4):
            sum += torch.normal(
                mean=0,
                std=std,
                size=reference.shape,
                device=reference.device,
                generator=generator,
            )
        return sum / 2
    else:
        return torch.normal(
            mean=0,
            std=std,
            size=reference.shape,
            device=reference.device,
            generator=generator,
        )


def reshape_weight_to_matrix(weight: torch.Tensor) -> torch.Tensor:
    weight_mat = weight
    height = weight_mat.size(0)
    return weight_mat.reshape(height, -1)


def spectral_norm(module,
                  max_weight_norm,
                  n_power_iterations=5,
                  do_power_iteration=True,
                  eps=1e-10):
    weight = getattr(module, 'weight' + '_orig')
    u = getattr(module, 'weight' + '_u')
    v = getattr(module, 'weight' + '_v')
    weight_mat = reshape_weight_to_matrix(weight)

    if do_power_iteration:
        with torch.no_grad():
            for _ in range(n_power_iterations):
                v = normalize(torch.mv(weight_mat.t(), u), dim=0, eps=eps,
                              out=v)
                u = normalize(torch.mv(weight_mat, v), dim=0, eps=eps, out=u)
            if n_power_iterations > 0:
                # See above on why we need to clone
                u = u.clone(memory_format=torch.contiguous_format)
                v = v.clone(memory_format=torch.contiguous_format)

    sigma = torch.dot(u, torch.mv(weight_mat, v))
    if sigma > max_weight_norm:
        weight = weight / sigma * max_weight_norm
        sigma = max_weight_norm
    return weight, sigma


def get_operator_lip_layer(module):
    module_name = str(module)
    if 'Linear' in module_name:
        return ('linear', lambda x: x)
    elif 'Conv' in module_name:
        return ('conv', lambda x: np.sqrt(module.parameters().size))
    elif 'FullSort' in module_name:
        return ('activation', lambda x: 1)
    elif 'ReLU' in module_name:
        return ('activation', lambda x: 1)
    elif 'Sigmoid' in module_name:
        return ('activation', lambda x: 0.5)
    elif 'Flatten' in module_name:
        return ('flatten', lambda x: 1)
    else:
        raise ValueError('Non Lipschitz layer')


def get_operator_lip(model):
    modules = list(model.modules())
    operators = []
    for m in modules[1:]:
        operators.append(get_operator_lip_layer(m))
    return operators


def get_loss_lip(criterion, temperature=1.):
    loss_name = str(criterion).lower()
    if 'crossentropy' in loss_name:
        return np.sqrt(2) / temperature
    elif 'multimargin' in loss_name:
        return 1
    else:
        raise ValueError('Loss not implemented or not Lipschitz')


class LipDPOptimizer(DPOptimizer):
    """docstring for LipDPOptimizer."""

    def __init__(self, max_weight_norm, *args, **kwargs):
        super(LipDPOptimizer, self).__init__(*args, **kwargs)
        self.max_weight_norm = max_weight_norm
        # self.register_step_post_hook(project)

    def accumulate(self):
        """
        Aggregated gradients into `p.summed_grad```
        """
        for p in self.params:
            _check_processed_flag(p.grad_sample)
            grad_sample = self._get_flat_grad_sample(p).sum(0)
            # import ipdb
            # ipdb.set_trace()
            if p.summed_grad is not None:
                # import ipdb
                # ipdb.set_trace()
                p.summed_grad += grad_sample
            else:
                p.summed_grad = grad_sample

            _mark_as_processed(p.grad_sample)

    def pre_step(
        self, closure: Optional[Callable[[], float]] = None
    ) -> Optional[float]:
        """
        Perform actions specific to ``DPOptimizer`` before calling
        underlying  ``optimizer.step()``

        Args:
            closure: A closure that reevaluates the model and
                returns the loss. Optional for most optimizers.
        """
        self.accumulate()
        if self._check_skip_next_step():
            self._is_last_step_skipped = True
            return False

        self.add_noise()
        self.scale_grad()

        if self.step_hook:
            self.step_hook(self)

        self._is_last_step_skipped = False
        return True

    def project(self, input_norm, operators_lip, loss_lip):
        x_norms = [input_norm]
        lip_x = []
        lip_theta = []
        # forward pass
        for group in self.param_groups:
            for p, (layer_key, fn) in zip(group["params"], operators_lip):
                with torch.no_grad():
                    if layer_key in ['linear', 'conv']:
                        p_clip, layer_norm = spectral_norm(p,
                                                           self.max_weight_norm
                                                           )
                        p.copy_(p_clip)
                    elif layer_key in ['activation', 'flatten',
                                       'view', 'pool', 'drop']:
                        layer_norm = fn(p)
                    else:
                        raise ValueError('Illicite layer key')
                    # TODO: investigate if bias is true
                    x_norms.append(layer_norm * x_norms[-1])
                    lip_x.append(layer_norm)
                    lip_theta.append(fn(layer_norm))
        # append loss lip
        lip_x.append(loss_lip)
        # backward pass
        deltas_backward = list(map(lambda x, y: x*y, lip_x[::-1][:-1],
                                   x_norms[::-1][1:]))
        deltas_backward = list(map(lambda x, y: x*y, deltas_backward,
                                   lip_theta[::-1]))
        self.deltas_backward = deltas_backward

    def add_noise(self):
        """
        Adds noise to gradients with sensitivities.
        Stores noised result in ``p.grad``
        """
        deltas = self.deltas_backward[::-1]
        for i, p in enumerate(self.params):
            # import ipdb
            # ipdb.set_trace()
            _check_processed_flag(p.summed_grad)
            # import ipdb
            # ipdb.set_trace()
            noise = _generate_noise(
                std=self.noise_multiplier * deltas[i],
                reference=p.summed_grad,
                generator=self.generator,
                secure_mode=self.secure_mode,
            )
            p.grad = (p.summed_grad + noise).view_as(p)

            _mark_as_processed(p.summed_grad)


class LipDPOptimizerPerLayer(LipDPOptimizer):
    """docstring for LipDPOptimizer."""

    def __init__(self, max_weight_norm, *args, **kwargs):
        super(LipDPOptimizer, self).__init__(*args, **kwargs)
        self.max_weight_norm = max_weight_norm

    def project(self, input_norm, operators_lip, loss_lip):
        x_norms = [input_norm]
        lip_x = []
        lip_theta = []
        i = 0
        # forward pass
        for group in self.param_groups:
            for p, (layer_key, fn) in zip(group["params"], operators_lip):
                with torch.no_grad():
                    if layer_key in ['linear', 'conv']:
                        p_clip, layer_norm = spectral_norm(p,
                                                           self.max_weight_norm[i]
                                                           )
                        p.copy_(p_clip)
                        i += 1
                    elif layer_key in ['activation', 'flatten',
                                       'view', 'pool', 'drop']:
                        layer_norm = fn(p)
                    else:
                        raise ValueError('Illicite layer key')
                    # TODO: investigate if bias is true
                    x_norms.append(layer_norm * x_norms[-1])
                    lip_x.append(layer_norm)
                    lip_theta.append(fn(layer_norm))
        # append loss lip
        lip_x.append(loss_lip)
        # backward pass
        deltas_backward = list(map(lambda x, y: x*y, lip_x[::-1][:-1],
                                   x_norms[::-1][1:]))
        deltas_backward = list(map(lambda x, y: x*y, deltas_backward,
                                   lip_theta[::-1]))
        self.deltas_backward = deltas_backward
