from typing import Dict

import torch
import torch.nn as nn
from torch.utils.data import DataLoader
import torch.optim as optim
from torcheval.metrics import MulticlassAUROC

from opacus.optimizers import DPOptimizer
from opacus.accountants import RDPAccountant
from opacus.accountants.utils import get_noise_multiplier
from opacus.utils.batch_memory_manager import BatchMemoryManager
from opacus.distributed import DifferentiallyPrivateDistributedDataParallel\
    as DPDDP

from lip_dp.optimizers import LipDPOptimizer, LipDPOptimizerPerLayer,\
    get_loss_lip


def cleanup():
    torch.distributed.destroy_process_group()


def train(
    net: torch.nn.Module,
    train_loader: DataLoader,
    parameters: Dict[str, float],
    dtype: torch.dtype,
    device: torch.device,
) -> nn.Module:
    """
    Train CNN on provided data set.

    Args:
        net: initialized neural network
        train_loader: DataLoader containing training set
        parameters: dictionary containing parameters to
        be passed to the optimizer.
        dtype: torch dtype
        device: torch device
    Returns:
        nn.Module: trained CNN.
    """
    # Initialize network
    net.to(dtype=dtype, device=device)
    # world_size = torch.distributed.get_world_size()
    world_size = 1
    if world_size > 1:
        net = DPDDP(net)
    net.train()

    # Define loss and optimizer
    criterion = torch.nn.CrossEntropyLoss()
    criterion = criterion.to(device=device)
    loss_temperature = parameters.get("loss_temperature", 1.)
    # operators_lip = get_operator_lip(net)
    # TODO: get the above function back
    method = parameters.get("method", "grad_clip")

    if method == "weight_norm":
        operators_lip = parameters.get("operators_lip", [])
        loss_lip = get_loss_lip(criterion, loss_temperature)

    optimizer = optim.Adam(
        net.parameters(),
        lr=parameters.get("lr", 0.001),
        weight_decay=parameters.get("weight_decay", 0.0),
    )

    sample_rate = 1 / len(train_loader)
    expected_batch_size = int(len(train_loader.dataset) * sample_rate)
    eps = parameters.get("epsilon_target", None)
    num_epochs = parameters.get("num_epochs", 1)
    input_norm = parameters.get("input_norm", 1)
    if eps is not None:
        delta = parameters.get("delta", 1/10000)
        parameters["noise_multiplier"] = get_noise_multiplier(target_epsilon=eps,
                                                              target_delta=delta,
                                                              sample_rate=sample_rate,
                                                              epochs=num_epochs
                                                              )

    if method == "grad_clip":
        optimizer = DPOptimizer(
            optimizer=optimizer,
            noise_multiplier=parameters.get("noise_multiplier", 1),
            max_grad_norm=parameters.get("max_grad_norm", 1.),
            expected_batch_size=expected_batch_size,
        )
    elif method == "weight_norm" and parameters.get("per_layer_norm", False):
        # valid only for CNN() model
        max_weight_norm = [parameters.get("max_weight_norm_0", 1.),
                           parameters.get("max_weight_norm_1", 1.),
                           parameters.get("max_weight_norm_2", 1.),
                           parameters.get("max_weight_norm_3", 1.),
                           parameters.get("max_weight_norm_4", 1.),
                           parameters.get("max_weight_norm_5", 1.)
                           ]
        optimizer = LipDPOptimizerPerLayer(
            optimizer=optimizer,
            noise_multiplier=parameters.get("noise_multiplier", 1),
            max_weight_norm=max_weight_norm,
            max_grad_norm=parameters.get("max_grad_norm", None),
            expected_batch_size=expected_batch_size,
        )
    elif method == "weight_norm":
        optimizer = LipDPOptimizer(
            optimizer=optimizer,
            noise_multiplier=parameters.get("noise_multiplier", 1),
            max_weight_norm=parameters.get("max_weight_norm", 1.),
            max_grad_norm=parameters.get("max_grad_norm", None),
            expected_batch_size=expected_batch_size,
        )
    else:
        raise ValueError("Method not in ['weight_norm', 'grad_clip']")

    accountant = RDPAccountant()
    optimizer.attach_step_hook(
        accountant.get_optimizer_hook_fn(sample_rate=sample_rate)
    )

    # Train Network
    # pyre-fixme[6]: Expected `int` for 1st param but got `float
    for _ in range(num_epochs):
        with BatchMemoryManager(
            data_loader=train_loader,
            max_physical_batch_size=parameters.get('max_physical_batch_size',
                                                   parameters.get('batch_size',
                                                                  512)),
            optimizer=optimizer
        ) as memory_safe_data_loader:
            for inputs, labels in memory_safe_data_loader:
                # check whether batch is empty or vector
                if inputs.size(0) == 0 or inputs.size(0) == 1:
                    continue
                # move data to proper dtype and device
                inputs = inputs.to(dtype=dtype, device=device)
                labels = labels.to(device=device)

                if method == "weight_norm":
                    optimizer.project(input_norm, operators_lip, loss_lip)
                # zero the parameter gradients
                optimizer.zero_grad()

                # forward + backward + optimize
                outputs = net(inputs)
                loss = criterion(outputs / loss_temperature, labels)
                loss.backward()
                optimizer.step()

        if method == "weight_norm":
            # last step isnot projected
            optimizer.project(input_norm, operators_lip, loss_lip)
    if world_size > 1:
        cleanup()
    return net, accountant


def evaluate_auc(
    net: nn.Module,
    data_loader: DataLoader,
    dtype: torch.dtype,
    device: torch.device
) -> float:
    """
    Compute classification auc on provided dataset.

    Args:
        net: trained model
        data_loader: DataLoader containing the evaluation set
        dtype: torch dtype
        device: torch device
    Returns:
        float: classification accuracy
        float: classification auc
    """
    net.eval()
    metric = MulticlassAUROC(num_classes=10)
    with torch.no_grad():
        for inputs, labels in data_loader:
            # move data to proper dtype and device
            inputs = inputs.to(dtype=dtype, device=device)
            labels = labels.to(device=device)
            outputs = net(inputs)
            metric.update(outputs, labels)
    return metric.compute()


def evaluate_acc_auc(
    net: nn.Module,
    data_loader: DataLoader,
    dtype: torch.dtype,
    device: torch.device
) -> float:
    """
    Compute classification accuracy on provided dataset.

    Args:
        net: trained model
        data_loader: DataLoader containing the evaluation set
        dtype: torch dtype
        device: torch device
    Returns:
        float: classification accuracy
        float: classification auc
    """
    net.eval()
    correct = 0
    total = 0
    metric = MulticlassAUROC(num_classes=10)
    with torch.no_grad():
        for inputs, labels in data_loader:
            # move data to proper dtype and device
            inputs = inputs.to(dtype=dtype, device=device)
            labels = labels.to(device=device)
            outputs = net(inputs)
            metric.update(outputs, labels)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
    return {
        "accuracy": correct / total,
        "auc": metric.compute().item()
    }
