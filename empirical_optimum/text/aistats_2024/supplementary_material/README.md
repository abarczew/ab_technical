# Experiments

This repository contains the Python code used to produce the experimental results presented in "DP-SGD with weight clipping".

## Setup

```
# To create a virtual environment
python -m venv myenv

# To activate the virtual environment
source myenv/bin/activate

# Install requirements
pip install -r requirements.txt
```

## Data

Except for MNIST, Fashion-MNIST and CIFAR-10, data should be downloaded and store in `data` folder. Here are the links:
* [Adult Income](https://archive.ics.uci.edu/ml/datasets/adult)
* [Android Permissions](https://archive-beta.ics.uci.edu/dataset/722/naticusdroid+android+permissions+dataset)
* [Default Credit](https://archive-beta.ics.uci.edu/dataset/350/default+of+credit+card+clients)
* [Dropout](https://archive-beta.ics.uci.edu/dataset/697/predict+students+dropout+and+academic+success)
* [German Credit](https://archive-beta.ics.uci.edu/dataset/144/statlog+german+credit+data)
* [Nursery](https://archive-beta.ics.uci.edu/dataset/76/nursery)
* [Patient Survival](https://www.kaggle.com/datasets/mitishaagarwal/patient)
* [Thyroid](https://archive-beta.ics.uci.edu/dataset/102/thyroid+disease)
* [Yeast](https://archive-beta.ics.uci.edu/dataset/110/yeast)

or run
```
wget -O data/adult_income.csv https://archive.ics.uci.edu/ml/machine-learning-databases/adult/
wget -O data/android.csv https://archive-beta.ics.uci.edu/dataset/722/naticusdroid+android+permissions+dataset/
wget -O data/default_credit.csv https://archive-beta.ics.uci.edu/dataset/350/default+of+credit+card+clients/
wget -O data/dropout.csv https://archive-beta.ics.uci.edu/dataset/697/predict+students+dropout+and+academic+success/
wget -O data/german.csv https://archive-beta.ics.uci.edu/dataset/144/statlog+german+credit+data/
wget -O data/nursery.csv https://archive-beta.ics.uci.edu/dataset/76/nursery/
wget -O data/patient_survival.csv https://www.kaggle.com/datasets/mitishaagarwal/patient/
wget -O data/thyroid.csv https://archive-beta.ics.uci.edu/dataset/102/thyroid+disease/
wget -O data/yeast.csv https://archive-beta.ics.uci.edu/dataset/110/yeast/
```

## Usage

One can change hyperparameters in each `parameters.json` located in `"exp/[dataname]/[grad_clip/weight_norm]/parameters.json"`, results, formatted as per `ax.service` API from Ax-platform, will then be stored in `"exp/[dataname]/[grad_clip/weight_norm]/[folder name of experiment]/results.json"`. There is one command to run the experiements:

```
python main.py --exp [dataname] --runs [number of runs for the search] --save-dir [folder name of experiment] --workers [number of workers]
```
