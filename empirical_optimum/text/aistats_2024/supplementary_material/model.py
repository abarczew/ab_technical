import numpy as np
import torch.nn as nn
import torch.nn.functional as F
from torch._tensor import Tensor
import torch
from torchvision.models import VGG, VGG11_Weights

tabular_datasets = ['income', 'android', 'breast', 'default_credit',
                    'dropout', 'german', 'nursery', 'thyroid', 'yeast']


def load_vision_model(exp_name):
    if exp_name == 'mnist':
        net = CNN()
        # net = CNNSig()
    elif exp_name == 'fashionmnist':
        net = FancyCNN()
        # net = FancyCNNSig()
    elif exp_name == 'cifar':
        net = ShallowVGG()
        # net = ShallowVGGSig()
    return net


def load_tabular_model(exp_name, dataset):
    net = ClassifNet(dataset.input_dim,
                     dataset.class_num)
    return net


class CNN(nn.Module):
    def __init__(self) -> None:
        super().__init__()
        self.conv1 = nn.Conv2d(1, 20, kernel_size=5, stride=1)
        self.fc1 = nn.Linear(8 * 8 * 20, 64)
        self.fc2 = nn.Linear(64, 10)
        self.operators_lip = [
            ('conv', lambda x: 5 * x),  # np.sqrt(5*5)
            ('activation', lambda x: 1),
            ('pool', lambda x: 1),
            ('view', lambda x: 1),
            ('linear', lambda x: x),
            ('activation', lambda x: 1),
            ('linear', lambda x: x),
            ('activation', lambda x: 1)
        ]

    def forward(self, x: Tensor) -> Tensor:
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 3, 3)
        x = x.view(-1, 8 * 8 * 20)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return F.log_softmax(x, dim=-1)


class CNNSig(nn.Module):
    def __init__(self) -> None:
        super().__init__()
        self.conv1 = nn.Conv2d(1, 20, kernel_size=5, stride=1)
        self.fc1 = nn.Linear(8 * 8 * 20, 64)
        self.fc2 = nn.Linear(64, 10)
        self.operators_lip = [
            ('conv', lambda x: 5 * x),  # np.sqrt(5*5)
            ('activation', lambda x: 1),
            ('pool', lambda x: 1),
            ('view', lambda x: 1),
            ('linear', lambda x: x),
            ('activation', lambda x: 0.5),
            ('linear', lambda x: x),
            ('activation', lambda x: 1)
        ]

    def forward(self, x: Tensor) -> Tensor:
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 3, 3)
        x = x.view(-1, 8 * 8 * 20)
        x = F.sigmoid(self.fc1(x))
        x = self.fc2(x)
        return F.log_softmax(x, dim=-1)


def dropout_linear_relu(dim_in, dim_out, p_drop):
    return [nn.Dropout(p_drop),
            nn.Linear(dim_in, dim_out),
            nn.ReLU(inplace=True)]


def dropout_linear_sig(dim_in, dim_out, p_drop):
    return [nn.Dropout(p_drop),
            nn.Linear(dim_in, dim_out),
            nn.Sigmoid()]


def conv_relu_maxp(in_channels, out_channels, ks):
    return [nn.Conv2d(in_channels, out_channels,
                      kernel_size=ks,
                      stride=1,
                      padding=int((ks-1)/2), bias=True),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2)]


class FancyCNN(nn.Module):

    def __init__(self, num_classes=10):
        super(FancyCNN, self).__init__()

        # By default, Linear layers and Conv layers use
        # Kaiming He initialization

        self.features = nn.Sequential(
            *conv_relu_maxp(1, 16, 3),
            *conv_relu_maxp(16, 32, 3),
            *conv_relu_maxp(32, 64, 3)
        )
        # You must compute the number of features manualy to instantiate the
        # next FC layer
        # self.num_features = 64*3*3

        # Or you create a dummy tensor for probing the size of the feature maps
        probe_tensor = torch.zeros((1, 1, 28, 28))
        out_features = self.features(probe_tensor).view(-1)

        self.classifier = nn.Sequential(
            *dropout_linear_relu(out_features.shape[0], 128, 0.5),
            *dropout_linear_relu(128, 256, 0.5),
            nn.Linear(256, num_classes)
        )

        self.operators_lip = [
            ('conv', lambda x: 3 * x),
            ('activation', lambda x: 1),
            ('pool', lambda x: 1),
            ('conv', lambda x: 3 * x),
            ('activation', lambda x: 1),
            ('pool', lambda x: 1),
            ('conv', lambda x: 3 * x),
            ('activation', lambda x: 1),
            ('pool', lambda x: 1),
            # TODO: bias?

            ('view', lambda x: 1),

            ('drop', lambda x: 1),
            ('linear', lambda x: x),
            ('activation', lambda x: 1),
            ('drop', lambda x: 1),
            ('linear', lambda x: x),
            ('activation', lambda x: 1),

            ('linear', lambda x: x)
        ]

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size()[0], -1)  # OR  x = x.view(-1, self.num_features)
        y = self.classifier(x)
        return y


class FancyCNNSig(nn.Module):

    def __init__(self, num_classes=10):
        super(FancyCNNSig, self).__init__()

        # By default, Linear layers and Conv layers use
        # Kaiming He initialization

        self.features = nn.Sequential(
            *conv_relu_maxp(1, 16, 3),
            *conv_relu_maxp(16, 32, 3),
            *conv_relu_maxp(32, 64, 3)
        )
        # You must compute the number of features manualy to instantiate the
        # next FC layer
        # self.num_features = 64*3*3

        # Or you create a dummy tensor for probing the size of the feature maps
        probe_tensor = torch.zeros((1, 1, 28, 28))
        out_features = self.features(probe_tensor).view(-1)

        self.classifier = nn.Sequential(
            *dropout_linear_sig(out_features.shape[0], 128, 0.5),
            *dropout_linear_sig(128, 256, 0.5),
            nn.Linear(256, num_classes)
        )

        self.operators_lip = [
            ('conv', lambda x: 3 * x),
            ('activation', lambda x: 1),
            ('pool', lambda x: 1),
            ('conv', lambda x: 3 * x),
            ('activation', lambda x: 1),
            ('pool', lambda x: 1),
            ('conv', lambda x: 3 * x),
            ('activation', lambda x: 1),
            ('pool', lambda x: 1),
            # TODO: bias?

            ('view', lambda x: 1),

            ('drop', lambda x: 1),
            ('linear', lambda x: x),
            ('activation', lambda x: 0.5),
            ('drop', lambda x: 1),
            ('linear', lambda x: x),
            ('activation', lambda x: 0.5),

            ('linear', lambda x: x)
        ]

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size()[0], -1)  # OR  x = x.view(-1, self.num_features)
        y = self.classifier(x)
        return y


class ShallowVGG(nn.Module):

    def __init__(self, num_classes=10):
        super(ShallowVGG, self).__init__()

        # By default, Linear layers and Conv layers use
        # Kaiming He initialization

        self.features = nn.Sequential(
            nn.Conv2d(3, 32, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten(start_dim=1, end_dim=-1),
            nn.Linear(128, num_classes, bias=True),
        )

        self.operators_lip = [
            ('conv', lambda x: 3 * x),
            ('activation', lambda x: 1),
            ('pool', lambda x: 1),
            ('conv', lambda x: 3 * x),
            ('activation', lambda x: 1),
            ('pool', lambda x: 1),
            ('conv', lambda x: 3 * x),
            ('activation', lambda x: 1),
            ('pool', lambda x: 1),
            ('conv', lambda x: 3 * x),
            ('activation', lambda x: 1),
            ('pool', lambda x: 1),
            ('flatten', lambda x: 1),
            ('linear', lambda x: x)
        ]

    def forward(self, x):
        return self.features(x)


class ShallowVGGSig(nn.Module):

    def __init__(self, num_classes=10):
        super(ShallowVGGSig, self).__init__()

        # By default, Linear layers and Conv layers use
        # Kaiming He initialization

        self.features = nn.Sequential(
            nn.Conv2d(3, 32, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1),
            nn.Sigmoid(),
            nn.AvgPool2d(kernel_size=2, stride=2),
            nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1),
            nn.Sigmoid(),
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten(start_dim=1, end_dim=-1),
            nn.Linear(128, num_classes, bias=True),
        )

        self.operators_lip = [
            ('conv', lambda x: 3 * x),
            ('activation', lambda x: 1),
            ('pool', lambda x: 1),
            ('conv', lambda x: 3 * x),
            ('activation', lambda x: 1),
            ('pool', lambda x: 1),
            ('conv', lambda x: 3 * x),
            ('activation', lambda x: 0.5),
            ('pool', lambda x: 1),
            ('conv', lambda x: 3 * x),
            ('activation', lambda x: 0.5),
            ('pool', lambda x: 1),
            ('flatten', lambda x: 1),
            ('linear', lambda x: x)
        ]

    def forward(self, x):
        return self.features(x)


class VGGLip(VGG):
    def __init__(self, features, operators_lip, *arg, **kwarg):
        super(VGGLip, self).__init__(features, *arg, **kwarg)
        self.operators_lip = operators_lip + [
            ('linear', lambda x: x),
            ('activation', lambda x: 1),
            ('drop', lambda x: 1),
            ('linear', lambda x: x),
            ('activation', lambda x: 1),
            ('linear', lambda x: x)
        ]


def make_layers(cfg, batch_norm=False):
    layers = []
    in_channels = 3
    for v in cfg:
        if v == 'M':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        else:
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
            if batch_norm:
                layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
            else:
                layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = v
    return nn.Sequential(*layers)


def make_operators_lip(cfg):
    operators_lip = []
    for v in cfg:
        if v == 'M':
            operators_lip += [('pool', lambda x: 1)]
        else:
            operators_lip += [('conv', lambda x: 3 * x)]
    return operators_lip


cfg = {
    'A': [64, 'M', 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
    'B': [64, 64, 'M', 128, 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512,
          'M'],
    'D': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512, 'M',
          512, 512, 512, 'M'],
    'E': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 256, 'M', 512, 512, 512,
          512, 'M',
          512, 512, 512, 512, 'M'],
}


def vgg11_lip(weights=VGG11_Weights.IMAGENET1K_V1, progress=True, **kwargs):
    if weights is not None:
        kwargs["init_weights"] = False
        # if weights.meta["categories"] is not None:
        #     _ovewrite_named_param(kwargs, "num_classes",
        #                           len(weights.meta["categories"]))
    model = VGGLip(make_layers(cfg['A']),
                   make_operators_lip(cfg['A']),
                   **kwargs)
    if weights is not None:
        model.load_state_dict(weights.get_state_dict(progress=progress))
    return model


class ClassifNet(nn.Module):
    """DP NeuralNet for classification."""

    def __init__(self, input_dim, output_dim, hidden_dim=20, input_norm=1):
        super().__init__()
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.input_norm = input_norm
        self.output_dim = output_dim
        self.linear_1 = nn.Linear(self.input_dim, self.hidden_dim,
                                  bias=False)
        self.linear_2 = nn.Linear(self.hidden_dim, self.output_dim,
                                  bias=False)
        self.fn_activation = nn.Sigmoid()
        self.fn_output = nn.Softmax(dim=0)
        self.max_x = np.min([i for i in [np.sqrt(input_dim), self.input_norm]
                             if i is not None])
        self.max_y = 1  # data needs to be normalized
        self.n_layers = 2
        self.activation_max = 1/2

        self.operators_lip = [
            ('linear', lambda x: x),
            ('activation', lambda x: 0.5),
            ('linear', lambda x: x),
            ('activation', lambda x: 0.5)
        ]

    def forward(self, x):
        hidden = self.fn_activation(self.linear_1(x))
        outputs = self.fn_output(self.linear_2(hidden))
        return outputs
