\section{Our approach}
\label{sec:approach}


\newcommand{\thetanorm}{u^{(\theta)}}

In this work, we leverage Lipschitz value estimation to determine sensitivity. While traditional \gradDpSgd{} controls sensitivity via gradient sample clipping, our new method estimates cumulative gradient sensitivity. This is grounded in Lipschitz-constrained model literature, highlighting the connection between the Lipschitz value for input and parameter. Subsection \ref{subsec:backprop} demonstrates the use of backpropagation for gradient sensitivity estimation. Subsection \ref{subsec:lip_values} delves into determining an upper Lipschitz bound, and in \ref{subsec:lipdpsg}, we introduce \weightDpSgd{}, a novel algorithm ensuring privacy without gradient clipping.

\subsection{Backpropagation}\label{subsec:backprop}

% Modern deep learning libraries, including PyTorch \cite{pytorch} and TensorFlow \cite{tensorflow2015-whitepaper}, utilize efficient backpropagation implementations grounded in automatic differentiation \cite{rall_automatic_1981}.
Consider a feed-forward network $f_\theta$.  We define $\mathcal{L}_k(\theta,(x_k,y))=\ell\left(\left(f_{\theta_K}^{(K)}\circ \ldots \circ f_{\theta_k}^{(k)}\right)(x_k), y\right)$.
For feed-forward networks, backpropagation relies on the subsequent recursive equations:
% todo: update with notation from alg
\begin{equation}\label{eq:back_prop}
  \begin{aligned}
    \lossPartialInput{k} & = \lossPartialInput{k+1}\frac{\partial x_{k+1}}{\partial x_k} = \lossPartialInput{k+1}\layerPartialInput{k} \\
    \lossPartialWeight{k} & = \lossPartialInput{k+1}\frac{\partial x_{k+1}}{\partial \theta_k} = \lossPartialInput{k+1}\layerPartialWeight{k}.
  \end{aligned}
\end{equation}
Note that $\theta_k$ and $x_k$ are vectors, so also $\lossPartialInput{k}$, $\lossPartialWeight{k}$ and $\lossPartialInput{k+1}$ are vectors, and $\layerPartialInput{k}$ and $\layerPartialWeight{k}$ are Jacobian matrices.
In terms of $2$-norms there holds
% We apply the Cauchy-Schwarz inequality to Equation \ref{eq:back_prop}:
\begin{equation}\label{eq:layer_inequality}
  \begin{aligned}
    \left\|\lossPartialInput{k}\right\|_2 & \leq \left\|\lossPartialInput{k+1}\right\|_2  \left\|\layerPartialInput{k}\right\|_2  \\
    \left\|\lossPartialWeight{k}\right\|_2 & \leq \left\|\lossPartialInput{k+1}\right\|_2 \left\|\layerPartialWeight{k}\right\|_2
  \end{aligned}
\end{equation}
%with $\|\cdot\|_2$ the spectral norm for Jacobian matrices, and the Euclidean norm for vectors.

We will use $l_k$ to denote an upper bound of $\max_{x_k,y}\frac{\partial \mathcal{L}_k(\theta,x_k,y)}{\partial x_k}$.  In particular, we will ensure that $l_{K+1}\ge \max_{k_{K+1},y}\frac{\partial\ell}{x_{K+1}}(x_{K+1},y)$ and
\begin{equation}\label{eq:layer_sensitivity}
  \begin{aligned}
    l_k & \ge  l_{k+1} \max_{x_k} \left\|\layerPartialInput{k}\right\|_2 \\
    \Delta_k & \ge  l_{k+1} \max_{x_k} \left\|\layerPartialWeight{k}\right\|_2
  \end{aligned}
\end{equation}
Hence, $l_k$ is an upper bound of
% the norm of the gradient with respect to $x_k$, i.e.,
$\max_{x_k}\left\|\lossPartialInput{k}\right\|$.
By definition \ref{def:sensitivity} and the triangle inequality, the sensitivity of the gradient $\lossPartialWeight{k}$ is upper bounded by twice $\max_{x_k} \|\lossPartialWeight{k}\|$, so $\Delta_k\ge \sensitivity\left(\lossPartialWeight{k}\right)/2$.

Note that we can easily provide such upper bounds $l_k$ and  $\theta_k$ as the layers $\fnnFunc^{(k)}$ and the loss $\ell$ are Lipschitz. If so,  since all $\fnnFunc^{(k)}$ and $\ell$ are differentiable on any $x_k$, per Rademacher’s theorem \cite{Rademacher1919},  $\left\| \lossPartialInput{k} \right\|$ is bounded by its Lipschitz value. We only need to find a tight upper bound of this Lipschitz value.

\subsection{Estimating lipschitz values} \label{subsec:lip_values}

In this section we bound Lipschitz values of different types of layers.

\textbf{Losses and activations.} Examples of Lipschitz losses encompass Softmax Cross-entropy, Cosine Similarity, and Multiclass Hinge. When it comes to activation layers, several prevalent ones, such as ReLU, tanh, and Sigmoid, are 1-Lipschitz. We provide a detailed list in the supplementary material  \cref{tab:lip}.

\textbf{Linear layers.} If $\layerFunc{k}$ is a linear layer, then
\begin{equation}\label{eq:linear_lip}
  \begin{aligned}
    \left\|\layerPartialWeight{k}\right\|_2 = \left\| \frac{\partial (W_k^{\top} x_k + B_k)}{\partial (W_k,B_k)} \right \|_2 = \|(x_k,1)\|_2, \\
    \left\|\layerPartialInput{k}\right\|_2 = \left\| \frac{\partial (W_k^{\top} x_k+B_k)}{\partial x_k} \right \|_2 = \|W_k\|_2.
  \end{aligned}
\end{equation}

\textbf{Convolutional layers.}


%While one can bound the $l_k$ and $\Delta_k$ of a convolutional layer depending on its specific structure, we here focus on a general class of convolutional layers which have the property that every output node $x_{k+1,i}$ is computed as $x_{k+1,j} = \sum_{(r,s):A_{r,s}=i} \theta_r x_{k,s}$ where $A\in (\{1 \ldots n_{k+1}\}\cup\{\bot\})^{|\theta_k|\times n_k}$.   This representation allows for many types of convolutional layers, e.g., rectangles moving over images, windows moving over strings, etc.   The key constraint which we exploit is that the product of each input node $x_{k,s}$ and parameter $\theta_{k,r}$ contributes to at most one output node. Let $R=\max_i |\{(r,s)\mid A_{r,s}=i\}|$.

There are many types of convolutional layers, e.g., depending on the data type (strings, 2D images, 3D images \ldots), shape of the filter (rectangles, diamonds \ldots).  Here we provide as an example only a derivation for convolutional layers for 2D images with rectangular filter.
In that case, the input layer consists of $n_k = c_{in} h w $ nodes and the output layer consists of $n_{k+1}= c_{out} hw$ nodes with $c_{in}$ input channels, $c_{out}$ output channels, $h$ the height of the image and $w$ the width.
Then, $\theta_k\in\mathbb{R}^{c_{in}\times c_{out}\times h' \times w'}$ with $h'$ the height of the filter and $w'$ the width of the filter.  Indexing input and output with channel and coordinates, i.e., $x_k\in \mathbb{R}^{c_{in}\times h\times w}$ and $x_{k+1}\in \mathbb{R}^{c_{out}\times h \times w}$ we can then write
\[
x_{k+1,c,i,j} = \sum_{d=1}^{c_{in}} \sum_{r=1}^{h'} \sum_{s=1}^{w'} x_{k,d,i+r,j+s} \theta_{k,c,d,r,s}
\]
where components out of range are zero.
We can derive (see Appendix \ref{sec:bound.lipschitz.convol} for details) that



%\[
%\|x_{k+1}\|_2^2 = \sum_{c,i,j} \left(\sum_{d=1}^{c_{out}} \sum_{r=1}^{h'} \sum_{s=1}^{w'} x_{k,d,i+r,j+s} \theta_{k,c,d,r,s} \right)^2
%\]



\begin{equation}\label{eq:conv_lip_input}
  \begin{aligned}
    \left\|\layerPartialInput{k}\right\|_2
    %= \left\| \frac{\partial \theta_k \ast x_k}{\partial x_k} \right \|_2
    \leq  \sqrt{h'w'}\|\theta_k\|_2
  \end{aligned}
\end{equation}

  \begin{equation}
    \left\|\layerPartialWeight{k}\right\|_2
    \leq \sqrt{h'w'}\|x_k\|_2
  \end{equation}


We summarize the upper bounds of the Lipschitz values, either on the input or on the parameters, for each layer type in the supplementary material \cref{tab:lip}.
We can conclude that networks for which the norms of the parameter vectors $\theta_k$ are bounded, are Lipschitz networks as introduced in \cite{miyato2018spectral}, i.e., they are FNN for which each layer function $f_{\theta_k}^{(k)}$ is Lipschitz.  We will denote by $\thetaSpaceLeC$ the set of all paremeter vectors $\theta$ for $f_\theta$ such that $\|\theta_k\|\le C$ for $k=1\ldots K$, and by $\thetaSpaceEqC$ the set of all parameter vectors for which $\|\theta_k\|=C$ for $k=1\ldots K$.


\textbf{LayerSensitivity.} We observe that the upper bounds we have found above are either functions of the norm of the parameters, or functions of the norm of the input. Let's call $\phi_{x_k}$ and $\phi_{\theta_k}$ these two functions. We can now introduce \cref{alg:layer_sensitivity} to compute the sensitivity $\Delta_k$ of layer $k$.  Here we denote by $X_k$ the maximal possible norm of $x_k$, i.e., for all possible inputs $x_k$, $\|x_k\|=\left\|(f^{(k-1)}_{\theta_{k-1}}\circ \ldots \circ f_{\theta_1}^{(1)})(x_1)\right\|\le X_k$.
It capitalizes on a forward pass to compute the maximal input norms $X_k$, and a backward pass applying Equation \ref{eq:layer_sensitivity}.

\begin{algorithm}[ht]
   \caption{$\textsc{LayerSensitivity}(f, \theta, u^{(\theta)})$}
   % \label{alg:\gradDpSgd-logloss-l1}
   \label{alg:layer_sensitivity}
\begin{algorithmic}[1]
  \State{{\bfseries Input:} $K$ layered feed-forward model $f$, parameters $\theta$, max norm or parameter norm $u^{(\theta)}$, max input norm $X_1$, upper bound of loss Lipschitz value $l_{K+1}$.}
   \For{$k=1$ {\bfseries to} $K$} \Comment{Forward pass}
   \State{$X_{k+1} \leftarrow X_{k}u_k^{(\theta)}$}
   \EndFor
   \For{$k=K$ {\bfseries to} $1$} \Comment{Backward pass}
   \State{$l_k\text{ }\leftarrow  l_{k+1} \left\|\layerPartialInput{k}\right\|_2 =l_{k+1}\phi_{x_{k}}(u_k^{(\theta)})$}
   \State{$\Delta_k \leftarrow l_{k+1} \left\|\layerPartialWeight{k}\right\|_2 =l_{k+1} \phi_{\theta_{k}}(X_k)$}
   \EndFor
   \State{{\bfseries Output:} $\Delta_K, \Delta_{K-1}, \dots, \Delta_1$.}
\end{algorithmic}
\end{algorithm}

\subsection{\weightDpSgd}\label{subsec:lipdpsg}
We introduce a novel differentially private stochastic gradient descent algorithm, called \weightDpSgd, that leverages the estimation of the per-layer sensitivity of the model to provide differential privacy without gradient clipping.


\begin{algorithm}[htb]
   \caption{$\textsc{\weightDpSgd}$: Differentially Private Stochastic Gradient Descent with Lipschitz constrains.}
   % \label{alg:\gradDpSgd-logloss-l1}
   \label{alg:weight-dpsgd}
   \begin{algorithmic}[1]
     \Function{\weightDpSgd}{Data set $\dset\in\dsetspace$, feed-forward model $f_\theta$, loss function $\mathcal{L}$, hypothesis space $\Theta\subseteq \mathbb{R}^k$, privacy parameters $\epsilon$ and $\delta$, noise multiplier $\sigma$, batch size $\batchsize\ge 1$, learning rate $\eta$, max gradient norm $C$}
  %constant $\vLipschitz\in\mathbb{R}_+^p$,
   \State{Initialize $\tilde{\theta}$ randomly from $\Theta$}
   % $\widetilde{\theta}^1$: Choose any point from $\mathcal{C}$.
   % \State{$l \leftarrow (C)_{1\leq k \leq K}$}\Comment{max gradient norm per layer}
   \State{$(\thetanorm,\tilde{\theta}) \gets$ \textsc{ClipWeights}($\tilde{\theta}$,$C$)}
     %      \For{$t=1$ {\bfseries to} $T$}
   \While{privacy budget $(\epsilon, \delta)$ not reached}
   % \STATE Pick $(x, y) \thicksim_u Z$ with replacement.

   \State{$(\Delta_k)_{k=1}^K \leftarrow$ \textsc{LayerSensitivity}$(f, \tilde{\theta}, \thetanorm)$}
%   \State{$\Delta_K, \Delta_{K-1}, \dots, \Delta_1 \leftarrow$ \textsc{ComputePerLayerSensitivity}$(f, \tilde{\theta}^{(t)}, l^{(t)})$}
   \State{$\batchset \gets \emptyset$}\Comment{Poisson sampling}
   \While{$\batchset=\emptyset$}
   \For{\label{ln:lipdpsgd.poisson}$z\in Z$}
   \State{With probability $s/|Z|$: $\batchset\gets\batchset\cup\{z\}$}
   \EndFor
   \EndWhile
   %\State{Draw sample $\batchset = {(x_1, y_1),\dots, (x_s, y_s)}$}\Comment{Sample batch with Poisson sampling}
   \For{$k=1\ldots K$} \Comment{gradient per layer}
   \State{\label{ln:draw.noise}Draw $b_k \thicksim \mathcal{N}(0, \sigma^2 \Delta_k^2 \mathbbm{I})$}
   \State{$\tilde{g}_k \leftarrow \frac{1}{|\batchset|}\left(\sum_{i=1}^{|\batchset|} \nabla_{\tilde{\theta}_k}\ell(f_{\tilde{\theta}}(x_i), y_i)+b_k\right)$}
   \State{$\tilde{\theta}_k \leftarrow \tilde{\theta}_k - \eta(t) \tilde{g}_k$}

%   \State{$\tilde{\theta}_k, l_k \leftarrow \prod( \tilde{\theta}_k - \eta\tilde{g}_k)$}\Comment{Normalize if spectral norm greater than $C$, store value in $l_k$  otherwise.}
   \EndFor
   \State{$(\thetanorm,\tilde{\theta}) \gets$ \textsc{ClipWeights}($\tilde{\theta}$,$C$)}
%   \State{$\thetatsucc \leftarrow \thetat - \eta(t)[s \gradt+\noiset]$}\Comment{Update}
   \EndWhile
   \State{\textbf{Return} $\tilde{\theta}$}
   \EndFunction
   \Function{ClipWeights}{$\tilde{\theta}$, $C$}
   \For{$k=1\ldots K$}
   \State{\label{ln:setThetaNorm}$\thetanorm_k\gets \min(C,\|\tilde{\theta}_k\|)$}
   \State{$\tilde{\theta}_k\gets l_k\tilde{\theta}_k/\|\tilde{\theta}_k\|$}
   \EndFor
   \State{\textbf{return} $(\thetanorm,\tilde{\theta})$}
   \EndFunction
\end{algorithmic}
\end{algorithm}

\begin{theorem}
  Provided a feed-forward model $\fnnFunc$ composed of Lipschitz constrained operators, a Lipschitz loss $\ell$ and a bounded input norm $X_1$, $\textsc{\weightDpSgd}$ is differentially private.
\end{theorem}

Indeed, \weightDpSgd{} utilizes the Gaussian mechanism. The gradient's sensitivity is determined without any privacy costs, as it depends only on the current parameter values (which are privatized in the previous step, and post-processing privatized values doesn't take additional privacy budget) and not on the data.

\textbf{Privacy accountanting.} \weightDpSgd{} adopts the same privacy accounting as \gradDpSgd. Specifically, the accountant draws upon the privacy amplification \cite{kasiviswanathan_what_2010} brought about by Poisson sampling and the Gaussian moment accountant \cite{abadi_deep_2016}. It's worth noting that while we utilized the Renyi Differential Privacy (RDP) accountant \cite{abadi_deep_2016, mironov_renyi_2019} in our experiments, \weightDpSgd{} is versatile enough to be compatible with alternative accountants.

\textbf{Requirments.} As detailed in previous subsection \ref{subsec:lip_values}, the loss and the model operators need to be Lipschitz and the norm of the input needs to be bounded. We've enumerated several losses and operators that meet these criteria in the supplementary material. While we use the spectral norm to characterize Lipschitzness \cite{yoshida_spectral_2017, miyato2018spectral} in our study \ref{subsec:lip_values}, other methods are also available, as discussed in \cite{arjovsky_wasserstein_2017}.



\textbf{ClipWeights.} The $\textsc{ClipWeights}$ function is essential to the algorithm, ensuring Lipschitzness, which facilitates model sensitivity estimation. As opposed to standard Lipschitz-constrained networks \cite{yoshida_spectral_2017, miyato2018spectral} which increase or decrease the norms of parameters to make them equal to a pre-definied value, our approach normalizes weights only when their current norm exceeds a threshold. This results in adding less DP noise for smaller norms. Importantly, as $\theta$ is already made private by noise addition in the previous iteration, its norm is private too.
% there's no privacy concern, as parameters, being private, ensure their norm's privacy.
%Moreover, to optimize computation, we limit spectral normalizations, leveraging stored parameter norms if they fall below the threshold, hence the utilization of $\thetanorm$ in \cref{alg:weight-dpsgd}.

\textbf{Computation techniques.} For both \cref{alg:layer_sensitivity} and  $\textsc{ClipWeights}$ it's crucial to compute the greatest singular matrix values efficiently. A renowned technique is the \textit{power method} \cite{MisesPraktischeVD}. If this isn't sufficiently fast,
%when dealing with convolutions, the related matrix might be of large dimensions, rendering the direct application of the power method infeasible. To address this,
the \textit{power method} can be enhanced using autograd \cite{scaman_lipschitz_2019}.
Another idea is to use the Frobenius norm, which is faster to compute but may have drawbacks in terms of tightly bounding the norm.
As computing spectral norms is relatively costly, we avoid to recompute them by storing them in $\thetanorm$ in \cref{alg:weight-dpsgd}.


\subsection{Avoiding the bias of gradient clipping}

Our \weightDpSgd{} algorithm finds a local optimum (for $\theta$) of $F(\theta,Z)$ in $\thetaSpaceLeC$ while \gradDpSgd{} doesn't necessarily find a local optimum of $F(\theta,Z)$ in $\thetaSpace$.
In particular, we prove in Appendix \ref{app:bias} the following
\newcommand{\thmStmWeightClipConverges}{For any objective function $F$ as defined in Section \ref{sec:erm}, \weightDpSgd{} converges to a local optimum of $F(\theta,Z)$ in $\thetaSpaceLeC$.}
\begin{theorem}
\label{thm:weightClipConverges}\thmStmWeightClipConverges{}
\end{theorem}
Essentially, the effect of scaling weight vectors to have bounded norm after a gradient step is equivalent to projecting the gradient on the boundary of the feasible space if the gradient brings the parameter vector out of $\thetaSpaceLeC$.

Furthermore, \cite{chen_understanding_2021} shows an example showing that gradient clipping can introduce bias.  We add a more detailed discussion in Appendix \ref{app:bias}.
Hence, \gradDpSgd{} does not necessarily converge to a local optimum of $F(\theta,Z)$, even when sufficient data is available to estimate $\theta$.  While \weightDpSgd{} can only find models in $\thetaSpaceLeC$ and this may introduce another suboptimality, as our experiments will show this is only a minor drawback in practice, while also others observed that Lipschitz networks have good properties \cite{bethune_pay_2022}.  Moreover, it is easy to check whether \weightDpSgd{} outputs parameters on the boundary of $\thetaSpaceLeC$ and hence the model could potentially improve by relaxing the weight norm constraint.  In contrast, it may not be feasible to detect that \gradDpSgd{} is outputting potentially suboptimal parameters.  Indeed, consider a federated learning setting (e.g., \cite{Bonawitz2017a}) where data owners collaborate to compute a model without revealing their data.  Each data owner locally computes a gradient and clips it, and then the data owners securely aggregate their gradients and send the average gradient to a central party updating the model.  In such setting, no party would be able to evaluate that gradient clipping introduces a strong bias in some direction.
