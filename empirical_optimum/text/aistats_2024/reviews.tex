\documentclass{article}

\graphicspath{ {../images/} }

\newcommand{\todo}[1]{

  \textcolor{green}{\textsc{Todo:} {#1}}

}

\newcommand{\janfoot}[1]{{\textcolor{blue}{{\footnote{{\textcolor{blue}{{#1}}}}}}}}

\begin{document}

\title{Reviews on DP-SGD with weight clipping}

\maketitle

\section{Reviews}
\subsection{Weakness}

\subsubsection{approach}
\begin{itemize}
\item The advantage of using weight clipping is only supported by empirical results on fully connected neural networks and datasets with a limited number of features. It is unknown whether this idea is general enough for broader applications.
\item From what I understand, all this paper does is clip the per-layer weights in order to obtain a naive and very loose bound on the Lipschitz constant (in Section 3.2) which is then used as the clipping threshold in DP-SGD. So it seems to me that this paper essentially does "classic gradient clipping" (as written in the paper) with a loose upper bound on the Lipschitz constant (obtained by weight clipping) set as the clipping threshold. However in practice, usually smaller clip norms perform better and clip norms near the Lipschitz constant perform very poorly; see for e.g., [1] (cited below). Thus, I doubt this method is any good for practical usage. Also see the next point for more concerns w.r.t. practical usability.
\item Also, this bound is only for multi-layer perceptrons and there are no analogous bounds derived for more widely used architectures like CNNs and Transformers (where it would be more challenging too, I guess). Moreover, I suspect that the bound for perceptrons will get looser as the number of layers increases and anyways empirical results are only shown for two-layer perceptions which is too small for an empirical paper.
\item This paper does not suggest anything novel or a concept which can be widely used. The motivation for this work is non-existent. It essentially formulates the problem in a different manner and if the network is Lipschitz constrained then putting an equivalent value of the maximum norm of the gradient will give the same algorithm and since the network always gives gradients which are Lipschitz constrained, then there would be no need to clip the gradients, which is what seems like the authors have basically done. Moreover, this method brings upon a lot of hyperparameters with respect to the number of layers which may not always be feasible to work at a large scale level.
\end{itemize}

\subsubsection{bias}
\begin{itemize}
  \item The authors claimed to solve the bias in gradient clipping which was not supported by any theoretical or empirical results in this paper.
  \item The paper claims that "weight clipping doesn't suffer from the bias which the classic gradient clipping can cause" (lines $46-47$ and $49-50$ in the contributions) but does NOT explain it. First, it should be clarified that the notion of "bias" is the statistical one in gradient estimation induced due to clipping. Under this notion of bias, the proposed method yields an unbiased gradient estimator because the clipping threshold is set $\geq$ Lipschitz constant (resulting in no actual clipping). Actually, there is a bias-variance tradeoff w.r.t. the clipping threshold as explained in [1, 2]. So even though increasing the clipping threshold reduces the bias, this comes at the cost of increasing the noise variance (which is proportional to the clipping threshold ${ }^2$ ). Therefore, the best clipping threshold -- which balances this bias-variance tradeoff - is invariably smaller than the Lipschitz constant (as I also mentioned in point \#1).
  \item I am not clear on the example you gave in the paper in section 3.1. I feel that the statistical advantage given is not applicable for this setting as there would be no parameters involved for that setting (because the number of features is 0), hence there will be no gradients. Then how can the authors comment on the superiority of their method on the DP-SGD or actually prove that DP-SGD would not hold valid in that case?
  \item Referring to lines 49-50: I couldn’t find a formal argument in the paper that weight clipping doesn’t suffer from the bias which classic gradient clipping can cause. This is listed as a contribution, and so it would be nice to highlight the contribution itself (e.g., is there any Theorem showing the same?, etc.).
\end{itemize}

\subsubsection{setting}
\begin{itemize}
  \item The authors used the word 'federated learning' many times while the only setting considered here is the DP-SGD. Not much was done in this paper for federated learning.
  \item The paper mentions federated learning (FL) as a strong motivation multiple times (e.g., line 3 in the abstract, 3rd-4th paras in intro, etc.), but neither the proposed technique nor the empirical evaluation seem FL-focused. For instance, the Federated Averaging optimizer is a standard foundational optimizer for FL (even for privacy-focused FL works, e.g., [3, 4]), and user-level DP is a natural notion of privacy in that setting. There are also benchmark datasets like StackOverflow, EMNIST, etc. available for evaluating with user-level granularity, and the paper fails to consider FL beyond the motivation. The paper mentions Federated SGD but I’m not sure if for example-level DP there is any significant difference between SGD and Federated SGD in general.
\end{itemize}

\subsubsection{emperical results}
\begin{itemize}
  \item I don't find the experiments convincing either. In addition to my concerns in point \#2, I have some more issues. For "gradient global" how is the clipping threshold being picked? The datasets apart from MNIST are not very standard. I'd have liked to see results on more standard datasets like CIFAR, etc. Further, I'd have preferred to see accuracy as the metric for standard datasets.
  \item I’m not sure I completely understand the empirical evaluation of classic grad clip. For instance, in Fig 1. (a), it is surprising to see the AUC decrease for gradient_global when Epsilon increases from its 2nd highest value to the highest value in the evaluation. I also don’t understand the behavior of gradient_clip in Fig 1. (b), wherein the ratio of avg. noise std. and norm of gradient seems to be constant for all values of epsilon evaluated? Does the latter indicate all experiments were run with a constant noise multiplier but for different number of steps (depending on the epsilon value)? The comparison with the baseline is very unclear to me.
  \item In Figure 1(a), why does the AUC go down for 'gradient_global' when epsilon increases to 10?
  \item In Figure 1(b), why does the value of 'noise std/gradient norm' of 'gradient_global' not change for different values of epsilon?
  \item Line 239-240. I do not understand the statement 'as the weight clipping norm increases'. It is not in Figure 1(a), right? Also, Figure 2(a) does not show the local Lipschitz value.
\end{itemize}

\subsubsection{calculus}
\begin{itemize}
  \item The authors did not consider the intercept parameter in each layer of the neural network, i.e., $b_k$ in Line 93, for the analysis of the norm of the gradient from the weight. Therefore, the bound could be incorrect and the privacy guarantee may be invalid.
  \item Many intuitive explanations in this paper lean towards being somewhat incorrect, and I feel can be avoided altogether if not explained well. E.g., in line 37, the explanation could be interpreted as always dividing gradients by the clip norm, whereas it is only true for gradients exceeding the clip norm. Similarly, in lines 64-65, there is a subtle difference between the singleton instance referred to in “all but one instances”, and “last instance”.
  \item Referring to lines 193 and 197: calling a variant of a technique by its functional call (which itself takes in 12 arguments) can be difficult to read and comprehend. Might it be better to give them intuitive shorthands/acronyms?
\end{itemize}

\subsubsection{usability}
\begin{itemize}
  \item How do you set the $C_i$ 's in practice? If I set them to be too small, then I may end up learning a very sub-optimal model.
  \item The bound on the Lipschitz constant in Section 3.2 requires that the loss function $\ell$ is Lipschitz. However, the two most commonly used losses, viz., the cross-entropy loss and squared loss are not Lipschitz. For the cross-entropy loss, $\frac{\partial \ell(y, \bar{y})}{\partial \tilde{y}}=\left(\frac{1-y}{1-\tilde{y}}-\frac{y}{\tilde{y}}\right)$ which is clearly unbounded for $\tilde{y} \rightarrow 0$ or $\tilde{y} \rightarrow 1$. For the squared loss, $\frac{\partial \ell(y, \bar{y})}{\partial \tilde{y}}=(\tilde{y}-y)$ which is also unbounded for $\tilde{y} \rightarrow \infty$. Now this can be circumvented if $\tilde{y}$ is uniformly bounded away from 0 and 1 (respectively, $\infty$ ) for the cross-entropy (respectively, squared) loss which is possible assuming the weights are bounded. But the computations don't seem trivial to me. Can the authors clarify how the Lipschitz constant of $\ell$ (i.e., $\hat{L}_{\ell}$ ) was computed?
  \item The quantity $r=\max \|x\|_2$ also should be estimated privately ideally or it should be mentioned that $r$ is publicly known.
\end{itemize}

\subsubsection{readability}
\begin{itemize}
  \item The presentation needs to be significantly improved. Algorithm 1 is hard to parse and parameterizing DP-SGD with so many inputs (in lines 121/122, 193, 197) makes it unwieldy; both need to be made concise for better readability. Also, I think there are some errors in lines $17-20$ of Algorithm 1 in the way it is currently stated - perhaps line 19 should be If Clip = 'weight' and $C=L^g$ in this case? Further, making unjustified claims such as in lines 46-47 and 49-50 (discussed above in point \#3) is not a good idea.
  \item The writing quality could be greatly improved. There are many typos even in the pseudo-code of the main Algorithm 1 (e.g., while using quotation marks, I see {“, ”} at various places).
\end{itemize}


\subsubsection{limitations}
\begin{itemize}
  \item Clipping the weights constrains the space of models we can learn; this kind of restriction could be detrimental. This aspect has not been discussed in the paper. As I mentioned in point \#2, this may make the choice of $C_i$ 's critical.
\end{itemize}


[1]: Das, Rudrajit, et al. "Beyond uniform lipschitz condition in differentially private optimization." arXiv preprint arXiv:2206.10713 (2022).
[2]: Kamath, Gautam, Xingtu Liu, and Huanyu Zhang. "Improved rates for differentially private stochastic convex optimization with heavy-tailed data." International Conference on Machine Learning. PMLR, 2022.
[3]: H. Brendan McMahan, Daniel Ramage, Kunal Talwar, Li Zhang. Learning Differentially Private Recurrent Language Models. ICLR 2018.
[4]: Peter Kairouz, Brendan McMahan, Shuang Song, Om Thakkar, Abhradeep Thakurta, Zheng Xu. Practical and Private (Deep) Learning Without Sampling or Shuffling. ICML 2021.

\subsection{Minor issues}
\begin{itemize}
  \item Citation issues: For example, [26] is published in ICLR 2018, and [32] is published in NeurIPS 2018. Please check again your citations to make sure the published papers are cited correctly.
  \item The x-axis (learning rate) in Figure 4 in the appendix does not follow the setting in Line 18 in the appendix. Please check the details describing the experimental setup.
  \item Line 150 . The definition of $v_k$ should not contain $\rho_{k-1}$.
\end{itemize}

\end{document}
