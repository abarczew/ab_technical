\section{Gradient clipping based DP-SGD}
\label{sec:grad-dpsgd}

For comparison with Algorithm \ref{alg:weight-dpsgd},
Algorithm \ref{alg:grad-dpsgd} shows the classic DP-SGD algorithm based on gradient clipping.

\input{subfiles/algos/dp_sgd.tex}


\section{Estimating Lipschitz values}

We summarize the upper bounds of the Lipschitz values, either on the input or on the parameters, for each layer type in \cref{tab:lip}. It's important to mention that for the loss, the Lipschitz value is solely dependent on the output $x_{K+1}$.

\begin{table}[ht]
\centering
\begin{tabular}{|cccc|}
\hline
Layer & Definition & Lip.{} value on input $x_k$ & Lip.{} value on parameter $\theta_k$ \\
\hline
Dense & $\theta_k^{\top}x_k $ & $\|\theta_k\|$ & $\|x_k\|$ \\
\hline
Convolutional & $\theta_k \ast x_k$ & $\sqrt{h^\prime w^\prime}\|\theta_k\|$ & $\sqrt{h^\prime w^\prime}\|x_k\|$ \\
\hline
Normalization &  $\frac{x-\mathrm{E}[x]}{\sqrt{\operatorname{Var}[x]+\epsilon}}$ & $1$ & - \\
\hline
ReLU & $\max(x_k, 0)$ & $1$ & - \\
\hline
Sigmoid & $\frac{1}{1+e^{-x_k}}$ & $1/2$ & - \\
\hline
Softmax Cross-entropy & $y\log\textsc{softmax}(x_{K+1}) / \tau$ & $\sqrt{2}/\tau$ & - \\
\hline
Cosine Similarity & $\frac{x_{K+1}^{\top}y}{\|x_{K+1}\|_2\|y\|_2}$ & $1/\min \|x_{K+1}\|$ & - \\
\hline
Multiclass Hinge & $\left\{\max \left(0, \frac{m}{2}-x_{i_{K+1}} \cdot y_i\right)\right\}$ & $1$ & - \\
\hline
\end{tabular}
\caption{Summary table of Lipschitz values with respect to the layer.}\label{tab:lip}
\end{table}

with $\textsc{Softmax}(x_i) = \frac{\exp(x_i)}{\sum_{j=1}^c\exp(x_j)}$, $c$ the number of classes. For cross-entropy, $\tau$ an hyperparameter on the Softmax Cross-entropy loss also known as the temperature.  For convolutional layers, $h'$ and $w'$ are the height and width of the filter.  For multiclass hinge, $m$ is a hyperparameter known as 'margin'.

\subsection{Details for the convolutional layer}
\label{sec:bound.lipschitz.convol}



%We must distinguish two cases. In the first one, we consider the Lipschitz value with respect to the input $x_k$. Given that operators in convolutional layers, namely convolved feature maps, are linear with respect to the input, we can apply the previous inequality \ref{eq:linear_lip}:

%\begin{equation}\label{eq:conv_lip_input}
%  \begin{aligned}
%    \left\|\layerPartialInput{k}\right\|_2 = \left\| \frac{\partial \theta_k \ast x_k}{\partial x_k} \right \|_2 \leq  \sqrt{n_{k+1}}\|\theta_k\|_1.
%  \end{aligned}
%\end{equation}

%In the second one, we consider the Lipschitz value with respect to the parameters. We can interpret the convolved feature map as a dot product by re-arranging coordinates of the filters, the input and the output \cite{miyato2018spectral}.




\begin{theorem}
  The convolved feature map $(\theta \ast \cdot): \mathbb{R}^{n_k \times |x_k|} \rightarrow \mathbb{R}^{n_{k+1} \times n \times n}$, with zero or circular padding, is Lipschitz and
  \begin{equation}
    \|\nabla_{\theta_k} (\theta_k \ast x_k) \|_2 \leq \sqrt{h'w'}\|x_k\|_2
    \text{ and }
    \|\nabla_{x_k} (\theta_k \ast x_k) \|_2 \leq \sqrt{h'w'}\|\theta_k\|_2
  \end{equation}
  with $w'$ and $h'$ the width and the height of the filter.
\end{theorem}

\begin{proof}
The output $x_{k+1} \in \mathbb{R}^{c_{o u t} \times n \times n}$ of the convolution operation is given by:
\[
x_{k+1, c, r, s}=\sum_{d=0}^{c_{i n}-1} \sum_{i=0}^{h'-1} \sum_{j=0}^{w'-1} x_{k, d, r+i, s+j} \theta_{k, c, d, i, j}
\]
There follows:
\begin{eqnarray*}
  \|x_{k+1}\|^2_2
  &=& \sum_{c=0}^{c_{o u t}-1} \sum_{r=1}^n \sum_{s=1}^n \left( \sum_{d=0}^{c_{i n}-1} \sum_{i=0}^{h'-1} \sum_{j=0}^{w'-1} x_{k,d, r+i, s+j} \theta_{k, c, d, i, j}\right)^2\\
  &\le&  \sum_{c=0}^{c_{o u t}-1} \sum_{r=1}^n \sum_{s=1}^n \left( \sum_{d=0}^{c_{i n}-1} \sum_{i=0}^{h'-1} \sum_{j=0}^{w'-1} x_{k,d, r+i, s+j}^2 \right) \left( \sum_{d=0}^{c_{i n}-1} \sum_{i=0}^{h'-1} \sum_{j=0}^{w'-1} \theta_{k, c, d, i, j}^2\right) \\
  &=&  \left( \sum_{d=0}^{c_{i n}-1} \sum_{i=0}^{h'-1} \sum_{j=0}^{w'-1}  \sum_{r=1}^n \sum_{s=1}^n x_{k,d, r+i, s+j}^2 \right) \left(  \sum_{c=0}^{c_{o u t}-1} \sum_{d=0}^{c_{i n}-1} \sum_{i=0}^{h'-1} \sum_{j=0}^{w'-1} \theta_{k, c, d, i, j}^2\right)  \\
  &\le& h'w' \left( \sum_{d=0}^{c_{i n}-1} \sum_{r=1}^n \sum_{s=1}^n x_{k,d, r, s}^2 \right) \left(  \sum_{c=0}^{c_{o u t}-1} \sum_{d=0}^{c_{i n}-1} \sum_{i=0}^{h'-1} \sum_{j=0}^{w'-1} \theta_{k, c, d, i, j}^2\right)  \\
  &=& h'w' \|x_k \|_2 \|\theta_k\|_2
\end{eqnarray*}


Since $\theta_k\ast \cdot$ is a linear operator:
\begin{equation*}
  \|(\theta_k \ast x_k) - (\theta_k^\prime \ast x_k)\|_2 = \|(\theta_k - \theta_k^\prime) \ast x_k\|_2 \leq \|\theta_k - \theta_k^\prime\|_2 \sqrt{h'w'}\|x_k\|_2
\end{equation*}

Finally, the convolved feature map is differentiable so the spectral norm of its Jacobian is bounded by its Lipschitz value:
\begin{equation*}
  \|\nabla_{\theta_k} (\theta_k \ast x_k) \|_2 \leq \sqrt{h'w'}\|x_k\|_2
\end{equation*}
Analogously,
\begin{equation*}
  \|\nabla_{x_k} (\theta_k \ast x_k) \|_2 \leq \sqrt{h'w'}\|\theta_k\|_2
\end{equation*}
\end{proof}

\section{Experiments}\label{sec:app.exp}

\paragraph{Optimization.}
For the tabular datasets, we performed a full grid search to optimize the hyperparameters.
For both tabular and image datasets, we employed Bayesian optimization \cite{balandat_botorch_2020}. Configured as a multi-objective optimization program \cite{daulton_differentiable_2020}, our focus was to cover the Pareto front between model utility (accuracy) and privacy ($\epsilon$ values at a constant level of $\delta$, set to $1/n$ as has become common in this type of experiments). To get to finally reported values, we select the point on the pareto front given by the Python librairy BoTorch \cite{balandat_botorch_2020}.


\subsection{Hyperparameters}
\label{sec:app.exp.hyperparameters}

\paragraph{Hyperparameter selection.}
In the literature, there are a wide range of improvements possible over a direct application of SGD to supervised learning, including general strategies such as pre-training, data augmentation and feature engineering, and DP-SGD specific optimizations such as adaptive maximum gradient norm thresholds.  All of these can be applied in a similar way to both \weightDpSgd{} and \gradDpSgd{} and to keep our comparison sufficiently simple, fair and understandable we didn't consider the optimization of these choices.

We did tune hyperparameters inherent to specific model categories, in particular
the initial learning rate $\eta(0)$ (to start the adaptive learning rate strategy $\eta(t)$) and (for image datasets) the number of groups, and hyperparameters related to the learning algorithm, in particular the (expected) batch size $s$ and the threshold $C$ on the gradient norm respectively weight norm.

The initial learning rate $\eta(0)$ is tuned while the following $\eta(t)$ are set adaptively. Specifically, we use the strategy of the Adam algorithm \cite{adam}, which update each parameter using the ratio between the moving average of the gradient (first moment) and the square root of the moving average of its squared value (second moment), ensuring fast convergence.



We also investigated varying the maximum norm of input vectors $X_0$ and the hyperparameter $\tau$ of the cross entropy objective function, but the effect of these hyperparameters turned out to be insignificant.
% tried to tune for weights X_0 and temperature, but no significant effect.
%weight: s, X_0, C, temperature
%grad: s, C
%softmax CE -> \sqrt{2}/temp
%a set of hyperparameters that directly impact parameter norms for \weightDpSgd. Among them are the input norm $X_0$, an upper bound of the Lipschitz value of the loss function $l_{K+1}$, batch size $s$, and the respective Lipschitz constraints of the layers $C$.
%In the case of \gradDpSgd, we search for optimal gradient clipping norm and batch size $s$. Similarly to the clipping norm of the \gradDpSgd, the Lipschitz constrains of the layer $C$ of \weightDpSgd{}

Both the clipping threshold $C$ for gradients in \gradDpSgd{} and the clipping threshold $C$ for weights in \weightDpSgd{} can be tuned for each layer separately. While this offers improved performance, it does come with the cost of consuming more of the privacy budget, and substantially increasing the dimensionality of the hyperparameter search space.
In a few experiments we didn't see significant improvements in allowing per-layer varying of $C_k$, so we didn't further pursue this avenue.

\cref{tab:hyper} summarizes the search space of hyperparameters.
It's important to note that we did not account for potential (small) privacy losses caused by hyperparameter search, a limitation also acknowledged in other recent works such as \cite{papernot_hyperparameter_2022}.

\begin{table}[ht]
\centering
\begin{tabular}{|cc|}
\hline
Hyperparameter & Range  \\
\hline
Noise multiplier $\sigma$ & [0.4, 5] \\
\hline
Weight clipping threshold $C$ & [1, 15] \\
\hline
Gradient clipping threshold $C$ & [1, 15] \\
\hline
Batch size $s$ & [32, 512] \\
\hline
$\eta(0)$ & [0.0001, 0.01] \\
\hline
Number of groups (group normalization) & [8, 32] \\
\hline\antoinecolor{update?}
% Max input norm $X_0$  & [1, 12] \\
% \hline
% Hyperparameter of CCE $\tau$  & [1, 12] \\
% \hline
\end{tabular}
\caption{Summary of hyperparameter space.}\label{tab:hyper}
\end{table}

%\janfoot{this is inconsistent: the text mentions that we optimize $\theta(0)$, $T$, $s$ and $C$, while the Table presents a different set.  In particular, text and Table overlap in Number of epochs $T$ (maybe), norm threshold $C$ and Batch size $s$;  The table contains (and the text does not contain) $\tau$ and 'noise multiplier'; the Text contains (and the table does not contain) $\eta(0)$.}
%\antoinefoot{I've fixed the table. As for noise multiplier, this is to cover levels of privacy, I've added this comment in the table.}
%\janfoot{Ok, but what is a noise multiplier?}\antoinefoot{I've added a definition of the noise multiplier above}




\subsection{Models}


\cref{tab:data} shows details of the models we used to train on tabular and image datasets. We consider $7$ tabular datasets: adult income \cite{misc_adult_2}, android permissions \cite{misc_naticusdroid_(android_permissions)_dataset_722}, breast cancer \cite{misc_breast_cancer_wisconsin_(diagnostic)_17}, default credit \cite{misc_default_of_credit_card_clients_350}, dropout \cite{misc_predict_students'_dropout_and_academic_success_697}, German credit \cite{misc_statlog_(german_credit_data)_144} and nursery \cite{misc_nursery_76}.  See Table \ref{tab:auc} for the number of instances and features for each tabular dataset.

\begin{table}[ht]
\centering
\begin{tabular}{|cccccc|}
\hline
Dataset & Image size & Model & Number of layers & Loss & No. of Parameters \\
\hline
Tabular Datasets & -  & MLP & 2 & CE & 140 to 2,120 \\
\hline
MNIST & 28x28x1 & ConvNet & 3 & CE & 83,154 \\
\hline
FashionMNIST & 28x28x1 & ConvNet & 6 & CE & 132,746 \\
\hline
CIFAR-10 & 32x32x3 & ShallowVGG & 6 & CE & 131,466 \\
\hline
\end{tabular}
\caption{Summary table of datasets with respective models architectures details.}\label{tab:data}
\end{table}


\subsection{Runtime}\label{runtime}

Our experiments didn't show significant deviations from the normal runtime behavior one can expect for neural network training.  As an illustration, we compared on the Default Credit dataset and on the MNIST dataset %, Fashion-MNIST and CIFAR-10
the mean epoch runtime of \gradDpSgd{} with \weightDpSgd{}.
%Since \weightDpSgd{} is implemented with PyTorch and Opacus, we compare with a similar implementation for \gradDpSgd{}.
We measure runtime against the logical batch size, limiting the physical batch size to prevent memory errors as recommended by the PyTorch documentation \cite{pytorch}. \cref{fig:runtime} shows how \weightDpSgd{} is slightly inefficient in terms of runtime compared to \gradDpSgd{}, especially on image datasets. It may be possible to further improve \weightDpSgd{} runtime as it currently heavily relies on the data sampler provided by Opacus, which processes data per instance, while applying batch processing techniques inspired on PyTorch would be more efficient.  The staircase shape of the plot seems to be a result of PyTorch and Python memory management strategies.

\begin{figure*}
  \centering
  \mbox{
    \subfigure[Default Credit\label{fig:default_credit_run}]{\includegraphics[width=0.48\linewidth]{../../src/exp/default_credit/plot/runtime/results.png}}\quad
    \subfigure[MNIST\label{fig:mnist_run}]{\includegraphics[width=0.48\linewidth]{../../src/exp/mnist/plot/runtime/results.png}}
  }
  \caption{Mean runtime in seconds per batch size on one epoch over the Default Credit dataset \ref{fig:default_credit_run} and the MNIST dataset \ref{fig:mnist_run} comparing \gradDpSgd{} (in blue) and \weightDpSgd{} (in red).}
  \label{fig:runtime}
\end{figure*}

\subsection{Detailed results}\label{app:gn_table}
\cref{tab:detail_acc} provides a summary of accuracy performances for tabular datasets with and without group normalization. It's worth noting that while epsilon values may not be identical across algorithms, we present the epsilon value of \gradDpSgd{} and report performances corresponding to lower epsilon values for the other two algorithms, consistent with \cref{tab:auc}.

\begin{table*}
  \caption{Accuracy per dataset and method at $\epsilon=1$ and $\delta=1/n$, in bold the best result and underlined when the difference with the best result is not statistically significant at a level of confidence of 5\%.}
  \label{tab:detail_acc}
  \centering
  \begin{tabular}{lrrrrrrr}
    \toprule
    Methods & & \multicolumn{2}{c}{\gradDpSgd{}}   & \multicolumn{2}{c}{\weightDpSgd{}} & \multicolumn{2}{c}{\globalWeightDpSgd{}}  \\
    Datasets (\#instances $n$ $\times$ \#features $p$) & $\epsilon$ & w/ GN & w/o GN & w/ GN & w/o GN & w/ GN & w/o GN \\
    \midrule
    Adult Income (48842x14) \cite{misc_adult_2} & 0.414 & 0.822 & 0.824 & 0.829 & \textbf{0.831} & 0.713 & 0.804 \\
    Android (29333x87) \cite{misc_naticusdroid_(android_permissions)_dataset_722} & 1.273 & 0.947 & 0.951 & 0.952 & \textbf{0.959} & 0.701 & 0.945 \\
    Breast Cancer (569x32) \cite{misc_breast_cancer_wisconsin_(diagnostic)_17} & 1.672 & 0.813 & 0.773 & \textbf{0.924} & 0.798 & 0.519 & 0.7 \\
    Default Credit (30000x24) \cite{misc_default_of_credit_card_clients_350} & 1.442 & 0.804 & 0.809 & 0.815 & \textbf{0.816} & 0.774 & 0.792 \\
    Dropout (4424x36) \cite{misc_predict_students'_dropout_and_academic_success_697} & 1.326 & 0.755 & 0.763 & 0.816 & \textbf{0.819} & 0.573 & 0.736 \\
    German Credit (1000x20) \cite{misc_statlog_(german_credit_data)_144} & 3.852 & 0.735 & 0.735 & 0.722 & \underline{0.746} & 0.493 & 0.68 \\
    Nursery (12960x8) \cite{misc_nursery_76} & 1.432 & 0.916 & 0.919 & 0.912 & \textbf{0.931} & 0.487 & 0.89 \\
    \bottomrule
  \end{tabular}
\end{table*}

\subsection{Gradient clipping behavior}
\label{app:exp.clipfreq}

In Section \ref{sec:avoid.bias} we argued that \gradDpSgd{} introduces bias.
There are several ways to demonstrate this.  For illustration we show here the error between the true average gradient
\[
  g_k^{\weightDpSgd} = \frac{1}{|V|} \sum_{i=1}^{|V|} \nabla_{\tilde{\theta}_k} \ell(f_{\tilde{\theta}}(x_i))
\]
i.e., the model update of Algorithm \ref{alg:weight-dpsgd} without noise, and the average clipped gradient
\[
g_k^{\gradDpSgd} = \frac{1}{|V|}\sum_{i=1}^{|V|} \hbox{clip}_C\left( \nabla_{\tilde{\theta}_k} \ell(f_{\tilde{\theta}}(x_i)) \right),
\]
i.e., the model update of Algorithm \ref{alg:grad-dpsgd} without noise.

\cref{fig:clip} shows the error $\left\| g_k^{\weightDpSgd} - g_k^{\gradDpSgd} \right\|$ together with the norm of the \gradDpSgd{} model update $\left\|g_k^{\gradDpSgd}\right\|$.

One can observe for both considered datasets that while the model converges and the average clipped gradient decreases, the error between \gradDpSgd{}'s average clipped gradient and the true average gradient increases.  At the end, the error in the gradient caused by clipping is significant, and hence the model converges to a different point than the real optimum.


\begin{figure*}
  \centering
  \mbox{
    \subfigure[Dropout\label{fig:dropout_clip}]{\includegraphics[width=0.48\linewidth]{../../src/exp/dropout/plot/clipping/results.png}}\quad
    \subfigure[Adult Income\label{fig:income_clip}]{\includegraphics[width=0.48\linewidth]{../../src/exp/income/plot/clipping/results.png}}
  }
  \caption{Norm of the average error $g - \text{clip}(g)$ (in blue) and norm of the average of $\text{clip}(g)$ (in red) across training iterations on the Dropout dataset \ref{fig:dropout_clip} (averaged over 500 instances) and the Adult Income dataset \ref{fig:income_clip} (averaged over 500 instances).}
  \label{fig:clip}
\end{figure*}

\subsection{Regularization techniques.}\label{app:regularization}
In \cite{de2022unlocking} multiple other regularization/optimization techniques are proposed.  In our experiments, we found that while these techniques sometimes help, there is no significant or systematic difference in the effect on the results of \gradDpSgd{} and \weightDpSgd{}.
For example, Figure \ref{fig:param_avg} illustrates how parameter averaging on Fashion-MNIST% and CIFAR-10
, as indicated by \cite{de2022unlocking}, has a similar impact observed for both \gradDpSgd{} and \weightDpSgd{}.  To keep our experiments simple and interpretable, in all other experiments in this paper we don't consider the optimizations proposed by  \cite{de2022unlocking}, except for the group normalization.

\begin{figure*}
  \centering
  \mbox{
    \subfigure[Fashion-MNIST\label{fig:fashion_ema}]{\includegraphics[width=0.48\linewidth]{../../src/exp/fashionmnist/plot/ema_23_nogroup_vs_noema/accuracy_results.png}}\quad
  %  \subfigure[CIFAR-10\label{fig:cifar_ema}]{\includegraphics[width=0.48\linewidth]{../../src/exp/cifar/plot/ema_23_nogroup_vs_noema/accuracy_results.png}}
  }
  \caption{Accuracy results for the Fashion-MNIST%\ref{fig:fashion_ema} and CIFAR-10 \ref{fig:cifar_ema} datasets
  , comparing \gradDpSgd{} (in blue) and \weightDpSgd{} (in red), lines are dashed when the underlying model is implemented without parameter averaging.}
  \label{fig:param_avg}
\end{figure*}


\section{\weightDpSgd{} library}
\label{sec:lib-LipDpSgd}
We offer an open-source toolkit for implementing LipDP-SGD on any FNN model structure. This toolkit builds on the Opacus and PyTorch libraries.
Drawing inspiration from Opacus, our library introduces the `LipPrivacyEngine` class to facilitate private training. This class is dependent on two main components: the `DataLoader`, which utilizes Poisson sampling to harness the advantages of privacy amplification \cite{kasiviswanathan_what_2010}, and the `Optimizer`, responsible for sensitivity calculation, differential privacy noise addition, and parameter normalization during each iteration.

`README.md`, provided in the supplementary materials, details how to run the library and how to reproduce the experiments.



% Below is a sample showcasing how to set up training using LipDP-SGD:
% % \lstinputlisting[language=Python]{algos/makelipprivate.py}
% \begin{lstlisting}[language=Python]
% from lip_dp import LipPrivacyEngine
%
% lip_privacy_engine = LipPrivacyEngine()
%
% model, optimizer, train_loader = lip_privacy_engine.make_private_with_epsilon(
%     module=model,
%     optimizer=optimizer,
%     data_loader=train_loader,
%     epochs=EPOCHS,
%     target_epsilon=EPSILON,
%     target_delta=DELTA,
% )
% \end{lstlisting}


\section{Avoiding the bias of gradient clipping}
\label{app:bias}

We show that \weightDpSgd{} converges to a local minimum in $\thetaSpaceLeC$ while \gradDpSgd{} suffers from bias and may converge to a point which is not a local minimum of $\thetaSpace$.

We use the word 'converge' here somewhat informally, as in each iteration independent noise is added the objective function slightly varies between iterations and hence none of the mentioned algorithms converges to an exact point.
We here informally mean approximate convergence to a small region, assuming a sufficiently large data set $Z$ and/or larger $\epsilon$ such that privacy noise doesn't significantly alter the shape of the objective function.  Our argument below hence makes abstraction of the noise for simplicity, but in the presence of small amounts of noise a similar argument holds approximately, i.e., after sufficient iterations \weightDpSgd{} will produce $\theta$ values close to a locally optimal $\theta$ while \gradDpSgd{} may produce $\theta$ values in a region not containing the relevant local minimum.

First, let us consider convergence.

\textbf{Theorem} \ref{thm:weightClipConverges}.  \thmStmWeightClipConverges{}

\begin{proof}[Proof sketch]
  We consider the problem of finding a local optimum in $\thetaSpaceLeC$:
  \[
    \begin{array}{ll}
      \text{minimize} & F(\theta,Z) \\
      \text{subject to} & \|\theta\|_2 \le C
    \end{array}
  \]
  We introduce a slack variable $\zeta$:
   \[
    \begin{array}{ll}
      \text{minimize} & F(\theta,Z) \\
      \text{subject to} & \|\theta\|_2 + \zeta^2 = C
    \end{array}
  \]
  Using Lagrange multipliers, we should minimize
  \[
    F(\theta,Z) - \lambda (\|\theta\|_2 + \zeta^2 - C)
  \]
  An optimum in $\theta$, $\lambda$ and $\zeta$ satisfies
  \begin{eqnarray}
    \label{eq:LeC.lagrange.1} \nabla_\theta F(\theta, Z) - \lambda \theta &=& 0\\
    \label{eq:LeC.lagrange.2} \|\theta\|_2 + \zeta^2 - C &=& 0\\
    \label{eq:LeC.lagrange.3} 2\lambda \zeta &=&0
  \end{eqnarray}
  From Eq \ref{eq:LeC.lagrange.3}, either $\lambda=0$ or $\zeta=0$
  If $\zeta>0$, $\theta$ is in the interior of $\thetaSpaceLeC$ and there follows $\lambda=0$ and from Eq \ref{eq:LeC.lagrange.1} that $\nabla_\theta F(\theta, Z) =0$.  For such $\theta$, \weightDpSgd{} does not perform weight clipping.  If the learning rate is sufficiently small, and if it converges to a $\theta$ with norm $\|\theta\|_2<C$ it is a local optimum.
  On the other hand, if $\zeta=0$, there follows from Eq \ref{eq:LeC.lagrange.2} that $\|\theta\|_2=C$, i.e., $\theta$ is on the boundary of $\thetaSpaceLeC$.
  If $\theta$ is a local optimum in $\thetaSpaceLeC$, then $\nabla_\theta F(\theta,Z)$ is perpendicular on the ball of vectors $\theta$ with norm $C$, and for such $\theta$ \weightDpSgd{} will add the multiple $\eta(t).\nabla_\theta F(\theta,Z)$ to $\theta$ and will next scale $\theta$ back to norm $C$, leaving $\theta$ unchanged.  For a $\theta$ which is not a local optimum in $\thetaSpaceLeC$, $\nabla_\theta F(\theta,Z)$ will not be perpendicular to the ball of $C$-norm parameter vectors, and adding the gradient and brining the norm back to $C$ will move $\theta$ closer to a local optimum on this boundary of $\thetaSpaceLeC$.  This is consistent with Eq \ref{eq:LeC.lagrange.1} which shows the gradient with respect to $\theta$ for the constrained problem to be of the form $\nabla_\theta F(\theta, Z) - \lambda \theta$.
\end{proof}


Theorem \ref{thm:weightClipConverges} shows that in a noiseless setting, if \weightDpSgd{} converges to a stable point that point will be a local optimum in $\thetaSpaceLeC$.  In the presence of noise and/or stochastic batch selection, algorithms of course don't converge to a specific point but move around close to the optimal point due to the noise in each iteration, and advanced methods exist to examine such kind of convergence.  The conclusion remains the same: \weightDpSgd will converge to a neighborhood of the real local optimum, while as we argue \gradDpSgd{} will often converge to a neighborhood of a different point.

Second, we argue that \gradDpSgd{} introduces bias. This was already pointed out in \cite{chen_understanding_2021}'s examples 1 and 2.  In Section \ref{app:exp.clipfreq} we also showed experiments demonstrating this phenomenon.  Below, we provide a simple example which we can handle (almost) analytically.
%we show with an example that \gradDpSgd{} may introduce bias and does not have this desirable property.

A simple situation where bias occurs and \gradDpSgd{} does not converge to an optimum of $F$ is when errors aren't symmetrically distributed, e.g., positive errors are less frequent but larger than negative errors.

Consider the scenario of simple linear regression.
A common assumption of linear regression is that instances are of the form $(x_i,y_i)$ where $x_i$ is drawn from some distribution $P_x$ and $y_i=ax_i+b+e_i$ where $e_i$ is drawn from some zero-mean distribution $P_e$.  When no other evidence is available, one often assume $P_e$ to be Gaussian, but this is not necessarily the case.  Suppose for our example that $P_x$ is the uniform distribution over $[0,1]$ and $P_e$ only has two possible values, in particular $P_e(9)=0.1$, $P_e(-1)=0.9$ and $P_e(e)=0$ for $e\not\in\{9,-1\}$.  So with high probability there is a small negative error $e_i$ while with small probability there is a large positive error, while the average $e_i$ is still $0$.
Consider a dataset $Z=\{(x_i,y_i)\}_{i=1}^n$.
Let us consider a model $f(x) = \theta_1 x \theta_2$ and
let us use the square loss $\mathcal{L}(\theta,Z)=\sum_{i=1}^n \ell(x_i,y_i)/n$ with $\ell(\theta, x,y) = (\theta_1 x + \theta_2 - y)^2$.
Then, the gradient is
\[
  \nabla_\theta \ell(\theta, x,y) = \left( 2(\theta_1 x + \theta_2 -y) x, 2(\theta_1 x + \theta_2 - y)\right)
\]
For an instance $(x_i,y_i)$ with $y_i = ax_i+b+e_i$, this implies
\[
  \nabla_\theta \ell(\theta, x_i,y_i) = \left( 2((\theta_1-a) x_i + (\theta_2-b) - e_i) x_i, 2((\theta_1-a) x_i + (\theta_2-b) - e_i)\right)
\]
For sufficiently large datasets $Z$ where empirical loss approximates population loss, the gradient considered by \weightDpSgd{} will approximate
\begin{eqnarray*}
  \nabla_\theta \mathcal{L}(\theta,Z)
  &\approx&  \sum_{e\in \{10,\}} P_e(e) \int_0^1 \nabla_\theta \ell(\theta, x, ax+b+e) \hbox{d}x \\
  &=&  \sum_{e\in \{10,\}} P_e(e) \int_0^1 \left( 2((\theta_1-a) x + (\theta_2-b) - e) x, 2((\theta_1-a) x + (\theta_2-b) - e)\right) \hbox{d}x \\
  &=&  \int_0^1 \left( 2((\theta_1-a) x^2 + (\theta_2-b)x - x\mathbb{E}[e]), 2((\theta_1-a) x + (\theta_2-b) - \mathbb{E}[e])\right) \hbox{d}x \\
  &=&  \left( 2((\theta_1-a)/3 + (\theta_2-b)/2), 2((\theta_1-a)/2 + (\theta_2-b))\right)
\end{eqnarray*}
This gradient becomes zero if $\theta_1=a$ and $\theta_2=b$ as intended.

However, if we use gradient clipping with threshold $C=1$ as in \gradDpSgd{}, we get:
\begin{eqnarray*}
  {\tilde{g}}
  &\approx&  \sum_{e\in \{10,\}} P_e(e) \int_0^1 clip_1\left(\nabla_\theta \ell(\theta, x, ax+b+e)\right) \hbox{d}x \\
  &=&  \sum_{e\in \{10,\}} P_e(e) \int_0^1 clip_1\left(
       \left( 2((\theta_1-a) x + (\theta_2-b) - e) x, 2((\theta_1-a) x + (\theta_2-b) - e)\right)
      \right) \hbox{d}x \\
\end{eqnarray*}
While for a given $e$ for part of the population $(\theta_1-a)x+\theta_2-b$ may be small, for a fraction of the instances the gradients are clipped.  For the instances with $e=9$ this effect is stronger.  The result is that for $\theta_1=a$ and $\theta_2=b$ the average clipped gradient $\tilde{g}$ doesn't become zero anymore, in particular $\|\tilde{g}\|=0.7791$.  In fact, $\tilde{g}$ becomes zero for $\theta_1=a+0.01765$ and $\theta_2=b+0.94221$.  Figure \ref{fig:grad_clip_bias} illustrates this situation.

\begin{figure}[htb]
  \label{fig:grad_clip_bias}
  \caption{An example of gradient clipping causing bias, here the average gradient becomes zero at $(0,0)$ while the average clipped gradient is $0$ at another point, causing convergence of \gradDpSgd{} to that point rather than the correct one.}
  \begin{center}
    \includegraphics[width=0.5\linewidth]{linreg_cg_norm.png}
    \end{center}
\end{figure}




% \section{Related work}
%
%
% This section provides some additional references next to those already in Section \ref{sec:related}.
%
% \subsection{Robustness for privacy}
% To address the computational challenges, robustness certification methods can be employed for Lipschitz estimation. Techniques like LipSDP \cite{fazlyab_efficient_2019} and Fast-Lip \cite{weng_towards_2018} are useful in estimating $L_k$, eliminating the need to compute matrix norms for both per-layer Lipschitz values and input bounds propagation. Similarly, training approaches for networks with guaranteed robustness, such as the LP-based method \cite{wong_provable_2018}, ensure the network remains Lipschitz constrained without requiring spectral regularization.
%
% Observe that techniques such as LipSDP offer Lipschitz value calculations based on decision variables determined either at the neuron level (as in LipSDP-Neuron) or at the layer level (as in LipSDP-Layer). This allows for the adjustment of the added privacy noise to the specified level. While using LipSDP-Neuron might offer finer granularity in scaling the DP noise compared to our approach, it introduces additional computational overhead.
%
% \subsection{Estimating Lipschitz values of neural networks}
% Three main techniques dominate the field when it comes to estimate the Lipschitz value of neural networks: automatic differentiation-based estimation \cite{scaman_lipschitz_2019}, robustness certification methods \cite{fazlyab_efficient_2019,weng_towards_2018}, and probabilistic estimation strategies \cite{couellan_probabilistic_2021,pichardie_probabilistic_2020}.
%
% Our emphasis in this context is on automatic differentiation, primarily because it hinges on the relationship between \(\lossPartialInput{} \) and \( \lossPartialWeight{} \).
