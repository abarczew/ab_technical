\newcommand{\dspace}{\mathcal{Z}}
\newcommand{\dsetspace}{\mathcal{Z}^*}
\newcommand{\dset}{Z}
\newcommand{\dsetDistr}{P_Z}
\newcommand{\dseta}{{Z_1}}
\newcommand{\dsetb}{{Z_2}}
\newcommand{\adjdsets}{{\mathcal{Z}^*_\sim}}
\newcommand{\exz}{z}
\newcommand{\exx}{x}
\newcommand{\exy}{y}
\newcommand{\xspace}{{\mathcal{X}}}
\newcommand{\yspace}{{\mathcal{Y}}}
\newcommand{\hyspace}{{\hat{\mathcal{Y}}}}
\newcommand{\Fll}{{F^{ll}}}
\newcommand{\sLipschitz}{{L^g}}
\newcommand{\vLipschitz}{L}
\newcommand{\thetat}{{{\tilde{\theta}}^{(t)}}}
\newcommand{\thetaT}{{{\tilde{\theta}}^{(T)}}}
\newcommand{\thetaone}{{{\tilde{\theta}^{(1)}}}}
\newcommand{\thetatsucc}{{{\tilde{\theta}}^{(t+1)}}}
\newcommand{\noiset}{{b^{(t)}}}
\newcommand{\gradt}{{g^{(t)}}}
\newcommand{\gradtv}{{g^{(t)}_v}}
\newcommand{\batchset}{{V}}
\newcommand{\batchi}{{v_i}}
\newcommand{\batchsize}{s}
\newcommand{\fnnFunc}{f_\theta}
\newcommand{\layerFunc}[1]{f^{({#1})}_{\theta_{{#1}}}}
\newcommand{\lossPartialInput}[1]{\frac{\partial\mathcal{L}_{{#1}}}{\partial x_{{#1}}}}
\newcommand{\lossPartialWeight}[1]{\frac{\partial\mathcal{L}_{#1}}{\partial \theta_{{#1}}}}
\newcommand{\layerPartialInput}[1]{\frac{\partial \layerFunc{{#1}}}{\partial x_{{#1}}}}
\newcommand{\layerPartialWeight}[1]{\frac{\partial \layerFunc{{#1}}}{\partial \theta_{{#1}}}}
\newcommand{\thetaSpace}{\Theta}
\newcommand{\thetaSpaceLeC}{\Theta_{\le C}}
\newcommand{\thetaSpaceEqC}{\Theta_{=C}}
\newcommand{\sensitivity}{s_2}

\section{Preliminaries and background}\label{sec:prelim}
In this section, we briefly review differential privacy, empirical risk minimization (ERM) and differentially private stochastic gradient descent (\gradDpSgd).

We will denote the space of all possible instances by $\dspace$ and the space of all possible datasets by $\dsetspace$.  We will denote by $[N]=\{1\ldots N\}$ the set of the $N$ smallest positive integers.

\subsection{Differential Privacy}

An algorithm is differentially private if even an adversary who knows all but one instances of a dataset can't distinguish from the output of the algorithm the last instance in the dataset.  More formally:

\begin{definition}[adjacent datasets]
  We say two datasets $\dseta,\dsetb \in \dsetspace$ are adjacent, denoted $\dseta\sim \dsetb$, if they differ in at most one element.  We denote by $\adjdsets$ the space of all pairs of adjacent datasets.
\end{definition}

\begin{definition}[differential privacy \cite{dwork_algorithmic_2013}]
  Let $\epsilon>0$ and $\delta>0$.  Let $\mathcal{A}:\dsetspace\to\mathcal{O}$ be a randomized algorithm taking as input datasets from $\dsetspace$.   The algorithm $\mathcal{A}$ is $(\epsilon,\delta)$-differentially private ($(\epsilon,\delta)$-DP) if for every pair of adjacent datasets $(\dseta,\dsetb)\in\adjdsets$, and for every subset $S\subseteq \mathcal{O}$ of possible outputs of $\mathcal{A}$, $P(\mathcal{A}(\dseta)\subseteq S) \le e^\epsilon P(\mathcal{A}(\dsetb)\subseteq S)+\delta$.  If $\delta=0$ we also say that $\mathcal{A}$ is $\epsilon$-DP.
\end{definition}
If the output of an algorithm $\mathcal{A}$ is a real number or a vector, it can be privately released thanks to differential privacy mechanisms such as the Laplace mechanism or the Gaussian mechanism \cite{dwork_calibrating_nodate}.  While our ideas are more generally applicable, in this paper we will focus on the Gaussian mechanism as it leads to simplier derivations.  In particular, the Gaussian mechanism adds Gaussian noise to a number or vector which depends on its sensitivity on the input.
\begin{definition}[sensitivity]
  \label{def:sensitivity}
  The $\ell_2$-sensitivity of a function $f:\dspace\to\mathbb{R}^p$ is
  \[\sensitivity(f) =\max_{\dseta,\dsetb\in\adjdsets} \|f(\dseta) - f(\dsetb)\|_2 \]
\end{definition}

\begin{lemma}[Gaussian mechanism]
  \label{lem:gaussmech}
  Let $f:\dspace\to\mathbb{R}^p$ be a function.  The Gaussian mechanism transforms $f$ into ${\hat{f}}$ with ${\hat{f}}(\dset) = f(\dset) + b$ where $b\sim \mathcal{N}(0,\sigma^2 I_p)\in\mathbb{R}^p$ is Gaussian distributed noise.  If the variance satisfies $\sigma^2 \ge 2\ln(1.25/\delta)(\sensitivity(f))^2/\epsilon^2$, then ${\hat{f}}$ is $(\epsilon,\delta)$-DP.
\end{lemma}

%the output of $\mathcal{A}$ before releasing it. The scale of the variance of the noise is calibrated to the sensitivity of $\mathcal{A}$ [WIP]

\subsection{Empirical risk minimization}
\label{sec:erm}
Unless made explicit otherwise we will consider databases $\dset=\{\exz_i\}_{i=1}^n$ containing $n$ instances $\exz_i=(\exx_i,\exy_i)\in\xspace\times \yspace$ with $\xspace=\mathbb{R}^p$ and $\yspace=\{0,1\}$ sampled identically and independently (i.i.d.) from an unknown distribution on $\dspace$. We are trying to build a model $f_\theta: \xspace \rightarrow \hyspace$ (with $\hyspace\subseteq\mathbb{R}$) parameterized by $\theta \in \Theta \subseteq \mathbb{R}^p$, so it minimizes the expected loss $\mathcal{L}(\theta)=\mathbb{E}_{z}[\mathcal{L}(\theta ; \exz)]$, where $\mathcal{L}(\theta; \exz)=\ell(f_\theta(\exx),\exy)$ is the loss of the model $f_\theta$ on data point $\exz$.
One can approximate $\mathcal{L}(\theta)$ by
\[
\hat{R}(\theta ; \dset)=\frac{1}{n} \sum_{i=1}^n \mathcal{L}\left(\theta ; \exz_i\right)=\frac{1}{n}\sum_{i=1}^n \ell(f_\theta(x_i), y_i),
\] the empirical risk of model $f_\theta$.  Empirical Risk Minimization (ERM) then minimizes an objective function $F(\theta,\dset)$ which adds to this empirical risk a regularization term $\psi(\theta)$ to find an estimate ${\hat{\theta}}$ of the model parameters:
%and $\psi(\theta)$, penalty on model parameters (e.g., $\L_1$ norm).

%\begin{definition}[Empirical Risk Minimization (ERM)]
\begin{displaymath}
\hat{\theta} \in \underset{\theta \in \Theta}{\arg \min }\  F(\theta ; \dset):=\hat{R}(\theta ; \dset)+\gamma \psi(\theta)
\end{displaymath}
where $\gamma \geq 0$ is a trade-off hyperparameter.
%\end{definition}

\paragraph*{Feed forward neural networks}
An important and easy to analyze class of neural networks are the feed forward networks (FNN).  A FNN is a direct acyclic graph where connections between nodes don't form cycles.

\begin{definition}\label{def:fnn}
  A FNN $f_\theta: \mathbb{R}^n \to \mathbb{R}^m$ is a function which can be expressed as
  \[
    \fnnFunc = \layerFunc{K} \circ \ldots \circ \layerFunc{1}
  \]
  where $\layerFunc{k} : \mathbb{R}^{n_{k}} \to \mathbb{R}^{n_{k+1}}$. $\layerFunc{k}$ is the $k$-th layer function parameterized by $\theta_k$ with input $x_k$ and output $x_{k+1}$ for $1\le k\le K$.
 %$\layerFunc{k}(\theta_k, x_k)$ by $x_{k+1}$.
  Here, $\theta=(\theta_1 \ldots \theta_K)$, $n=n_1$ and $m=n_{K+1}$.
\end{definition}
Common layers include fully connected layers, convolutional layers and activation layers. Parameters of the first two correspond to weight and bias matrices, $\theta_k = (W_k, B_k)$, while activation layers have no parameter, $\theta_k = ()$.
%\paragraph*{Multi-Layer perceptrons}
%A popular and easy to analyze deep learning model is the multi-layer perceptron (MLP).  We will use it in this paper to illustrate our idea.
%\begin{definition}\label{def:mlp}
%  A $K$-layer Perceptron $f_{M L P}: \mathbb{R}^n \rightarrow \mathbb{R}^m$ is the function
%  \begin{align*}
%    f_{M L P}(x)= \rho_K \circ P_K \circ \rho_{K-1} \circ \cdots \circ \rho_1 \circ P_1(x),
%  \end{align*}
%  where $P_k: x \mapsto M_k x+b_k$ is an affine function and $\rho_k: x \mapsto\left(h_k\left(x_i\right)\right)_{i \in [ 1, n_k ]}$ is a non-linear (component-wise) activation function.  $f_{MLP}$ is parameterized by a tuple of matrices $\theta = (M_k)_{k=1}^K$.
%\end{definition}
%  We can then consider the empirical loss function
%  \[
%    {\hat R}(\theta; Z) = \jancolor{\frac{1}{n}} \sum_{i=1}^n \mathcal{L}(\theta; z_i) = \jancolor{\frac{1}{n}} \sum_{i=1}^n \ell(f_\theta(x_i), y_i)
%  \]
  %\begin{align*}
%  {\hat R}(\theta; Z) = \sum_{i=1}^n \mathcal{L}(\theta; z_i) = \sum_{i=1}^n \ell(f_{M L P}(\theta, x_i), y_i)
%\end{align*}
%  with $\ell$ an error cost function e.g., mean squared error (MSE) or cross-entropy (CE).

\subsection{Stochastic gradient descent}

To minimize $F(\theta,\dset)$,
 one can use gradient descent, i.e., iteratively for a number of time steps $t=1\ldots T$ one computes a gradient $\gradt = \nabla F(\thetat,\dset)$ on the current model $\thetat$ and updates the model setting $\thetatsucc=\thetat - \eta(t) \gradt$ where $\eta(t)$ is a learning rate.  Stochastic gradient descent (SGD) introduces some randomness and avoids the need to recompute all gradients in each iteration by sampling in each iteration a batch $\batchset \subseteq Z$ and computing an approximate gradient ${\hat{g}}_t = \frac{1}{|\batchset|} \left(\sum_{i=1}^{|\batchset|} \nabla \mathcal{L}(\thetat,\batchi) + \noiset\right)+\gamma\nabla\psi(\theta)$.



%To be more concrete, in each iteration of the SGD algorithm, the data owners and the Aggregator collaboratively compute a noisy gradient ${\hat{G}}_t = \frac{1}{\batchsize} \left(\sum_{i=1}^{\batchsize} \nabla \mathcal{L}(\thetat,\batchi) + \noiset\right)+\gamma\nabla\psi(\theta)$ over a sample $\batchset=\{\batchi\}_{i=1}^{\batchsize}$  of the data set where $\noiset$ is appropriate noise.


 To avoid leaking sensitive information, \cite{abadi_deep_2016} proposes to add noise to the gradients.   Determining good values for the scale of this noise
 %in the $b_t$ variables
 has been the topic of several studies.  One simple strategy starts by assuming an upper bound for the norm of the gradient.  Let us first define Lipschitz functions:

\begin{definition}[Lipschitz function]
  Let $\sLipschitz>0 .$ A function $f$ is $\sLipschitz$-Lipschitz with respect to some norm $\|\cdot\|$ if for all $\theta, \theta^{\prime} \in \Theta$ there holds
  $\left\|f(\theta)-f\left(\theta^{\prime}\right)\right\| \leq \sLipschitz\left\|\theta-\theta^{\prime}\right\|.$
  If $f$ is differentiable and $\|\cdot\|=\|\cdot\|_2$, the above property is equivalent to:
  $$
  \|\nabla f(\theta)\|_2 \leq \sLipschitz, \quad \forall \theta \in \Theta
  $$
  We call the smallest value $\sLipschitz$ for which $f$ is $\sLipschitz$-Lipschitz the Lipschitz value of $f$.
\end{definition}

Then, from the model one can derive a constant $\sLipschitz$ such that the objective function is $\sLipschitz$-Lipschitz, while knowing bounds on the data next allows for computing a bound on the sensitivity of the gradient.  Once one knows the sensitivity, one can determine the noise to be added from the privacy parameters as in Lemma \ref{lem:gaussmech}.
The classic \gradDpSgd{} algorithm \cite{abadi_deep_2016}, which we recall in Algorithm \ref{alg:grad-dpsgd} in Appendix \ref{sec:grad-dpsgd} for completeness, clips the gradient of each instance to a maximum value $C$ (i.e., scales down the gradient if its norm is above $C$) and then adds noise based on this maximal norm $C$.
\newcommand{\clip}[2]{\hbox{clip}_{{#1}}\left({#2}\right)}
\[
  \tilde{g}_t = \frac{1}{|\batchset|} \left(\sum_{i=1}^{|\batchset|}  \clip{C}{\nabla_{\tilde{\theta}}\mathcal{L}(f(x_i; \tilde{\theta}))} + b_t\right) + \gamma \nabla\psi(\theta)
\]
where $b_t$ is appropriate noise and where
\[
  \clip{C}{v} = v.\min\left(1,\frac{C}{\|v\|}\right).
  \]


\subsection{Regularization}

Several papers \cite{ioffe_batch_2015,Wu2018GroupN} have pointed out that regularization can help to improve the performance of stochastic gradient descent.  Although batch normalization \cite{ioffe_batch_2015} does not provide protection against privacy leakage, group normalization \cite{Wu2018GroupN} has the potential to do so \cite{de2022unlocking}.  \cite{de2022unlocking} combines regularization with \gradDpSgd{}, the algorithm to which we propose an improvement in the current paper.  Our experiments suggest that the strategy of \cite{de2022unlocking} most relevant to our work is the so-called group normalization.  Group normalization is a technique adding specific layers, called group normalization layers, to the network.  Making abstraction of some elements specific to image datasets, we can formalize it as follows.

% For every layer $k$ of a FNN, let $G_k$ be a partitioning of the components of the input vector $x_k$ to that layer $k$.  If layer $k$ is not a group normalization layer, then $G_k= \bigl\{\{1 \ldots |x_k|\}\bigr\} $ just contains one partition class with all components of $x_k$.  If
\newcommand{\set}[2]{x_{#1}^{({#2})}}
\newcommand{\setCard}[2]{\left|{x_{#1}^{({#2})}}\right|}
\newcommand{\grpidx}{q}

For a vector $v$, we will denote the dimension of $v$ by $|v|$, i.e., $v\in\mathbb{R}^v$.  We will denote by $[n]$ the set $\{1 \ldots n\}$ of the first positive integers.

If the $k$-th layer is a normalization layer, then there holds $|x_k|=|x_{k+1}|$.
Moreover, the structure of the normalization layer defines a partitioning $\Gamma_k=\{\Gamma_{k,1} \ldots \Gamma_{k,|G|} \}$ of $[|x_k|]$, i.e., a partitioning of the components of $x_k$.  The components of $x_k$ and $x_{k+1}$ are then grouped, and we define $x_k^{(k:\grpidx)} = \left(x_{k,j}\right)_{j\in \Gamma_{k,\grpidx}}$, i.e., $x_k^{(k:\grpidx)}$ is a subvector containing a group of components.  Similarly, $x_{k+1}^{(k:\grpidx)} =  \left(x_{k+1,j}\right)_{j\in \Gamma_{k,\grpidx}}$.
Then, %$\layerFunc{k}$
the $k$-th layer performs the following operation:
\begin{equation}\label{def:normalization}
  x_{k+1}^{(k:\grpidx)}=\layerFunc{k}(x_k^{(k:\grpidx)})=\frac{1}{\sigma^{(k:\grpidx)}}\left(x_k^{(k:\grpidx)}-\mu^{(k:\grpidx)}\right),
\end{equation}
(but note we will adapt this in Eq \eqref{eq:safe.group.normalization}) where
%$\grpidx$ is the index of the partition $\set{k}{\grpidx}$ of pixels or features of $x_k$ in which the mean $\mu^{(\grpidx)}$ and standard deviation (std) $\sigma^{(\grpidx)}$ are computed:

\begin{eqnarray*}
  \mu^{(k:\grpidx)} &=& \frac{1}{|\Gamma_{k,\grpidx}|} \sum_{j=1}^{|\Gamma_{k,\grpidx}|} x_{k,j},\\
  \sigma^{(k:\grpidx)} &=& \left(\frac{1}{|\Gamma_{k,\grpidx}|} \sum_{j=1}^{|\Gamma_{k,\grpidx}|} \left(x_{k,j} - \mu^{(k:\grpidx)}\right)^2+\kappa\right)^{1/2},
\end{eqnarray*}
with $\kappa$ a small constant. %and $\setCard{k}{i}$ is the size of the $i$-th group.

Various feature normalization methods primarily vary in their definitions of the partition of features $\Gamma_{k,\grpidx}$.






% Here is the formal definition copied from \cite{Wu2018GroupN}:
% A normalization layer performs the following operation:
% $$
% \hat{x}_i=\frac{1}{\sigma_i}\left(x_i-\mu_i\right) .
% $$
% Here $x$ is the feature computed by a layer, and $i$ is an index. In the case of $2 \mathrm{D}$ images, $i=\left(i_N, i_C, i_H, i_W\right)$ is a $4 \mathrm{D}$ vector indexing the features in $(N, C, H, W)$ order, where $N$ is the batch axis, $C$ is the channel axis, and $H$ and $W$ are the spatial height and width axes.
% $\mu$ and $\sigma$ in (1) are the mean and standard deviation (std) computed by:
% $$
% \mu_i=\frac{1}{m} \sum_{k \in \mathcal{S}_i} x_k, \quad \sigma_i=\sqrt{\frac{1}{m} \sum_{k \in \mathcal{S}_i}\left(x_k-\mu_i\right)^2+\epsilon},
% $$
% with $\epsilon$ as a small constant. $\mathcal{S}_i$ is the set of pixels in which the mean and std are computed, and $m$ is the size of this set.
%
% Formally, a Group Norm layer computes $\mu$ and $\sigma$ in a set $\mathcal{S}_i$ defined as:
% $$
% \mathcal{S}_i=\left\{k \mid k_N=i_N,\left\lfloor\frac{k_C}{C / G}\right\rfloor=\left\lfloor\frac{i_C}{C / G}\right\rfloor\right\} .
% $$
%
% Here $G$ is the number of groups, which is a pre-defined hyper-parameter ( $G=32$ by default). $C / G$ is the number of channels per group. $\lfloor . \rfloor$ is the floor operation, and $"\left\lfloor\frac{k_C}{C / G}\right\rfloor=\left\lfloor\frac{i_C}{C / G}\right\rfloor "$ means that the indexes $i$ and $k$ are in the same group of channels, assuming each group of channels are stored in a sequential order along the $C$ axis.



%
%   x_k -> group normalization layer -> x_{k+1} -> dense layer f_{k+1} -> x_{k+2}
%
%
% x_k     = x_{k,G_{k,1}}   ... x_{k,G_{k,|G_k|}}
% x_{k+1} = x_{k+1,G_{k,1}} ... x_{k+1,G_{k,|G_k|}}
%
%
% ============================================================================
%
% x_k \in R^{channel * height * width}
%
% Features_k = [channel] \times [pixels]
% Groups_k = \{Group_{k,i} \}_{i=1}^{|Groups_k|}
% Group_{k,i} = SomeChannels \times [pixels]
%
% ===========================================================================
%
%
% hidden units_k = x_k   --> N_k
%
% dense layer:
% weights: N_k * N_k+1
%
% hidden units_k+1 = x_{k+1} ---> N_k+1
