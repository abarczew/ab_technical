\section{Experimental results}\label{sec:experiment}
In this section, we conduct an empirical evaluation of our approach.

\subsection{Experimental setup}
\label{sec:exp.setup}

We consider the following experimental questions:
\begin{itemize}[noitemsep,topsep=0pt]
\item[Q1] How does \weightDpSgd{}, our proposed technique, compare against the conventional \gradDpSgd{} as introduced by \cite{abadi_deep_2016}?
\item[Q2] What is the effect of allowing $\|\theta_k\| < C$ rather than normalizing $\|\theta_k\|$ to $C$?  This question seems relevant given that some authors
(e.g., \cite{bethune_pay_2022}\cite{miyato2018spectral}) also suggest to consider networks which constant gradient norm rather than maximal gradient norm, i.e., roughly with $\theta$ in
%papers consider only models from
$\thetaSpaceEqC$ rather than $\thetaSpaceLeC$. % \cite{bethune_pay_2022}.
\end{itemize}


% \subsection{Evaluation and training}\label{subsec:bench}
\paragraph{Implementation}
% For comparison fairness, we replicated experiments using the \gradDpSgd{} methodology from \cite{abadi_deep_2016}, as instantiated in
We implemented both the \gradDpSgd{} and \weightDpSgd{} methods to ensure that comparisons %between \weightDpSgd{} and \gradDpSgd{}
were made under consistent model structures and preprocessing conditions.
% For added context, we also referenced performance metrics as detailed in prior works like \cite{papernot,..}, wherever available.
\\
To answer question Q2, we also implemented \globalWeightDpSgd{}, a version of \weightDpSgd{} limited to networks whose weight norms are fixed, i.e., $\forall k:\|\theta_k\|=C$, obtained by setting $\thetanorm_k\gets C$ in Line \ref{ln:setThetaNorm} in Algorithm \ref{alg:weight-dpsgd}.
\\
\textbf{Toolkit.} We offer an open-source Python toolkit for implementing \weightDpSgd{} and \gradDpSgd{} on any feed-forward model structure, building on the Opacus \cite{opacus} and PyTorch \cite{pytorch} libraries.
See Appendix \ref{sec:lib-LipDpSgd} for more details.


\textbf{Hyperparameters.}
We selected a number of hyperparameters to tune for our experiments, aiming at making a fair comparison between the studied techniques while minimizing the distractions of potential orthogonal improvements.  To optimize these hyperparameters, we used Bayesian optimization \cite{balandat_botorch_2020}.
Appendix \ref{sec:app.exp.hyperparameters} provides a detailed discussion.


\textbf{Datasets and models.} We carried out experiments
on both tabular data sets and data sets with image data.
%in two phases. In the initial phase, our focus was on widely-recognized
First, we consider a collection of 10 real-world tabular datasets (names and citations in Table \ref{tab:auc}).  For these, we trained multi-layer perceptrons (MLP).
%In the subsequent phase, we benchmarked the performance of multi-layer perceptrons (MLP) on a collection of 10 real-world tabular datasets.
Here, due to the inherent imbalance in many of these datasets, we report the AUC, the area under the ROC curve, as opposed to accuracy, ensuring a more informative performance metric. A comprehensive list of model-dataset combinations is available in the supplementary material \cref{tab:data}.

Second, the image datasets include MNIST \cite{deng2012mnist}, Fashion-MNIST \cite{xiao_fashion-mnist_2017} and CIFAR-10 \cite{cifar}. For these, we trained convolutional neural networks (CNN).
% , recording accuracy across a range of $ \epsilon $ values.
Given that accuracy is a commonly adopted metric for these datasets, we opted for it to facilitate easy comparisons with prior research.



%\textbf{Toolkit.} We offer an open-source Python toolkit for implementing \weightDpSgd{} on any sequential model structure. This toolkit serves as an interface for both the Opacus \cite{opacus} and PyTorch \cite{pytorch} libraries. Drawing inspiration from Opacus, our library introduces the `LipPrivacyEngine` class to facilitate private training. This class is dependent on two main components: the `DataLoader`, which utilizes Poisson sampling to harness the advantages of privacy amplification \cite{kasiviswanathan_what_2010}, and the `Optimizer`, responsible for sensitivity calculation, differential privacy noise addition, and parameter normalization at each training step.

\textbf{Infrastructure.} All experiments were orchestrated across dual Tesla P100 GPU platforms (12GB capacity), operating under CUDA version 10, with a 62GB RAM provision for Fashion-MNIST and CIFAR-10. Remaining experiments were performed  on an E5-2696V2 Processor setup, equipped with 8 vCPUs and a 52GB RAM cache. The total runtime of the experiments was approximately 50 hours, which corresponds to an estimated carbon emission of $1.96$ kg %, as reported in
\cite{lacoste2019quantifying}.

%We first focus on the MNIST, Fashion-MNIST and CIFAR-10 datasets and then summarize the results for the other $10$ datasets.
More details on the experimental setup and an analysis of the complexity can be found in Appendix \ref{sec:app.exp}.

\begin{figure*}[t]
  \centering
  \mbox{
    \subfigure[MNIST\label{fig:mnist_perf}]{\includegraphics[width=0.32\linewidth]{plot/logscale09/results.png}}\quad
    \subfigure[Fashion-MNIST\label{fig:FashionMNIST_perf}]{\includegraphics[width=0.32\linewidth]{plot/monovsfront/results.png}}\quad
    \subfigure[CIFAR-10\label{fig:cifar_perf}]{\includegraphics[width=0.32\linewidth]{plot/logscale11/results.png}}
  }

  \caption{Accuracy results for the MNIST (\ref{fig:mnist_perf}), Fashion-MNIST (\ref{fig:FashionMNIST_perf}), and CIFAR-10 (\ref{fig:cifar_perf}) datasets, comparing \weightDpSgd{} (in blue) and \gradDpSgd{} (in red). Vertical lines represent the standard error of the mean. All experiments were performed with a fixed \(\delta = 10^{-5}\). Detailed model specifications and hyperparameters for each dataset are available in the supplementary material.}
  \label{fig:big_perf}
\end{figure*}

\subsection{Results}
Table \ref{tab:auc} shows our results on the tabular data, comparing \gradDpSgd{} with \weightDpSgd{} and \globalWeightDpSgd{}.

Figure \ref{fig:big_perf} shows our results for the image data sets.
Due to our Bayesian optimization approach, not for all hyperparameter combinations an experiment is run, hence as the plots only show the pareto front (of privacy cost $\epsilon$ and accuracy), the plotted data points are not equidistant.


\begin{table*}
  \caption{AUC per dataset and method at $\epsilon=1$ and $\delta=1/n$, in bold the best result and underlined when the difference with best result is not statistically significant at a level of confidence of 5\%.}
  \label{tab:auc}
  \centering
  \begin{tabular}{lrrr}
    \toprule
    Methods &  \gradDpSgd{} &  \weightDpSgd{} & \globalWeightDpSgd{} \\
    Datasets (\#instances $n$ $\times$ \#features $p$)     &                &                 &               \\
    \midrule
    Adult Income (48842x14) \cite{misc_adult_2}    &            0.563 &                                        \textbf{0.732} & 0.674 \\
    Android   (29333x87)\cite{misc_naticusdroid_(android_permissions)_dataset_722}       & 0.773 &             \underline{0.826} & \textbf{0.866}  \\
    Breast Cancer (569x32) \cite{misc_breast_cancer_wisconsin_(diagnostic)_17}   &            0.865 &          \textbf{0.925} & 0.801\\
    Default Credit (30000x24) \cite{misc_default_of_credit_card_clients_350}  &             0.580 &            \textbf{0.595} & \underline{0.586}\\
    Dropout       (4424x36) \cite{misc_predict_students'_dropout_and_academic_success_697}   &       0.608 &   \textbf{0.636} & 0.616\\
    German Credit (1000x20) \cite{misc_statlog_(german_credit_data)_144}          &             0.549 &        \textbf{0.615} & 0.604\\
    Nursery        (12960x8) \cite{misc_nursery_76} &            0.750 &                                       \textbf{0.890} & 0.825\\
    Patient Survival (85519x104) \cite{mitisha_agarwal_2021} &            \textbf{0.681} &                     \underline{0.677}& \underline{0.680}\\
    Thyroid  (7200x5) \cite{misc_thyroid_disease_102}       &            0.587 &                               \textbf{0.597} & 0.598\\
    Yeast  (1484x8) \cite{misc_yeast_110}         &            0.591 &                                         \textbf{0.631} & 0.623\\
    \bottomrule
  \end{tabular}
\end{table*}

\subsection{Discussion}

\textbf{MLP.} As demonstrated in \cref{tab:auc}, \weightDpSgd{} consistently achieves better performance in terms of AUC compared to \gradDpSgd, with the exception of the Patient Survival dataset where the difference is not statistically significant. This observation holds true across datasets of varying numbers of instances and features, as well as for tasks involving imbalanced datasets such as the Dropout or Default Credit datasets.
This table also shows the performance one can obtain by limiting the networks to Lipschitz networks whose norm of weights equals a given constant.  The results of this approach are a bit inferior, while still outperforming \gradDpSgd{}.


For an overall conclusion, we perform a Wilcoxon Signed-rank test, at a confidence level of $5$\%, on $10$ measures of AUC for each dataset between the \gradDpSgd{} based on the gradient clipping and the \weightDpSgd{} based on our method, results are shown in \cref{tab:auc}.

\textbf{CNN.} In \cref{fig:big_perf}, \weightDpSgd{} exhibits performance either on par with or superior to \gradDpSgd{} for MNIST, Fashion-MNIST, and CIFAR-10.
%The proven efficacy of \gradDpSgd{} on these tasks reinforces the competitive edge of \weightDpSgd, especially on traditional network structures without the need for intricate feature engineering or other enhancements.



%As mentioned in \cref{sec:exp.setup}, we ensured the use of identical base models for comparison. However, it's noteworthy that \weightDpSgd{} offers the flexibility to employ more complex models compared to \gradDpSgd. This is because \weightDpSgd{} eliminates the need for per-sample gradient clipping, instead emphasizing weight normalization at the averaged gradient level, as detailed in \cref{alg:weight-dpsgd}. As a result, this allows for the use of larger batch sizes and, potentially, more intricate models. This capability could lead to enhanced performance on datasets that necessitate such advanced models, for instance, CIFAR-10.



%In summary, while \weightDpSgd{} holds its own against \gradDpSgd{} on conventional datasets, its true added value is evident on real-world and more challenging tasks. Furthermore, \weightDpSgd{} paves the way for the exploration of more sophisticated models.

In summary, we can conclude that we can answer to our experimental questions that \weightDpSgd{} outperforms \gradDpSgd{} on both tabular data sets with MLP and image data sets with CNN.  Moreover, it is beneficial to not normalize the norm of the weight vector $\theta$ to a fixed value but to exploit cases where it becomes smaller.


%\textbf{Scaling strategies. } By default, we used the smallest available parameter norm at each step, denoted as $\thetanorm$, where $\thetanorm_k = \min(C, \|\theta\|_2)$, as outlined in \cref{alg:weight-dpsgd}. This "local sensitivity" scaling strategy differs from the "global sensitivity" approach, which always sets $\thetanorm_k = C$, potentially reducing runtime but not maximizing the current position of $\theta$ in the search. Testing on tabular datasets, the local method generally outperforms the global strategy at a 5\% confidence level, except for three datasets where differences are not significant. Detailed results are in Appendix \ref{tab:auc-localglobal}.
