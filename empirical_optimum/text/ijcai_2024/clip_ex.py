
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

def grad_abex(adiff,bdiff,e,x) :
    q = adiff*x+bdiff-e
    return np.array([2*q*x,2*q])

def grad_abex_clip(adiff,bdiff,e,x) :
    g = grad_abex(adiff,bdiff,e,x)
    C = 1
    g_norm = np.linalg.norm(g)
    if(g_norm > C):
        g = g*C/g_norm
    return g

def exp_grad_ab_clip(adiff,bdiff) :
    Nx = 1000
    s = 0
    for e_p in [(9,0.1),(-1,0.9)] :
        e = e_p[0]
        p = e_p[1]
        for xi in range(Nx):
            x = (xi+0.5)/Nx
            g = grad_abex_clip(adiff,bdiff,e,x)
            s = s + g*p/(1.0*Nx)
    return s


def exp_grad_ab(adiff,bdiff) :
    Nx = 10
    s = 0
    for e_p in [(9,0.1),(-1,0.9)] :
        e = e_p[0]
        p = e_p[1]
        for xi in range(Nx):
            x = (xi+0.5)/Nx
            g = grad_abex(adiff,bdiff,e,x)
            #print("e=",e,"  p=",p,"   g=",g,"  delta=",g*p/(1.0*Nx))
            s = s + g*p/(1.0*Nx)
    return s

def plot() :
    #v = [-20.0,-5.0,-2.0,-1.0,-0.5,0.0,0.5,1.0,2.0,5.0,20.0]
    vi = [(u/10)-3 for u in range(50)]
    vj = [(u/10)-3 for u in range(50)]
    f_wc = np.zeros([len(v)])
    f_gc = np.zeros([len(v)])
    #for i in range(len(v)):
    for j in range(len(v)):
        f_wc[j] = exp_grad_ab(v[j],0)[1]
        f_gc[j] = exp_grad_ab_clip(v[j],0)[1]
        print("j=",j,", v[j]=",v[j],", wc=",f_wc[j],", gc=",f_gc[j])
    return (f_wc,f_gc)
            

def find_cz() : # find clip zero
    vi = [((u-7)/100000)-0.01765 for u in range(15)]
    vj = [((u-7)/100000)-0.94221 for u in range(15)]
    fn = np.zeros([len(vi),len(vj)])
    f1 = np.zeros([len(vi),len(vj)])
    f2 = np.zeros([len(vi),len(vj)])
    for i in range(len(vi)):
        for j in range(len(vj)):
            g = exp_grad_ab_clip(vi[i],vj[j])
            f1[i,j] = g[0]
            f2[i,j] = g[1]
            fn[i,j] = np.linalg.norm(g)
    print(fn)
    return np.unravel_index(np.argmin(fn),fn.shape)


def plot_cz() : # find clip zero
    #vi = [((u-30)/20)-0.01765 for u in range(60)]
    #vj = [((u-30)/20)-0.94221 for u in range(60)]
    vi = np.arange(-0.5,0.1001,0.01)
    vj = np.arange(-1.1,0.1001,0.02)
    fn = np.zeros([len(vi),len(vj)])
    #f1 = np.zeros([len(vi),len(vj)])
    #f2 = np.zeros([len(vi),len(vj)])
    for i in range(len(vi)):
        for j in range(len(vj)):
            g = exp_grad_ab_clip(vi[i],vj[j])
            #f1[i,j] = g[0]
            #f2[i,j] = g[1]
            fn[i,j] = np.linalg.norm(g)
    X,Y = np.meshgrid(vi,vj)
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    ax.plot_surface(X, Y, fn, vmin=0, cmap=cm.Blues)
    plt.show()
    
    

