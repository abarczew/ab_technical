python search.py -e mnist -s  experiment -r 50
python search.py -e fashionmnist -s  experiment -r 50
python search.py -e cifar -s  experiment -r 50

python search.py -e android -s  experiment -r 50 --no-group
python search.py -e breast -s  experiment -r 50 --no-group
python search.py -e default_credit -s  experiment -r 50 --no-group
python search.py -e dropout -s  experiment -r 50 --no-group
python search.py -e german -s  experiment -r 50 --no-group
python search.py -e income -s  experiment -r 50 --no-group
python search.py -e nursery -s  experiment -r 50 --no-group

python measure.py -e mnist -s  experiment -r 50
python measure.py -e fashionmnist -s  experiment -r 50
python measure.py -e cifar -s  experiment -r 50

python measure.py -e android -s  experiment -r 10 --no-group
python measure.py -e breast -s  experiment -r 10 --no-group
python measure.py -e default_credit -s  experiment -r 10 --no-group
python measure.py -e dropout -s  experiment -r 10 --no-group
python measure.py -e german -s  experiment -r 10 --no-group
python measure.py -e income -s  experiment -r 10 --no-group
python measure.py -e nursery -s  experiment -r 10 --no-group
