\documentclass{cap2024}

% Recommended, but optional, packages for figures and better typesetting:
\usepackage{microtype}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{booktabs} % for professional tables
\usepackage{enumitem}

\usepackage{algorithm}
%\usepackage{algorithmic}
\usepackage{algpseudocode}

% hyperref makes hyperlinks in the resulting PDF.
% If your build breaks (sometimes temporarily if a hyperlink spans a page)
% please comment out the following usepackage line and replace
% \usepackage{icml2024} with \usepackage[nohyperref]{icml2024} above.
\usepackage{hyperref}


% Attempt to make hyperref and algorithmic work together better:
% \newcommand{\theHalgorithm}{\arabic{algorithm}}

% Use the following line for the initial blind version submitted for review:


% If accepted, instead use the following line for the camera-ready submission:
% \usepackage[accepted]{icml2024}

% For theorems and such
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{amsthm}
\usepackage{bbm}

% if you use cleveref..
\usepackage[capitalize,noabbrev]{cleveref}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% THEOREMS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \theoremstyle{plain}
% \newtheorem{theorem}{Theorem}[section]
% \newtheorem{proposition}[theorem]{Proposition}
% \newtheorem{lemma}[theorem]{Lemma}
% \newtheorem{corollary}[theorem]{Corollary}
% \theoremstyle{definition}
% \newtheorem{definition}[theorem]{Definition}
% \newtheorem{assumption}[theorem]{Assumption}
% \theoremstyle{remark}
% \newtheorem{remark}[theorem]{Remark}

\newcommand{\jancolor}[1]{\textcolor{blue}{{#1}}}
\newcommand{\antoinecolor}[1]{\textcolor{red}{{#1}}}
\newcommand{\janfoot}[1]{\jancolor{\footnote{\jancolor{{#1}}}}}
\newcommand{\antoinefoot}[1]{\textcolor{red}{\footnote{\textcolor{red}{{{#1}}}}}}

\newcommand{\dspace}{\mathcal{Z}}
\newcommand{\dsetspace}{\mathcal{Z}^*}
\newcommand{\dset}{Z}
\newcommand{\dsetDistr}{P_Z}
\newcommand{\dseta}{{Z_1}}
\newcommand{\dsetb}{{Z_2}}
\newcommand{\adjdsets}{{\mathcal{Z}^*_\sim}}
\newcommand{\exz}{{\rm{z}}}
\newcommand{\exx}{{\rm{x}}}
\newcommand{\exy}{{\rm{y}}}
\newcommand{\xspace}{{\mathcal{X}}}
\newcommand{\yspace}{{\mathcal{Y}}}
\newcommand{\hyspace}{{\hat{\mathcal{Y}}}}
\newcommand{\Fll}{{F^{ll}}}
\newcommand{\sLipschitz}{{L^g}}
\newcommand{\vLipschitz}{L}
\newcommand{\thetat}{{{\tilde{\theta}}^{(t)}}}
\newcommand{\thetaT}{{{\tilde{\theta}}^{(T)}}}
\newcommand{\thetaone}{{{\tilde{\theta}^{(1)}}}}
\newcommand{\thetatsucc}{{{\tilde{\theta}}^{(t+1)}}}
\newcommand{\noiset}{{b^{(t)}}}
\newcommand{\gradt}{{g^{(t)}}}
\newcommand{\gradtv}{{g^{(t)}_v}}
\newcommand{\batchset}{{V}}
\newcommand{\batchi}{{v_i}}
\newcommand{\batchsize}{s}
\newcommand{\fnnFunc}{f_\theta}
\newcommand{\layerFunc}[1]{f^{({#1})}_{\theta_{{#1}}}}
\newcommand{\lossPartialInput}[1]{\frac{\partial\mathcal{L}_{{#1}}}{\partial x_{{#1}}}}
\newcommand{\lossPartialWeight}[1]{\frac{\partial\mathcal{L}_{#1}}{\partial \theta_{{#1}}}}
\newcommand{\layerPartialInput}[1]{\frac{\partial \layerFunc{{#1}}}{\partial x_{{#1}}}}
\newcommand{\layerPartialWeight}[1]{\frac{\partial \layerFunc{{#1}}}{\partial \theta_{{#1}}}}
\newcommand{\thetaSpace}{\Theta}
\newcommand{\thetaSpaceLeC}{\Theta_{\le C}}
\newcommand{\thetaSpaceEqC}{\Theta_{=C}}
\newcommand{\sensitivity}{s_2}
\newcommand{\clip}[2]{\hbox{clip}_{{#1}}\left({#2}\right)}
\newcommand{\set}[2]{x_{#1}^{({#2})}}
\newcommand{\setCard}[2]{\left|{x_{#1}^{({#2})}}\right|}
\newcommand{\grpidx}{q}
\newcommand{\globalWeightDpSgd}{Fix-Lip-DP-SGD}
\newcommand{\weightDpSgd}{Lip-DP-SGD}
\newcommand{\gradDpSgd}{DP-SGD}

% Todonotes is useful during development; simply uncomment the next line
%    and comment out the line below the next line to turn off comments
%\usepackage[disable,textsize=tiny]{todonotes}
\usepackage[textsize=tiny]{todonotes}


\title{DP-SGD with weight clipping}
\author[1]{Antoine Barczewski}
\author[2]{Jan Ramon}
\affil[1]{Université Lille, Inria}
\affil[2]{Inria}

\begin{document}
\maketitle



% \input{subfiles/abstract.tex}
% \medskip
%
% \keywords{Machine Learning, Differential Privacy, Optimization}
%
% \input{subfiles/introduction.tex}
% \input{subfiles/preliminaries.tex}
\input{subfiles/approach.tex}
%\input{subfiles/implementation.tex}
% \input{subfiles/experimental_results.tex}
% \input{subfiles/related_work.tex}
% \input{subfiles/limitations.tex}
% \input{subfiles/conclusions.tex}

\bibliography{../biblio}
\bibliographystyle{apalike}
%
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % APPENDIX
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \newpage
% \appendix
% \onecolumn
% \input{subfiles/appendix.tex}

\end{document}


% This document was modified from the file originally made available by
% Pat Langley and Andrea Danyluk for ICML-2K. This version was created
% by Iain Murray in 2018, and modified by Alexandre Bouchard in
% 2019 and 2021 and by Csaba Szepesvari, Gang Niu and Sivan Sabato in 2022.
% Modified again in 2023 and 2024 by Sivan Sabato and Jonathan Scarlett.
% Previous contributors include Dan Roy, Lise Getoor and Tobias
% Scheffer, which was slightly modified from the 2010 version by
% Thorsten Joachims & Johannes Fuernkranz, slightly modified from the
% 2009 version by Kiri Wagstaff and Sam Roweis's 2008 version, which is
% slightly modified from Prasad Tadepalli's 2007 version which is a
% lightly changed version of the previous year's version by Andrew
% Moore, which was in turn edited from those of Kristian Kersting and
% Codrina Lauth. Alex Smola contributed to the algorithmic style files.
