from ax.service.ax_client import AxClient
import numpy as np
import pandas as pd
import scipy
from itertools import accumulate
from typing import List, Optional, Tuple

import torch
from torch.utils.data import DataLoader, Dataset, Subset, random_split
from torchvision import datasets, transforms

from sklearn.datasets import load_breast_cancer, fetch_california_housing, \
    load_diabetes
from sklearn.preprocessing import OneHotEncoder
from sklearn.datasets import fetch_openml

tabular_datasets = ['income', 'android', 'breast', 'default_credit',
                    'dropout', 'german', 'nursery', 'thyroid', 'yeast']
DATADIR = "data"


def load_vision_data(exp_name):
    if exp_name == 'mnist':
        train_valid_set, test_set = load_mnist()
    elif exp_name == 'fashionmnist':
        train_valid_set, test_set = load_fashionmnist()
    elif exp_name == 'cifar':
        train_valid_set, test_set = load_cifar()
    else:
        raise ValueError('Illicite dataset name')

    return train_valid_set, test_set


def load_tabular_data(exp_name):
    X, y = get_data(exp_name, sample_size=-1)
    dataset = MyDataset(X, np.array(y), 0.2,
                        standardize=-1,
                        input_norm=1)
    input_dim = dataset.input_dim
    class_num = dataset.class_num
    test_set_size = int(len(dataset) * 0.2)
    train_set_size = len(dataset) - test_set_size
    torch_seed = torch.Generator().manual_seed(42)
    train_valid_set, test_set = random_split(dataset,
                                             [train_set_size,
                                              test_set_size],
                                             generator=torch_seed)
    return train_valid_set, test_set, input_dim, class_num


def load_mnist(input_norm=1):
    # input_norm = 1
    # TODO: impact norm in transform
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.1307,), (0.3081,))
    ])
    train_valid_set = datasets.MNIST('../data', train=True, download=True,
                                     transform=transform)
    test_set = datasets.MNIST('../data', train=False,
                              transform=transform)
    return train_valid_set, test_set


def load_fashionmnist(input_norm=1):
    # input_norm = 1
    # TODO: impact norm in transform
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.286,), (0.353,))
    ])
    train_valid_set = datasets.FashionMNIST('../data',
                                            train=True,
                                            download=True,
                                            transform=transform)
    test_set = datasets.FashionMNIST('../data',
                                     train=False,
                                     transform=transform)
    return train_valid_set, test_set


def load_cifar(input_norm=1):
    # input_norm = 1
    # TODO: impact norm in transform
    # transform = VGG11_Weights.IMAGENET1K_V1.transforms()
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.4914, 0.4822, 0.4465),
                             (0.2023, 0.1994, 0.2010))
    ])
    train_valid_set = datasets.CIFAR10('../data',
                                       train=True,
                                       download=True,
                                       transform=transform)
    test_set = datasets.CIFAR10('../data',
                                train=False,
                                transform=transform)
    return train_valid_set, test_set


def get_partition_data_loaders(
    train_valid_set: Dataset,
    test_set: Dataset,
    downsample_pct: float = 0.5,
    train_pct: float = 0.8,
    batch_size: int = 128,
    num_workers: int = 0,
    deterministic_partitions: bool = False,
    downsample_pct_test: Optional[float] = None,
    pin_memory: bool = False
) -> Tuple[DataLoader, DataLoader, DataLoader]:
    """
    Helper function for partitioning training data into training and validation
     sets,
        downsampling data, and initializing DataLoaders for each partition.

    Args:
        train_valid_set: torch.dataset
        downsample_pct: the proportion of the dataset to use for training, and
            validation
        train_pct: the proportion of the downsampled data to use for training
        batch_size: how many samples per batch to load
        num_workers: number of workers (subprocesses) for loading data
        deterministic_partitions: whether to partition data in a deterministic
            fashion
        downsample_pct_test: the proportion of the dataset to use for test,
        default
            to be equal to downsample_pct

    Returns:
        DataLoader: training data
        DataLoader: validation data
        DataLoader: test data
    """
    # Partition into training/validation
    # pyre-ignore [6]
    downsampled_num_examples = int(downsample_pct * len(train_valid_set))
    n_train_examples = int(train_pct * downsampled_num_examples)
    n_valid_examples = downsampled_num_examples - n_train_examples
    train_set, valid_set, _ = split_dataset(
        dataset=train_valid_set,
        lengths=[
            n_train_examples,
            n_valid_examples,
            len(train_valid_set) - downsampled_num_examples,  # pyre-ignore [6]
        ],
        deterministic_partitions=deterministic_partitions,
    )
    if downsample_pct_test is None:
        downsample_pct_test = downsample_pct
    # pyre-ignore [6]
    downsampled_num_test_examples = int(downsample_pct_test * len(test_set))
    test_set, _ = split_dataset(
        test_set,
        lengths=[
            downsampled_num_test_examples,
            len(test_set) - downsampled_num_test_examples,  # pyre-ignore [6]
        ],
        deterministic_partitions=deterministic_partitions,
    )
    train_loader = DataLoader(
        train_set, batch_size=batch_size, shuffle=True,
        num_workers=num_workers, pin_memory=pin_memory
    )
    valid_loader = DataLoader(
        valid_set, batch_size=batch_size, shuffle=False,
        num_workers=num_workers, pin_memory=pin_memory
    )
    test_loader = DataLoader(
        test_set, batch_size=batch_size, shuffle=False,
        num_workers=num_workers, pin_memory=pin_memory
    )
    return train_loader, valid_loader, test_loader


def split_dataset(
    dataset: Dataset, lengths: List[int],
    deterministic_partitions: bool = False
) -> List[Dataset]:
    """
    Split a dataset either randomly or deterministically.

    Args:
        dataset: the dataset to split
        lengths: the lengths of each partition
        deterministic_partitions: deterministic_partitions: whether
            to partition data in a deterministic fashion

    Returns:
        List[Dataset]: split datasets
    """
    if deterministic_partitions:
        indices = list(range(sum(lengths)))
    else:
        indices = torch.randperm(sum(lengths)).tolist()
    return [
        Subset(dataset, indices[offset - length: offset])
        for offset, length in zip(accumulate(lengths), lengths)
    ]


def get_labels_and_class_counts(labels_list):
    '''
    Calculates the counts of all unique classes.
    '''
    labels = np.array(labels_list)
    _, class_counts = np.unique(labels, return_counts=True)
    return labels, class_counts


nb_classes = 10


class ImbalancedMNNIST(Dataset):
    def __init__(self, imbal_class_prop, root, train, download, transform):
        self.dataset = datasets.MNIST(
            root=root, train=train, download=download, transform=transform)
        self.train = train
        self.imbal_class_prop = imbal_class_prop
        self.idxs = self.resample()
        self.nb_classes = 10

    def get_labels_and_class_counts(self):
        return self.labels, self.imbal_class_counts

    def resample(self):
        '''
        Resample the indices to create an artificially imbalanced dataset.
        '''
        if self.train:
            targets, class_counts = get_labels_and_class_counts(
                self.dataset.targets)
        else:
            targets, class_counts = get_labels_and_class_counts(
                self.dataset.targets)
        # Get class indices for resampling
        class_indices = [np.where(targets == i)[0]
                         for i in range(nb_classes)]
        # Reduce class count by proportion
        self.imbal_class_counts = [
            int(count * prop)
            for count, prop in zip(class_counts, self.imbal_class_prop)
        ]
        # Get class indices for reduced class count
        idxs = []
        for c in range(nb_classes):
            imbal_class_count = self.imbal_class_counts[c]
            idxs.append(class_indices[c][:imbal_class_count])
        idxs = np.hstack(idxs)
        self.labels = targets[idxs]
        return idxs

    def __getitem__(self, index):
        img, target = self.dataset[self.idxs[index]]
        return img, target

    def __len__(self):
        return len(self.idxs)


def load_imbal_mnist(input_norm=1,
                     imbal_class_prop=np.hstack(([0.1] * 5, [1.0] * 5))):
    # input_norm = 1
    # TODO: impact norm in transform
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.1307,), (0.3081,))
    ])
    train_valid_set = ImbalancedMNNIST(imbal_class_prop,
                                       '../data',
                                       train=True, download=True,
                                       transform=transform)
    test_set = ImbalancedMNNIST(imbal_class_prop,
                                '../data',
                                train=False,
                                download=True,
                                transform=transform)
    return train_valid_set, test_set


class MyDataset(Dataset):
    def __init__(self, X, y, test_size,
                 task='classification', standardize=False, input_norm=None):
        if standardize:
            X = (X - X.min(axis=0)) / (X.max(axis=0) - X.min(axis=0))
            if task == 'regression':
                y = (y - y.min(axis=0)) / (y.max(axis=0) - y.min(axis=0))
        if input_norm is not None:
            X = X / np.max(np.linalg.norm(X, axis=1)) * input_norm
        if scipy.sparse.issparse(X):
            self.x_data = torch.sparse_csr_tensor(torch.tensor(X.indptr,
                                                               dtype=torch.int64),
                                                  torch.tensor(X.indices,
                                                  dtype=torch.int64),
                                                  torch.tensor(X.data),
                                                  dtype=torch.float32)
        else:
            X = np.array(X, dtype=np.float32)
            self.x_data = torch.tensor(X, dtype=torch.float32)
        # y = np.array(y, dtype=np.float32)

        if task == 'classification':
            n_class = len(np.unique(y))
            if n_class == 2:
                self.class_num = n_class
            else:
                self.class_num = n_class
            if len(y.shape) == 1 and self.class_num == 1:
                y = y[:, np.newaxis]
            else:
                y = np.eye(self.class_num)[y]
        else:
            y = y[:, np.newaxis]
        self.y_data = torch.tensor(y, dtype=torch.float32)
        self.input_dim = X.shape[1]

    def __len__(self):
        return self.x_data.shape[0]

    def __getitem__(self, idx):
        return self.x_data[idx], self.y_data[idx]


def get_data(dataset_name, sample_size=-1):
    if dataset_name == 'framingham':
        df_fram = pd.read_csv(f'{DATADIR}/{dataset_name}.csv').fillna(0)
        X = df_fram[[col for col in df_fram.columns if col != 'TenYearCHD']]
        y = df_fram.TenYearCHD
    elif dataset_name == 'breast':
        data = load_breast_cancer()
        y = data.target
        X = data.data
    elif dataset_name == 'bank-full':
        df_ban = pd.read_csv(f'{DATADIR}/{dataset_name}.csv', sep=';')
        enc = OneHotEncoder(drop='first')
        X = enc.fit_transform(df_ban[[col for col
                                      in df_ban.columns if col != 'y']])
        y = df_ban.y.map(lambda x: 1 if x == 'yes' else 0)
    elif dataset_name == 'diabetes':
        df_dia = pd.read_csv(f'{DATADIR}/{dataset_name}.csv')
        target_name = 'Outcome'
        X = df_dia[[col for col in df_dia.columns if col != target_name]]
        y = df_dia[target_name]
    elif dataset_name == 'android':
        df_dia = pd.read_csv(f'{DATADIR}/{dataset_name}.csv')
        target_name = 'Result'
        X = df_dia[[col for col in df_dia.columns if col != target_name]]
        y = df_dia[target_name]
    elif dataset_name == 'mnist':
        mnist = fetch_openml('mnist_784', version=1, cache=True)
        X, y = mnist.data / 250, mnist.target.astype(np.int8)
    elif dataset_name == 'titanic':
        df = pd.read_csv(f'{DATADIR}/{dataset_name}.csv')
        target_name = 'Survived'
        train_data = df.copy()
        train_data["Age"].fillna(df["Age"].median(skipna=True), inplace=True)
        train_data["Embarked"].fillna(df['Embarked'].value_counts().idxmax(), inplace=True)
        train_data.drop('Cabin', axis=1, inplace=True)
        train_data['TravelAlone'] = np.where((train_data["SibSp"] + train_data["Parch"]) > 0, 0, 1)
        train_data.drop('SibSp', axis=1, inplace=True)
        train_data.drop('Parch', axis=1, inplace=True)
        training = pd.get_dummies(train_data, columns=["Pclass", "Embarked", "Sex"])
        training.drop('Sex_female', axis=1, inplace=True)
        training.drop('PassengerId', axis=1, inplace=True)
        training.drop('Name', axis=1, inplace=True)
        training.drop('Ticket', axis=1, inplace=True)
        X = training[[col for col in training.columns if col != target_name]]
        y = training[target_name]
    elif dataset_name == 'income':
        names = ['age', 'workclass', 'fnlwgt', 'education', 'education-num',
                 'marital-status', 'occupation', 'relationship', 'race',
                 'sex', 'capital-gain', 'capital-loss', 'hours-per-week',
                 'native-country', 'income']
        df = pd.read_csv(f"{DATADIR}/adult.data", names=names)
        df = df.applymap(lambda x: np.nan if x == '?' else x)
        target_name = 'income'
        input_features = [col for col in df.columns if col not in ['kfold', target_name]]
        numerical_columns = ['age', 'fnlwgt', 'capital-gain', 'capital-loss', 'hours-per-week']
        categorical_columns = [col for col in input_features if not col in numerical_columns]

        def impute_null_values(data, categorical_columns, numerical_columns):
            for col in categorical_columns:
                data.loc[:, col] = data[col].fillna("NONE")

            for cols in numerical_columns:
                data.loc[:, cols] = data[cols].fillna(data[cols].median())
            return data
        df = impute_null_values(df, categorical_columns, numerical_columns)
        enc = OneHotEncoder(sparse=False, drop='first')
        df_cat = pd.DataFrame(enc.fit_transform(df[categorical_columns]))
        X = pd.concat([df[numerical_columns], df_cat], axis=1)
        y = df[target_name].map({' <=50K': 0, ' >50K': 1})
    elif dataset_name == 'housing':
        data = fetch_california_housing()
        y = data.target
        X = data.data
    elif dataset_name == 'diabetes_reg':
        data = load_diabetes()
        y = data.target
        X = data.data
    elif dataset_name == 'patient_survival':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.csv')
        target_name = 'hospital_death'
        numerical_cat = [
            'elective_surgery',
            'apache_post_operative',
            'arf_apache',
            'gcs_unable_apache',
            'intubated_apache',
            'ventilated_apache',
            'aids',
            'cirrhosis',
            'diabetes_mellitus',
            'hepatic_failure',
            'immunosuppression',
            'leukemia',
            'lymphoma',
            'solid_tumor_with_metastasis']
        categorical = ['ethnicity',
                       'gender',
                       'icu_type',
                       'apache_3j_bodysystem',
                       'apache_2_bodysystem']
        col_drop = ['encounter_id', 'icu_admit_source', 'Unnamed: 83',
                    'icu_id', 'icu_stay_type', 'patient_id', 'hospital_id']
        bmi_col = ['bmi', 'weight', 'height']
        num_col = [col for col in data.columns
                   if col not in numerical_cat+categorical
                   + col_drop+[target_name]]
        data = data.drop(col_drop, axis=1)
        data = data.loc[data[bmi_col].dropna(axis=0).index]
        data = data.loc[data[categorical].dropna(axis=0).index]
        data[numerical_cat] = data[numerical_cat].fillna(0)
        data[num_col] = data[num_col].fillna(data[num_col].mean())
        X = data[[col for col in data.columns if col != target_name]]
        X = pd.get_dummies(X, columns=categorical, drop_first=True)
        y = data[target_name]
    elif dataset_name == 'german':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.data-numeric',
                           header=None, delimiter='\s+')
        y = data.values[:, -1]
        y = np.array(list(map(lambda x: 0 if x == 2 else x, y)))
        X = data.values[:, :-1]
    elif dataset_name == 'dropout':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.csv', delimiter=';')
        target_name = 'Target'
        y = data[target_name]
        y = np.array(list(map(lambda x: 0 if x == 'Graduate' else 1, y)))
        X = data[[col for col in data.columns if col != target_name]]
        categorical = ['Marital status', 'Application mode',
                       'Application order', 'Course',
                       'Previous qualification', 'Nacionality',
                       "Mother's qualification", "Father's qualification",
                       "Mother's occupation", "Father's occupation"]
        X = pd.get_dummies(X, columns=categorical, drop_first=True)
        X = X.applymap(lambda x: int(x) if type(x) == bool else x)
    elif dataset_name == 'nursery':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.data',
                           header=None, delimiter=',')
        y = data.values[:, -1]
        mask = {k: v for v, k in enumerate(np.unique(y))}
        y = np.array(list(map(lambda x: mask[x], y)))
        X = data.values[:, :-1]
        X = pd.get_dummies(pd.DataFrame(X), drop_first=True)
        X = X.applymap(lambda x: int(x) if type(x) == bool else x)
    elif dataset_name == 'shuttle':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.tst',
                           header=None, delimiter='\s+')
        y = data.values[:, -1] - 1
        X = data.values[:, :-1]
    elif dataset_name == 'default_credit':
        data = pd.read_excel(f'{DATADIR}/{dataset_name}.xls', skiprows=1)
        categorical = ['EDUCATION', 'MARRIAGE']
        target = 'default payment next month'
        dropcol = 'ID'
        y = data[target]
        X = data.drop([dropcol, target], axis=1)
        X = pd.get_dummies(X, columns=categorical, drop_first=True)
        X = X.applymap(lambda x: int(x) if type(x) == bool else x)
    elif dataset_name == 'thyroid':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.csv')
        data = data.values[:, 1:]
        y = (data[:, -1] - 1).astype('int')
        X = data[:, :-1]
    elif dataset_name == 'yeast':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.data',
                           header=None, delimiter='\s+')
        data = data.values[:, 1:]
        y = data[:, -1]
        mask = {k: v for v, k in enumerate(np.unique(y))}
        y = np.array(list(map(lambda x: mask[x], y)))
        X = data[:, :-1]
        X = X.astype(float)
    elif dataset_name == 'kddcup':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.data_10_percent_corrected',
                           header=None, delimiter=',')
        y = data[41].values
        mask = {k: v for v, k in enumerate(np.unique(y))}
        y = np.array(list(map(lambda x: mask[x], y)))
        X = data.drop(41, axis=1)
        X = pd.get_dummies(X, drop_first=True).values
    else:
        raise 'Dataset not in datasets list'
    if sample_size == -1:
        return X, y
    else:
        rnd_index = np.random.choice(len(y), sample_size, replace=False)
        return X.loc[rnd_index], y.loc[rnd_index]


def load_best_params(method, exp_name, save_dir, filename='results.json'):
    path = f"exp/{exp_name}/{method}/{save_dir}/{filename}"
    ax_client = (
        AxClient.load_from_json_file(path)
    )
    frontier = ax_client.get_pareto_optimal_parameters()
    # we take first index but it could be the mode instead
    best_idx = [i for i in frontier.keys()][-1]
    best_parameters = frontier[best_idx][0]
    best_parameters.pop('noise_multiplier')
    return best_parameters
