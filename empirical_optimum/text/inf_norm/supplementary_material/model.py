import math
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
from torch._tensor import Tensor
import torch
from torchvision.models import VGG, VGG11_Weights

tabular_datasets = ['income', 'android', 'breast', 'default_credit',
                    'dropout', 'german', 'nursery']


def load_vision_model(exp_name, num_groups, alpha):
    if exp_name == 'mnist':
        net = CNN(num_groups, alpha)
    elif exp_name == 'fashionmnist':
        net = FancyCNN(num_groups, alpha)
    elif exp_name == 'cifar':
        net = ShallowVGG(num_groups, alpha)
    return net


def load_tabular_model(exp_name, input_dim, class_num, group):
    net = MLP(input_dim, class_num, group)
    return net


class GroupNormLip(nn.GroupNorm):
    """docstring for GroupNormLip."""

    def __init__(self, alpha, *args, **kwargs):
        super(GroupNormLip, self).__init__(*args, **kwargs)
        self.alpha = alpha

    def forward(self, input: Tensor) -> Tensor:
        N, C, H, W = input.shape
        G = self.num_groups
        input = torch.reshape(input, [N, G, C // G, H, W])
        mean = input.mean(dim=[2, 3, 4], keepdim=True)
        invstd = torch.sqrt(input.var([2, 3, 4], unbiased=False, keepdim=True))
        invstd = torch.maximum(invstd, self.alpha * torch.ones_like(invstd))
        output = (input - mean) / invstd
        output = torch.reshape(output, [N, C, H, W])
        return output


class CNN(nn.Module):
    def __init__(self, num_groups, alpha) -> None:
        super().__init__()
        group = num_groups > 0
        self.group = group
        num_groups = math.gcd(num_groups, 20)
        alpha = alpha / np.sqrt(20//num_groups)
        self.conv1 = nn.Conv2d(1, 20, kernel_size=3, stride=1)
        self.gn1 = GroupNormLip(alpha, num_groups, 20, affine=False)
        self.fc1 = nn.Linear(8 * 8 * 20, 64)
        self.fc2 = nn.Linear(64, 10)
        self.operators_lip = [
            ('conv', lambda x: 3 * x),
            ('bias'+group*'+gn',
             lambda x: 3 / alpha if group else 1),
            ('linear', lambda x: x),
            ('bias', lambda x: 1),
            ('linear', lambda x: x),
            ('bias', lambda x: 1)
        ]

    def forward(self, x: Tensor) -> Tensor:
        x = self.conv1(x)
        if self.group:
            x = self.gn1(x)
        x = F.relu(x)
        x = F.max_pool2d(x, 3, 3)
        x = x.view(-1, 8 * 8 * 20)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return x


def dropout_linear_relu(dim_in, dim_out, p_drop):
    return [nn.Dropout(p_drop),
            nn.Linear(dim_in, dim_out, bias=True),
            nn.ReLU(inplace=True)]


def dropout_linear_sig(dim_in, dim_out, p_drop):
    return [nn.Dropout(p_drop),
            nn.Linear(dim_in, dim_out, bias=True),
            nn.Sigmoid()]


def conv_relu_maxp(in_channels, out_channels, ks, num_group=0, alpha=0):
    if num_group > 0:
        # num_groups = math.gcd(32, out_channels)
        layers = [nn.Conv2d(in_channels, out_channels,
                            kernel_size=ks,
                            stride=1,
                            padding=int((ks-1)/2), bias=True),
                  GroupNormLip(alpha, num_group, out_channels, affine=False),
                  nn.ReLU(inplace=True),
                  nn.MaxPool2d(kernel_size=2)]
    else:
        layers = [nn.Conv2d(in_channels, out_channels,
                            kernel_size=ks,
                            stride=1,
                            padding=int((ks-1)/2), bias=True),
                  nn.ReLU(inplace=True),
                  nn.MaxPool2d(kernel_size=2)]
    return layers


class FancyCNN(nn.Module):

    def __init__(self, num_group, alpha, num_classes=10):
        super(FancyCNN, self).__init__()
        group = num_group > 0
        self.group = group
        num_channels = [16, 32, 64]
        if group:
            num_groups = [math.gcd(num_group, i) for i in num_channels]
            alphas = [alpha / np.sqrt(num_channels[i]//num_groups[i])
                      for i in range(len(num_channels))]
        else:
            num_groups = [0] * len(num_channels)
            alphas = [0] * len(num_channels)
        self.features = nn.Sequential(
            *conv_relu_maxp(1, num_channels[0], 3,
                            num_groups[0], alphas[0]),
            *conv_relu_maxp(num_channels[0], num_channels[1], 3,
                            num_groups[1], alphas[1]),
            *conv_relu_maxp(num_channels[1], num_channels[2], 3,
                            num_groups[2], alphas[2])
        )

        probe_tensor = torch.zeros((1, 1, 28, 28))
        out_features = self.features(probe_tensor).view(-1)

        self.classifier = nn.Sequential(
            *dropout_linear_relu(out_features.shape[0], 128, 0.5),
            *dropout_linear_relu(128, 256, 0.5),
            nn.Linear(256, num_classes, bias=True)
        )

        self.operators_lip = [
            ('conv', lambda x: 3 * x),
            ('bias'+group*'+gn',
             lambda x: 3 / alphas[0] if group else 1),

            ('conv', lambda x: 3 * x),
            ('bias'+group*'+gn',
             lambda x: 3 / alphas[1] if group else 1),

            ('conv', lambda x: 3 * x),
            ('bias'+group*'+gn',
             lambda x: 3 / alphas[2] if group else 1),

            ('linear', lambda x: x),
            ('bias', lambda x: 1),
            ('linear', lambda x: x),
            ('bias', lambda x: 1),

            ('linear', lambda x: x),
            ('bias', lambda x: 1)
        ]

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size()[0], -1)  # OR  x = x.view(-1, self.num_features)
        y = self.classifier(x)
        return y


def conv_relu(in_channels, out_channels, ks, num_group=0, alpha=0):
    if num_group > 0:
        # num_groups = math.gcd(32, out_channels)
        layers = [nn.Conv2d(in_channels, out_channels,
                            kernel_size=ks,
                            stride=1,
                            padding=1, bias=True),
                  nn.GroupNorm(num_group, out_channels, affine=False),
                  nn.ReLU(inplace=True)]
    else:
        layers = [nn.Conv2d(in_channels, out_channels,
                            kernel_size=ks,
                            stride=1,
                            padding=1, bias=True),
                  nn.ReLU(inplace=True)]
    return layers


class ShallowVGG(nn.Module):

    def __init__(self, num_group, alpha, num_classes=10):
        super(ShallowVGG, self).__init__()
        group = num_group > 0
        self.group = group
        num_channels = [32, 64, 64, 128]
        if group:
            num_groups = [math.gcd(num_group, i) for i in num_channels]
            alphas = [alpha / np.sqrt(num_channels[i]//num_groups[i])
                      for i in range(len(num_channels))]
        else:
            num_groups = [0] * len(num_channels)
            alphas = [0] * len(num_channels)
        self.features = nn.Sequential(
            *conv_relu(3, num_channels[0], 3,
                       num_groups[0], alphas[0]),
            nn.AvgPool2d(kernel_size=2, stride=2),
            *conv_relu(num_channels[0], num_channels[1], 3,
                       num_groups[1], alphas[1]),
            nn.AvgPool2d(kernel_size=2, stride=2),
            *conv_relu(num_channels[1], num_channels[2], 3,
                       num_groups[2], alphas[2]),
            nn.AvgPool2d(kernel_size=2, stride=2),
            *conv_relu(num_channels[2], num_channels[3], 3,
                       num_groups[3], alphas[3]),
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten(start_dim=1, end_dim=-1),
            nn.Linear(128, num_classes, bias=True),
        )

        self.operators_lip = [
            ('conv', lambda x: 2 * x),
            ('bias'+group*'+gn',
             lambda x: 2 / alphas[0] if group else 1),
            # ('group', lambda x: 1),
            ('conv', lambda x: 3 * x),
            ('bias'+group*'+gn',
             lambda x: 2 / alphas[1] if group else 1),
            # ('group', lambda x: 1),
            ('conv', lambda x: 2 * x),
            ('bias'+group*'+gn',
             lambda x: 2 / alphas[2] if group else 1),
            # ('group', lambda x: 1),
            ('conv', lambda x: 2 * x),
            ('bias'+group*'+gn',
             lambda x: 2 / alphas[3] if group else 1),
            # ('group', lambda x: 1),
            ('linear', lambda x: x),
            ('bias', lambda x: 1)
        ]

    def forward(self, x):
        return self.features(x)


def attach_weigth_norm(module, weight_norm):
    if weight_norm:
        return nn.utils.parametrizations.weight_norm(module)
    else:
        return module


def linear_dropout_relu(dim_in, dim_out, p_drop, bias, num_groups):
    # num_groups = math.gcd(32, dim_out)
    return [nn.Linear(dim_in, dim_out, bias=bias),
            nn.GroupNorm(num_groups, dim_out, affine=False),
            nn.Dropout(p_drop),
            nn.ReLU(inplace=True)]


class MLP(nn.Module):

    def __init__(self, input_dim=30, num_classes=2,
                 bias=True, group=True):
        super(MLP, self).__init__()
        num_groups = 16

        if group:
            self.classifier = nn.Sequential(
                *linear_dropout_relu(input_dim, 128, 0.001, bias, num_groups),
                # *linear_dropout_relu(128, 256, 0.01),
                nn.Linear(128, num_classes, bias=bias)
            )

            self.operators_lip = [
                ('linear', lambda x: x),
                ('bias'+group*'+gn',
                 lambda x: 128//num_groups if group else 1),
                # ('group', lambda x: 1),

                ('linear', lambda x: x),
                ('bias', lambda x: 1)
            ]
        else:
            self.classifier = nn.Sequential(
                nn.Linear(input_dim, 128, bias),
                nn.Dropout(0.001),
                nn.ReLU(inplace=True),
                nn.Linear(128, num_classes, bias=bias)
            )

            self.operators_lip = [
                ('linear', lambda x: x),
                ('bias', lambda x: 1),
                # ('group', lambda x: 1),

                ('linear', lambda x: x),
                ('bias', lambda x: 1)
            ]

    def forward(self, x):
        y = self.classifier(x)
        return y
