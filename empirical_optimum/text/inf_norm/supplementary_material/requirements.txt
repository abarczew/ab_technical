asttokens==2.4.0
attrs==23.1.0
ax-platform==0.3.3
backcall==0.2.0
botorch==0.8.5
certifi==2023.7.22
charset-normalizer==3.2.0
cmake==3.27.4.1
comm==0.1.4
debugpy==1.7.0
decorator==5.1.1
executing==1.2.0
fastjsonschema==2.18.0
filelock==3.12.3
gpytorch==1.10
idna==3.4
importlib-metadata==6.8.0
importlib-resources==6.1.0
inflection==0.5.1
ipdb==0.13.13
ipykernel==6.25.2
ipython==8.12.2
ipywidgets==8.1.1
jedi==0.19.0
Jinja2==3.1.2
joblib==1.3.2
jsonschema==4.19.1
jsonschema-specifications==2023.7.1
jupyter_client==8.3.1
jupyter_core==5.3.1
jupyterlab-widgets==3.0.9
linear-operator==0.4.0
lit==16.0.6
MarkupSafe==2.1.3
matplotlib-inline==0.1.6
mpmath==1.3.0
multipledispatch==1.0.0
nbformat==5.9.2
nest-asyncio==1.5.7
networkx==3.1
numpy==1.24.4
nvidia-cublas-cu11==11.10.3.66
nvidia-cuda-cupti-cu11==11.7.101
nvidia-cuda-nvrtc-cu11==11.7.99
nvidia-cuda-runtime-cu11==11.7.99
nvidia-cudnn-cu11==8.5.0.96
nvidia-cufft-cu11==10.9.0.58
nvidia-curand-cu11==10.2.10.91
nvidia-cusolver-cu11==11.4.0.1
nvidia-cusparse-cu11==11.7.4.91
nvidia-nccl-cu11==2.14.3
nvidia-nvtx-cu11==11.7.91
opacus==1.4.0
opt-einsum==3.3.0
packaging==23.1
pandas==2.0.3
parso==0.8.3
pexpect==4.8.0
pickleshare==0.7.5
Pillow==10.0.0
pkgutil_resolve_name==1.3.10
platformdirs==3.10.0
plotly==5.17.0
prompt-toolkit==3.0.39
psutil==5.9.5
ptyprocess==0.7.0
pure-eval==0.2.2
Pygments==2.16.1
pyro-api==0.1.2
pyro-ppl==1.8.6
python-dateutil==2.8.2
pytz==2023.3.post1
pyzmq==25.1.1
referencing==0.30.2
requests==2.31.0
rpds-py==0.10.3
scikit-learn==1.3.0
scipy==1.10.1
six==1.16.0
stack-data==0.6.2
sympy==1.12
tenacity==8.2.3
threadpoolctl==3.2.0
tomli==2.0.1
torch==2.0.1
torcheval==0.0.7
torchvision==0.15.2
tornado==6.3.3
tqdm==4.66.1
traitlets==5.9.0
triton==2.0.0
typeguard==2.13.3
typing_extensions==4.7.1
tzdata==2023.3
urllib3==2.0.4
wcwidth==0.2.6
widgetsnbextension==4.0.9
zipp==3.16.2
torch-ema
