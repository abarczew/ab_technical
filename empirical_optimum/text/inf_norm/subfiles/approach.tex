\section{Our approach}
\label{sec:approach}


\newcommand{\thetanorm}{u^{(\theta)}}

% In this work, we constrain the objective function to be Lipschitz, and exploit this to determine sensitivity. An important advantage is that while traditional \gradDpSgd{} controls sensitivity via gradient clipping of each sample separately, our new method estimates gradient sensitivity based on only the model in a data-independent way. This is grounded in Lipschitz-constrained model literature (\cref{sec:related}), highlighting the connection between the Lipschitz value for input and parameter. Subsection \ref{subsec:backprop} demonstrates the use of backpropagation for gradient sensitivity estimation. Subsection \ref{subsec:lip_values} delves into determining an upper Lipschitz bound, and in \ref{subsec:lipdpsg}, we introduce \weightDpSgd{}, a novel algorithm ensuring privacy without gradient clipping.

\subsection{Layer sensitivity}\label{subsec:backprop}


Consider a feed-forward network $f_\theta$.  We define $\mathcal{L}_k(\theta,(x_k,y))=\ell\left(\left(f_{\theta_K}^{(K)}\circ \ldots \circ f_{\theta_k}^{(k)}\right)(x_k), y\right)$ with $\theta_k = (W_k, B_k)$.
For feed-forward networks, the chain rule gives:
% todo: update with notation from alg
\begin{equation}\label{eq:back_prop}
  \begin{aligned}
    \lossPartialInput{k} & = \frac{\partial\ell}{\partial x_{K+1}}\frac{\partial f_{\theta_K}^{(K)}}{\partial x_k}.
  \end{aligned}
\end{equation}
Any matrix or vector norm is submultiplicative, especially the $\|\cdot\|_{p}$ norm with $p \in \{1, 2, \infty\}$, hence:
\begin{equation}\label{eq:back_prop}
  \begin{aligned}
    \left\|\lossPartialInput{k}\right\|_{p} & \leq \left\| \frac{\partial\ell}{\partial x_{K+1}}\right\|_{p} \left\| \frac{\partial f_{\theta_K}^{(K)}}{\partial x_k}\right\|_{p}.
  \end{aligned}
\end{equation}
Since $f_{\theta_K}^{(K)}$ is differentiable almost everywhere, by Rademacher's theorem, $\left\| \frac{\partial f_{\theta_K}^{(K)}}{\partial x_k}\right\|_{p}$ is bounded by the Lipschitz value of $f_{\theta_K}^{(K)}$. If every layer is $c_k$-Lipschitz for all $k \in [1, K]$ with respect to the $\|\cdot\|_{p}$ norm, and all activation functions such as ReLU, Tanh, and Sigmoid are $1$-Lipschitz with respect to the $\|\cdot\|_{p}$ norm, and if $\left\| \frac{\partial\ell}{\partial x_{K+1}}\right\|_{p} \leq \tau$, where $\ell$ represents the loss, then the loss is $\tau$-Lipschitz with respect to the $\|\cdot\|_{p}$ norm.
\begin{equation}\label{eq:backprop.ineq.partial_input}
  \begin{aligned}
    \left\|\lossPartialInput{k}\right\|_{p} & \leq \tau \prod_{i=k}^K c_i.
  \end{aligned}
\end{equation}

We now consider the induced $\|\cdot\|_{2,p}$ norm ($\|A\|_{\alpha, \beta}=\sup _{x \neq 0} \frac{\|A x\|_\beta}{\|x\|_\alpha}$) for studiying $\lossPartialWeight{k}$. Induced norms are consistent, then one can prove that $\|A B\|_{\alpha, \gamma} \leq\|A\|_{\beta, \gamma}\|B\|_{\alpha, \beta}$ \citep{two-to-infinity-norm} which, applied to the chain rule, gives:
\begin{equation}
  \begin{aligned}
    \left\|\lossPartialWeight{k}\right\|_{2,p} & \leq \left\|\lossPartialInput{k+1}\right\|_{p}\left\|\layerPartialWeight{k}\right\|_{2, p}
  \end{aligned}
\end{equation}

We onlly focus on linear operators such as linear layers or convolutional layers. Thus, $\layerPartialWeight{k}$ solely relies on $\vec{x_k}$, the serialized vector of $x_k$. If we consider the weights and biases seperatly, and refer to \cref{eq:backprop.ineq.partial_input}, considering that the $\|\cdot\|_{2,p}$ norm is inherently induced by the $\|\cdot\|_2$, we get:

\begin{equation}
  \begin{aligned}
    \left\|\frac{\partial \mathcal{L}_k}{\partial W_k}\right\|_{2,p} & \leq \tau \prod_{i=k+1}^K c_i \cdot \|\vec{x_k}\|_2 \\ & \text{[check for convolution]};\\
    \left\|\frac{\partial \mathcal{L}_k}{\partial B_k}\right\|_{2,p} & \leq \tau \prod_{i=k+1}^K c_i \cdot 1.
  \end{aligned}
\end{equation}
Note that $\max\limits_{x_k} \left\|\lossPartialWeight{k}\right\|_{2,p}$ represents the elementwise $\ell_2$ sensitivity of the gradient of the weights (the bias) at layer $k$, denoted as $\Delta_{W_k}$ ($\Delta_{B_k}$). This $\ell_2$ sensitivity value can be used to scale Gaussian noise in order to enforce differential privacy. Finally, we have,
\begin{equation}
  \begin{aligned}\label{eq:sensitivity}
    \Delta_{W_k} \triangleq \max\limits_{x_k} \left\|\frac{\partial \mathcal{L}_k}{\partial W_k}\right\|_{2,p} & \leq \tau \prod_{i=k+1}^K c_i \max\limits_{x_k} \|\vec{x_k}\|_2;\\
    \Delta_{B_k} \triangleq \max\limits_{x_k} \left\|\frac{\partial \mathcal{L}_k}{\partial B_k}\right\|_{2,p} & \leq \tau \prod_{i=k+1}^K c_i.
  \end{aligned}
\end{equation}

\subsection{Gradient scale}\label{subsec:grad_scale}
Let \[g_k^{(t)} = \left(\frac{\partial \mathcal{L}_k}{\partial W_k^t}, \frac{\partial \mathcal{L}_k}{\partial B_k^t}  \right).\]
The classic parameter update rule for $(W_k,B_k)$ is:
\begin{equation*}  
    W_k^{(t+1)} = W_k^{(t)} - \eta \left(\frac{\partial \mathcal{L}_k}{\partial W_k^t} + \frac{\sigma \Delta_{W_k}}{|V|}\zeta\right)
\end{equation*}
\begin{equation*}  
    B_k^{(t+1)} = B_k^{(t)} - \eta \left(\frac{\partial \mathcal{L}_k}{\partial B_k^t}  + \frac{\sigma \Delta_{B_k}}{|V|}\zeta\right)
\end{equation*}

with $g_k^{(t)}$ the aggregated gradient at iteration $t$ and layer $k$, the noise multiplier $\sigma$,
the batchset $V$ and $\zeta \sim \mathcal{N}(0,\mathbbm{I})$.

Remember that the layer $k$ has an input $x_k$.
Define $\Delta^\prime \triangleq \tau \prod_{i=k+1}^K c_i $.
Per \ref{eq:sensitivity}, we get:
\begin{equation}\label{eq:delta_prime}
  \frac{\max\limits_{x_k \in \mathcal{X}_k} \left\|
      % g_k^{(t)}
      \frac{\partial \mathcal{L}_k}{\partial W_k}
    \right\|_{2,p}}{\max\limits_{x_k \in V_k}\|\vec{x_k}\|_2} \leq \Delta^\prime
\end{equation}



%But, in the case the layer $k$ has an input $x_k$ e.g., not the bias,
We can use instead the following modifed version for updating $W_k$:

\begin{equation}
  W_k^{(t+1)} = W_k^{(t)} - \eta \left(\frac{g_{W_k}^{(t)}}{\max\limits_{x_k \in V_k}\|\vec{x_k}\|} + \frac{\sigma \Delta^\prime}{|V|}\zeta\right)
\end{equation}

with $V_k = \{(f_k\circ \ldots \circ f_1)(x)|(x,y)\in V\}$,
%with $V_k$ the set of input at layer $k$ in the batchset $V$,
and for $B_k$
\[
    B_k^{(t+1)} = B_k^{(t)} - \eta \left(g_{B_k}^{(t)} + \frac{\sigma \Delta}{|V|}\zeta\right)
\]



with $\mathcal{X}_k$ the set of all possible inputs at layer $k$.


