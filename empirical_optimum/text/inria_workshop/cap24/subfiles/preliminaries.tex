\newcommand{\dspace}{\mathcal{Z}}
\newcommand{\dsetspace}{\mathcal{Z}^*}
\newcommand{\dset}{Z}
\newcommand{\dseta}{{Z^1}}
\newcommand{\dsetb}{{Z^2}}
\newcommand{\adjdsets}{{\mathcal{Z}^*_\sim}}
\newcommand{\exz}{z}
\newcommand{\exx}{x}
\newcommand{\exy}{y}
\newcommand{\yspace}{{\mathcal{Y}}}
\newcommand{\Fll}{{F^{ll}}}
\newcommand{\sLipschitz}{{L^g}}
\newcommand{\vLipschitz}{L}
\newcommand{\thetat}{{{\tilde{\theta}}^{(t)}}}
\newcommand{\thetaT}{{{\tilde{\theta}}^{(T)}}}
\newcommand{\thetaone}{{{\tilde{\theta}^{(1)}}}}
\newcommand{\thetatsucc}{{{\tilde{\theta}}^{(t+1)}}}
\newcommand{\noiset}{{b^{(t)}}}
\newcommand{\gradt}{{g^{(t)}}}
\newcommand{\gradtv}{{g^{(t)}_v}}
\newcommand{\batchset}{{V}}
\newcommand{\batchi}{{v_i}}
\newcommand{\batchsize}{s}
\newcommand{\gradtn}{{\hat g}^{(t)}}


\section{Preliminaries}


\begingroup
\setbeamertemplate{navigation symbols}{}  % remove page number within the group
\begin{frame}[noframenumbering]{}
    \centering
    \vspace{3.5cm}
    \Huge
    %\textcolor{myblue}{Part 1 \\ - \\ Detecting Base Station failure at Sigfox}
    \textcolor{myblue}{Preliminaries}
    %\textcolor{myblue}{Anomaly detection in communication networks: application to Sigfox}
\end{frame}
\endgroup

\begin{frame}[noframenumbering]{Gaussian mechanism as a building block of the DP-SGD}

\begin{itemize}
  \item Differential privacy (DP) ensures that an algorithm's output remains indistinguishable, even when one instance of the dataset is changed, to an adversary who knows all but that instance.
  \item If the output of an algorithm $\mathcal{A}$ is a real number or a vector, it can be privately released using DP mechanisms like the Laplace or Gaussian mechanisms \citep{dwork_calibrating_nodate}.
\end{itemize}
\pause

\begin{minipage}[t]{0.48\linewidth}
    \vspace{0pt}
    \centering\textbf{Differential Privacy}
    \begin{center}
      \begin{tcolorbox}
      Let $\epsilon>0$ and $\delta>0$. Consider a randomized algorithm $\mathcal{A}$ that takes datasets from $\dsetspace$ as input and produces outputs in $\mathcal{O}$. The algorithm $\mathcal{A}$ is $(\epsilon,\delta)$-DP if, for any pair of adjacent datasets $(\dseta,\dsetb)\in\adjdsets$ and any subset $S\subseteq \mathcal{O}$ of possible outputs, $P(\mathcal{A}(\dseta)\subseteq S) \le e^\epsilon P(\mathcal{A}(\dsetb)\subseteq S)+\delta$. When $\delta=0$, $\mathcal{A}$ is referred to as $\epsilon$-DP.
     \end{tcolorbox}
   \end{center}
\end{minipage}\hfill
\begin{minipage}[t]{0.47\linewidth}
        \vspace{0pt}
        \centering\textbf{Gaussian Mechanism}\\
        \smallskip
        \begin{center}
          \begin{tcolorbox}
            Let $f:\dsetspace\to\mathbb{R}^p$ be a function. The Gaussian mechanism transforms $f$ into ${\hat{f}}$ by adding Gaussian distributed noise $b\sim \mathcal{N}(0,\sigma^2 I_p)\in\mathbb{R}^p$ to $f(\dset)$, resulting in ${\hat{f}}(\dset) = f(\dset) + b$. If the variance satisfies $\sigma^2 \ge 2\ln(1.25/\delta)(\Delta_2(f))^2/\epsilon^2$, with sensitivity $\Delta_2(f) =\max_{\dseta,\dsetb\in\adjdsets} \|f(\dseta) - f(\dsetb)\|_2$, then ${\hat{f}}$ is $(\epsilon,\delta)$-DP.
         \end{tcolorbox}
        \end{center}

\end{minipage}
\end{frame}

\begingroup
\begin{frame}[t]{Gradient clipping, standard of DP-SGD but a lot of bias}
\begin{minipage}[t]{0.62\linewidth}
  \vspace{0pt}
  \begin{tcolorbox}[title= Algorithm (DP-SGD) \citep{abadi_deep_2016},size=title,boxrule=0.2pt]
  \begin{algorithm}[H]
  \SetKwInOut{Input}{Input}
  \SetKwInOut{Output}{Output}

  \Input{Data set $\dset\in\dsetspace$, privacy parameters $\epsilon$ and $\delta$, batch size $\batchsize\ge 1$, learning rate $\eta_t$,
  maximum clipping norm
  $C\in\mathbb{R}$}
  Initialize $\thetaone$ randomly from $\Theta$\;
  $\sigma \leftarrow \textsc{SearchDPNoise}(\epsilon, \delta, n, T, s)$ \Comment{Set noise multiplier}\;
  \For{$t \leftarrow 1$ \KwTo $T$}{
    Draw uniformly $\batchset\in \dset^\batchsize$\Comment{Sample batch}\;
    \For{$v\in V$ let the data owner of $v$}{
    $\gradtv \leftarrow \nabla\mathcal{L}(\thetat;v)$\;
    $\gradtv\gets\gradtv / \operatorname{max}\left(1, \|\gradt\|_2/C\right)$\Comment{Gradient clipping}\;
    }
    $\gradtn \leftarrow \sum_{v\in V} (\gradtv+\noiset)/|V|$ with $\noiset \thicksim \mathcal{N}(0, \sigma^2 C \mathbbm{I})$
     \Comment{Secure aggregation by data owners}\;
    $\thetatsucc \leftarrow \thetat - \eta(t)\gradtn$\Comment{Update}\;
    }
  \Output{$\theta^{priv} = \thetaT$.}

\end{algorithm}

  \end{tcolorbox}
\end{minipage}\hfill
\pause
\begin{minipage}[t]{0.35\linewidth}
  \vspace{0pt}
  \begin{itemize}
  \item Gradient clipping has become the conventional approach for DP-SGD.
  \item In $\textsc{SearchDPNoise}$, the moment accountant method is employed to determine the appropriate noise multiplier $\sigma$ for achieving the desired level of $(\epsilon, \delta)$-differential privacy.
  \item Gradient clipping effectively bounds the gradient but introduces significant bias, rendering it challenging to apply to real-world datasets.
  \end{itemize}
\end{minipage}

\end{frame}
\endgroup
