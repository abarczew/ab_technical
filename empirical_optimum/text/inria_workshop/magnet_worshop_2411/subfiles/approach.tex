\newcommand{\fnnFunc}{f_\theta}
\newcommand{\layerFunc}[1]{f^{({#1})}_{\theta_{{#1}}}}


\section{Approach}


\begingroup
\setbeamertemplate{navigation symbols}{}  % remove page number within the group
\begin{frame}[noframenumbering]{}
    \centering
    \vspace{3.5cm}
    \Huge
    %\textcolor{myblue}{Part 1 \\ - \\ Detecting Base Station failure at Sigfox}
    \textcolor{myblue}{Approach}
    %\textcolor{myblue}{Anomaly detection in communication networks: application to Sigfox}
\end{frame}
\endgroup

\begin{frame}[noframenumbering]{Lipschitz value of feed-forward networks}

\begin{tcolorbox}
  A feed-forward neural network (FNN) $f_\theta: \mathbb{R}^n \to \mathbb{R}^m$ is a function which can be expressed as
  \[
    \fnnFunc = \layerFunc{K} \circ \ldots \circ \layerFunc{1}
  \]
  where $\layerFunc{k} : \mathbb{R}^{n_{k}} \to \mathbb{R}^{n_{k+1}}$. $\layerFunc{k}$ is the $k$-th layer function parameterized by $\theta_k$ for $1\le k\le K$. We denote the input of $\layerFunc{k}$ by $x_k$ and its output by $x_{k+1}$.
  %$\layerFunc{k}(\theta_k, x_k)$ by $x_{k+1}$.
  Here, $\theta=(\theta_1 \ldots \theta_K)$, $n=n_1$ and $m=n_{K+1}$.
\end{tcolorbox}
  Common layers include fully connected layers, convolutional layers and activation layers. Parameters of the first two correspond to weight and bias matrices, $\theta_k = (W_k, B_k)$, while activation layers have no parameter, $\theta_k = ()$.
\end{frame}

\begingroup
\begin{frame}[t]{Enforcing Lipschitz continuity with spectral normalization}
\begin{itemize}
  \item The lipschitz value of a layer of feed-forward networks is either 1 e.g., activations layers such as ReLU or Sigmoid, or depend on the norm of the parameter $\theta_k$ e.g., linear or convolutional layers.
  \item Spectral normalization is a popular method for enforcing Lipschitz constraints, involving scaling the weights of the network by their largest singular value \citep{miyato2018spectral}
  \item The model's overall Lipschitz value can serve as an upper bound for the gradient norm and subsequently for scaling the DP noise.
\end{itemize}
\end{frame}
\endgroup


\newcommand{\grpidx}{q}
\newcommand{\layerPartialInput}[1]{\frac{\partial \layerFunc{{#1}}}{\partial x_{{#1}}}}
\newcommand{\layerPartialWeight}[1]{\frac{\partial \layerFunc{{#1}}}{\partial \theta_{{#1}}}}
\newcommand{\lossPartialInput}[1]{\frac{\partial\mathcal{L}_{{#1}}}{\partial x_{{#1}}}}
\newcommand{\lossPartialWeight}[1]{\frac{\partial\mathcal{L}_{#1}}{\partial \theta_{{#1}}}}


\begingroup
\begin{frame}[t]{Gradient sensitivity can be bounded by the model Lipschitz value}

  \tikzstyle{na} = [baseline=-.5ex]
  
  % Below we mix an ordinary equation with TikZ nodes. Note that we have to
  % adjust the baseline of the nodes to get proper alignment with the rest of
  % the equation.
  \begin{tcolorbox}  
    \begin{equation*}
      \left\|\lossPartialWeight{k}\right\|_{2,p} \leq
          \tikz[baseline]{
              \node[fill=blue!20,anchor=base] (t1)
              {$ \left\|\lossPartialInput{k+1}\right\|_{p}$};
          } 
          \tikz[baseline]{
            \node[fill=red!20, ellipse,anchor=base] (t2)
              {$\left\|\layerPartialWeight{k}\right\|_{2, p}$};
          }
        \end{equation*}
  
    \begin{itemize}[]
      \item \textcolor{black}{Depends on the parameters norm}
          \tikz[na]\node [coordinate] (n1) {};
      \item \textcolor{black}{Depends on the input norm}
      \tikz[na]\node [coordinate] (n2) {};
    \end{itemize}
    
    % Now it's time to draw some edges between the global nodes. Note that we
    % have to apply the 'overlay' style.
    \begin{tikzpicture}[overlay]
      \path[->] (n1) edge [bend right] (t1);
      \path[->] (n2) edge [bend right] (t2);
    \end{tikzpicture}
  \end{tcolorbox}
    
\end{frame}
\endgroup

\begingroup
\begin{frame}[t]{Gradient sensitivity can be bounded by the model Lipschitz value}

  \tikzstyle{na} = [baseline=-.5ex]
  
  % Below we mix an ordinary equation with TikZ nodes. Note that we have to
  % adjust the baseline of the nodes to get proper alignment with the rest of
  % the equation.
  \begin{tcolorbox}
    \begin{equation*}
      \left\|\lossPartialWeight{k}\right\|_{2,p} \leq
          \tikz[baseline]{
              \node[fill=blue!20,anchor=base] (t1)
              {$ \left\|\lossPartialInput{k+1}\right\|_{p}$};
          } 
          \tikz[baseline]{
            \node[fill=red!20, ellipse,anchor=base] (t2)
              {$\left\|\layerPartialWeight{k}\right\|_{2, p}$};
          }
        \end{equation*}
    \begin{equation*}
      \left\|\lossPartialWeight{k}\right\|_{2,p} \leq
          \tikz[baseline]{
              \node[fill=blue!20,anchor=base] (t1)
              {$ \left\|\lossPartialInput{k+1}\right\|_{p}$};
          } 
          \tikz[baseline]{
            \node[fill=red!20, ellipse,anchor=base] (t2)
              {$\max\limits_{x_k} \|\vec{x_k}\|_2$};
          }
        \end{equation*}
      \pause
      \begin{equation*}
        \left\|\frac{\lossPartialWeight{k}}{\max\limits_{x_k} \|\vec{x_k}\|_2}\right\|_{2,p} \leq
            \tikz[baseline]{
                \node[fill=blue!20,anchor=base] (t1)
                {$ \left\|\lossPartialInput{k+1}\right\|_{p}$};
            }
      \end{equation*}
      \pause
      \begin{equation*}
        \left\|\frac{\lossPartialWeight{k}}{\max\limits_{x_k} \|\vec{x_k}\|_2}\right\|_{2,p} \leq
            \tikz[baseline]{
                \node[fill=blue!20,anchor=base] (t1)
                {$ \tau \prod_{i=k+1}^K \left\|\layerPartialInput{i}\right\|_{2, p} $};
            }
      \end{equation*}
  \end{tcolorbox}
    
\end{frame}
\endgroup


\newcommand{\thetanorm}{u^{(\theta)}}

\begingroup
\begin{frame}[t]{Lip-DP-SGD clips parameters at the batch level}

\begin{tcolorbox}[title= Lip-DP-SGD (DP-SGD with Weight Clipping) ,size=title,boxrule=0.2pt]
\begin{algorithm}[H]
\SetKwInOut{Input}{Input}
\SetKwInOut{Output}{Output}

\Input{Data set $\dset\in\dsetspace$, privacy parameters $\epsilon$ and $\delta$, batch size $\batchsize\ge 1$, learning rate $\eta_t$,
maximum clipping norm $C\in\mathbb{R}^K$
and Lipschitz strategy $Lip \in \{"local","global"\}$}
Initialize $\tilde{\theta}$ randomly from $\Theta$\;
$\sigma \leftarrow \textsc{SearchDPNoise}(\epsilon, \delta, n, T, s)$ \Comment{Set noise multiplier}\;
\For{$t \leftarrow 1$ \KwTo $T$}{
  $(\Delta_k)_{k=1}^K \leftarrow$ \textsc{LayerSensitivity}$(f, \tilde{\theta}, Lip)$\Comment{Compute model Lipschitz value}\;
  Draw uniformly $\batchset\in \dset^\batchsize$\Comment{Sample batch}\;
  \For{$i \leftarrow 1$ \KwTo $K$}{
  $X_k \leftarrow \max\limits_{i \in |\batchset|} \|f_{\tilde{\theta}_{k-1}}(x_i)\|$\Comment{Max input norm of layer $k$}\;
  $b_k \thicksim \mathcal{N}(0, \sigma^2 \Delta_k^2 \mathbbm{I})$\\
  $\tilde{g}_k \leftarrow \frac{1}{|\batchset|}\left(\frac{1}{X_k}\sum_{i=1}^{|\batchset|} \nabla_{\tilde{\theta}_k}\ell(f_{\tilde{\theta}}(x_i), y_i)+b_k\right)$\Comment{Scaled gradient with DP noise}\;
  $\tilde{\theta}_k \leftarrow \tilde{\theta}_k - \eta(t) \tilde{g}_k$\\
  $\tilde{\theta}_k \gets$ \textsc{ClipWeights}($\tilde{\theta}_k $,$C$)\Comment{Spectral normalization to enforce Lipschitzness}\;
  }
  }
\Output{$\theta^{priv} = \thetaT$.}

\end{algorithm}

\end{tcolorbox}

\end{frame}
\endgroup

\begingroup
\begin{frame}[c]{Gradient clipping introduces optimization bias\\
   while Lipschitz constraints causes model bias}
  \begin{tcolorbox}
    \begin{center}
      \includegraphics[width=0.8\textwidth]{figures/bias.png}
    \end{center}
  \end{tcolorbox}
\end{frame}
\endgroup
