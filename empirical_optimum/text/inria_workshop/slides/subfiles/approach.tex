\newcommand{\fnnFunc}{f_\theta}
\newcommand{\layerFunc}[1]{f^{({#1})}_{\theta_{{#1}}}}


\section{Approach}


\begingroup
\setbeamertemplate{navigation symbols}{}  % remove page number within the group
\begin{frame}[noframenumbering]{}
    \centering
    \vspace{3.5cm}
    \Huge
    %\textcolor{myblue}{Part 1 \\ - \\ Detecting Base Station failure at Sigfox}
    \textcolor{myblue}{Approach}
    %\textcolor{myblue}{Anomaly detection in communication networks: application to Sigfox}
\end{frame}
\endgroup

\begin{frame}[noframenumbering]{Lipschitz value of feed-forward networks}

\begin{tcolorbox}
  A feed-forward neural network (FNN) $f_\theta: \mathbb{R}^n \to \mathbb{R}^m$ is a function which can be expressed as
  \[
    \fnnFunc = \layerFunc{K} \circ \ldots \circ \layerFunc{1}
  \]
  where $\layerFunc{k} : \mathbb{R}^{n_{k}} \to \mathbb{R}^{n_{k+1}}$. $\layerFunc{k}$ is the $k$-th layer function parameterized by $\theta_k$ for $1\le k\le K$. We denote the input of $\layerFunc{k}$ by $x_k$ and its output by $x_{k+1}$.
  %$\layerFunc{k}(\theta_k, x_k)$ by $x_{k+1}$.
  Here, $\theta=(\theta_1 \ldots \theta_K)$, $n=n_1$ and $m=n_{K+1}$.
\end{tcolorbox}
  Common layers include fully connected layers, convolutional layers and activation layers. Parameters of the first two correspond to weight and bias matrices, $\theta_k = (W_k, B_k)$, while activation layers have no parameter, $\theta_k = ()$.
\end{frame}

\begingroup
\begin{frame}[t]{Enforcing Lipschitz continuity with spectral normalization}
\begin{itemize}
  \item The lipschitz value of a layer of feed-forward networks is either 1 e.g., activations layers such as ReLU or Sigmoid, or depend on the norm of the parameter $\theta_k$ e.g., linear or convolutional layers.
  \item Spectral normalization is a popular method for enforcing Lipschitz constraints, involving scaling the weights of the network by their largest singular value \citep{miyato2018spectral}
  \item The model's overall Lipschitz value can serve as an upper bound for the gradient norm and subsequently for scaling the DP noise.
\end{itemize}
\end{frame}
\endgroup


\newcommand{\grpidx}{q}
\newcommand{\layerPartialInput}[1]{\frac{\partial \layerFunc{{#1}}}{\partial x_{{#1}}}}
\newcommand{\layerPartialWeight}[1]{\frac{\partial \layerFunc{{#1}}}{\partial \theta_{{#1}}}}


\begingroup
\begin{frame}[t]{Computing the model Lipschitz value to scale the noise at a layer level}

\begin{tcolorbox}[title= LayerSensitivity ,size=title,boxrule=0.2pt]
\begin{algorithm}[H]
\SetKwInOut{Input}{Input}
\SetKwInOut{Output}{Output}

\Input{$K$ layer feed-forward model $f$, parameters $\theta$, parameter norm $u^{(\theta)}$, max input norm $X_1$, upper bound of loss Lipschitz value $l_{K+1}$.}
\For{$k \leftarrow 1$ \KwTo $K$}{

  \eIf{$\theta_k=\emptyset$}{
  \eIf{$k$-th layer is a normalization layer}{
  $X_{k+1} \leftarrow \min(|x_{k+1}|^{\frac{1}{2}}, \frac{X_k}{\alpha})$ \Comment{with $  x_{k+1}^{(k:\grpidx)}=\layerFunc{k}(x_k^{(k:\grpidx)})=\frac{x_k^{(k:\grpidx)}-\mu^{(k:\grpidx)}}{\max(\alpha, \sigma^{(k:\grpidx)})}$}\;
  }{
  $X_{k+1} \leftarrow X_{k}L_{x_k}$\Comment{$L_{x_k}$ an upper bound for $\left\|\layerPartialInput{k}\right\|$}\;
  }
  }{
  $X_{k+1} \leftarrow X_{k}u_k^{(\theta)}$\Comment{$u_{k}^{(\theta)} = \min(C, \|\tilde{\theta}_{k}\|)$, with $C$ the clipping norm}\;
  }
  }
  \For{$k=K$ {\bfseries to} $1$}{ %\Comment{Backward pass}
  $l_k\text{ }\leftarrow  l_{k+1} L_{x_k}$ \;
  $\Delta_k \leftarrow l_{k+1} L_{\theta_k}$ \Comment{$L_{\theta_k}$ an upper bound for $\left\|\layerPartialWeight{k}\right\|$}\; %\Comment{$L_{\theta_k}=0$ when $\theta_k=\emptyset$}
  }
\Output{$(\Delta_1, \dots, \Delta_{K-1}, \Delta_K)$.}
\end{algorithm}
\end{tcolorbox}
\end{frame}
\endgroup


\newcommand{\thetanorm}{u^{(\theta)}}

\begingroup
\begin{frame}[t]{}

\begin{tcolorbox}[title= Lip-DP-SGD (DP-SGD with Weight Clipping) ,size=title,boxrule=0.2pt]
\begin{algorithm}[H]
\SetKwInOut{Input}{Input}
\SetKwInOut{Output}{Output}

\Input{Data set $\dset\in\dsetspace$, privacy parameters $\epsilon$ and $\delta$, batch size $\batchsize\ge 1$, learning rate $\eta_t$,
maximum clipping norm $C\in\mathbb{R}^K$
and Lipschitz strategy $Lip \in \{"local","global"\}$}
Initialize $\thetaone$ randomly from $\Theta$\;
$\sigma \leftarrow \textsc{SearchDPNoise}(\epsilon, \delta, n, T, s)$ \Comment{Set noise multiplier}\;
\For{$t \leftarrow 1$ \KwTo $T$}{
  $(\Delta_k)_{k=1}^K \leftarrow$ \textsc{LayerSensitivity}$(f, \tilde{\theta}, \thetanorm)$\Comment{Compute model Lipschitz value}\;
  Draw uniformly $\batchset\in \dset^\batchsize$\Comment{Sample batch}\;
  \For{$i \leftarrow 1$ \KwTo $K$}{
  $b_k \thicksim \mathcal{N}(0, \sigma^2 \Delta_k^2 \mathbbm{I})$\\
  $\tilde{g}_k \leftarrow \frac{1}{|\batchset|}\left(\sum_{i=1}^{|\batchset|} \nabla_{\tilde{\theta}_k}\ell(f_{\tilde{\theta}}(x_i), y_i)+b_k\right)$\\
  $\tilde{\theta}_k \leftarrow \tilde{\theta}_k - \eta(t) \tilde{g}_k$
  }
  $(\thetanorm,\tilde{\theta}) \gets$ \textsc{ClipWeights}($\tilde{\theta}$,$C$)\Comment{Spectral normalization to enforce Lipschitzness}\;
  }
\Output{$\theta^{priv} = \thetaT$.}

\end{algorithm}

\end{tcolorbox}

\end{frame}
\endgroup
