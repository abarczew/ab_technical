\section{Our approach}
\label{sec:method}

In this section, we detail how taking into account the position of the current model in the search space rather than using the worst case gradient norm 
%upper bounds of partial derivatives of the loss, rather than on the $\ell_2$-norm of its gradient,
can bring better utilility when designing a differentially private SGD algorithm. We illustrate this with the logistic regression and two different privacy budget accountants: one relying on the strong composition as described by \cite{bassily_differentially_2014} and the other based on the moment accountant as per \cite{abadi_deep_2016}.

\subsection{Scale the noise to upper-bounds of the partial derivatives of the gradient}\label{sec:partial_L}

Let $f$ be a function bounded w.r.t. the $2$-norm, then all partial derivatives of $f$ are also bounded, in particular
\[
  \forall j \in [p], \exists L_j \in \mathbb{R}, \left| \frac{\partial f}{\partial\theta_j}(x, y)  \right| \leq L_j.
\]
This is typically the case when $f$ is $L$ Lipschitz or when $f$ is clipped. \cref{alg:dp-sgd-logloss-l1} can either take as input $L_j$, upper-bounds of the partial derivates of the loss, or $L$ the Lipschitz constant or the clipping norm. In the first case, $\textsc{ScaleNoise}$ outputs a vector $(\sigma_i)_i$ of length $p$ and in the other, it gives a scalar $\sigma$. One can note that, if $L_j$ is lower than $L/\sqrt{p}$, thanks to some information specific to this dimension $j$, then the resulting amount of noise will be lower than the one derived from $L$.
% \cref{alg:dp-sgd-logloss-l1} sums up these two algorithms, only the accountant $acc$ changes.

\begin{theorem}
  \cref{alg:dp-sgd-logloss-l1} ensures $(\epsilon, \delta)$-differential privacy.
\end{theorem}

\begin{proof}
Moment accountant provides lower privacy loss at each time step than the strong composition theorem. So if we can prove $(\epsilon, \delta)$-differential privacy for the former, the proof for the latter follows. \cite{abadi_deep_2016}, which first introduced moment accountant, proves $(\epsilon, \delta)$-differential privacy thanks to the following moments bound on Gaussian mechanism with random sampling.

\begin{lemma}
  Suppose that $f: D \rightarrow \mathbb{R}^p$ with $|f(\cdot)_i| \leq L_i$, with $i \in [p]$. Let $\sigma \geq 1$ and let $J$ be a sample from $[n]$ where each $i \in[n]$ is chosen independently with probability $q<\frac{1}{16 \sigma}$. Then for any positive integer $\lambda \leq \sigma^2 \ln \frac{1}{q \sigma}$, the mechanism $\mathcal{M}(d)=$ $\sum_{i \in J} f\left(d_i\right)+\mathcal{N}\left(0, \sigma^2 \operatorname{diag}((L_i)_{i\in[p]}\right)$ satisfies
  $$
  \alpha_{\mathcal{M}}(\lambda) \leq \frac{q^2 \lambda(\lambda+1)}{(1-q) \sigma^2}+O\left(q^3 \lambda^3 / \sigma^3\right) .
  $$
\end{lemma}

Note that we have adapted the above lemma so the partial derivative upper-bound $L_i$ isn't the same for all directions anymore. However, the proof remains exactly the same as \cite{abadi_deep_2016} as it focus first on a one-dimensional set-up before generalizing to the multivariate case. Only now, the clipping norm changes depending on the coordinate.
\end{proof}


\subsection{Tighter upper-bound of the LogLoss partial derivatives}

Whether it be the Lipschitz constant or the clipping norm, they appear to be too conservative when it comes to the logistic regression.

Consider now Algorithm $\textsc{PartialBound}$ where we replaced in \cref{alg:dp-sgd-logloss-l1} $L_j$ by $\frac{\mathbbm{1}_{x \neq 0}(\theta_j)}{1+e^{-\|\theta\|_1}}$.


\begin{theorem}\label{th:dp_logloss}
  (Privacy guarantee). Algorithm $\textsc{PartialBound}$ is $(\epsilon, \delta)$-differentially private.
\end{theorem}

\begin{proof}
  Since $S^\prime(x) = S(x)(1-S(x))$, first order derivation of the LogLoss $\ell$ gives
  \begin{align*}
    \nabla_\theta\ell(x, y)
    & = -y\frac{S(x;\theta)(1-S(x;\theta))}{S(x;\theta)}x\\
    &\qquad +(1-y)\frac{S(x;\theta)(1-S(x;\theta))}{(1-S(x;\theta))}x \\
    & = -y(1-S(x;\theta))x + (1-y)S(x;\theta)x \\
    & = \left(S(x;\theta) - y\right)x
  \end{align*}
  So, for all $j \in [p]$,
  \begin{align*}
    \frac{\partial\ell}{\partial\theta_j}(x, y)
    = \left(S(x;\theta) - y\right)x_j \\
  \end{align*}
  Since $x \in [-1, 1]^p$, $S(x;\theta)$ is maximal when $\forall i \in [p], x_i = \text{sign}(\theta_i)$, where $\operatorname{sign}(x)=1$ if $x>0, \operatorname{sign}(x)=-1$ if $x<$ 0 , and $\operatorname{sign}(x)=0$ if $x=0$. For the same reason $S(x;\theta) - 1$ is maximal when $\forall i \in [p], x_i = -\text{sign}(\theta_i)$. As $S(x;\theta) - 1 = -S(-x;\theta)$, all in all we get

  \begin{equation}\label{eq:leq_logloss_partial}
  \begin{split}
    \left|\frac{\partial\ell}{\partial\theta_j}(x, y)\right|
    & \leq \frac{|\operatorname{sign}(\theta_j)|}{1+e^{-\|\theta\|_1}} \\
    & \leq \frac{\mathbbm{1}_{x \neq 0}(\theta_j)}{1+e^{-\|\theta\|_1}}
  \end{split}
  \end{equation}
  At each update of $\theta$, one can use \eqref{eq:leq_logloss_partial} to find an upper-bound of all partial derivatives of the loss.  The rest of the proof follows section \ref{sec:partial_L} with $L_j$ estimated thanks to this upper-bound.
\end{proof}

\subsection{$1$-norm regularizer to lower even more the amount of the added noise}
Let $\mathcal{L}$ be the loss of the logistic regression with $1$-norm regularizer, then
\begin{align*}
  \mathcal{L}(x,y)
  & = \ell((x,y)) + \frac{\gamma}{n} \|\theta\|_1 \\
  & = -y\log S(x;\theta) -(1-y)\log(1-S(x;\theta)) \\
  & \qquad + \frac{\gamma}{n} \|\theta\|_1
\end{align*}
with $S$ the sigmoid function, $y\in\{0,1\}$ and $\gamma$ is the hyper-parameter that controls the degree of regularization. Considering the subgradient at zero, we can use the following update equation for all $j \in [p]$:
\begin{equation}\label{eq:l1_update}
  \theta_j^{t+1}=\theta_j^t-\eta(t) \frac{\partial\ell}{\partial\theta_j}(x, y)+\frac{\gamma}{n} \eta(t) \operatorname{sign}\left(\theta_j^t\right)
\end{equation}
where $\operatorname{sign}(x)=1$ if $x>0, \operatorname{sign}(x)=-1$ if $x<$ 0 , and $\operatorname{sign}(x)=0$ if $x=0$. However, this method as very little chance to bring weights to zero, unless they fall exactly on zero during the training.

\cite{tsuruoka_stochastic_2009} describes an efficient and widely used implementation of the SGD with $1$-norm regularizer that actually produces a compact model. In a nutshell, first, the method clips to zero any weight that changes sign when updated (not to be confused with the clipping which bounds the norm of gradients). Doing so, we can expect a lot of weights to become zero during training \cite{Carpenter2008LazySS}.
Second, the regularization i.e., the last term on the right hand-side of the update equation \eqref{eq:l1_update},  is accumulated at each timestep so one update cannot bring too much variance of weights over steps, e.g., one weight becoming non-zero only due to the gradient of last sample gradient picked for training. We detail further algorithm $\textsc{AddRegularization}$ in appendix.

Algorithm $\textsc{AddRegularization}$ needs to be applied in Algorithm $\textsc{PartialBound}$ after the step \textbf{Descent} to get Algorithm $\textsc{PartialBound}\ell_1$

\begin{theorem}\label{th:dp_logloss_l1}
  (Privacy guarantee). Algorithm $\textsc{PartialBound}\ell_1$ is $(\epsilon, \delta)$-differentially private.
\end{theorem}

\begin{proof}
  As post-processing does not impact differential privacy level \cite{dwork_algorithmic_2013}, \ref{th:dp_logloss} proves algorithm \ref{alg:dp-sgd-logloss-l1} is $(\epsilon, \delta)$-differentially private.
\end{proof}

The regularization works as post-processing but also impacts the upper bounds of the partial derivatives of the loss as we decride them in \cref{sec:partial_L}. Eventually, the utility is boosted by this effect as described in \cref{sec:experiment}.
