\newcommand{\antoinefoot}[1]{\textcolor{red}{\footnote{\textcolor{red}{{{#1}}}}}}

\newcommand{\dspace}{\mathcal{Z}}
\newcommand{\dsetspace}{\mathcal{Z}^*}
\newcommand{\dset}{Z}
\newcommand{\dseta}{{Z^1}}
\newcommand{\dsetb}{{Z^2}}
\newcommand{\adjdsets}{{\mathcal{Z}^*_\sim}}
\newcommand{\exz}{z}
\newcommand{\exx}{x}
\newcommand{\exy}{y}
\newcommand{\xspace}{{\mathcal{X}}}
\newcommand{\yspace}{{\mathcal{Y}}}
\newcommand{\ellll}{{\ell^{ll}}}
\newcommand{\Fll}{{F^{ll}}}
\newcommand{\sLipschitz}{{L^g}}
\newcommand{\vLipschitz}{L}
\newcommand{\thetat}{{{\tilde{\theta}}^{(t)}}}
\newcommand{\thetaT}{{{\tilde{\theta}}^{(T)}}}
\newcommand{\thetaone}{{{\tilde{\theta}^{(1)}}}}
\newcommand{\thetatsucc}{{{\tilde{\theta}}^{(t+1)}}}
\newcommand{\noiset}{{b^{(t)}}}
\newcommand{\gradt}{{g^{(t)}}}
\newcommand{\batchset}{{V}}
\newcommand{\batchi}{{v_i}}
\newcommand{\batchsize}{s}
\section{Preliminaries and background}\label{sec:prelim}
In this section, we briefly review differential privacy, empirical risk minimization (ERM) and differentially private stochastic gradient descent (DP-SGD).

We will denote the space of all possible instances by $\dspace$ and the space of all possible datasets by $\dsetspace$.  We will denoted by $[N]=\{1\ldots N\}$ the set of $N$ smallest positive integers.

\subsection{Differential Privacy}

An algorithm is differentially private if even an adversary who knows all but one instances of a dataset can't distinguish from the output of the algorithm the last instance in the dataset.  More formally:

\begin{definition}[adjacent datasets]
  We say two datasets $\dseta,\dsetb \in \dsetspace$ are adjacent, denoted $\dseta\sim \dsetb$, if they differ in only one element.  We denote by $\adjdsets$ the space of all pairs of adjacent datasets.
\end{definition}

\begin{definition}[differential privacy \cite{dwork_algorithmic_2013}]
  Let $\epsilon>0$ and $\delta>0$.  Let $\mathcal{A}:\dsetspace\to\mathcal{O}$ be a randomized algorithm taking as input datasets from $\dsetspace$.   The algorithm $\mathcal{A}$ is $(\epsilon,\delta)$-differentially private ($(\epsilon,\delta)$-DP) if for every pair of adjacent datasets $(\dseta,\dsetb)\in\adjdsets$, and for every subset $S\subseteq \mathcal{O}$ of possible outputs of $\mathcal{A}$, $P(\mathcal{A}(\dseta)\subseteq S) \le e^\epsilon P(\mathcal{A}(\dsetb)\subseteq S)+\delta$.  If $\delta=0$ we also say that $\mathcal{A}$ is $\epsilon$-DP.
\end{definition}
If the output of an algorithm $\mathcal{A}$ is a real number or a vector, it can be privately released thanks to differential privacy mechanisms such as the Laplace mechanism or the Gaussian mechanism \cite{dwork_calibrating_nodate}.  While our key idea is more generally applicable, in this paper we will focus on the Gaussian mechanism as it leads to simplier derivations.  In particular, the Gaussian mechanism adds Gaussian noise to a number or vector which depends on its sensitivity on the input.
\begin{definition}[sensitivity]
  The $2$-sensitivity of a function $f:\dsetspace\to\mathbb{R}^p$ is
  \[\Delta_2(f) =\max_{\dseta,\dsetb\in\adjdsets} \|f(\dseta),f(\dsetb)\|_2 \]
\end{definition}
\begin{lemma}[Gaussian mechanism]
  \ref{lem:gaussmech}
  Let $f:\dsetspace\to\mathbb{R}^p$ be a function.  The Gaussian mechanism transforms $f$ into ${\hat{f}}$ with ${\hat{f}}(\dset) = f(\dset) + b$ where $b\sim \mathcal{N}(0,\sigma^2 I_p)\in\mathbb{R}^p$ is Gaussian distributed noise.  If the variance satisfies $\sigma^2 \ge 2\ln(1.25/\delta)(\Delta_2(f))^2/\epsilon^2$, then ${\hat{f}}$ is $(\epsilon,\delta)$-DP.
\end{lemma}

%the output of $\mathcal{A}$ before releasing it. The scale of the variance of the noise is calibrated to the sensitivity of $\mathcal{A}$ [WIP]

\subsection{Empirical risk minimization}
Unless made explicit otherwise we will consider databases $\dset=\{\exz_i\}_{i=1}^n$ containing $n$ instances $\exz=(\exx_i,\exy_i)\in\xspace\times \yspace$ with $\xspace=\mathbb{R}^p$ and $\yspace=\{0,1\}$ sampled identically and independently (i.i.d.) from $\dsetspace$. We are trying to build a model $h_\theta: \xspace \rightarrow \yspace$ parameterized by $\theta \in \Theta \subseteq \mathbb{R}^p$, so it minimizes the expected loss $\ell(\theta)=\mathbb{E}_{x,y}[\ell(\theta ; \exz)]$, where $\ell(\theta; \exz)$ is the loss of the model $h_\theta$ on data point $\exz$.
One can approximate $\ell(\theta)$ by $\hat{R}(\theta ; \dset)=\frac{1}{n} \sum_{i=1}^n \ell\left(\theta ; \exz_i\right)$, the empirical risk of model $h_\theta$.  Empirical Risk Minimization (ERM) then minimizes an objective function $F(\theta,\dset)$ which adds to this empirical risk a regularization term $\psi(\theta)$ to find an estimate ${\hat{\theta}}$ of the model parameters:
%and $\psi(\theta)$, penalty on model parameters (e.g., $\L_1$ norm).

%\begin{definition}[Empirical Risk Minimization (ERM)]
$$
\hat{\theta} \in \underset{\theta \in \Theta}{\arg \min }\  F(\theta ; \dset):=\hat{R}(\theta ; \dset)+\gamma \psi(\theta)
$$
where $\gamma \geq 0$ is a trade-off hyperparameter.
%\end{definition}

\paragraph*{Logistic regression}
A popular and easy to analyze machine learning model is logistic regression.  We will use it in this paper to illustrate our idea.  In particular, logistic regression aims at fitting parameters $\theta$ such that the inner product $\theta \exx$ approximates $\exy$.  It uses the LogLoss loss function $\ellll(\exz) = \ellll((\exx,\exy)) = -\exy\log S(\theta\exx) -(1-\exy)\log(1-S(\theta\exx))$ with $S$ the logistic function (see Appendix \ref{app:logistic} for more details) and $y\in\{0,1\}$. The logistic regression model is widely used, especially within the medical field, mainly for its interpretability and its statistical properties. %One can get insights on the model or the underlying data just by reading the parameters of the model.

\subsection{Stochastic gradient descent}

To minimize $\Fll(\theta,\dset)=\ellll(\theta,\dset)+\gamma\psi(\theta)$ one can use stochastic gradient descent (SGD).  The SGD algorithm iteratively for a number of time steps $t=1\ldots T$ computes a gradient $\gradt = \nabla F(\thetat,\dset)$ on the current model $\thetat$ and updates the model using that gradient setting $\thetatsucc=\thetat - \eta(t) \gradt$ where $\eta(t)$ is a learning rate.

We consider federated learning settings where data is distributed over multiple data owners who want to collaborate to build a model but who don't want to reveal their own data.   They could encrypt the whole machine learning process, performing all operations using secure multi-party computation \cite{DavidSMPC2018} but that would be computationally very expensive.  A popular strategy is therefore to aggregate securely in each round the gradient each data owner computes on its local data together with an appropriate amount of noise, and then reveal the noisy aggregated gradient to an Aggregator agent who then updates the global model.  Secure aggregation can be performed in an efficient and robust way \cite{Dwork2006ourselves,Bonawitz2017a,SabaterMLJ2022}.  The downside is that in each iteration some information leaks, and it is the total of all these leaks that determines the privacy cost.

To be more concrete, in each iteration of the SGD algorithm, the data owners and the Aggregator collaboratively compute a noisy gradient ${\hat{G}}_t = \frac{1}{\batchsize} \sum_{i=1}^{\batchsize} \nabla F(\thetat,\batchi) + \noiset$ over a sample $\batchset=\{\batchi\}_{i=1}^{\batchsize}$  of the data set where $\noiset$ is appropriate noise.


Determining good values for the scale of the noise in the $b_t$ variables has been the topic of several studies.  One simple strategy starts by assuming an upper bound for the norm of the gradient.  Let us first define Lipschitz functions:

\begin{definition}[Lipschitz function]
  Let $\sLipschitz>0 .$ A function $f$ is $\sLipschitz$-Lipschitz with respect to some norm $\|\cdot\|$ if for all $\theta, \theta^{\prime} \in \Theta$ there holds
  $\left|f(\theta)-f\left(\theta^{\prime}\right)\right| \leq \sLipschitz\left\|\theta-\theta^{\prime}\right\|.$
  If $f$ is differentiable and $\|\cdot\|=\|\cdot\|_2$, the above property is equivalent to:
  $$
  \|\nabla f(\theta)\|_2 \leq \sLipschitz, \quad \forall \theta \in \Theta
  $$
\end{definition}

The key idea is then that from the model one can derive a constant $\sLipschitz$ such that the objective function is $\sLipschitz$-Lipschitz, while knowing bounds on the data next allows for computing a bound on the sensitivity of the gradient.  Once one knows sensitivity, one can determine the noise to be added from the privacy parameters as in Lemma \ref{lem:gaussmech}.

Several options have been explored in the literature.
Algorithm \ref{alg:dpsgd} shows an algorithm which is generic for the purpose of our explanation.
In \cite{bassily_differentially_2014}, an algorithm is proposed which for a given input $(\dset,\ell,\Theta,\epsilon,\delta,T,\batchsize,\eta,g_{\max})$ behaves  as a call to $\textsc{DP-SGD}(\dset,\ell,\Theta,\epsilon,\delta,T,\batchsize,\eta,g_{\max},"C","global")$ in Algorithm \ref{alg:dpsgd}.
In particular, it computes the total privacy cost by Dwork's strong composition theorem \cite{dwork_algorithmic_2013} (accounting method $"C"$), which states that if we perform $T$ operations which are $(\epsilon_i,\delta_i)$-DP ($i=1\ldots T$), then the total privacy cost is bounded by $(\sum_{i=1}^T \epsilon_i, \sum_{i=1}^T \delta_i)$.
% todo: correct composition
In \cite{abadi_deep_2016}, an algorithm is proposed which for a given input $(\dset,\ell,\Theta,\epsilon,\delta,T,\batchsize,\eta,g_{\max})$ behaves as a call to $\textsc{DP-SGD}(\dset,\ell,\Theta,\epsilon,\delta,T,\batchsize,\eta,g_{\max},"M","global")$ in Algorithm \ref{alg:dpsgd}.

\textcolor{blue}{Jan: Check whether these algorithms use fixed batch sizes, max gradient norms or other deviating things.}

\input{subfiles/algos/dp_sgd_logloss_l1_orig}
%\input{subfiles/algos/dp_sgd_logloss_l1}
\input{subfiles/algos/scale_noise}
