\section{Our approach}
\label{sec:approach}

% TODO:add remark in preliminaries on gloabal lipschtiz constant being the gradient clipping norm if dnn.
In this section, we investigate the benefits of Lipschitz constrained neural networks for designing DP-SGD algorithms. We demonstrate that these networks provide a global Lipschitz constant without requiring gradient clipping. Additionally, we show that taking into account the position of the current model in the search space (local Lipschitz value), rather than using the worst-case gradient norm (global Lipschitz value), can bring additional utilility for DP-SGD algorithms. 

% Finally, we highlight settings that always benefit from local Lipschitz constant such as using the $1$-norm regularizer.

\newcommand{\predy}{{\tilde y}}
\newcommand{\layerval}[1]{u_{{#1}}}
\newcommand{\layermid}[1]{v_{{#1}}}

\subsection{The statistical disadvantage of gradient clipping}
\label{sec:grad.clip.bias}

Several existing approaches follow gradient clipping as described in Algorithm \ref{alg:dpsgd}.  Essentially, to preserve privacy each gradient is clipped independently so its norm doesn't exceed a constant $C$.
A result of this clipping is however that the resulting model may be biased.
Consider the trivial example of a dataset $\mathcal{Z}=\mathcal{X}\times \mathcal{Y}$ where $\mathcal{X}=\{()\}$, i.e., there are $0$ features, and $\mathcal{Y}=\mathbb{R}$, and where we use the square loss $\ell(\predy,y)=(\predy-y)^2$.
Assume instances are drawn from the probability distribution $\mathcal{P}_\mathcal{Y}$.  Without any feature to base the prediction on, if the algorithm predicts $\predy$ the expected mean squared error is $\mathbb{E}_{y\sim\mathcal{P}_\mathcal{Y}}[(\predy-y)^2]=\text{var}(\mathcal{P}_\mathcal{Y}) + (\predy-\mu_\mathcal{Y})^2$ with $\mu_\mathcal{Y}$ the mean of $\mathcal{P}_\mathcal{Y}$.
The best a model can do is to always predict the same value $\mu_\mathcal{Y}$.
For a given prediction $\theta$ and target value $y_i$, the gradient is $2(y_i-\theta)$.  Clipping all gradient norms to a maximum of $1$ would mean we use the gradient $\min(1,\max(-1,2(y_i-\theta)))$. If our dataset would contain $n/2$ instances with $y_i=1$ and $n/2$ instances with $y_i=-1$ and if we would start with $\theta=1/2$ then we would have $n/2$ gradients $2(1-\theta)=1$ and $n/2$ gradients $\max(-1,2(-1-\theta))=-1$ to obtain a sum of all gradients of $0$, so we would be stuck on a point $\theta=1/2$ which is $1/2$ above the mean $\mu_\mathcal{Y}=0$.  Even when evaluating the gradient on a randomly sampled batch, it would be in expectation $0$ as long as $\theta$ is in the interval $[-1/2,1/2]$ and hence never converge to the right optimum.

\subsection{Lipschitz constant of Multi-Layer Perceptrons}
Let $f_{M L P}$ be a $K$-layer multi-layer perceptron as defined in \cref{def:mlp}.  We define $\layerval{k} = \rho_{k-1} \circ P_{k-1} \cdots \rho_1 \circ P_1 (x)$ (and hence $\layerval{1}=x$), $\layermid{k} = \rho_{k-1} \circ P_{k-1} (\layerval{k-1})$ and $\predy=f_{M L P}(\theta; x)=\layerval{K+1}$.  Applying the chain rule, the gradient of $\mathcal{L}$ with respect to the weight matrix  $M_k$ is
\begin{align}\label{eq:grad}
  \nabla_{M_k} \mathcal{L}(\theta, z) = \frac{\partial\ell(\predy,y)}{\partial\predy} \frac{\partial \layerval{K+1}}{\partial \layermid{k+1}} \layerval{k}.
\end{align}
In general, $\frac{\partial \layerval{i}}{\partial\layermid{j}}$ is a Jacobian matrix as both $\layerval{i}$ and $\layermid{j}$ are vectors, but in Eq \eqref{eq:grad} $\frac{\partial \layerval{K+1}}{\partial\layermid{k+1}}$ is a gradient vector as the final layer has only one output node.  Many common activation functions are $1$-Lipschitz, e.g., ReLU, tanh and Sigmoid.  With 1-Lipschitz activation functions and $\hat{L}_{P_i}$-Lipschtiz linear transformations $P_i$, when we combine the chain rule and the Cauchy-Schwarz inequality, we get
\begin{align}\label{eq:lip}
  \forall k \in [1, K-1], \left\|\frac{\partial \layerval{K+1}}{\partial \layermid{k+1}} \layerval{k}\right\|_2  %&
  \leq \prod_{i=k}^{K-1} \hat{L}_{P_i} \|\layerval{k}\|_2 %\nonumber\\ &
  \leq \prod_{i=k}^{K-1} \hat{L}_{P_i} \prod_{j=1}^{k-1} \hat{L}_{P_j} \|x\|_2 %\nonumber\\ &
 = \prod_{i=1}^{K-1} \hat{L}_{P_i} \|x\|_2.
\end{align}

  We represent the complete gradient $\nabla \mathcal{L}$ as a concatenation of the $\nabla_{M_k} \mathcal{L}$ partial gradients from \eqref{eq:grad}, and set $r = \max(\|x\|_2)$.  If $\ell$ is $\hat{L}_{\ell}$-Lipschitz then combining \eqref{eq:grad} and \eqref{eq:lip} we get
\begin{align}
\| \nabla \mathcal{L} \|_2
& = \left \| (\|\nabla_{M_k} \mathcal{L}(\theta, z)\|_2)_{1\leq k \leq K} \right \|_2 \nonumber \\
& \leq \left \| (\hat{L}_{\ell}\prod_{i=1}^{K-1} \hat{L}_{P_i} \|x\|_2)_{1\leq k \leq K} \right \|_2 \nonumber \\
  & \leq \left(\hat{L}_{\ell} \prod_{i=1}^{K-1} \hat{L}_{P_i}\right) r \sqrt{K}
    \label{eq:nableL.UB}
\end{align}


An obvious upper-bound for $\hat{L}_{P_i}$ is $\|M_i\|_2$, and that is what we use in our experiments for reasons of computational efficiency.
Still, \cite{scaman_lipschitz_2019} suggests a tighter overall upper-bound using the SVD decompositions $M_i=U_i \Sigma_i V_i^{\top}$.  In particular, $P_i$ is ${\hat L}_{P_i}$-Lipschitz with $\hat{L}_{P_i}= \max _{\sigma_i \in[0,1]^{n_i}}\left\|\tilde{\Sigma}_{i+1} V_{i+1}^{\top} \operatorname{diag}\left(\sigma_{i+1}\right) U_i \widetilde{\Sigma}_i\right\|_2$ where $\sigma_i$ corresponds to all possible derivatives of the activation gate, $n_i$ the number of nodes in the inner layer $i$ and $\tilde{\Sigma}_i=\Sigma_i \text { if } i \in\{1, K\} \text { and } \tilde{\Sigma}_i=\Sigma_i^{1 / 2}$ otherwise. %In both cases, the Lipschitz constant grows with the singular values of each $M_i$.
Unfortunately, applying the ideas of \cite{scaman_lipschitz_2019} directly in our experiments didn't show sufficiently good scaling and further optimizing the computational cost is outside the scope of the current paper.

%\subsection{Estimating Lipschitz constants of multi-layer perceptrons}
\subsection{Lipschitz constrained neural networks}

To prevent scaling the noise according to potentially unbounded values of our Lipschitz constants, we adopt a popular method used in the generative adversarial network (GAN) field, known as Lipschitz constrained neural networks.
Lipschitz constrained neural networks are neural networks that are constrained to representations of $L^g$-Lipschitz continuous functions. One popular approach to enforce Lipschitz constraints is by using spectral normalization, which scales the weights of the network by their largest singular value \cite{miyato2018spectral}.



Thus, with $C_i$ an upper bound on the spectral norm $\|M_i\|_2$, the gradient norm $\|\nabla \mathcal{L}\|_2$ is bounded by
\[L^g_{glob} = r\sqrt{K}\hat{L}_{\ell} \prod_{i=1}^{K-1} C_i.\]
Here, $L^g_{glob}$ is a global constant not depending on the parameters $\theta$.

At the cost of some extra computation time, we can at each iteration compute a local Lipschitz constant using the derivation above.  In particular, knowing the current position $\theta$ in the search space, which is publicly known in our setting, we can upper bound $\max_{Z\in \mathcal{Z}^n} \|\nabla \mathcal{L}(\theta,Z)\|$ using \eqref{eq:nableL.UB}, getting that  $\|\nabla \mathcal{L}\|_2$ is bounded by
\[L^g_{loc}(\theta) = r \sqrt{K} \hat{L}_{\ell} \prod_{i=1}^{K-1} \hat{L}_{P_i}.\]

%Thus, with $C_i$ the upper bound on the spectral norm of $M_i$, we can define the local and global lip constant as follows
%\begin{equation}\label{eq:lip_regression}
%  L^g =
%  \begin{cases}
%    r \hat{L}_{\ell} \prod_{i=1}^{K-1} C_i \sqrt{K} \text{ or } C,& \text{if } Lip = \text{"global"}\\
%    r \hat{L}_{\ell} \prod_{i=1}^{K-1} \hat{L}_{R_i} \sqrt{K},& \text{if } Lip = \text{"local"}
%  \end{cases}
%\end{equation}
%with $C$ the gradient clipping norm if chosen over enforcing Lipschitz continuity.

\subsection{Scaling the noise}

In this paper, we propose weight-clipping-based SGD.  The high level strategy can be seen in Algorith \ref{alg:dpsgd} with the $Clip=''weight''$ parameter setting.  Instead of clipping the gradient norm at some constant $C$ (in Line \ref{ln:grad.clip}) as in existing algorithms, we clip the weights in Line \ref{ln:weight.clip} where we denote by $\thetat_i$ the part of the gradient related to later $i$, i.e., the matrix $M_i$, at time step $t$.  The norm of each $\thetat_i$ is independently clipped to a constant $C_i$.
It is important to note that weight clipping does not suffer of statistical bias as discussed for gradient clipping in Section \ref{sec:grad.clip.bias}.  Indeed, weight clipping happens only once per iteration and doesn't affect the relative size of the gradients of the model with respect to different instances, while gradient clipping affects (possibly differently) the gradient of each training instance or data owner.

Algorithm \ref{alg:dpsgd} shows two variants of the algorithm using weight clipping: either we take into account the maximum gradient norm $L^g_{glob}$ for determining the noise scale (Line \ref{ln:Lg.global}) or we take into account the local maximum gradient norm $L^g_{loc}(\theta)$ (Line \ref{ln:Lg.local}).

The first variant is still similar in spirit to \cite{abadi_deep_2016}, where on just clips using a hyperparameter threshold.  This variant can be called as  $\textsc{DP-SGD}(\dset,r, K,\ell,\Theta,\sigma,T,\batchsize,\eta,"weight",C,"global")$ in \cref{alg:dpsgd}. While this algorithm has been suggested in previous work \cite{ziller_sensitivity_2021}, it has not been empirically evaluated. In \cref{sec:experiment}, we demonstrate its superiority over the gradient clipping method.

The second, novel variant estimates an upper bound for the gradient based on the current position $\theta$ in the search space. It is called as $\textsc{DP-SGD}(\dset,r, K,\ell,\Theta,\sigma,T,\batchsize,\eta,"weight",C,"local")$ in \cref{alg:dpsgd}. The main difference with previous algorithms \cite{bassily_differentially_2014,abadi_deep_2016} is that the bound on the sensitivity of the gradient isn't constant across all updates of the SGD anymore.  One can note that $\theta$ is computed privately throughout the SGD algorithm, and can hence be shared with all data owners contributing to the secure aggregation of the gradient.

\begin{theorem}\label{th:dpsgd}
  $\textsc{DP-SGD}(\dset,r, K,\ell,\Theta,\sigma,T,\batchsize,\eta,"weight", C,"local")$ is $(\epsilon, \delta)$-differentially private
\end{theorem}

Scaling the noise using a smaller upper bound of the sensitivity of the gradient leads to adding less noise and eventually getting better performances. Computing this bound at each update of the SGD gives the opportunity to have smaller values than the global bound. We show in \cref{sec:experiment} how this method further enhances performance.
