\section{Experimental results}\label{sec:experiment}
In this section, we present an empirical comparison of the performance of three different algorithms of DP-SGD. Specifically, we compare DP-SGD based on Lipschitz constrained neural networks with DP-SGD based on gradient clipping. In the former, we evaluate the performance by scaling the differential private noise on both local and global Lipschitz constants, while the latter is the popular method introduced by \cite{abadi_deep_2016}. We evaluate the algorithms on $11$ popular datasets, for each dataset, we report two evaluation metrics: the area under the R.O.C. curve (AUC) and the ratio of average standard deviation of noise added to the weights by the norm of the gradient at each time step. The former metric measures the overall utility of the model, while the latter gives a more direct measure of the noise reduction due to our improvements.
% shows the relative noise added.

%As described in \cref{sec:approach}, we implement all DP-SGD methods using a
We use for all datasets a
two-layer perceptron, data of $n$ samples are randomly selected with replacement and loaded in batches of $10$. We fixed the target privacy parameter $\delta=(1/n)^2$, and explore several sets of hyperparameters, including the number of hidden units, number of epochs, learning rate, gradient and weight clipping norm and privacy level $\epsilon$. We use the Python \verb|dp_accounting.calibrate_dp_mechanism| search function from Tensorflow \cite{tensorflow2015-whitepaper} to calibrate the corresponding noise multiplier value $\sigma$ (function $\textsc{SearchDPNoise}$ in \cref{alg:dpsgd}).

Unless otherwise specified, all experiments were repeated 10 times and plots contain next to the averages also the standard deviations.
The computations were carried out on a system equipped with an Intel® Xeon® E5-2696V2 Processor, 8 vCPUs, and 52 GB RAM. The total runtime of the experiments was approximately 50 hours, which corresponds to an estimated carbon emission of $1.96$ kg, as reported in \cite{lacoste2019quantifying}. We first focus on the MNIST dataset and then summarize the results for all $11$ datasets.
More details on the experimental setup can be found in the supplementary material.

\subsection{Exploration using MNIST}
We first visualize some relevant metrics using the MNIST dataset.  More complete plots can be found in supplementary material.
%\janfoot{We won't have space to describe all datasets, and only describing one of the 12 datasets isn't very systematic. I suggest to make a supplementary material file with additional information, among others for each dataset a short description, characteristics (number of instances, number of features, \ldots) and citation (where can it be downloaded).  If possible we should also upload all scripts needed to reproduce our experiments (which may then start with wget commands to download the datasets).}\antoinefoot{ok, I'll do it}
The MNIST dataset contains handwritten digits images and the corresponding labels (0-9). It is split in one training dataset of 60,000 examples and a testing dataset of 10,000 examples. In order to apply a multi-layer perceptron on it, we expand the 28x28 size grey-level images in one normalized vector of 784 coordinates.


% \begin{figure}[!htb]
%    \begin{minipage}{0.48\textwidth}
%      \centerline{\includegraphics[width=1.0\columnwidth]{../../src/results/exp_0414/epsilon.png}}
%      \caption{AUC for $2$-layer perceptron on the MNIST dataset per values of $\epsilon$, with $\delta=1 / 10000^2$}
%      \label{fig:mnist_auc_epsilon}
%    \end{minipage}\hfill
%    \begin{minipage}{0.48\textwidth}
%      \centerline{\includegraphics[width=1.0\columnwidth]{../../src/results/exp_0414/sigma.png}}
%      \caption{Ratio of average variance of added noise by the gradient norm for $2$-layer perceptron on the MNIST dataset per values of $\epsilon$, with $\delta=1 / 10000^2$}
%      \label{fig:mnist_sigma}
%    \end{minipage}
% \end{figure}

\begin{figure}[ht]
  \vskip 0.2in
    \begin{center}
    \subfigure[]{\includegraphics[width=0.48\columnwidth]{../../src/results/exp_0515/epsilon_perf_mean.png}}
    \subfigure[]{\includegraphics[width=0.48\columnwidth]{../../src/results/exp_0515/sigma_perf_mean.png}}
    \caption{AUC (a) and Ratio of average standard deviation of added noise by the gradient norm (b) for $2$-layer perceptron on the MNIST dataset per values of $\epsilon$, with $\delta=1 / 10000^2$, error bars showing $1$ standard deviation}
    \label{fig:mnist_auc_sigma}
  \end{center}
\end{figure}

\begin{figure}[ht]
  \vskip 0.2in
    \begin{center}
    \subfigure[]{\includegraphics[width=0.48\columnwidth]{../../src/results/exp_0514/weightclip_perf.png}} \label{fig:mnist_norm}
    \subfigure[]{\includegraphics[width=0.48\columnwidth]{../../src/results/exp_0509/epochs_perf.png}}
    \caption{AUC per weight clipping norm (a) and number of epochs (b) at $\epsilon=1$ and $\delta=1/10000^2$, error bars showing $1$ standard deviation}
    \label{fig:mnist_auc_hyperparams}
  \end{center}
\end{figure}

As seen in \cref{fig:mnist_auc_sigma} (a), using weight clipping yields better results in terms of AUC compared to using gradient clipping for $\epsilon \leq 10$. Moreover, among the weight clipping methods, the one based on the local Lipschitz value consistently outperforms the one based on the global Lipschitz value, especially as the weight clipping norm increases. This observation is supported by \cref{fig:mnist_auc_hyperparams} (a), which shows that the local Lipschitz value remains lower than the upper bound imposed by the global Lipschitz value for all clipping norm values. These findings are further corroborated by \cref{fig:mnist_auc_sigma} (b). One can also see from \cref{fig:mnist_auc_sigma} (b) that the gradient clipping method introduces less noise relative to the gradient norm. However, it is plausible to assume that the weight clipping norm method compensates for the higher amount of noise added by inducing less bias compared to the gradient clipping method.

The results depicted in \cref{fig:mnist_auc_hyperparams} (b) indicate that weight clipping utilizing the local Lipschitz value leads to superior performance in comparison to using the global Lipschitz value, especially for a low number of epochs, which is corroborated by a significant difference confirmed by a Wilcoxon Signed-rank test.

\subsection{Comparison of clipping strategies}
We next perform a more systematic comparison of the three alternative clipping strategies on $11$ datasets.
%We show results of our experiment on $10$ other datasets: the adult income dataset \cite{misc_adult_2}, the android permissions dataset \cite{misc_naticusdroid_(android_permissions)_dataset_722}, the breast cancer dataset \cite{misc_breast_cancer_wisconsin_(diagnostic)_17}, the default credit dataset \cite{misc_default_of_credit_card_clients_350}, the dropout dataset \cite{misc_predict_students'_dropout_and_academic_success_697}, the German credit dataset \cite{misc_statlog_(german_credit_data)_144}, the nursery dataset \cite{misc_nursery_76}, the patient survival dataset \cite{mitisha_agarwal_2021}, the thyroid dataset \cite{misc_thyroid_disease_102} and the yeast dataset \cite{misc_yeast_110}.
For each of them, we compare the weight clipping method, based on local and global Lipschitz value, with the gradient clipping method in terms of AUC. To do so, we perform a Wilcoxon Signed-rank test on $10$ measures of AUC for each dataset between the DP-SGD based on the gradient clipping and the one based on the weight clipping with local Lipschitz value.


\begin{table}
  \caption{AUC per datasets and methods at $\epsilon=1$, in bold the best result and underlined when the difference with best result is not statistically significant at a level of confidence of 5\%}
  \label{tab:auc}
  \centering
  \begin{tabular}{lrrr}
    \toprule
    Methods &  gradient global &  weight global &  weight local \\
    Datasets (\#instances $n$ $\times$ \#features $p$)     &                  &                &               \\
    \midrule
    Adult Income (48842x14) \cite{misc_adult_2}    &            0.563 &          0.674 &         \textbf{0.732} \\
    Android   (29333x87)\cite{misc_naticusdroid_(android_permissions)_dataset_722}       & 0.773 &          \textbf{0.866} &         \underline{0.826} \\
    Breast Cancer (569x32) \cite{misc_breast_cancer_wisconsin_(diagnostic)_17}   &            0.865 &          0.801 &         \textbf{0.925} \\
    Default Credit (30000x24) \cite{misc_default_of_credit_card_clients_350}  &             0.580 &          \underline{0.586} &         \textbf{0.595} \\
    Dropout       (4424x36) \cite{misc_predict_students'_dropout_and_academic_success_697}   &            0.608 &          0.616 &         \textbf{0.636} \\
    German Credit (1000x20) \cite{misc_statlog_(german_credit_data)_144}          &             0.549 &          0.604 &         \textbf{0.615} \\
    MNIST (70000x724) \cite{deng2012mnist}  &            0.842 &          0.895 &         \textbf{0.899} \\
    Nursery        (12960x8) \cite{misc_nursery_76} &            0.750 &          0.825 &         \textbf{0.890} \\
    Patient Survival (85519x104) \cite{mitisha_agarwal_2021} &            \textbf{0.681} &          \underline{0.680} &         \underline{0.677} \\
    Thyroid  (7200x5) \cite{misc_thyroid_disease_102}       &            0.587 &          0.598 &         \textbf{0.597} \\
    Yeast  (1484x8) \cite{misc_yeast_110}         &            0.591 &          0.623 &         \textbf{0.631} \\
    \bottomrule
  \end{tabular}
\end{table}


As demonstrated in \cref{tab:auc}, the DP-SGD algorithm with weight clipping consistently achieves better performance in terms of AUC compared to the gradient clipping method, with the exception of the Patient Survival dataset where the difference is not statistically significant. This observation holds true across datasets of varying length and width, as well as for tasks involving imbalanced datasets such as the Dropout or Default Credit datasets. Furthermore, the local Lipschitz value outperforms the global Lipschitz value method at a confidence level of $5$\%, with the exception of the Android dataset, Default Credit dataset, and Patient Survival dataset where the difference is not significant.

In summary, using the weight clipping method in DP-SGD consistently outperforms the gradient clipping method, with the local variant providing further gains.
%. Additionally, scaling the noise on the local Lipschitz value not only avoids the privacy budget spent on searching for the optimal clipping norm, but also leads to the best performance.
