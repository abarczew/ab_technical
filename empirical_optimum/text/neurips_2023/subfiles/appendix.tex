\section{Algorithm}
\subsection{1-norm regulizer algorithm: $\textsc{AddRegularization}$}\label{app:alg_add_regularization}
\cref{alg:add_regularization} details how regularization is applied within a $\textsc{DP-SGD}$ as described by \cite{tsuruoka_stochastic_2009}.

\input{subfiles/algos/add_regularization}

\section{Derivations}

\subsection{The logistic function}
\label{app:logistic}

Recall that the logistic function is
\begin{equation}
S(x) = \frac{1}{1+e^{-x}}
\end{equation}
Its derivative is
\begin{eqnarray}
  S'(x) %&=& \frac{\hbox{d}S(x)}{\hbox{d} e^{-x}} \frac{\hbox{d} e^{-x}}{\hbox{d}(-x)} \frac{\hbox{d}(-x)}{\hbox{d}x} \nonumber \\
  &=& \frac{-1}{(1+e^{-x})^2} e^{-x} (-1) \nonumber \\
        &=& \frac{1}{1+e^{-x}} \frac{e^{-x}}{1+e^{-x}} \nonumber \\
        &=& S(x) \frac{1}{e^x+1} \nonumber \\
        &=& S(x) (1-S(x))
\end{eqnarray}

We consider the loss function
\begin{equation}
  \ellll(\theta; x,y) = -y \log(S(\theta x)) - (1-y) \log(1-S(\theta x))
\end{equation}

The gradient to $\theta$ is
\begin{eqnarray}
  \nabla_\theta \ellll(\theta; x,y)
  &=& \nonumber -y \nabla_\theta \log(S(\theta x)) - (1-y) \nabla_\theta \log(1-S(\theta x)) \\
  &=& \nonumber -\frac{y}{S(\theta x)} \nabla_\theta S(\theta x) - \frac{(1-y)}{1-S(\theta x)} \nabla_\theta (1-S(\theta x)) \\
  &=& \nonumber -\frac{y}{S(\theta x)} S(\theta x) (1-S(\theta x)) \nabla_\theta (\theta x) - \frac{(1-y)}{1-S(\theta x)} (-1) (1-S(\theta x)) S(\theta x) \nabla_\theta (\theta x) \\
  &=& \nonumber - y (1-S(\theta x)) \nabla_\theta (\theta x) + (1-y) S(\theta x) \nabla_\theta (\theta x) \\
  &=& \nonumber - y (1-S(\theta x)) x + (1-y) S(\theta x) x \\
  &=& (S(\theta x)-y) x
  \end{eqnarray}

Since $x \in [-1, 1]^p$, $S(x;\theta)$ is maximal when $\forall i \in [p], x_i = \text{sign}(\theta_i)$. For the same reason $S(x;\theta) - 1$ is maximal when $\forall i \in [p], x_i = -\text{sign}(\theta_i)$. As $S(x;\theta) - 1 = -S(-x;\theta)$, all in all we get

\begin{equation}\label{eq:leq_local}
\begin{split}
  \left|\frac{\partial\ell^{ll}}{\partial\theta_j}(x, y)\right|
  & \leq \frac{|\operatorname{sign}(\theta_j)|}{1+e^{-\|\theta\|_1}} \\
  & \leq \frac{1}{1+e^{-\|\theta\|_1}}
\end{split}
\end{equation}

As for all $\theta \in \Theta, \frac{1}{1+e^{-\|\theta\|_1}} \leq 1$,  we have:
\begin{equation}\label{eq:lip_local_global}
  L^g =
  \begin{cases}
    \sqrt{p},& \text{if } Lip = \text{"global"}\\
    \frac{\sqrt{p}}{1+e^{-\|\theta\|_1}},& \text{if } Lip = \text{"local"}
  \end{cases}
\end{equation}

\subsection{The linear regression}\label{sec:linear_reg}
Recall that the cost function $J$ of linear regression model of parameter $\theta$ is
\begin{equation}
  J(x,y; \theta) = \frac{1}{2} (x\theta^{\top} - y)^2
\end{equation}
with $x,y \in [-1,1]^p \times \mathbbm{R}$.

It's derivative is
\begin{equation}
  \nabla_\theta J(x,y;\theta) = (x\theta^{\top} - y)x
\end{equation}

Hence the $2$-norm of the gradient of $J$ is

\begin{align}
  \|\nabla_\theta J(x,y;\theta)\|_2 & = \|(x\theta^{\top} - y) x\|_2& \\
  & \leq \sqrt{p} \|(x\theta^{\top} - y)\|_2 & \text{(Cauchy-Schwarz inequality)}\\
  & \leq \sqrt{p} (\|x\theta^{\top}\|_2 + |y|)& \text{(triangular inequality)}\\
  & \leq \sqrt{p} (\|x\theta^{\top}\|_2 + \max|y|)&\\
  & \leq \sqrt{p} (\|x\|_2\|\theta\|_2 + \max|y|)&  \text{(Cauchy-Schwarz inequality)}\\
  & \leq p \|\theta\|_2 + \sqrt{p}\max|y|&
\end{align}

 So, we have
 \begin{equation}\label{eq:lip_regression}
   L^g =
   \begin{cases}
     p \max\|\theta\|_2 + \sqrt{p}\max|y|,& \text{if } Lip = \text{"global"}\\
     p \|\theta\|_2 + \sqrt{p}\max|y|,& \text{if } Lip = \text{"local"}
   \end{cases}
 \end{equation}
\newpage

\subsection{Relu activation function for classification tasks}
Recall that the softmax function of $x_i$ the $i$-th component of the vector $x$ of length $c$ is
$$
  \operatorname{softmax_i}(x) = \frac{e^{x_i}}{\sum_{j=1}^c e^{x_j}}
$$
and the Relu is defined as
$$
  \operatorname{Relu}(x) = \max(0, x).
$$
The forward pass of the model is as follows:
\begin{align}
  \boldsymbol{x} & =\text { input } \\
  \boldsymbol{z} & =\boldsymbol{W} \boldsymbol{x} \\
  \boldsymbol{h} & =\operatorname{Relu}(\boldsymbol{z}) \\
  \hat{\boldsymbol{y}} & =\operatorname{softmax}(\boldsymbol{h}) \\
  \ell & =C E(\boldsymbol{y}, \hat{\boldsymbol{y}})
\end{align}

Let's begin with the derivative of the softmax function:
$$
\frac{\partial}{\partial h_i} \operatorname{softmax}_i(h)=\frac{\partial}{\partial h_i} \frac{e^{h_i}}{\sum_j e^{h_j}}
$$
Using the quotient rule:

\begin{align}
\frac{\partial}{\partial h_i} \operatorname{softmax}_i(h) & =\frac{\left(\frac{\partial}{\partial h_i} e^{h_i}\right) \sum_j e^{h_j}-e^{h_i} \frac{\partial}{\partial h_i} \sum_j e^{h_j}}{\left(\sum_j e^{h_j}\right)^2} \\
& =\frac{e^{h_i} \sum_j e^{h_j}-e^{h_i} e^{h_i}}{\left(\sum_j e^{h_j}\right)^2} \\
& =\frac{e^{h_i}}{\sum_j e^{h_j}} \frac{\left(\sum_j e^{h_j}-e^{h_i}\right)}{\sum_j e^{h_j}} \\
& =\operatorname{softmax}_i(h)\left(1-\operatorname{softmax}_i(h)\right) \\
& = \hat{y}_i (1 - \hat{y}_i)
\end{align}

And when $i\neq l$:

\begin{align}
\frac{\partial}{\partial h_l} \operatorname{softmax}_i(h) & =\frac{\left(\frac{\partial}{\partial h_l} e^{h_i}\right) \sum_j e^{h_j}-e^{h_i} \frac{\partial}{\partial h_l} \sum_j e^j}{\left(\sum_j e^{h_j}\right)^2} \\
& =\frac{0-e^{h_i} e^{h_l}}{\left(\sum_j e^{h_j}\right)^2} \\
& =-\operatorname{softmax}_i(h) \operatorname{softmax}_l(h) \\
& = -\hat{y}_i \hat{y}_l
\end{align}

So, when we apply the derivative of the cross-entropy on top, we get:
\begin{align}
  \frac{\partial\ell}{\partial h} & = \left(- y_i \frac{\hat{y}_i (1 - \hat{y}_i)}{\hat{y}_i} - \sum_{l\neq i} y_l \frac{-\hat{y}_i \hat{y}_l}{\hat{y}_l} \right)_i\\
  & = \left(- y_i(1-\hat{y}_i) + \sum_{l\neq i} \hat{y}_i y_l \right)_i\\
  & = \left(- y_i + \hat{y}_i \sum_l y_l \right)_i\\
  & = (\hat{y} - y)^{\top}
\end{align}

Now, we focus on $\frac{\partial h}{\partial W}$,
\begin{align}
  \frac{\partial h}{\partial W} & = \operatorname{Relu}^{\prime} (z) x^{\top}\\
  & = \operatorname{sign}(h) \mathbbm{1}_{n_W} x^{\top}
\end{align}
with $\mathbbm{1}_{n_W}$ a vector of ones of size $(n_W, 1)$ and $n_W$ the number of rows of $W$.

Finally, we can apply the chain rule:
\begin{align}
  \frac{\partial\ell}{\partial W} & = \frac{\partial\ell}{\partial h} \frac{\partial h}{\partial W} \\ \label{eq:der_relu}
  & = (\hat{y} - y)^{\top}\operatorname{sign}(h)\mathbbm{1}_{n_W}x^{\top}
\end{align}

\subsection{Relu activation function for regression tasks}

The forward pass of the model is as follows:
\begin{align}
  \boldsymbol{x} & =\text { input } \\
  \boldsymbol{z} & =\boldsymbol{W} \boldsymbol{x} \\
  \boldsymbol{h} & =\operatorname{Relu}(\boldsymbol{z}) \\
  \hat{\boldsymbol{y}} & =\boldsymbol{U}\boldsymbol{h} \\
  \ell & =M S E(\boldsymbol{y}, \hat{\boldsymbol{y}})
\end{align}
with $x$ a column  vector of $p$ elements, $W$ a matrix of size $(q, p)$, $z$ a column vector of $q$ elemnts, $h$ a column vector of $q$ elements, $U$ a row vector of $q$ elements, $\hat{y}$ a scalar and $y$ a scalar.

\cref{sec:linear_reg} gives directly $\frac{\partial \ell}{\partial U}$, namely
\begin{equation}\label{eq:der_lin_reg}
  \frac{\partial \ell}{\partial U} = (U h - y)h^{\top}.
\end{equation}
Hence,
\begin{equation}
  \left \|\frac{\partial \ell}{\partial U} \right\|_2 \leq q\|U\|_2 + \sqrt{q}\max |y| .
\end{equation}
with $q$ the length of $h$ i.e., the number of hidden units.
As for $\frac{\partial \ell}{\partial W}$, the chain rule gives
\begin{align}
  \frac{\partial \ell}{\partial W} & = \frac{\partial\ell}{\partial h} \frac{\partial h}{\partial W} & \\
  & = \frac{\partial\ell}{\partial h} \operatorname{sign}(h) \mathbbm{1}_{n_W} x^{\top} & \text{(see \cref{eq:der_relu})} \\
  & = (U h - y)U \operatorname{sign}(h) \mathbbm{1}_{n_W} x^{\top} & \text{(see \cref{eq:der_lin_reg})} .
\end{align}
Hence,

\begin{align}
  \left \| \frac{\partial \ell}{\partial W} \right \|_2
  & =  \| ( (U h - y) U \operatorname{sign}(h)x_1, \dots, & \\
  & \quad\quad\qquad (U h - y) U \operatorname{sign}(h)x_p  ) \|_2 & \\
  & =  \|  ( (U h - y) \sum_i U_i x_1, \dots, & \\
  & \qquad\qquad (U h - y) \sum_i U_i x_p  )  \|_2 & \\
  & \leq  (\|U\|_2 \|h\|_2 + |y|)   \left |\sum_i U_i \right |  \|x\|_2 & \\
  & \leq \sqrt{p} \left |\sum_i U_i \right | (\|U\|_2 \|\operatorname{Relu}(Wx)\|_2 + \max|y|) & \\
  & \leq \sqrt{p} \left |\sum_i U_i \right | (\|U\|_2 \|W\|_2\|x)\|_2 + \max|y|) &  \\
  & \leq \sqrt{p} \left |\sum_i U_i \right | (\sqrt{p}\|U\|_2 \|W\|_2 + \max|y|)
\end{align}

\subsection{Generalization to multilayer perceptrons}
\begin{definition}
  A $K$-layer Multi-Layer-Perceptron $f_{M L P}: \mathbb{R}^n \rightarrow \mathbb{R}^m$ is the function
  $$
  f_{M L P}(x)=T_K \circ \rho_{K-1} \circ \cdots \circ \rho_1 \circ T_1(x),
  $$
  where $T_k: x \mapsto M_k x+b_k$ is an affine function and $\rho_k: x \mapsto\left(g_k\left(x_i\right)\right)_{i \in [ 1, n_k ]}$ is a non-linear activation function.
\end{definition}

We consider the loss function
\begin{align}
  \mathcal{L}(f_{M L P}; x,y) & = \ell(f_{M L P}(x), y)\\
  & = \ell(\hat{y},y)
\end{align}
with $\ell$ the error function e.g. MSE or CE.

Applying the chain rule, the gradient to $M_k$ is
\begin{align}\label{eq:grad_k}
  \nabla_{M_k} \mathcal{L} = \frac{\partial\ell}{\partial\hat{y}}(\hat{y}, y)\frac{\partial f_{M L P}}{\partial z_{k+1}}z_{k}
\end{align}
with $z_k = \rho_{k-1} \circ T_{k-1} \circ \cdots \circ \rho_1 \circ T_1(x)$ and $z_1 = x$.

With 1-Lipschitz activation functions (e.g. ReLU, Leaky ReLU, SoftPlus, Tanh, Sigmoid, ArcTan or Softsign), when we combine the chain rule and the Cauchy-Schwarz inequality, we get
\begin{align}\label{eq:lipbound}
  \forall k \in [1, K-1], \left\|\frac{\partial f_{M L P}}{\partial z_{k+1}}z_{k}\right\|_2
  & \leq \prod_{i=k}^{K} \| M_i \|_2 \|z_k\|_2\\
  & \leq \prod_{i=k}^{K} \| M_i \|_2 \prod_{i=1}^{k-1} \| M_i \|_2 \|x\|_2\\
  & \leq \prod_{i=1}^{K} \| M_i \|_2 \|x\|_2
\end{align}

According to \cite{scaman_lipschitz_2019}, an upper bound of the Lipschitz constant of each layer can be tighter than $\|M_i\|_2$ thanks to the SVD decomposition of each matrix $M_i=U_i \Sigma_i V_i^{\top}$. Let's call $\hat{L}_k$ this upper bound so we have
\begin{align}
  \forall k \in [1, K-1], \left\|\frac{\partial f_{M L P}}{\partial z_{k+1}}z_{k}\right\|_2
  & \leq \prod_{i=1}^{K}\hat{L}_i \|x\|_2\\
  & \leq \hat{L}_{M L P} \|x\|_2.\label{eq:lipbound_tight}
\end{align}


So, combining \eqref{eq:grad_k} and \eqref{eq:lipbound_tight}
\begin{equation}
  \|\nabla_{M_k} \mathcal{L} \|_2 \leq \left\|\frac{\partial\ell}{\partial\hat{y}}(\hat{y}, y)\right\|_2\hat{L}_{M L P} \|x\|_2.
\end{equation}

We define the total norm of the gradients as the $2$-norm of the $2$-norms of the vector of gradients, as defined in PyTorch. Hence, with $\hat{L}_{\ell} = \max \left\|\frac{\partial\ell}{\partial\hat{y}}(\hat{y}, y)\right\|_2$,
\begin{align}
  \| \nabla \mathcal{L} \|_2 & \leq \left \| (\hat{L}_{\ell}\hat{L}_{M L P} \|x\|_2)_{1\leq k \leq K} \right \|_2 \\
  & \leq \hat{L}_{\ell} \hat{L}_{M L P} \|x\|_2\sqrt{K}\\
  & \leq r \hat{L}_{\ell} \hat{L}_{M L P} \sqrt{K}
\end{align}
with $r = \max(\|x\|_2)$.

To conclude,
\begin{equation}\label{eq:lip_regression}
  L^g =
  \begin{cases}
    r \hat{L}_{\ell} B_{M L P} \sqrt{K} \text{ or } C,& \text{if } Lip = \text{"global"}\\
    r \hat{L}_{\ell} \hat{L}_{M L P} \sqrt{K},& \text{if } Lip = \text{"local"}
  \end{cases}
\end{equation}
with $C$ the gradient norm clipping bound, $B_{M L P} = \prod_{i=1}^K \lambda_i$ and $\lambda_i$ the upper bound on the spectral norm of $M_i$.
