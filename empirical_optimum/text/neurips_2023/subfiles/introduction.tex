

\section{Introduction}
\label{sec:intro}

While machine learning allows for extracting statistical information from data with both high economical and societal value, there is a growing awareness of the risks for data privacy and confidentiality.  Differential privacy \cite{dwork_algorithmic_2013} has emerged as an important metric for studying statistical privacy.

Due to the popularity of deep neural networks (DNNs) and similar models, one of the recently most trending algorithmic techniques in machine learning has been stochastic gradient descent (SGD), which is a technique allowing for iteratively improving a candidate model using the gradient of the objective function on the data.

In this paper we consider a federated learning setting, i.e., we assume the data is distributed over multiple data owners who want to collaboratively learn a model on the union of their datasets without disclosing their private data.  We assume there is a party, which we call the aggregator, which takes the role of coordinator of this collaborative process.

Making an algorithm differentially private requires that noise is added to each piece of information which can be seen by an adversary.  We consider a classic strategy of federated learning of DNNs where in each iteration an aggregator broadcasts the current model parameters, after which all data owners compute locally the gradients of the objective function for this global model on their private data, securely aggregate these gradients and send the aggregated gradient to the aggregator.

To achieve differential privacy with a minimum amount of noise, it is important to be able to bound precisely the sensitivity of the information which the participants will observe.  In the case of federated SGD, one approach is to bound the sensitivity of the gradient by assuming the objective function is Lipschitz continuous \cite{bassily_differentially_2014}.
Various improvements exist in the case one can make additional assumptions about the objective function.  For example, if the objective function is strongly convex, one can bound the number of iterations needed and in that way avoid to have to distribute the available privacy budget over too many iterations \cite{bassily_private_2019}.
  In the case of DNN, the objective function is not convex and typically not even Lipschitz continuous.  Therefore, a common method is to 'clip' contributed gradients \cite{abadi_deep_2016}, i.e., to divide gradients by the maximum possible norm they may get.  These normalized gradients have bounded norm and hence bounded sensitivity.

In this paper, we argue that gradient clipping may not lead to optimal statistical results, and we propose instead to use weight clipping, an idea suggested
%\antoinefoot{ Their contribution is on automatic differentiation and they show an application of it on DP-SGD. They define DP-SGD as the training of DNN with DP.}
\cite{ziller_sensitivity_2021}
but to the best of our knowledge not investigated yet in depth.  Moreover, we also propose to consider the maximum gradient norm given the current position in the search space rather than the global maximum gradient norm, as this leads to additional advantages.
In particular, our contributions are as follows:
\begin{itemize}
\item We present a new DP-SGD algorithm that (1) enforces bounded sensitivity of the gradients by bounding the model weights, and (2) adaptively upper bounds the gradient norm depending on the (publicly known) current model.  We argue that such weight clipping doesn't suffer from the bias which the classic gradient clipping can cause.
\item We show improved differential privacy guarantees for the federated SGD setting where the aggregated gradient is the leaked information.   %, which are in worst case equivalent to existing bounds but could in practice allow for using a substantially smaller amount of noise.
   We argue that the weight clipping strategy doesn't suffer from the bias which the classic gradient clipping can cause.
\item We present an empirical evaluation, confirming that on a range of popular datasets the amount of noise needed to achieve the same level of differential privacy is lower, and this affects favorably the performance of the trained models.
%\item We also empirically show that adaptively bounding the gradient sensitivity based on the current position in the search space in practice avoids the need to search for an appropriate value for the hyperparameter specifying the maximal weight or gradient norm. \antoinefoot{we only show it for mnist}
\end{itemize}

%In this paper, we explore an orthogonal direction.  Our main idea is that if anyway the aggregated gradients are public and hence also the global model after each iteration of the federated SGD algorithm, we can exploit the public knowledge about the current location of the model parameters in the search space rather than relying on a global bound on the norm of the gradient.  Indeed, at each point in the search space we can compute publicly what is the maximal possible gradient norm we can observe given any dataset.  This gradient norm is often smaller than the worst case gradient norm at the worst case point in the search space. For DNNs, this approach entails computing and constraining the worst-case bound without significantly affecting the model's performance.

%The contributions of the paper are therefore the following:
%\begin{itemize}
%\item We present a new DP-SGD algorithm that enforce the Lipschitz continuity to bound the worst case gradient norm. \textcolor{blue}{Antoine: I've seen one paper suggesting gradient clipping but no implementation or experiements, can we still say that our algorithm with global lipschtiz constant based on weight clipping is new?}
%\item We show improved privacy guarantees for the SGD setting where the aggregated gradient is the leaked information, which are in worst case equivalent to existing bounds but could in practice allow for using a substantially smaller amount of noise.
%\item We present an empirical evaluation, confirming that on a range of popular datasets the amount of noise needed to achieve the same level of differential privacy is lower, and this affects favorably the performance of the trained model.
%\item We compare our approach to standard DP-SGD based on gradient clipping, and our results show that our method outperforms this baseline approach.
%\end{itemize}

%* contributions:
%  - propose new algorithm, considering separately the noise in each dimension and exploiting sparsity
%  - Based on this we prove a new bound, allowing for fewer noise
%  - perform experiments

% * Organization of paper ....

The remainder of this paper is organized as follows.  First, we review a number of basic concepts, definitions and notations in Section \ref{sec:prelim}.  Next, we present our new method in Section \ref{sec:approach} and present an empirical evaluation in Section \ref{sec:experiment}.  We discuss related work in Section \ref{sec:related} and limitations in Section \ref{sec:limitations}.  Finally, we provide conclusions and directions for future work in Section \ref{sec:concl}.
