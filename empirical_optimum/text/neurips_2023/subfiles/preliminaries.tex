
\newcommand{\dspace}{\mathcal{Z}}
\newcommand{\dsetspace}{\mathcal{Z}^*}
\newcommand{\dset}{Z}
\newcommand{\dseta}{{Z^1}}
\newcommand{\dsetb}{{Z^2}}
\newcommand{\adjdsets}{{\mathcal{Z}^*_\sim}}
\newcommand{\exz}{z}
\newcommand{\exx}{x}
\newcommand{\exy}{y}
\newcommand{\xspace}{{\mathcal{X}}}
\newcommand{\yspace}{{\mathcal{Y}}}
\newcommand{\Fll}{{F^{ll}}}
\newcommand{\sLipschitz}{{L^g}}
\newcommand{\vLipschitz}{L}
\newcommand{\thetat}{{{\tilde{\theta}}^{(t)}}}
\newcommand{\thetaT}{{{\tilde{\theta}}^{(T)}}}
\newcommand{\thetaone}{{{\tilde{\theta}^{(1)}}}}
\newcommand{\thetatsucc}{{{\tilde{\theta}}^{(t+1)}}}
\newcommand{\noiset}{{b^{(t)}}}
\newcommand{\gradt}{{g^{(t)}}}
\newcommand{\gradtv}{{g^{(t)}_v}}
\newcommand{\batchset}{{V}}
\newcommand{\batchi}{{v_i}}
\newcommand{\batchsize}{s}

\section{Preliminaries and background}\label{sec:prelim}
In this section, we briefly review differential privacy, empirical risk minimization (ERM) and differentially private stochastic gradient descent (DP-SGD).

We will denote the space of all possible instances by $\dspace$ and the space of all possible datasets by $\dsetspace$.  We will denote by $[N]=\{1\ldots N\}$ the set of $N$ smallest positive integers.

\subsection{Differential Privacy}

An algorithm is differentially private if even an adversary who knows all but one instances of a dataset can't distinguish from the output of the algorithm the last instance in the dataset.  More formally:

\begin{definition}[adjacent datasets]
  We say two datasets $\dseta,\dsetb \in \dsetspace$ are adjacent, denoted $\dseta\sim \dsetb$, if they differ in at most one element.  We denote by $\adjdsets$ the space of all pairs of adjacent datasets.
\end{definition}

\begin{definition}[differential privacy \cite{dwork_algorithmic_2013}]
  Let $\epsilon>0$ and $\delta>0$.  Let $\mathcal{A}:\dsetspace\to\mathcal{O}$ be a randomized algorithm taking as input datasets from $\dsetspace$.   The algorithm $\mathcal{A}$ is $(\epsilon,\delta)$-differentially private ($(\epsilon,\delta)$-DP) if for every pair of adjacent datasets $(\dseta,\dsetb)\in\adjdsets$, and for every subset $S\subseteq \mathcal{O}$ of possible outputs of $\mathcal{A}$, $P(\mathcal{A}(\dseta)\subseteq S) \le e^\epsilon P(\mathcal{A}(\dsetb)\subseteq S)+\delta$.  If $\delta=0$ we also say that $\mathcal{A}$ is $\epsilon$-DP.
\end{definition}
If the output of an algorithm $\mathcal{A}$ is a real number or a vector, it can be privately released thanks to differential privacy mechanisms such as the Laplace mechanism or the Gaussian mechanism \cite{dwork_calibrating_nodate}.  While our key idea is more generally applicable, in this paper we will focus on the Gaussian mechanism as it leads to simplier derivations.  In particular, the Gaussian mechanism adds Gaussian noise to a number or vector which depends on its sensitivity on the input.
\begin{definition}[sensitivity]
  The $2$-sensitivity of a function $f:\dsetspace\to\mathbb{R}^p$ is
  \[\Delta_2(f) =\max_{\dseta,\dsetb\in\adjdsets} \|f(\dseta) - f(\dsetb)\|_2 \]
\end{definition}
% antoine: isn't it the norm of the difference of the images instead?
% jan: yes, adapted
\begin{lemma}[Gaussian mechanism]
  \label{lem:gaussmech}
  Let $f:\dsetspace\to\mathbb{R}^p$ be a function.  The Gaussian mechanism transforms $f$ into ${\hat{f}}$ with ${\hat{f}}(\dset) = f(\dset) + b$ where $b\sim \mathcal{N}(0,\sigma^2 I_p)\in\mathbb{R}^p$ is Gaussian distributed noise.  If the variance satisfies $\sigma^2 \ge 2\ln(1.25/\delta)(\Delta_2(f))^2/\epsilon^2$, then ${\hat{f}}$ is $(\epsilon,\delta)$-DP.
\end{lemma}

%the output of $\mathcal{A}$ before releasing it. The scale of the variance of the noise is calibrated to the sensitivity of $\mathcal{A}$ [WIP]

\subsection{Empirical risk minimization}
Unless made explicit otherwise we will consider databases $\dset=\{\exz_i\}_{i=1}^n$ containing $n$ instances $\exz_i=(\exx_i,\exy_i)\in\xspace\times \yspace$ with $\xspace=\mathbb{R}^p$ and $\yspace=\{0,1\}$ sampled identically and independently (i.i.d.) from $\dsetspace$. We are trying to build a model $h_\theta: \xspace \rightarrow \yspace$ parameterized by $\theta \in \Theta \subseteq \mathbb{R}^p$, so it minimizes the expected loss $\mathcal{L}(\theta)=\mathbb{E}_{z}[\mathcal{L}(\theta ; \exz)]$, where $\mathcal{L}(\theta; \exz)=\ell(h_\theta(\exx),\exy)$ is the loss of the model $h_\theta$ on data point $\exz$.
One can approximate $\mathcal{L}(\theta)$ by $\hat{R}(\theta ; \dset)=\frac{1}{n} \sum_{i=1}^n \mathcal{L}\left(\theta ; \exz_i\right)$, the empirical risk of model $h_\theta$.  Empirical Risk Minimization (ERM) then minimizes an objective function $F(\theta,\dset)$ which adds to this empirical risk a regularization term $\psi(\theta)$ to find an estimate ${\hat{\theta}}$ of the model parameters:
%and $\psi(\theta)$, penalty on model parameters (e.g., $\L_1$ norm).

%\begin{definition}[Empirical Risk Minimization (ERM)]
\begin{displaymath}
\hat{\theta} \in \underset{\theta \in \Theta}{\arg \min }\  F(\theta ; \dset):=\hat{R}(\theta ; \dset)+\gamma \psi(\theta)
\end{displaymath}
where $\gamma \geq 0$ is a trade-off hyperparameter.
%\end{definition}

\paragraph*{Multi-Layer perceptrons}
A popular and easy to analyze deep learning model is the multi-layer perceptron (MLP).  We will use it in this paper to illustrate our idea.
\begin{definition}\label{def:mlp}
  A $K$-layer Perceptron $f_{M L P}: \mathbb{R}^n \rightarrow \mathbb{R}^m$ is the function
  \begin{align*}
    f_{M L P}(x)= \rho_K \circ P_K \circ \rho_{K-1} \circ \cdots \circ \rho_1 \circ P_1(x),
  \end{align*}
  where $P_k: x \mapsto M_k x+b_k$ is an affine function and $\rho_k: x \mapsto\left(h_k\left(x_i\right)\right)_{i \in [ 1, n_k ]}$ is a non-linear (component-wise) activation function.  $f_{MLP}$ is parameterized by a tuple of matrices $\theta = (M_k)_{k=1}^K$.
\end{definition}
We can then consider the loss function
\begin{align*}
  {\hat R}(\theta; Z) = \sum_{i=1}^n \mathcal{L}(\theta; z_i) = \sum_{i=1}^n \ell(f_{M L P}(\theta, x_i), y_i)
\end{align*}
with $\ell$ an error cost function e.g. mean squared error (MSE) or cross-entropy (CE).

\subsection{Stochastic gradient descent}

To minimize $F(\theta,\dset)$
 one can use stochastic gradient descent (SGD).  The SGD algorithm iteratively for a number of time steps $t=1\ldots T$ computes a gradient $\gradt = \nabla F(\thetat,\dset)$ on the current model $\thetat$ and updates the model using $\gradt$, setting $\thetatsucc=\thetat - \eta(t) \gradt$ where $\eta(t)$ is a learning rate.

We consider federated learning settings where data is distributed over multiple data owners who want to collaborate to build a model but who don't want to reveal their own data.   They could encrypt the whole machine learning process, performing all operations using secure multi-party computation \cite{DavidSMPC2018} but that would be computationally expensive.  A popular strategy is therefore to aggregate securely in each round the gradient each data owner computes on its local data together with an appropriate amount of noise, and then reveal the noisy aggregated gradient to an Aggregator agent who then updates the global model.  Secure aggregation can be performed in an efficient and robust way \cite{Dwork2006ourselves,Bonawitz2017a,SabaterMLJ2022}.  The downside is that in each iteration some information leaks, and it is the total of all these leaks that determines the privacy cost.

To be more concrete, in each iteration of the SGD algorithm, the data owners and the Aggregator collaboratively compute a noisy gradient ${\hat{G}}_t = \frac{1}{\batchsize} \left(\sum_{i=1}^{\batchsize} \nabla \mathcal{L}(\thetat,\batchi) + \noiset\right)+\gamma\psi(\theta)$ over a sample $\batchset=\{\batchi\}_{i=1}^{\batchsize}$  of the data set where $\noiset$ is appropriate noise.


Determining good values for the scale of the noise in the $b_t$ variables has been the topic of several studies.  One simple strategy starts by assuming an upper bound for the norm of the gradient.  Let us first define Lipschitz functions:

\begin{definition}[Lipschitz function]
  Let $\sLipschitz>0 .$ A function $f$ is $\sLipschitz$-Lipschitz with respect to some norm $\|\cdot\|$ if for all $\theta, \theta^{\prime} \in \Theta$ there holds
  $\left\|f(\theta)-f\left(\theta^{\prime}\right)\right\| \leq \sLipschitz\left\|\theta-\theta^{\prime}\right\|.$
  If $f$ is differentiable and $\|\cdot\|=\|\cdot\|_2$, the above property is equivalent to:
  $$
  \|\nabla f(\theta)\|_2 \leq \sLipschitz, \quad \forall \theta \in \Theta
  $$
\end{definition}

Then, from the model one can derive a constant $\sLipschitz$ such that the objective function is $\sLipschitz$-Lipschitz, while knowing bounds on the data next allows for computing a bound on the sensitivity of the gradient.  Once one knows the sensitivity, one can determine the noise to be added from the privacy parameters as in Lemma \ref{lem:gaussmech}.

Algorithm \ref{alg:dpsgd} shows an algorithm which is generic for the purpose of our explanation.
In \cite{abadi_deep_2016}, an algorithm is proposed which for a given input $(\dset,n,\ell,\Theta,\epsilon, \delta,T,\batchsize,\eta,C)$ behaves as a call to $\textsc{DP-SGD}(\dset,n,\operatorname{None}, \operatorname{None},\ell,\Theta,\epsilon, \delta,T,\batchsize,\eta,"gradient", C)$ in Algorithm \ref{alg:dpsgd}. In $\textsc{SearchDPNoise}$, we utilize the moment accountant method to determine the optimal noise multiplier $\sigma$ needed to achieve a desired level of $(\epsilon, \delta)$-differential privacy, taking into account the number of instances in the dataset $\dset$, the batch size $s$, and the number of epochs $T$.

\input{subfiles/algos/dp_sgd}
