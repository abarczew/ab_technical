import numpy as np
import scipy
import torch
from torch.utils.data import Dataset, RandomSampler, BatchSampler
from torch.nn.utils import clip_grad_norm_
import pandas as pd
from utils import get_noise_multiplier, clip_param_norm, viz_hist_epoch
from lipEstimation.lipschitz_approximations import lipschitz_second_order_ub

seed = torch.Generator().manual_seed(42)


class MyDataset(Dataset):
    def __init__(self, X, y, test_size,
                 task='classification', standardize=False, input_norm=None):
        if standardize:
            X = (X - X.min(axis=0)) / (X.max(axis=0) - X.min(axis=0))
            if task == 'regression':
                y = (y - y.min(axis=0)) / (y.max(axis=0) - y.min(axis=0))
        if input_norm is not None:
            X = X / np.max(np.linalg.norm(X, axis=1)) * input_norm
        if scipy.sparse.issparse(X):
            self.x_data = torch.sparse_csr_tensor(torch.tensor(X.indptr, dtype=torch.int64),
                                                  torch.tensor(X.indices, dtype=torch.int64),
                                                  torch.tensor(X.data), dtype=torch.float32)
        else:
            X = np.array(X, dtype=np.float32)
            self.x_data = torch.tensor(X, dtype=torch.float32)
        # y = np.array(y, dtype=np.float32)

        if task == 'classification':
            n_class = len(np.unique(y))
            if n_class == 2:
                self.class_num = 1
            else:
                self.class_num = n_class
            if len(y.shape) == 1 and self.class_num == 1:
                y = y[:, np.newaxis]
            else:
                y = np.eye(self.class_num)[y]
        else:
            y = y[:, np.newaxis]
        self.y_data = torch.tensor(y, dtype=torch.float32)
        self.input_dim = X.shape[1]

    def __len__(self):
        return self.x_data.shape[0]

    def __getitem__(self, idx):
        return self.x_data[idx], self.y_data[idx]


class DPModel(torch.nn.Module):
    """Parent class for DP model."""

    def __init__(self):
        super(DPModel, self).__init__()
        self.cumulative_penalty = 0
        self.penalties = []
        self.max_x = 1
        self.max_y = 1
        self.n_layers = 1
        for params in self.parameters():
            self.penalties.append(torch.zeros_like(params))

    def forward(self, x):
        pass

    def step_nodp(self, learning_rate):
        # This is what optimizer.step() does
        for params in [p for p in self.parameters() if p.grad is not None]:
            new_params = torch.add(params, - learning_rate * params.grad)
            with torch.no_grad():
                params.copy_(new_params)

    def step(self, learning_rate, trainer):
        if trainer.lip_method == 'no_dp':
            self.step_nodp(learning_rate)
        else:
            self.step_dp(learning_rate, trainer)

    def _get_lip_bound(self, lip_const):
        return lip_const * self.max_x * self.max_y * np.sqrt(self.n_layers)

    def _get_lip_global(self, clipping_param_norm):
        lip_const = (clipping_param_norm * self.activation_max)**self.n_layers
        return self._get_lip_bound(np.array([lip_const]*self.n_layers))

    def _get_lip_local(self, method='autolip'):
        with torch.no_grad():
            if method == 'autolip':
                params_norm = [torch.linalg.norm(p, 2)
                               for p in self.parameters()]
                lip_const = np.array([np.prod(params_norm)
                                      * self.activation_max**self.n_layers]
                                     * self.n_layers)
            elif method == 'seqlip':
                lip_const = np.array([lipschitz_second_order_ub(self)
                                      * self.activation_max**self.n_layers]
                                     * self.n_layers)
            else:
                raise NameError('Method not allowed')
        return self._get_lip_bound(lip_const)

    def step_dp(self, learning_rate, trainer):
        sigmas = self._get_sigma(trainer)
        parameters = [p for p in self.parameters() if p.grad is not None]
        for params, sigma in zip(parameters, sigmas):
            with torch.no_grad():
                added_noise = torch.normal(mean=0., std=sigma,
                                           size=params.size(),
                                           generator=trainer.seed)
                added_noise = added_noise / trainer.batch_size
            params.grad = torch.add(params.grad, added_noise)
            new_params = torch.add(params, - learning_rate * params.grad)
            with torch.no_grad():
                params.copy_(new_params)

    def _get_sigma(self, trainer):
        if trainer.lip_method == 'global':
            if trainer.clipping_grad_norm is not None:
                lip_const = np.array([trainer.clipping_grad_norm]
                                     * self.n_layers)
            else:
                lip_const = self._get_lip_global(trainer.clipping_param_norm)
        elif trainer.lip_method == 'local':
            lip_const = self._get_lip_local(trainer.lip_local_method)
        else:
            raise NameError('Lip method not allowed')
        if trainer.accountant == 'comp':
            sigma = self._get_comp(lip_const, trainer.epsilon,
                                   trainer.delta, trainer.epochs,
                                   trainer.n_samples)
        elif trainer.accountant == 'ma':
            sigma = trainer.noise_multiplier * lip_const
        else:
            raise NameError('Accountant method not allowed')
        return sigma

    def _get_comp(self, lip, epsilon, delta, epochs, n_samples):
        # n_samples are picked at each epoch
        return 16*lip*np.sqrt(epochs*n_samples*np.log(2/delta)
                                    * np.log(2.5*epochs/delta))\
            / (n_samples*epsilon)

    def get_input_sizes(self, batch_size):
        self.input_sizes = [(batch_size, self.input_dim)]
        return self

    def add_penalty(self, learning_rate, lasso_const):
        """Apply the L1 penalty to each updated feature
        This implements the truncated gradient approach by
        [Tsuruoka, Y., Tsujii, J., and Ananiadou, S., 2009].
        """
        with torch.no_grad():
            self.cumulative_penalty += learning_rate * lasso_const
            for params, penalty in zip(self.parameters(), self.penalties):
                # is it params nonzero or grad nonzero?
                nonzero_indx = (params != 0).nonzero(as_tuple=True)
                nonzero_params = params[nonzero_indx]
                # nonzero_penalty = penalty[nonzero_indx]
                nonzero_penalty = penalty[nonzero_indx]
                pos_indx = (params > 0).nonzero(as_tuple=True)
                penalized = torch.add(params[pos_indx],
                                      - self.cumulative_penalty
                                      - penalty[pos_indx])
                clipped = torch.clamp(penalized, max=0)
                params[pos_indx] = clipped
                neg_indx = (params < 0).nonzero(as_tuple=True)
                penalized = torch.add(params[neg_indx],
                                      + self.cumulative_penalty
                                      - penalty[neg_indx])
                clipped = torch.clamp(penalized, max=0)
                params[neg_indx] = clipped
                penalty[nonzero_indx] = torch.add(nonzero_penalty,
                                                  params[nonzero_indx]
                                                  - nonzero_params)


class LogisticRegression(DPModel):
    """DP LogisticRegression"""

    def __init__(self, input_dim, output_dim):
        super().__init__()
        self.input_dim = input_dim
        self.linear = torch.nn.Linear(input_dim, output_dim)
        self.cumulative_penalty = 0
        self.penalties = []
        with torch.no_grad():
            for params in self.parameters():
                self.penalties.append(torch.zeros_like(params))

    def forward(self, x):
        outputs = torch.sigmoid(self.linear(x))
        return outputs

    def _get_lip_global(self, params):
        return np.sqrt(params.size()[-1]) * torch.ones(params.size()[-1])

    def _get_lip_local(self, params):
        with torch.no_grad():
            if len(params.size()) > 1:
                theta_norm1 = torch.linalg.norm(params, 1, dim=1)
            else:
                theta_norm1 = torch.abs(params)
            # clip exp parameter to avoid overflow
            theta_norm1 = torch.clip(theta_norm1, -45., 45.)
            lip_const = 1 / (1 + np.exp(-theta_norm1))
        return lip_const


class ClassifNet(DPModel):
    """DP NeuralNet for classification."""

    def __init__(self, input_dim, output_dim, hidden_dim,
                 activation, bias=False, input_norm=None):
        super().__init__()
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.input_norm = input_norm
        self.output_dim = output_dim
        self.linear_1 = torch.nn.Linear(self.input_dim, self.hidden_dim,
                                        bias=bias)
        self.linear_2 = torch.nn.Linear(self.hidden_dim, self.output_dim,
                                        bias=bias)
        self.fn_activation = activation['fn']
        self.fn_output = torch.nn.Softmax(dim=0)
        self.max_x = np.min([i for i in [np.sqrt(input_dim), self.input_norm]
                             if i is not None])
        self.max_y = 1  # data needs to be normalized
        self.n_layers = 2
        self.activation_max = activation['activation_max']

    def forward(self, x):
        hidden = self.fn_activation(self.linear_1(x))
        outputs = self.fn_output(self.linear_2(hidden))
        return outputs


class RegNet(DPModel):
    """DP NeuralNet for regression."""

    def __init__(self, input_dim, hidden_dim=50,
                 activation={'fn': torch.nn.ReLU(inplace=False),
                             'activation_max': 1},
                 bias=False, input_norm=None):
        super().__init__()
        self.input_dim = input_dim
        self.output_dim = 1
        self.hidden_dim = hidden_dim
        self.input_norm = input_norm
        self.linear_1 = torch.nn.Linear(self.input_dim, self.hidden_dim,
                                        bias=bias)
        self.linear_2 = torch.nn.Linear(self.hidden_dim, self.output_dim,
                                        bias=bias)
        self.activation = activation['fn']
        self.max_x = np.min([i for i in [np.sqrt(input_dim), self.input_norm]
                             if i is not None])
        self.max_y = 1  # data needs to be standardize
        self.n_layers = 2
        self.activation_max = activation['activation_max']

    def forward(self, x):
        hidden = self.activation(self.linear_1(x))
        outputs = self.linear_2(hidden)
        return outputs


class Trainer():
    def __init__(self, model, criterion, metric, learning_rate,
                 lip_method, lip_local_method, accountant,
                 epsilon, delta, n_samples, lasso_const, seed,
                 callback, batch_size, epochs,
                 clipping_grad_norm=None, clipping_param_norm=None):
        self.model = model
        self.model = self.model.get_input_sizes(batch_size)
        self.criterion = criterion
        self.metric = metric
        self.learning_rate = learning_rate
        self.lip_method = lip_method
        self.lip_local_method = lip_local_method
        self.accountant = accountant
        self.epsilon = epsilon
        self.delta = delta
        self.n_samples = n_samples
        self.lasso_const = lasso_const / n_samples
        self.seed = seed
        self.callback = callback
        self.batch_size = batch_size
        self.epochs = epochs
        self.torch_seed = torch.Generator().manual_seed(42)
        if clipping_param_norm is not None:
            self.clipping_grad_norm = None
        else:
            self.clipping_grad_norm = clipping_grad_norm
        self.clipping_param_norm = clipping_param_norm
        if self.accountant == 'ma':
            self.noise_multiplier = get_noise_multiplier(self.n_samples,
                                                         self.epsilon,
                                                         self.delta,
                                                         self.epochs,
                                                         self.batch_size)

    def fit(self, train_subset, test_set):
        for epoch in range(self.epochs):
            train_sampler = RandomSampler(train_subset,
                                          replacement=True,
                                          num_samples=self.n_samples,
                                          generator=self.torch_seed)
            train_loader = BatchSampler(train_sampler,
                                        self.batch_size,
                                        drop_last=False)
            # train
            _ = self._train(train_subset, train_loader)
            # validate
            loss_test, metric_test = self._test(test_set)
            ndim_active = self._count_ndim_active()
            if self.lip_method == 'no_dp':
                sigma_mean = 0
            else:
                sigma_mean = []
                for params in self.model.parameters():
                    with torch.no_grad():
                        sigma = self.model._get_sigma(self)
                        relative_noise = sigma.mean().item() \
                            / torch.linalg.norm(params, 2)
                        sigma_mean.append(relative_noise)
                sigma_mean = np.mean(sigma_mean)
            self.callback.on_test_step_end(loss_test, metric_test,
                                           ndim_active, epoch,
                                           sigma_mean)
        return self.callback.history

    def _train(self, dataset, loader):
        # put model in train mode
        self.model.train()

        for batch in loader:
            # TODO: see if first pass is ok maybe none
            parameters = [p for p in self.model.parameters()
                          if p.grad is not None]
            grad = [torch.zeros_like(params) for params in parameters]
            for sample in batch:
                x, y = dataset[sample]
                self.model.zero_grad()
                y_hat = self.model(x)
                loss = self.criterion(y_hat, y)
                loss.backward()
                if self.clipping_grad_norm is not None:
                    for params in parameters:
                        clip_grad_norm_(params,
                                        max_norm=self.clipping_grad_norm)
                for j, params in enumerate(parameters):
                    grad[j].add_(params.grad)
            for j, params in enumerate(parameters):
                params.grad = grad[j] / self.batch_size
            if self.clipping_param_norm is not None:
                with torch.no_grad():
                    for params in parameters:
                        params_clip = clip_param_norm(params,
                                                      self.clipping_param_norm)
                        params.copy_(params_clip)
            self.model.step(self.learning_rate, self)
            if self.lasso_const != 0:
                self.model.add_penalty(self.learning_rate, self.lasso_const)

        return loss.item()

    def _test(self, test_set):
        # put model in evaluation mode
        self.model.eval()
        x_test, y_test = test_set.x_data, test_set.y_data
        with torch.no_grad():
            outputs_test = self.model(x_test)
            loss_test = self.criterion(outputs_test, y_test).item()
            metric_test = self.metric(y_test.detach().numpy(),
                                      outputs_test.detach().numpy())
            # acc_test = accuracy_score(y_test.detach().numpy().argmax(1),
            #                           outputs_test.detach().numpy().argmax(1))
        return loss_test, metric_test

    def _count_ndim_active(self):
        ndim_active = 0
        with torch.no_grad():
            for params in self.model.parameters():
                nonzero_params = (params != torch.zeros_like(params)).int()
                ndim_active += torch.sum(nonzero_params)
        return ndim_active.item()


class Callback(object):
    """docstring for Callback."""

    def __init__(self, args):
        super(Callback, self).__init__()
        self.history = History(args)

    def on_test_step_end(self, loss_test, metric_test,
                         ndim_active, epoch, sigma_mean):
        self.history.update({'loss': loss_test,
                             'metric': metric_test,
                             'ndim_active': ndim_active,
                             'epoch': epoch,
                             'sigma': sigma_mean})

    def on_train_begin(self): pass
    def on_train_end(self): pass
    def on_epoch_begin(self): pass
    def on_epoch_end(self): pass
    def on_batch_begin(self): pass
    def on_batch_end(self): pass
    def on_loss_begin(self): pass
    def on_loss_end(self): pass

    def on_step_begin(self, sigma):
        sigma_mean = torch.mean(sigma).item()
        self.history.update({'sigma': sigma_mean})

    def on_step_end(self): pass


class History(object):
    """docstring for History."""

    def __init__(self, args):
        super(History, self).__init__()
        self.records = []
        self.args = args
        self.items_file = args['items_file']

    def update(self, item):
        self.records.append(item)

    def output(self):
        return {'args': self.args, 'records': self.records}

    def output_epoch_viz(self, filepath):
        outpath = f'{filepath}/epoch'
        items_filepath = [item for item in self.args.items()
                          if item[0] in self.items_file]
        for k, v in items_filepath:
            outpath += f'_{k}={v}'
        df_records = pd.DataFrame.from_records(self.records)
        viz_hist_epoch(df_records, outpath)

    def build_results(self):
        df_records = pd.DataFrame.from_records(self.records)
        df_args = pd.DataFrame.from_records([self.args])
        df_perf = pd.concat([df_records, df_args], axis=1)
        df_perf = df_perf.ffill(axis=0)
        return df_perf

    def to_csv(self, filepath):
        df = pd.DataFrame.from_records([self.args] + self.records)
        df.to_csv(filepath, index=False)
