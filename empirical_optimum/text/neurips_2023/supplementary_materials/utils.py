import random
import pandas as pd
import numpy as np
from typing import Optional
import math
import dp_accounting
from sklearn.datasets import load_breast_cancer, fetch_california_housing, \
    load_diabetes
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.datasets import fetch_openml
from itertools import product
import torch
from conf import DATADIR, OUTDIR
import matplotlib.pyplot as plt
plt.style.use('seaborn-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)

markers = ['o', '^', 's', 'P', 'v', '>', '<', '*', '+', 'x', 'D', 2]


def get_data(dataset_name, sample_size=-1):
    if dataset_name == 'framingham':
        df_fram = pd.read_csv(f'{DATADIR}/{dataset_name}.csv').fillna(0)
        X = df_fram[[col for col in df_fram.columns if col != 'TenYearCHD']]
        y = df_fram.TenYearCHD
    elif dataset_name == 'breast_cancer':
        data = load_breast_cancer()
        y = data.target
        X = data.data
    elif dataset_name == 'bank-full':
        df_ban = pd.read_csv(f'{DATADIR}/{dataset_name}.csv', sep=';')
        enc = OneHotEncoder(drop='first')
        X = enc.fit_transform(df_ban[[col for col
                                      in df_ban.columns if col != 'y']])
        y = df_ban.y.map(lambda x: 1 if x == 'yes' else 0)
    elif dataset_name == 'diabetes':
        df_dia = pd.read_csv(f'{DATADIR}/{dataset_name}.csv')
        target_name = 'Outcome'
        X = df_dia[[col for col in df_dia.columns if col != target_name]]
        y = df_dia[target_name]
    elif dataset_name == 'permission':
        df_dia = pd.read_csv(f'{DATADIR}/{dataset_name}.csv')
        target_name = 'Result'
        X = df_dia[[col for col in df_dia.columns if col != target_name]]
        y = df_dia[target_name]
    elif dataset_name == 'mnist':
        mnist = fetch_openml('mnist_784', version=1, cache=True)
        X, y = mnist.data / 250, mnist.target.astype(np.int8)
    elif dataset_name == 'titanic':
        df = pd.read_csv(f'{DATADIR}/{dataset_name}.csv')
        target_name = 'Survived'
        train_data = df.copy()
        train_data["Age"].fillna(df["Age"].median(skipna=True), inplace=True)
        train_data["Embarked"].fillna(df['Embarked'].value_counts().idxmax(), inplace=True)
        train_data.drop('Cabin', axis=1, inplace=True)
        train_data['TravelAlone'] = np.where((train_data["SibSp"] + train_data["Parch"]) > 0, 0, 1)
        train_data.drop('SibSp', axis=1, inplace=True)
        train_data.drop('Parch', axis=1, inplace=True)
        training = pd.get_dummies(train_data, columns=["Pclass", "Embarked", "Sex"])
        training.drop('Sex_female', axis=1, inplace=True)
        training.drop('PassengerId', axis=1, inplace=True)
        training.drop('Name', axis=1, inplace=True)
        training.drop('Ticket', axis=1, inplace=True)
        X = training[[col for col in training.columns if col != target_name]]
        y = training[target_name]
    elif dataset_name == 'adult_income':
        df = pd.read_csv(f"{DATADIR}/{dataset_name}.csv")
        df = df.applymap(lambda x: np.nan if x == '?' else x)
        target_name = 'income'
        input_features = [col for col in df.columns if col not in ['kfold', target_name]]
        numerical_columns = ['age', 'fnlwgt', 'capital.gain', 'capital.loss', 'hours.per.week']
        categorical_columns = [col for col in input_features if not col in numerical_columns]

        def impute_null_values(data, categorical_columns, numerical_columns):
            for col in categorical_columns:
                data.loc[:, col] = data[col].fillna("NONE")

            for cols in numerical_columns:
                data.loc[:, cols] = data[cols].fillna(data[cols].median())
            return data
        df = impute_null_values(df, categorical_columns, numerical_columns)
        enc = OneHotEncoder(sparse=False, drop='first')
        df_cat = pd.DataFrame(enc.fit_transform(df[categorical_columns]))
        X = pd.concat([df[numerical_columns], df_cat], axis=1)
        y = df[target_name].map({'<=50K': 0, '>50K': 1})
    elif dataset_name == 'housing':
        data = fetch_california_housing()
        y = data.target
        X = data.data
    elif dataset_name == 'diabetes_reg':
        data = load_diabetes()
        y = data.target
        X = data.data
    elif dataset_name == 'patient_survival':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.csv')
        target_name = 'hospital_death'
        numerical_cat = [
            'elective_surgery',
            'apache_post_operative',
            'arf_apache',
            'gcs_unable_apache',
            'intubated_apache',
            'ventilated_apache',
            'aids',
            'cirrhosis',
            'diabetes_mellitus',
            'hepatic_failure',
            'immunosuppression',
            'leukemia',
            'lymphoma',
            'solid_tumor_with_metastasis']
        categorical = ['ethnicity',
                       'gender',
                       'icu_type',
                       'apache_3j_bodysystem',
                       'apache_2_bodysystem']
        col_drop = ['encounter_id', 'icu_admit_source', 'Unnamed: 83',
                    'icu_id', 'icu_stay_type', 'patient_id', 'hospital_id']
        bmi_col = ['bmi', 'weight', 'height']
        num_col = [col for col in data.columns
                   if col not in numerical_cat+categorical
                   + col_drop+[target_name]]
        data = data.drop(col_drop, axis=1)
        data = data.loc[data[bmi_col].dropna(axis=0).index]
        data = data.loc[data[categorical].dropna(axis=0).index]
        data[numerical_cat] = data[numerical_cat].fillna(0)
        data[num_col] = data[num_col].fillna(data[num_col].mean())
        X = data[[col for col in data.columns if col != target_name]]
        X = pd.get_dummies(X, columns=categorical, drop_first=True)
        y = data[target_name]
    elif dataset_name == 'german':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.data-numeric',
                           header=None, delimiter='\s+')
        y = data.values[:, -1]
        y = np.array(list(map(lambda x: 0 if x == 2 else x, y)))
        X = data.values[:, :-1]
    elif dataset_name == 'dropout':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.csv', delimiter=';')
        target_name = 'Target'
        y = data[target_name]
        y = np.array(list(map(lambda x: 0 if x == 'Graduate' else 1, y)))
        X = data[[col for col in data.columns if col != target_name]]
        categorical = ['Marital status', 'Application mode',
                       'Application order', 'Course',
                       'Previous qualification', 'Nacionality',
                       "Mother's qualification", "Father's qualification",
                       "Mother's occupation", "Father's occupation"]
        X = pd.get_dummies(X, columns=categorical, drop_first=True)
    elif dataset_name == 'nursery':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.data',
                           header=None, delimiter=',')
        y = data.values[:, -1]
        mask = {k: v for v, k in enumerate(np.unique(y))}
        y = np.array(list(map(lambda x: mask[x], y)))
        X = data.values[:, :-1]
        X = np.array(pd.get_dummies(pd.DataFrame(X), drop_first=True))
    elif dataset_name == 'shuttle':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.tst',
                           header=None, delimiter='\s+')
        y = data.values[:, -1] - 1
        X = data.values[:, :-1]
    elif dataset_name == 'default_credit':
        data = pd.read_excel(f'{DATADIR}/{dataset_name}.xls', skiprows=1)
        categorical = ['EDUCATION', 'MARRIAGE']
        target = 'default payment next month'
        dropcol = 'ID'
        y = data[target]
        X = data.drop([dropcol, target], axis=1)
        X = pd.get_dummies(X, columns=categorical, drop_first=True)
    elif dataset_name == 'thyroid':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.csv')
        data = data.values[:, 1:]
        y = (data[:, -1] - 1).astype('int')
        X = data[:, :-1]
    elif dataset_name == 'yeast':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.data',
                           header=None, delimiter='\s+')
        data = data.values[:, 1:]
        y = data[:, -1]
        mask = {k: v for v, k in enumerate(np.unique(y))}
        y = np.array(list(map(lambda x: mask[x], y)))
        X = data[:, :-1]
    elif dataset_name == 'kddcup':
        data = pd.read_csv(f'{DATADIR}/{dataset_name}.data_10_percent_corrected',
                           header=None, delimiter=',')
        y = data[41].values
        mask = {k: v for v, k in enumerate(np.unique(y))}
        y = np.array(list(map(lambda x: mask[x], y)))
        X = data.drop(41, axis=1)
        X = pd.get_dummies(X, drop_first=True).values
    else:
        raise 'Dataset not in datasets list'
    if sample_size == -1:
        return X, y
    else:
        rnd_index = np.random.choice(len(y), sample_size, replace=False)
        return X.loc[rnd_index], y.loc[rnd_index]


def split_train_test(X, y, test_ratio=0.2, seed=42):
    n_samples = len(X)
    random.seed(seed)
    index_test = random.sample(range(n_samples), int(test_ratio * n_samples))
    index_train = [i for i in range(n_samples) if i not in index_test]
    X_test = X[index_test, :]
    y_test = y[index_test]
    X_train = X[index_train, :]
    y_train = y[index_train]
    return X_train, y_train, X_test, y_test


def get_args_list(args):
    args_product = product(*args.values())
    args_list = [dict(zip(args.keys(), prod)) for prod in args_product]
    args_list = filter(lambda arg: (arg['clipping_grad_norm'] is not None
                       or arg['clipping_param_norm'] is not None),
                       args_list)
    args_list = filter(lambda arg: (arg['lip_method'] != 'no_dp'
                       or arg['epsilon'] is None),
                       args_list)
    args_list = filter(lambda arg: (arg['lip_method'] != 'local'
                                    or arg['clipping_param_norm'] is not None),
                       args_list)
    args_list = filter(lambda arg: (arg['lip_method'] != 'local'
                                    or arg['lip_local_method'] is not None),
                       args_list)
    args_list = list(args_list)
    args_all = args_list[0]
    args_list = args_list * args_all['n_tries']
    return args_list, args_all


def viz_perf_vs_epochs(df_plot, outpath, hyper_params,
                       viz_metric, epsilon=1.):
    """
    returns metric per epochs and per methods
    """
    df_plot = df_plot.loc[df_plot.epsilon == epsilon]
    best_hyper_params = df_plot.loc[df_plot.metric == df_plot.metric.max()][hyper_params].values[0]
    for hyper_param, best_value in zip(hyper_params, best_hyper_params):
        df_plot = df_plot.loc[df_plot[hyper_param] == best_value]
    df_plot = df_plot[['epoch', 'method', viz_metric]]\
        .groupby(['method', 'epoch'])\
        .agg(['median', 'std'])\
        .reset_index()
    methods = df_plot.method.unique()[::-1]
    for i, method in enumerate(methods):
        df_subplot = df_plot.loc[df_plot.method == method]
        plt.errorbar(df_subplot.epoch,
                     df_subplot[viz_metric]['median'],
                     yerr=df_subplot[viz_metric]['std'],
                     label=f'{method}',
                     marker=markers[i])
    plt.grid()
    plt.xlabel('epoch')
    if viz_metric == 'metric':
        plt.ylabel('AUC')
    else:
        plt.ylabel(viz_metric)
    plt.legend()
    plt.savefig(f'{outpath}_{list(zip(hyper_params, best_hyper_params))}_eps={epsilon}.png',
                bbox_inches='tight')
    plt.close()


def q90(x, viz_metric):
    if viz_metric == 'loss':
        quantile = 1-0.9
    else:
        quantile = 0.9
        q90 = np.quantile(x.dropna(), quantile, interpolation='nearest')
        return q90


def viz_perf_vs_epsilon(df, outpath, viz_metric):
    """
    returns max metric per epsilon and per methods
    """
    df_run = df.groupby(
        ['epsilon', 'method'] + ['run']).metric.max().unstack(level=1)
    agg_metric = 'mean'
    df_run_tab = df_run.reset_index()
    df_plot = df_run_tab\
        .groupby('epsilon')\
        .agg(['std', agg_metric])\
        .reset_index()
    methods = df.method.unique()[::-1]
    for i, method in enumerate(methods):
        plt.errorbar(df_plot.epsilon,
                     df_plot[method][agg_metric],
                     yerr=df_plot[method]['std'],
                     label=f'{method}',
                     marker=markers[i])
    plt.grid()
    plt.xlabel('Epsilon')
    plt.xscale('log')
    if viz_metric == 'metric':
        plt.ylabel('AUC')
        plt.ylim(0.49, 1.01)
    else:
        plt.ylabel(viz_metric)
    plt.legend()
    plt.savefig(f'{outpath}.png', bbox_inches='tight')
    plt.close()


def viz_sigma_vs_epsilon(df, outpath):
    """
    returns relative noise to gradient norm per epsilon and per methods
    """
    df_run = df.groupby(
        ['epsilon', 'method'] + ['run']).metric.max()
    df_plot = df.loc[df.metric.isin(df_run)]
    df_plot = df_plot[['epsilon', 'method', 'sigma']]\
        .groupby(['method', 'epsilon'])\
        .agg(['std', 'mean'])\
        .reset_index()
    methods = df.method.unique()[::-1]
    for i, method in enumerate(methods):
        mask_method = df_plot.method == method
        df_subplot = df_plot.loc[mask_method]
        plt.errorbar(df_subplot.epsilon,
                     df_subplot['sigma']['mean'],
                     yerr=df_subplot['sigma']['std'],
                     label=f'{method}',
                     marker=markers[i])
    plt.grid()
    plt.xlabel('Epsilon')
    plt.xscale('log')
    plt.ylabel('Sigma')
    plt.yscale('log')
    plt.legend()
    plt.savefig(f'{outpath}.png', bbox_inches='tight')
    plt.close()


def viz_perf_vs_hyperparms(df, outpath, hyper_params,
                           viz_metric, epsilon=1.):
    df = df.loc[(df.epsilon == epsilon) & (df.clipping_param_norm != 'None')]
    for hyper_param in hyper_params:
        df_run = df.groupby(
            [hyper_param, 'method'] + ['run']).metric.max().unstack(level=1)
        df_run_tab = df_run.reset_index()
        df_plot = df_run_tab\
            .groupby(hyper_param)\
            .agg(['std', 'median'])\
            .reset_index()
        methods = df.method.unique()[::-1]
        for i, method in enumerate(methods):
            plt.errorbar(df_plot[hyper_param],
                         df_plot[method]['median'],
                         yerr=df_plot[method]['std'],
                         label=f'{method}',
                         marker=markers[i])
        plt.grid()
        plt.xlabel(hyper_param)
        plt.xscale('log')
        if viz_metric == 'metric':
            plt.ylabel('AUC')
            plt.ylim(0.49, 1.01)
        else:
            plt.ylabel(viz_metric)
        plt.legend()
        plt.savefig(f'{outpath}_{hyper_param}_eps={epsilon}.png',
                    bbox_inches='tight')
        plt.close()


def viz_hist_epoch(df_records, outpath):
    plt.plot(df_records.epoch, df_records.loss)
    plt.grid()
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.savefig(f'{outpath}.png', bbox_inches='tight')
    plt.close()


def get_noise_multiplier(num_train: int,
                         epsilon: float,
                         delta: float,
                         epochs: int,
                         batch_size: int,
                         tolerance: float = 1e-2) -> Optional[float]:
    """Computes the noise multiplier for DP-SGD given privacy parameters.

      The algorithm performs binary search on the values of epsilon.

    Args:
      num_train: number of input training points.
      epsilon: epsilon parameter in (epsilon, delta)-DP.
      delta: delta parameter in (epsilon, delta)-DP.
      epochs: number of training epochs.
      batch_size: the number of examples in each batch of gradient descent.
      tolerance: an upper bound on the absolute difference between the input
        (desired) epsilon and the epsilon value corresponding to the
        noise_multiplier that is output.

    Returns:
      noise_multiplier: the smallest noise multiplier value (within plus or
       minus the given tolerance) for which using DPKerasAdamOptimizer will
       result in an (epsilon, delta)-differentially private trained model.
    """
    orders = ([1.25, 1.5, 1.75, 2., 2.25, 2.5, 3., 3.5, 4., 4.5] +
              list(range(5, 64)) + [128, 256, 512])
    steps = int(math.ceil(epochs * num_train / batch_size))

    def make_event_from_param(noise_multiplier):
        return dp_accounting.SelfComposedDpEvent(
            dp_accounting.PoissonSampledDpEvent(
                sampling_probability=batch_size / num_train,
                event=dp_accounting.GaussianDpEvent(noise_multiplier)), steps)

    return dp_accounting.calibrate_dp_mechanism(
        lambda: dp_accounting.rdp.RdpAccountant(orders),
        make_event_from_param,
        epsilon,
        delta,
        dp_accounting.LowerEndpointAndGuess(0, 1),
        tol=tolerance)


def clip_param_norm(param, max_norm=1):
    param_norm = torch.linalg.norm(param, 2)
    if param_norm > max_norm:
        return (param / param_norm) * max_norm
    else:
        return param


def get_input_norm(clipping_grad_norm, clipping_param_norm,
                   activation_max, n_layers=2):
    if clipping_grad_norm is None:
        return None
    elif clipping_param_norm is None:
        return None
    else:
        return clipping_grad_norm \
            / ((clipping_param_norm * activation_max)**n_layers
               * np.sqrt(n_layers))


def init_normal(module):
    if type(module) == torch.nn.Linear:
        torch.nn.init.normal_(module.weight, mean=0, std=1.)
