import torch

DATADIR = 'data'
OUTDIR = 'results'

args_all = {'accountant': ['ma'],
            'model': ['classifnet'],
            'activation': [{'fn': torch.sigmoid,
                            'activation_max': 1/4}],
            'n_jobs': [-1.0],
            'test_size': [0.2],
            'batch_size': [10],
            'lasso_const': [0.],
            'epsilon': [0.1, 1.0, 10.0],
            'input_norm': [None],
            'viz_metric': ['metric'],
            'lip_local_method': ['autolip'],
            'items_file': [['dataset_name', 'model', 'learning_rate', 'epsilon',
                            'lip_method', 'accountant', 'clipping_grad_norm',
                            'clipping_param_norm', 'hidden_dim']],
            'epochs': [30],
            'sample_size': [-1],
            'hidden_dim': [20, 100, 1000],
            'learning_rate': [0.00001, 0.0001, 0.001, 0.01],
            'lip_method': ['local', 'global'],
            'clipping_grad_norm': [2, 4, 8],
            'clipping_param_norm': [0.1, 1, 5, 10],
            'hyper_params': [['learning_rate', 'hidden_dim',
                              'clipping_param_norm']],
            'n_tries': [10]
            }

args_mnist = {**args_all,
              'dataset_name': ['mnist'],
              'delta': [1/10000**2],
              'standardize': [False],
              'name': ['mnist']
              }

args_breast_cancer = {**args_all,
                      'dataset_name': ['breast_cancer'],
                      'delta': [1/100**2],
                      'standardize': [True],
                      'name': ['breast_cancer']
                      }

args_adult_income = {**args_all,
                     'dataset_name': ['adult_income'],
                     'delta': [1/1000**2],
                     'standardize': [True],
                     'name': ['adult_income']
                     }

args_permission = {**args_all,
                   'dataset_name': ['permission'],
                   'delta': [1/1000**2],
                   'standardize': [True],
                   'name': ['permission']
                   }

args_patient_survival = {**args_all,
                         'dataset_name': ['patient_survival'],
                         'delta': [1/1000**2],
                         'standardize': [True],
                         'name': ['patient_survival']
                         }

args_german = {**args_all,
               'dataset_name': ['german'],
               'delta': [1/1000**2],
               'standardize': [True],
               'name': ['german']
               }

args_dropout = {**args_all,
                'dataset_name': ['dropout'],
                'delta': [1/1000**2],
                'standardize': [True],
                'name': ['dropout']
                }

args_nursery = {**args_all,
                'dataset_name': ['nursery'],
                'delta': [1/1000**2],
                'standardize': [True],
                'name': ['nursery']
                }

args_default_credit = {**args_all,
                       'dataset_name': ['default_credit'],
                       'delta': [1/1000**2],
                       'standardize': [True],
                       'name': ['default_credit']
                       }

args_thyroid = {**args_all,
                'dataset_name': ['thyroid'],
                'delta': [1/1000**2],
                'standardize': [True],
                'name': ['thyroid']
                }

args_yeast = {**args_all,
              'dataset_name': ['yeast'],
              'delta': [1/1000**2],
              'standardize': [True],
              'name': ['yeast']
              }

args_list = [args_mnist, args_breast_cancer, args_adult_income,
             args_permission, args_patient_survival, args_german,
             args_dropout, args_nursery, args_default_credit,
             args_thyroid, args_yeast]
