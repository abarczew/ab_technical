\section{Experimental results}\label{sec:experiment}
In this section, we conduct an empirical evaluation of our approach.

\subsection{Experimental setup}
\label{sec:exp.setup}

We consider the following experimental questions:
\begin{itemize}[noitemsep,topsep=0pt]
\item[Q1] How does \weightDpSgd{}, our proposed technique, compare against the conventional \gradDpSgd{} as introduced by \cite{abadi_deep_2016}?
\item[Q2] What is the effect of allowing $\|\theta_k\| < C$ rather than normalizing $\|\theta_k\|$ to $C$?  This question seems relevant given that some authors
(e.g., \cite{bethune_pay_2022}) also suggest to consider networks which constant gradient norm rather than maximal gradient norm, i.e., roughly with $\theta$ in
%papers consider only models from
$\thetaSpaceEqC$ rather than $\thetaSpaceLeC$. % \cite{bethune_pay_2022}.
% \item[Q3] Does \weightDpSgd{} induce less bias than \gradDpSgd{}?
% \item[Q3] What is the impact of using norms other than the \(2\)-norm?
\end{itemize}

\paragraph{Norms.}\label{exp:norm} In this section, we focus on the \(\ell_{2,2}\) norm. All subsequent results are based on this norm. Although computing this norm is more computationally intensive, it allows for the use of the Gaussian mechanism, which is standard in this field.

\paragraph{Hyperparameters.}\label{exp:hyper} We selected a number of hyperparameters to tune for our experiments, aiming at making a fair comparison between the studied techniques while minimizing the distractions of potential orthogonal improvements.  To optimize these hyperparameters, we used Bayesian optimization \cite{balandat_botorch_2020}.
Appendix \ref{sec:app.exp.hyperparameters} provides a detailed discussion.


\paragraph{Datasets and models.} We carried out experiments
on both tabular datasets and datasets with image data.
%in two phases. In the initial phase, our focus was on widely-recognized
First, we consider a collection of 7 real-world tabular datasets (names and citations in Table \ref{tab:auc}).  For these, we trained multi-layer perceptrons (MLP). A comprehensive list of model-dataset combinations is available in the Appendix \cref{tab:data}.
To answer question Q2, we also implemented \globalWeightDpSgd{}, a version of \weightDpSgd{} limited to networks whose weight norms are fixed, i.e., $\forall k:\|\theta_k\|=C$, obtained by setting $\thetanorm_k\gets C$ in Line \ref{ln:setThetaNorm} in Algorithm \ref{alg:weight-dpsgd}.

Second, the image datasets used include MNIST \cite{deng2012mnist}, 
Fashion-MNIST \cite{xiao_fashion-mnist_2017}, and CIFAR-10 \cite{cifar}. 
We trained convolutional neural networks (CNNs) for the first two datasets
 and a Wide-ResNet \cite{Zagoruyko2016WRN} for the latter 
 (see details in \cref{tab:data}). We implemented all regularization 
 techniques as described in \cite{de2022unlocking}, including group 
 normalization \cite{Wu2018GroupN}, large batch size, weight 
 standardization \cite{qiao2020microbatchtrainingbatchchannelnormalization}, 
 augmentation multiplicity \cite{de2022unlocking}, 
 and parameter averaging \cite{Polyak1992AccelerationOS}. 
 These techniques are compatible with Lipschitz-constrained networks, 
 except for group normalization, for which we proposed an adapted version 
 in \cref{eq:safe.group.normalization}.\\
We opted for the accuracy to facilitate easy comparisons with prior research.


\paragraph{Infrastructure.} All experiments were orchestrated across dual Tesla P100 GPU platforms (12GB capacity), operating under CUDA version 10, with a 62GB RAM provision for Fashion-MNIST and CIFAR-10. Remaining experiments were performed  on an E5-2696V2 Processor setup, equipped with 8 vCPUs and a 52GB RAM cache. The total runtime of the experiments was approximately 50 hours, which corresponds to an estimated carbon emission of $1.96$ kg %, as reported in
\cite{lacoste2019quantifying}.
More details on the experimental setup and an analysis of the runtime can be found in Appendix \ref{sec:app.exp}.

\begin{figure*}[ht]
  \centering
  \mbox{
    \subfigure[MNIST\label{fig:mnist_perf}]{\includegraphics[width=0.32\linewidth]{plots/mnist/acc_eps.png}}\quad
    \subfigure[Fashion-MNIST\label{fig:FashionMNIST_perf}]{\includegraphics[width=0.32\linewidth]{plots/fashionmnist/acc_eps.png}}\quad
    \subfigure[CIFAR-10\label{fig:cifar_perf}]{\includegraphics[width=0.32\linewidth]{plots/cifar/acc_eps.png}}
  }

  \caption{Accuracy results, with a fixed \(\delta = 10^{-5}\), for the MNIST (\ref{fig:mnist_perf}), Fashion-MNIST (\ref{fig:FashionMNIST_perf}), and CIFAR-10 (\ref{fig:cifar_perf}) test datasets. The plots show the median accuracy over 5 runs, with vertical lines indicating the standard error of the mean. See Appendix \ref{sec:app.exp.hyperparameters} for details on model specifications and hyperparameters.}
  \label{fig:big_perf}
\end{figure*}

\begin{table*}[h]
  \caption{Accuracy and $\epsilon$ per dataset and method at $\delta=1/n$, in bold the best result and underlined when the difference with the best result is not statistically significant at a level of confidence of 5\%.}
  \label{tab:auc}
  \centering
  \begin{tabular}{lrrrrrrr}
    \toprule
    Methods & & \gradDpSgd{}   & \weightDpSgd{} & \globalWeightDpSgd{}  \\
    Datasets (\#instances $n$ $\times$ \#features $p$) & $\epsilon$ &  &  &  \\
    \midrule
    Adult Income (48842x14) \cite{misc_adult_2} & 0.414 & 0.824 & \textbf{0.831} & 0.804 \\
    Android (29333x87) \cite{misc_naticusdroid_android_permissions_dataset_722} & 1.273 & 0.951 & \textbf{0.959} & 0.945 \\
    Breast Cancer (569x32) \cite{misc_breast_cancer_wisconsin_diagnostic_17} & 1.672 & 0.773 & \textbf{0.798} & 0.7 \\
    Default Credit (30000x24) \cite{misc_default_of_credit_card_clients_350} & 1.442 & 0.809 & \textbf{0.816} & 0.792 \\
    Dropout (4424x36) \cite{misc_predict_students'_dropout_and_academic_success_697} & 1.326 & 0.763 & \textbf{0.819} & 0.736 \\
    German Credit (1000x20) \cite{misc_statlog_german_credit_data_144} & 3.852 & 0.735 & \underline{0.746} & 0.768 \\
    Nursery (12960x8) \cite{misc_nursery_76} & 1.432 & 0.919 & \textbf{0.931} & 0.89 \\
    \bottomrule
  \end{tabular}
\end{table*}

\subsection{Results}\label{subsec:results}
% Figure \ref{fig:big_perf} shows our results for the image data sets. For each one of them we report results with or without group normalization applied with \gradDpSgd{} and \weightDpSgd{}.
%
% Table \ref{tab:auc} shows our results on the tabular data, comparing \gradDpSgd{} with \weightDpSgd{} and \globalWeightDpSgd{}.
%
%  Appendix \ref{sec:app.exp} presents a number of more detailed and complementary results.
%
% \subsection{Discussion}\label{subsec:discussion}

\paragraph{Image datasets.} In \cref{fig:big_perf}, \weightDpSgd{} 
surpasses all state-of-the-art results on all three image datasets. 
Previous performances were based on DP-SGD as introduced by \cite{abadi_deep_2016}, 
either combined with regularization techniques \cite{de2022unlocking} 
or with bespoke activation functions \cite{Papernot_Thakurta_Song_Chien_Erlingsson_2021}. Note that while we present 
results using the same set of hyperparameters for MNIST and Fashion-MNIST, 
the results for CIFAR-10 come from a Pareto front of two sets of 
hyperparameters. For epsilon values below 4.2, the results come from a 
Wide-ResNet-16-4, and for epsilon values above 4.2, they come from a 
Wide-ResNet-40-4. See the complete list of hyperparameters 
in \cref{sec:app.exp.hyperparameters}.

\paragraph{Tabular datasets.} In \cref{tab:auc}, we perform a Wilcoxon Signed-rank test, at a confidence level of $5$\%, on $10$ measures of accuracy for each dataset between the \gradDpSgd{} based on the gradient clipping and the \weightDpSgd{} based on our method. \weightDpSgd{} consistently outperforms \gradDpSgd{} in terms of accuracy. This trend holds across datasets with varying numbers of instances and features, including tasks with imbalanced datasets like Dropout or Default Credit datasets. While highly impactful for convolutional layers, group normalization does not yield improvements for either \gradDpSgd{} or \weightDpSgd{} in the case of tabular datasets. See Appendix \ref{app:gn_table} for complete results.

Additionally, \cref{tab:auc} presents the performance achieved by constraining networks to Lipschitz networks, where the norm of weights is set to a constant, denoted as \globalWeightDpSgd{}. The results from this approach are inferior, even when compared to \gradDpSgd{}.

\paragraph{Conclusion.} In summary, our experimental results demonstrate that \weightDpSgd{} sets new state-of-the-art benchmarks on the three most popular vision datasets, outperforming \gradDpSgd{}. Additionally, \weightDpSgd{} also outperforms \gradDpSgd{} on tabular datasets using MLPs, where it is advantageous to allow the norm of the weight vector \(\theta\) to vary rather than normalizing it to a fixed value, leveraging situations where it can be smaller.


%\textbf{Scaling strategies. } By default, we used the smallest available parameter norm at each step, denoted as $\thetanorm$, where $\thetanorm_k = \min(C, \|\theta\|_2)$, as outlined in \cref{alg:weight-dpsgd}. This "local sensitivity" scaling strategy differs from the "global sensitivity" approach, which always sets $\thetanorm_k = C$, potentially reducing runtime but not maximizing the current position of $\theta$ in the search. Testing on tabular datasets, the local method generally outperforms the global strategy at a 5\% confidence level, except for three datasets where differences are not significant. Detailed results are in Appendix \ref{tab:auc-localglobal}.
