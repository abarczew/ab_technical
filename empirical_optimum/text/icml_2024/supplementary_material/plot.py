import torch
import pickle
import argparse
import matplotlib.colors as mcolors
from scipy.stats import wilcoxon
from opacus.accountants.utils import get_noise_multiplier
from netcal.presentation import ReliabilityDiagram
from netcal.metrics import ECE
from netcal.scaling import TemperatureScaling
import numpy as np
import ast
import ax
import os
import matplotlib.pyplot as plt
import pandas as pd
from ax.service.ax_client import AxClient
from ax.service.utils.instantiation import ObjectiveProperties
from ax.utils.notebook.plotting import render
from ax.plot.pareto_utils import compute_posterior_pareto_frontier
from ax.plot.pareto_frontier import plot_pareto_frontier
plt.style.use('seaborn-v0_8-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)
markers = ['o', '^', 's', 'P', 'v', '>', '<', '*', '+', 'x', 'D', 2]

methods = ['grad_clip', 'weight_norm']
exp_names = ['mnist', 'fashionmnist', 'cifar',
             'income', 'android', 'breast', 'default_credit',
             'dropout', 'german', 'nursery']

linestyles = ['-', '--']
colors = ['tab:blue', 'tab:red']

parser = argparse.ArgumentParser()
parser.add_argument('--exp-name', '-e', dest='exp_name', default='mnist',
                    choices=exp_names,
                    help='experiment name: ' + ' | '.join(exp_names) +
                    ' (default: mnist)')
parser.add_argument('--save-dir', '-s', dest='save_dir',
                    help='The directory used to save the results',
                    default='save_temp', type=str)


def plot_evaluation(exp_name,
                    filename='trials.csv',
                    folder_weight='experiment_nogroup',
                    folder_weight_group='experiment_group',
                    folder_grad='experiment_nogroup',
                    folder_grad_group='experiment_group',
                    metric="accuracy",
                    methods=methods,
                    agg='mean'):
    groups = ['group', 'nogroup']
    paths = {'grad_clip': {'nogroup': folder_grad,
                           'group': folder_grad_group},
             'weight_norm': {'nogroup': folder_weight,
                             'group': folder_weight_group}}
    path_results = f"exp/{exp_name}/plot/{folder_weight_group}_vs_nogroup/"

    if not os.path.exists(path_results):
        os.makedirs(path_results)

    for i, method in enumerate(methods):
        if method == 'grad_clip':
            method_name = 'DP-SGD'
        else:
            method_name = 'Lip-DP-SGD'
        for j, group in enumerate(groups):
            if group == 'group':
                group_name = 'w/ GroupNorm'
            else:
                group_name = 'w/o GroupNorm'
            path = f"exp/{exp_name}/{method}/{paths[method][group]}/{filename}"
            if not os.path.exists(path):
                continue
            df_evaluation = pd.read_csv(path)
            df_plot = df_evaluation[['noise_multiplier', 'epsilon', metric]].groupby(
                'noise_multiplier').agg([agg, 'std']).reset_index()

            plt.errorbar(df_plot['epsilon'][agg],
                         df_plot[metric][agg],
                         yerr=df_plot[metric]['std'],
                         label=f'{method_name} {group_name}',
                         marker=markers[i],
                         linestyle=linestyles[j],
                         color=mcolors.TABLEAU_COLORS[colors[i]])

    plt.grid()
    plt.xlabel('Epsilon')
    plt.xscale('log')
    if metric == "accuracy":
        plt.ylabel("Accuracy")
    elif metric == 'auc':
        plt.ylabel("AUC")
    elif metric == 'ece':
        plt.ylabel('ECE')
    plt.legend(loc='lower right')

    plt.savefig(f'{path_results}{metric}_results.png', bbox_inches='tight')
    plt.close()


def run():
    args = parser.parse_args()
    plot_evaluation(args.exp_name,
                    filename='trials.csv',
                    folder_weight=f'{args.save_dir}_nogroup',
                    folder_weight_group=f'{args.save_dir}_group',
                    folder_grad=f'{args.save_dir}_nogroup',
                    folder_grad_group=f'{args.save_dir}_group',
                    metric="accuracy",
                    methods=methods,
                    agg='mean')


if __name__ == '__main__':
    run()
