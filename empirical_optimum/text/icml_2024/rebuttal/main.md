# First review
1. Enforcing Lipschitzness in deeper models requires some care, but there are techniques available to ensure Lipschitz neural networks can efficiently handle any classification task [1]. Our paper does not focus on these techniques but on introducing a new approach to differentially private training, distinct from standard DP-SGD. We've demonstrated that when Lipschitzness is enforced, Lip-DP-SGD performs better than DP-SGD on many tasks with similar architectures. These findings hold true even for more complex architectures, provided that Lipschitzness is maintained without significantly impacting gradients, as detailed in the cited paper.

[1] Louis Béthune, Thibaut Boissin, Mathieu Serrurier, Franck Mamalet, Corentin Friedrich, and Alberto Gonzalez Sanz. Pay attention to your loss : understanding misconceptions about lipschitz neural networks. Advances in Neural Information Processing Systems, 2022

2. With the exception of CIFAR-10 and the German credit dataset, Lip-DP-SGD consistently outperforms DP-SGD.  It is normal that approaches have advantages and disadvantages, it very rare to see new techniques which perform much better on every possible existing dataset.  We honestly also report datasets where the improvement is smaller, which should be a strength of the paper.  

3. During the forward pass, actual data is not utilized; instead, the norm at which all data is scaled is employed. Thus, this norm effectively becomes a hyperparameter and can be utilized without compromising privacy.

4. We refer the reviewer to Appendix C.3 "Runtime" which provides a plot comparing the runtime of Lip-DP-SGD on two different tasks.

# second reviewer
The constraint on Lipschitzness ensures that the Lipschitz value remains within bounds, preventing it from skyrocketing. Enforcing Lipschitzness in deeper models requires some care, but there are techniques available to ensure Lipschitz neural networks can efficiently handle any classification task [1]. We've demonstrated that when Lipschitzness is enforced, Lip-DP-SGD performs better than DP-SGD on many tasks with similar architectures. These findings hold true even for more complex architectures, provided that Lipschitzness is maintained without significantly impacting gradients, as detailed in the cited paper.

[1] Louis Béthune, Thibaut Boissin, Mathieu Serrurier, Franck Mamalet, Corentin Friedrich, and Alberto Gonzalez Sanz. Pay attention to your loss : understanding misconceptions about lipschitz neural networks. Advances in Neural Information Processing Systems, 2022

# Third reviewer
1. DP-SGD introduces a bias in that training converges to a non-optimal model, see also [2], while Lip-DP-SGD does converge to the optimal model satisfying the Lipschits constraint.  The Lipschitz constraint itself does not constitute a bias or decrease in predictive performance [1].  In case of need, one can always increase the Lipschitz upper bound so the Lip-DP-SGD model gets closer to the unconstrained model.  Regarding the DP variant of the proof, we specify in the current analysis that "In the presence of noise and/or stochastic batch selection, algorithms of course don’t converge to a specific point but move around close to the optimal point due to the [unbiassed] noise in each iteration, and advanced methods exist to examine such kind of convergence." Lastly, concerning the terminology "adding" of the gradient, it is indeed a typo; we meant "subtracting," noting that this correction does not alter the validity of the proof.

[1] Louis Béthune, Thibaut Boissin, Mathieu Serrurier, Franck Mamalet, Corentin Friedrich, and Alberto Gonzalez Sanz. Pay attention to your loss : understanding misconceptions about lipschitz neural networks. Advances in Neural Information Processing Systems, 2022
[2] Chen, X., Wu, S. Z., and Hong, M. Understanding gradient clipping in private sgd: A geometric perspective, Neural Information Processing Systems, 2020

2. The main difference lies in the model choice. De et al. utilize a 40-layered ResNet, whereas we employ a 6-layered VGG. Hence, we display baselines without group normalization to show the improvement brought by the latter. From this, one can see that the enhancement observed by De et al. solely due to group normalization is similar to the one we show on DP-SGD.

3. In Lip-DP-SGD, similar to DP-SGD, the focus is on ensuring privacy in the model parameters, rather than guaranteeing privacy at each individual server operation. For instance, operations like gradient clipping require access to the actual gradient norm, thus compromising privacy. However, there is merit in exploring ways to adapt Lip-DP-SGD for setups where privacy is crucial at specific stages. As for the new hyperparameter $\alpha$, it can be tuned but the real purpose of it is to prevent the group standard deviation from vanishing, so a good choice is to set $\alpha$ at a value with the same order of magnitude as the inverse of size of the group. We will add this note in the final version.

4. Indeed, it's accurate to note that Alg. 1 tends to overestimate the Lipschitz value of the overall model. However, despite this overestimation, the value remains bounded due to our enforcement of Lipschitzness. While enforcing Lipschitzness does incur a loss in utility, it also prevents from using large DP noise. Consequently, the tradeoff between utility and privacy is similar to the one DP-SGD copes with.

5. We thank the reviewer for highlighting typos in the text, we will amend the final text accordingly.

## Official comment on rebuttal

Thank you very much for your comment.
Q1. We hope that at least it is clear that it is not true that "DP-SGD moves around the optimal point.".  This is not new, but already pointed out by [Chen et al.].  The idea is that at the optimal point, where the sum of the (unclipped) gradients is zero, the sum of the clipped gradients is not zero (due to the clipping).

[Chen et al.] provide a simple example: Consider optimizing $f(x)=\frac{1}{3} \sum_{i=1}^3 \frac{1}{2}\left(x-a_i\right)^2$ over $x \in \mathbb{R}$, where $a_1=a_2=-3$ and $a_3=9$. Since the gradient $\nabla f(x)=x-1$, the optimum is $x^*=1$. Now suppose we run SGD with gradient clipping with a threshold of $c=1$. At the optimum, the gradients for all three examples are clipped and the expected clipped gradient is $1 / 3$, which leads the parameter to move away from $x^*$.

The addition of noise doesn't change much, consider for example the case where we have a bit of noise but less than the difference between the sum of the gradients and the sum of clipped gradients.  In that case, even with "favorable noise", DP-SGD will keep drifting away from the optimal point.

In contrast, in case of weight clipping (Lip-DP-SGD), if we are at the optimal point, the expected value of the gradient plus noise will be zero (because the noise is zero-mean).    We admit that our proof is not fully rigorous for the case where there is noise, but it shows the limit for the regime with small amounts of noise and this doesn't take away the shortcomings of DP-SGD on which several previous papers agreed.


Q3. There is maybe a misunderstanding here.  We simply want to point out that DP-SGD needs access to the non-privatized data, so should be executed by a party trusted with that part of the data.  For example, gradient clipping needs information about the real data, which would compromise privacy IF the party running that step of the DP-SGD algorithm would not be trusted with that part of the data.  We make the same assumptions for Lip-DP-SGD, i.e., it is executed by a party (or parties in the federated setting) which trusted with the relevant parts of the data.  The only requirement, similarly to DP-SGD is that its output is differentially private.  Now, the DP-SGD algorithm computes $W$, and so does Lip-DP-SGD.  You agree that DP-SGD is accepted to be privacy-preserving.  It is privacy preserving because this $W$ is made differentially private by adding appropriate amounts of noise.  We know that the amounts of noise added are sufficient, because in DP-SGD the clipped gradients have bounded norm and in Lip-DP-SGD the gradients have bounded norm due to the Lipschitz property.  Now that both DP-SGD and Lip-DP-SGD have already access to $W$, it doesn't harm privacy if Lip-DP-SGD also computes from it $\\|W\\|_2$.  


Q4. As said, we agree that Alg. 1 may output an overestimation.  The reviewer speculates that this overestimation will lead to poor performance as DP-SGD does not need to make approximations, but our experimental results suggests that Lip-DP-SGD always performs comparably or better than DP-SGD.  If that is a surprising result (given the arguments of the reviewer), that may strengthen the value of our results.  We agree many further improvements are possible, but at least our results suggest weight clipping is a viable idea.



# Forth reviewer
1. [1] projects the model parameters regardless of the parameter norm, aiming to prevent gradients from vanishing or exploding. In contrast, our approach focuses on bounding the Lipschitz value to limit excessive DP noise. Additionally, it's worth noting that [1] does not exploit the significant synergy between Lipschitz-constrained networks and group normalization.

2. We thank the reviewer for highlighting this typo, we will correct the final text accordingly.

3. It is a great idea. After looking at [2], it seems like Lip-DP-SGD could also benefit from per-layer clipping since the model parameters can be clipped layer-wise. As for the results, it's hard to tell if the modified group normalization would decrease the performances. Indeed, even modified, the group normalization showed a greater positive impact with Lip-DP-SGD than the regular group normalization with DP-SGD on two datasets out of three.

4. We thank the reviewer for pointing this out. These plots can be made with the code provided but we will add them in the Appendix for completeness.

5. Yes, understanding the training dynamics is in terms of SNR as well as further theoretical analysis of Lip-DP-SGD remains an interesting area of research as we noted in our conclusion.
