from lip_dp import LipPrivacyEngine

lipprivacy_engine = LipPrivacyEngine()

model, optimizer, train_loader = lipprivacy_engine.make_private_with_epsilon(
    module=model,
    optimizer=optimizer,
    data_loader=train_loader,
    epochs=EPOCHS,
    target_epsilon=EPSILON,
    target_delta=DELTA,
)
