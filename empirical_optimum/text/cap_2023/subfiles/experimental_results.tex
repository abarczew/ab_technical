\section{Experimental results}\label{sec:experiment}
In this section we present the performance of our methodology of tighter partial derivative bounds of the LogLoss function with both the strong composition theorem and the moment accountant. We will show that our method brings better results than any set of popular bounds of the norm of the gradient used with any set of privacy loss accountant.

More precisely, we evaluate the performances of three methods to scale the noise i.e., $\textsc{Lip}$, $\textsc{Clip}$ and $\textsc{PartialBound}$, which respectively stand for methods based on the Lipschitz constant, the clipping norm and the bounds of the partial derivatives of the loss (our method). They are all used with two privacy loss accountants i.e., the strong composition theorem ($\textsc{Comp}$) or the moment accountant ($\textsc{Ma}$). Note that, by doing so, we compare our methodology with the two most popular DP-SGDs: one based on the Lipschitz constant with the strong composition theorem \cite{bassily_differentially_2014} ($\textsc{LipComp}$) and the other based on the clipping norm of the gradient with moments accountant \cite{abadi_deep_2016} ($\textsc{ClipMa}$).

% We show results on 4 popular datasets: MNIST dataset, Breast Cancer dataset, Titanic dataset and Adult Income dataset.
We show results on four datasets: the MNIST dataset, the breast cancer dataset, the adult income dataset and the android permissions dataset. We record two evaluation metrics, the area under the R.O.C. curve (AUC) and the average standard deviation of noise added to the weights at each time step. The former metric ensures the overall utility while the latter shows how close we are to non-DP weights.

As described in \ref{sec:approach}, all DP-SGD methods are implemented within a logistic regression model, trained in $6$ epochs each of them containing $n$ samples randomly picked with replacement and loaded in batch of $1$ (so the number of steps is $T=6n$). We fixed the target $\delta=(1 / n)^2$ and the learning-rate and the clipping norm  are fined tuned depending on the dataset. When the privacy loss accountant is the moment accountant, then we use the Python \verb|dp_accounting.calibrate_dp_mechanism| search function to calibrate the corresponding noisemultiplier value (function $\textsc{SearchDPNoise}$ in \cref{alg:scale_noise}).

We first focus on the MNIST dataset and then sum up results for all four datasets.

\subsection{MNIST}
This dataset contains handwritten digits images and the corresponding labels (0-9). It is split in one training dataset of 60,000 examples and a testing dataset of 10,000 examples. In order to apply a logistic regression on it, we expand the 28x28 size grey-level images in one normalized vector of 784 coordinates.

\begin{figure}[ht]
  \vskip 0.2in
  \begin{center}
    \centerline{\includegraphics[width=\columnwidth]{../../src/results/logloss_mnist_auc_vs_epsilon_lr=0.001_C=0.0.png}}
    \caption{AUC for logistic regression on the MNIST dataset per values of $\epsilon$, $\gamma=0$ ($1$-norm regularizer), $lr=0.001$}
    \label{fig:mnist_auc}
  \end{center}
  \vskip -0.2in
\end{figure}

\cref{fig:mnist_auc} shows that, our method, $\textsc{PartialBound}$, brings better performances than any other bound. What is more is that even used with the strong composition theorem, $\textsc{Comp}$, our method, $\textsc{PartialBoundComp}$, is better than any other bound used with moment accountant e.g., $\textsc{ClipMa}$, at any privacy level, from $\epsilon$ equals $0.01$ to $10$. Unsuprisingly, our method benefits from the moment accountant as it boosts perfomances for any values of $\epsilon$ except for high values ($\epsilon \geq 10$) where the gap is closed between the strong composition theorem and the moment accountant.

\begin{figure}[ht]
  \vskip 0.2in
  \begin{center}
    \centerline{\includegraphics[width=\columnwidth]{../../src/results/logloss_mnist_auc_vs_epsilon_lr=0.001_C=0.01.png}}
    \caption{AUC for logistic regression on the MNIST dataset per values of $\epsilon$, $\gamma=0.01$ ($1$-norm regularizer), $lr=0.001$}
    \label{fig:mnist_auc_penalty}
  \end{center}
  \vskip -0.2in
\end{figure}

However, it seems that at very high privacy levels ($\epsilon$ $\leq 0.01$), the only parameter that can enable learning (AUC $> 0.5$) is the $1$-norm regularizer, only when applied to our method, as shown in \cref{fig:mnist_auc_penalty}. Indeed, the $\textsc{PartialBound}$ is the only bound that leverages the sparsity of weights resulting from the $1$-norm regularizer while it impacts negatively all the other methods. Although, it seems that this effect curbs as $\epsilon$ grows as shown in \cref{fig:mnist_sigma}: the average standard deviation of noise added to the weights at each time step is more than $10^2$ times bigger on $\textsc{PartialBoundMa}$ between $\gamma=0.01$ ($1$-norm regularizer) and $\gamma=0$ at $\epsilon=0.001$ and goes down to less than $10$ times bigger at $\epsilon=10$. This translates in performaces being even slightly better for $\epsilon \geq 10$.

\begin{figure}[ht]
  \vskip 0.2in
  \begin{center}
    \centerline{\includegraphics[width=\columnwidth]{../../src/results/logloss_mnist_sigma_vs_epsilon_lr=0.001.png}}
    \caption{Average standard deviasion of result weights frop logistic regression on the MNIST dataset per values of $\epsilon$ and $\gamma$ ($1$-norm regularizer), $lr=0.001$}
    \label{fig:mnist_sigma}
  \end{center}
  \vskip -0.2in
\end{figure}

In a nutshell, Gaussian noise scaled on our bound $\textsc{PartialBound}$ is always lower than the one scaled on the clipping norm or on the Lipschitz constant as shown in \cref{fig:mnist_sigma}. Thanks to that, DP-SGD relying on $\textsc{PartialBound}$ performs always better than the ones from \cite{bassily_differentially_2014} and \cite{abadi_deep_2016}, even when we use our bound with the strong composition theorem. Moment accountant and the $1$-norm regularizer enhance perfomances, the latter even enabling learning for $\epsilon \leq 0.01$, although their boosting effect fade away as $\epsilon$ grows.

\subsection{Other datasets}
We show results of our experiment on three other datasets: the breast cancer dataset \cite{Wolberg_breast:1995}, the adult income dataset \cite{Kohavi_adult:1996} and the android permissions dataset \cite{misc_naticusdroid_(android_permissions)_dataset_722}. For each one of them, we compare our method, $\textsc{PartialBoundMa}$, with the clipping norm with moment accountant, which is the most powerful and popular method we can compare to. For this purpose, we compute the relative difference in AUC between the two methods i.e.,  $metric = \operatorname{AUC}_{\textsc{PartialBoundMa}}/\operatorname{AUC}_{\textsc{ClipMa}} - 1$.


\begin{table}[t]
\caption{Relative difference in AUC between $\textsc{PartialBoundMa}$ and $\textsc{ClipMa}$.}
\label{tab:auc}
\vskip 0.15in
\begin{center}
\begin{small}
\begin{sc}
\begin{tabular}{lrrrr}
\toprule
$\epsilon$ &  0.01  &  0.1  &  1.0  &  10.0 \\
Dataset ($n\text{x}p$) &        &        &        &        \\
\midrule
Adult (48842x14) &   0.04 &   0.31 &   0.16 &   0.07 \\
Breast (569x32) &  -0.06 &  -0.02 &   0.00 &  -0.01 \\
Android (29333x87) &  -0.01 &   0.18 &   0.00 &   0.00 \\
MNIST (70000x724) &   0.42 &   0.68 &   0.58 &   0.24 \\
\bottomrule
\end{tabular}
\end{sc}
\end{small}
\end{center}
\vskip -0.1in
\end{table}

\cref{tab:auc} shows that our method is the best one, especially for values of $\epsilon$ below $1$. We get between 18\% and 68\% better AUC when $\epsilon=1$ except for the breast cancer dataset. On the latter, our method fails to bring any improvement due to the small size of it (569x32). In this context, there are two few gradient steps taken ($n$ times the number of epoch $6$) and the width of the dataset does not allow to benefit from both our uppper-bound denomitor, which grows with $p$, and $\gamma$ ($1$-norm regularizer). To a lesser extent, the same goes for the adult income dataset and the Android permissions dataset. When $1$-norm regularizer is applied, the gain in variance of noise is to small to make up for the graet loss of information. In comparaison, MNIST seems to have the perfect shape to take the most out of our method.
