\section{Our approach}
\label{sec:method}

In this section, we detail how taking into account the position of the current model in the search space (local Lipschitz constant) rather than using the worst case gradient norm (global Lipschitz constant) can bring better utilility when designing a differentially private SGD algorithm. We illustrate this first with the logistic regression and then with deep neural networks (DNN).
% Finally, we highlight settings that always benefit from local Lipschitz constant such as using the $1$-norm regularizer.

\subsection{Scaling the noise on the local Lipschitz constant}
We propose a new algorithm which for a given input $(\dset,\ell,\Theta,\epsilon,\delta,T,\batchsize,\eta,g_{\max})$ behaves as a call to $\textsc{DP-SGD}(\dset,\ell,\Theta,\epsilon,\delta,T,\batchsize,\eta,g_{\max},"C","local")$ or to $\textsc{DP-SGD}(\dset,\ell,\Theta,\epsilon,\delta,T,\batchsize,\eta,g_{\max},"M","local")$ in \cref{alg:dpsgd}. The main difference with previous algorithms \cite{bassily_differentially_2014}\cite{abadi_deep_2016} is that the bound on the sensitivity of the gradient isn't constant across all updates of the SGD anymore. Instead, a "local" Lipschitz constant $L^g$ is computed as in \cref{alg:scale_noise} and would typically depend on the current value of the model parameter $\theta$. One can note that $\theta$ is computed privately through out the SGD. So, whatever is the privacy accounting method "$M$" or "$C$", scaling the noise on the the "local" Lipschitz constant implies differential privacy.

\begin{theorem}\label{th:dpsgd}
  $\textsc{DP-SGD}(\dset,\ell,\Theta,\epsilon,\delta,T,\batchsize,\eta,g_{\max},"C","local")$  and $\textsc{DP-SGD}(\dset,\ell,\Theta,\epsilon,\delta,T,\batchsize,\eta,g_{\max},"M","local")$ are $(\epsilon, \delta)$-differentially private
\end{theorem}

Scaling the noise to a smaller bound of the sensitivity of the gradient leads to adding less noise and eventually getting better performances. Computing this bound at each update of the SGD gives the opportunity to have smaller values than the global bound. We show in \cref{sec:experiment} how this method indeed strongly enhances performances even when applied within the logistic regression.

\subsection{Local Lipschitz constant of the logistic regression}
Let's find an upper-bound of the gradient of the logistic regression that depends on the position in the search space $\Theta$. For all $\theta$ in $\Theta$ and $(x,y)$ in $Z$, we have
\[
\nabla_\theta\ell^{ll}(x, y) = \left(S(x;\theta) - y\right)x
\]
see details in \cref{app:logistic}. Since $x \in [-1, 1]^p$, $S(x;\theta)$ is maximal when $\forall i \in [p], x_i = \text{sign}(\theta_i)$. For the same reason $S(x;\theta) - 1$ is maximal when $\forall i \in [p], x_i = -\text{sign}(\theta_i)$. As $S(x;\theta) - 1 = -S(-x;\theta)$, all in all we get

\begin{equation}\label{eq:leq_logloss_partial}
\begin{split}
  \left|\frac{\partial\ell^{ll}}{\partial\theta_j}(x, y)\right|
  & \leq \frac{|\operatorname{sign}(\theta_j)|}{1+e^{-\|\theta\|_1}} \\
  & \leq \frac{1}{1+e^{-\|\theta\|_1}}
\end{split}
\end{equation}

\eqref{eq:leq_logloss_partial} provides an upper-bound that decreases with the $1$-norm of $\theta$. We show in \cref{sec:experiment} that using this upper-bound as the "local" Lipschitz strategy in \cref{alg:scale_noise} produces smaller $\sigma$ and eventually better performance in \cref{alg:dpsgd} than with the "global" strategy. One can also see that, adding a $1$-norm regularizer to the loss would directly lower down the bound provided by the "local" Lipschitz strategy as opposed to the "global" strategy. We also confirm this statetment empirically in \cref{sec:experiment}.

\subsection{Local Lipschitz constant of Deep Neural Networks (DNN)}
Now let's find the upper-bound of the sensitivity of the gradient of a DNN. First, we will do so with a one layer DNN and then we will extend these results to all multi-layer perceptrons. In particular we are going to compute the upper-bound of the gradients of a one-layer neural network trained with cross-entropy loss. The forward pass of the model is as follows:
$$
\begin{aligned}
\boldsymbol{x} & =\text { input } \\
\boldsymbol{z} & =\boldsymbol{W} \boldsymbol{x} \\
\boldsymbol{h} & =\operatorname{S}(\boldsymbol{z}) \\
\boldsymbol{\theta} & =\boldsymbol{U} \boldsymbol{h} \\
\hat{\boldsymbol{y}} & =\operatorname{S}(\boldsymbol{\theta}) \\
\ell & =C E(\boldsymbol{y}, \hat{\boldsymbol{y}})
\end{aligned}
$$
with $\operatorname{S}$ the sigmoid function. We need to compute both $\frac{\partial\ell}{\partial U}$ and $\frac{\partial\ell}{\partial W}$.
$\frac{\partial\ell}{\partial U}$ is equivalent to $\nabla_\theta\ell^{ll}$ that we computed in the previous section, except that $h \in [0, 1]^{d_U}$ with $d_U$ the width of $U$. So we have
$$
\left\| \frac{\partial\ell}{\partial U}\right\|_2 \leq \frac{\sqrt{d_U}}{1 + e^{-\|U\|_1}}
$$
Now, let's apply the chain rule to $\frac{\partial\ell}{\partial W}$:
$$
\begin{aligned}
  \frac{\partial\ell}{\partial W} & = \frac{\partial\ell}{\partial h}*\frac{\partial h}{\partial W}
\end{aligned}
$$
Again, based on the previous section, we have
$$
  \begin{aligned}
    \frac{\partial\ell}{\partial h} & = (S(Uh^\top) - y)U \\
    \frac{\partial h}{\partial W} & = S(Wx^\top)S(\mathbbm{1} - Wx^\top)x^\top
  \end{aligned}
$$
All in all, we get
$$
\begin{aligned}
  \left \| \frac{\partial\ell}{\partial W} \right\|_2 & = \| (S(Uh^\top) - y)U S(Wx^\top)S(\mathbbm{1} - Wx^\top)x^\top \| \\
  & \leq \| S(U\mathbbm{1}^\top) - 1)U S(W\mathbbm{1}^\top)S(\mathbbm{1} - W\mathbbm{1}^\top)\mathbbm{1}^\top \|_2
\end{aligned}
$$
with $\|.\|_2$ of a matrix the vector of $\|.\|_2$ of its rows.

Recursively, we can extend this result to any multi-layer perceptrons. The loss of the model is $\mathcal{L}(x, y):= \ell\left(y, f(x)\right)$ with $\ell$ the cross-entropy, $f = Act^l\circ FC^l\circ\dots\circ Act^0FC^0$, $FC^i$ the $i$-th fully connected layer with parameter $W^i$ so that $FC^i(x) = W^i x^\top$ and $Act^i$ the $i$-th activation function, in our case the sigmoid function. The cross-entropy and the sigmoid functions can be replaced by any other loss and activation function, here we just wanted to capitalize on the previous section. As a results, for any layer $i > 1$, we get
\begin{equation}\label{eq:leq_dnn}
  \begin{aligned}
    \left\|\frac{\partial\mathcal{L}}{\partial W^i}\right\|_2 \leq & \| (S(W^i\mathbbm{1}^\top)-y)\\
    &\quad \prod_{k=1}^{i-1}S(W^k\mathbbm{1}^\top)S(\mathbbm{1}-W^k\mathbbm{1}^\top)W^k \\
    &\quad S(W^0\mathbbm{1}^\top)S(\mathbbm{1}-W^0\mathbbm{1}^\top)\mathbbm{1}^\top\|_2
  \end{aligned}
\end{equation}

\eqref{eq:leq_dnn} provides an upper-bound that decreases with the $2$-norm of the parameters $W^i$. We show in \cref{sec:experiment} that using this upper-bound as the "local" Lipschitz constant strategy in \cref{alg:scale_noise} produces smaller $\sigma$ and eventually better performance in \cref{alg:dpsgd} than with the "global" strategy. One can also see that, on top of adding a $2$-norm regularizer to the loss, the drop-out regularization can directly lower down the bound provided by the "local" Lipschitz strategy as opposed to the "global" strategy. We also confirm this statement empirically in \cref{sec:experiment}.
