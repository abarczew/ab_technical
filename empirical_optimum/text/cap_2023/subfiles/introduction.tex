

\section{Introduction}
\label{sec:intro}

While machine learning allows for extracting statistical information from data with both high economical and societal value, there is a growing awareness of the risks for data privacy and confidentiality.  Differential privacy \cite{dwork_algorithmic_2013} has emerged as an important standard for studying statistical privacy.

Due to the popularity of deep neural networks (DNNs) and similar models, one of the recently most trending algorithmic techniques in machine learning has been stochastic gradient descent (SGD), which is a technique allowing for iteratively improving a candidate model using the gradient of the objective function on the data.

In this paper we consider a federated learning setting, i.e., we assume the data is distributed over multiple data owners who want to collaboratively learn a model on the union of their datasets without disclosing their private data.  We assume there is a party, which we call the aggregator, which takes the role of coordinator of this collaborative process.

Making an algorithm differentially private requires that noise is added to each piece of information which can be seen by an adversary.  We consider a classic strategy of federated learning of DNNs where in each iteration an aggregator broadcasts the current model parameters, after which all data owners compute locally the gradients of the objective function for this global model on their private data, securely aggregate these gradients and send the aggregated gradient to the aggregator.

To achieve differential privacy with a minimum amount of noise, it is important to be able to bound precisely the sensitivity of the information which the participants will observe.  In the case of federated SGD, one approach is to bound the sensitivity of the gradient by assuming the objective function is Lipschitz \cite{bassily_differentially_2014}.

Various improvements exist in the case one can make additional assumptions about the objective function.  For example, if the objective function is strongly convex, one can bound the number of iterations needed and in that way avoid to have to distribute the available privacy budget over too many iterations \cite{PaulMangoldShouldHaveSeveralReferences}.


In this paper, we explore an orthogonal direction.  Our main idea is that if anyway all gradients are public and hence also the global model after each iteration of the federated SGD algorithm, we can exploit the public knowledge about the current location of the model parameters in the search space rather than relying on a global bound on the norm of the gradient.  Indeed, at each point in the search space we can compute publicly what is the maximal possible gradient norm we can observe given any dataset.  This gradient norm is often smaller than the worst case gradient norm at the worst case point in the search space.

The contributions of the paper are therefore the following:
\begin{itemize}
\item We show improved privacy guarantees for the SGD setting where the aggregated gradient is the leaked information, which are in worst case equivalent to existing bounds but could in practice allow for using a substantially smaller amount of noise.
\item We present an empirical evaluation, confirming that on a range of popular datasets the amount of noise needed to achieve the same level of differential privacy is lower, and this affects favorably the performance of the trained model.
\item We also observe that for models involving logistic regression sparse models are more likely to be in more favorable points in the search space from a gradient norm point of view.  We therefore consider using a $1$-norm regularizer, which helps improve sparsity of the model.  We confirm empirically this allows for further noise reductions and performance improvements in practice.
\end{itemize}

%* contributions:
%  - propose new algorithm, considering separately the noise in each dimension and exploiting sparsity
%  - Based on this we prove a new bound, allowing for fewer noise
%  - perform experiments

% * Organization of paper ....

The remainder of this paper is organized as follows.  First, we review a number of basic concepts, definitions and notations in Section \ref{sec:prelim}.  Next, we present our new method in Section \ref{sec:method} and present an empirical evaluation in Section \ref{sec:experiment}.  We discuss related work in Section \ref{sec:related}.  Finally, we provide conclusions and directions for future work in Section \ref{sec:concl}.
