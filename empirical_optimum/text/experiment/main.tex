\documentclass{article}
\usepackage{hyperref}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{amsmath}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]
\usepackage[linesnumbered,ruled]{algorithm2e}
\usepackage{enumitem}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{bbm}
\usepackage{bbold}
\usepackage{setspace} % for \onehalfspacing and \singlespacing macros
\onehalfspacing

\usepackage{etoolbox}
\AtBeginEnvironment{quote}{\par\singlespacing\small}

\graphicspath{ {../images/} }

\newcommand{\todo}[1]{

  \textcolor{green}{\textsc{Todo:} {#1}}

}

\newcommand{\janfoot}[1]{{\textcolor{blue}{{\footnote{{\textcolor{blue}{{#1}}}}}}}}

\begin{document}

\title{Differentially private gradient descent with empirically computed sensitivity}
\author{PhD student: Antoine Barczewski\\Supervisor: Jan Ramon}
\date{2022}
\maketitle

\textbf{Abstract.}
Trained machine learning models can leak sensitive information about their underlying training dataset. To qualify this risk, differentially private versions of Empirical Risk Minimization (ERM) have arised to achieve privacy guarantee without underming overall performances. The most popular approach is the DP-SGD which basically adds random pertubations at each update of the gradient. However, these techniques have poor performances  when reasonnable guarantees of differential privacy is achieved. We propose a novel approach to better estimate the scale of the pertubations added which eventually brings better performance and reduces the variance of the resulting optimum.

\input{subfiles/preliminaries}

\section{Related Work}
\subsection{Differentially Private Empirical Risk Minimization}
Differentially Private Empirical Risk Minimization (DP-ERM) has first been studied by adding noise to the result of the optimization program along its objectives \cite{chaudhuri_differentially_nodate}. To provide a gradient descent already private rather than only its result, noise terms are added at each gradient update \cite{bassily_differentially_2014} (DP-SGD) as detailed in algorithm \ref{alg:dp-sgd}. Faster convergence of the latter have been developed \cite{wang_differentially_2017} building up differential privacy on the SVRG algorithm \cite{johnson_accelerating_2013}. Although DP-SGD has turned out to be the standard approach, competitive results have been achieved when challenging the protocol of the gradient descent towards ERM, such as differentially private coordinate-wise updates \cite{mangold_differentially_2022}.

\input{subfiles/dp_sgd}


\section{Noise variance update}
In this section we propose three methods to scale down the variance $\sigma^2$ of the noise $b_t$ from algorithm \ref{alg:dp-sgd}. The first two finds lower $L$-Lipschitz constant estimation at each update of the gradient, while the last one aims at estimating empirically the sensitivity of the gradient.

\subsection{Scale the noise to each $L$-Lipschitz constant of the partial derivatives of the gradient}\label{sec:partial_L}

We define a generalized form of Lipschitz continuity.  In particular, let $L\in\mathbb{R}^p_+$, then we say that a function $f$ is $L$-Lipschitz if its gradient is bounded in every direction $j \in [p]$, i.e.,
\[
  \left| \frac{\partial f}{\partial\theta_j}  (d) \right| \leq L_j
\]

Let us consider again Algorithm \ref{alg:dp-sgd} but assume that $\ell$ is $L$-Lipschitz with $L\in\mathbb{R}^p_+$ rather than $L\in\mathbb{R}_+$, while for the rest the same conditions hold as described in Algorithm \ref{alg:dp-sgd}.
Then, setting $\Sigma=diag(f_n(L_j))$ with $f_n(x) = \frac{32n^2x^2\log(n/\delta)\log(1/\delta)}{\epsilon^2}$, ensures $(\epsilon, \delta)$-differential privacy.

\todo{would ensure $(\epsilon, \delta)$-differential privacy if we can adapt proof from \cite{bassily_differentially_2014}. First, let's adapt the proof from \cite{chaudhuri_differentially_nodate}, on which is based the latter. Basically, we will try to prove privacy guarantees of an ERM with output pertubations with $b$ drawn from the Gaussian distribution $\mathcal{N}(0, \Sigma)$, where $\Sigma=diag\left((\frac{L_i^2 \sqrt{8 \log \frac{2}{\delta}+4 \epsilon}}{\epsilon})_i\right)$.}

\input{subfiles/dp_sgd_output}

\begin{theorem}
  Algorithm \ref{alg:dp-sgd-output} guarantees $(\epsilon, \delta)$-differential privacy.
\end{theorem}

\begin{proof}
If we want to prove that Algorithm \ref{alg:dp-sgd-output} satisfies $(\epsilon, \delta)$-privacy, it suffices to show that for all $\alpha \in \mathbb{R}^p$ the following is true.
$$
e^{-\epsilon}\left(\operatorname{pdf}\left(\theta^{\text {priv }}=\alpha ; \mathcal{D}^{\prime}\right)-\delta\right) \leq \operatorname{pdf}\left(\theta^{\text {priv }}=\alpha ; \mathcal{D}\right) \leq e^\epsilon \operatorname{pdf}\left(\theta^{\text {priv }}=\alpha ; \mathcal{D}^{\prime}\right)+\delta
$$
First consider an $\alpha \in \mathbb{R}^p$. If we have $\theta^{\text {priv}}=\alpha$, then it means that $\alpha=\arg \min _{\theta \in \mathbb{R}^p} n \hat{\mathcal{L}}(\theta ; \mathcal{D})+$ $r(\theta)+\frac{\Delta}{2}\|\theta\|_2^2+b^T \theta$. Setting the gradient of the objective function to zero we get the following.
\begin{equation}\label{eq:mapping}
  b(\alpha ; \mathcal{D})=-(n \nabla \hat{\mathcal{L}}(\alpha ; \mathcal{D})+\nabla r(\alpha)+\Delta \alpha)
\end{equation}

We claim that, given a data set $\mathcal{D}$, $b$ is bijective. Equation \eqref{eq:mapping} shows that, since the objective is strictly convex, for a fixed $b(\alpha ; \mathcal{D})$, there is a unique $\alpha$; therefore the map from $\alpha$ to $b(\alpha ; \mathcal{D})$ is surjective. The equation \eqref{eq:mapping} also shows that for any $\alpha$ there exists a $b(\alpha ; \mathcal{D})$ for which $\alpha$ is the minimizer, so the map from $\alpha$ to $b(\alpha ; \mathcal{D})$ is injective.

To show $(\epsilon,\delta)$-differential privacy, we need to compute the ratio $\mathrm{pdf}_{\mathcal{D}}\left(\theta^{\text {piv }}=\alpha\right) / \operatorname{pdf}_{\mathcal{D}^{\prime}}\left(\theta^{\text {priv}}=\alpha\right)$ of the densities of $\theta^{\text {priv}}$ under the two data sets $\mathcal{D}$ and $\mathcal{D}^{\prime}$. This ratio can be written as
\cite{Bill86} $$\frac{\mathrm{pdf}_{\mathcal{D}}\left(\theta^{\text {piv }}=\alpha\right)}{\operatorname{pdf}_{\mathcal{D}^{\prime}}\left(\theta^{\text {priv}}=\alpha\right)}=\frac{\nu(b(\alpha ; \mathcal{D}) ; \epsilon, \delta, (L_i)_i)}{\nu\left(b\left(\alpha ; \mathcal{D}^{\prime}\right) ; \epsilon, \delta, (L_i)_i\right)} \frac{\left|\operatorname{det}\left(\nabla b\left(\alpha ; \mathcal{D}^{\prime}\right)\right)\right|}{\left|\operatorname{det}(\nabla b(\alpha ; \mathcal{D})) \right|}$$
We bound the ratios of the densities $\nu$ and the determinants separately.

First, we show that for all $\alpha \in \mathbb{R}^p, e^{-\epsilon} \leq \frac{\left|\operatorname{det}\left(\nabla b\left(\alpha ; \mathcal{D}^{\prime}\right)\right)\right|}{|\operatorname{det}(\nabla b(\alpha ; \mathcal{D}))|} \leq e^\epsilon$. The following lemma would be helpful in bounding the ratio.

\begin{lemma}\cite{chaudhuri_differentially_nodate}\label{lem:det}
  If $A$ is a full-rank matrix and if $E$ is matrix with rank at most 2, then,
  $$
  \frac{\operatorname{det}(A+E)-\operatorname{det}(A)}{\operatorname{det}(A)}=\lambda_1\left(A^{-1} E\right)+\lambda_2\left(A^{-1} E\right)+\lambda_1\left(A^{-1} E\right) \lambda_2\left(A^{-1} E\right)
  $$
  where $\lambda_i(Z)$ is the $i$-th highest eigenvalue of matrix $Z$.
\end{lemma}
Let $\nabla b(\alpha ; \mathcal{D})=-\left(n \nabla^2 \hat{\mathcal{L}}(\alpha ; \mathcal{D})+\nabla^2 r(\alpha)+\Delta \mathbb{I}_p\right)=-A$, where $\mathbb{I}_p$ is an identity matrix of $p \times p$ dimensions. W.l.o.g. assume that $\mathcal{D}^{\prime}$ has one entry more as compared to $\mathcal{D}$, and $\mathcal{D}$ has $n$ entries. Let $E=\nabla^2 \ell\left(\alpha ; d_{n+1}\right)$. Therefore, $\left|\operatorname{det}\left(\nabla b\left(\alpha ; \mathcal{D}^{\prime}\right)\right)\right|=\operatorname{det}(A+E)$. Since $n \nabla^2 \hat{\mathcal{L}}(\alpha ; \mathcal{D})+\nabla^2 r(\alpha)$ is positive semi-definite (as both $\hat{\mathcal{L}}$ and $r$ are convex), the smallest eigenvalue of $A$ is $\Delta$. Since $E=\nabla^2 \ell\left(\alpha ; d_{n+1}\right)=\ell^{\prime\prime}\left(\alpha ; d_{n+1}\right)x_{n+1}x_{n+1}^\top$, $E$ is a positive semi-definite matrix of rank at most one and $A^{-1} E$ has at most one non-zero eigenvalue. Additionally, it follows that $\lambda_1\left(A^{-1} E\right) \leq \frac{\lambda_1(E)}{\Delta}$. Applying Lemma \ref{lem:det} , we have $\frac{\operatorname{det}(A+E)}{\operatorname{det}(A)} \leq 1+\frac{\psi}{\Delta}$, since $\lambda_1(E) \leq \psi$ by assumption. Replacing the value of $\Delta$ we get $\frac{\left|\operatorname{det}\left(\nabla b\left(\alpha ; \mathcal{D}^{\prime}\right)\right)\right|}{|\operatorname{det}(\nabla b(\alpha ; \mathcal{D}))|} \leq e^{\frac{\epsilon}{2}}$.

To bound $\frac{\nu(b(\alpha ; \mathcal{D}) ; \epsilon, \delta, (L_i)_i)}{\nu\left(b\left(\alpha ; \mathcal{D}^{\prime}\right) ; \epsilon, \delta, (L_i)_i\right)}$, recall that the noise vector $b$ is drawn from the Gaussian distribution $\mathcal{N}(0, \Sigma)$, where $\Sigma=diag\left((\frac{L_i \sqrt{8 \log \frac{2}{\delta}+4 \epsilon}}{\epsilon})_i\right)$. Let us assume $\Gamma=b(\alpha ; \mathcal{D})-$ $b\left(\alpha ; \mathcal{D}^{\prime}\right)$. With this we have the following:
$$
\begin{aligned}
&\frac{\nu(b(\alpha ; \mathcal{D}) ; \epsilon, \delta, (L_i)_i)}{\nu\left(b\left(\alpha ; \mathcal{D}^{\prime}\right) ; \epsilon, \delta, (L_i)_i\right)}=\frac{e^{-\frac{b(\alpha ; \mathcal{D})^\top \Sigma^{-1}b(\alpha ; \mathcal{D})}{2}}}{e^{-\frac{b(\alpha ; \mathcal{D^\prime})^\top \Sigma^{-1}b(\alpha ; \mathcal{D^\prime})}{2}}} \\
&=e^{\frac{1}{2}\left|b(\alpha ; \mathcal{D})^\top \Sigma^{-1}b(\alpha ; \mathcal{D})-b(\alpha ; \mathcal{D^\prime})^\top \Sigma^{-1}b(\alpha ; \mathcal{D^\prime})\right|} \\
&=e^{\frac{1}{2}\left|b(\alpha ; \mathcal{D})^\top \Sigma^{-1}b(\alpha ; \mathcal{D})-(b(\alpha ; \mathcal{D})-\Gamma)^\top \Sigma^{-1}(b(\alpha ; \mathcal{D})-\Gamma)\right|} \\
&=e^{\frac{1}{2}\left|2b(\alpha ; \mathcal{D})^\top \Sigma^{-1}\Gamma-\Gamma^\top\Sigma^{-1}\Gamma\right|}
\end{aligned}
$$

Since $\forall i \in [p], |\nabla \ell(\theta ;)_i| \leq L_i$ for all $\theta \in \mathbb{R}^p$ and for all $d \in \mathcal{T}$, therefore $|\Gamma_i| \leq L_i$. Hence the following is true.
\begin{equation}\label{eq:leq_exp}
  e^{\frac{1}{2}\left|2b(\alpha ; \mathcal{D})^\top \Sigma^{-1}\Gamma-\Gamma^\top\Sigma^{-1}\Gamma\right|}\leq e^{\frac{1}{2}\left|2b(\alpha ; \mathcal{D})^\top \Sigma^{-1}\Gamma+\Gamma^\top\Sigma^{-1}\Gamma\right|} \leq e^{\frac{1}{2}\left|2b(\alpha ; \mathcal{D})^\top \Sigma^{-1}\Gamma+L^\top\Sigma^{-1}L\right|}
\end{equation}

The following two lemmas will be useful in bounding $|b(\alpha ; \mathcal{D})^\top \Sigma^{-1}\Gamma|$. Both of them follow from basic probability theory and hence we skip their proofs.
\begin{lemma}\label{lem:dist_dot}
  Let $Z \sim \mathcal{N}\left(0, \Sigma\right)$ and $v \in \mathbb{R}^p$ be a fixed vector. Then
  $$
  \langle Z, v\rangle \sim \mathcal{N}\left(0,v^\top\Sigma v\right)
  $$
\end{lemma}

Note that $\Gamma$ is independent of the noise vector. Therefore using Lemma \ref{lem:dist_dot}, we get $b(\alpha ; \mathcal{D})^\top \Sigma^{-1}\Gamma \sim \mathcal{N}\left(0,\Gamma^\top\Sigma^{-1}\Gamma\right)$. The following lemma provides a tail bound for normal distribution which we use to bound the probability that the noise vector $b(\alpha ; \mathcal{D})$ is not in the set GOOD.
\begin{lemma}
  Let $Z \sim \mathcal{N}(0,1)$, then for all $t>1$, we have
  $$
  \operatorname{Pr}[|Z|>t] \leq e^{-t^2 / 2}
  $$
\end{lemma}
Using this lemma and the fact that $|\Gamma_i| \leq L_i$, we get $\operatorname{Pr}[|b(\alpha ; \mathcal{D})^\top \Sigma^{-1}\Gamma| \geq \sqrt{L^\top\Sigma^{-1}L} t] \leq e^{-\frac{t^2}{2}}$, where $t>1$.\\ Let GOOD be the set $\left\{a \in \mathbb{R}^p|a\Sigma^{-1}\Gamma| \geq \sqrt{L^\top\Sigma^{-1}L} t\right\}$. We want the noise vector $b(\alpha ; \mathcal{D})$ to be in the set GOOD w.p. at least $1-\delta$. Setting $t=\sqrt{2 \log \frac{2}{\delta}}$ implies that $2 e^{-\frac{t^2}{2}}=\delta$. To make sure $t \geq 1$, we need to have $\delta \leq \frac{2}{\sqrt{e}}$. This always true for any non trivial $\delta$. Replacing $t=\sqrt{2 \log \frac{2}{\delta}}$ in $\sqrt{L^\top\Sigma^{-1}L}  t$, we get from Equation \ref{eq:leq_exp} that $\frac{\nu(b(\alpha ; \mathcal{D}) ; \epsilon, \delta (L_i)_i)}{\nu\left(b\left(\alpha ; \mathcal{D}^{\prime}\right) ; \epsilon, \delta, (L_i)_i\right)} \leq e^{\frac{1}{2}\left(\sqrt{L^\top\Sigma^{-1}L} \sqrt{8 \log \frac{2}{\delta}}+L^\top\Sigma^{-1}L\right)}$. Solving for $\Sigma$ we get $\Sigma_{i,i} \geq \frac{L_i \sqrt{8 \log \frac{2}{\delta}+4 \epsilon}}{\epsilon}$. To complete the argument, we show the following:
$$
\begin{aligned}
&\operatorname{pdf}\left(\theta^{\text {priv }}=\alpha ; \mathcal{D}\right)=\operatorname{Pr}[b \in \mathrm{GOOD}] \operatorname{pdf}\left(\theta^{\text {priv }}=\alpha \mid b \in \mathrm{GOOD} ; \mathcal{D}\right) \\
&+\operatorname{Pr}[b \in \overline{\mathrm{GOOD}}] \operatorname{pdf}\left(\theta^{\text {priv }}=\alpha \mid b \in \overline{\mathrm{GOOD}} ; \mathcal{D}\right) \\
&\leq \operatorname{Pr}[b \in \mathrm{GOOD}] \operatorname{pdf}\left(\theta^{\text {priv }}=\alpha \mid b \in \mathrm{GOOD} ; \mathcal{D}\right)+\delta \\
&\leq e^\epsilon \operatorname{Pr}[b \in \mathrm{GOOD}] \operatorname{pdf}\left(\theta^{\text {priv }}=\alpha \mid b \in \mathrm{GOOD} ; \mathcal{D}^{\prime}\right)+\delta \\
&\leq e^\epsilon \operatorname{pdf}\left(\theta^{\text {priv }}=\alpha ; \mathcal{D}^{\prime}\right)+\delta
\end{aligned}
$$
where $b$ is the noise vector in Algorithm. This concludes the proof.
\end{proof}

\subsection{Updating the noise variance at each update of the gradient of a logistic regression}

Let $\ell$ be the loss of the Logistic Regression, then $\ell(d) = \ell((x,y)) = -y\log S(x;\theta) -(1-y)\log(1-S(x;\theta))$ with $S$ the sigmoid function and $y\in\{0,1\}$.

\input{subfiles/dp_sgd_logloss}

\begin{theorem}\label{th:dp_logloss}
  (Privacy guarantee). Algorithm \ref{alg:dp-sgd-logloss} is $(\epsilon, \delta)$-differentially private.
\end{theorem}

\begin{proof}
  Since $S^\prime(x) = S(x)(1-S(x))$, first order derivation of the LogLoss $\ell$ gives
  \begin{align*}
    \nabla_\theta\ell(d)
    & = -y\frac{S(x;\theta)(1-S(x;\theta))}{S(x;\theta)}x + (1-y)\frac{S(x;\theta)(1-S(x;\theta))}{(1-S(x;\theta))}x \\
    & = -y(1-S(x;\theta))x + (1-y)S(x;\theta)x \\
    & = \left(S(x;\theta) - y\right)x
  \end{align*}
  So, for all $j \in [p]$,
  \begin{align*}
    \frac{\partial\ell}{\partial\theta_j}(d)
    = \left(S(x;\theta) - y\right)x_j \\
  \end{align*}
  Since $x \in [-1, 1]^p$, $S(x;\theta)$ is maximal when $\forall i \in [p], x_i = \text{sign}(\theta_i)$, where $\operatorname{sign}(x)=1$ if $x>0, \operatorname{sign}(x)=-1$ if $x<$ 0 , and $\operatorname{sign}(x)=0$ if $x=0$. For the same reason $S(x;\theta) - 1$ is maximal when $\forall i \in [p], x_i = -\text{sign}(\theta_i)$. As $S(x;\theta) - 1 = -S(-x;\theta)$, all in all we get

  \begin{equation*}
    \left|\frac{\partial\ell}{\partial\theta_j}(d)\right| \leq
    \frac{|\operatorname{sign}(\theta_j)|}{1+e^{-\|\theta\|_1}}
  \end{equation*}
  \begin{equation}\label{eq:leq_logloss_partial}
    \left|\frac{\partial\ell}{\partial\theta_j}(d)\right| \leq
    \frac{\mathbb{1}_{x \neq 0}(\theta_j)}{1+e^{-\|\theta\|_1}}
  \end{equation}
  At each update of $\theta$, one can use \eqref{eq:leq_logloss_partial} to find an upper-bound of all partial derivatives of the loss.  The rest of the proof follows section \ref{sec:partial_L} with $L_j$ estimated thanks to this upper-bound.
\end{proof}

[Utility proof needs to be done]


\subsubsection{DP-SGD on logistic regression with $1$-norm regularizer}
Let $\mathcal{L}$ be the loss of the Logistic Regression with $1$-norm regularizer, then $$\mathcal{L}(d) = \ell((x,y)) + \frac{C}{n} \|\theta\|_1 = -y\log S(x;\theta) -(1-y)\log(1-S(x;\theta)) + \frac{C}{n} \|\theta\|_1$$
with $S$ the sigmoid function, $y\in\{0,1\}$ and $C$ is the hyper-parameter that controls the degree of regularization. Considering the subgradient at zero, we can use the following update equation for all $j \in [p]$:
\begin{equation}\label{eq:l1_update}
  \theta_j^{t+1}=\theta_j^t-\eta(t) \frac{\partial\ell}{\partial\theta_j}(d)-\frac{C}{N} \eta(t) \operatorname{sign}\left(\theta_j^t\right)
\end{equation}
where $\operatorname{sign}(x)=1$ if $x>0, \operatorname{sign}(x)=-1$ if $x<$ 0 , and $\operatorname{sign}(x)=0$ if $x=0$. However, unless a weight falls exactly on zero, this method doesn't produce a compact model i.e. weights don't become zero as the result of the training.

\cite{tsuruoka_stochastic_2009} describes an efficient and widely used implementation of the SGD with $1$-norm regularizer that actually produces a compact model. In a nutshell, first, the method clips to zero any weight that changes sign when updated (not to be confused with the clipping which bounds the norm of gradients). Doing so, we can expect a lot of weights to become zero during training (\cite{Carpenter2008LazySS})
% Todo: note that it results in biaised classifier
% Todo: note that the ordering of the visiting of the samples can in some extreme cases deter from
% todo: folder with pointers from paul + update related work
. Second, the penalty, i.e., the last term on the right hand-side of the update equation \eqref{eq:l1_update},  is accumulated at each timestep so one update cannot bring too much variance of weights over steps, e.g., one weight becoming non-zero only due to the gradient of last sample gradient picked for training.

Let $u_t$ be the absolute value of the total accumulated penalty up to the $t^{th}$ timestep. Since the absolute value of the $1$-norm regularizer in \eqref{eq:l1_update} does not depend on the weight and we are using the same regularization constant $C$ for all weights, it is simply accumulated as:
$$
u_t=\frac{C}{n} \sum_{k=1}^t \eta (k)
$$
At each training sample, we update the weights as follows:
\begin{equation}\label{eq:l1_half_update}
\theta_j^{t+\frac{1}{2}}=\theta_j^t-\eta(t) \frac{\partial \ell}{\partial \theta_j}(d_t)
\end{equation}
if $\theta_j^{t+\frac{1}{2}}>0$ then
$$
\theta_j^{t+1}=\max \left(0, \theta_j^{t+\frac{1}{2}}-\left(u_t+q_j^{t-1}\right)\right),
$$
else if $\theta_j^{t+\frac{1}{2}}<0$ then
$$
\theta_j^{t+1}=\min \left(0, \theta_j^{t+\frac{1}{2}}+\left(u_t-q_j^{t-1}\right)\right),
$$
where $q_j^t$ is the total $L_1$-penalty that $\theta_j$ has actually received up to the point:
$$
q_j^t=\sum_{k=1}^t\left(\theta_j^{k+1}-\theta_j^{k+\frac{1}{2}}\right)
$$

To get a differentially private version, noise has to be added at \eqref{eq:l1_half_update}. So all in all, we end up with algorithm \ref{alg:dp-sgd-logloss-l1}.

\input{subfiles/dp_sgd_logloss_l1}

\begin{theorem}\label{th:dp_logloss_l1}
  (Privacy guarantee). Algorithm \ref{alg:dp-sgd-logloss-l1} is $(\epsilon, \delta)$-differentially private.
\end{theorem}

\begin{proof}
  As post-processing does not impact differential privacy level \cite{dwork_algorithmic_2013}, \ref{th:dp_logloss} proves algorithm \ref{alg:dp-sgd-logloss-l1} is $(\epsilon, \delta)$-differentially private.
\end{proof}


\subsection{Multivariate noise with moment accountant}

Moment accountant in DP-SGD have been introduced by Abadi and al. \cite{abadi_deep_2016}. The proof of the $(\epsilon, \delta)$-differential privacy relies on the following moments bound on Gaussian mechanism with random sampling.

\begin{lemma}
  Suppose that $f: D \rightarrow \mathbb{R}^p$ with $|f(\cdot)_i| \leq C_i$, with $i \in [p]$. Let $\sigma \geq 1$ and let $J$ be a sample from $[n]$ where each $i \in[n]$ is chosen independently with probability $q<\frac{1}{16 \sigma}$. Then for any positive integer $\lambda \leq \sigma^2 \ln \frac{1}{q \sigma}$, the mechanism $\mathcal{M}(d)=$ $\sum_{i \in J} f\left(d_i\right)+\mathcal{N}\left(0, \sigma^2 \operatorname{diag}((C_i)_{i\in[p]}\right)$ satisfies
  $$
  \alpha_{\mathcal{M}}(\lambda) \leq \frac{q^2 \lambda(\lambda+1)}{(1-q) \sigma^2}+O\left(q^3 \lambda^3 / \sigma^3\right) .
  $$
\end{lemma}

The proof is exactly the same as \cite{abadi_deep_2016}.
\begin{proof}
Fix $d^{\prime}$ and let $d=d^{\prime} \cup\left\{d_n\right\}$. Without loss of generality, $f\left(d_n\right)=\mathbf{e}_1$ and $\sum_{i \in J \backslash[n]} f\left(d_i\right)=\mathbf{0}$. Thus $\mathcal{M}(d)$ and $\mathcal{M}\left(d^{\prime}\right)$ are distributed identically except for the first coordinate and hence we have a one-dimensional problem. Let $\mu_0$ denote the pdf of $\mathcal{N}\left(0, \sigma^2\right)$ and let $\mu_1$ denote the pdf of $\mathcal{N}\left(1, \sigma^2\right)$, we assume that $C_1 = 1 $. Thus:
$$
\begin{aligned}
\mathcal{M}\left(d^{\prime}\right) & \sim \mu_0 \\
\mathcal{M}(d) & \sim \mu \triangleq(1-q) \mu_0+q \mu_1
\end{aligned}
$$
We want to show that
$$
\begin{aligned}
\mathbb{E}_{z \sim \mu}\left[\left(\mu(z) / \mu_0(z)\right)^\lambda\right] & \leq \alpha, \\
\text { and } \mathbb{E}_{z \sim \mu_0}\left[\left(\mu_0(z) / \mu(z)\right)^\lambda\right] & \leq \alpha,
\end{aligned}
$$
for some explicit $\alpha$ to be determined later.
We will use the same method to prove both bounds. Assume we have two distributions $\nu_0$ and $\nu_1$, and we wish to bound
$$
\mathbb{E}_{z \sim \nu_0}\left[\left(\nu_0(z) / \nu_1(z)\right)^\lambda\right]=\mathbb{E}_{z \sim \nu_1}\left[\left(\nu_0(z) / \nu_1(z)\right)^{\lambda+1}\right]
$$

Using binomial expansion, we have
$$
\begin{aligned}
& \mathbb{E}_{z \sim \nu_1}\left[\left(\nu_0(z) / \nu_1(z)\right)^{\lambda+1}\right] \\
= & \mathbb{E}_{z \sim \nu_1}\left[\left(1+\left(\nu_0(z)-\nu_1(z)\right) / \nu_1(z)\right)^{\lambda+1}\right] \\
= & \mathbb{E}_{z \sim \nu_1}\left[\left(1+\left(\nu_0(z)-\nu_1(z)\right) / \nu_1(z)\right)^{\lambda+1}\right] \\
= & \sum_{t=0}^{\lambda+1}\left(\begin{array}{c}
\lambda+1 \\
t
\end{array}\right) \mathbb{E}_{z \sim \nu_1}\left[\left(\left(\nu_0(z)-\nu_1(z)\right) / \nu_1(z)\right)^t\right] .
\end{aligned}
$$
The first term in (5) is 1 , and the second term is
$$
\begin{aligned}
\mathbb{E}_{z \sim \nu_1}\left[\frac{\nu_0(z)-\nu_1(z)}{\nu_1(z)}\right] & =\int_{-\infty}^{\infty} \nu_1(z) \frac{\nu_0(z)-\nu_1(z)}{\nu_1(z)} \mathrm{d} z \\
& =\int_{-\infty}^{\infty} \nu_0(z) \mathrm{d} z-\int_{-\infty}^{\infty} \nu_1(z) \mathrm{d} z \\
& =1-1=0 .
\end{aligned}
$$
To prove the lemma it suffices to show show that for both $\nu_0=\mu, \nu_1=\mu_0$ and $\nu_0=\mu_0, \nu_1=\mu$, the third term is bounded by $q^2 \lambda(\lambda+1) /(1-q) \sigma^2$ and that this bound dominates the sum of the remaining terms. We will prove the more difficult second case $\left(\nu_0=\mu_0, \nu_1=\mu\right)$; the proof of the other case is similar.

To upper bound the third term in $(\$ 5)$, we note that $\mu(z) \geq$ $(1-q) \mu_0(z)$, and write
$$
\begin{aligned}
\mathbb{E}_{z \sim \mu} & {\left[\left(\frac{\mu_0(z)-\mu(z)}{\mu(z)}\right)^2\right] } \\
& =q^2 \mathbb{E}_{z \sim \mu}\left[\left(\frac{\mu_0(z)-\mu_1(z)}{\mu(z)}\right)^2\right] \\
& =q^2 \int_{-\infty}^{\infty} \frac{\left(\mu_0(z)-\mu_1(z)\right)^2}{\mu(z)} \mathrm{d} z \\
& \leq \frac{q^2}{1-q} \int_{-\infty}^{\infty} \frac{\left(\mu_0(z)-\mu_1(z)\right)^2}{\mu_0(z)} \mathrm{d} z \\
& =\frac{q^2}{1-q} \mathbb{E}_{z \sim \mu_0}\left[\left(\frac{\mu_0(z)-\mu_1(z)}{\mu_0(z)}\right)^2\right] .
\end{aligned}
$$

An easy fact is that for any $a \in \mathbb{R}, \mathbb{E}_{z \sim \mu_0} \exp \left(2 a z / 2 \sigma^2\right)=$ $\exp \left(a^2 / 2 \sigma^2\right)$. Thus,
$$
\begin{aligned}
\mathbb{E}_{z \sim \mu_0} & {\left[\left(\frac{\mu_0(z)-\mu_1(z)}{\mu_0(z)}\right)^2\right] } \\
= & \mathbb{E}_{z \sim \mu_0}\left[\left(1-\exp \left(\frac{2 z-1}{2 \sigma^2}\right)\right)^2\right] \\
= & 1-2 \mathbb{E}_{z \sim \mu_0}\left[\exp \left(\frac{2 z-1}{2 \sigma^2}\right)\right] \\
& +\mathbb{E}_{z \sim \mu_0}\left[\exp \left(\frac{4 z-2}{2 \sigma^2}\right)\right] \\
= & 1-2 \exp \left(\frac{1}{2 \sigma^2}\right) \cdot \exp \left(\frac{-1}{2 \sigma^2}\right) \\
& +\exp \left(\frac{4}{2 \sigma^2}\right) \cdot \exp \left(\frac{-2}{2 \sigma^2}\right) \\
= & \exp \left(1 / \sigma^2\right)-1 .
\end{aligned}
$$
Thus the third term in the binomial expansion (\$)
$$
\left(\begin{array}{c}
1+\lambda \\
2
\end{array}\right) \mathbb{E}_{z \in \mu}\left[\left(\frac{\mu_0(z)-\mu(z)}{\mu(z)}\right)^2\right] \leq \frac{\lambda(\lambda+1) q^2}{(1-q) \sigma^2} .
$$
To bound the remaining terms, we first note that by standard calculus, we get:
$$
\begin{aligned}
& \forall z \leq 0:\left|\mu_0(z)-\mu_1(z)\right| \leq-(z-1) \mu_0(z) / \sigma^2, \\
& \forall z \geq 1:\left|\mu_0(z)-\mu_1(z)\right| \leq z \mu_1(z) / \sigma^2, \\
& \forall 0 \leq z \leq 1:\left|\mu_0(z)-\mu_1(z)\right| \leq \mu_0(z)\left(\exp \left(1 / 2 \sigma^2\right)-1\right) \\
& \leq \mu_0(z) / \sigma^2 . \\
&
\end{aligned}
$$
We can then write
$$
\begin{aligned}
\mathbb{E}_{z \sim \mu} & {\left[\left(\frac{\mu_0(z)-\mu(z)}{\mu(z)}\right)^t\right] } \\
\leq & \int_{-\infty}^0 \mu(z)\left|\left(\frac{\mu_0(z)-\mu(z)}{\mu(z)}\right)^t\right| \mathrm{d} z \\
& +\int_0^1 \mu(z)\left|\left(\frac{\mu_0(z)-\mu(z)}{\mu(z)}\right)^t\right| \mathrm{d} z \\
& +\int_1^{\infty} \mu(z)\left|\left(\frac{\mu_0(z)-\mu(z)}{\mu(z)}\right)^t\right| \mathrm{d} z .
\end{aligned}
$$
We consider these terms individually. We repeatedly make use of three observations: (1) $\mu_0-\mu=q\left(\mu_0-\mu_1\right)$, (2) $\mu \geq(1-q) \mu_0$, and (3) $\mathbb{E}_{\mu_0}\left[|z|^t\right] \leq \sigma^t(t-1) !$ !. The first term can then be bounded by
$$
\begin{aligned}
\frac{q^t}{(1-q)^{t-1} \sigma^{2 t}} & \int_{-\infty}^0 \mu_0(z)|z-1|^t \mathrm{~d} z \\
& \leq \frac{(2 q)^t(t-1) ! !}{2(1-q)^{t-1} \sigma^t} .
\end{aligned}
$$
The second term is at most
$$
\begin{aligned}
\frac{q^t}{(1-q)^t} \int_0^1 \mu(z) \mid & \left|\left(\frac{\mu_0(z)-\mu_1(z)}{\mu_0(z)}\right)^t\right| \mathrm{d} z \\
& \leq \frac{q^t}{(1-q)^t} \int_0^1 \mu(z) \frac{1}{\sigma^{2 t}} \mathrm{~d} z \\
& \leq \frac{q^t}{(1-q)^t \sigma^{2 t}} .
\end{aligned}
$$
Similarly, the third term is at most
$$
\begin{aligned}
& \frac{q^t}{(1-q)^{t-1} \sigma^{2 t}} \int_1^{\infty} \mu_0(z)\left(\frac{z \mu_1(z)}{\mu_0(z)}\right)^t \mathrm{~d} z \\
& \leq \frac{q^t}{(1-q)^{t-1} \sigma^{2 t}} \int_1^{\infty} \mu_0(z) \exp \left((2 t z-t) / 2 \sigma^2\right) z^t \mathrm{~d} z \\
& \leq \frac{q^t \exp \left(\left(t^2-t\right) / 2 \sigma^2\right)}{(1-q)^{t-1} \sigma^{2 t}} \int_0^{\infty} \mu_0(z-t) z^t \mathrm{~d} z \\
& \leq \frac{(2 q)^t \exp \left(\left(t^2-t\right) / 2 \sigma^2\right)\left(\sigma^t(t-1) ! !+t^t\right)}{2(1-q)^{t-1} \sigma^{2 t}}
\end{aligned}
$$
Under the assumptions on $q, \sigma$, and $\lambda$, it is easy to check that the three terms, and their sum, drop off geometrically fast in $t$ for $t>3$. Hence the binomial expansion (5) is dominated by the $t=3$ term, which is $O\left(q^3 \lambda^3 / \sigma^3\right)$. The claim follows.
\end{proof}


\bibliographystyle{IEEEtran}
\bibliography{../biblio}
\end{document}
