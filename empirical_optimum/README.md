# Empirical optimum

## Objective
This repo explores and experiments over finding the optimum covariance matrix empirically.

## Pending questions
* when adding the new vector, what should be the corresponding target value?-> random or prediction from algo
* should the fake vectors unique value be drawn from real distribution? -> after preprocessing
* sensitivity should be computed for all pairs of dataset, here we compute it only against the true dataset, is it ok? -> the max of the norm of the resulting gradients
* can't we calibrate the added noise to the output covariance instead of the sensitivity on each dimension? -> since feature should be independant then there's no need

## Todo
* use parameters from paper for tf dp lr on mnist
* add noise to each lot in emp_opt (new implementation)
* compare results with different number of features (PCA)


* how to apply the privacy budget?
-> observe relationship eigenvalue decomposition of the cov matrix and iid noise method (if ortho then similar to iid noise?)

-> go to large datasets
-> go to narrow datasets
-> compare to baseline dp logistic regression as describe in literature
-> compare the variance of the noise that is added to the final model (we hope the classical method has higher variance)
-> compare variance only if tensorflow method isn't based sgd but plain gradient descent (otherwise comparision isn't fair)


if we learn on each
D U 0, .., 0 (0)
D U 1, 0, ..
D U 0, 1, 0, ..
D U 1, 1, 0, ..
we end up with vector of coefs
(0) - (1) + ((0) - (2)) == (0) - (3)
