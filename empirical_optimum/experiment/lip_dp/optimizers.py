import torch
from torch.nn.functional import normalize
from torch.nn.utils import clip_grad_norm_
from typing import Callable, List, Optional, Union
from opacus.optimizers import DPOptimizer
from opt_einsum.contract import contract
import numpy as np


def _mark_as_processed(obj: Union[torch.Tensor, List[torch.Tensor]]):
    """
    Marks parameters that have already been used in the optimizer step.

    DP-SGD puts certain restrictions on how gradients can be accumulated.
    In particular, no gradient can be used twice - client must
    call .zero_grad() between optimizer steps, otherwise privacy
    guarantees are compromised.
    This method marks tensors that have already been used in optimizer steps
    to then check if zero_grad has been duly called.

    Notes:
          This is used to only mark ``p.grad_sample`` and ``p.summed_grad``

    Args:
        obj: tensor or a list of tensors to be marked
    """

    if isinstance(obj, torch.Tensor):
        obj._processed = True
    elif isinstance(obj, list):
        for x in obj:
            x._processed = True


def _unmark_as_processed(obj: Union[torch.Tensor, List[torch.Tensor]]):

    if isinstance(obj, torch.Tensor):
        if hasattr(obj, "_processed"):
            delattr(obj, "_processed")
    elif isinstance(obj, list):
        for x in obj:
            if hasattr(x, "_processed"):
                delattr(x, "_processed")


def _check_processed_flag_tensor(x: torch.Tensor):
    """
    Checks if this gradient tensor has been used in optimization step.

    See Also:
        :meth:`~opacus.optimizers.optimizer._mark_as_processed`

    Args:
        x: gradient tensor

    Raises:
        ValueError
            If tensor has attribute ``._processed`` previously set by
            ``_mark_as_processed`` method
    """

    if hasattr(x, "_processed"):
        raise ValueError(
            "Gradients haven't been cleared since the last optimizer step. "
            "to obtain privacy guarantees you must call optimizer.zero_grad()"
            "on each step"
        )


def _check_processed_flag(obj: Union[torch.Tensor, List[torch.Tensor]]):
    """
    Checks if this gradient tensor (or a list of tensors) has been previously
    used in optimization step.

    See Also:
        :meth:`~opacus.optimizers.optimizer._mark_as_processed`

    Args:
        x: gradient tensor or a list of tensors

    Raises:
        ValueError
            If tensor (or at least one tensor from the list) has attribute
            ``._processed`` previously set by ``_mark_as_processed`` method
    """

    if isinstance(obj, torch.Tensor):
        _check_processed_flag_tensor(obj)
    elif isinstance(obj, list):
        for x in obj:
            _check_processed_flag_tensor(x)


def _generate_noise(
    std: float,
    reference: torch.Tensor,
    generator=None,
    secure_mode: bool = False,
) -> torch.Tensor:
    """
    Generates noise according to a Gaussian distribution with mean 0

    Args:
        std: Standard deviation of the noise
        reference: The reference Tensor to get the appropriate shape and device
            for generating the noise
        generator: The PyTorch noise generator
        secure_mode: boolean showing if "secure" noise need to be generated
            (see the notes)
    """
    zeros = torch.zeros(reference.shape, device=reference.device)
    if std == 0:
        return zeros
    if secure_mode:
        torch.normal(
            mean=0,
            std=std,
            size=(1, 1),
            device=reference.device,
            generator=generator,
        )  # generate, but throw away first generated Gaussian sample
        sum = zeros
        for _ in range(4):
            sum += torch.normal(
                mean=0,
                std=std,
                size=reference.shape,
                device=reference.device,
                generator=generator,
            )
        return sum / 2
    else:
        return torch.normal(
            mean=0,
            std=std,
            size=reference.shape,
            device=reference.device,
            generator=generator,
        )


def reshape_weight_to_matrix(weight: torch.Tensor) -> torch.Tensor:
    weight_mat = weight
    height = weight_mat.size(0)
    return weight_mat.reshape(height, -1)


def spectral_norm(weight,
                  max_weight_norm,
                  fix_sigma,
                  n_power_iterations=5,
                  do_power_iteration=True,
                  eps=1e-10):
    with torch.no_grad():
        weight_mat = reshape_weight_to_matrix(weight)

        h, w = weight_mat.size()
        # randomly initialize `u` and `v`
        u = normalize(weight.new_empty(h).normal_(0, 1), dim=0, eps=eps)
        v = normalize(weight.new_empty(w).normal_(0, 1), dim=0, eps=eps)

    if do_power_iteration:
        with torch.no_grad():
            for _ in range(n_power_iterations):
                v = normalize(torch.mv(weight_mat.t(), u), dim=0, eps=eps,
                              out=v)
                u = normalize(torch.mv(weight_mat, v), dim=0, eps=eps, out=u)
            if n_power_iterations > 0:
                # See above on why we need to clone
                u = u.clone(memory_format=torch.contiguous_format)
                v = v.clone(memory_format=torch.contiguous_format)

    sigma = torch.dot(u, torch.mv(weight_mat, v)).item()
    if fix_sigma or sigma > max_weight_norm:
        weight = weight / sigma * max_weight_norm
        sigma = max_weight_norm
    return weight, sigma


def vector_norm(weight,
                max_weight_norm,
                fix_sigma):
    with torch.no_grad():
        sigma = torch.linalg.vector_norm(weight).item()
        if fix_sigma or sigma > max_weight_norm:
            weight = weight / sigma * max_weight_norm
            sigma = max_weight_norm
    return weight, sigma


def get_loss_lip(criterion, temperature=1.):
    loss_name = str(criterion).lower()
    if 'crossentropy' in loss_name:
        return (np.sqrt(2) / temperature)
    elif 'multimargin' in loss_name:
        return 1
    else:
        raise ValueError('Loss not implemented or not Lipschitz')


def get_lip_theta(x_norm_curr, param_shape):
    # affine layer
    if len(param_shape) == 3:  # to check if batch_size in shape
        return x_norm_curr
    else:
        return param_shape[-1] * x_norm_curr


class LipDPOptimizer(DPOptimizer):
    """docstring for LipDPOptimizer."""

    def __init__(self, max_weight_norm, fix_sigma, named_parameters,
                 *args, **kwargs):
        super(LipDPOptimizer, self).__init__(*args, **kwargs)
        self.max_weight_norm = max_weight_norm
        self.fix_sigma = fix_sigma
        self.named_parameters = named_parameters
        # self.register_step_post_hook(project)

    def accumulate(self):
        """
        Aggregated gradients into `p.summed_grad```
        """
        for p in self.params:
            _check_processed_flag(p.grad_sample)
            grad_sample = self._get_flat_grad_sample(p).sum(0)
            if p.summed_grad is not None:
                p.summed_grad += grad_sample
            else:
                p.summed_grad = grad_sample

            _mark_as_processed(p.grad_sample)

    def pre_step(
        self, closure: Optional[Callable[[], float]] = None
    ) -> Optional[float]:
        """
        Perform actions specific to ``DPOptimizer`` before calling
        underlying  ``optimizer.step()``

        Args:
            closure: A closure that reevaluates the model and
                returns the loss. Optional for most optimizers.
        """
        self.accumulate()
        if self._check_skip_next_step():
            self._is_last_step_skipped = True
            return False

        self.add_noise()
        self.scale_grad()

        if self.step_hook:
            self.step_hook(self)

        self._is_last_step_skipped = False
        return True

    def project(self, input_norm, operators_lip, loss_lip):
        """
        Compute layer sensitivity and clip weights.

        params :
            input_norm (float): Maximum norm of the input norm X_1
            loss_lip (float): Lipschitz value of the loss
        """
        lip_theta = []  # L_{\theta_k}
        x_norm_curr = input_norm  # X_1
        lip_x = []  # L_{x_k}
        # forward pass
        # iteration over parameters grouped by different optimizer (here one)
        for group in self.param_groups:
            # group parameters in layers (weights and bias)
            layers = zip(*(iter(group["params"]),) * 2)
            layers_name = zip(*(iter(self.named_parameters),) * 2)
            # iteration over all trainable layers
            for layer, layer_name in zip(layers, layers_name):
                # iteration over parameters of the layer
                for p, name in zip(layer, layer_name):
                    with torch.no_grad():
                        if 'weight' in name:
                            p_clip, layer_norm = spectral_norm(p,
                                                               self.max_weight_norm,
                                                               self.fix_sigma
                                                               )
                            # fix_sigma : if true we fix the weight norm to some
                            #     value, if false the weight norm can get smaller
                            # max_weight_norm : hyperparameter.
                            p.copy_(p_clip)  # storing the clipped weights in
                            # the model layer p
                            # x_norm_curr : upper bound on norm of node values
                            x_norm_curr *= layer_norm
                            # compute L_{\theta_k}
                            lip_theta.append(get_lip_theta(x_norm_curr, p.shape))
                            # store L_{x_k}
                            lip_x.append(layer_norm)
                        elif 'bias' in name:
                            p_clip, layer_norm = vector_norm(p,
                                                             # bias norm should be lower than weight norm
                                                             self.max_weight_norm,
                                                             self.fix_sigma
                                                             )
                            p.copy_(p_clip)
                            # bias adds up to the norm of the output of the linear operation
                            x_norm_curr += layer_norm
                            lip_theta.append(1)
                            lip_x.append(1)
                        else:
                            raise ValueError(f'Illicite parameter: {name}')
        # append Lipschitz of the loss
        lip_x.append(loss_lip)
        # backward pass
        current = 1
        lip_x_backward = []
        for i in lip_x[::-1]:
            current = current * i
            lip_x_backward.append(current)
        deltas_backward = list(map(lambda x, y: x*y, lip_x_backward,
                                   lip_theta[::-1]))
        self.deltas_backward = deltas_backward

    # def project(self, input_norm, operators_lip, loss_lip):
    #     """ compute layer sensitivity """
    #     # clip weights
    #     lip_theta = []
    #     x_norm_curr = input_norm
    #     lip_x = []
    #     # forward pass
    #     for group in self.param_groups:
    #         # pytorch models have several parameter groups,
    #         # we only use models with a single parameter group
    #         # the loop is only ran once
    #         for p, (layer_key, fn) in zip(group["params"], operators_lip):
    #             # iteration over all trainable parameters
    #             # group["params"] are the trainable weights of the layer
    #             # operators_lip gives the layer type layer_key and
    #             # the function to compute lipschitz value upper bound fn
    #             with torch.no_grad():
    #                 if layer_key in ['linear', 'conv']:
    #                     p_clip, layer_norm = spectral_norm(p,
    #                                                        self.max_weight_norm,
    #                                                        self.fix_sigma
    #                                                        )
    #                     # fix_sigma : if true we fix the weight norm to some
    #                     #     value, if false the weight norm can get smaller
    #                     # max_weight_norm : hyperparameter.
    #                     p.copy_(p_clip)  # storing the clipped weights in
    #                     # the model layer p
    #                     x_norm_curr *= layer_norm
    #                     # x_norm_curr : upper bound on norm of node values
    #                     lip_theta.append(fn(x_norm_curr))  # u_k^{(\theta)}
    #                     lip_x.append(layer_norm)           # L_{x_k}
    #                 elif layer_key in ['bias']:
    #                     p_clip, layer_norm = vector_norm(p,
    #                                                      self.max_weight_norm,
    #                                                      self.fix_sigma
    #                                                      )
    #                     p.copy_(p_clip)
    #                     x_norm_curr += layer_norm
    #                     lip_theta.append(1)
    #                     lip_x.append(1)
    #                 elif layer_key in ['bias+gn']:
    #                     p_clip, layer_norm = vector_norm(p,
    #                                                      self.max_weight_norm,
    #                                                      self.fix_sigma
    #                                                      )
    #                     p.copy_(p_clip)
    #                     x_norm_curr = np.min([np.sqrt(torch.numel(p)),
    #                                          (x_norm_curr+layer_norm) * fn(1)])
    #                     lip_theta.append(1)
    #                     lip_x.append(1)  # check if batch_size in dim 0
    #                 else:
    #                     raise ValueError('Illicite layer key')
    #     # append loss lip
    #     lip_x.append(loss_lip)
    #     # backward pass
    #     current = 1
    #     lip_x_backward = []
    #     for i in lip_x[::-1]:
    #         current = current * i
    #         lip_x_backward.append(current)
    #     deltas_backward = list(map(lambda x, y: x*y, lip_x_backward,
    #                                lip_theta[::-1]))
    #     self.deltas_backward = deltas_backward

    def add_noise(self):
        """
        Adds noise to gradients with sensitivities.
        Stores noised result in ``p.grad``
        """
        deltas = self.deltas_backward[::-1]
        for i, p in enumerate(self.params):
            _check_processed_flag(p.summed_grad)
            noise = _generate_noise(
                std=self.noise_multiplier * deltas[i],
                reference=p.summed_grad,
                generator=self.generator,
                secure_mode=self.secure_mode,
            )
            p.grad = (p.summed_grad + noise).view_as(p)

            _mark_as_processed(p.summed_grad)


class LipDPOptimizerPerLayer(LipDPOptimizer):
    """docstring for LipDPOptimizer."""

    def __init__(self, *args, **kwargs):
        super(LipDPOptimizer, self).__init__(*args, **kwargs)

    def project(self, input_norm, operators_lip, loss_lip):
        x_norms = []
        x_norm_curr = input_norm
        lip_x = []
        i = 0
        # forward pass
        for group in self.param_groups:
            for p, (layer_key, fn) in zip(group["params"], operators_lip):
                with torch.no_grad():
                    if layer_key in ['linear', 'conv']:
                        p_clip, layer_norm = spectral_norm(p,
                                                           self.max_weight_norm[i],
                                                           self.fix_sigma
                                                           )
                        p.copy_(p_clip)
                        x_norm_curr *= layer_norm
                    elif layer_key in ['bias']:
                        clip_grad_norm_(p, max_norm=1)
                        x_norm_curr += 1
                    else:
                        raise ValueError('Illicite layer key')
                    i += 1
                    x_norm_curr *= layer_norm
                    x_norms.append(x_norm_curr)
                    lip_x.append(fn(layer_norm))
                    x_norms.append(fn(x_norm_curr))  # fn defined in model
        # append loss lip
        lip_x.append(loss_lip)  # TODO: is it divided by batch_size?
        # backward pass
        current = lip_x[-1]
        lip_x_backward = []
        for i in lip_x[::-1][1:]:
            current = current * i
            lip_x_backward.append(current)
        deltas_backward = list(map(lambda x, y: x*y, lip_x_backward,
                                   x_norms[::-1]))
        self.deltas_backward = deltas_backward


class DPOptimizerGrad(DPOptimizer):
    """docstring for DPOptimizerGrad."""

    def __init__(self, *args, **kwargs):
        super(DPOptimizerGrad, self).__init__(*args, **kwargs)
        self.clip_factors = []
        self.clip_diff = []
        self.grad = []

    def clip_and_accumulate(self):
        """
        Performs gradient clipping.
        Stores clipped and aggregated gradients into `p.summed_grad```
        """

        if len(self.grad_samples[0]) == 0:
            # Empty batch
            per_sample_clip_factor = torch.zeros((0,))
        else:
            per_param_norms = [
                g.reshape(len(g), -1).norm(2, dim=-1) for g in self.grad_samples
            ]
            per_sample_norms = torch.stack(per_param_norms, dim=1).norm(2, dim=1)
            per_sample_clip_factor = (
                self.max_grad_norm / (per_sample_norms + 1e-6)
            ).clamp(max=1.0)
            self.clip_factors.append(per_sample_clip_factor.mean().item())

        for p in self.params:
            _check_processed_flag(p.grad_sample)
            grad_sample = self._get_flat_grad_sample(p)
            grad = contract("i,i...", per_sample_clip_factor, grad_sample)
            clip_diff = grad_sample.sum(axis=0) - grad
            self.clip_diff.append(clip_diff)
            self.grad.append(grad)

            if p.summed_grad is not None:
                p.summed_grad += grad
            else:
                p.summed_grad = grad

            _mark_as_processed(p.grad_sample)
