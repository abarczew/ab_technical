# Experiments

This repository contains the Python code used to produce the experimental results presented in "DP-SGD with weight clipping".

## Setup

```
# To create a virtual environment
python -m venv myenv

# To activate the virtual environment
source myenv/bin/activate

# Install requirements
pip install -r requirements.txt
```

## Data

All datasets are downloaded through Python except for the Android Permissions dataset which should be downloaded and store in `data` folder with the following link:
* [Android Permissions](https://archive-beta.ics.uci.edu/dataset/722/naticusdroid+android+permissions+dataset)

or run
```
wget -O data/android.csv https://archive.ics.uci.edu/static/public/722/naticusdroid+android+permissions+dataset.zip
```

## Usage

One can change hyperparameters in each `parameters.json` located in `"exp/[dataname]/[grad_clip/weight_norm]/parameters.json"`, results, formatted as per `ax.service` API from Ax-platform, will then be stored in `"exp/[dataname]/[grad_clip/weight_norm]/[folder name of experiment]/results.json"`.

There is one command to run the search:
```
python search.py --exp [dataname] --runs [number of runs for the search] --save-dir [folder name of experiment] --workers [number of workers]
```
There is another command to run the measurements based on the previous search, the results are then stored in `exp/[dataname]/[grad_clip/weight_norm]/[folder name of the experiment]_[group/nogroup]/trials.csv`:
```
python measure.py --exp [dataname] --runs [number of measures] --save-dir [folder name of experiment] --workers [number of workers]
```
If one wants to use pre-defined parameters for measurements, and thus replicate experiments from the paper, one can run the following command. The results are then stored in `exp/[dataname]/[grad_clip/weight_norm]/[folder name of the experiment]_[group/nogroup]/trials.csv`.
```
python measure.py --exp [dataname] --runs [number of measures] --save-dir [destination folder name of experiment] --workers [number of workers] --params best_parameters.json
```
If one wants to run measurements wihtout group normalization then they just have to add `--no-group` at the end of the previous command.

Finally, run the following command to plot results:
```
python plot.py --exp [dataname] --save-dir [folder name of experiment]
```
Or alternatively, one can run one file to do it all:
```
./main.sh
```
