import torch
import json
import os
import argparse
import numpy as np

from ax.service.ax_client import AxClient, ObjectiveProperties

from opacus import GradSampleModule
from opacus.data_loader import DPDataLoader

from lip_dp.train import train, evaluate_acc_auc

from utils import load_mnist, load_fashionmnist, load_cifar, \
    get_partition_data_loaders, load_imbal_mnist
from model import load_model

exp_names = ['mnist', 'fashionmnist', 'cifar']

parser = argparse.ArgumentParser()
parser.add_argument('--exp-name', '-e', dest='exp_name', default='mnist',
                    choices=exp_names,
                    help='experiment name: ' + ' | '.join(exp_names) +
                    ' (default: mnist)')
parser.add_argument('-j', '--workers', default=0, type=int, metavar='N',
                    help='number of data loading workers (default: 0)')
# TODO: check how it's used
parser.add_argument('--runs', '-r', default=50, type=int,
                    help='number of trials to run')
parser.add_argument('--save-dir', '-s', dest='save_dir',
                    help='The directory used to save the trained models',
                    default='save_temp', type=str)
parser.add_argument('--imbal', '-i', default=False, type=bool,
                    help='boolean to load imbalanced dataset, \
                    only available for mnist')
parser.add_argument('--datapoints', '-d', default=10, type=int,
                    help='number of datapoints per curve')


def run(method):
    global args
    args = parser.parse_args()

    epsilon_range = np.geomspace(0.09, 11.0, args.datapoints)

    path = f"exp/{args.exp_name}/{method}/"
    path_results = f"{path}{args.save_dir}/"

    if not os.path.exists(path_results):
        os.makedirs(path_results)

    torch.manual_seed(42)
    dtype = torch.float
    os.environ["PYTORCH_CUDA_ALLOC_CONF"] = "max_split_size_mb:512"
    # torch.distributed.init_process_group(
    #     init_method="env://",
    #     backend="gloo",
    # )
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    input_norm = 1
    # TODO: impact norm in transform
    if args.exp_name == 'mnist' and args.imbal:
        train_valid_set, test_set = load_imbal_mnist(input_norm)
    elif args.exp_name == 'mnist':
        train_valid_set, test_set = load_mnist(input_norm)
    elif args.exp_name == 'fashionmnist':
        train_valid_set, test_set = load_fashionmnist(input_norm)
    elif args.exp_name == 'cifar':
        train_valid_set, test_set = load_cifar(input_norm)

    if args.imbal:
        metric_name = "auc"
    else:
        metric_name = "accuracy"

    def train_evaluate(parameters, epsilon):
        """
        Train the model and then compute an evaluation metric.

        In this tutorial, the CNN utils package is doing a lot of work
        under the hood:
            - `train` initializes the network, defines the loss function
            and optimizer, performs the training loop, and returns the
            trained model.
            - `evaluate` computes the accuracy of the model on the
            evaluation dataset and returns the metric.

        For your use case, you can define training and evaluation functions
        of your choosing.

        """
        net = load_model(args.exp_name)

        parameters["operators_lip"] = net.operators_lip
        parameters["epsilon_target"] = epsilon
        net = GradSampleModule(net, batch_first=True)

        # Initialize dataloaders
        train_loader, valid_loader, test_loader = get_partition_data_loaders(
            train_valid_set=train_valid_set,
            test_set=test_set,
            downsample_pct=1.,
            train_pct=0.8,
            batch_size=parameters.get("batch_size", 32),
            num_workers=args.workers,
            deterministic_partitions=False,
            downsample_pct_test=None,
            pin_memory=(args.exp_name == 'cifar')
        )
        train_loader = DPDataLoader.from_data_loader(train_loader,
                                                     distributed=False)
        delta = parameters.get("delta", 1/10000)
        net, accountant = train(
            net=net,
            train_loader=train_loader,
            parameters=parameters,
            dtype=dtype,
            device=device,
        )
        return {
            metric_name: (evaluate_acc_auc(
                net=net,
                data_loader=valid_loader,
                dtype=dtype,
                device=device,)[metric_name], None),
            "epsilon": (accountant.get_epsilon(delta), None)}

    with open(path+'parameters_mono.json', 'r') as f:
        parameters = json.load(f)

    ax_client = AxClient()
    ax_client.create_experiment(
        name=path.replace('/', '_'),  # The name of the experiment.
        parameters=parameters,
        # The objective name and minimization setting.
        objectives={metric_name: ObjectiveProperties(minimize=False)},
    )
    for eps in epsilon_range:
        for i in range(args.runs):
            parameters, trial_index = ax_client.get_next_trial()
            ax_client.complete_trial(trial_index=trial_index,
                                     raw_data=train_evaluate(parameters, eps))

    # write results
    ax_client.save_to_json_file(filepath=path_results+"results.json")


if __name__ == '__main__':
    for method in ['grad_clip', 'weight_norm']:
        run(method)
