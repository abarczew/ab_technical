from opacus import PrivacyEngine
from opacus import GradSampleModule
from collections import OrderedDict
from typing import Dict, Callable
from scipy.spatial.distance import pdist
from lip_dp.deel_torchlip.deel.torchlip.functional import kr_loss, hkr_loss, hinge_margin_loss
from lip_dp.deel_torchlip.deel import torchlip
import torch
from torchvision import datasets
from opacus.optimizers import DPOptimizer
import torch.nn.functional as F
import functools

# First we select the two classes
selected_classes = [0, 8]  # must be two classes as we perform binary classification


def prepare_data(dataset, class_a=0, class_b=8):
    """
    This function converts the MNIST data to make it suitable for our binary
    classification setup.
    """
    x = dataset.data[:1000]
    y = dataset.targets[:1000]
    # select items from the two selected classes
    mask = (y == class_a) + (
        y == class_b
    )  # mask to select only items from class_a or class_b
    x = x[mask]
    y = y[mask]

    # convert from range int[0,255] to float32[-1,1]
    x = x.float() / 255
    x = x.reshape((-1, 28, 28, 1))
    # change label to binary classification {-1,1}

    y_ = torch.zeros_like(y).float()
    y_[y == class_a] = 1.0
    y_[y == class_b] = -1.0
    return torch.utils.data.TensorDataset(x, y_)


train = datasets.MNIST("./data", train=True, download=True)
test = datasets.MNIST("./data", train=False, download=True)

# Prepare the data
train = prepare_data(train, selected_classes[0], selected_classes[1])
test = prepare_data(test, selected_classes[0], selected_classes[1])

# Display infos about dataset
print(
    f"Train set size: {len(train)} samples, classes proportions: "
    f"{100 * (train.tensors[1] == 1).numpy().mean():.2f} %"
)
print(
    f"Test set size: {len(test)} samples, classes proportions: "
    f"{100 * (test.tensors[1] == 1).numpy().mean():.2f} %"
)


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

ninputs = 28 * 28
wass = torchlip.Sequential(
    torch.nn.Flatten(),
    torch.nn.Linear(ninputs, 128),
    torchlip.FullSort(),
    torch.nn.Linear(128, 64),
    torchlip.FullSort(),
    torch.nn.Linear(64, 32),
    torchlip.FullSort(),
    torchlip.FrobeniusLinear(32, 10),
).to(device)


# wass = GradSampleModule(wass, batch_first=True)


wass

# training parameters
epochs = 7
batch_size = 32

# loss parameters
min_margin = 1
alpha = 10

# dp parameter
epsilon = 10
delta = 1/10**3

# dp-sgd parameter
max_grad_norm = 1

train_loader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
test_loader = torch.utils.data.DataLoader(test, batch_size=32, shuffle=False)

optimizer = torch.optim.Adam(lr=0.001, params=wass.parameters())
sample_rate = 1 / len(train_loader)
expected_batch_size = int(len(train_loader.dataset) * sample_rate)
# optimizer = DPOptimizer(
#     optimizer=optimizer,
#     noise_multiplier=1.0,
#     max_grad_norm=1.0,
#     expected_batch_size=expected_batch_size,
# )

privacy_engine = PrivacyEngine()

wass, optimizer, train_loader = privacy_engine.make_private_with_epsilon(
    module=wass,
    optimizer=optimizer,
    data_loader=train_loader,
    epochs=epochs,
    target_epsilon=epsilon,
    target_delta=delta,
    max_grad_norm=max_grad_norm,
)


def spectral_hook(model, input, output):
    print('hook')


def apply_spectral_hook(module):
    if hasattr(module, 'weight'):
        module.register_forward_hook(spectral_hook)


def remove_all_hooks(model: torch.nn.Module) -> None:
    for name, child in model._modules.items():
        if child is not None:
            if hasattr(child, "_forward_hooks"):
                child._forward_hooks: Dict[int, Callable] = OrderedDict()
            elif hasattr(child, "_forward_pre_hooks"):
                child._forward_pre_hooks: Dict[int, Callable] = OrderedDict()
            elif hasattr(child, "_backward_hooks"):
                child._backward_hooks: Dict[int, Callable] = OrderedDict()
                remove_all_hooks(child)


def execute_every(n):
    def decorator(f):
        cnt_exec = 0

        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            nonlocal cnt_exec
            print(cnt_exec)
            cnt_exec += 1
            if cnt_exec % n == 0:
                f(*args, **kwargs)

        return wrapper
    return decorator


G = 1
lip = []
delta = []


def compute_hook(module, grad_input, grad_output):
    print("backward kook")
    pass


@execute_every(batch_size)
def apply_compute_hook(module):
    if hasattr(module, 'weight'):
        module.register_backward_hook(compute_hook)


# wass.apply(apply_compute_hook)


for epoch in range(epochs):

    m_kr, m_hm, m_acc = 0, 0, 0
    wass.train()

    for step, (data, target) in enumerate(train_loader):

        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = wass(data)
        # loss = hkr_loss(output, target, alpha=alpha,
        #                 min_margin=min_margin)
        loss = F.cross_entropy(output, target)
        loss.backward()
        optimizer.step()

        # constrain lipschitzness
        wass.apply(apply_spectral_hook)
        wass(data)
        remove_all_hooks(wass)

        # Compute metrics on batch
        m_kr += kr_loss(output, target, (1, -1))
        m_hm += hinge_margin_loss(output, target, min_margin)
        m_acc += (torch.sign(output).flatten() == torch.sign(target)).sum() / len(
            target
        )

    # Train metrics for the current epoch
    metrics = [
        f"{k}: {v:.04f}"
        for k, v in {
            "loss": loss,
            "KR": m_kr / (step + 1),
            "acc": m_acc / (step + 1),
        }.items()
    ]

    # Compute test loss for the current epoch
    wass.eval()
    testo = []
    for data, target in test_loader:
        data, target = data.to(device), target.to(device)
        testo.append(wass(data).detach().cpu())
    testo = torch.cat(testo).flatten()

    # Validation metrics for the current epoch
    metrics += [
        f"val_{k}: {v:.04f}"
        for k, v in {
            "loss": hkr_loss(
                testo, test.tensors[1], alpha=alpha, min_margin=min_margin
            ),
            "KR": kr_loss(testo.flatten(), test.tensors[1], (1, -1)),
            "acc": (torch.sign(testo).flatten() == torch.sign(test.tensors[1]))
            .float()
            .mean(),
        }.items()
    ]

    print(f"Epoch {epoch + 1}/{epochs}")
    print(" - ".join(metrics))


wass.eval()
p = []
for batch, _ in train_loader:
    x = batch.numpy()
    y = wass(batch.to(device)).detach().cpu().numpy()
    xd = pdist(x.reshape(batch.shape[0], -1))
    yd = pdist(y.reshape(batch.shape[0], -1))

    p.append((yd / xd).max())
print(torch.tensor(p).max())
