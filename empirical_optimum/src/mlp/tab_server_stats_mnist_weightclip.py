from scipy.stats import wilcoxon
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('seaborn-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)

markers = ['o', '^', 's', 'P', 'v', '>', '<', '*', '+', 'x', 'D', 2]


df_local = pd.read_csv('results/exp_0514/mnist_local_perf_big.csv')
df_global = pd.read_csv('results/exp_0514/mnist_local_perf_small.csv')
df_other = pd.read_csv('results/exp_0514/measure_perf.csv')
df_other_3 = pd.read_csv('results/exp_0514/measure_perf_3.csv')
# df = df_global.append(df_local)
# df = df.append(df_other)
# df = df.fillna('None')

feature_exp = ['dataset_name', 'method', 'epsilon', 'clipping_param_norm']
epochs = 30
df = pd.DataFrame()
for dt, n_tries in zip([df_local, df_global, df_other, df_other_3], [10, 10, 5, 5]):
    dt = dt.fillna('None')
    dt['lip_element'] = dt.apply(lambda x: 'weight'
                                 if x.clipping_param_norm != 'None'
                                 else 'gradient', axis=1)
    dt['method'] = dt.apply(lambda x:
                            f'{x.lip_element}_{x.lip_method}',
                            axis=1)
    combo_feat = len(dt[feature_exp].drop_duplicates())
    runs = list(np.repeat(list(range(n_tries)), epochs))
    dt['run'] = np.tile(runs, combo_feat)
    df = df.append(dt)

df = df.loc[df.clipping_param_norm != 0.3]
# df['lip_element'] = df.apply(lambda x: 'weight'
#                              if x.clipping_param_norm != 'None'
#                              else 'gradient', axis=1)
# df['method'] = df.apply(lambda x:
#                         f'{x.lip_element}_{x.lip_method}',
#                         axis=1)
# feature_exp = ['dataset_name', 'method', 'epsilon', 'clipping_param_norm']
# combo_feat = len(df[feature_exp].drop_duplicates())
# n_tries = 10
# epochs = 30
# runs = list(np.repeat(list(range(n_tries)), epochs))
# df['run'] = np.tile(runs, combo_feat)

df_run = df.loc[df.epsilon == 1].groupby(
    ['clipping_param_norm', 'method'] + ['run']).metric.max().unstack(level=1)
df_run

df_run_tab = df_run.reset_index()
df_plot = df_run_tab\
    .groupby('clipping_param_norm')\
    .agg(['std', 'median'])\
    .reset_index()
methods = ['weight_global', 'weight_local'][::-1]
for i, method in enumerate(methods):
    plt.errorbar(df_plot.clipping_param_norm,
                 df_plot[method]['median'],
                 yerr=df_plot[method]['std'],
                 label=f'{method}',
                 marker=markers[i])
plt.grid()
plt.xlabel('Weight clipping norm')
# plt.xscale('log')
plt.ylabel('AUC')
plt.ylim(0.49, 1.01)
plt.legend()
plt.savefig('results/exp_0514/weightclip_perf.png', bbox_inches='tight')
plt.close()
