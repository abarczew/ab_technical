import os
from datetime import datetime
import pandas as pd
from conf import OUTDIR
from sklearn.metrics import roc_auc_score, mean_absolute_percentage_error
import torch
from torch.utils.data import random_split
from utils import get_data, viz_perf_vs_hyperparms, viz_perf_vs_epochs, \
    viz_perf_vs_epsilon, get_input_norm, viz_sigma_vs_epsilon, init_normal
from pytorch_modules import MyDataset, LogisticRegression, ClassifNet, \
    Callback, Trainer, RegNet

args = {'dataset_name': 'bank-full',
        'model': 'classifnet',
        'sample_size': -1,
        'epochs': 10,
        'test_size': 0.2,
        'batch_size': 10,
        'standardize': False,
        'activation': {'fn': torch.sigmoid,
                       'activation_max': 1/4},
        'learning_rate': 0.0002,
        'lasso_const': 0.,
        'lip_method': 'global',
        'lip_local_method': 'autolip',
        'accountant': 'ma',
        'epsilon': 1.0,
        'delta': 1 / 1000**2,
        'clipping_grad_norm': 2,
        'clipping_param_norm': None,
        'hidden_dim': 20,
        'viz_metric': 'metric',
        'items_file': ['dataset_name', 'model',
                       'learning_rate', 'epsilon',
                       'lip_method', 'accountant',
                       'clipping_grad_norm', 'clipping_param_norm',
                       'hidden_dim'],
        'hyper_params': ['learning_rate', 'hidden_dim',
                         'clipping_param_norm']}

torch_seed = torch.Generator().manual_seed(42)


def main():
    now = datetime.now().strftime("%m%d")
    outpath = f'{OUTDIR}/test_{now}'
    if not os.path.exists(outpath):
        os.makedirs(outpath)
    outpath_training = f'{outpath}/training'
    if not os.path.exists(outpath_training):
        os.makedirs(outpath_training)
    input_norm = get_input_norm(args['clipping_grad_norm'],
                                args['clipping_param_norm'],
                                args['activation']['activation_max'])
    X, y = get_data(args['dataset_name'],
                    sample_size=args['sample_size'])
    if args['model'] == 'regnet':
        dataset = MyDataset(X, y, args["test_size"],
                            'regression',
                            standardize=args['standardize'])
    else:
        dataset = MyDataset(X, y, args["test_size"],
                            standardize=args['standardize'],
                            input_norm=input_norm)
    test_set_size = int(len(dataset) * args['test_size'])
    train_set_size = len(dataset) - test_set_size
    train_subset, test_subset = random_split(dataset,
                                             [train_set_size, test_set_size],
                                             generator=torch_seed)
    test_dataset = test_subset.dataset
    if args['model'] == 'regnet':
        criterion = torch.nn.MSELoss()
        metric = mean_absolute_percentage_error
    elif dataset.class_num > 1:
        criterion = torch.nn.CrossEntropyLoss()
        metric = roc_auc_score
    else:
        criterion = torch.nn.BCELoss()
        metric = roc_auc_score
    if args['model'] == 'lr':
        model = LogisticRegression(dataset.input_dim,
                                   dataset.class_num)
    elif args['model'] == 'classifnet':
        model = ClassifNet(dataset.input_dim,
                           dataset.class_num,
                           int(dataset.class_num * 10),
                           args['activation'],
                           input_norm=input_norm)
    elif args['model'] == 'regnet':
        model = RegNet(dataset.input_dim, args['hidden_dim'],
                       args['activation'],
                       input_norm=input_norm)
    model.apply(init_normal)
    callback = Callback(args)
    trainer = Trainer(model,
                      criterion,
                      metric,
                      args['learning_rate'],
                      args['lip_method'],
                      args['lip_local_method'],
                      args['accountant'],
                      args['epsilon'],
                      args['delta'],
                      train_set_size,
                      args['lasso_const'],
                      torch_seed,
                      callback,
                      args['batch_size'],
                      args['epochs'],
                      args['clipping_grad_norm'],
                      args['clipping_param_norm'])
    history = trainer.fit(train_subset, test_dataset)
    history.output_epoch_viz(outpath_training)
    print(history.output())

    df_perf_all = history.build_results()

    df_conf = pd.DataFrame.from_records([args])
    path = f'{outpath}/{args["dataset_name"]}'
    df_conf.to_csv(f"{path}_conf.csv")
    df_perf_all = df_perf_all.fillna('None')
    df_perf_all.to_csv(f"{path}_perf.csv")
    df_perf_all['lip_element'] = df_perf_all.apply(lambda x: 'param'
                                                   if x.clipping_param_norm is not None
                                                   else 'grad', axis=1)
    df_perf_all['method'] = df_perf_all.apply(lambda x:
                                              f'{x.lip_element}_{x.lip_method}_{x.accountant}',
                                              axis=1)

    path_epoch = f'{path}_epoch'
    viz_perf_vs_epochs(df_perf_all, path_epoch,
                       args['hyper_params'],
                       args['viz_metric'],
                       epsilon=1.)
    path_eps = f'{path}_epsilon'
    viz_perf_vs_epsilon(df_perf_all, path_eps,
                        args['viz_metric'])
    path_hyper = f'{path}_hyper'
    viz_perf_vs_hyperparms(df_perf_all, path_hyper,
                           args['hyper_params'],
                           args['viz_metric'],
                           epsilon=1.)
    path_sigma = f'{path}_sigma'
    viz_sigma_vs_epsilon(df_perf_all, path_sigma)


if __name__ == '__main__':
    main()
