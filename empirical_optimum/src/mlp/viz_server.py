import pandas as pd
from utils import viz_perf_vs_epsilon, viz_sigma_vs_epsilon, \
    viz_perf_vs_epochs, viz_perf_vs_hyperparms

filenames = ['mnist_perf_grad_correct', 'mnist_weight_big_perf',
             'mnist_weight_small_perf', 'mnist_weight_small_perf_small_lr']
hyper_params = ['learning_rate', 'hidden_dim',
                'clipping_param_norm']
viz_metric = 'metric'


if __name__ == '__main__':
    df_perf_all = pd.DataFrame()
    path = 'results/exp_0414'
    for filename in filenames:
        df = pd.read_csv(f'{path}/{filename}.csv')
        df_perf_all = df_perf_all.append(df)
    df_perf_all = df_perf_all.fillna('None')
    df_perf_all['lip_element'] = df_perf_all.apply(lambda x: 'weight'
                                                   if x.clipping_param_norm != 'None'
                                                   else 'gradient', axis=1)
    df_perf_all['method'] = df_perf_all.apply(lambda x:
                                              f'{x.lip_element}_{x.lip_method}',
                                              axis=1)

    path_epoch = f'{path}/epoch'
    viz_perf_vs_epochs(df_perf_all, path_epoch,
                       hyper_params,
                       viz_metric,
                       epsilon=1.)
    path_eps = f'{path}/epsilon'
    viz_perf_vs_epsilon(df_perf_all, path_eps,
                        viz_metric)
    path_hyper = f'{path}/hyper'
    viz_perf_vs_hyperparms(df_perf_all, path_hyper,
                           hyper_params,
                           viz_metric,
                           epsilon=1.)
    path_sigma = f'{path}/sigma'
    viz_sigma_vs_epsilon(df_perf_all, path_sigma)
