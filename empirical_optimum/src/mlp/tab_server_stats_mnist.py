from scipy.stats import wilcoxon
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('seaborn-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)

markers = ['o', '^', 's', 'P', 'v', '>', '<', '*', '+', 'x', 'D', 2]


df_local = pd.read_csv('results/exp_0509/mnist_local_perf.csv')
df_global = pd.read_csv('results/exp_0509/mnist_global_perf.csv')
df = df_global.append(df_local)
df = df.fillna('None')
df['lip_element'] = df.apply(lambda x: 'weight'
                             if x.clipping_param_norm != 'None'
                             else 'gradient', axis=1)
df['method'] = df.apply(lambda x:
                        f'{x.lip_element}_{x.lip_method}',
                        axis=1)
feature_exp = ['dataset_name', 'method', 'epsilon']
combo_feat = len(df[feature_exp].drop_duplicates())
n_tries = 10
epochs = [5, 10, 15, 20, 25, 30]
runs = []
for i in epochs:
    runs += list(np.repeat(list(range(n_tries)), i))
df['run'] = np.tile(runs, combo_feat)
df_run = df.loc[df.epsilon == 1].groupby(
    ['dataset_name', 'method', 'epochs'] + ['run']).metric.max().unstack(level=1)

df_run_tab = df_run.reset_index()
df_plot = df_run_tab\
    .groupby('epochs')\
    .agg(['std', 'median'])\
    .reset_index()
methods = ['weight_global', 'weight_local'][::-1]
for i, method in enumerate(methods):
    plt.errorbar(df_plot.epochs,
                 df_plot[method]['median'],
                 yerr=df_plot[method]['std'],
                 label=f'{method}',
                 marker=markers[i])
plt.grid()
plt.xlabel('Epochs')
plt.ylabel('AUC')
plt.ylim(0.49, 1.01)
plt.legend()
plt.savefig('results/exp_0509/epochs_perf.png', bbox_inches='tight')
plt.close()

df_plot

df_run_test = df_run.reset_index()
df_run_test = df_run_test.loc[df_run_test.epochs == 5]
for dataset in df_run_test['dataset_name'].unique():
    print(
        f'{dataset} has p-value {wilcoxon(df_run_test.loc[df_run_test.dataset_name == dataset].weight_global, df_run_test.loc[df_run_test.dataset_name == dataset].weight_local, alternative="less").pvalue}')
