import torch

DATADIR = 'data'
OUTDIR = 'results'

args_all = {'lip_method': ['local', 'global'],  # 'global', 'no_dp'
            'lip_local_method': [None, 'autolip', 'seqlip'],
            'accountant': ['ma'],  # 'comp',
            'model': ['classifnet'],
            'activation': [{'fn': torch.sigmoid,
                           'activation_max': 1/2}],  # , 'dnn'
            'epochs': [6],
            'n_jobs': [-1],
            'test_size': [0.2],
            'batch_size': [10],  # 1,
            'epsilon': [0.1, 1.0, 10.0],
            'clipping_grad_norm': [None, 4, 8],
            'clipping_param_norm': [None, 1, 10],
            'hidden_dim': [20],
            'viz_metric': ['metric'],
            'items_file': [['dataset_name', 'model',
                           'learning_rate', 'epsilon',
                            'lip_method', 'accountant',
                            'clipping_grad_norm', 'clipping_param_norm',
                            'hidden_dim']]}

args_breast = {**args_all,
               'dataset_name': 'breast_cancer',
               'sample_size': -1,
               'delta': 1 / 100 ** 2,
               'learning_rate': [0.0001],
               'lasso_const': [0, 0.01],
               'standardize': True}

args_income = {**args_all,
               'dataset_name': 'adult_income',
               'sample_size': -1,
               'delta': 1 / 10000 ** 2,
               'learning_rate': [0.001],
               'lasso_const': [0, 0.01],
               'standardize': True}

args_fram = {**args_all,
             'dataset_name': 'framingham',
             'sample_size': -1,
             'delta': 1 / 1000 ** 2,
             'learning_rate': [0.1],
             'lasso_const': [0, 0.01],
             'standardize': True}

args_titanic = {**args_all,
                'dataset_name': 'titanic',
                'sample_size': -1,
                'delta': 1 / 100 ** 2,
                'learning_rate': [0.0001],
                'lasso_const': [0, 0.000001],
                'standardize': True}

args_bank = {**args_all,
             'dataset_name': 'bank-full',
             'sample_size': -1,
             'delta': 1 / 10000 ** 2,
             'learning_rate': [0.001],
             'lasso_const': [0, 0.01],
             'standardize': False}

args_dia = {**args_all,
            'dataset_name': 'diabetes',
            'sample_size': -1,
            'delta': 1 / 100 ** 2,
            'learning_rate': [0.0001],
            'lasso_const': [0, 0.01],
            'standardize': False}

args_perm = {**args_all,
             'dataset_name': 'permission',
             'sample_size': -1,
             'delta': 1 / 1000 ** 2,
             'learning_rate': [0.0001],
             'lasso_const': [0, 0.001],
             'standardize': True}

args_mnist = {**args_all,
              'dataset_name': ['mnist'],
              'sample_size': [-1],
              'delta': [1 / 10000 ** 2],
              'learning_rate': [0.001, 0.01],
              'lasso_const': [0.],
              'standardize': [False]}

args_dnn = {**args_all,
            'delta': 1 / 10000 ** 2}

args_mnist_clipsearch = {**args_all,
                         'dataset_name': 'mnist',
                         'sample_size': -1,
                         'delta': 1 / 10000 ** 2,
                         'learning_rate': [0.001],
                         'lasso_const': [0],
                         'standardize': False}

args_nursery_seqlip = {'accountant': ['ma'],
                       'model': ['classifnet'],
                       'activation': [{'fn': torch.sigmoid,
                                       'activation_max': 1/4}],
                       'n_jobs': [-1.0],
                       'test_size': [0.2],
                       'batch_size': [10],
                       'lasso_const': [0.],
                       'epsilon': [1.0],
                       'input_norm': [None],
                       'viz_metric': ['metric'],
                       'lip_local_method': ['seqlip'],
                       'items_file': [['dataset_name', 'model', 'learning_rate', 'epsilon',
                                       'lip_method', 'accountant', 'clipping_grad_norm', 'clipping_param_norm', 'hidden_dim']],
                       'epochs': [19],
                       'hidden_dim': [20],
                       'dataset_name': ['nursery'],
                       'sample_size': [-1.0],
                       'delta': [1/1000**2],
                       'learning_rate': [0.01],
                       'standardize': [True],
                       'lip_method': ['local'],
                       'clipping_grad_norm': [None],
                       'clipping_param_norm': [1],
                       'hyper_params': [['learning_rate', 'hidden_dim', 'clipping_param_norm']],
                       'name': ['nursery_seqlip']}

# ,args_dia, args_bank, args_mnist_clipsearch
# , args_income, args_perm, args_breast
args_list = [args_nursery_seqlip]
