import pandas as pd

filenames_mnist = ['mnist_perf_grad_correct', 'mnist_weight_small_perf']
filenames_other = ['breast_grad_small_perf', 'breast_weight_small_perf',
                   'income_grad_small_perf', 'income_weight_small_perf',
                   'perm_grad_small_perf', 'perm_weight_small_perf',
                   'survival_grad_small_perf', 'survival_weight_small_perf']
filenames_other_2 = ['default_credit_grad_small_perf', 'default_credit_weight_small_perf',
                     'dropout_grad_small_perf', 'dropout_weight_small_perf',
                     'german_grad_small_perf', 'german_weight_small_perf',
                     'nursery_grad_small_perf', 'nursery_weight_small_perf',
                     'shuttle_grad_small_perf', 'shuttle_weight_small_perf',
                     'thyroid_grad_small_perf', 'thyroid_weight_small_perf',
                     'yeast_grad_small_perf', 'yeast_weight_small_perf']
hyper_params = ['learning_rate', 'hidden_dim',
                'clipping_param_norm']
viz_metric = 'metric'


if __name__ == '__main__':
    df_perf_all = pd.DataFrame()
    path = 'results/exp_0414'
    for filename in filenames_mnist:
        df = pd.read_csv(f'{path}/{filename}.csv')
        df_perf_all = df_perf_all.append(df)
    path = 'results/exp_0417'
    for filename in filenames_other:
        df = pd.read_csv(f'{path}/{filename}.csv')
        df_perf_all = df_perf_all.append(df)
    path = 'results/exp_0502'
    for filename in filenames_other_2:
        df = pd.read_csv(f'{path}/{filename}.csv')
        df_perf_all = df_perf_all.append(df)
    df_perf_all = df_perf_all.fillna('None')
    df_perf_all['lip_element'] = df_perf_all.apply(lambda x: 'weight'
                                                   if x.clipping_param_norm != 'None'
                                                   else 'gradient', axis=1)
    df_perf_all['method'] = df_perf_all.apply(lambda x:
                                              f'{x.lip_element}_{x.lip_method}',
                                              axis=1)
    df_plot = df_perf_all.loc[df_perf_all.epsilon == 1]\
        .groupby(['dataset_name', 'method'])[viz_metric]\
        .max()
    df_table = df_plot.unstack(level=-1)
    print(df_table.applymap(lambda x: round(x, 3)).to_latex())
