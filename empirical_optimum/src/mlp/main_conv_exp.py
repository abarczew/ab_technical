import torch
import numpy as np

from typing import Dict

from ax.service.ax_client import AxClient, ObjectiveProperties
from ax.service.utils.report_utils import exp_to_df
from ax.utils.tutorials.cnn_utils import evaluate, get_partition_data_loaders

import torch.nn as nn
import torch.nn.functional as F
from torch._tensor import Tensor
from torch.utils.data import DataLoader
from torchvision import datasets, transforms
import torch.optim as optim

from opacus import GradSampleModule
from opacus.optimizers import DPOptimizer
from opacus.data_loader import DPDataLoader
from opacus.accountants.utils import get_noise_multiplier

from lip_dp.optimizers import LipDPOptimizer, get_operator_lip, get_loss_lip

from exp.mnist.utils import load_mnist

torch.manual_seed(42)
dtype = torch.float
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
path = f"exp/mnist/{method}/"

input_norm = 1
# TODO: impact norm in transform
train_valid_set, test_set = load_mnist(input_norm)

ax_client = AxClient()

ax_client.create_experiment(
    name="tune_cnn_on_mnist",  # The name of the experiment.
    parameters=[
        {
            "name": "lr",  # The name of the parameter.
            "type": "range",  # The type of the parameter ("range", "choice" or "fixed").
            "bounds": [1e-6, 0.4],  # The bounds for range parameters.
            # "values" The possible values for choice parameters .
            # "value" The fixed value for fixed parameters.
            # Optional, the value type ("int", "float", "bool" or "str"). Defaults to inference from type of "bounds".
            "value_type": "float",
            # Optional, whether to use a log scale for range parameters. Defaults to False.
            "log_scale": True,
            # "is_ordered" Optional, a flag for choice parameters.
        },
        {
            "name": "momentum",
            "type": "range",
            "bounds": [0.0, 1.0],
        },
        {
            "name": "batch_size",
            "type": "range",
            "value_type": "int",
            "bounds": [10, 512]
        }
    ],
    # The objective name and minimization setting.
    objectives={"accuracy": ObjectiveProperties(minimize=False)},
    # parameter_constraints: Optional, a list of strings of form "p1 >= p2" or "p1 + p2 <= some_bound".
    # outcome_constraints: Optional, a list of strings of form "constrained_metric <= some_bound".
)


class CNN(nn.Module):
    def __init__(self) -> None:
        super().__init__()
        self.conv1 = nn.Conv2d(1, 20, kernel_size=5, stride=1)
        self.fc1 = nn.Linear(8 * 8 * 20, 64)
        self.fc2 = nn.Linear(64, 10)
        self.operators_lip = [
            ('conv', lambda x: np.sqrt(5)),  # TODO: is kernel_size the size of parameters?
            ('activation', lambda x: 1),
            ('pool', lambda x: 1),
            ('view', lambda x: 1),  # TODO: has no parameter, should get rid?
            ('linear', lambda x: x),
            ('activation', lambda x: 1),
            ('linear', lambda x: x),
            ('activation', lambda x: 0.5)  # TODO: is it for log_softmax?
        ]

    def forward(self, x: Tensor) -> Tensor:
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 3, 3)
        x = x.view(-1, 8 * 8 * 20)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return F.log_softmax(x, dim=-1)


def train(
    net: torch.nn.Module,
    train_loader: DataLoader,
    parameters: Dict[str, float],
    dtype: torch.dtype,
    device: torch.device,
) -> nn.Module:
    """
    Train CNN on provided data set.

    Args:
        net: initialized neural network
        train_loader: DataLoader containing training set
        parameters: dictionary containing parameters to be passed to the optimizer.
            - lr: default (0.001)
            - momentum: default (0.0)
            - weight_decay: default (0.0)
            - num_epochs: default (1)
        dtype: torch dtype
        device: torch device
    Returns:
        nn.Module: trained CNN.
    """
    # Initialize network
    net.to(dtype=dtype, device=device)
    net.train()

    # Define loss and optimizer
    criterion = torch.nn.CrossEntropyLoss()
    # operators_lip = get_operator_lip(net)
    # TODO: get the above function back
    method = parameters.get("method", "weight_norm")

    if method == "weight_norm":
        operators_lip = parameters.get("operators_lip", [])
        loss_lip = get_loss_lip(criterion)

    optimizer = optim.SGD(
        net.parameters(),
        lr=parameters.get("lr", 0.001),
        momentum=parameters.get("momentum", 0.0),
        weight_decay=parameters.get("weight_decay", 0.0),
    )
    # TODO: change optimizer
    if method == "grad_clip":
        optimizer = DPOptimizer(
            optimizer=optimizer,
            noise_multiplier=parameters.get("noise_multiplier", 1),
            max_grad_norm=parameters.get("max_grad_norm", 1.),
            expected_batch_size=parameters.get("expected_batch_size", 1),
        )
    elif method == "weight_norm":
        optimizer = LipDPOptimizer(
            optimizer=optimizer,
            noise_multiplier=parameters.get("noise_multiplier", 1),
            max_grad_norm=parameters.get("max_grad_norm", 1.),
            # TODO: change name of max grad norm
            expected_batch_size=parameters.get("expected_batch_size", 1),
        )
    else:
        raise ValueError("Method not in ['weight_norm', 'grad_clip']")

    scheduler = optim.lr_scheduler.StepLR(
        optimizer,
        step_size=int(parameters.get("step_size", 30)),
        gamma=parameters.get("gamma", 1.0),  # default is no learning rate decay
    )
    num_epochs = parameters.get("num_epochs", 1)

    # Train Network
    # pyre-fixme[6]: Expected `int` for 1st param but got `float`.
    for _ in range(num_epochs):
        for inputs, labels in train_loader:
            # move data to proper dtype and device
            inputs = inputs.to(dtype=dtype, device=device)
            labels = labels.to(device=device)

            if method == "weight_norm":
                optimizer.project(input_norm, operators_lip, loss_lip)
            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            scheduler.step()
        if method == "weight_norm":
            # last step isnot projected
            optimizer.project(input_norm, operators_lip, loss_lip)
    return net


def train_evaluate(parameters):
    """
    Train the model and then compute an evaluation metric.

    In this tutorial, the CNN utils package is doing a lot of work
    under the hood:
        - `train` initializes the network, defines the loss function
        and optimizer, performs the training loop, and returns the
        trained model.
        - `evaluate` computes the accuracy of the model on the
        evaluation dataset and returns the metric.

    For your use case, you can define training and evaluation functions
    of your choosing.

    """
    net = CNN()
    parameters["operators_lip"] = net.operators_lip
    net = GradSampleModule(net, batch_first=True)

    # Initialize dataloaders
    train_loader, valid_loader, test_loader = get_partition_data_loaders(
        train_valid_set=train_valid_set,
        test_set=test_set,
        downsample_pct=0.5,
        train_pct=0.8,
        batch_size=parameters.get("batch_size", 32),
        num_workers=0,
        deterministic_partitions=False,
        downsample_pct_test=None,
    )
    train_loader = DPDataLoader.from_data_loader(train_loader,
                                                 distributed=False)

    epsilon = parameters.get("epsilon", 1.)
    delta = parameters.get("delta", 1/10000)
    num_epochs = parameters.get("num_epochs", 1)
    sample_rate = 1 / len(train_loader)
    expected_batch_size = int(len(train_loader.dataset) * sample_rate)
    parameters["expected_batch_size"] = expected_batch_size

    parameters["noise_multiplier"] = get_noise_multiplier(
        target_epsilon=epsilon,
        target_delta=delta,
        sample_rate=sample_rate,
        epochs=num_epochs)

    net = train(
        net=net,
        train_loader=train_loader,
        parameters=parameters,
        dtype=dtype,
        device=device,
    )
    # TODO: add auc
    return evaluate(
        net=net,
        data_loader=valid_loader,
        dtype=dtype,
        device=device,
    )


# Attach the trial
ax_client.attach_trial(
    parameters={"lr": 0.000026, "momentum": 0.58, "batch_size": 32}
)

# Get the parameters and run the trial
baseline_parameters = ax_client.get_trial_parameters(trial_index=0)
ax_client.complete_trial(trial_index=0, raw_data=train_evaluate(baseline_parameters))

for i in range(3):
    parameters, trial_index = ax_client.get_next_trial()
    # Local evaluation here can be replaced with deployment to external system.
    ax_client.complete_trial(trial_index=trial_index, raw_data=train_evaluate(parameters))

df_exp = ax_client.get_trials_data_frame()
df_exp
