from scipy.stats import wilcoxon
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('seaborn-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)

markers = ['o', '^', 's', 'P', 'v', '>', '<', '*', '+', 'x', 'D', 2]


df_income = pd.read_csv('results/exp_0503/measure_perf_income_grad.csv')
df = pd.read_csv('results/exp_0503/measure_perf.csv')
df = df.append(df_income)
feature_exp = ['dataset_name', 'method', 'epsilon']
df['run'] = np.tile(np.repeat([i for i in range(10)], 30), len(df[feature_exp].drop_duplicates()))

for dataset in df['dataset_name'].unique():
    df_run = df.loc[df.dataset_name == dataset].groupby(
        ['epsilon', 'method'] + ['run']).metric.max().unstack(level=1)
    agg_metric = 'mean'
    df_run_tab = df_run.reset_index()
    df_plot = df_run_tab\
        .groupby('epsilon')\
        .agg(['std', agg_metric])\
        .reset_index()
    methods = df.method.unique()[::-1]
    for i, method in enumerate(methods):
        plt.errorbar(df_plot.epsilon,
                     df_plot[method][agg_metric],
                     yerr=df_plot[method]['std'],
                     label=f'{method}',
                     marker=markers[i])
    plt.grid()
    plt.xlabel('Epsilon')
    plt.xscale('log')
    plt.ylabel('AUC')
    plt.ylim(0., 1.01)
    plt.legend()
    plt.savefig(f'results/exp_0523/{dataset}_epsilon_perf.png', bbox_inches='tight')
    plt.close()

df_run = df.groupby(
    ['epsilon', 'method'] + ['run']).metric.max().unstack(level=1)
df_tab = df_run.loc[df_run.epsilon == 1]\
    .groupby(['dataset_name', 'method'])['metric']\
    .max()
df_tab = df_tab.unstack(level=-1)
df_tab
df_run = df.loc[df.epsilon == 1].groupby(
    ['dataset_name', 'method'] + ['run']).metric.max().unstack(level=1)

df_run_tab = df_run.reset_index()
df_run_tab.groupby(['dataset_name'])[['grad_global_ma', 'param_local_ma']].agg(
    lambda x: wilcoxon(x['grad_global_ma'], x['param_local_ma']))
wilcoxon(df_run.grad_global_ma, df_run.param_local_ma)

for dataset in df_run_tab['dataset_name'].unique():
    print(
        f'{dataset} has p-value {wilcoxon(df_run_tab.loc[df_run_tab.dataset_name == dataset].param_global_ma, df_run_tab.loc[df_run_tab.dataset_name == dataset].param_local_ma).pvalue}')
