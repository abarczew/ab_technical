from scipy.stats import wilcoxon
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('seaborn-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)

markers = ['o', '^', 's', 'P', 'v', '>', '<', '*', '+', 'x', 'D', 2]


df_local = pd.read_csv('results/exp_0515/mnist_weight_perf.csv')
df_global = pd.read_csv('results/exp_0515/mnist_grad_perf.csv')
df_other = pd.read_csv('results/exp_0515/mnist_eps_weight_perf.csv')
df_other_grad = pd.read_csv('results/exp_0515/mnist_eps_grad_perf.csv')
feature_exp = ['dataset_name', 'method', 'epsilon', 'clipping_param_norm']
epochs = 30
df = pd.DataFrame()
for dt, n_tries in zip([df_local, df_global, df_other, df_other_grad], [10, 5, 5, 5]):
    dt = dt.fillna('None')
    dt['lip_element'] = dt.apply(lambda x: 'weight'
                                 if x.clipping_param_norm != 'None'
                                 else 'gradient', axis=1)
    dt['method'] = dt.apply(lambda x:
                            f'{x.lip_element}_{x.lip_method}',
                            axis=1)
    combo_feat = len(dt[feature_exp].drop_duplicates())
    runs = list(np.repeat(list(range(n_tries)), epochs))
    dt['run'] = np.tile(runs, combo_feat)
    df = df.append(dt)

df_run = df.groupby(
    ['epsilon', 'method'] + ['run']).metric.max().unstack(level=1)
agg_metric = 'mean'
df_run_tab = df_run.reset_index()
df_plot = df_run_tab\
    .groupby('epsilon')\
    .agg(['std', agg_metric])\
    .reset_index()
methods = ['gradient_global', 'weight_global', 'weight_local'][::-1]
for i, method in enumerate(methods):
    plt.errorbar(df_plot.epsilon,
                 df_plot[method][agg_metric],
                 yerr=df_plot[method]['std'],
                 label=f'{method}',
                 marker=markers[i])
plt.grid()
plt.xlabel('Epsilon')
plt.xscale('log')
plt.ylabel('AUC')
plt.ylim(0.49, 1.01)
plt.legend()
plt.savefig(f'results/exp_0515/epsilon_perf_{agg_metric}.png', bbox_inches='tight')
plt.close()


df_run = df.groupby(
    ['epsilon', 'method'] + ['run']).sigma.mean().unstack(level=1)
agg_metric = 'mean'
df_plot = df\
    .groupby(['epsilon', 'method'])\
    .sigma\
    .agg(['std', agg_metric])\
    .unstack(level=1)\
    .reset_index()
methods = ['gradient_global', 'weight_global', 'weight_local'][::-1]
for i, method in enumerate(methods):
    plt.errorbar(df_plot.epsilon,
                 df_plot[agg_metric][method],
                 yerr=df_plot['std'][method],
                 label=f'{method}',
                 marker=markers[i])
plt.grid()
plt.xlabel('Epsilon')
plt.xscale('log')
plt.ylabel('Noise std / gradient norm')
plt.legend()
plt.savefig(f'results/exp_0515/sigma_perf_{agg_metric}.png', bbox_inches='tight')
plt.close()
