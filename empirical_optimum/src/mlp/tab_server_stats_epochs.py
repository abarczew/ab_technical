from scipy.stats import wilcoxon
import pandas as pd
import numpy as np


df_local = pd.read_csv('results/exp_0509/mnist_local_perf.csv')
df_global = pd.read_csv('results/exp_0509/mnist_global_perf.csv')
df = df_global.append(df_local)
df = df.fillna('None')
df['lip_element'] = df.apply(lambda x: 'weight'
                             if x.clipping_param_norm != 'None'
                             else 'gradient', axis=1)
df['method'] = df.apply(lambda x:
                        f'{x.lip_element}_{x.lip_method}',
                        axis=1)
feature_exp = ['dataset_name', 'method', 'epsilon']
epochs = [5,  5,  5,  5,  5,  5,  5,  5,  5,  5, 10, 10, 10, 10, 10, 10, 10,
          10, 10, 10, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 20, 20, 20, 20,
          20, 20, 20, 20, 20, 20, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 30,
          30, 30, 30, 30, 30, 30, 30, 30, 30]
runs = []
for k, v in enumerate(epochs+epochs):
    runs += [k] * v
df['run'] = np.tile(runs, len(df[feature_exp].drop_duplicates()))
df_run = df.groupby(feature_exp + ['run']).metric.max().reset_index()
df_tab = df_run.loc[df_run.epsilon == 1]\
    .groupby(['dataset_name', 'method'])['metric']\
    .max()
df_tab = df_tab.unstack(level=-1)
df_tab
df_run = df.loc[df.epsilon == 1].groupby(
    ['dataset_name', 'method'] + ['run']).metric.max().unstack(level=1)

df_run_tab = df_run.reset_index()
df_run_tab.groupby(['dataset_name'])[['grad_global_ma', 'param_local_ma']].agg(
    lambda x: wilcoxon(x['grad_global_ma'], x['param_local_ma']))
wilcoxon(df_run.grad_global_ma, df_run.param_local_ma)

for dataset in df_run_tab['dataset_name'].unique():
    print(
        f'{dataset} has p-value {wilcoxon(df_run_tab.loc[df_run_tab.dataset_name == dataset].grad_global_ma, df_run_tab.loc[df_run_tab.dataset_name == dataset].param_local_ma).pvalue}')
