from __future__ import print_function
import argparse
import torch
import torch.nn.functional as F
from torchvision import datasets, transforms
from scipy.spatial.distance import pdist
from opacus import PrivacyEngine
from opacus import GradSampleModule
from opacus.optimizers import DPOptimizer
from lip_dp.optimizers import LipDPOptimizer, get_operator_lip, get_loss_lip

batch_size = 32
test_batch_size = 32

train_kwargs = {'batch_size': batch_size}
test_kwargs = {'batch_size': test_batch_size}

transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.1307,), (0.3081,))
])
dataset1 = datasets.MNIST('../data', train=True, download=True,
                          transform=transform)
dataset2 = datasets.MNIST('../data', train=False,
                          transform=transform)
train_loader = torch.utils.data.DataLoader(dataset1, **train_kwargs)
test_loader = torch.utils.data.DataLoader(dataset2, **test_kwargs)


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

ninputs = 28 * 28
model = torch.nn.Sequential(
    torch.nn.Flatten(),
    torch.nn.Linear(ninputs, 128),
    torch.nn.Sigmoid(),
    torch.nn.Linear(128, 64),
    torch.nn.Sigmoid(),
    torch.nn.Linear(64, 32),
    torch.nn.Sigmoid(),
    torch.nn.Linear(32, 10),
).to(device)

# training parameters
epochs = 7

# loss parameters
min_margin = 1
alpha = 10

# dp parameter
epsilon = 10
delta = 1/10**3

# dp-sgd parameter
max_grad_norm = 1

# TODO: apply norm to input
input_norm = 1

optimizer = torch.optim.Adam(lr=0.001, params=model.parameters())

model = GradSampleModule(model, batch_first=True)
sample_rate = 1 / len(train_loader)
expected_batch_size = int(len(train_loader.dataset) * sample_rate)
optimizer = LipDPOptimizer(
    optimizer=optimizer,
    noise_multiplier=1.0,
    max_grad_norm=1.0,
    expected_batch_size=expected_batch_size,
)
# TODO: add dp data loader

# TODO: [nicetohave] -> get rid of persamplegrad for weight clipping

# model, optimizer, train_loader = privacy_engine.make_private_with_epsilon(
#     module=model,
#     optimizer=optimizer,
#     data_loader=train_loader,
#     epochs=epochs,
#     target_epsilon=epsilon,
#     target_delta=delta,
#     max_grad_norm=max_grad_norm,
# )

criterion = torch.nn.CrossEntropyLoss()
operators_lip = get_operator_lip(model)
loss_lip = get_loss_lip(criterion)

for epoch in range(epochs):

    m_kr, m_hm, m_acc = 0, 0, 0
    model.train()

    for step, (data, target) in enumerate(train_loader):

        data, target = data.to(device), target.to(device)
        # TODO: get rid of parameters
        # TODO: check if project gives extra dp budget?
        optimizer.project(input_norm, operators_lip, loss_lip)
        optimizer.zero_grad()
        output = model(data)
        loss = criterion(output, target)
        loss.backward()
        optimizer.step()

    # last step isnot projected
    optimizer.project(input_norm, operators_lip, loss_lip)

    # Train metrics for the current epoch
    metrics = [
        f"{k}: {v:.04f}"
        for k, v in {
            "loss": loss
        }.items()
    ]

    # Compute test loss for the current epoch
    model.eval()

    print(f"Epoch {epoch + 1}/{epochs}")
    print(" - ".join(metrics))


model.eval()
p = []
for batch, _ in train_loader:
    x = batch.numpy()
    y = model(batch.to(device)).detach().cpu().numpy()
    xd = pdist(x.reshape(batch.shape[0], -1))
    yd = pdist(y.reshape(batch.shape[0], -1))

    p.append((yd / xd).max())
print(torch.tensor(p).max())
