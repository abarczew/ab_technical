import pandas as pd
import torch

filenames_mnist = ['mnist_perf_grad_correct', 'mnist_weight_small_perf']
filenames_other = ['breast_grad_small_perf', 'breast_weight_small_perf',
                   'income_grad_small_perf', 'income_weight_small_perf',
                   'perm_grad_small_perf', 'perm_weight_small_perf',
                   'survival_grad_small_perf', 'survival_weight_small_perf']
filenames_other_2 = ['default_credit_grad_small_perf', 'default_credit_weight_small_perf',
                     'dropout_grad_small_perf', 'dropout_weight_small_perf',
                     'german_grad_small_perf', 'german_weight_small_perf',
                     'nursery_grad_small_perf', 'nursery_weight_small_perf',
                     'shuttle_grad_small_perf', 'shuttle_weight_small_perf',
                     'thyroid_grad_small_perf', 'thyroid_weight_small_perf',
                     'yeast_grad_small_perf', 'yeast_weight_small_perf']
hyper_params = ['learning_rate', 'hidden_dim',
                'clipping_param_norm']
viz_metric = 'metric'
conf_features = ['accountant', 'model', 'activation', 'n_jobs', 'test_size',
                 'batch_size', 'lasso_const', 'epsilon', 'input_norm',
                 'viz_metric', 'lip_local_method', 'items_file', 'epochs',
                 'hidden_dim', 'dataset_name', 'sample_size', 'delta',
                 'learning_rate', 'standardize', 'lip_method',
                 'clipping_grad_norm', 'clipping_param_norm', 'hyper_params',
                 'name']


def build_conf(x, k, epoch):
    if k == 'activation':
        return {'fn': torch.sigmoid,
                'activation_max': 1/4}
    elif x[k] == 'None':
        return None
    elif k == 'batch_size':
        return int(x[k])
    elif k == 'epochs':
        return int(epoch)
    else:
        return x[k]


def get_best_args(epoch):
    df_perf_all = pd.DataFrame()
    path = 'results/exp_0414'
    for filename in filenames_mnist:
        df = pd.read_csv(f'{path}/{filename}.csv')
        df_perf_all = df_perf_all.append(df)
    path = 'results/exp_0417'
    for filename in filenames_other:
        df = pd.read_csv(f'{path}/{filename}.csv')
        df_perf_all = df_perf_all.append(df)
    path = 'results/exp_0502'
    for filename in filenames_other_2:
        df = pd.read_csv(f'{path}/{filename}.csv')
        df_perf_all = df_perf_all.append(df)
    df_perf_all = df_perf_all.fillna('None')
    df_perf_all['lip_element'] = df_perf_all.apply(lambda x: 'weight'
                                                   if x.clipping_param_norm != 'None'
                                                   else 'gradient', axis=1)
    df_perf_all['method'] = df_perf_all.apply(lambda x:
                                              f'{x.lip_element}_{x.lip_method}',
                                              axis=1)
    df_perf_all = df_perf_all.reset_index(drop=True)

    df_plot = df_perf_all\
        .groupby(['dataset_name', 'method', 'epsilon'])[[viz_metric]]\
        .max()
    df_plot = df_plot.reset_index()
    metric_to_pick = df_plot.loc[(df_plot.epsilon == 1)].metric.values
    df_perf_best = df_perf_all.loc[df_perf_all.metric.isin(metric_to_pick)]
    df_perf_best['conf'] = df_perf_best.apply(
        lambda x: {k: build_conf(x, k, epoch) for k in conf_features}, axis=1)

    return list(df_perf_best['conf'].values)
