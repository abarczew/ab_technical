from scipy.stats import wilcoxon
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('seaborn-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)

markers = ['o', '^', 's', 'P', 'v', '>', '<', '*', '+', 'x', 'D', 2]


df = pd.read_csv('results/exp_0516/measure_perf.csv')
df_other = pd.read_csv('results/exp_0516/measure_perf_2.csv')
df = df.append(df_other)
df = df.fillna('None')
df['lip_element'] = df.apply(lambda x: 'weight'
                             if x.clipping_param_norm != 'None'
                             else 'gradient', axis=1)
df['method'] = df.apply(lambda x:
                        f'{x.lip_element}_{x.lip_method}',
                        axis=1)
df = df.loc[df.method.isin(['weight_global', 'weight_local'])]
feature_exp = ['dataset_name', 'method', 'epsilon', 'clipping_param_norm']
combo_feat = len(df[feature_exp].drop_duplicates())
n_tries = 5
epochs = 30
runs = list(np.repeat(list(range(n_tries)), epochs))
df['run'] = np.tile(runs, combo_feat)
df_run = df.loc[df.epsilon == 1].groupby(
    ['clipping_param_norm', 'method', 'dataset_name'] + ['run']).metric.max().unstack(level=1)


df_run_tab = df_run.reset_index()
dataset_names = df_run_tab.dataset_name.unique()
methods = ['weight_global', 'weight_local'][::-1]
for dataset in dataset_names:
    df_plot = df_run_tab.loc[df_run_tab.dataset_name == dataset]
    df_plot = df_plot\
        .groupby('clipping_param_norm')\
        .agg(['std', 'mean'])\
        .reset_index()

    for i, method in enumerate(methods):
        plt.errorbar(df_plot.clipping_param_norm,
                     df_plot[method]['mean'],
                     yerr=df_plot[method]['std'],
                     label=f'{method}',
                     marker=markers[i])
    plt.grid()
    plt.xlabel('Weight clipping norm')
    plt.ylabel('Weight clipping norm')
    plt.ylim(0.49, 1.01)
    plt.legend()
    plt.savefig(f'results/exp_0516/{dataset}_weightclip_perf_2.png', bbox_inches='tight')
    plt.close()
