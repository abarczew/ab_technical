python main_evaluation.py -e android -s  search_group_08 -r 10 --no-group
python main_evaluation.py -e breast -s  search_group_08 -r 10 --no-group
python main_evaluation.py -e default_credit -s  search_group_08 -r 10 --no-group
python main_evaluation.py -e dropout -s  search_group_08 -r 10 --no-group
python main_evaluation.py -e german -s  search_group_08 -r 10 --no-group
python main_evaluation.py -e income -s  search_group_08 -r 10 --no-group
python main_evaluation.py -e nursery -s  search_group_08 -r 10 --no-group

python main_evaluation.py -e android -s  search_group_08 -r 10 --group
python main_evaluation.py -e breast -s  search_group_08 -r 10 --group
python main_evaluation.py -e default_credit -s  search_group_08 -r 10 --group
python main_evaluation.py -e dropout -s  search_group_08 -r 10 --group
python main_evaluation.py -e german -s  search_group_08 -r 10 --group
python main_evaluation.py -e income -s  search_group_08 -r 10 --group
python main_evaluation.py -e nursery -s  search_group_08 -r 10 --group

python main_evaluation.py -e android -s  search_group_08 -r 10 --group -f true
python main_evaluation.py -e breast -s  search_group_08 -r 10 --group -f true
python main_evaluation.py -e default_credit -s  search_group_08 -r 10 --group -f true
python main_evaluation.py -e dropout -s  search_group_08 -r 10 --group -f true
python main_evaluation.py -e german -s  search_group_08 -r 10 --group -f true
python main_evaluation.py -e income -s  search_group_08 -r 10 --group -f true
python main_evaluation.py -e nursery -s  search_group_08 -r 10 --group -f true

python main_evaluation.py -e android -s  search_group_08 -r 10 --no-group -f true
python main_evaluation.py -e breast -s  search_group_08 -r 10 --no-group -f true
python main_evaluation.py -e default_credit -s  search_group_08 -r 10 --no-group -f true
python main_evaluation.py -e dropout -s  search_group_08 -r 10 --no-group -f true
python main_evaluation.py -e german -s  search_group_08 -r 10 --no-group -f true
python main_evaluation.py -e income -s  search_group_08 -r 10 --no-group -f true
python main_evaluation.py -e nursery -s  search_group_08 -r 10 --no-group -f true
