from model import load_model
import torch
import numpy as np
exp_names = ['mnist', 'fashionmnist', 'cifar', 'mlp_5', 'mlp_104']


class ClassifNet(torch.nn.Module):
    """DP NeuralNet for classification."""

    def __init__(self, input_dim, output_dim=2, hidden_dim=20,
                 activation={'fn': torch.sigmoid,
                             'activation_max': 1/2},
                 bias=False, input_norm=None):
        super().__init__()
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.input_norm = input_norm
        self.output_dim = output_dim
        self.linear_1 = torch.nn.Linear(self.input_dim, self.hidden_dim,
                                        bias=bias)
        self.linear_2 = torch.nn.Linear(self.hidden_dim, self.output_dim,
                                        bias=bias)
        self.fn_activation = activation['fn']
        self.fn_output = torch.nn.Softmax(dim=0)
        self.max_x = np.min([i for i in [np.sqrt(input_dim), self.input_norm]
                             if i is not None])
        self.max_y = 1  # data needs to be normalized
        self.n_layers = 2
        self.activation_max = activation['activation_max']

    def forward(self, x):
        hidden = self.fn_activation(self.linear_1(x))
        outputs = self.fn_output(self.linear_2(hidden))
        return outputs


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


for exp in exp_names:
    if 'mlp' in exp:
        input_dim = int(exp.split('_')[1])
        net = ClassifNet(input_dim)
    else:
        net = load_model(exp)
    print(f"Model for {exp} has {count_parameters(net)} trainable parameters.")
