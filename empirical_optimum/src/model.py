import math
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
from torch._tensor import Tensor
import torch
from torchvision.models import VGG, VGG11_Weights, vgg19, VGG19_Weights

tabular_datasets = ['income', 'android', 'breast', 'default_credit',
                    'dropout', 'german', 'nursery', 'thyroid', 'yeast']


def load_vision_model(exp_name, group=True):
    if exp_name == 'mnist':
        # net = FancyCNN()
        net = CNN(group)
        # net = CNNSig()
    elif exp_name == 'fashionmnist':
        net = FancyCNN(group)
        # net = FancyCNNSig()
    elif exp_name == 'cifar':
        # net = FancyCNNCifar(group)
        # net = ShallowVGG(group)
        # net = ShallowVGGSig()
        net = vgg19(weights=VGG19_Weights.IMAGENET1K_V1, num_classes=10)
        net.test = 'test'
        print(net.test)
    return net


def load_tabular_model(exp_name, input_dim, class_num, group):
    net = MLP(input_dim, class_num, group)
    return net


class GroupNormLip(nn.GroupNorm):
    """docstring for GroupNormLip."""

    def __init__(self, *args, **kwargs):
        super(GroupNormLip, self).__init__(*args, **kwargs)

    def forward(self, input: Tensor) -> Tensor:
        N, C, H, W = input.shape
        G = self.num_groups
        input = torch.reshape(input, [N, G, C // G, H, W])
        mean = input.mean(dim=[2, 3, 4], keepdim=True)
        invstd = torch.sqrt(input.var([2, 3, 4], unbiased=False, keepdim=True))
        # invstd = torch.maximum(invstd, torch.ones_like(invstd))
        output = (input - mean) / invstd
        output = torch.reshape(output, [N, C, H, W])
        return output


class CNN(nn.Module):
    def __init__(self, group=True) -> None:
        super().__init__()
        self.group = group
        num_groups = math.gcd(32, 20)
        num_groups = 20  # TODO: get rid of it once optimizer is updated
        self.conv1 = nn.Conv2d(1, 20, kernel_size=3, stride=1)
        self.gn1 = GroupNormLip(num_groups, 20, affine=False)
        self.fc1 = nn.Linear(8 * 8 * 20, 64)
        self.fc2 = nn.Linear(64, 10)
        self.operators_lip = [
            ('conv', lambda x: 3 * x),
            ('bias'+group*'+gn',
             lambda x: np.sqrt(20//num_groups)*3 if group else 1),
            # ('group', lambda x: 1),
            ('linear', lambda x: x),
            ('bias', lambda x: 1),
            ('linear', lambda x: x),
            ('bias', lambda x: 1)
        ]

    def forward(self, x: Tensor) -> Tensor:
        x = self.conv1(x)
        if self.group:
            x = self.gn1(x)
        x = F.relu(x)
        x = F.max_pool2d(x, 3, 3)
        x = x.view(-1, 8 * 8 * 20)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return x
        # return F.softmax(x, dim=-1)  # instead of log_softmax to compute ece


def dropout_linear_relu(dim_in, dim_out, p_drop):
    return [nn.Dropout(p_drop),
            nn.Linear(dim_in, dim_out, bias=True),
            nn.ReLU(inplace=True)]


def dropout_linear_sig(dim_in, dim_out, p_drop):
    return [nn.Dropout(p_drop),
            nn.Linear(dim_in, dim_out, bias=True),
            nn.Sigmoid()]


def conv_relu_maxp(in_channels, out_channels, ks, num_groups=0):
    if num_groups > 0:
        # num_groups = math.gcd(32, out_channels)
        layers = [nn.Conv2d(in_channels, out_channels,
                            kernel_size=ks,
                            stride=1,
                            padding=int((ks-1)/2), bias=True),
                  GroupNormLip(num_groups, out_channels, affine=False),
                  nn.ReLU(inplace=True),
                  nn.MaxPool2d(kernel_size=2)]
    else:
        layers = [nn.Conv2d(in_channels, out_channels,
                            kernel_size=ks,
                            stride=1,
                            padding=int((ks-1)/2), bias=True),
                  nn.ReLU(inplace=True),
                  nn.MaxPool2d(kernel_size=2)]
    return layers


class FancyCNN(nn.Module):

    def __init__(self, group=True, num_classes=10):
        super(FancyCNN, self).__init__()

        if group:
            num_groups = [16, 32, 64]  # instance normalization
        else:
            num_groups = [0]*3
        self.features = nn.Sequential(
            *conv_relu_maxp(1, 16, 3, num_groups[0]),
            *conv_relu_maxp(16, 32, 3, num_groups[1]),
            *conv_relu_maxp(32, 64, 3, num_groups[2])
        )

        probe_tensor = torch.zeros((1, 1, 28, 28))
        out_features = self.features(probe_tensor).view(-1)

        self.classifier = nn.Sequential(
            *dropout_linear_relu(out_features.shape[0], 128, 0.5),
            *dropout_linear_relu(128, 256, 0.5),
            nn.Linear(256, num_classes, bias=True)
        )

        self.operators_lip = [
            ('conv', lambda x: 3 * x),
            ('bias'+group*'+gn',
             lambda x: np.sqrt(16//num_groups[0])*3 if group else 1),

            ('conv', lambda x: 3 * x),
            ('bias'+group*'+gn',
             lambda x: np.sqrt(32//num_groups[1])*3 if group else 1),

            ('conv', lambda x: 3 * x),
            ('bias'+group*'+gn',
             lambda x: np.sqrt(64//num_groups[2])*3 if group else 1),

            ('linear', lambda x: x),
            ('bias', lambda x: 1),
            ('linear', lambda x: x),
            ('bias', lambda x: 1),

            ('linear', lambda x: x),
            ('bias', lambda x: 1)
        ]

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size()[0], -1)  # OR  x = x.view(-1, self.num_features)
        y = self.classifier(x)
        return y


class FancyCNNCifar(FancyCNN):

    def __init__(self, group=True, num_classes=10):
        super(FancyCNNCifar, self).__init__()

        if group:
            num_groups = [16, 32, 64]  # instance normalization
        else:
            num_groups = [0]*3

        self.features = nn.Sequential(
            *conv_relu_maxp(3, 16, 3, num_groups[0]),
            *conv_relu_maxp(16, 32, 3, num_groups[1]),
            *conv_relu_maxp(32, 64, 3, num_groups[2])
        )
        # You must compute the number of features manualy to instantiate the
        # next FC layer
        # self.num_features = 64*3*3

        # Or you create a dummy tensor for probing the size of the feature maps
        probe_tensor = torch.zeros((1, 3, 32, 32))
        out_features = self.features(probe_tensor).view(-1)

        self.classifier = nn.Sequential(
            *dropout_linear_sig(out_features.shape[0], 128, 0.5),
            *dropout_linear_sig(128, 256, 0.5),
            nn.Linear(256, num_classes, bias=True)
        )


def conv_relu(in_channels, out_channels, ks, num_groups=0):
    if num_groups > 0:
        # num_groups = math.gcd(32, out_channels)
        layers = [nn.Conv2d(in_channels, out_channels,
                            kernel_size=ks,
                            stride=1,
                            padding=1, bias=True),
                  nn.GroupNorm(num_groups, out_channels, affine=False),
                  nn.ReLU(inplace=True)]
    else:
        layers = [nn.Conv2d(in_channels, out_channels,
                            kernel_size=ks,
                            stride=1,
                            padding=1, bias=True),
                  nn.ReLU(inplace=True)]
    return layers


class ShallowVGG(nn.Module):

    def __init__(self, group=True, num_classes=10):
        super(ShallowVGG, self).__init__()
        if group:
            num_groups = [32, 64, 64, 128]
        else:
            num_groups = [0]*4
        self.features = nn.Sequential(
            *conv_relu(3, 32, 3, num_groups[0]),
            nn.AvgPool2d(kernel_size=2, stride=2),
            *conv_relu(32, 64, 3, num_groups[1]),
            nn.AvgPool2d(kernel_size=2, stride=2),
            *conv_relu(64, 64, 3, num_groups[2]),
            nn.AvgPool2d(kernel_size=2, stride=2),
            *conv_relu(64, 128, 3, num_groups[3]),
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten(start_dim=1, end_dim=-1),
            nn.Linear(128, num_classes, bias=True),
        )

        self.operators_lip = [
            ('conv', lambda x: 2 * x),
            ('bias'+group*'+gn',
             lambda x: np.sqrt(32//num_groups[0])*2 if group else 1),
            # ('group', lambda x: 1),
            ('conv', lambda x: 3 * x),
            ('bias'+group*'+gn',
             lambda x: np.sqrt(64//num_groups[1])*2 if group else 1),
            # ('group', lambda x: 1),
            ('conv', lambda x: 2 * x),
            ('bias'+group*'+gn',
             lambda x: np.sqrt(64//num_groups[2])*2 if group else 1),
            # ('group', lambda x: 1),
            ('conv', lambda x: 2 * x),
            ('bias'+group*'+gn',
             lambda x: np.sqrt(128//num_groups[3])*2 if group else 1),
            # ('group', lambda x: 1),
            ('linear', lambda x: x),
            ('bias', lambda x: 1)
        ]

    def forward(self, x):
        return self.features(x)


class VGGLip(VGG):
    def __init__(self, features, operators_lip, *arg, **kwarg):
        super(VGGLip, self).__init__(features, *arg, **kwarg)
        self.operators_lip = operators_lip + [
            ('linear', lambda x: x),
            ('activation', lambda x: 1),
            ('drop', lambda x: 1),
            ('linear', lambda x: x),
            ('activation', lambda x: 1),
            ('linear', lambda x: x)
        ]


def make_layers(cfg, batch_norm=False):
    layers = []
    in_channels = 3
    for v in cfg:
        if v == 'M':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        else:
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
            if batch_norm:
                layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
            else:
                layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = v
    return nn.Sequential(*layers)


def make_operators_lip(cfg):
    operators_lip = []
    for v in cfg:
        if v == 'M':
            operators_lip += [('pool', lambda x: 1)]
        else:
            operators_lip += [('conv', lambda x: 3 * x)]
    return operators_lip


cfg = {
    'A': [64, 'M', 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
    'B': [64, 64, 'M', 128, 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512,
          'M'],
    'D': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512, 'M',
          512, 512, 512, 'M'],
    'E': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 256, 'M', 512, 512, 512,
          512, 'M',
          512, 512, 512, 512, 'M'],
}


def vgg11_lip(weights=VGG11_Weights.IMAGENET1K_V1, progress=True, **kwargs):
    if weights is not None:
        kwargs["init_weights"] = False
        # if weights.meta["categories"] is not None:
        #     _ovewrite_named_param(kwargs, "num_classes",
        #                           len(weights.meta["categories"]))
    model = VGGLip(make_layers(cfg['A']),
                   make_operators_lip(cfg['A']),
                   **kwargs)
    if weights is not None:
        model.load_state_dict(weights.get_state_dict(progress=progress))
    return model


def attach_weigth_norm(module, weight_norm):
    if weight_norm:
        return nn.utils.parametrizations.weight_norm(module)
    else:
        return module


def linear_dropout_relu(dim_in, dim_out, p_drop, bias, num_groups):
    # num_groups = math.gcd(32, dim_out)
    return [nn.Linear(dim_in, dim_out, bias=bias),
            nn.GroupNorm(num_groups, dim_out, affine=False),
            nn.Dropout(p_drop),
            nn.ReLU(inplace=True)]


class MLP(nn.Module):

    def __init__(self, input_dim=30, num_classes=2,
                 bias=True, group=True):
        super(MLP, self).__init__()
        num_groups = 16

        if group:
            self.classifier = nn.Sequential(
                *linear_dropout_relu(input_dim, 128, 0.001, bias, num_groups),
                # *linear_dropout_relu(128, 256, 0.01),
                nn.Linear(128, num_classes, bias=bias)
            )

            self.operators_lip = [
                ('linear', lambda x: x),
                ('bias+gn', lambda x: 128//num_groups if group else 1),
                # ('group', lambda x: 1),

                ('linear', lambda x: x),
                ('bias', lambda x: 1)
            ]
        else:
            self.classifier = nn.Sequential(
                nn.Linear(input_dim, 128, bias),
                nn.Dropout(0.001),
                nn.ReLU(inplace=True),
                nn.Linear(128, num_classes, bias=bias)
            )

            self.operators_lip = [
                ('linear', lambda x: x),
                ('bias', lambda x: 1),
                # ('group', lambda x: 1),

                ('linear', lambda x: x),
                ('bias', lambda x: 1)
            ]

    def forward(self, x):
        y = self.classifier(x)
        return y
