import torch
import numpy as np
import pandas as pd
import os
import argparse

from ax.service.ax_client import AxClient
from ax.plot.pareto_utils import compute_posterior_pareto_frontier

from opacus import GradSampleModule
from opacus.data_loader import DPDataLoader

from lip_dp.train import train, evaluate_acc_auc

from utils import load_mnist, load_fashionmnist, load_cifar
from model import load_model

import matplotlib.pyplot as plt
plt.style.use('seaborn-v0_8-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)

markers = ['o', '^', 's', 'P', 'v', '>', '<', '*', '+', 'x', 'D', 2]
exp_names = ['mnist', 'fashionmnist', 'cifar']
method_names = ['grad_clip', 'weight_norm']

parser = argparse.ArgumentParser()
parser.add_argument('--exp-name', '-e', dest='exp_name', default='mnist',
                    choices=exp_names,
                    help='experiment name: ' + ' | '.join(exp_names) +
                    ' (default: mnist)')
parser.add_argument('-j', '--workers', default=0, type=int, metavar='N',
                    help='number of data loading workers (default: 0)')
# TODO: check how it's used
parser.add_argument('--runs', '-r', default=50, type=int,
                    help='number of trials to run')
parser.add_argument('--num-point', '-n', dest='num_point',
                    default=25, type=int,
                    help='number of point in the pareto frontier')
parser.add_argument('--override-epochs', '-o', dest='over_epochs',
                    default=None, type=int,
                    help='debug mode, override the stored number of epochs')
parser.add_argument('--save-dir', '-s', dest='save_dir',
                    help='The directory used to save the trained models',
                    default='save_temp', type=str)
parser.add_argument('--root-dir', '-d', dest='root_dir',
                    help='The directory with best arms',
                    default='logscale30', type=str)


def run():
    global args
    args = parser.parse_args()

    path = f"exp/{args.exp_name}/plot/"
    path_results = f"{path}{args.root_dir}/"

    if not os.path.exists(path_results):
        os.makedirs(path_results)

    torch.manual_seed(42)
    dtype = torch.float
    if args.exp_name == 'cifar':
        os.environ["PYTORCH_CUDA_ALLOC_CONF"] = "max_split_size_mb:512"
    # torch.distributed.init_process_group(
    #     init_method="env://",
    #     backend="gloo",
    # )
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    input_norm = 1
    # TODO: impact norm in transform
    if args.exp_name == 'mnist':
        train_valid_set, test_set = load_mnist(input_norm)
    elif args.exp_name == 'fashionmnist':
        train_valid_set, test_set = load_fashionmnist(input_norm)
    elif args.exp_name == 'cifar':
        train_valid_set, test_set = load_cifar(input_norm)

    results = {}
    for method in method_names:
        path = f"exp/{args.exp_name}/{method}/{args.root_dir}/results.json"

        ax_client = (
            AxClient.load_from_json_file(path)
        )

        objectives = ax_client.experiment\
            .optimization_config\
            .objective\
            .objectives
        frontier = compute_posterior_pareto_frontier(
            experiment=ax_client.experiment,
            data=ax_client.experiment.fetch_data(),
            primary_objective=objectives[0].metric,
            secondary_objective=objectives[1].metric,
            absolute_metrics=["epsilon", "accuracy"],
            num_points=args.num_point,
        )

        # evaluate frontier on testset
        test_accuracy = []
        test_auc = []
        test_epsilon = []

        for best_arm in frontier.param_dicts:
            if args.over_epochs is not None:
                best_arm['num_epochs'] = args.over_epochs
            # Initialize dataloaders
            train_loader = torch.utils.data.DataLoader(
                train_valid_set,
                batch_size=best_arm.get("batch_size"),
                pin_memory=(args.exp_name == 'cifar')
            )
            test_loader = torch.utils.data.DataLoader(
                test_set,
                batch_size=best_arm.get("batch_size"),
                pin_memory=(args.exp_name == 'cifar')
            )
            train_loader = DPDataLoader.from_data_loader(train_loader,
                                                         distributed=False)
            if args.exp_name == 'cifar':
                best_arm['max_physical_batch_size'] = 64
            for _ in range(args.runs):
                net = load_model(args.exp_name)
                best_arm["operators_lip"] = net.operators_lip
                net = GradSampleModule(net, batch_first=True)
                net, accountant = train(
                    net=net,
                    train_loader=train_loader,
                    parameters=best_arm,
                    dtype=dtype,
                    device=device,
                )
                acc, auc = evaluate_acc_auc(
                    net=net,
                    data_loader=test_loader,
                    dtype=dtype,
                    device=device,
                )
                delta = best_arm.get("delta", 1/10000)
                test_epsilon.append(accountant.get_epsilon(delta))
                test_accuracy.append(acc)
                test_auc.append(auc.item())

                results[method] = {'accuracy': test_accuracy,
                                   'auc': test_auc,
                                   'epsilon': test_auc}

    df = pd.DataFrame()
    for i, method in enumerate(method_names):
        dfplot = pd.DataFrame.from_dict(results[method])
        frontier_len = len(results[method]['accuracy']) // args.runs
        dfplot['group_idx'] = np.repeat(list(range(frontier_len)),
                                        args.runs)
        dfplot = dfplot.groupby('group_idx')\
            .agg(['std', 'mean'])\
            .reset_index()\
            .drop('group_idx', axis=1, level=0)
        dfplot['method'] = method
        df = pd.concat([df, dfplot])

        dfplot = df.loc[df.method == method]
        plt.errorbar(dfplot['epsilon']['mean'],
                     dfplot['accuracy']['mean'],
                     yerr=dfplot['accuracy']['std'],
                     label=f'{method}',
                     marker=markers[i])

    plt.grid()
    plt.xlabel('Epsilon')
    plt.xscale('log')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.savefig(f'{path_results}results.png', bbox_inches='tight')
    plt.close()

    df.to_csv(f"{path_results}trials.csv")


if __name__ == '__main__':
    run()
