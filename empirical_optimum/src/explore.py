import torch
import pickle
import matplotlib.colors as mcolors
from scipy.stats import wilcoxon
from opacus.accountants.utils import get_noise_multiplier
from netcal.presentation import ReliabilityDiagram
from netcal.metrics import ECE
from netcal.scaling import TemperatureScaling
import numpy as np
import ast
import ax
import os
import matplotlib.pyplot as plt
import pandas as pd
from ax.service.ax_client import AxClient
from ax.service.utils.instantiation import ObjectiveProperties
from ax.utils.notebook.plotting import render
from ax.plot.pareto_utils import compute_posterior_pareto_frontier
from ax.plot.pareto_frontier import plot_pareto_frontier
plt.style.use('seaborn-v0_8-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)
markers = ['o', '^', 's', 'P', 'v', '>', '<', '*', '+', 'x', 'D', 2]

methods = ['grad_clip', 'weight_norm']
exp_names = ['mnist', 'fashionmnist', 'cifar']
folder_names = ['logscale28', 'logscale29']
filename = "results.json"


exp_name = 'mnist'
method = 'grad_clip'
folder_name = 'logscale28'

for exp_name in ['mnist']:
    for method in ['grad_clip']:  # methods:
        if exp_name == 'cifar':
            folder_name = 'logscale29'
        else:
            folder_name = 'logscale28'

        path = f"exp/{exp_name}/{method}/{folder_name}/{filename}"

        ax_client = (
            AxClient.load_from_json_file(path)
        )

        objectives = ax_client.experiment.optimization_config.objective.objectives
        frontier = compute_posterior_pareto_frontier(
            experiment=ax_client.experiment,
            data=ax_client.experiment.fetch_data(),
            primary_objective=objectives[0].metric,
            secondary_objective=objectives[1].metric,
            absolute_metrics=["epsilon", "accuracy"],
            num_points=25,
        )
        print('#'*10)
        print('#'*3+exp_name)
        render(plot_pareto_frontier(frontier, CI_level=0.90))
df = pd.DataFrame({'epsilon': frontier[1]['epsilon'], 'accuracy': frontier[1]['accuracy']})
df.sort_values('epsilon')

frontier[1]['epsilon']

folder_name = 'logscale30'
for exp_name in ['fashionmnist', 'cifar']:
    for method in methods:

        path = f"exp/{exp_name}/{method}/{folder_name}/{filename}"

        ax_client = (
            AxClient.load_from_json_file(path)
        )

        objectives = ax_client.experiment.optimization_config.objective.objectives
        frontier = compute_posterior_pareto_frontier(
            experiment=ax_client.experiment,
            data=ax_client.experiment.fetch_data(),
            primary_objective=objectives[0].metric,
            secondary_objective=objectives[1].metric,
            absolute_metrics=["epsilon", "accuracy"],
            num_points=25,
        )
        print('#'*10)
        print('#'*3+exp_name)
        render(plot_pareto_frontier(frontier, CI_level=0.90))


folder_name = 'fix_16'
for exp_name in ['fashionmnist']:
    for method in methods:

        path = f"exp/{exp_name}/{method}/{folder_name}/{filename}"

        ax_client = (
            AxClient.load_from_json_file(path)
        )

        objectives = ax_client.experiment.optimization_config.objective.objectives
        frontier = compute_posterior_pareto_frontier(
            experiment=ax_client.experiment,
            data=ax_client.experiment.fetch_data(),
            primary_objective=objectives[0].metric,
            secondary_objective=objectives[1].metric,
            absolute_metrics=["epsilon", "accuracy"],
            num_points=25,
        )
        print('#'*10)
        print('#'*3+exp_name)
        render(plot_pareto_frontier(frontier, CI_level=0.90))

ax_client.experiment.search_space.parameters
folder_name = 'imbal02'
for exp_name in ['mnist']:
    for method in methods:

        path = f"exp/{exp_name}/{method}/{folder_name}/{filename}"

        ax_client = (
            AxClient.load_from_json_file(path)
        )

        objectives = ax_client.experiment.optimization_config.objective.objectives
        frontier = compute_posterior_pareto_frontier(
            experiment=ax_client.experiment,
            data=ax_client.experiment.fetch_data(),
            primary_objective=objectives[0].metric,
            secondary_objective=objectives[1].metric,
            absolute_metrics=["epsilon", "accuracy"],
            num_points=25,
        )
        print('#'*10)
        print('#'*3+exp_name)
        render(plot_pareto_frontier(frontier, CI_level=0.90))

folder_name = 'perlayer02'
for exp_name in ['mnist']:
    for method in ['weight_norm']:

        path = f"exp/{exp_name}/{method}/{folder_name}/{filename}"

        ax_client = (
            AxClient.load_from_json_file(path)
        )

        objectives = ax_client.experiment.optimization_config.objective.objectives
        frontier = compute_posterior_pareto_frontier(
            experiment=ax_client.experiment,
            data=ax_client.experiment.fetch_data(),
            primary_objective=objectives[0].metric,
            secondary_objective=objectives[1].metric,
            absolute_metrics=["epsilon", "accuracy"],
            num_points=50,
        )
        print('#'*10)
        print('#'*3+exp_name)
        render(plot_pareto_frontier(frontier, CI_level=0.90))


# plot on validation set

def plot_frontiers(path_method_list,
                   path_results,
                   perf_metric="accuracy",
                   num_points=25,
                   methods=methods):
    if not os.path.exists(path_results):
        os.makedirs(path_results)

    df = pd.DataFrame()
    for path, method in path_method_list:
        ax_client = (
            AxClient.load_from_json_file(path)
        )

        objectives = ax_client.experiment.optimization_config\
            .objective.objectives
        frontier = compute_posterior_pareto_frontier(
            experiment=ax_client.experiment,
            data=ax_client.experiment.fetch_data(),
            primary_objective=objectives[0].metric,
            secondary_objective=objectives[1].metric,
            absolute_metrics=["epsilon", perf_metric],
            num_points=num_points,
        )
        df_frontier = pd.DataFrame({'epsilon': frontier.means['epsilon'],
                                    perf_metric: frontier.means[perf_metric],
                                    'sems': frontier.sems[perf_metric],
                                    'method': method
                                    })
        df = pd.concat([df, df_frontier])

    df = df.sort_values(['method', 'epsilon'])
    df = df.loc[df.epsilon > 0]

    for i, method in enumerate(methods):
        df_plot = df.loc[df.method == method]
        plt.errorbar(df_plot.epsilon,
                     df_plot[perf_metric],
                     yerr=df_plot.sems,
                     label=f'{method}',
                     marker=markers[i])

    plt.grid()
    plt.xlabel('Epsilon')
    plt.xscale('log')
    if perf_metric == "accuracy":
        plt.ylabel("Accuracy")
    else:
        plt.ylabel("AUC")
    plt.legend()

    plt.savefig(f'{path_results}results.png', bbox_inches='tight')
    plt.close()


def plot_mono(path_method_list,
              path_results,
              perf_metric="accuracy",
              methods=methods):
    if not os.path.exists(path_results):
        os.makedirs(path_results)
    df_results = []
    for path, method in path_method_list:
        ax_client = (AxClient.load_from_json_file(path))
        df = ax.service.utils.report_utils.exp_to_df(ax_client.experiment)
        df = df.sort_values('epsilon')
        df['epsilon_group'] = df['epsilon'].map(lambda x: round(x, 1))
        df = df[['epsilon_group', 'epsilon', perf_metric]]\
            .groupby(['epsilon_group'])\
            .agg(['mean', 'sem'])
        df_results.append((df, method))
    for i, df_method in enumerate(df_results):
        df_plot, method = df_method
        plt.errorbar(df_plot['epsilon']['mean'],
                     df_plot[perf_metric]['mean'],
                     yerr=df_plot[perf_metric]['sem'],
                     label=f'{method}',
                     marker=markers[i])
    plt.grid()
    plt.xlabel('Epsilon')
    plt.xscale('log')
    if perf_metric == "accuracy":
        plt.ylabel("Accuracy")
    else:
        plt.ylabel("AUC")
    plt.legend()

    plt.savefig(f'{path_results}results.png', bbox_inches='tight')
    plt.close()


# balanced mnist
exp_name = 'mnist'
filename = 'results.json'
path_grad = (f"exp/{exp_name}/grad_clip/logscale28/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/perlayer02/{filename}",
               'weight_norm')
path_method_list = [path_grad, path_weight]


ax_client = (
    AxClient.load_from_json_file(path_grad[0])
)
ax_client.experiment.search_space.parameters
num_points = 10
objectives = ax_client.experiment.optimization_config\
    .objective.objectives
frontier = compute_posterior_pareto_frontier(
    experiment=ax_client.experiment,
    data=ax_client.experiment.fetch_data(),
    primary_objective=objectives[0].metric,
    secondary_objective=objectives[1].metric,
    absolute_metrics=["epsilon", "accuracy"],
    num_points=num_points,
)
frontier.param_dicts
ax.storage.json_store.encoders.experiment_to_dict(ax_client.experiment)


plot_frontiers(path_method_list, f"exp/{exp_name}/plot/balanced/")

# mnist mono
exp_name = 'mnist'
filename = 'results.json'
folder_name = 'mono09'
path_grad = (f"exp/{exp_name}/grad_clip/{folder_name}/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/{folder_name}/{filename}",
               'weight_norm')
path_method_list = [path_grad, path_weight]

plot_mono(path_method_list, f"exp/{exp_name}/plot/{folder_name}/")

# fashionmnist
exp_name = 'fashionmnist'
filename = 'results.json'
folder_name = 'logscale30'
path_grad = (f"exp/{exp_name}/grad_clip/{folder_name}/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/{folder_name}/{filename}",
               'weight_norm')
path_method_list = [path_grad, path_weight]

plot_frontiers(path_method_list, f"exp/{exp_name}/plot/{folder_name}/")

# fashionmnist
exp_name = 'fashionmnist'
filename = 'results.json'
folder_name = 'fix_16'
path_grad = (f"exp/{exp_name}/grad_clip/{folder_name}/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/fix_18_weight/{filename}",  # fix_18_weight is same as fix_16 but corrected only for weight_norm
               'weight_norm')
path_method_list = [path_grad, path_weight]

plot_frontiers(path_method_list, f"exp/{exp_name}/plot/{folder_name}/")

exp_name = 'mnist'
filename = 'results.json'
folder_name = 'fix_16'
path_grad = (f"exp/{exp_name}/grad_clip/{folder_name}/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/fix_18_weight/{filename}",
               'weight_norm')
path_method_list = [path_grad, path_weight]

plot_frontiers(path_method_list, f"exp/{exp_name}/plot/{folder_name}/")

# TODO: retrieve best params and compute acc and ece for all epsilons, 10 times

exp_name = 'mnist'
filename = 'results.json'
folder_name = 'fix_perlayer_18_weight'
path_grad = (f"exp/{exp_name}/grad_clip/fix_16/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/{folder_name}/{filename}",
               'weight_norm')
path_method_list = [path_grad, path_weight]

plot_frontiers(path_method_list, f"exp/{exp_name}/plot/{folder_name}/", num_points=10)

exp_name = 'mnist'
filename = 'results.json'
folder_name = 'search_biasnorm_06'
path = f"exp/{exp_name}/weight_norm/{folder_name}/{filename}"
perf_metric = 'accuracy'
method = 'weight_norm'
num_points = 5
ax_client = (
    AxClient.load_from_json_file(path)
)

objectives = ax_client.experiment.optimization_config\
    .objective.objectives
frontier = compute_posterior_pareto_frontier(
    experiment=ax_client.experiment,
    data=ax_client.experiment.fetch_data(),
    primary_objective=objectives[0].metric,
    secondary_objective=objectives[1].metric,
    absolute_metrics=["epsilon", perf_metric],
    num_points=num_points,
)
df_frontier = pd.DataFrame({'epsilon': frontier.means['epsilon'],
                            perf_metric: frontier.means[perf_metric],
                            'sems': frontier.sems[perf_metric],
                            'method': method
                            })

frontier
path = f"exp/fashionmnist/weight_norm/search_biasnorm_06/{filename}"
ax_client = (
    AxClient.load_from_json_file(path)
)
frontier = ax_client.get_pareto_optimal_parameters()
frontier


exp_name = 'cifar'
filename = 'results.json'
path_grad = (f"exp/{exp_name}/grad_clip/fix_18_weight/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/search_biasnorm_07/{filename}",
               'weight_norm')
path_method_list = [path_grad, path_weight]

plot_frontiers(path_method_list, f"exp/{exp_name}/plot/search_biasnorm_07/", perf_metric="accuracy")


# mnist evaluations
exp_name = 'mnist'
filename = 'trials.csv'
folder_name = 'fix_18_weight'
path_grad = (f"exp/{exp_name}/grad_clip/{folder_name}/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/{folder_name}/{filename}",
               'weight_norm')
path_method_list = [path_grad, path_weight]
path_results = f"exp/{exp_name}/plot/{folder_name}/"


def quantile_up(x, q=0.75):
    return x.quantile(q)


def quantile_low(x, q=0.25):
    return x.quantile(q)


linestyles = ['-', '--']
colors = ['tab:blue', 'tab:red']


def plot_evaluation_group(exp_name,
                          filename='trials.csv',
                          folder_weight='search_biasnorm_08',
                          folder_weight_group='search_groupmulbias_09',
                          folder_grad='search_biasnorm_08',
                          folder_grad_group='search_group_08',
                          metric="accuracy",
                          methods=methods,
                          agg='mean'):
    groups = ['group', 'nogroup']
    paths = {'grad_clip': {'nogroup': f"exp/{exp_name}/grad_clip/{folder_grad}/{filename}",
                           'group': f"exp/{exp_name}/grad_clip/{folder_grad_group}/{filename}"},
             'weight_norm': {'nogroup': f"exp/{exp_name}/weight_norm/{folder_weight}/{filename}",
                             'group': f"exp/{exp_name}/weight_norm/{folder_weight_group}/{filename}"}}
    path_results = f"exp/{exp_name}/plot/{folder_weight_group}_vs_noema/"

    if not os.path.exists(path_results):
        os.makedirs(path_results)

    for i, method in enumerate(methods):
        if method == 'grad_clip':
            method_name = 'DP-SGD'
        else:
            method_name = 'Lip-DP-SGD'
        for j, group in enumerate(groups):
            if group == 'group':
                group_name = 'w/ Parameter averaging'
            else:
                group_name = 'w/o Parameter averaging'
            df_evaluation = pd.read_csv(paths[method][group])
            df_plot = df_evaluation[['noise_multiplier', 'epsilon', metric]].groupby(
                'noise_multiplier').agg([agg, 'std']).reset_index()

            plt.errorbar(df_plot['epsilon'][agg],
                         df_plot[metric][agg],
                         yerr=df_plot[metric]['std'],
                         # yerr=[df_plot[metric]['median'] - df_plot[metric]['quantile_low'],
                         #       df_plot[metric]['quantile_up'] - df_plot[metric]['median']],
                         label=f'{method_name} {group_name}',
                         marker=markers[i],
                         linestyle=linestyles[j],
                         color=mcolors.TABLEAU_COLORS[colors[i]])

    plt.grid()
    plt.xlabel('Epsilon')
    plt.xscale('log')
    if metric == "accuracy":
        plt.ylabel("Accuracy")
    elif metric == 'auc':
        plt.ylabel("AUC")
    elif metric == 'ece':
        plt.ylabel('ECE')
    plt.legend(loc='lower right')

    plt.savefig(f'{path_results}{metric}_results.png', bbox_inches='tight')
    plt.close()


pd.Series([5 - 4])

plot_evaluation_group('mnist',
                      filename='trials.csv',
                      folder_weight='search_biasnorm_08',
                      folder_weight_group='ema_23_nogroup',
                      folder_grad='search_biasnorm_08',
                      folder_grad_group='ema_23_nogroup',
                      metric="accuracy",
                      methods=methods,
                      agg='mean')

plot_evaluation_group('fashionmnist',
                      filename='trials.csv',
                      folder_weight='search_biasnorm_06',
                      folder_weight_group='ema_23_nogroup',
                      folder_grad='fix_18_weight',
                      folder_grad_group='ema_23_nogroup',
                      metric="accuracy",
                      methods=methods,
                      agg='mean')

plot_evaluation_group('cifar',
                      filename='trials.csv',
                      folder_weight='search_mulbias_10',
                      folder_weight_group='ema_23_nogroup',
                      folder_grad='fix_18_weight',
                      folder_grad_group='ema_23_nogroup',
                      metric="accuracy",
                      methods=methods,
                      agg='mean')

plot_evaluation_group('mnist')
plot_evaluation_group('fashionmnist', folder_weight='search_biasnorm_06',
                      folder_weight_group='search_groupmulbias_09',
                      folder_grad='fix_18_weight',
                      folder_grad_group='search_group_08')
plot_evaluation_group('cifar', folder_weight='search_mulbias_10',
                      folder_weight_group='search_groupmulbias_09',
                      folder_grad='fix_18_weight',
                      folder_grad_group='search_group_08')


def plot_evaluation(exp_name,
                    filename='trials.csv',
                    folder_weight='search_biasnorm_06',
                    folder_grad='fix_18_weight',
                    metric="accuracy",
                    methods=methods,
                    agg='mean'):
    path_grad = (f"exp/{exp_name}/grad_clip/{folder_grad}/{filename}",
                 'grad_clip')
    path_weight = (f"exp/{exp_name}/weight_norm/{folder_weight}/{filename}",
                   'weight_norm')
    path_method_list = [path_grad, path_weight]
    path_results = f"exp/{exp_name}/plot/{folder_weight}/"

    if not os.path.exists(path_results):
        os.makedirs(path_results)

    for i, path_method in enumerate(path_method_list):
        path, method = path_method
        if method == 'grad_clip':
            method_name = 'DP-SGD'
        else:
            method_name = 'Lip-DP-SGD'
        df_evaluation = pd.read_csv(path)
        df_plot = df_evaluation[['noise_multiplier', 'epsilon', metric]].groupby(
            'noise_multiplier').agg([agg, 'std', quantile_low, quantile_up]).reset_index()

    # return df_evaluation, df_plot

        plt.errorbar(df_plot['epsilon'][agg],
                     df_plot[metric][agg],
                     yerr=df_plot[metric]['std'],
                     # yerr=[df_plot[metric]['median'] - df_plot[metric]['quantile_low'],
                     #       df_plot[metric]['quantile_up'] - df_plot[metric]['median']],
                     label=f'{method_name}',
                     marker=markers[i])

    plt.grid()
    plt.xlabel('Epsilon')
    plt.xscale('log')
    if metric == "accuracy":
        plt.ylabel("Accuracy")
    elif metric == 'auc':
        plt.ylabel("AUC")
    elif metric == 'ece':
        plt.ylabel('ECE')
    plt.legend(loc='lower right')

    plt.savefig(f'{path_results}{metric}_results.png', bbox_inches='tight')
    plt.close()


plot_evaluation('mnist', metric='accuracy', folder_weight='search_groupmulbias_09',
                folder_grad='search_group_08')
plot_evaluation('mnist', metric="ece", folder_weight='search_groupmulbias_09',
                folder_grad='search_group_08')
plot_evaluation('fashionmnist', metric='accuracy', folder_weight='search_groupmulbias_09',
                folder_grad='search_group_08')
plot_evaluation('fashionmnist', metric='ece', folder_weight='search_groupmulbias_09',
                folder_grad='search_group_08')

plot_evaluation('cifar', metric="accuracy", folder_weight='search_groupmulbias_09',
                folder_grad='search_group_08')
plot_evaluation('cifar', metric="ece", folder_weight='search_groupmulbias_09',
                folder_grad='search_group_08')

plot_evaluation('android', metric='accuracy',
                folder_weight='search_group_08',
                folder_grad='search_group_08')
plot_evaluation('android', metric='ece', folder_weight='search_group_08',
                folder_grad='search_group_08')
plot_evaluation('breast', metric='accuracy', folder_weight='search_group_08')
plot_evaluation('breast', metric='ece', folder_weight='search_group_08')
plot_evaluation('default_credit', metric='accuracy', folder_weight='search_group_08')
plot_evaluation('default_credit', metric='ece', folder_weight='search_group_08')
plot_evaluation('dropout', metric='accuracy', folder_weight='search_group_08',
                folder_grad='search_group_08')
plot_evaluation('dropout', metric='ece', folder_weight='search_group_08',
                folder_grad='search_group_08')
plot_evaluation('german', metric='accuracy', folder_weight='search_group_08')
plot_evaluation('german', metric='ece', folder_weight='search_group_08')
plot_evaluation('income', metric='accuracy', folder_weight='search_group_08',
                folder_grad='search_group_08')
plot_evaluation('income', metric='ece', folder_weight='search_group_08',
                folder_grad='search_group_08')
plot_evaluation('nursery', metric='accuracy', folder_weight='search_group_08')
plot_evaluation('nursery', metric='ece', folder_weight='search_group_08')
plot_evaluation('thyroid', metric='accuracy', folder_weight='search_group_08')
plot_evaluation('thyroid', metric='ece', folder_weight='search_group_08')


#######################
#######################
### statiscal tests ###
#######################
#######################

def get_evaluation(exp_name,
                   filename='trials.csv',
                   folder_weight='search_group_08',
                   folder_grad='search_group_08',
                   metric="accuracy",
                   methods=methods,
                   agg='mean'):
    path_grad = (f"exp/{exp_name}/grad_clip/{folder_grad}/",
                 'grad_clip')
    path_weight = (f"exp/{exp_name}/weight_norm/{folder_weight}/",
                   'weight_norm')
    path_method_list = [path_grad, path_weight]
    path_results = f"exp/{exp_name}/plot/{folder_weight}/"

    if not os.path.exists(path_results):
        os.makedirs(path_results)

    df = pd.DataFrame()
    for i, path_method in enumerate(path_method_list):
        path, method = path_method
        if method == 'grad_clip':
            method_name = 'DP-SGD'
        else:
            method_name = 'Lip-DP-SGD'
        df_evaluation = pd.read_csv(path+filename)
        df_evaluation['method'] = method_name
        df = pd.concat([df, df_evaluation])

    return df


def print_stats(df_sgd, df_lip, metrics=['accuracy', 'ece', 'epsilon'], r=3):
    for metric in metrics:
        metric_sgd = df_sgd[metric].mean()
        metric_lip = df_lip[metric].mean()
        significativity = wilcoxon(df_sgd[metric], df_lip[metric]).pvalue <= 0.05
        print(f'{metric}: DP-SGD={round(metric_sgd, r)}, Lip-DP-SGD={round(metric_lip, r)}, significativity={significativity}')


def pick_one_evaluation(exp_name, noise_sgd, noise_lip,
                        folder_group='instance_plaingn_28',
                        folder_nogroup='instance_plaingn_28_nogroup',
                        folder_fix='instance_plaingn_28_fix',
                        folder_fix_nogroup='instance_plaingn_28_nogroup_fix'):
    path = f"exp/{exp_name}/grad_clip/search_group_08/trials.csv"
    df = pd.read_csv(path)
    df_sgd_group = df.loc[df.noise_multiplier == noise_sgd]

    path = f"exp/{exp_name}/grad_clip/search_nogroup_10/trials.csv"
    df = pd.read_csv(path)
    df_sgd_nogroup = df.loc[df.noise_multiplier == noise_sgd]

    path = f"exp/{exp_name}/weight_norm/{folder_group}/trials.csv"
    df = pd.read_csv(path)
    df_lip_group = df.loc[df.noise_multiplier == noise_lip]

    path = f"exp/{exp_name}/weight_norm/{folder_nogroup}/trials.csv"
    df = pd.read_csv(path)
    df_lip_nogroup = df.loc[df.noise_multiplier == noise_lip]

    path = f"exp/{exp_name}/weight_norm/{folder_fix}/trials.csv"
    df = pd.read_csv(path)
    df_lip_fix = df.loc[df.noise_multiplier == noise_lip]

    path = f"exp/{exp_name}/weight_norm/{folder_fix_nogroup}/trials.csv"
    df = pd.read_csv(path)
    df_lip_fix_nogroup = df.loc[df.noise_multiplier == noise_lip]

    return df_sgd_group, df_sgd_nogroup, df_lip_group, df_lip_nogroup, df_lip_fix, df_lip_fix_nogroup


# income
df = get_evaluation('income')
df[['noise_multiplier', 'epsilon']].drop_duplicates().sort_values('epsilon')
df_sgd_group, df_sgd_nogroup, df_lip_group, df_lip_nogroup, df_lip_fix, df_lip_fix_nogroup = pick_one_evaluation(
    'income', 1.5, 1.5)
print('GROUP')
print_stats(df_sgd_group, df_lip_group)
print('NOGROUP')
print_stats(df_sgd_nogroup, df_lip_nogroup)
print('FIX')
print_stats(df_sgd_group, df_lip_fix, metrics=['accuracy', 'ece'])
print('FIX_NOGROUP')
print_stats(df_sgd_group, df_lip_fix_nogroup, metrics=['accuracy', 'ece'])

# android
df = get_evaluation('android')
df[['noise_multiplier', 'epsilon']].drop_duplicates().sort_values('epsilon')
df_sgd_group, df_sgd_nogroup, df_lip_group, df_lip_nogroup, df_lip_fix, df_lip_fix_nogroup = pick_one_evaluation(
    'android', 0.7, 0.7)
print('GROUP')
print_stats(df_sgd_group, df_lip_group)
print('NOGROUP')
print_stats(df_sgd_nogroup, df_lip_nogroup)
print('FIX')
print_stats(df_sgd_group, df_lip_fix, metrics=['accuracy', 'ece'])
print('FIX_NOGROUP')
print_stats(df_sgd_group, df_lip_fix_nogroup, metrics=['accuracy', 'ece'])

df = get_evaluation('breast')
df[['noise_multiplier', 'epsilon']].drop_duplicates().sort_values('epsilon')
df_sgd_group, df_sgd_nogroup, df_lip_group, df_lip_nogroup, df_lip_fix, df_lip_fix_nogroup = pick_one_evaluation(
    'breast', 1.5, 1.5)
print('GROUP')
print_stats(df_sgd_group, df_lip_group)
print('NOGROUP')
print_stats(df_sgd_nogroup, df_lip_nogroup)
print('FIX')
print_stats(df_sgd_group, df_lip_fix, metrics=['accuracy', 'ece'])
print('FIX_NOGROUP')
print_stats(df_sgd_group, df_lip_fix_nogroup, metrics=['accuracy', 'ece'])

df = get_evaluation('default_credit')
df[['noise_multiplier', 'epsilon']].drop_duplicates().sort_values('epsilon')
df_sgd_group, df_sgd_nogroup, df_lip_group, df_lip_nogroup, df_lip_fix, df_lip_fix_nogroup = pick_one_evaluation(
    'default_credit', 0.7, 0.9)
print('GROUP')
print_stats(df_sgd_group, df_lip_group)
print('NOGROUP')
print_stats(df_sgd_nogroup, df_lip_nogroup)
print('FIX')
print_stats(df_sgd_group, df_lip_fix, metrics=['accuracy', 'ece'])
print('FIX_NOGROUP')
print_stats(df_sgd_group, df_lip_fix_nogroup, metrics=['accuracy', 'ece'])

df = get_evaluation('dropout')
df[['noise_multiplier', 'epsilon']].drop_duplicates().sort_values('epsilon')
df_sgd_group, df_sgd_nogroup, df_lip_group, df_lip_nogroup, df_lip_fix, df_lip_fix_nogroup = pick_one_evaluation(
    'dropout', 0.9, 1.5)
print('GROUP')
print_stats(df_sgd_group, df_lip_group)
print('NOGROUP')
print_stats(df_sgd_nogroup, df_lip_nogroup)
print('FIX')
print_stats(df_sgd_group, df_lip_fix, metrics=['accuracy', 'ece'])
print('FIX_NOGROUP')
print_stats(df_sgd_group, df_lip_fix_nogroup, metrics=['accuracy', 'ece'])

df = get_evaluation('german')
df[['noise_multiplier', 'epsilon']].drop_duplicates().sort_values('epsilon')
df_sgd_group, df_sgd_nogroup, df_lip_group, df_lip_nogroup, df_lip_fix, df_lip_fix_nogroup = pick_one_evaluation(
    'german', 1.5, 1.5)
print('GROUP')
print_stats(df_sgd_group, df_lip_group)
print('NOGROUP')
print_stats(df_sgd_nogroup, df_lip_nogroup)
print('FIX')
print_stats(df_sgd_group, df_lip_fix, metrics=['accuracy', 'ece'])
print('FIX_NOGROUP')
print_stats(df_sgd_group, df_lip_fix_nogroup, metrics=['accuracy', 'ece'])

df = get_evaluation('income')
df[['noise_multiplier', 'epsilon']].drop_duplicates().sort_values('epsilon')
df_sgd_group, df_sgd_nogroup, df_lip_group, df_lip_nogroup, df_lip_fix, df_lip_fix_nogroup = pick_one_evaluation(
    'income', 0.9, 0.7)
print('GROUP')
print_stats(df_sgd_group, df_lip_group)
print('NOGROUP')
print_stats(df_sgd_nogroup, df_lip_nogroup)
print('FIX')
print_stats(df_sgd_group, df_lip_fix, metrics=['accuracy', 'ece'])
print('FIX_NOGROUP')
print_stats(df_sgd_group, df_lip_fix_nogroup, metrics=['accuracy', 'ece'])

df = get_evaluation('nursery')
df[['noise_multiplier', 'epsilon']].drop_duplicates().sort_values('epsilon')
df_sgd_group, df_sgd_nogroup, df_lip_group, df_lip_nogroup, df_lip_fix, df_lip_fix_nogroup = pick_one_evaluation(
    'nursery', 0.9, 0.9)
print('GROUP')
print_stats(df_sgd_group, df_lip_group)
print('NOGROUP')
print_stats(df_sgd_nogroup, df_lip_nogroup)
print('FIX')
print_stats(df_sgd_group, df_lip_fix, metrics=['accuracy', 'ece'])
print('FIX_NOGROUP')
print_stats(df_sgd_group, df_lip_fix_nogroup, metrics=['accuracy', 'ece'])

df = get_evaluation('thyroid')
df[['noise_multiplier', 'epsilon']].drop_duplicates().sort_values('epsilon')
df_sgd_group, df_sgd_nogroup, df_lip_group, df_lip_nogroup, df_lip_fix = pick_one_evaluation(
    'income', 0.7, 0.7)
print('GROUP')
print_stats(df_sgd_group, df_lip_group)
print('NOGROUP')
print_stats(df_sgd_nogroup, df_lip_nogroup)
print('FIX')
print_stats(df_lip_group, df_lip_fix, metrics=['accuracy', 'ece'])

# clip factor analysis
exp_name = 'default_credit'
method = 'grad_clip'
path = f"exp/{exp_name}/{method}/clip_factors/results.pck"
with open(path, 'rb') as fp:
    clip_factors = pickle.load(fp)

pd.Series(clip_factors)[-100:].plot()
pd.Series((np.array(clip_factors)[::-1] > 0.99).cumsum() / np.arange(1, len(clip_factors)+1)).plot()
len(clip_factors)


def moving_average(x, w):
    return np.convolve(x, np.ones(w), 'valid') / w


pd.Series(moving_average(clip_factors, 10000)).plot()


exp_name = 'income'
method = 'grad_clip'
path = f"exp/{exp_name}/{method}/clip_factors/results_diff.pck"
with open(path, 'rb') as fp:
    clip_diff = pickle.load(fp)
path = f"exp/{exp_name}/{method}/clip_factors/results_grad.pck"
with open(path, 'rb') as fp:
    clip_grad = pickle.load(fp)

path_results = f'exp/{exp_name}/plot/clipping/'
if not os.path.exists(path_results):
    os.makedirs(path_results)


def get_avg_grad(grads, bin_size=500):
    clip = {}
    for j in range(4):
        clip[j] = []
        for i in range(len(grads[j::4])):
            if i % bin_size == 0:
                norm_diff = torch.stack(grads[j::4][i:i+bin_size],
                                        dim=0).mean(axis=0).norm(2, dim=-1)
                clip[j].append(norm_diff)

    diffs = []
    for i in range(4):
        diff = torch.stack(clip[i], dim=0)
        if len(diff.shape) == 1:
            diff = diff[:, None]
        diffs.append(diff)
    return torch.cat(diffs, dim=1).mean(axis=1)


diffs = pd.Series(get_avg_grad(clip_diff))
grads = pd.Series(get_avg_grad(clip_grad))


plt.plot(diffs.index,
         diffs,
         label='||g - clip(g)||',
         marker=markers[0],
         color=mcolors.TABLEAU_COLORS[colors[0]])
plt.plot(diffs.index,
         grads,
         label='||clip(g)||',
         marker=markers[1],
         color=mcolors.TABLEAU_COLORS[colors[1]])
plt.grid()
plt.legend()
plt.xlabel('Iteration')
plt.ylabel('Norm')
plt.savefig(f'{path_results}results.png', bbox_inches='tight')
plt.close()
pd.Series(torch.cat(diffs, dim=1).mean(axis=1)).plot()


# runtime
exp_name = 'mnist'
folder_name = 'runtime_22'
filename = 'trials.csv'
path_grad = f"exp/{exp_name}/grad_clip/runtime/{folder_name}/{filename}"
path_weight = f"exp/{exp_name}/weight_norm/runtime/{folder_name}/{filename}"
runtime_grad = pd.read_csv(path_grad)
runtime_weight = pd.read_csv(path_weight)
plot_path = f"exp/{exp_name}/runtime/"
if not os.path.exists(plot_path):
    os.makedirs(plot_path)
plt.plot(runtime_grad.batch_size,
         runtime_grad.time,
         label='DP-SGD',
         marker=markers[0],
         color=mcolors.TABLEAU_COLORS[colors[0]])
plt.plot(runtime_grad.batch_size,
         runtime_weight.time,
         label='Lip-DP-SGD',
         marker=markers[1],
         color=mcolors.TABLEAU_COLORS[colors[1]])
plt.grid()
plt.legend()
plt.xlabel('Batch size')
plt.ylabel('Runtime (s)')
plt.savefig(f'{plot_path}results.png', bbox_inches='tight')
plt.close()

# fashionmnist mono
exp_name = 'fashionmnist'
filename = 'results.json'
folder_name = 'mono09'
path_grad = (f"exp/{exp_name}/grad_clip/{folder_name}/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/{folder_name}/{filename}",
               'weight_norm')
path_method_list = [path_grad, path_weight]


plot_mono(path_method_list, f"exp/{exp_name}/plot/{folder_name}/")


# fashionmnist mono vs non mono
exp_name = 'fashionmnist'
filename = 'results.json'
folder_name = 'mono09'
perf_metric = 'accuracy'
path_grad = (f"exp/{exp_name}/grad_clip/{folder_name}/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/{folder_name}/{filename}",
               'weight_norm')
path_weight_front = (f"exp/{exp_name}/weight_norm/logscale09/{filename}",
                     'weight_norm')
path_method_list = [path_grad, path_weight]

ax_client = (
    AxClient.load_from_json_file(path_grad[0])
)
df_grad = ax.service.utils.report_utils.exp_to_df(ax_client.experiment)
df_grad = df_grad.sort_values('epsilon')
df_grad['epsilon_group'] = df_grad['epsilon'].map(lambda x: round(x, 1))
df_grad = df_grad[['epsilon_group', 'epsilon', perf_metric]]\
    .groupby(['epsilon_group'])\
    .agg(['mean', 'sem'])

ax_client = (
    AxClient.load_from_json_file(path_weight_front[0])
)

objectives = ax_client.experiment.optimization_config\
    .objective.objectives
frontier = compute_posterior_pareto_frontier(
    experiment=ax_client.experiment,
    data=ax_client.experiment.fetch_data(),
    primary_objective=objectives[0].metric,
    secondary_objective=objectives[1].metric,
    absolute_metrics=["epsilon", perf_metric],
    num_points=25,
)
df_weight_front = pd.DataFrame({'epsilon': frontier.means['epsilon'],
                                perf_metric: frontier.means[perf_metric],
                                'sem': frontier.sems[perf_metric]
                                })
df_weight_front = df_weight_front.sort_values('epsilon')

ax_client = (
    AxClient.load_from_json_file(path_weight[0])
)
df_weight = ax.service.utils.report_utils.exp_to_df(ax_client.experiment)
df_weight = df_weight.sort_values('epsilon')
df_weight['epsilon_group'] = df_weight['epsilon'].map(lambda x: round(x, 1))
df_weight_mono = df_weight[['epsilon_group', 'epsilon', perf_metric]]\
    .groupby(['epsilon_group'])\
    .agg(['mean', 'sem'])

path_results = f'exp/{exp_name}/plot/monovsfront/'
if not os.path.exists(path_results):
    os.makedirs(path_results)
df_plot = df_weight_front.loc[df_weight_front.epsilon < 12]\
    .drop_duplicates()
plt.errorbar(df_grad['epsilon']['mean'],
             df_grad[perf_metric]['mean'],
             yerr=df_grad[perf_metric]['sem'],
             label=f'grad_clip',
             marker=markers[0])
plt.errorbar(df_plot['epsilon'],
             df_plot[perf_metric],
             yerr=df_plot['sem'],
             label=f'weight_norm',
             marker=markers[1])
plt.grid()
plt.xlabel('Epsilon')
plt.xscale('log')
if perf_metric == "accuracy":
    plt.ylabel("Accuracy")
else:
    plt.ylabel("AUC")
plt.legend()

plt.savefig(f'{path_results}results.png', bbox_inches='tight')
plt.close()

# cifar
exp_name = 'cifar'
filename = 'results.json'
folder_name = 'logscale09'
path_grad = (f"exp/{exp_name}/grad_clip/logscale30/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/{folder_name}/{filename}",
               'weight_norm')
path_method_list = [path_grad, path_weight]

ax_client = (
    AxClient.load_from_json_file(path_grad[0])
)

objectives = ax_client.experiment.optimization_config\
    .objective.objectives
frontier = compute_posterior_pareto_frontier(
    experiment=ax_client.experiment,
    data=ax_client.experiment.fetch_data(),
    primary_objective=objectives[0].metric,
    secondary_objective=objectives[1].metric,
    absolute_metrics=["epsilon", perf_metric],
    num_points=25,
)
df_grad_front = pd.DataFrame({'epsilon': frontier.means['epsilon'],
                              perf_metric: frontier.means[perf_metric],
                              'sem': frontier.sems[perf_metric]
                              })
# loscale30
df_grad_front = df_grad_front.sort_values('epsilon')


ax_client = (
    AxClient.load_from_json_file(path_weight[0])
)

objectives = ax_client.experiment.optimization_config\
    .objective.objectives
frontier = compute_posterior_pareto_frontier(
    experiment=ax_client.experiment,
    data=ax_client.experiment.fetch_data(),
    primary_objective=objectives[0].metric,
    secondary_objective=objectives[1].metric,
    absolute_metrics=["epsilon", perf_metric],
    num_points=25,
)
df_weight_front = pd.DataFrame({'epsilon': frontier.means['epsilon'],
                                perf_metric: frontier.means[perf_metric],
                                'sem': frontier.sems[perf_metric]
                                })

df_weight_front = df_weight_front.sort_values('epsilon').iloc[:-2]


path_weight = (f"exp/{exp_name}/weight_norm/verylow10/{filename}",
               'weight_norm')

ax_client = (
    AxClient.load_from_json_file(path_weight[0])
)
df_low = ax.service.utils.report_utils.exp_to_df(ax_client.experiment)
df_low = df_low.sort_values('epsilon')
df_low['epsilon_group'] = df_low['epsilon'].map(lambda x: round(x, 0))
df_low = df_low[['epsilon_group', 'epsilon', perf_metric]]\
    .groupby(['epsilon_group'])\
    .agg(['max', 'mean', 'sem'])
df_low = df_low.iloc[1:4]
df_plot_low = pd.DataFrame({'epsilon': df_low['epsilon']['mean'],
                            'accuracy': df_low['accuracy']['max'],
                           'sem': df_low['accuracy']['sem']})
df_plot_low = df_plot_low.reset_index(drop=True)
df_weight_front = pd.concat([df_weight_front, df_plot_low])
df_weight_front = df_weight_front.reset_index()
df_weight_front = df_weight_front.loc[~df_weight_front.index.isin([1, 24])]


path_results = f'exp/{exp_name}/plot/logscale11/'
if not os.path.exists(path_results):
    os.makedirs(path_results)
plt.errorbar(df_grad_front['epsilon'],
             df_grad_front[perf_metric],
             yerr=df_grad_front['sem'],
             label=f'DP-SGD',
             marker=markers[0])
plt.errorbar(df_weight_front['epsilon'],
             df_weight_front[perf_metric],
             yerr=df_weight_front['sem'],
             label=f'LipDP-SGD',
             marker=markers[1])
plt.grid()
plt.xlabel('Epsilon')
plt.xscale('log')
if perf_metric == "accuracy":
    plt.ylabel("Accuracy")
else:
    plt.ylabel("AUC")
plt.legend()

plt.savefig(f'{path_results}results.png', bbox_inches='tight')
plt.close()


plot_frontiers(path_method_list, f"exp/{exp_name}/plot/{folder_name}/")


# fashionmnist perlayer sigmoid
exp_name = 'fashionmnist'
filename = 'results.json'
folder_name = 'perlayersig05'
path_grad = (f"exp/{exp_name}/grad_clip/{folder_name}/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/{folder_name}/{filename}",
               'weight_norm')
path_method_list = [path_grad, path_weight]

plot_frontiers(path_method_list, f"exp/{exp_name}/plot/{folder_name}/")

# mnist perlayer sigmoid vs all layer sig
exp_name = 'mnist'
filename = 'results.json'
folder_name = 'logscale09'
path_grad = (f"exp/{exp_name}/grad_clip/perlayersig05/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/{folder_name}/{filename}",
               'weight_norm')
path_method_list = [path_grad, path_weight]

plot_frontiers(path_method_list, f"exp/{exp_name}/plot/{folder_name}/")

# mnist imbalanced sigmoid auc
# TODO: make manual attempts
exp_name = 'mnist'
filename = 'results.json'
folder_name = 'imbalsig10'
path_grad = (f"exp/{exp_name}/grad_clip/{folder_name}/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/{folder_name}/{filename}",
               'weight_norm')
path_method_list = [path_grad, path_weight]

plot_frontiers(path_method_list,
               f"exp/{exp_name}/plot/{folder_name}/",
               perf_metric="auc")

# mnist imbalanced acc
exp_name = 'mnist'
filename = 'results.json'
folder_name = 'imbal02'
path_grad = (f"exp/{exp_name}/grad_clip/{folder_name}/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/{folder_name}/{filename}",
               'weight_norm')
path_method_list = [path_grad, path_weight]

plot_frontiers(path_method_list,
               f"exp/{exp_name}/plot/{folder_name}/",
               perf_metric="accuracy")

# mnist manual
exp_name = 'mnist'
filename = 'results.json'
folder_name = 'manual06'
path_grad = (f"exp/{exp_name}/grad_clip/logscale28/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/{folder_name}/{filename}",
               'weight_norm')
path_method_list = [path_grad, path_weight]

plot_frontiers(path_method_list,
               f"exp/{exp_name}/plot/{folder_name}/",
               perf_metric="accuracy")

# fashionmnist manual
exp_name = 'fashionmnist'
filename = 'results.json'
folder_name = 'manual06'
path_grad = (f"exp/{exp_name}/grad_clip/logscale30/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/{folder_name}/{filename}",
               'weight_norm')
path_method_list = [path_grad, path_weight]

plot_frontiers(path_method_list,
               f"exp/{exp_name}/plot/{folder_name}/",
               perf_metric="accuracy")

# fashionmnist logscale09
exp_name = 'fashionmnist'
filename = 'results.json'
folder_name = 'logscale09'
path_grad = (f"exp/{exp_name}/grad_clip/logscale30/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/{folder_name}/{filename}",
               'weight_norm')
path_method_list = [path_grad, path_weight]

plot_frontiers(path_method_list,
               f"exp/{exp_name}/plot/{folder_name}/",
               perf_metric="accuracy")

path_weight = (f"exp/mnist/weight_norm/test/results.json",
               'weight_norm')
ax_client = (
    AxClient.load_from_json_file(path_weight[0])
)
df = ax.service.utils.report_utils.exp_to_df(ax_client.experiment)
df

# cifar manual
exp_name = 'cifar'
filename = 'results.json'
folder_name = 'fix_18_weight'
path_grad = (f"exp/{exp_name}/grad_clip/{folder_name}/{filename}",
             'grad_clip')
path_weight = (f"exp/{exp_name}/weight_norm/{folder_name}/{filename}",
               'weight_norm')
path_method_list = [path_grad, path_weight]

plot_frontiers(path_method_list,
               f"exp/{exp_name}/plot/{folder_name}/",
               perf_metric="accuracy")

exp_name = 'android'
method = 'weight_norm'
save_dir = 'search_03'
filename = 'results.json'
path = f"exp/{exp_name}/{method}/{save_dir}/{filename}"
ax_client = (
    AxClient.load_from_json_file(path)
)
frontier = ax_client.get_pareto_optimal_parameters()
# mnist runtime
exp_name = 'mnist'
filename = 'results.json'
folder_name = 'runtime18'
path_grad = (f"exp/{exp_name}/grad_clip/{folder_name}/{filename}",
             'DP-SGD')
path_weight = (f"exp/{exp_name}/weight_norm/{folder_name}/{filename}",
               'Lip-DP-SGD')
path_method_list = [path_grad, path_weight]

path_results = f'exp/{exp_name}/plot/{folder_name}/'
if not os.path.exists(path_results):
    os.makedirs(path_results)

for i, path_method in enumerate(path_method_list):
    path, method = path_method
    ax_client = (AxClient.load_from_json_file(path))
    df = ax.service.utils.report_utils.exp_to_df(ax_client.experiment)
    plt.plot(df.batch_size,
             df.time,
             label=f'{method}',
             marker=markers[i])
plt.grid()
plt.xlabel('Batch size')
plt.ylabel("Runtime (s)")
plt.xlim(-1000, 51000)
plt.ylim(-0.5, 19)
plt.legend()

plt.savefig(f'{path_results}results.png', bbox_inches='tight')
plt.close()

# fashionmnist runtime
exp_name = 'fashionmnist'
filename = 'results.json'
folder_name = 'runtime22'
path_grad = (f"exp/{exp_name}/grad_clip/{folder_name}/{filename}",
             'DP-SGD')
path_weight = (f"exp/{exp_name}/weight_norm/{folder_name}/{filename}",
               'Lip-DP-SGD')
path_method_list = [path_grad, path_weight]

path_results = f'exp/{exp_name}/plot/{folder_name}/'
if not os.path.exists(path_results):
    os.makedirs(path_results)

for i, path_method in enumerate(path_method_list):
    path, method = path_method
    ax_client = (AxClient.load_from_json_file(path))
    df = ax.service.utils.report_utils.exp_to_df(ax_client.experiment)
    plt.plot(df.batch_size,
             df.time,
             label=f'{method}',
             marker=markers[i])
plt.grid()
plt.xlabel('Batch size')
plt.ylabel("Runtime (s)")
plt.xlim(-1000, 51000)
plt.ylim(-0.5, 19)
plt.legend()

plt.savefig(f'{path_results}results.png', bbox_inches='tight')
plt.close()

# cifar runtime
exp_name = 'cifar'
filename = 'results.json'
folder_name = 'runtime22'
path_grad = (f"exp/{exp_name}/grad_clip/{folder_name}/{filename}",
             'DP-SGD')
path_weight = (f"exp/{exp_name}/weight_norm/{folder_name}/{filename}",
               'Lip-DP-SGD')
path_method_list = [path_grad, path_weight]

path_results = f'exp/{exp_name}/plot/{folder_name}/'
if not os.path.exists(path_results):
    os.makedirs(path_results)

for i, path_method in enumerate(path_method_list):
    path, method = path_method
    ax_client = (AxClient.load_from_json_file(path))
    df = ax.service.utils.report_utils.exp_to_df(ax_client.experiment)
    plt.plot(df.batch_size,
             df.time,
             label=f'{method}',
             marker=markers[i])
plt.grid()
plt.xlabel('Batch size')
plt.ylabel("Runtime (s)")
plt.xlim(-1000, 51000)
plt.ylim(-0.5, 19)
plt.legend()

plt.savefig(f'{path_results}results.png', bbox_inches='tight')
plt.close()

df


# TODO: investigate negative espilon
# TODO: look before pareto
# TODO: look into division by lot size
# TODO:


perf_metric = 'auc'
df = pd.DataFrame()
for path, method in path_method_list:
    ax_client = (
        AxClient.load_from_json_file(path)
    )

    objectives = ax_client.experiment.optimization_config\
        .objective.objectives
    frontier = compute_posterior_pareto_frontier(
        experiment=ax_client.experiment,
        data=ax_client.experiment.fetch_data(),
        primary_objective=objectives[0].metric,
        secondary_objective=objectives[1].metric,
        absolute_metrics=["epsilon", perf_metric],
        num_points=num_points,
    )
    df_frontier = pd.DataFrame({'epsilon': frontier.means['epsilon'],
                                perf_metric: frontier.means[perf_metric],
                                'sems': frontier.sems[perf_metric],
                               'method': method
                                })
    df = pd.concat([df, df_frontier])

df

df = pd.DataFrame()
folder_name = 'imbal02'
for exp_name in ['mnist']:
    for method in methods:

        path = f"exp/{exp_name}/{method}/{folder_name}/{filename}"

        ax_client = (
            AxClient.load_from_json_file(path)
        )

        objectives = ax_client.experiment.optimization_config.objective.objectives
        frontier = compute_posterior_pareto_frontier(
            experiment=ax_client.experiment,
            data=ax_client.experiment.fetch_data(),
            primary_objective=objectives[0].metric,
            secondary_objective=objectives[1].metric,
            absolute_metrics=["epsilon", "accuracy"],
            num_points=25,
        )
        df_frontier = pd.DataFrame({'epsilon': frontier.means['epsilon'],
                                    'accuracy': frontier.means['accuracy'],
                                   'sems': frontier.sems['accuracy'],
                                    'method': method
                                    })
        df = pd.concat([df, df_frontier])


df = df.sort_values(['method', 'epsilon'])

for method in methods:
    df_plot = df.loc[df.method == method]
    plt.errorbar(df_plot.epsilon,
                 df_plot.accuracy,
                 yerr=df_plot.sems,
                 label=f'{method}',
                 marker=markers[0])

plt.grid()
plt.xlabel('Epsilon')
plt.xscale('log')
plt.ylabel('Accuracy')
plt.legend()

plt.show()
#
# plt.savefig(f'{path_results}results.png', bbox_inches='tight')
# plt.close()


# evaluate frontier on testset
test_accuracy = []
test_auc = []
for best_arm in frontier.param_dicts:
    net = CNN()
    parameters["operators_lip"] = net.operators_lip
    net = GradSampleModule(net, batch_first=True)

    # Initialize dataloaders
    train_loader = torch.utils.data.DataLoader(
        train_valid_set,
        batch_size=best_arm.get("batch_size"),
    )
    test_loader = torch.utils.data.DataLoader(
        test_set,
        batch_size=best_arm.get("batch_size"),
    )
    train_loader = DPDataLoader.from_data_loader(train_loader,
                                                 distributed=False)

    net, accountant = train(
        net=net,
        train_loader=train_loader,
        parameters=best_arm,
        dtype=dtype,
        device=device,
    )
    acc, auc = evaluate_acc_auc(
        net=net,
        data_loader=test_loader,
        dtype=dtype,
        device=device,
    )
    test_accuracy.append(acc)
    test_auc.append(auc.item())

# write results
results_means = frontier.means
results_means['test_accuracy'] = test_accuracy
results_means['test_auc'] = [0., 2.]
results = {'means': results_means,
           'sems': {k: list(v) for k, v in frontier.sems.items()}
           }
with open(path+"frontier.json", "w") as outfile:
    json.dump(results, outfile)

df_exp.to_csv(path+'trials.csv')

get_noise_multiplier(target_epsilon=1., target_delta=1/10000, sample_rate=100/60000, epochs=10)
[0.45, 0.56, 0.90, 5.3]


def get_calibration(exp='cifar'):
    noise_multiplier_range = [0.45, 0.56, 0.90, 5.3]
    res_allnoise = []
    for noise_multiplier in noise_multiplier_range:
        res = {}
        for method in ['grad_clip', 'weight_norm']:
            # exp='mnist'
            # method = 'grad_clip'
            # noise_multiplier = 0.45
            path = f"exp/{exp}/{method}/calibration_30/"
            df = pd.read_csv(path+"trials.csv")
            outputs = df.loc[df.noise_multiplier == noise_multiplier]['labels'].values
            ground_truth = np.array(ast.literal_eval(outputs[0]))
            outputs = df.loc[df.noise_multiplier == noise_multiplier]['outputs'].values
            confidences = np.array(ast.literal_eval(outputs[0]))
            n_bins = 10
            ece = ECE(n_bins)
            res[method] = ece.measure(confidences, ground_truth)
            # diagram = ReliabilityDiagram(n_bins)
            # # visualize miscalibration of uncalibrated
            # fig = diagram.plot(confidences, ground_truth)
            # path_results = f'exp/cifar/{method}/calibration_29/'
            # fig.savefig(f'{path_results}results_{noise_multiplier}.png', bbox_inches='tight')
            epsilon = round(df.loc[df.noise_multiplier ==
                                   noise_multiplier]['epsilon'].values[0], 2)
        res_allnoise += [{'noise_multiplier': noise_multiplier,
                          'epsilon': epsilon, **res}]

    return res_allnoise


res = get_calibration('mnist')
res
pd.DataFrame.from_records(res)\
    .sort_values('epsilon')\
    .set_index('epsilon')\
    .drop('noise_multiplier', axis=1)\
    .rename({'grad_clip': 'DP-SGD', 'weight_norm': 'Lip-DP-SGD'}, axis=1)\
    .plot(kind='bar', title='ECE per epsilon on CIFAR10', ylabel='ECE', xlabel='epsilon')\
    .get_figure()\
    .savefig('exp/cifar/plot/calibration_29/ece.png')


col_name = 'labels'
noise_multiplier = 1.48
outputs = df_grad.loc[df_grad.noise_multiplier == noise_multiplier][col_name].values
ground_truth = np.array(ast.literal_eval(outputs[0]))

df_grad

col_name = 'outputs'
noise_multiplier = 1.48
outputs = df_grad.loc[df_grad.noise_multiplier == noise_multiplier][col_name].values
confidences = np.array(ast.literal_eval(outputs[0]))


temperature = TemperatureScaling()
temperature.fit(confidences, ground_truth)
calibrated = temperature.transform(confidences)


n_bins = 10

ece = ECE(n_bins)
uncalibrated_score = ece.measure(confidences, ground_truth)
calibrated_score = ece.measure(calibrated, ground_truth)


n_bins = 10

diagram = ReliabilityDiagram(n_bins)
fig = diagram.plot(confidences, ground_truth)  # visualize miscalibration of uncalibrated
path_results = 'exp/cifar/grad_clip/calibration_29/'
fig.savefig(f'{path_results}results.png', bbox_inches='tight')
diagram.plot(calibrated, ground_truth)   # visualize miscalibration of calibrated


path = "exp/cifar/grad_clip/calibration_29/trials.csv"
df_grad = pd.read_csv(path)


col_name = 'labels'
noise_multiplier = 1.48
outputs = df_grad.loc[df_grad.noise_multiplier == noise_multiplier][col_name].values
ground_truth = np.array(ast.literal_eval(outputs[0]))

col_name = 'outputs'
noise_multiplier = 1.48
outputs = df_grad.loc[df_grad.noise_multiplier == noise_multiplier][col_name].values
confidences = np.array(ast.literal_eval(outputs[0]))


temperature = TemperatureScaling()
temperature.fit(confidences, ground_truth)
calibrated = temperature.transform(confidences)


n_bins = 10

ece = ECE(n_bins)
uncalibrated_score = ece.measure(confidences, ground_truth)
calibrated_score = ece.measure(calibrated, ground_truth)


n_bins = 10

diagram = ReliabilityDiagram(n_bins)
diagram.plot(confidences, ground_truth)
