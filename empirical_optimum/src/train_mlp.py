import numpy as np
from torch.utils.tensorboard import SummaryWriter
import torch.optim as optim
import torch.nn.functional as F
import torch.nn as nn
import torch
import json
import os
import argparse
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

import torchvision
import torchvision.transforms as transforms

from opacus import GradSampleModule
from opacus.data_loader import DPDataLoader

from lip_dp.train import train, evaluate_acc_auc, evaluate_ece

from utils import load_vision_data, load_tabular_data, load_best_params
from model import load_vision_model, load_tabular_model

exp_names = ['mnist', 'fashionmnist', 'cifar',
             'income', 'android', 'breast', 'default_credit',
             'dropout', 'german', 'nursery', 'thyroid', 'yeast']

parser = argparse.ArgumentParser()
parser.add_argument('--exp-name', '-e', dest='exp_name', default='mnist',
                    choices=exp_names,
                    help='experiment name: ' + ' | '.join(exp_names) +
                    ' (default: mnist)')
parser.add_argument('-j', '--workers', default=0, type=int, metavar='N',
                    help='number of data loading workers (default: 0)')
# TODO: check how it's used
parser.add_argument('--runs', '-r', default=50, type=int,
                    help='number of trials to run')
parser.add_argument('--save-dir', '-s', dest='save_dir',
                    help='The directory used to save the trained models',
                    default='save_temp', type=str)
parser.add_argument('--imbal', '-i', default=False, type=bool,
                    help='boolean to load imbalanced dataset, \
                    only available for mnist')
parser.add_argument('--fix', '-f', default=False, type=bool,
                    help='boolean to init optimizer with fixed sigma')

args = {'exp_name': 'mnist'}
parameters = {
    "batch_size": 100,
    "max_weight_norm": 7.0,
    "num_epochs": 10,
    "delta": 1e-4,
    "loss_temperature": 1.0
}

torch.manual_seed(42)
dtype = torch.float
os.environ["PYTORCH_CUDA_ALLOC_CONF"] = "max_split_size_mb:512"

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

train_valid_set, test_set = load_vision_data(args.get('exp_name'))

net = load_vision_model(args.get('exp_name'))
net = GradSampleModule(net, batch_first=True)


# Initialize dataloaders
train_loader = torch.utils.data.DataLoader(
    train_valid_set,
    batch_size=parameters.get("batch_size"),
    shuffle=True,
    pin_memory=(args.get('exp_name') == 'cifar')
)
test_loader = torch.utils.data.DataLoader(
    test_set,
    batch_size=parameters.get("batch_size"),
    shuffle=True,
    pin_memory=(args.get('exp_name') == 'cifar')
)
train_loader = DPDataLoader.from_data_loader(train_loader,
                                             distributed=False)

# train_loader = DPDataLoader.from_data_loader(train_loader,
#                                              distributed=False)

classes = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}
criterion = torch.nn.CrossEntropyLoss()
criterion = criterion.to(device=device)
optimizer = optim.Adam(
    net.parameters(),
    lr=0.001,
    weight_decay=0.0,
)

sample_rate = 1 / len(train_loader)
expected_batch_size = int(len(train_loader.dataset) * sample_rate)

optimizer = LipDPOptimizer(
    optimizer=optimizer,
    noise_multiplier=parameters.get("noise_multiplier", 1),
    max_weight_norm=parameters.get("max_weight_norm", 1.),
    max_grad_norm=parameters.get("max_grad_norm", None),
    expected_batch_size=expected_batch_size,
    fix_sigma=parameters.get("fix_sigma", False)
)

accountant = RDPAccountant()
optimizer.attach_step_hook(
    accountant.get_optimizer_hook_fn(sample_rate=sample_rate)
)


# default `log_dir` is "runs" - we'll be more specific here
writer = SummaryWriter(f'runs/{args.get("exp_name")}_experiment_0')


def matplotlib_imshow(img, one_channel=False):
    if one_channel:
        img = img.mean(dim=0)
    img = img / 2 + 0.5     # unnormalize
    npimg = img.numpy()
    if one_channel:
        plt.imshow(npimg, cmap="Greys")
    else:
        plt.imshow(np.transpose(npimg, (1, 2, 0)))


# get some random training images
dataiter = iter(train_loader)
images, labels = next(dataiter)

# create grid of images
img_grid = torchvision.utils.make_grid(images)

# show images
matplotlib_imshow(img_grid, one_channel=True)

# write to tensorboard
writer.add_image('four_mnist_images', img_grid)

writer.add_graph(net, images)
writer.close()


# helper functions

def images_to_probs(net, images):
    '''
    Generates predictions and corresponding probabilities from a trained
    network and a list of images
    '''
    output = net(images)
    # convert output probabilities to predicted class
    _, preds_tensor = torch.max(output, 1)
    preds = np.squeeze(preds_tensor.numpy())
    return preds, [F.softmax(el, dim=0)[i].item() for i, el in zip(preds, output)]


def plot_classes_preds(net, images, labels):
    '''
    Generates matplotlib Figure using a trained network, along with images
    and labels from a batch, that shows the network's top prediction along
    with its probability, alongside the actual label, coloring this
    information based on whether the prediction was correct or not.
    Uses the "images_to_probs" function.
    '''
    preds, probs = images_to_probs(net, images)
    # plot the images in the batch, along with predicted and true labels
    fig = plt.figure(figsize=(12, 48))
    for idx in np.arange(4):
        ax = fig.add_subplot(1, 4, idx+1, xticks=[], yticks=[])
        matplotlib_imshow(images[idx], one_channel=True)
        ax.set_title("{0}, {1:.1f}%\n(label: {2})".format(
            classes[preds[idx]],
            probs[idx] * 100.0,
            classes[labels[idx]]),
            color=("green" if preds[idx] == labels[idx].item() else "red"))
    return fig


running_loss = 0.0
for epoch in range(1):  # loop over the dataset multiple times

    for i, data in enumerate(train_loader, 0):

        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        running_loss += loss.item()
        if i % 10 == 9:    # every 1000 mini-batches...

            # ...log the running loss
            writer.add_scalar('training loss',
                              running_loss / 10,
                              epoch * len(train_loader) + i)

            # ...log a Matplotlib Figure showing the model's predictions on a
            # random mini-batch
            running_loss = 0.0
print('Finished Training')

net, accountant = train(
    net=net,
    train_loader=train_loader,
    parameters=parameters,
    dtype=dtype,
    device=device,
)


def run(method):
    global args
    args = parser.parse_args()

    path = f"exp/{args.exp_name}/{method}/"
    path_results = f"{path}{args.save_dir}/"

    if not os.path.exists(path_results):
        os.makedirs(path_results)

    torch.manual_seed(42)
    dtype = torch.float
    os.environ["PYTORCH_CUDA_ALLOC_CONF"] = "max_split_size_mb:512"

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # load best params
    if args.exp_name in ['mnist', 'cifar']:
        with open(path+'parameters_ece.json', 'r') as f:
            parameters = json.load(f)
    else:
        parameters = load_best_params(method,
                                      exp_name=args.exp_name,
                                      save_dir=args.save_dir)
    parameters['method'] = method
    parameters['fix_sigma'] = args.fix

    if args.exp_name in ['mnist', 'fashionmnist', 'cifar']:
        train_valid_set, test_set = load_vision_data(args.exp_name)
        class_num = 10
    else:
        train_valid_set, test_set, input_dim, class_num = load_tabular_data(args.exp_name)

    if args.exp_name in ['mnist', 'fashionmnist', 'cifar']:
        net = load_vision_model(args.exp_name)
    else:
        net = load_tabular_model(args.exp_name,
                                 input_dim,
                                 class_num)

    parameters["operators_lip"] = net.operators_lip
    net = GradSampleModule(net, batch_first=True)

    # Initialize dataloaders
    train_loader = torch.utils.data.DataLoader(
        train_valid_set,
        batch_size=parameters.get("batch_size"),
        shuffle=True,
        pin_memory=(args.exp_name == 'cifar')
    )
    test_loader = torch.utils.data.DataLoader(
        test_set,
        batch_size=parameters.get("batch_size"),
        shuffle=True,
        pin_memory=(args.exp_name == 'cifar')
    )

    train_loader = DPDataLoader.from_data_loader(train_loader,
                                                 distributed=False)

    net, accountant = train(
        net=net,
        train_loader=train_loader,
        parameters=parameters,
        dtype=dtype,
        device=device,
    )
    delta = parameters.get("delta", 1/10000)  # todo: apply actual values
    epsilon = accountant.get_epsilon(delta)
    noise_multiplier = parameters.get("noise_multiplier")


if __name__ == '__main__':
    for method in ['grad_clip', 'weight_norm']:  # 'grad_clip',
        run(method)
