# Jury

template:
* Full name
* position (professor, maitre de conference, directeur de recherche ...)
* institution
* research group
* website
* email address

## Potential Reviewers

### Catuscia Palamidessi
* Director of Research
* INRIA
* Leader of the equipe Comète
* https://www.lix.polytechnique.fr/~catuscia/
* catuscia@lix.polytechnique.fr

### Alain Rakotomamonjy
* Professor
* Université de Rouen
* LITIS Rouen
* https://github.com/arakotom
* Alain.Rakoto@insa-rouen.fr

## Potential examiners

### Antoine Boutet
* HDR
* INRIA
* Privatics
* https://sites.google.com/site/antoineboutet/
* antoine.boutet @ insa-lyon.fr

### Kevin Scaman
* Research Scientist
* INRIA
* Argo
* https://kscaman.github.io/
* kevin.scaman@inria.fr