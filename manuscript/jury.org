* Jury
We need two "rapporteur·ices". It is good to have one french and one
foreigner.

Topics that should be covered are: convex optimization, federated
learning, privacy and fairness. It would also be nice to have some who
have worked on sparse things.

Recall that we should have:
- at least 2 women (KC, CP)
- at least 50% HDR (EM, KC, RG, CP, AD, AB, MT), and rapporteur·ices
  should also be (EM, KC)
- at least 50% outside of project (EM, KC, RG, CP, CG), and
  rapporteur·ices should also be (EM, KC)
There also needs to be an agreement for participating in visio.

** Constraints for the defense
- Katrina: unsure between 15/9 and 7/10.
- Joseph: not 29/9 since Camille defends 9-12h.


** Rapporteurs
- [Accepted 18/4/23] Eric Moulines (optimization/federated learning):
  possibly post doc with him after, and I would be interested in his
  take on FL with control variates (which he seems to think is not a
  so promising research direction long term).
- [Invited 21/4/23, Somehow accepted 27/4/23] Katrina Ligett.

- [Invited 18/4/23 - Refused: maybe examinatrice?] Kamalika Chaudhuri
  (privacy/optimization): she worked extensively on private
  optimization, and I would be interested in her take on private
  optimization in general.

Maybe: Catuscia Palamidessi? Or I should just ask her to be
examinatrice?

** Examinateurs
- Rémi Gribonval (privacy/sparsity/optimization): he worked on
  sparsity and I would be interested in his take on our private greedy
  algorithms.
- Catuscia Palamidessi (privacy/fairness): she worked a lot on
  fairness/privacy and I would be interested in her take on our
  fairness/privacy interplay results.
- Cristobal Guzman (privacy/optimization): he worked a lot on private
  optimization, and I would be interested in his take on greedy
  algorithms for privacy in general.
- Aymeric Dieuleveut (optimization/federated learning/privacy): he
  will possibly be involved in post doc, and I would be interested in
  his take on local training vs compression in federated learning.
** Directeurs
- Aurélien Bellet
- Marc Tommasi
- Joseph Salmon (en invité?)
