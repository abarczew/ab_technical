\chapter{Conclusion and Perspectives}
\label{chap:conclusion}

\section{Conclusion}
\label{sec:conclusion-1}

This thesis has addressed critical challenges in privacy-preserving machine 
learning, with a specific focus on optimizing and analyzing machine learning 
models in scenarios that involve repeated querying. Repeated queries, such as 
iterative gradient computations in optimization or multiple evaluations in 
empirical cumulative distribution function (ECDF) estimation, give significant 
challenges to maintaining strong privacy guarantees while preserving utility. 
By designing specific privacy-preserving strategies to the structural properties of 
these applications, this thesis demonstrates that it is possible to 
achieve better privacy-utility trade-offs than with generic methods.

In the context of optimization (\Cref{chap:emp-opt}), we demonstrated that constraining models to a 
subclass of Lipschitz constrained functions allows for improved privacy-utility 
trade-offs. By leveraging gradient sensitivity properties, we showed that the 
bias introduced by selecting models from this subclass is less harmful to 
utility compared to the bias induced by gradient clipping in popular 
differentially private optimization methods. This insight led to the 
development of \weightDpSgd{}, a novel optimization algorithm that reduces 
the privacy budget and improves both accuracy and runtime performance compared to SOTA, as 
demonstrated experimentally on a diverse range of datasets.

For ECDF computation (\Cref{chap:priv-ecdf}), we explored its unique structural properties to design 
a novel algorithm that provides strong privacy guarantees while preserving 
utility. Unlike traditional methods, our approach is compatible with a wide 
range of security protocols, including function secret sharing, enabling its 
use in more complex scenarios such as multi-party computation. This 
contribution highlights how structural insights into ECDFs can be leveraged 
to achieve enhanced privacy and enable practical deployment in collaborative 
environments.

Through these two contributions, this thesis has shown that tailoring 
privacy-preserving techniques to specific applications can uncover new 
opportunities for improving privacy-utility trade-offs while enabling broader 
practical setups. By addressing optimization and ECDF estimation in particular, 
this work lays a foundation for the development of more efficient, accurate, 
and scalable privacy-preserving techniques in machine learning and data analysis.


\section{Perspectives}
\label{sec:perspectives}
Looking forward, the findings and methods presented in this thesis suggest 
promising directions for future research.

Extending these insights to other applications, exploring tighter privacy bounds for specific tasks, and enhancing the scalability of privacy-preserving protocols in distributed and collaborative settings are just a few avenues for continued exploration. Ultimately, this work contributes to the broader effort to balance the competing demands of privacy and utility in modern machine learning systems, paving the way for more secure and practical data-driven solutions.

We list bellow a few of these future research topics.
% Several opportunities for future work remain.  First, it would be interesting to
% better integrate and improve ideas such as in  \cite{scaman_lipschitz_2019} to
% %improve the efficiency of the algorithm in \cite{scaman_lipschitz_2019} to
% find improved bounds on gradients of Lipschitz-constrained neural networks, as this may allow to further reduce  the amount of noise needed.

\paragraph{Theoretical analysis of \weightDpSgd.} In \Cref{chap:emp-opt} we prove empirically the
the benefit one can take from \weightDpSgd{}. However, further theoretical analysis of \weightDpSgd{}, especially the interaction between sensitivity, learning rate and number of iterations, remains an interesting area of research, similar to the work of \cite{song_evading_2020} on \gradDpSgd{}.
An analysis on the interactions between hyperparameters would provide valuable insights into the optimal use of our method and its potential combination with other regularization techniques.


\paragraph{Deep Learning architectures.} In \Cref{chap:emp-opt}, we demonstrated that \weightDpSgd{} is applicable to various feed-forward neural networks. Specifically, we achieved state-of-the-art results with MLPs, CNNs, and ResNets. While this work primarily focused on these architectures, there is significant potential to extend our approach to other classes of models, as well as to additional instances of feed-forward neural networks, such as transformers. Recent studies, such as \cite{qi2023lipsformer}, have proposed methods to efficiently enforce Lipschitz constraints on transformers. However, we have not yet explored whether \weightDpSgd{} can be directly applied to these models without encountering issues like DP noise amplification due to the large number of parameters inherent to transformers. This raises the possibility that specific refinements may be required to adapt our method effectively in such cases.


% \paragraph{Lipschitz constrain networks.} There are two main pain points, the
% over-estimation and the computational cost. [scaman, x] come up with solutions
% than can tackle these two issues at the same time thanks to an implementation
% that fits the GRADDIF? approach of modern deep learning libraries. This method
% could be implemented to provide Lipschitz constrained network that can be fast
% and with a soft constrain?

\paragraph{Exponential Mechanism.} The exponential mechanism is particularly well-suited for optimization scenarios; however, there is limited literature on leveraging this mechanism for differentially private model optimization. While the Gaussian mechanism has become a de facto choice due to its favorable composition properties, it also presents significant drawbacks in our context, such as the reliance on the $\ell_2$ norm for estimating the Lipschitz value. Incorporating the exponential mechanism into \weightDpSgd{} could overcome these limitations, enabling the use of $\ell_1$ or $\ell_\infty$ norms to provide faster and more reliable estimates of Lipschitz values.

\paragraph{Display continuous metrics with DP.} 
The DP ECDF allows for the publication of the complete ROC curve while ensuring privacy guarantees. It is based on a discrete count function, which we leveraged to design a binary mechanism.  
However, sharing continuous metrics—such as those in healthcare, finance (e.g., stock prices), or location tracking—introduces distinct challenges and promising research directions. One key difficulty stems from temporal correlations in continuous data. When data points are interdependent over time, a single modification can influence multiple points, potentially heightening the risk of privacy breaches. This phenomenon, known as "temporal privacy leakage," can weaken the expected privacy protection of DP mechanisms \cite{TemporalCorr}.  
Some approaches attempt to exploit the autocorrelation inherent in certain metrics \cite{katsomallos:hal-02466471}, but they struggle to achieve a satisfactory balance between privacy and utility.







%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
