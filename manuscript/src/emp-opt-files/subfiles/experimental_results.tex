\section{Experimental results}\label{sec:experiment}
In this section, we conduct an empirical evaluation of our approach.

\subsection{Experimental setup}
\label{sec:exp.setup}

We consider the following experimental questions:
\begin{itemize}[noitemsep,topsep=0pt]
\item[Q1] How does \weightDpSgd{}, our proposed technique, compare against the conventional \gradDpSgd{} as introduced by \cite{abadi_deep_2016}?
\item[Q2] What is the effect of allowing $\|\theta_k\| < C$ rather than normalizing $\|\theta_k\|$ to $C$?  This question seems relevant given that some authors
(e.g., \cite{bethune_pay_2022}) also suggest to consider networks which constant gradient norm rather than maximal gradient norm, i.e., roughly with $\theta$ in
%papers consider only models from
$\thetaSpaceEqC$ rather than $\thetaSpaceLeC$. % \cite{bethune_pay_2022}.
% \item[Q3] Does \weightDpSgd{} induce less bias than \gradDpSgd{}?
% \item[Q3] What is the impact of using norms other than the \(2\)-norm?
\end{itemize}

\paragraph{Norms.}\label{exp:norm} In this section, we focus on the \(\ell_{2,2}\) norm. All subsequent results are based on this norm. Although computing this norm is more computationally intensive, it allows for the use of the Gaussian mechanism, which is standard in this field.

\paragraph{Hyperparameters.}\label{exp:hyper} We selected a number of hyperparameters to tune for our experiments, aiming at making a fair comparison between the studied techniques while minimizing the distractions of potential orthogonal improvements.  To optimize these hyperparameters, we used Bayesian optimization \cite{balandat_botorch_2020}.


% Appendix \ref{sec:app.exp.hyperparameters} provides a detailed discussion.

In the literature, there are a wide range of improvements possible over a direct application of SGD to supervised learning, including general strategies such as pre-training, data augmentation and feature engineering, and DP-SGD specific optimizations such as adaptive maximum gradient norm thresholds.  All of these can be applied in a similar way to both \weightDpSgd{} and \gradDpSgd{} and to keep our comparison sufficiently simple, fair and understandable we didn't consider the optimization of these choices.

We did tune hyperparameters inherent to specific model categories, in particular
the initial learning rate $\eta(0)$ (to start the adaptive learning rate strategy $\eta(t)$) and (for image datasets) the number of groups, and hyperparameters related to the learning algorithm, in particular the (expected) batch size $s$, the Lipschitz upper bound of the normalization layer $\alpha$ and the threshold $C$ on the gradient norm respectively weight norm.

The initial learning rate $\eta(0)$ is tuned while the following $\eta(t)$ are set adaptively. Specifically, we use the strategy of the Adam algorithm \cite{adam}, which update each parameter using the ratio between the moving average of the gradient (first moment) and the square root of the moving average of its squared value (second moment), ensuring fast convergence.



We also investigated varying the hyperparameter $\tau$ of the cross entropy objective function, but the effect of this hyperparameter turned out to be insignificant.
% tried to tune for weights X_0 and temperature, but no significant effect.
%weight: s, X_0, C, temperature
%grad: s, C
%softmax CE -> \sqrt{2}/temp
%a set of hyperparameters that directly impact parameter norms for \weightDpSgd. Among them are the input norm $X_0$, an upper bound of the Lipschitz value of the loss function $l_{K+1}$, batch size $s$, and the respective Lipschitz constraints of the layers $C$.
%In the case of \gradDpSgd, we search for optimal gradient clipping norm and batch size $s$. Similarly to the clipping norm of the \gradDpSgd, the Lipschitz constrains of the layer $C$ of \weightDpSgd{}

Both the clipping threshold $C$ for gradients in \gradDpSgd{} and the clipping threshold $C$ for weights in \weightDpSgd{} can be tuned for each layer separately. While this offers improved performance, it does come with the cost of consuming more of the privacy budget, and substantially increasing the dimensionality of the hyperparameter search space.
In a few experiments we didn't see significant improvements in allowing per-layer varying of $C_k$, so we didn't further pursue this avenue.

\Cref{tab:hyper} summarizes the search space of hyperparameters.
It's important to note that we did not account for potential (small) 
privacy losses caused by hyperparameter search, a limitation also 
acknowledged in other recent works such as \\
\cite{papernot_hyperparameter_2022}.

\begin{table}[ht]
\caption{Summary of hyperparameter space.}\label{tab:hyper}
\centering
\begin{tabular}{cc}
\hline
Hyperparameter & Range  \\
\hline
Noise multiplier $\sigma$ & [0.4, 5] \\
% \hline
Weight clipping threshold $C$ & [1, 15] \\
% \hline
Gradient clipping threshold $C$ & [1, 15] \\
% \hline
Batch size $s$ & [32, 512] \\
% \hline
$\eta(0)$ & [0.0001, 0.01] \\
% \hline
Number of groups (group normalization) & [8, 32] \\
% \hline
$\alpha$ (group normalization) & [$0.1/(|x_k^{(k:\grpidx)}|)$, $1/(|x_k^{(k:\grpidx)}|)$] \\
\hline
% Max input norm $X_0$  & [1, 12] \\
% \hline
% Hyperparameter of CCE $\tau$  & [1, 12] \\
% \hline
\end{tabular}
\end{table}

\paragraph{Datasets and models.} We carried out experiments
on both tabular datasets and datasets with image data.
%in two phases. In the initial phase, our focus was on widely-recognized
First, we consider a collection of 7 real-world tabular datasets (names and citations in Table \ref{tab:auc}).  
For these, we trained multi-layer perceptrons (MLP). A comprehensive list of model-dataset combinations is available in \Cref{tab:data}.
To answer question Q2, we also implemented \globalWeightDpSgd{}, a version of \weightDpSgd{} limited to networks whose weight norms are fixed, i.e., $\forall k:\|\theta_k\|=C$, obtained by setting $\thetanorm_k\gets C$ in Line \ref{ln:setThetaNorm} in Algorithm \ref{alg:weight-dpsgd}.

Second, the image datasets used include MNIST \cite{deng2012mnist}, 
Fashion-MNIST \cite{xiao_fashion-mnist_2017}, and CIFAR-10 \cite{cifar}. 
We trained convolutional neural networks (CNNs) for the first two datasets
 and a Wide-ResNet \cite{Zagoruyko2016WRN} for the latter 
 (see details in \Cref{tab:data}). We implemented all regularization 
 techniques as described in \cite{de2022unlocking}, including group 
 normalization \cite{Wu2018GroupN}, large batch size, weight 
 standardization \cite{qiao2020microbatchtrainingbatchchannelnormalization}, 
 augmentation multiplicity \cite{de2022unlocking}, 
 and parameter averaging \cite{Polyak1992AccelerationOS}. 
 These techniques are compatible with Lipschitz-constrained networks, 
 except for group normalization, for which we proposed an adapted version 
 in \Cref{eq:safe.group.normalization}.\\
We opted for the accuracy to facilitate easy comparisons with prior research.

\Cref{tab:data} shows details of the models we used to train on tabular 
and image datasets. We consider $7$ tabular datasets: adult income 
\cite{misc_adult_2}, android permissions 
\cite{misc_naticusdroid_(android_permissions)_dataset_722}, 
breast cancer \cite{misc_breast_cancer_wisconsin_(diagnostic)_17}, 
default credit \cite{misc_default_of_credit_card_clients_350}, 
dropout \cite{misc_predict_students'_dropout_and_academic_success_697}, 
German credit \cite{misc_statlog_german_credit_data_144} 
and nursery \cite{misc_nursery_76}.  See Table \ref{tab:auc} for the number 
of instances and features for each tabular dataset.

\begin{table}[ht]
\caption{Summary table of datasets with respective models architectures details.}\label{tab:data}
\centering
\begin{tabular}{ccccc}
\hline
Dataset & Image size & Model & Loss & No. of Parameters \\
\hline
Tabular Datasets & -  & MLP & CE & 140 to 2,120 \\
% \hline
MNIST & 28x28x1 & ConvNet & CE &1,625,866 \\
% \hline
FashionMNIST & 28x28x1 & ConvNet & CE & 1,625,866 \\
% \hline
CIFAR-10 & 32x32x3 & WideResnet & CE & 8,944,266 \\
\hline
\end{tabular}
\end{table}


\paragraph{Infrastructure.} All experiments were orchestrated across dual Tesla P100 GPU platforms (12GB capacity), operating under CUDA version 10, with a 62GB RAM provision for Fashion-MNIST and CIFAR-10. Remaining experiments were performed  on an E5-2696V2 Processor setup, equipped with 8 vCPUs and a 52GB RAM cache. The total runtime of the experiments was approximately 50 hours, which corresponds to an estimated carbon emission of $1.96$ kg %, as reported in
\cite{lacoste2019quantifying}.

\begin{figure*}[ht]
  % \hspace*{-O.5in}
  \centerline
  \mbox{
    \subfigure[MNIST\label{fig:mnist_perf}]{\includegraphics[width=0.5\linewidth]{emp-opt/mnist/acc_eps.png}}\quad
    \subfigure[Fashion-MNIST\label{fig:FashionMNIST_perf}]{\includegraphics[width=0.5\linewidth]{emp-opt/fashionmnist/acc_eps.png}}\quad
    \subfigure[CIFAR-10\label{fig:cifar_perf}]{\includegraphics[width=0.5\linewidth]{emp-opt/cifar/acc_eps.png}}
  }

  \caption{Accuracy results, with a fixed \(\delta = 10^{-5}\), for the MNIST (\ref{fig:mnist_perf}), Fashion-MNIST (\ref{fig:FashionMNIST_perf}), and CIFAR-10 (\ref{fig:cifar_perf}) test datasets. The plots show the median accuracy over 5 runs, with vertical lines indicating the standard error of the mean. See \Cref{tab:hyper} for details on model specifications and hyperparameters.}
  \label{fig:big_perf}
\end{figure*}


\begin{table*}[h]
  \caption{Accuracy and $\epsilon$ per dataset and method at $\delta=1/n$, in bold the best result and underlined when the difference with the best result is not statistically significant at a level of confidence of 5\%.}
  \label{tab:auc}
  \centering
  \small\addtolength{\tabcolsep}{-2pt}
  \begin{tabular}{lrrrrrrr}
    \toprule
    Methods & & {\footnotesize \gradDpSgd{}}   & {\footnotesize \weightDpSgd{}} & {\footnotesize \globalWeightDpSgd{}}  \\
    Datasets (\#instances $n$ $\times$ \#features $p$) & $\epsilon$ &  &  &  \\
    \midrule
    Adult Income {\scriptsize (48842x14) \cite{misc_adult_2}} & 0.414 & 0.824 & \textbf{0.831} & 0.804 \\
    Android {\scriptsize (29333x87) \cite{misc_naticusdroid_(android_permissions)_dataset_722}} & 1.273 & 0.951 & \textbf{0.959} & 0.945 \\
    Breast Cancer {\scriptsize (569x32) \cite{misc_breast_cancer_wisconsin_(diagnostic)_17}} & 1.672 & 0.773 & \textbf{0.798} & 0.7 \\
    Default Credit {\scriptsize (30000x24) \cite{misc_default_of_credit_card_clients_350}} & 1.442 & 0.809 & \textbf{0.816} & 0.792 \\
    Dropout {\scriptsize (4424x36) \cite{misc_predict_students'_dropout_and_academic_success_697}} & 1.326 & 0.763 & \textbf{0.819} & 0.736 \\
    German Credit {\scriptsize (1000x20) \cite{misc_statlog_german_credit_data_144}} & 3.852 & 0.735 & \underline{0.746} & 0.768 \\
    Nursery {\scriptsize (12960x8) \cite{misc_nursery_76}} & 1.432 & 0.919 & \textbf{0.931} & 0.89 \\
    \bottomrule
  \end{tabular}
\end{table*}

\subsection{Results}\label{subsec:results}
% Figure \ref{fig:big_perf} shows our results for the image data sets. For each one of them we report results with or without group normalization applied with \gradDpSgd{} and \weightDpSgd{}.
%
% Table \ref{tab:auc} shows our results on the tabular data, comparing \gradDpSgd{} with \weightDpSgd{} and \globalWeightDpSgd{}.
%
%  Appendix \ref{sec:app.exp} presents a number of more detailed and complementary results.
%
% \subsection{Discussion}\label{subsec:discussion}

\paragraph{Image datasets.} In \Cref{fig:big_perf}, \weightDpSgd{} 
surpasses all state-of-the-art results on all three image datasets. 
Previous performances were based on DP-SGD as introduced by \cite{abadi_deep_2016}, 
either combined with regularization techniques \cite{de2022unlocking} 
or with bespoke activation functions \cite{Papernot_Thakurta_Song_Chien_Erlingsson_2021}. Note that while we present 
results using the same set of hyperparameters for MNIST and Fashion-MNIST, 
the results for CIFAR-10 come from a Pareto front of two sets of 
hyperparameters. For epsilon values below 4.2, the results come from a 
Wide-ResNet-16-4, and for epsilon values above 4.2, they come from a 
Wide-ResNet-40-4. See the complete list of hyperparameters 
in \Cref{tab:hyper}.

\paragraph{Tabular datasets.} In \Cref{tab:auc}, we perform a Wilcoxon Signed-rank test, at a confidence level of $5$\%, on $10$ measures of accuracy for each dataset between the \gradDpSgd{} based on the gradient clipping and the \weightDpSgd{} based on our method. \weightDpSgd{} consistently outperforms \gradDpSgd{} in terms of accuracy. This trend holds across datasets with varying numbers of instances and features, including tasks with imbalanced datasets like Dropout or Default Credit datasets. While highly impactful for convolutional layers, group normalization does not yield improvements for either \gradDpSgd{} or \weightDpSgd{} in the case of tabular datasets.

Additionally, \Cref{tab:auc} presents the performance achieved by constraining networks to Lipschitz networks, where the norm of weights is set to a constant, denoted as \globalWeightDpSgd{}. The results from this approach are inferior, even when compared to \gradDpSgd{}.

\paragraph{Conclusion.} In summary, our experimental results demonstrate that \weightDpSgd{} sets new state-of-the-art benchmarks on the three most popular vision datasets, outperforming \gradDpSgd{}. Additionally, \weightDpSgd{} also outperforms \gradDpSgd{} on tabular datasets using MLPs, where it is advantageous to allow the norm of the weight vector \(\theta\) to vary rather than normalizing it to a fixed value, leveraging situations where it can be smaller.

\subsection{Runtime}\label{runtime}

Our experiments didn't show significant deviations from the normal runtime behavior one can expect for neural network training.  As an illustration, we compared on the MNIST dataset and on the CIFAR-10 dataset %, Fashion-MNIST and CIFAR-10
the median epoch runtime of \gradDpSgd{} with \weightDpSgd{}. Both implementations utilize Opacus \cite{opacus} and PyTorch \cite{pytorch}, employing the same set of hyperparameters, such as augmentation multiplicity, to ensure a fair comparison.
%Since \weightDpSgd{} is implemented with PyTorch and Opacus, we compare with a similar implementation for \gradDpSgd{}.
We measure runtime against the logical batch size, limiting the physical batch size to prevent memory errors as recommended by the PyTorch documentation \cite{pytorch}. \Cref{fig:runtime} shows how \weightDpSgd{} is more efficient in terms of runtime compared to \gradDpSgd{}, especially for big batch sizes. This is mainly due to the fact that \gradDpSgd{} requires to clip the gradient at the sample level, slowering down the process.

\begin{figure*}
  \centering
  \mbox{
    \subfigure[MNIST\label{fig:mnist_run}]{\includegraphics[width=0.48\linewidth]{emp-opt/mnist/runtime.png}}
    \subfigure[CIFAR-10\label{fig:cifar_run}]{\includegraphics[width=0.48\linewidth]{emp-opt/cifar/runtime.png}}\quad
  }
  \caption{Median runtime in seconds per batch size on one epoch over the MNIST dataset \ref{fig:mnist_run} and the CIFAR-10 dataset \ref{fig:cifar_run} comparing \gradDpSgd{} (in orange) and \weightDpSgd{} (in blue).}
  \label{fig:runtime}
\end{figure*}

\subsection{Gradient clipping behavior}
\label{app:exp.clipfreq}

In Section \ref{sec:avoid.bias} we argued that \gradDpSgd{} introduces bias.
There are several ways to demonstrate this.  For illustration we show here the error between the true average gradient
\[
  g_k^{\weightDpSgd} = \frac{1}{|V|} \sum_{i=1}^{|V|} \nabla_{\tilde{\theta}_k} \ell(f_{\tilde{\theta}}(x_i))
\]
i.e., the model update of Algorithm \ref{alg:weight-dpsgd} without noise, and the average clipped gradient
\[
g_k^{\gradDpSgd} = \frac{1}{|V|}\sum_{i=1}^{|V|} \hbox{clip}_C\left( \nabla_{\tilde{\theta}_k} \ell(f_{\tilde{\theta}}(x_i)) \right),
\]
i.e., the model update of Algorithm \ref{alg:dpsgd_grad} without noise.

\Cref{fig:clip} shows the error $\left\| g_k^{\weightDpSgd} - g_k^{\gradDpSgd} \right\|$ together with the norm of the \gradDpSgd{} model update $\left\|g_k^{\gradDpSgd}\right\|$.

One can observe for both considered datasets that while the model converges and the average clipped gradient decreases, the error between \gradDpSgd{}'s average clipped gradient and the true average gradient increases.  At the end, the error in the gradient caused by clipping is significant, and hence the model converges to a different point than the real optimum.


\begin{figure*}
  \centering
  \mbox{
    \subfigure[Dropout\label{fig:dropout_clip}]{\includegraphics[width=0.48\linewidth]{dropout_clipping.png}}\quad
    \subfigure[Adult Income\label{fig:income_clip}]{\includegraphics[width=0.48\linewidth]{income_clipping.png}}
  }
  \caption{Norm of the average error $g - \text{clip}(g)$ (in blue) and norm of the average of $\text{clip}(g)$ (in red) across training iterations on the Dropout dataset \ref{fig:dropout_clip} (averaged over 500 instances) and the Adult Income dataset \ref{fig:income_clip} (averaged over 500 instances).}
  \label{fig:clip}
\end{figure*}



%\textbf{Scaling strategies. } By default, we used the smallest available parameter norm at each step, denoted as $\thetanorm$, where $\thetanorm_k = \min(C, \|\theta\|_2)$, as outlined in \Cref{alg:weight-dpsgd}. This "local sensitivity" scaling strategy differs from the "global sensitivity" approach, which always sets $\thetanorm_k = C$, potentially reducing runtime but not maximizing the current position of $\theta$ in the search. Testing on tabular datasets, the local method generally outperforms the global strategy at a 5\% confidence level, except for three datasets where differences are not significant. Detailed results are in Appendix \ref{tab:auc-localglobal}.
