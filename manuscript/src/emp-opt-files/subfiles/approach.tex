\section{Our approach}
\label{sec:approach}


\newcommand{\thetanorm}{u^{(\theta)}}

In this work, we constrain the objective function to be Lipschitz, and exploit this to determine sensitivity. An important advantage is that while traditional \gradDpSgd{} controls sensitivity via gradient clipping of each sample separately, our new method estimates gradient sensitivity based on only the model in a data-independent way. This is grounded in Lipschitz-constrained model literature (\cref{sec:related}), highlighting the connection between the Lipschitz value for input and parameter. Subsection \ref{subsec:lip_values} delves into determining an upper Lipschitz bound. Subsection \ref{subsec:backprop} demonstrates the use of backpropagation for gradient sensitivity estimation, and in \ref{subsec:lipdpsg}, we introduce \weightDpSgd{}, a novel algorithm ensuring privacy without gradient clipping.


\subsection{Estimating lipschitz values} \label{subsec:lip_values}

In this section we bound Lipschitz values of different types of layers.
We treat linear operations (e.g., linear transformations, convolutions) and activation functions as different layers.
We present results with regard to any $p$-norms with $p \in \{1, 2, +\infty\}$.

\paragraph{Loss function and activation layer.} Examples of Lipschitz losses 
encompass Softmax Cross-entropy, Cosine Similarity, and Multiclass Hinge. When 
it comes to activation layers, layers composed of an activation function, 
several prevalent ones, such as ReLU, tanh, and Sigmoid are 1-Lipschitz 
with respect to all $p$-norms. We provide a detailed list in  
\cref{tab:lip}.


\paragraph{Normalization layer. } % adpat notation
To be able to easily bound sensitivity, we define the operation of a normalization layer $f_{\theta_k}^{(k)}$ slightly differently than Eq \eqref{def:normalization}:
\begin{equation}
  \label{eq:safe.group.normalization}
    x_{k+1}^{(k:\grpidx)}=\layerFunc{k}(x_k^{(k:\grpidx)})=\frac{x_k^{(k:\grpidx)}-\mu^{(k:\grpidx)}}{\max(1, \sigma^{(k:\grpidx)})}.
  \end{equation}
It is easy to see that the sensitivity is bounded by
%If $\layerFunc{k}$ is a normalization layer, then,
\begin{equation}\label{eq:normal_lip}
  \begin{aligned}
    \left\|\layerPartialInput{k}\right\|_p \leq \max_{\grpidx \in [|\Gamma_k|]} \frac{1}{\max\left(1,\sigma^{(k:\grpidx)}\right)} \le 1.
  \end{aligned}
\end{equation}

%as one can derive from \cite{gouk2020regularisation}.
%Thus, the Lipschitz value of the normalization layer is dependent on the norm of the input. To avoid utilizing information from the data that will later be employed to compute overall sensitivity and scale the noise, we propose an adapted form of the group normalization operation:
%\begin{equation}
%  \label{eq:safe.group.normalization}
%    \layerFunc{k}(x_k^{(i)})=\frac{x_k^{(i)}-\mu^{(i)}}{\max(1, \sigma^{(i)})}.
%\end{equation}
%This version
%This ensures a Lipschitz value of $\sqrt{|x_k^{(i)}|}$ for the input of the normalization layer, with $|x_k^{(i)}|$ the number of pixels or features within the set $\set{k}{i}$.
Note that a group normalization layer has no trainable parameters.
%It's important to note that we employ a group normalization layer without any trainable parameters, eliminating any Lipschitz value associated with the parameters.

\paragraph{Linear layers.} \cite{gouk2020regularisation} presents a formulation for 
the Lipschitz value of linear layers in relation to p-norms. This formulation 
can be extended to determine the Lipschitz value with respect to the parameters.
Therefore, if $\layerFunc{k}$ is a linear layer, then

\begin{equation}\label{eq:linear_lip}
  \begin{aligned}
    \left\|\layerPartialWeight{k}\right\|_p = \left\| \frac{\partial (W_k^{\top} x_k + B_k)}{\partial (W_k,B_k)} \right \|_p = \|(\vec{x_k},1)\|_p, \\
    \left\|\layerPartialInput{k}\right\|_p = \left\| \frac{\partial (W_k^{\top} x_k+B_k)}{\partial x_k} \right \|_p = \|W_k\|_p,
  \end{aligned}
\end{equation}
with $\vec{x_k}$ the serialized vector of $x_k$.


\paragraph{Convolutional layers.} There are many types of convolutional layers, e.g., depending on the data type (strings, 2D images, 3D images \ldots), shape of the filter (rectangles, diamonds \ldots).  Here we provide as an example only a derivation for convolutional layers for 2D images with rectangular filter.
In that case, the input layer consists of $n_k = c_{in} h w $ nodes and the output layer consists of $n_{k+1}= c_{out} hw$ nodes with $c_{in}$ input channels, $c_{out}$ output channels, $h$ the height of the image and $w$ the width.
Then, $\theta_k\in\mathbb{R}^{c_{in}\times c_{out}\times h' \times w'}$ with $h'$ the height of the filter and $w'$ the width of the filter.  Indexing input and output with channel and coordinates, i.e., $x_k\in \mathbb{R}^{c_{in}\times h\times w}$ and $x_{k+1}\in \mathbb{R}^{c_{out}\times h \times w}$ we can then write
\[
x_{k+1,c,i,j} = \sum_{d=1}^{c_{in}} \sum_{r=1}^{h'} \sum_{s=1}^{w'} x_{k,d,i+r,j+s} \theta_{k,c,d,r,s}
\]
where components out of range are zero.
% We can derive 
% % (see Appendix \ref{sec:bound.lipschitz.convol} for details) 
% that



%\[
%\|x_{k+1}\|_2^2 = \sum_{c,i,j} \left(\sum_{d=1}^{c_{out}} \sum_{r=1}^{h'} \sum_{s=1}^{w'} x_{k,d,i+r,j+s} \theta_{k,c,d,r,s} \right)^2
%\]

\begin{theorem}\label{th:ineq.conv}
  The convolved feature map $(\theta \ast \cdot): \mathbb{R}^{n_k \times |x_k|} \rightarrow \mathbb{R}^{n_{k+1} \times n \times n}$, with zero or circular padding, is Lipschitz and
  \begin{equation}
    \|\nabla_{\theta_k} (\theta_k \ast x_k) \|_p \leq \sqrt{h'w'}\|x_k\|_p
    \text{ and }
    \|\nabla_{x_k} (\theta_k \ast x_k) \|_p \leq \sqrt{h'w'}\|\theta_k\|_p
  \end{equation}
  with $w'$ and $h'$ the width and the height of the filter.
\end{theorem}

\begin{proof}
The output $x_{k+1} \in \mathbb{R}^{c_{o u t} \times n \times n}$ of the convolution operation is given by:
\[
x_{k+1, c, r, s}=\sum_{d=0}^{c_{i n}-1} \sum_{i=0}^{h'-1} \sum_{j=0}^{w'-1} x_{k, d, r+i, s+j} \theta_{k, c, d, i, j}
\]
There follows, for any $p \in \{1, 2, +\infty\}$:
\begin{eqnarray*}
  \|x_{k+1}\|^p_p
  &=& \sum_{c=0}^{c_{o u t}-1} \sum_{r=1}^n \sum_{s=1}^n \left| \sum_{d=0}^{c_{i n}-1} \sum_{i=0}^{h'-1} \sum_{j=0}^{w'-1} x_{k,d, r+i, s+j} \theta_{k, c, d, i, j}\right|^p \\
  &\le&  \sum_{c=0}^{c_{o u t}-1} \sum_{r=1}^n \sum_{s=1}^n \left( \sum_{d=0}^{c_{i n}-1} \sum_{i=0}^{h'-1} \sum_{j=0}^{w'-1} |x_{k,d, r+i, s+j}|^p \right) \left( \sum_{d=0}^{c_{i n}-1} \sum_{i=0}^{h'-1} \sum_{j=0}^{w'-1} |\theta_{k, c, d, i, j}|^p\right) \text{(triangle inequality)}\\ %, $p \in \{1, 2, +\infty\}$
  &=&  \left( \sum_{d=0}^{c_{i n}-1} \sum_{i=0}^{h'-1} \sum_{j=0}^{w'-1}  \sum_{r=1}^n \sum_{s=1}^n |x_{k,d, r+i, s+j}|^p \right) \left(  \sum_{c=0}^{c_{o u t}-1} \sum_{d=0}^{c_{i n}-1} \sum_{i=0}^{h'-1} \sum_{j=0}^{w'-1} |\theta_{k, c, d, i, j}|^p\right)  \\
  &\le& h'w' \left( \sum_{d=0}^{c_{i n}-1} \sum_{r=1}^n \sum_{s=1}^n |x_{k,d, r, s}|^p \right) \left(  \sum_{c=0}^{c_{o u t}-1} \sum_{d=0}^{c_{i n}-1} \sum_{i=0}^{h'-1} \sum_{j=0}^{w'-1} |\theta_{k, c, d, i, j}|^p\right)  \\
  &=& h'w' \|x_k \|^p_p \|\theta_k\|^p_p
\end{eqnarray*}


Since $\theta_k\ast \cdot$ is a linear operator:
\begin{equation*}
  \|(\theta_k \ast x_k) - (\theta_k^\prime \ast x_k)\|_p = \|(\theta_k - \theta_k^\prime) \ast x_k\|_p \leq \|\theta_k - \theta_k^\prime\|_p (h'w')^{\frac{1}{p}}\|x_k\|_p
\end{equation*}

Finally, the convolved feature map is differentiable so the spectral norm of its Jacobian is bounded by its Lipschitz value:
\begin{equation*}
  \|\nabla_{\theta_k} (\theta_k \ast x_k) \|_p \leq (h'w')^{\frac{1}{p}}\|x_k\|_p
\end{equation*}
Analogously,
\begin{equation*}
  \|\nabla_{x_k} (\theta_k \ast x_k) \|_p \leq (h'w')^{\frac{1}{p}}\|\theta_k\|_p
\end{equation*}
\end{proof}


From \cref{th:ineq.conv}, we state that,
\begin{equation}\label{eq:conv_lip_input}
  \begin{aligned}
    \left\|\layerPartialInput{k}\right\|_p
    %= \left\| \frac{\partial \theta_k \ast x_k}{\partial x_k} \right \|_2
    \leq  (h'w')^{\frac{1}{p}}\|\theta_k\|_p
  \end{aligned}
\end{equation}

  \begin{equation}\label{eq:conv_lip_weight}
    \left\|\layerPartialWeight{k}\right\|_p
    \leq (h'w')^{\frac{1}{p}}\|\vec{x_k}\|_p
  \end{equation}

\paragraph{Residual connections} Resnet architectures \cite{resnet} are usually based on residual blocks:
\begin{equation*}
  f^{(j+k)}_{\theta_{j+k}}(x_j)=x_j + (f^{(j+k-1)}_{\theta_{j+k-1}}\circ \ldots \circ f_{\theta_j}^{(j)})(x_j)
\end{equation*}
\cite{gouk2020regularisation} shows that the Lipschitz value of residual blocks is bounded by
\begin{equation}
  \left\|\frac{\partial f^{(k+j)}_{\theta_{k+j}}}{\partial x_j}\right\|_p \le 1 + \prod_{i=j}^{j+k-1} \left\| \frac{\partial f^{(i)}_{\theta_i}}{\partial x_i}\right\|_p .
\end{equation}
We summarize the upper bounds of the Lipschitz values, either on the input or on the parameters, for each layer type in \cref{tab:lip}.
We can conclude that networks for which the norms of the parameter vectors $\theta_k$ are bounded, are Lipschitz networks as introduced in \cite{miyato2018spectral}, i.e., they are FNN for which each layer function $f_{\theta_k}^{(k)}$ is Lipschitz.  We will denote by $\thetaSpaceLeC$ and by $\thetaSpaceEqC$ the sets of all paremeter vectors $\theta$ for $f_\theta$ such that $\|\theta_k\|\le C$ and $\|\theta_k\|=C$ respectively, for $k=1\ldots K$.

% \paragraph{Computing sensitivty.} We will denote by $L_{\theta_k}$ an upper bound for $\left\|\layerPartialWeight{k}\right\|$ and by $L_{x_k}$ an upper bound for $\left\|\layerPartialInput{k}\right\|$. We can now introduce \cref{alg:layer_sensitivity} to compute the sensitivity $\Delta_k$ of layer $k$.  Here we denote by $X_k$ the maximal possible norm of $x_k$, i.e., for all possible inputs $x_k$, $\|x_k\|=\left\|(f^{(k-1)}_{\theta_{k-1}}\circ \ldots \circ f_{\theta_1}^{(1)})(x_1)\right\|\le X_k$, with $X_1$ the norm on which we scale every input $x_1$.
% It capitalizes on a forward pass to compute the maximal input norms $X_k$ --- which depends on the previous layer range, in the case of normalization or activation layers, or on $u_{k-1}^{\theta} = \min(C, \|\tilde{\theta}_{k-1}\|)$ (see \cref{alg:weight-dpsgd}), in the case of linear or convolutional layers (one can verify for each type of layer that $u_{k-1}^{\theta}$ is an upper bound for $\|x_{k}\|/\|x_{k-1}\|$) --- and a backward pass applying Equation \ref{eq:layer_sensitivity}.

% \input{subfiles/algos/layer_sensitivity.tex}


% add tanh, review cross entropy, review caption, put caption on top, get same
% layout as below
\begin{table}[ht]
  \caption{Summary table of the upper bounds of the Lipschitz values, either on the 
  input (Lip. on $x_k$) or on the parameters (Lip. on $\theta_k$), for each layer type. 
  Note that for the loss, the Lipschitz value is 
  solely dependent on the output $x_{K+1}$. Please refer to \cref{sec:prel.traindn} for more details on loss functions.
  }\label{tab:lip}
  \centering
  \small\addtolength{\tabcolsep}{-5pt}
  \begin{tabular}{cccc}
  \hline
  Layer & Definition & Lip.{} on $x_k$ & Lip.{} on $\theta_k$ \\
  \hline
  Dense & $\theta_k^{\top}x_k $ & $\|\theta_k\|$ & $\|x_k\|$ \\
  % \hline
  Convolutional & $\theta_k \ast x_k$ & $\sqrt{h^\prime w^\prime}\|\theta_k\|$ & $\sqrt{h^\prime w^\prime}\|x_k\|$ \\
  % \hline
  Normalization &  $(x_k^{(k:\grpidx)}-\mu^{(k:\grpidx)})/\max(\alpha, \sigma^{(k:\grpidx)})$ & $1/\alpha$ & - \\
  % \hline
  ReLU & $\max(x_k, 0)$ & $1$ & - \\
  % \hline
  Sigmoid & $1/(1+e^{-x_k})$ & $1$ & - \\
  % \hline
  Tanh & $(e^x - e^{-x})(e^x + e^{-x})$ & $1$ & - \\
  % \hline
  Softmax Cross-entropy & $y^{\top}\log\left(\textsc{softmax}(x_{K+1})\right) / \tau$ & $\sqrt{2}/\tau$ & - \\
  % \hline
  Cosine Similarity & $x_{K+1}^{\top}y/(\|x_{K+1}\|\|y\|)$ & $1/\min \|x_{K+1}\|$ & - \\
  % \hline
  Multiclass Hinge & $\sum_{\substack{i=1 \\ i\neq \argmax y}}^{c} \max(0, m - x_{K+1} \cdot y + x_{K+1, i}) / c$ & $1$ & - \\
  \hline
  \end{tabular}
  \end{table}



\subsection{Backpropagation}\label{subsec:backprop}

% Modern deep learning libraries, including PyTorch \cite{pytorch} and TensorFlow \cite{tensorflow2015-whitepaper}, utilize efficient backpropagation implementations grounded in automatic differentiation \cite{rall_automatic_1981}.
Consider a feed-forward network $f_\theta$.  We define $\mathcal{L}_k(\theta,(x_k,y))=\ell\left(\left(f_{\theta_K}^{(K)}\circ \ldots \circ f_{\theta_k}^{(k)}\right)(x_k), y\right)$.
For feed-forward networks, the chain rule gives:
% todo: update with notation from alg
\begin{equation}\label{eq:back_prop}
  \begin{aligned}
    \lossPartialInput{k} & = \frac{\partial\ell}{\partial x_{K+1}}\frac{\partial f_{\theta_K}^{(K)}}{\partial x_k}.
  \end{aligned}
\end{equation}
Any matrix or vector norm is submultiplicative, especially the $\|\cdot\|_{p}$ norm with $p \in \{1, 2, +\infty\}$, hence:
\begin{equation}\label{eq:back_prop}
  \begin{aligned}
    \left\|\lossPartialInput{k}\right\|_{p} & \leq \left\| \frac{\partial\ell}{\partial x_{K+1}}\right\|_{p} \left\| \frac{\partial f_{\theta_K}^{(K)}}{\partial x_k}\right\|_{p}.
  \end{aligned}
\end{equation}
In \cref{subsec:lip_values}, we show that the Lipschitz value with regard to the input of $f_{\theta_k}^{(k)}$ is bounded and the bound depends on the norm of the parameters
in the case of linear or convolutional layers. Let $c_k$ be the Lipschitz value with regards to the input of any layer $k$.
As $f_{\theta_k}$ is Lipschitz constrained, when the layer $k$ has parameters, $c_k$ is a linear function of $\min(C, \|\theta_k\|)$,
 as stated in \cref{eq:linear_lip,eq:conv_lip_input}, with $C$ the maximum weight norm.
If $\left\| \frac{\partial\ell}{\partial x_{K+1}}\right\|_{p} \leq \tau$, where $\ell$ represents the loss then,
\begin{equation}\label{eq:backprop.ineq.partial_input}
  \begin{aligned}
    \left\|\lossPartialInput{k}\right\|_{p} & \leq \tau \prod_{i=k}^K c_i.
  \end{aligned}
\end{equation}

We now consider the induced $\|\cdot\|_{2,p}$ norm ($\|A\|_{\alpha, \beta}=\sup _{x \neq 0} \frac{\|A x\|_\beta}{\|x\|_\alpha}$) for studiying $\lossPartialWeight{k}$. Induced norms are consistent, then one can prove that $\|A B\|_{\alpha, \gamma} \leq\|A\|_{\beta, \gamma}\|B\|_{\alpha, \beta}$ \cite{two-to-infinity-norm} which, applied to the chain rule, gives:
\begin{equation}\label{eq:backprop.ineq.partial_weight}
  \begin{aligned}
    \left\|\lossPartialWeight{k}\right\|_{2,p} & \leq \left\|\lossPartialInput{k+1}\right\|_{p}\left\|\layerPartialWeight{k}\right\|_{2, p}
  \end{aligned}
\end{equation}

Combining \ref{eq:backprop.ineq.partial_input} and \ref{eq:backprop.ineq.partial_weight} 
provides an upper bound of the $2,p$-norm of the gradient at layer $k$,
\begin{equation}\label{eq:backprop.ineq.grad}
  \begin{aligned}
    \left\|\lossPartialWeight{k}\right\|_{2,p} & \leq \tau \prod_{i=k+1}^K c_i\left\|\layerPartialWeight{k}\right\|_{2, p}
  \end{aligned}
\end{equation}

If $\theta_k \neq \emptyset$, then \cref{eq:linear_lip,eq:conv_lip_weight} show that the upper bound of $\left\|\layerPartialWeight{k}\right\|_{2, p}$
depends on $\|\vec{x_k}\|_2$. Hence,

\begin{equation}\label{eq:backprop.ineq.grad.modified}
  \begin{aligned}
    \left\|\lossPartialWeight{k}\right\|_{2,p} & \leq \tau \prod_{i=k+1}^K c_i X_k,
  \end{aligned}
\end{equation}

with $X_k = \sqrt{hw}\max\limits_{x_k \in V_k}\|\vec{x_k}\|_2$
with $hw=1$ in case of a linear layer and $V_k = \{(f_k\circ \ldots \circ f_1)(x)|(x,y)\in V\}$.

\subsection{\weightDpSgd}\label{subsec:lipdpsg}
We introduce in \cref{alg:weight-dpsgd} a novel differentially private stochastic gradient descent algorithm, called \weightDpSgd, that leverages the estimation of the per-layer sensitivity of the model to provide differential privacy without gradient clipping.

\input{src/emp-opt-files/subfiles/algos/lip_dp_sgd.tex}

\paragraph{Differential Privacy.}  From \cref{eq:backprop.ineq.grad.modified} follows,
\begin{equation}\label{eq:gradient.modified}
  \forall (x, y) \in \mathcal{X}\times\mathcal{Y}, \quad \left\|\frac{\nabla_{\tilde{\theta}_k}\ell(f_{\tilde{\theta}}(x), y)}{X_k} \right\|_{2,p} \leq \tau\prod_{i=k+1}^{K}c_i
\end{equation}
The left-hand side of \cref{eq:gradient.modified} represents the gradient scaled by the maximum input norm. 
The $\ell_{2,p}$-sensitivity of this scaled gradient is bounded, 
with the upper-bound depending solely on the Lipschitz constant of the loss function $\tau$, 
the maximum parameter norm $C$, and/or the parameter norm $\|\tilde{\theta_k}\|_{2,p}$.

\begin{theorem}
  Given a feed-forward model $\fnnFunc$ composed of Lipschitz constrained operators and a Lipschitz loss $\ell$, $\textsc{\weightDpSgd}$ is differentially private.
\end{theorem}

Indeed, the scaled gradient's sensitivity is determined without any privacy costs, as it depends only on the current parameter values (which are privatized in the previous step, and post-processing privatized values doesn't take additional privacy budget) and not on the data.
If the selected norm is the \(2,2\)-norm, the Gaussian mechanism can be applied to ensure privacy by utilizing the sensitivity of the scaled gradient. Otherwise, the exponential mechanism can be employed to achieve differential privacy \cite{mcsherry2007mechanism} using a custom score function.
\paragraph{Privacy accounting.} \weightDpSgd{} adopts the same privacy accounting as \gradDpSgd. Specifically, the accountant draws upon the privacy amplification \cite{kasiviswanathan_what_2010} brought about by Poisson sampling and the Gaussian moment accountant \cite{abadi_deep_2016}. It's worth noting that while we utilized the Renyi Differential Privacy (RDP) accountant \cite{abadi_deep_2016,mironov_renyi_2019} in our experiments, \weightDpSgd{} is versatile enough to be compatible with alternative accountants.
Note that RDP is primarily designed to operate with the Gaussian mechanism, but \cite{pmlr-v89-wang19b} demonstrate its applicability with other differential privacy mechanisms, particularly the exponential mechanism.

\paragraph{Requirements.} As detailed in Section \ref{subsec:lip_values}, the loss and the model operators need to be Lipschitz. We've enumerated several losses and operators that meet this criterion in \Cref{tab:lip}. While we use $\textsc{ClipWeights}$ to characterize Lipschitzness \cite{yoshida_spectral_2017,miyato2018spectral} in our study \ref{subsec:lip_values}, other methods are also available, as discussed in \cite{arjovsky_wasserstein_2017}.

\paragraph{ClipWeights.} The $\textsc{ClipWeights}$ function is essential to the algorithm, ensuring Lipschitzness, which facilitates model sensitivity estimation. As opposed to standard Lipschitz-constrained networks \cite{yoshida_spectral_2017,miyato2018spectral} which increase or decrease the norms of parameters to make them equal to a pre-definied value, our approach normalizes weights only when their current norm exceeds a threshold. This results in adding less DP noise for smaller norms. Importantly, as $\tilde{\theta}$ is already made private in the previous iteration, its norm is private too.
Note that, when the norm used is the $2$-norm i.e., $\textsc{ClipWeights}$ is a spectral normalization, we perform also a Björck orthogonalization for fast and near-orthogonal convolutions \cite{orthogonal_convolution}.
% there's no privacy concern, as parameters, being private, ensure their norm's privacy.
%Moreover, to optimize computation, we limit spectral normalizations, leveraging stored parameter norms if they fall below the threshold, hence the utilization of $\thetanorm$ in \cref{alg:weight-dpsgd}.

\paragraph{Computing norms} 
The $\ell_{2,1}$ and $\ell_{2,\infty}$ norms can be computed exactly in linear time relative to the number of elements. However, calculating the $\ell_{2,2}$ norm, which corresponds to the largest singular value of the matrix, is infeasible using standard singular value decomposition techniques. To address this efficiently, we use techniques based on power method \cite{gouk2020regularisation} that leverage the backward computational graph of modern deep learning libraries like \cite[TensorFlow]{tensorflow2015-whitepaper} and \cite[PyTorch]{pytorch}. 

% [merge with following and add orthonormalization]

% \paragraph{Computation techniques.} For both \cref{alg:layer_sensitivity} and  $\textsc{ClipWeights}$ it's crucial to compute the greatest singular matrix values efficiently. A renowned technique is the \textit{power method} \cite{MisesPraktischeVD}. If this isn't sufficiently fast,
% %when dealing with convolutions, the related matrix might be of large dimensions, rendering the direct application of the power method infeasible. To address this,
% the \textit{power method} can be enhanced using AutoGrad \cite{scaman_lipschitz_2019}.
% Another idea is to use the Frobenius norm, which is faster to compute but may have drawbacks in terms of tightly bounding the norm.
% As computing spectral norms is relatively costly, we avoid to recompute them by storing them in $\thetanorm$ in \cref{alg:weight-dpsgd}.


\subsection{Avoiding the bias of gradient clipping}
\label{sec:avoid.bias}

% Our \weightDpSgd{} algorithm finds a local optimum (for $\theta$) of $F(\theta,Z)$ in $\thetaSpaceLeC$ while \gradDpSgd{} doesn't necessarily find a local optimum of $F(\theta,Z)$ in $\thetaSpace$.
% In particular, we prove in Appendix \ref{app:bias} the following
% \newcommand{\thmStmWeightClipConverges}{
%   Let $F$ be an objective function as defined in Section \ref{sec:erm}, and $Z$, $f_\theta$, $\mathcal{L}$, $\Theta=\thetaSpaceLeC$, $T$, $\sigma=0$, $s$, $\eta$ and $C$ be input parameters of \weightDpSgd{} satisfying the requirements specified in Section \ref{subsec:lipdpsg}.
%   Assume that for these inputs \weightDpSgd{} converges to a point $\theta^*$ (in the sense that $\lim_{k,T\to\infty} \theta_k = \theta^*$).
%   Then, $\theta^*$ is a local optimum of $F(\theta,Z)$ in $\thetaSpaceLeC$.
% %  For any objective function $F$ as defined in Section \ref{sec:erm}, \weightDpSgd{} converges to a local optimum of $F(\theta,Z)$ in $\thetaSpaceLeC$.
% }
% \begin{theorem}
% \label{thm:weightClipConverges}\thmStmWeightClipConverges{}
% \end{theorem}
% Essentially, making abstraction of the unbiased DP noise, the effect of scaling weight vectors to have bounded norm after a gradient step is equivalent to projecting the gradient on the boundary of the feasible space if the gradient brings the parameter vector out of $\thetaSpaceLeC$.

We show that \weightDpSgd{} converges to a local minimum in $\thetaSpaceLeC$ while \gradDpSgd{} suffers from bias and may converge to a point which is not a local minimum of $\thetaSpace$.

We use the word 'converge' here somewhat informally, as in each iteration independent noise is added the objective function slightly varies between iterations and hence none of the mentioned algorithms converges to an exact point.
We here informally mean approximate convergence to a small region, assuming a sufficiently large data set $Z$ and/or larger $\epsilon$ such that privacy noise doesn't significantly alter the shape of the objective function.  Our argument below hence makes abstraction of the noise for simplicity, but in the presence of small amounts of noise a similar argument holds approximately, i.e., after sufficient iterations \weightDpSgd{} will produce $\theta$ values close to a locally optimal $\theta^*$ while \gradDpSgd{} may produce $\theta$ values in a region not containing the relevant local minimum.

First, let us consider convergence.

% \textbf{Theorem} \ref{thm:weightClipConverges}.  \thmStmWeightClipConverges{}
\newcommand{\thmStmWeightClipConverges}{
  Let $F$ be an objective function as defined in Section \ref{sec:erm}, and $Z$, $f_\theta$, $\mathcal{L}$, $\Theta=\thetaSpaceLeC$, $T$, $\sigma=0$, $s$, $\eta$ and $C$ be input parameters of \weightDpSgd{} satisfying the requirements specified in Section \ref{subsec:lipdpsg}.
  Assume that for these inputs \weightDpSgd{} converges to a point $\theta^*$ (in the sense that $\lim_{k,T\to\infty} \theta_k = \theta^*$).
  Then, $\theta^*$ is a local optimum of $F(\theta,Z)$ in $\thetaSpaceLeC$.
%  For any objective function $F$ as defined in Section \ref{sec:erm}, \weightDpSgd{} converges to a local optimum of $F(\theta,Z)$ in $\thetaSpaceLeC$.
}
\begin{theorem}
\label{thm:weightClipConverges}\thmStmWeightClipConverges{}
\end{theorem}

\begin{proof}[Proof sketch]
  We consider the problem of finding a local optimum in $\thetaSpaceLeC$:
  \[
    \begin{array}{ll}
      \text{minimize} & F(\theta,Z) \\
      \text{subject to} & \|\theta\|_2 \le C
    \end{array}
  \]
  We introduce a slack variable $\zeta$:
   \[
    \begin{array}{ll}
      \text{minimize} & F(\theta,Z) \\
      \text{subject to} & \|\theta\|_2 + \zeta^2 = C
    \end{array}
  \]
  Using Lagrange multipliers, we should minimize
  \[
    F(\theta,Z) - \lambda (\|\theta\|_2 + \zeta^2 - C)
  \]
  An optimum in $\theta$, $\lambda$ and $\zeta$ satisfies
  \begin{eqnarray}
    \label{eq:LeC.lagrange.1} \nabla_\theta F(\theta, Z) - \lambda \theta &=& 0\\
    \label{eq:LeC.lagrange.2} \|\theta\|_2 + \zeta^2 - C &=& 0\\
    \label{eq:LeC.lagrange.3} 2\lambda \zeta &=&0
  \end{eqnarray}
  From Eq \ref{eq:LeC.lagrange.3}, either $\lambda=0$ or $\zeta=0$
  If $\zeta>0$, $\theta$ is in the interior of $\thetaSpaceLeC$ and there follows $\lambda=0$ and from Eq \ref{eq:LeC.lagrange.1} that $\nabla_\theta F(\theta, Z) =0$.  For such $\theta$, \weightDpSgd{} does not perform weight clipping.  If the learning rate is sufficiently small, and if it converges to a $\theta$ with norm $\|\theta\|_2<C$ it is a local optimum.
  On the other hand, if $\zeta=0$, there follows from Eq \ref{eq:LeC.lagrange.2} that $\|\theta\|_2=C$, i.e., $\theta$ is on the boundary of $\thetaSpaceLeC$.
  If $\theta$ is a local optimum in $\thetaSpaceLeC$, then $\nabla_\theta F(\theta,Z)$ is perpendicular on the ball of vectors $\theta$ with norm $C$, and for such $\theta$ \weightDpSgd{} will add the multiple $\eta(t).\nabla_\theta F(\theta,Z)$ to $\theta$ and will next scale $\theta$ back to norm $C$, leaving $\theta$ unchanged.  For a $\theta$ which is not a local optimum in $\thetaSpaceLeC$, $\nabla_\theta F(\theta,Z)$ will not be perpendicular to the ball of $C$-norm parameter vectors, and adding the gradient and brining the norm back to $C$ will move $\theta$ closer to a local optimum on this boundary of $\thetaSpaceLeC$.  This is consistent with Eq \ref{eq:LeC.lagrange.1} which shows the gradient with respect to $\theta$ for the constrained problem to be of the form $\nabla_\theta F(\theta, Z) - \lambda \theta$.
\end{proof}


Theorem \ref{thm:weightClipConverges} shows that in a noiseless setting, if \weightDpSgd{} converges to a stable point that point will be a local optimum in $\thetaSpaceLeC$.  In the presence of noise and/or stochastic batch selection, algorithms of course don't converge to a specific point but move around close to the optimal point due to the noise in each iteration, and advanced methods exist to examine such kind of convergence.  The conclusion remains the same: \weightDpSgd will converge to a neighborhood of the real local optimum, while as we argue \gradDpSgd{} will often converge to a neighborhood of a different point.

Second, we argue that \gradDpSgd{} introduces bias. This was already pointed out in \cite{chen_understanding_2021}'s examples 1 and 2.  In Section \ref{app:exp.clipfreq} we also showed experiments demonstrating this phenomenon.  Below, we provide a simple example which we can handle (almost) analytically.
%we show with an example that \gradDpSgd{} may introduce bias and does not have this desirable property.

A simple situation where bias occurs and \gradDpSgd{} does not converge to an optimum of $F$ is when errors aren't symmetrically distributed, e.g., positive errors are less frequent but larger than negative errors.

Consider the scenario of simple linear regression.
A common assumption of linear regression is that instances are of the form $(x_i,y_i)$ where $x_i$ is drawn from some distribution $P_x$ and $y_i=ax_i+b+e_i$ where $e_i$ is drawn from some zero-mean distribution $P_e$.  When no other evidence is available, one often assume $P_e$ to be Gaussian, but this is not necessarily the case.  Suppose for our example that $P_x$ is the uniform distribution over $[0,1]$ and $P_e$ only has two possible values, in particular $P_e(9)=0.1$, $P_e(-1)=0.9$ and $P_e(e)=0$ for $e\not\in\{9,-1\}$.  So with high probability there is a small negative error $e_i$ while with small probability there is a large positive error, while the average $e_i$ is still $0$.
Consider a dataset $Z=\{(x_i,y_i)\}_{i=1}^n$.
Let us consider a model $f(x) = \theta_1 x \theta_2$ and
let us use the square loss $\mathcal{L}(\theta,Z)=\sum_{i=1}^n \ell(x_i,y_i)/n$ with $\ell(\theta, x,y) = (\theta_1 x + \theta_2 - y)^2$.
Then, the gradient is
\[
  \nabla_\theta \ell(\theta, x,y) = \left( 2(\theta_1 x + \theta_2 -y) x, 2(\theta_1 x + \theta_2 - y)\right)
\]
For an instance $(x_i,y_i)$ with $y_i = ax_i+b+e_i$, this implies
\[
  \nabla_\theta \ell(\theta, x_i,y_i) = \left( 2((\theta_1-a) x_i + (\theta_2-b) - e_i) x_i, 2((\theta_1-a) x_i + (\theta_2-b) - e_i)\right)
\]
For sufficiently large datasets $Z$ where empirical loss approximates population loss, the gradient considered by \weightDpSgd{} will approximate
\begin{eqnarray*}
  \nabla_\theta \mathcal{L}(\theta,Z)
  &\approx&  \sum_{e\in \{10,\}} P_e(e) \int_0^1 \nabla_\theta \ell(\theta, x, ax+b+e) \hbox{d}x \\
  &=&  \sum_{e\in \{10,\}} P_e(e) \int_0^1 \left( 2((\theta_1-a) x + (\theta_2-b) - e) x, 2((\theta_1-a) x + (\theta_2-b) - e)\right) \hbox{d}x \\
  &=&  \int_0^1 \left( 2((\theta_1-a) x^2 + (\theta_2-b)x - x\mathbb{E}[e]), 2((\theta_1-a) x + (\theta_2-b) - \mathbb{E}[e])\right) \hbox{d}x \\
  &=&  \left( 2((\theta_1-a)/3 + (\theta_2-b)/2), 2((\theta_1-a)/2 + (\theta_2-b))\right)
\end{eqnarray*}
This gradient becomes zero if $\theta_1=a$ and $\theta_2=b$ as intended.

However, if we use gradient clipping with threshold $C=1$ as in \gradDpSgd{}, we get:
\begin{eqnarray*}
  {\tilde{g}}
  &\approx&  \sum_{e\in \{10,\}} P_e(e) \int_0^1 clip_1\left(\nabla_\theta \ell(\theta, x, ax+b+e)\right) \hbox{d}x \\
  &=&  \sum_{e\in \{10,\}} P_e(e) \int_0^1 clip_1\left(
       \left( 2((\theta_1-a) x + (\theta_2-b) - e) x, 2((\theta_1-a) x + (\theta_2-b) - e)\right)
      \right) \hbox{d}x \\
\end{eqnarray*}
While for a given $e$ for part of the population $(\theta_1-a)x+\theta_2-b$ may be small, for a fraction of the instances the gradients are clipped.  For the instances with $e=9$ this effect is stronger.  The result is that for $\theta_1=a$ and $\theta_2=b$ the average clipped gradient $\tilde{g}$ doesn't become zero anymore, in particular $\|\tilde{g}\|=0.7791$.  In fact, $\tilde{g}$ becomes zero for $\theta_1=a+0.01765$ and $\theta_2=b+0.94221$.  Figure \ref{fig:grad_clip_bias} illustrates this situation.

\begin{figure}[htb]
  \label{fig:grad_clip_bias}
  \caption{An example of gradient clipping causing bias, here the average gradient becomes zero at $(0,0)$ while the average clipped gradient is $0$ at another point, causing convergence of \gradDpSgd{} to that point rather than the correct one.}
  \begin{center}
    \includegraphics[width=0.5\linewidth]{linreg_cg_norm.png}
    \end{center}
\end{figure}



Furthermore, \cite{chen_understanding_2021} shows an example showing that gradient clipping can introduce bias.
Hence, \gradDpSgd{} does not necessarily converge to a local optimum of $F(\theta,Z)$, even when sufficient data is available to estimate $\theta$.  While \weightDpSgd{} can only find models in $\thetaSpaceLeC$ and this may introduce another suboptimality, as our experiments will show this is only a minor drawback in practice, while also others observed that Lipschitz networks have good properties \cite{bethune_pay_2022}.  Moreover, it is easy to check whether \weightDpSgd{} outputs parameters on the boundary of $\thetaSpaceLeC$ and hence the model could potentially improve by relaxing the weight norm constraint.  In contrast, it may not be feasible to detect that \gradDpSgd{} is outputting potentially suboptimal parameters.  Indeed, consider a federated learning setting (e.g., \cite{Bonawitz2017a}) where data owners collaborate to compute a model without revealing their data.  Each data owner locally computes a gradient and clips it, and then the data owners securely aggregate their gradients and send the average gradient to a central party updating the model.  In such setting, for privacy reasons no party would be able to evaluate that gradient clipping introduces a strong bias in some direction.  Still, our experiments show that in practice at the time of convergence for the best hyperparameter values clipping is still active for a significant fraction of gradients (See \Cref{fig:dropout_clip})
