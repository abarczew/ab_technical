\chapter{Introduction}

The past few decades have been marked by unprecedented advances in
artificial intelligence, driven by machine learning. This success is
due to the remarkable alignment of three factors: the development of
more expressive model architectures, together with a gigantic increase
in computing power, and, most importantly, the availability of
voluminous data. This has led to the utilization of machine learning
in many domains. Machine learning has notably become the backbone of
many industrial products, ranging from recommendations in social
networks to fraud detection or self-driving cars. It is also at the
core of many important scientific discoveries across many fields of
research like medicine, pharmacy, social sciences, and many others.

In most of these applications, there is a serious tension between the
importance of the possible discoveries, the privacy of individuals
whose data is used for training models, and the fairness of the
predictions. Indeed, it is now well-known that machine learning models
trained on sensitive data tend to leak confidential
information. Similarly, usual model training procedures often
transcribe and amplify underlying discrimination in the data, and can
even create new sources of discrimination. While machine
learning-enabled discoveries can be very profitable for humanity (\eg
discovery of new drugs or risk management), failing to address these
privacy and fairness issues can have dramatic consequences on
individuals (\eg blackmailing, discrimination, public shame, and, in
extreme cases, fatality). The increased awareness of the risks
incurred by using sensitive data at such a large scale has given birth
to the fields of privacy-preserving and fair machine learning.

To preserve data privacy, new algorithms for training machine
learning models have emerged. These algorithms are designed to
guarantee a robust notion of privacy, that has now become standard:
differential privacy. Yet, training models in this way ineluctably
results in more imprecise models. There is thus a trade-off between
data privacy and the models' utility. This trade-off is harsh, and in many
cases (\eg for high-dimensional models), existing algorithms have
trouble learning useful models while guaranteeing meaningful
privacy. Furthermore, there are growing concerns that these
differentially private training algorithms may result in disparate
impact, which could exacerbate discrimination even more.

In this thesis, we explore how structural properties of the problem at
hand influence the privacy-utility trade-off, and the impact of
enforcing privacy on the fairness of predictions in machine
learning. We propose new differentially private training methods based
on coordinate descent. These methods can improve the privacy-utility
trade-off beyond known lower bounds by exploiting structural
properties like imbalance in the model's parameters or sparsity of the
solution. We then study the impact of privacy on fairness and derive
upper bounds on the difference in fairness between private and
non-private models.


\section{Context on Supervised Learning}
\label{sec:cont-superv-learn}

In this thesis, we focus on supervised learning. In this paradigm of
machine learning, we aim at predicting a label based on some
features. This is a very general framework, as labels can take many
different forms. It includes many different tasks, such as:
\begin{itemize}
\item classification: categorical labels (\eg identifying
  fractured bones on an X-ray),
\item regression: continuous labels (\eg predicting the price of a
  house),
\end{itemize}
and some other problems, notably in computer vision (\eg image
segmentation), that we do not consider in this thesis.

To make this prediction, we train a model on a set of training
data. This data contains labeled records (\ie pair of features and
label), that all describe the same unknown underlying phenomenon. We
assess the soundness of a model's prediction on a training record with
a \emph{loss function}. This function measures how close the model's
predicted label is to the true label: it is small if the predicted
label is right, and high otherwise. For instance, in regression tasks,
this can simply be the square of the difference between these two
labels.

To measure the fitness of a model on a training dataset, we use the
average value of the loss function over each record. This value is
called the \emph{empirical risk}, and it gives a measure of utility of
the model: a small empirical risk means that the model's predictions
on the training data are, on average, good.

\paragraph{Empirical Risk Minimization.}

Learning a model amounts to finding a model that has a small empirical
risk. Typically, we define an \emph{hypothesis class}, that is a set
of models (\eg linear models), and search for the one whose empirical
risk is minimal. This process is called \emph{empirical risk
  minimization}.
% \TODO{Likewise: you can mention deep learning, may make it clear early that coordinate descent is not the current method of choice for deep learning}
To solve this problem, we generally chose a parametric hypothesis
class, and we optimize over the parameters of this class. Since we
minimize the \emph{empirical} risk, it may happen that learned models
overfit the data (\ie they memorize training data, but have poor
performance on unseen data).
%
% Depending on the complexity of the hypothesis class, the resulting
% model may or may not be good on new, unseen data. On the one hand, if
% this class is too simple, the trained model may not minimize the
% empirical risk very well, and thus, not learn much from the data. On
% the other hand, if it is too complex, it may memorize the training
% data, perfectly minimizing the empirical risk, but failing to
% recognize a pattern that can predict the right label on unseen
% data. Yet, choosing the hypothesis class appropriately can be
% quite difficult.
A common practice to avoid overfitting is to add a
\emph{regularization term} to the empirical risk and minimize this
regularized version.
% Note that the empirical risk only captures the
% behaviour of the model on the training data. Therefore, a model that
% predicts the correct label on training records, and an arbitrary value
% otherwise, would minimize the empirical risk. Due to this phenomenon,
% we need to impose additional structural constraints on the
% model. These constraints are of two types. First, we restrict the set
% of possible models to a predefined set, that we call the
% \emph{hypothesis class}. Generally, we decide the structure of the
% model (\eg linear models or deep neural networks), and parameterize it
% with a limited number of values that govern its behaviour. Second, we
% alter the empirical risk with a regularization term.
This also allows for enforcing desirable structural properties (\eg
sparsity) on the learned model.
%Using a complex hypothesis class biases the training process
%towards learning simpler and more structured models.
% \begin{quoting}
%   Let $p > 0$, $\cW \in \RR^p$, and
%   $\cH = \{ h_w: \cX \rightarrow \cY \mid w \in \cW \}$ be an
%   hypothesis class parameterized by $\cW$. The empirical risk
%   minimization problem aims at finding an element of the set
%   \begin{align*}
%     \argmin_{w \in \cW} \bigg\{ F(w) := f( h_w ) + \psi(w) \bigg\}
%     \enspace,
%   \end{align*}
%   where $f(h_w)$ is the empirical risk of the model $h_w$
%   parameterized by $w \in \cW$, and $\psi$ is a regularization term
%   that controls the complexity of the model.
% \end{quoting}

The regularized empirical risk minimization formulation encompasses
many different problems. It can notably be instantiated to Ridge
regression, LASSO, (dual) SVM, or deep neural networks.

% We note that it also comprises more
% complex problems like training deep neural networks, but the methods
% we develop in this thesis do not apply to such problems.

\paragraph{Minimizing the (Regularized) Empirical Risk.}

The regularized empirical risk minimization is a composite
optimization problem with two terms: the empirical risk, and the
regularizer. These two terms have different regularity properties. The
former is differentiable, and the latter is simple enough so that it
can be dealt with proximal (projection-like) tools.  The most widely
used algorithm for solving this problem is surely (proximal)
\emph{gradient descent}, and its stochastic variant. This algorithm
starts with a random model from the hypothesis class and iteratively
refines it by updating its parameters. At each iteration, it computes
the gradient of the loss (or a stochastic estimate) at the current
iterate and uses it to improve the model.

Some problem have particular structure, that can be leveraged.  Parts
of the models may be more important than others, or have a different
scale.%\TODO{imbalanced here is not what I am used to, say in classification}
Unfortunately, gradient descent is indifferent to these structural
properties. As such, updating the model part by part may help to grasp
its structural properties more finely. This has sparked interest in
\emph{coordinate descent} methods, that are capable of exploiting this
structure. These methods are indeed extremely efficient on problems
where coordinates have different scales, where gradient descent often
struggles to make any progress.

These coordinate descent methods are the center of this thesis. In
particular, we will show that their aforementioned properties can help
to find better solutions when the privacy of data matters.



\section{The Challenge of Privacy-Preserving Machine Learning}
\label{sec:problem-with-privacy}

Privacy issues did not start with machine learning. They inevitably
arise when collecting and processing personal data. Quantifying the
information leakage incurred by releasing the result of a computation
on a database has thus been at the core of a multitude of works. One
of these proposed \emph{differential privacy}, which is now
well-adopted, and generally recognized as a very robust measure of
privacy.

\paragraph{Differential Privacy.}
Differential privacy emerged from the idea that releasing the result
of a computation on a database should not reveal too precisely whether
a specific individual was part of the database or not. The privacy
leakage is measured by looking at how the probability of observing a
given output is impacted when a record of the database is replaced by
another. Differential privacy requires such replacement not to affect
this probability by more than a constant multiplicative factor, that
is parameterized by a value called the \emph{privacy budget}.
% \begin{quoting}
%   Let $\cA: \cD \rightarrow \cE$ be a randomized algorithm, taking a
%   dataset from a space of datasets $\cD$ as input, and releasing a
%   value from an arbitrary set $\cE$. The algorithm $\cA$ is
%   $(\epsilon, \delta)$-differentially private if for all
%   $S \subseteq \cE$, and any pair of datasets $D, D' \in \cD$
%   differing on one element,
%   \begin{align*}
%     \prob( \cA(D) \in S ) \le \exp(\epsilon) \prob( \cA(D') \in S ) + \delta
%     \enspace,
%   \end{align*}
%   where $\epsilon \ge 0$ and $\delta \in [0, 1]$ quantify the strength
%   of the privacy guarantee.
% \end{quoting}

Given the above intuition, we see that any (non-trivial)
data-dependent deterministic algorithm cannot achieve any differential
privacy guarantee. Therefore, to satisfy differential privacy,
randomness must be incorporated into the algorithm. This ensures that
an external observer cannot know too confidently if what they observe
is due to this randomness, or to the content of the database. Of
course, this process reduces the quality of the answer. While an
observer will indeed not be able to reconstruct the sensitive
information, the result will be imprecise. This highlights the
fundamental tension between privacy and utility\footnote{There, the
  term ``utility'' is a generic measure of the precision of the
  result. Depending on the applications, there exist different ways of
  measuring it. }: this is generally referred to as the
\emph{privacy-utility} trade-off. This trade-off can be seen as the
answer to the following question: \emph{under a fixed privacy budget,
  what is the best utility that can be achieved?}

\paragraph{Differentially Private Empirical Risk Minimization.}

Training a machine learning model on a dataset is typically done using
optimization algorithms that iteratively query a database, and use the
result of these queries to find a good model. As such, machine
learning suffers from the curse described above: to release a useful
model, sensitive information \emph{must} be leaked.

In our supervised learning setting, differentially private
optimization algorithms have been proposed for solving the empirical
risk minimization problem. Notably, the differentially private variant
of (stochastic) gradient descent is widely used in practice. This
algorithm works similarly to the (stochastic) gradient descent
algorithm, except that, at each iteration, it adds noise to the
gradient before performing the gradient step. This guarantees that the
algorithm is differentially private. Of course, this has an impact on
its utility, and, as for any data-dependent computation, finding the
exact result with a differentially private algorithm is not possible.

% \footnote{Note that we consider the
%   \emph{empirical risk} here. If sampling from the joint distribution
%   was possible, only information about the underlying distribution
%   would be learned, and no confidential information would be
%   leaked. Again, sampling from this distribution is unfortunately not
%   possible.}
% \begin{quoting}
%   Let $\epsilon \ge 0$, $\delta \in [0, 1]$, and
%   $\cA : \cD \rightarrow \cH$ be a $(\epsilon,\delta)$-differentially
%   private algorithm. We measure the utility of $\cA$ as a solution to
%   the empirical risk minimization problem on a dataset $D \in \cD$ as
%   \begin{align*}
%     \Utility(\cA)(D) = \expect( F( \cA(D) ) ) - \min_{w \in \cW} F(h_w)
%     \enspace,
%   \end{align*}
%   where the expectation is over the randomness of $\cA$.
% \end{quoting}

\paragraph{Privacy-Utility Trade-Off in Machine Learning.}

The privacy-utility trade-off of differentially private empirical risk
minimization has been extensively studied. Tight lower bounds have
been derived for the best possible utility (measured as the excess
empirical risk) under a given privacy budget. Therefore, for
\emph{any} differentially private algorithm solving the empirical risk
minimization problem, there exists a problem for which the utility of
the algorithm necessarily decreases \emph{polynomially} in the number
of parameters of the model, but improves as the number of training
records increases. Notably, the utility achieved by differentially
private (stochastic) gradient descent matches these lower bounds.

However, these lower bounds are \emph{worst-case} bounds and hold
under very general assumptions. It may therefore be possible to
achieve better utility on \emph{some} problems that satisfy additional
assumptions. In a sense, and similarly to how choosing the right
method for the right problem is important for computational
efficiency, \emph{choosing the right method for the right problem is
  also crucial for using the privacy budget efficiently}. This
observation is at the core of the methods we explore in this thesis.

% \TODO{ The transition is still quite artificial there. I did not find
%   how to smooth it yet. }

% \paragraph{Differential Privacy and Fairness.}

% In some critical applications, privacy may not be the only concern. In
% particular, some parts of the population may be less accurately
% represented in the data, or be the subject discrimination in
% society. These issues tend to influence the behaviour of the model,
% which may end up reproducing discrimination. Even worse, the
% conception of the model itself may lead it to amplify or create more
% discrimination.

% This second type of ethical concerns have started to draw interest
% less than a decade ago, under the name of \emph{algorithmic
%   fairness}. Many ways of measuring fairness mathematically have
% arisen, and different measures of fairness cover different types of
% discrimination.

% In general, privacy and fairness issues are not independent problems,
% and sensitive datasets tend to be sensitive to both of these
% issues. This has led the scientific community to study how the fact of
% training models in a differentially private way may influence
% fairness.

\paragraph{Interplay between Privacy and Fairness.}

In additional to privacy issues, concerns about fairness of machine
learning have risen in the past decade. Fairness of a model's
prediction can be measured in different ways, depending on the nature
of the task. One category of fairness notions is \emph{group
  fairness}, that measure discrepancies in a model's performance (for
some metric) on different groups of the population.

Multiple factors can cause unfair predictions, from discrimination in
the data collection process, to inappropriate algorithm
design. Training models under differential privacy affects the
predictions of the model, and may thus be one of these factors. This
is often called the \emph{disparate impact of differential
  privacy}. To this day, it is still unclear whether this disparate
impact is fundamental in differentially private machine learning, or
if it is due to the design of current differentially private training
methods.



\section{Contributions}
\label{sec:contributions}

This thesis explores new optimization algorithms, that can be used for
training machine learning problems in a differentially private
way. While existing algorithms can, in theory, learn differentially
private models optimally (in terms of the privacy-utility trade-off),
there are still many problems where learning a non-trivial model
privately is difficult. We argue that some problems have a particular
structure, that can be exploited to obtain better private models under
the same privacy budget.

As concerns about the disparate impact of differential privacy are
growing, we also investigate the impact of differential privacy on
fairness. In particular, we derive an upper bound on this impact, and
show that, depending on the problem structure, this upper bound can
give meaningful guarantees.


% In this thesis, we study two major ethical questions that arise in
% machine learning applications: privacy and fairness. Our contributions
% stem from the observation that, from a computational perspective,
% structural properties of problems have a decisive influence on (i)~how
% easy it is to train a model on a dataset, and (ii)~what methods should
% be used to train a model. These computational questions have been
% widely studied, resulting in a the development of a myriad of methods
% for training machine learning models, each tailored to a specific type
% of problems. However, only a few differentially private methods have
% been studied.

% Designing new methods for differentially private training of machine
% learning model is fundamental for the development of more ethical
% machine learning. Indeed, in this context, the right choice of the
% training method not only affects the efficiency of the training in
% terms of computational cost, but also \emph{in terms of precision of
%   the result}. Nonetheless, this quest for the best privacy-utility
% trade-off should not come at a higher cost for some part of the
% population. In particular, it is a high priority to ensure that
% preserving the privacy of some does not imply the aggravation of
% already strong discrimination.
This thesis is thus dedicated to the study of differentially private
machine learning, where we aim at exploring what can be done when more
is known about the problem than the usual, very general,
assumptions. In short, we aim at answering the following question:
\begin{quoting}
  How can structural properties of machine learning problems can be
  exploited to improve the privacy-utility trade-off, and how do they
  impact the fairness of the resulting model?
\end{quoting}

To answer this question, we start by designing and analyzing two new
differentially private algorithms for solving the empirical risk
minimization problem. These two algorithms are variants of the
\emph{coordinate descent} algorithm, with two different rules for
selecting the coordinate to update: \emph{random selection}, and
\emph{greedy selection}. We show that these algorithms can improve
utility by exploiting structural properties of the problem like
imbalancedness of the gradient coordinates or sparsity of the
solution. We then turn to study the fairness of privately learned
models. To this end, we show that \emph{many group fairness notions
  are pointwise Lipschitz}, and use this property to derive guarantees
on the difference between fairness between private models and their
non-private counterparts.

\section{Outline of the Thesis}
\label{sec:outline-thesis}

The first two chapters introduce the mathematical background that we
use throughout the thesis.
\begin{itemize}
\item In \Cref{cha:convex-optim} we introduce the empirical risk
  minimization problem. We describe the convexity and
  (coordinate-wise) smoothness assumptions, and describe proximal
  (stochastic) gradient descent and proximal coordinate descent. We
  discuss the convergence properties of these algorithms for composite
  problems, and explain how coordinate descent is able to exploit
  coordinate-wise smoothness to converge faster than gradient descent.

\item Then, \Cref{cha:diff-priv-mach} turns to the differentially
  private variant of empirical risk minimization. We present
  differential privacy, and show how a differentially private variant
  of stochastic gradient descent can be used to solve empirical risk
  minimization privately. We describe existing lower bounds on the
  privacy-utility trade-off of differentially private empirical risk
  minimization, and show that, under the usual assumptions, it is
  notably matched by the differentially private stochastic gradient
  descent algorithm.
\end{itemize}


The next three chapters of this thesis describe our three
contributions. \Cref{chap:dp-cd,chap:greedy-cd} are dedicated to
differentially private coordinate descent methods, and
\Cref{chap:fair-privacy} studies the interplay of differential privacy
with fairness.
\begin{itemize}
\item \Cref{chap:dp-cd} introduces differentially private proximal
  coordinate descent. At each iteration, one coordinate is randomly
  selected and updated with a noisy proximal gradient step. These
  noisy updates allow the algorithm to satisfy differential
  privacy. The utility of this algorithm is analyzed, and we show that
  it can adapt to the coordinate-wise smoothness of the objective
  function to outperform differentially private stochastic gradient
  descent.

\item \Cref{chap:greedy-cd} studies differentially privacy
  \emph{greedy} coordinate descent. At each iteration, one coordinate
  is selected greedily as the (noisy) largest entry of the
  gradient. We analyze its utility on smooth objective, and show that
  this selection rule allows to reduce the dependence of utility on
  the dimension from polynomial to logarithmic \emph{in unconstrained
    problems}. This notably happens when the structure of the problem
  is favorable (\eg for problems with sparse solutions, or imbalanced
  coordinates): the algorithm can exploit this structure to beat the
  (general) lower bounds. Importantly, this phenomenon arises without
  constraints on the problem, which shows that our algorithm
  automatically adapts to the underlying structure of the problem.

\item \Cref{chap:fair-privacy} is devoted to the interplay between
  differential privacy and fairness. We show that the accuracy (on a
  part of the population) of a model is ``pointwise'' Lipschitz, and
  that this property is inherited by multiple group fairness
  notions. This allows to derive an upper bound on the difference of
  fairness between \emph{any} pair of models. We then use this
  regularity property to show that the fairness of private models
  necessarily stays in a bounded region around the one of their
  non-private counterparts.


\end{itemize}
Finally, \Cref{chap:conclusion} concludes this work, summarizing our
contributions as an answer to the question stated in the previous
section. We also describe some perspectives that we find promising for
future research.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
