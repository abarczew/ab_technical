\chapter{Introduction}
In recent years, the intersection of machine learning and privacy has 
received significant attention, driven by the increasing deployment 
of machine learning algorithms and models across various domains. 
These algorithms and models 
have become the backbone 
of many industrial products and scientific discoveries, ranging from 
social network recommendations and fraud detection to self-driving 
cars and medical research. However, the success of these models 
relies heavily on vast amounts of data, often containing sensitive 
information, raising serious concerns about data privacy.

The growing awareness of these risks has led to the emergence of 
privacy-preserving machine learning, where the goal is to develop 
models that protect the confidentiality of the underlying training 
data. Differential privacy (DP), introduced by Dwork et al., has 
become canonical for quantifying and ensuring privacy in 
algorithms. Differential privacy ensures that the output of a 
computation does not significantly differ when any single data 
point in the dataset is changed, thereby protecting individual privacy.

% [add intro on sgd?]
% Stochastic Gradient Descent (SGD) has been a pivotal algorithm in 
% training machine learning models, especially in the context of deep 
% learning. The advent of differentially private versions of SGD (DP-SGD) 
% has enabled the training of models that uphold strong privacy 
% guarantees by introducing carefully calibrated noise to the gradients 
% during the training process. While this approach successfully ensures 
% privacy, it often leads to a trade-off with model accuracy, particularly 
% in high-dimensional settings. This trade-off poses a significant challenge, 
% as existing algorithms sometimes struggle to balance the utility of the 
% model with the robustness of privacy.

Differential privacy has emerged as the gold standard for statistical 
privacy, providing a formal framework to quantify and mitigate the 
risk of data leakage. Yet, training differentially private models 
introduce a trade-off: the more privacy is enforced, the less 
accurate models become. This trade-off is particularly 
challenging in high-dimensional settings, where existing algorithms 
struggle to produce models that are both useful and privacy-preserving.

In this thesis, we explore the challenges and potential methods 
for preserving data privacy in machine learning. 
We examine the implications of these methods on model utility and 
discuss how to balance these competing objectives in 
practice. Additionally \footnote{dp-ecdf isn't a method for ml}, we delve into the role of empirical cumulative 
distribution functions (ECDF) in 
privacy-preserving machine learning, highlighting their importance 
and the challenges associated with computing them in a 
privacy-preserving manner.

\section{Supervised Learning}\footnote{change the title to be less like a background}
\label{sec:int.sup-learn}
Supervised learning is a fundamental concept in the field of machine 
learning, where the goal is to learn a function that maps inputs to 
outputs based on a set of labeled examples. In this framework, a machine 
learning model is trained to make predictions or decisions by learning 
from a dataset that contains both the input data and the corresponding correct 
output, or label. This learning process is termed "supervised" because the 
model is guided by the provided labels.

Supervised learning can be broadly divided into two main tasks: 
classification and regression. In a classification task, the goal is to 
predict a discrete label or category for a given input \eg predicting if 
an email is "spam" or "not spam". On the other hand, regression 
involves predicting a continuous value \eg predicting the price of a 
house based on its features, such as size and location.

A critical aspect of supervised learning is the separation of the available 
data into training and validation/test sets. The training set is used to train the 
model \ie this is where the model learns the relationship between the input data 
and the corresponding labels. The validation and test sets, on the other hand, are used to 
evaluate the model's performance on unseen data.
The validation set is used several times during training to estimate the model 
performance typically for hyperparameter tuning, whereas 
the test set is intended to be used only once to provide a final assessment 
of the model's performance. The separation between training and validation/test sets ensures that 
the model’s performance is not overly optimistic and that it generalizes 
well to new, unseen examples.

\paragraph{Loss Function.} At the heart of supervised learning is the concept
of a loss function. The loss function quantifies how well the model's 
predictions match the actual labels. 
In other words, it measures the "error" of the model. 
The choice of loss function depends on the type of problem being 
solved. For instance, in a classification problem, the cross-entropy loss 
is commonly used, while in regression tasks, the mean squared error is a 
popular choice. The goal of training is to minimize this loss function, 
thereby improving the model’s performance.

\paragraph{Empirical Risk Minimization (ERM).} The process of minimizing 
the loss function over the training data is 
known as empirical risk minimization (ERM). In ERM, the model is adjusted 
throughout the training to reduce the average loss over the training set, which is 
referred to as the empirical risk. The underlying assumption is that by 
minimizing the empirical risk, 
the model will perform well on unseen data, 
thereby minimizing the true risk or the expected loss on the overall data 
distribution. ERM, along with careful measures to ensure that the model 
generalizes well and avoids what is known as overfitting, serves as a 
fundamental principle in many supervised learning algorithms.

\paragraph{Stochastic Gradient Descent (SGD).} To effectively minimize 
the loss function during training, and minimize the empirical risk, 
one of the most widely used optimization 
techniques is Stochastic Gradient Descent (SGD). Unlike traditional gradient 
descent \cite{rumelhart1986learning}, which computes the gradient of the loss function with respect to 
the entire training dataset, SGD approximates this by computing the 
gradient \cite{lagrange1788mecanique} for only a small, randomly selected subset of the data, known as 
a mini-batch. This approach significantly reduces the computational cost 
and allows the model to update more frequently, leading to faster convergence. 
While SGD introduces some noise into the optimization process due to the 
random sampling of mini-batches, this noise can help the model escape local 
minima and potentially find better solutions in the parameter space. Over 
time, as the model iteratively updates its parameters to minimize the loss 
function, it improves its ability to generalize from the training data to 
new, unseen examples. Variants of SGD, such as Momentum \cite{sgdm1964}, 
RMSprop \cite{RMSProp}, and Adam \cite{adam}, further enhance its performance by adapting the learning rate or 
incorporating momentum to accelerate convergence and stabilize the learning 
process.

Stochastic gradient descent plays a central role in this thesis. 
Specifically, we will demonstrate how it can be effectively integrated 
with differential privacy techniques to enable privacy-preserving machine 
learning.


\section{Privacy-preserving machine learning.}
\label{sec:int.priv-ml}

As machine learning models grow more sophisticated and are increasingly 
applied to sensitive domains such as healthcare, finance, and social networks, 
the risk of exposing private information has increased. 
Privacy-preserving machine learning seeks to address these concerns by developing 
techniques that allow for the extraction of useful insights from data 
without compromising individual privacy.

\paragraph{Differential Privacy.} One of the most influential concepts in this field is differential privacy. 
Introduced by Dwork et al., differential privacy provides a mathematically 
rigorous framework for quantifying and managing privacy risks. At its core, 
differential privacy ensures that the inclusion or exclusion of any single 
data point in a dataset has a minimal impact on the outcome of a computation, 
thereby protecting the privacy of individuals whose data is included. This 
property is particularly desirable when implementing machine learning models that 
operate on sensitive datasets.

To achieve differential privacy, several mechanisms have been devised. 
These mechanisms typically involve the introduction of controlled 
randomness into the learning process, making it difficult for adversaries 
to infer information about any individual data point. Commonly used 
mechanisms on a single value or vector include the Laplace mechanism, the Exponential mechanism, and 
the Gaussian mechanism, each tailored to different types of data and 
privacy requirements. These mechanisms are carefully designed to balance 
the trade-off between privacy and utility, ensuring that while privacy is 
preserved, the machine learning model remains useful.

\paragraph{Privacy budget accounting.}
Often denoted as \((\varepsilon, \delta)\), the privacy budget is a tuple of parameters that 
quantifies the level of privacy provided by a differentially private algorithm. 
A smaller \(\varepsilon\), for instance, implies stronger privacy guarantees but typically at 
the cost of reduced model accuracy.
Managing the privacy budget effectively is essential, especially when 
multiple queries or computations are performed on the same dataset. 
As every query on the dataset reveals additional information, 
it increases the overall privacy budget.

Techniques such as composition theorems (\cref{th:prel.account_rdp}) and privacy 
amplification (\cref{th:amplification}) are used to track and optimize the use of the privacy budget 
over the course of a machine learning task.

\paragraph{Differentially Private Stochastic Gradient Descent (DP-SGD).} 
One of the most significant applications of differential privacy in machine 
learning is the development of Differentially Private Stochastic Gradient Descent (DP-SGD). 
DP-SGD extends SGD by incorporating differential privacy 
guarantees, ensuring that the gradients used to update the model do not 
reveal sensitive information about individual data points. 
The most popular variant of DP-SGD uses gradient clipping and adding noise before updating the model, 
thus preserving privacy while still allowing the model to learn from the data. 
Finally, privacy accounting techniques help manage the cumulative privacy 
loss over multiple iterations. By carefully managing the privacy budget, 
DP-SGD ensures that the model remains both effective and compliant with 
the desired privacy constraints, striking a balance between model utility 
and privacy.
\footnote{no basics for ecdf}
\footnote{no problem or motivations yet: this is done in the following section}

\section{Contributions}
\label{sec:contributions}

This thesis focuses on the optimization and analysis of machine learning 
models, addressing the challenge of minimizing the privacy budget in settings 
that involve repeated querying. In optimization tasks, repeated querying is 
inherent, as gradients are computed iteratively on batches of data. 
Similarly, the computation of empirical cumulative distribution 
functions (ECDFs) requires multiple evaluations to provide 
meaningful results. While the effects of repeated queries on privacy have 
been studied extensively, we argue that better solutions can be designed 
by tailoring approaches to specific applications.

In the case of optimization, this thesis demonstrates that leveraging 
gradient sensitivity properties can improve privacy-utility trade-offs 
when models are constrained to a subclass of Lipschitz-bounded functions. 
We prove that the bias introduced by choosing models from this subclass is 
less detrimental than the bias induced by gradient modifications in popular 
optimization methods. For ECDFs, we show that exploiting their structural 
properties allows for stronger privacy guarantees, even in more complex 
scenarios such as multi-party computation.

In summary, this thesis highlights how analyzing the structural properties 
of specific applications can uncover opportunities to achieve stronger 
privacy guarantees within more practical setups. The overarching question 
driving this work is as follows:

\begin{quoting}
How can the structural properties of specific applications—here, 
stochastic gradient descent and ECDFs—be leveraged to improve the 
privacy-utility trade-off and enable broader setups, such as multi-party 
computation?
\end{quoting}


\newcommand{\weightDpSgd}{\textsc{Lip-DP-SGD}}
This thesis answers this question positively by introducing two novel 
algorithms. First, we present \weightDpSgd{}, an optimization algorithm 
that leverages the constrained Lipschitzness of the gradient. Experimental 
evaluations across a diverse range of datasets demonstrate that this 
constraint results in less accuracy degradation and improved runtime 
performance compared to current state-of-the-art methods. 

Second, we propose a new algorithm for privacy-preserving ECDF computation, 
which ensures privacy guarantees while maintaining the utility of the 
output. By leveraging ECDF-specific properties, this algorithm is 
compatible with a variety of security protocols, including function secret 
sharing, offering enhanced privacy and accuracy. 

Through these contributions, the thesis demonstrates that application-specific 
structural insights can effectively improve the privacy-utility trade-off and 
enable the deployment of privacy-preserving techniques in a broader range of 
practical and collaborative scenarios.

\section{Outline of the Thesis}
\label{sec:outline-thesis}

The first three chapters introduce the theoretical background 
that is used in the thesis.
\begin{itemize}
\item In \Cref{cha:prel.optim}, we present the empirical risk minimization 
problem and provide a formal definition of differentiability, followed by 
an introduction to stochastic gradient descent. We then discuss neural 
networks and the optimization challenges they pose. Finally, we define 
Lipschitzness, a key property that plays a central role in the development 
of our proposed optimization approach.

\item Then, \Cref{cha:prel.dp} explores the differentially private 
  formulation of the empirical risk minimization problem. We introduce the 
  concept of differential privacy and the foundational components that 
  enable the design of differentially private algorithms. Finally, we 
  demonstrate how a differentially private version of stochastic 
  gradient descent can be employed to solve the empirical risk 
  minimization problem while preserving privacy.

\item In \Cref{cha:prel.mpc}, we define the fundamental security 
  primitives necessary to understand the challenges of multi-party 
  computation. We introduce key security concepts, such as computational 
  indistinguishability, as well as foundational notions in multi-party 
  computation, including common methods for sharing secrets among stakeholders.
\end{itemize}

The next two chapters of this thesis describe our two
contributions. \Cref{chap:emp-opt} is dedicated to
a novel approach for differentially private stochastic gradient descent, 
and \Cref{chap:priv-ecdf} studies how a complete ECDF can be displayed
collaboratively with strong privacy guarantees.
\begin{itemize}
\item \Cref{chap:emp-opt}  presents a novel approach that mitigates the bias arising from traditional gradient
clipping. By leveraging a public upper bound of the Lipschitz value of the current model,
we can achieve reﬁned noise level adjustments. We present a new algorithm with improved
diﬀerential privacy guarantees and a systematic empirical evaluation, showing that our new
approach outperforms existing approaches also in practice.

\item \Cref{chap:priv-ecdf} proposes strategies to compute
differentially private empirical distribution functions. While revealing complete functions is more
expensive from the point of view of privacy budget, it may also provide richer and more valuable
information to the learner. We prove privacy guarantees and discuss the computational cost, both
for a generic strategy fitting any security model
and a special-purpose strategy based on secret
sharing. We survey a number of applications and
present experiments.


\end{itemize}
Finally, in \Cref{chap:conclusion}, we conclude this work by summarizing 
our contributions as a response to the central question stated in the previous section. 
Additionally, we outline several promising directions for future research.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
