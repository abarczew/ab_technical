\chapter{Appendix of Chapter~\ref{chap:priv-ecdf}}
\label{chap:app.priv-ecdf}

\section{Proofs}

\subsection{Proof of Theorem \ref{thm:ecdf.dp}}
\label{sec:proof.thm.ecdf.dp}

Before proving Theorem \ref{thm:ecdf.dp}, we first introduce some additional definitions and lemmas.
For $L\in\mathbb{N}\setminus\{0\}$, let
\[Z_{L,d} = \left(\indicSet{[d+(j-1)2^l+1,d+j2^l]}\right)_{(j,l)\in \rvidxL{L}}\]
be a vector of functions indexed by $\rvidxL{L}$ where $\indicSet{X}:\mathbb{N}\to\{0,1\}$ is a function with $\forall x\in X: \indicSet{X}(x)=1$ and $\forall x\in\mathbb{N}\setminus X : \indicSet{X}(x)=0$.  Note that $[d,d-1]=\emptyset$ and hence $\indicSet{[d,d-1]}=0$.

\begin{lemma}
  \label{lm:need.halfL.noiseterms}
  Let $L\in \mathbb{N}\setminus\{0\}$, $d\in\mathbb{N}$ and $b-d\in\left[2^L\right]$, then there exist vectors $\chi^s\in\{-1,0,1\}^{\rvidxL{L}}$, $s\in\{+,-\}$, with the number of non-zero elements $\|\chi^s\|_0$ at most $\lceil (L+1)/2\rceil$ such that $\indicSet{[d+1,b]} = \chi^-.Z_{L,d}$ and  $\indicSet{[b,d+2^L]} = \chi^+.Z_{L,d}$.
\end{lemma}
\begin{proof}
  We need to prove that we can write the functions $\indicSet{[d+1,b]}$ and $\indicSet{[b,d+2^L]}$ as weighted sums (with coefficients $-1$ or $+1$) of elements of $Z_{L,d}$.  We proceed by induction.

  \textit{Base cases.} Consider first $L=0$ and $L=1$.
In both cases it is easy to verify that $\indicSet{[d+1,b]}$ and $\indicSet{[b,d+2^L]}$ are both elements of $Z_{2,d}$ and hence $\chi^+$ and $\chi^-$ can be set to suitable base vectors, i.e., $\|\chi^+\|_0=\|\chi^-\|_0=1$.

  \textit{Induction step.}
  Suppose that $L\ge 2$ and assume that the lemma has been proven for $L'\le L-2$.  There are four cases, depending on $b'=\lceil (b-d)/2^{L-2}\rceil\in[4]$.
  \vspace{-1mm}
  \begin{itemize}
    \item
      Consider first $b'=0$, i.e., $b-d \le 2^{L-2}$:
      \begin{itemize}
        \item[($\chi^-$)]
          Applying the lemma for $L'=L-2$ we can write $\indicSet{[d+1,b]}$ as a weighted sum of $\lceil (L'+1)/2\rceil < \lceil (L+1)/2\rceil$ elements of $Z_{L',d}\subseteq Z_{L,d}$.
        \item[($\chi^+$)] As $\indicSet{[b,d+2^L]}=\indicSet{[d+1,d+2^L]}-\indicSet{[d+1,b-1]}$, we can write $\indicSet{[b,d+2^L]}$ as a weighted sum of at most $\lceil (L+1)/2\rceil$ elements of $Z_{L,d}$: $\indicSet{[d+1,d+2^L]} \in Z_{L,d}$ and either $\indicSet{[d+1,b-1]} = 0$ or $b-1\in[2^{L-2}]$ implying that $\indicSet{[d+1,b-1]}$ as in case ($\chi^-$) above.
          \end{itemize}
        \item Consider next $b'=1$, i.e., $2^{L-2}+1\le b-d\le 2^{L-1}$.
          \begin{itemize}
          \item[$\chi^-$] As  $\indicSet{[d+1,b]}=\indicSet{[d+1,d+2^{L-2}]}+\indicSet{[(d+2^{L-2})+1,b]}$,  $\indicSet{[b,d+2^L]}\in Z_{L,d}$ it suffices to apply the induction hypothesis and note that we can write $\indicSet{[(d+2^{L-2})+1,b]}$ as a sum of $\lceil (L+1)/2\rceil-1$ elements of $Z_{L-2,d+2^{L-2}}\subseteq Z_{L,d}$.
            \item[$\chi^+$] $\indicSet{[b,d+2^L]} = \indicSet{[b,d+2^{L-1}]} + \indicSet{[d+2^{L-1}+1,d+2^L]}$ and $\indicSet{[b,d+2^{L-1}]}\in Z_{L,d}$ hence it suffices to apply the induction hypothesis to $\indicSet{[d+2^{L-1}+1,d+2^L]}$.
          \end{itemize}
\item
  Cases $b'=2$ and $b'=3$ are analoguous to cases $b'=1$ and
   $b'=0$ respectively.
\end{itemize}
This completes the proof.
\end{proof}



\textbf{Theorem \ref{thm:ecdf.dp}} \thmEcdfDp

\begin{proof}
  Consider two adjacent datasets $X^{(1)}$ and $X^{(2)}$.
  As the datasets are adjacent, they differ in only one instance,
  i.e., there exists a dataset $X'$ and instances $x_\Delta^{(1)}$ and $x_\Delta^{(2)}$ such that $X^{(s)}=X'\cup\{x_\Delta^{(s)}\}$ for $s\in \{1,2\}$.

  The inner product of $\rvidxL{L}$-indexed vectors $\eta Z_{L,0}$ is a function mapping any $i\in[2^L]$ to
\[ \sum_{(j,l)} \{\eta_{j,l}\mid (j-1)2^l+1 \le i \le j2^l\}
  = \sum_{l=0}^L \eta_{\lceil i/2^l\rceil,l}.
\]
  Then, defining $\ecdf{\ffeat}(X,\tau) = \left(\ecdf{\ffeat}(X,\tau_i)\right)_{i\in[2^L]}$ and  $\ecdfdp{\ffeat}(X,\tau) = \left(\ecdfdp{\ffeat}(X,\tau_i)\right)_{i\in[2^L]}$, where we set $\forall i\in [N+1, 2^L]:\tau_i=\phimax$, we can rewrite Eq \eqref{eq:def.ecdfdp.pt} as
\begin{equation}
  \label{eq:def.ecdfdp.func}
  \ecdfdp{\ffeat}(X,\tau) = \ecdf{\ffeat}(X,\tau) + \frac{\eta Z_{L,0}}{n}
\end{equation}
For $s\in{1,2}$, there holds
\begin{equation}
  \label{eq:diff.ecdf.XpXs}
n\ecdf{\ffeat}(X^{(s)},\tau) - (n-1)\ecdf{\ffeat}(X^{(s)},\tau) = \indicSet{[t_s+1,2^L]}
\end{equation}
where $t_s=\max\left\{i \in [N]\mid x_\Delta^{(s)} > \tau_i\right\}$.
Without loss of generality we assume that $X_\Delta^{(2)} < x_\Delta^{(1)}$.
Combining Eq \eqref{eq:def.ecdfdp.func} and twice Eq \eqref{eq:diff.ecdf.XpXs} we get
\begin{equation}
  \label{eq:ecdfdp.diffX12}
  n\ecdfdp{\ffeat}(X^{(2)},\tau)
  = n\ecdf{\ffeat}(X^{(1)},\tau)
   +\indicSet{[t_1+1,t_2]}+\eta Z_{L,0} %\label{eq:ecdfdp.diffX12}
\end{equation}
\newcommand{\lmid}{{l_{\hbox{{\tiny{m}}}}}}
\newcommand{\jmid}{{j_{\hbox{{\tiny{m}}}}}}
Consider the largest $\lmid\in [0,L]$ for which there exists a $\jmid\in\mathbb{N}$ such that $t_1\le \jmid 2^{\lmid} < t_2$.  Notice that $\jmid$ is odd, as else we would have $(\jmid/2)2^{\lmid+1}=\jmid 2^{\lmid}$ and $\lmid$ would not be maximal.  Also, there holds $(\jmid-1)2^{\lmid} < t_1$ as else we would have $t_1 \le (\jmid-1)2^{\lmid}$ with $\jmid-1$ even and again $\lmid$ would not be maximal.  Similarly we can infer $t_2\le (\jmid+1)2^{\lmid}$. As $t_2\le 2^L$ there follows $\lmid\le L-1$.
Let $d_1=(\jmid-1)2^{\lmid}$ and $d_2 = \jmid 2^{\lmid}$.
Applying twice Lemma \ref{lm:need.halfL.noiseterms} we can conclude there exist vectors $\chi_1^+,\chi_2^- \in\{-1,0,+1\}^{\rvidxL{\lmid}}$ with $\|\chi_1^+\|_0\le \lceil L/2\rceil$ and $\|\chi_2^-\|_0\le \lceil L/2\rceil$ such that
$\chi_1^+.Z_{\lmid,d_1 }= \indicSet{[t_1+1,d_2]}$ and $\chi_2^-.Z_{\lmid,d_2 } = \indicSet{[d_2 +1,t_2]}$.  As the elements of the vectors $Z_{\lmid,d_1 }$ and $Z_{\lmid,d_2}$ also occur in $Z_{L,0}$, we can conclude that there exists a vector $\chi\in\{-1,0,+1\}^{\rvidxL{L}}$ with $\|\chi\|_0 \le L+1$ such that
\begin{equation}
  \label{eq:chi.t1t2}
  \chi.Z_{L,0} = \indicSet{\left[t_1+1,t_2\right]}.
  \end{equation}

We now express the probability of observing $\ecdfdp{\ffeat}(X^{(2)})$ given $X^{(2)}$ and compare it the probability of making the same observation given $X^{(1)}$.  Using Eqs \eqref{eq:ecdfdp.diffX12} and \eqref{eq:chi.t1t2} and setting $\Gamma(y,X^{(1)}) = y-n.\ecdfdp{\ffeat}(X^{(1)},\tau)$,
\begin{eqnarray*}
  \lefteqn{P\left(n.\ecdfdp{\ffeat}(X^{(2)},\tau)=y\right)}
  &&\\
  &=& P\left(n\ecdf{\ffeat}(X^{(1)},\tau)+\indicSet{[t_1+1,t_2]}+\eta Z_{L,0} = y\right) \\
&=& P\left(\chi Z_{L,0}+\eta Z_{L,0}=\Gamma(y,X')\right)\\
  &=& \int_u P(\eta = u) \, \indicVal{\chi Z_{L,0}+\eta Z_{L,0}=\Gamma(y,X')}\\
  &=& \int_u P(\eta = u+\chi) \, \indicVal{\eta Z_{L,0}=\Gamma(y,X')}
\end{eqnarray*}
Also,
\begin{eqnarray*}
  \lefteqn{P\left(n.\ecdfdp{\ffeat}(X^{(2)},\tau)=y\right)} &&\\
&=& \int_u P(\eta = u) \, \indicVal{\eta Z_{L,0}=\Gamma(y,X')}
\end{eqnarray*}
The probability ratio for a given $\eta$ is
  \begin{eqnarray*}
    \lefteqn{\left|\log\left(\frac{P(\eta = u+\chi)}{P(\eta = u)}\right)\right|}
    &&\\
    &=& \left|\sum_{(j,l)}  \log\left(\frac{P(\eta_{j,l} = u_{j,l}+\chi_{j,l})}{P(\eta_{j,l} = u_{j,l})}\right) \right|   \\
    &\le & \sum_{(j,l) : \chi_{j,l}\neq 0} \left|\log\left(\frac{P(\eta_{j,l} = u_{j,l}+\chi_{j,l})}{P(\eta_{j,l} = u_{j,l})}\right)\right|    \\
    &\le & (L+1) \frac{\epsilon}{L+1} \\
    &=& \epsilon
  \end{eqnarray*}
  We can conclude
  \[
\left|\log\left(\frac{P\left(n.\ecdfdp{\ffeat}(X^{(2)},\tau)=y\right)}{P\left(n.\ecdfdp{\ffeat}(X^{(2)},\tau)=y\right)}\right)\right| \le \epsilon
\]
as both probabilities are integrals over functions who only differ by a factor $e^\epsilon$.  This proves the privacy guarantee.

The expected squared error made by adding the noise is
\begin{eqnarray*}
  \lefteqn{\mathbb{E}\left[\left(\ecdfdp{\ffeat}(x)-\ecdf{\ffeat}(x)\right)^2\right]}
  && \\
  &=& \mathbb{E}\left[\left( \sum_{l=0}^L \eta_{\lceil i/2^l\rceil,l} \right)^2\right] \\
  &=& \sum_{l=0}^L \hbox{var}\left(\eta_{\lceil i/2^l\rceil,l}\right) \\
  &=& (L+1).2\left(\frac{L+1}{\epsilon}\right)^2 \\
  &=& 2(L+1)^3/\epsilon^2
  \end{eqnarray*}

\end{proof}

The above result has some implications for the more commonly studied problem of releasing statistics under continual observation \cite{DBLP:journals/corr/abs-2103-16787}.  In this problem, one considers data streams $(x_i)_{i=1}^N$ and wants to report at any time step $t$ the partial sum $s_t=\sum_{i=1}^t x_i$.  Two data streams are considered adjacent if they differ at most in one time step.  A change at a time step $t^*$ changes all partial sums between $t^*$ and $N$, i.e., the sums in a half-open interval.  In our setting, we also considered datasets adjacent if an instance changes its value, which means the partial sums change between its old value and its new value, i.e., the sums in a closed interval.   Applying our technique above to this problem, we get the following results:

\begin{theorem}
  \label{thm:improve.data.stream.dp}
  Let datasets $X^{(1)},X^{(2)}\in\mathcal{X}^N$ be adjacent if there exists at most one $i$ such that $X^{(1)}_i \neq X^{(2)}_i$.  Let $L=\lceil\log_2(N)\rceil$. Then, publishing ${\hat{s}}_t = \sum_{i=1}^t x_i + \sum_{l=0}^L \eta_{\lceil i/2^l\rceil,l}$ where $\eta_{j,l}\sim Lap(\lceil(L+1)/2\rceil/\epsilon$ is $\epsilon$-differentially private.    Similarly, with $\eta_{j,i}\sim \mathcal{N}(0,\lceil(L+1)/2\rceil z^2)$ the publishing is $1/2z^2$-zCDP (as defined in \cite{DBLP:journals/corr/abs-2103-16787}).
\end{theorem}

\begin{proof}
  The proof is a direct application of Lemma \ref{lm:need.halfL.noiseterms} using similar ideas as in the proof of Theorem \ref{thm:ecdf.dp}.
\end{proof}

The proof of Theorem 1 in \cite{DBLP:journals/corr/abs-2103-16787} concludes that at most $L$ terms in the partial sums they disclose will change between adjacent datasets, and hence need to compose $L$ differential private mechanisms.  Even though they focus on Renyi differential privacy rather than $\epsilon$-differential privacy, our idea allows in their setting too to reduce the number of changed terms with about a factor $2$ (when using base $2$) and hence to improve the privacy guarantee.

\subsection{Proof of Theorem \ref{thm:HL.DP}}
\label{sec:proof.HL.DP}

\textbf{Theorem \ref{thm:HL.DP}.} \thmHLDP
\begin{proof}
  The algorithm queries data at lines \ref{ln:HL.ecdf} and at lines  \ref{ln:HL.expobs1}--\ref{ln:HL.expobs2}.

  First, from Theorem \ref{thm:ecdf.dp} we know that by using $Lap(1/\epsilon')$ noise variables for evaluating $\ecdfdp{\HLModelFunc}^{-1}$, the resulting $\ecdfdp{\HLModelFunc}(\cdot)$ is $(L+1)\epsilon'$-DP independently of the number of needed evaluations of it during calls to Algorithm \ref{algo:invEcdf}.

  Next, for the evaluation of the statistics in lines \ref{ln:HL.expobs1}--\ref{ln:HL.expobs2} independent Laplacian random variables are used.  However, when we compare two adjacent datasets where only one instance differs, only $2$ of the $Q$ groups and the corresponding $8$ statistics are affected.  Hence, if all $4Q$ statistics in lines \ref{ln:HL.expobs1}--\ref{ln:HL.expobs2} are $\epsilon'$-DP, together they are $8\epsilon'$-DP.

  In summary, applying the classic composition rule for differential privacy we get that the algorithm is $(L+1)\epsilon' + 8\epsilon' = \epsilon$ -differentially private.
\end{proof}

\section{Additional experimental results}

Figures \ref{fig:ex.roc.bank.0.2} and \ref{fig:ex.roc.diabetic.0.05} show examples of ROC curves on the Bank and Diabetic datasets.

\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[width=\columnwidth]{priv-ecdf/roc_exp_results_bank-full_0.2.png}}
\caption{ROC curve for logistic regression on the Bank dataset, and $\epsilon$-DP curves with $\epsilon=0.2$.}
\label{fig:ex.roc.bank.0.2}
\end{center}
\vskip -0.2in
\end{figure}


\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[width=\columnwidth]{priv-ecdf/roc_exp_results_diabetic_data_0.05.png}}
\caption{ROC curve for logistic regression on the Diabetic dataset, and $\epsilon$-DP curves with $\epsilon=0.05$.}
\label{fig:ex.roc.diabetic.0.05}
\end{center}
\vskip -0.2in
\end{figure}

Figure \ref{fig:HL.dia} show the relative MSE of the Hosmer-Lemeshow statistic on the Diabetes dataset as a function of $\epsilon$.

\begin{figure}[ht]
\vskip 0.2in
\begin{center}
% \centerline{\includegraphics[width=\columnwidth]{priv-ecdf/hl_smooth_exp_dia.png}}
\caption{Hosmer-Lemeshow statistic MSE for a logistic regression model on the Diabetes dataset}
\label{fig:HL.dia}
\end{center}
\vskip -0.2in
\end{figure}