% This is samplepaper.tex, a sample chapter demonstrating the
% LLNCS macro package for Springer Computer Science proceedings;
% Version 2.20 of 2017/10/04
%
\documentclass[runningheads]{llncs}
%
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
% Used for displaying a sample figure. If possible, figure files should
% be included in EPS format.
%
% If you use the hyperref package, please uncomment the following line
% to display URLs in blue roman font according to Springer's eBook style:
% \renewcommand\UrlFont{\color{blue}\rmfamily}

\begin{document}
%
\title{First task: exercices on differential privacy}
\author{Antoine Barczewski}
\institute{Inria}
%
\maketitle

\section{Preliminaries}
\subsection{Definitions and Theorems \cite{dwork:2014}}

\begin{definition}[Differential Privacy]
  A randomized algorithm $\mathcal{M}$ with domain
  $\mathbb{N}^{\vert\mathcal{X}\vert}$ is $(\epsilon, \delta)$-differentially
  private if for all $\mathcal{S} \subseteq Range(\mathcal{M})$
  and for all $x,y \in \mathbb{N}^{|\mathcal{X}|}$ such that $||x - y||_{1}$:
  $$\mathrm{Pr}[\mathcal{M}(x) \in \mathcal{S}] \leq \exp(\epsilon)\mathrm{Pr}[\mathcal{M}(y) \in \mathcal{S}] + \delta $$
  where the probability space is over the coin flips of the mechanism $\mathcal{M}$.
  If $\delta = 0$, we say that $\mathcal{M}$ is $\epsilon$-differentially private.
\end{definition}

\begin{definition}[$l_{1}$-sensitivity]
  The $l_{1}$-sensitivity of a function
  $f$ : $\mathbb{N}^{|\mathcal{X}|} \to \mathbb{R}^{k}$ is:
  $$\Delta f = \max_{\substack{x,y \in \mathbb{N}^{|\mathcal{X}|} \\ \|x - y\|_{1}=1}} \|f(x) - f(y)\|_{1}$$
\end{definition}

\begin{definition}[The Laplace Distribution]
  The Laplace Distribution (centered at 0) with scale $b$ is the distribution
  with probability density function:
  $$\mathrm{Lap}(x|b) = \frac{1}{2b}\exp\Bigl(-\frac{|x|}{b}\Bigr).$$
\end{definition}

\begin{definition}[The Laplace Mechanism]
  Given any function $f$ :
  $\mathbb{N}^{|\mathcal{X}|} \to \mathbb{R}^{k}$, the Laplace mechanism is defined
  as:
  $$\mathcal{M}_{L}(x, f(\cdot), \epsilon) = f(x) + (Y_{1},\ldots,Y_{k})$$
  where $Y_{i}$ are i.i.d. random variables drawn from $\mathrm{Lap}(\Delta f/b)$.
\end{definition}

\begin{theorem}
  The Laplace mechanism preserves $(\delta, 0)$-differential
  privacy.
\end{theorem}

\begin{theorem}\label{th:group}
  Any $(\epsilon, 0)$-differentially private mechanism $\mathcal{M}$ is
  $(k\epsilon, 0)$-differentially private for groups of size $k$. That is, for
  $\|x - y\|_1 \leq k$ and all $\mathcal{S} \subseteq Range(\mathcal{M})$
  $$\mathrm{Pr}[\mathcal{M}(x) \in \mathcal{S}] \leq \exp(k\epsilon)\mathrm{Pr}[\mathcal{M}(y) \in \mathcal{S}]$$
  where the probability space is over the coin flips of the mechanism $\mathcal{M}$
\end{theorem}

\begin{definition}[The Gaussian mechanism]
  Let $f : \mathbb{N}^{|\mathcal{X}|} \to \mathbb{R}^{k}$ be an arbitrary
  $d$-dimensional function, and define its $l_2$ sensitivity to be \\
  $\Delta_2 f = max_{adjacent x,y} \|f(x) - f(y)\|_2$. The Gaussian
Mechanism with parameter $\sigma$ adds noise scaled to $\mathcal{N}(0, \sigma^2)$
to each of the $d$ components of the output.
\end{definition}

\begin{theorem}\label{th:gauss_mech}
  Let $\epsilon \in (0, 1)$ be arbitrary. For $c^{2} > 2 \ln(1.25 / \delta)$, the
  Gaussian Mechanism with parameter $\sigma \geq c\Delta_2 f / \epsilon$ is
  $(\epsilon, \delta)$-differentially private.
\end{theorem}

\section{First problem}
\subsection{Statement}
Suppose temperature measurments are done on patients by an entity who should be able to publish
these measures without compromise patients identity. Suppose an adversary knows
the set of patients, can take patients' temperatures but doesn't
know the measurment error.

How much noise should be added to these temperature measurments,
which are already noisy, so an acceptable level of privacy is met?

\subsection{Proposed answer}
We define two possible answers depending on whether the differentially private
mechanism is Laplace or Gaussian. In both cases we suppose the measurment error
$m_{\mathrm{error}}$ be Gaussian such that $m_{\mathrm{error}} \sim \mathcal{N}(0, \sigma^2)$. \\
Here we will detail only the case of the Gaussian noise, as we don't know yet how
to sum Gaussian and Laplace variables, which is required in the context of Laplace
mechanism.

\begin{proposition}[Gaussian mechanism]
  Let $\epsilon \in (0, 1)$ be arbitrary and $m_{\mathrm{error}} \sim \mathcal{N}(0, \sigma_{\mathrm{error}}^2)$
  be the error made at the measurment of the temperature. \\
  For $c^{2} > 2 \ln(1.25 / \delta)$, the
  Gaussian Mechanism, on the identity function, with parameter
  $\sigma \geq \sqrt{{c / \epsilon}^2 - \sigma_{\mathrm{error}}^2}$ is
  $(\epsilon, \delta)$-differentially private.
\end{proposition}

\begin{proof}
  Let $D \in \mathbb{R}^{k}$ be the measured temperatures of all k patients, such
  that $\forall i \in [1, k], D_i = m_i + m_{{\mathrm{error}}_i}$ with $m_i$ being
  the $true$ temperature of patient $i$ and $m_{{\mathrm{error}}_i}$ being the measurment error
  such that $m_{{\mathrm{error}}_i}$ are i.i.d. random variables drawn from $\mathcal{N}(0, \sigma_{\mathrm{error}}^2)$.
  Let the algorithm be to the identity function. Thus, adding a
  Gaussian mechanism to the algorithm ouputs:
  $$D + (Y_1,\cdots,Y_k)$$
  where $Y_i$ are i.i.d. random variables drawn from $\mathcal{N}(0, \sigma^2).$
  So, by definition of D, the output is:
  \begin{align*}
    D + (Y_1,\cdots,Y_k) &= (m + m_{\mathrm{error}}) + (Y_1,\cdots,Y_k) \\
    &= m + (m_{{\mathrm{error}}_1} + Y_1,\cdots,m_{{\mathrm{error}}_k} + Y_k).
  \end{align*}
  As the sum of normally distributed variable is also normally distributed with
  parameters being the sum of the means and the sum of the variances, we have:
  $$\forall i \in [1, k], m_{{\mathrm{error}}_i} + Y_i \sim \mathcal{N}(0, \sigma^2 + \sigma_{\mathrm{error}}^2).$$
  So by theorem \ref{th:gauss_mech}, if $\sqrt{\sigma^2 + \sigma_{\mathrm{error}}^2} \geq c\Delta_2 f / \epsilon$
  with $\Delta_2 f = \Delta_2 identity = 1$
  and $c^{2} > 2 \ln(1.25 / \delta)$, then adding $(m_{\mathrm{error}} + Y)$ to the $true$
  temperature is a Gaussian mechanism $(\epsilon, \delta)$-differentially private.\\
  Moreover, since we have:
  \begin{align*}
    \sqrt{\sigma^2 + \sigma_{\mathrm{error}}^2} &\geq c / \epsilon \\
    \sigma^2 + \sigma_{\mathrm{error}}^2 &\geq {c / \epsilon}^2 \\
    \sigma^2 &\geq {c / \epsilon}^2 - \sigma_{\mathrm{error}}^2 \\
    \sigma &\geq \sqrt{{c / \epsilon}^2 - \sigma_{\mathrm{error}}^2}. \\
  \end{align*}
  where the second and last inequalities follows that $x \mapsto x^2$ and $x \mapsto \sqrt{x}$
  are monotonic on $\mathbb{R}^+$. \\
  Then, we can state that the Gaussian Mechanism, on the identity function, with parameter
  $\sigma \geq \sqrt{{c / \epsilon}^2 - \sigma_{\mathrm{error}}^2}$ is
  $(\epsilon, \delta)$-differentially private.
\end{proof}

\section{Second problem}
\subsection{Statement}
Suppose 6 temperatures measurments are done on patients every $t$ time by an entity who should be able to publish
these measures without compromise patients identity. Suppose an adversary knows
the set of patients, can take these 6 temperatures from the patients but doesn't
know the measurment error.

How much noise should be added to these temperature measurments,
which are already noisy and highly correlated for one patient, so an acceptable level of privacy is met?

\subsection{Proposed answer}
Again, we define two possible answers depending on whether the differentially private
mechanism is Laplace or Gaussian. In both cases we suppose the measurment error
$m_{\mathrm{error}}$ be Gaussian such that $m_{\mathrm{error}} \sim \mathcal{N}(0, \sigma^2)$.\\
The difference with the previous problem is that, the 6 types of temperature
measurment are highly correlated and yet each account for a single record. However,
we can define this set of 6 types of temperature measurment as a group. By
theorem \ref{th:group}, if one sets $\epsilon$ instead to $\epsilon/6$, it falls
back to the first problem.

\bibliographystyle{IEEEtran}
\bibliography{refs}
\end{document}
