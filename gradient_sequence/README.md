# setup
Install scikit learn in dev mode, run
```
cd src/scikit-learn
pip install --verbose --no-build-isolation --editable .
```

# todo
* what it means to have multi-dimensionnal time-series
* derive a definition of autocorrelation
* without correlated noise, does it mean that noisy gradient sequence can be sanitized?
* if no autocorrelation, average of gradient can be replaced by robust method to lower down privacy loss
