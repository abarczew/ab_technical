import numpy as np


def get_sigmoid(X, m):
    return 1. / (1. + np.exp(-X.dot(m)))


def get_loss(X, y, m):
    probs = get_sigmoid(X, m)
    return - np.sum(y * np.log(probs)
                    + (1 - y) * np.log(1 - probs))


def get_gradient(X, y, m):
    return X.T.dot(((get_sigmoid(X, m) - y)))


def get_inv_hessian(X, y, m):
    probs = get_sigmoid(X, m)
    W = np.diag((probs * (1 - probs)).flatten())
    hessian = X.T.dot(W).dot(X)
    return np.linalg.inv(hessian)


def get_clip(v, c):
    v_norm = np.linalg.norm(v)
    return v / np.max([1, v_norm / c])


def gradient_descent(X, y, intercept=False, newton=False,
                     lr=0.001, c=None, epochs=10):
    if intercept:
        X = np.concatenate([X, np.ones((len(X), 1))], axis=1)
    ndim = X.shape[1]
    m = np.random.normal(0, 0.01, ndim).reshape((ndim, 1))
    loss_seq = []
    grad_seq = []

    for _ in range(epochs):

        grad = get_gradient(X, y, m)
        if c is not None:
            grad = get_clip(grad, c)
        if newton:
            inv_hess = get_inv_hessian(X, y, m)
            step = inv_hess.dot(grad)
        else:
            step = lr * grad
        m = m - step

        loss = get_loss(X, y, m)

        grad_seq.append(grad.flatten())
        loss_seq.append(loss)

    return m, np.array(loss_seq), np.array(grad_seq)
