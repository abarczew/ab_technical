import random
import numpy as np
from sklearn.datasets import load_breast_cancer
from sklearn.datasets import fetch_openml


def get_data(dataset_name):
    if dataset_name == 'cancer':
        data = load_breast_cancer()
        y = data.target
        X = data.data
    elif dataset_name == 'mnist':
        mnist = fetch_openml('mnist_784', version=1, cache=True)
        X, y = mnist.data, mnist.target.astype(np.int8)
    else:
        raise 'Dataset not in datasets list'
    return X, y


def sample_narray(narrays, sample_nb, seed=42):
    np.random.seed(seed)
    sample_idx = np.random.choice(range(len(narrays[0])),
                                  sample_nb,
                                  replace=False)
    return [narray[sample_idx, :] for narray in narrays]


def split_train_test(X, y, test_ratio=0.2, seed=42):
    n_samples = len(X)
    random.seed(seed)
    index_test = random.sample(range(n_samples), int(test_ratio * n_samples))
    index_train = [i for i in range(n_samples) if i not in index_test]
    X_test = X[index_test, :]
    y_test = y[index_test]
    X_train = X[index_train, :]
    y_train = y[index_train]
    return X_train, y_train, X_test, y_test
