from sklearn.metrics import roc_auc_score
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
import pandas as pd
import numpy as np
import statsmodels.api as sm
from conf import LOSSES, OUTDIR
from utils import get_data, split_train_test
import matplotlib.pyplot as plt
import copy
plt.style.use('seaborn-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)


learning_rates = {'constant': [0.1, 0.01, 0.001], 'optimal': [0.01, 0.1],
                  'invscaling': [0.1, 0.01, 0.001],
                  'adaptive': [0.1, 0.01, 0.001]}

n_epochs_range = [10, 100, 1000]
results = []
outpath = f'{OUTDIR}/grad_seq_explore_batchsgd'


def run():
    X, y = get_data()
    X_train, y_train, X_test, y_test = split_train_test(X, y)
    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
    for loss in LOSSES:
        for learning_rate in learning_rates.keys():
            for eta0 in learning_rates[learning_rate]:
                for n_epochs in n_epochs_range:
                    clf = SGDClassifier(loss=loss, learning_rate=learning_rate,
                                        eta0=eta0, warm_start=True)
                    coefs = []
                    etas = []
                    for _ in range(n_epochs):
                        clf.partial_fit(X_train, y_train,
                                        classes=np.unique(y_train))
                        coefs.append(copy.copy(clf.coef_))
                        etas.append(copy.copy(clf.eta))
                    auc = roc_auc_score(y_test, clf.decision_function(X_test))

                    coefs = np.array(list(map(lambda x: x.ravel(),
                                              np.array(coefs))))
                    dif_coef = coefs[:-1] - coefs[1:]
                    gradients_seq = - np.array([dif_coef[i] / etas[i]
                                                for i in range(len(dif_coef))])
                    # compute acf for lag=1 on all features of the gradient
                    acfs = [sm.tsa.stattools.acf(gradients_seq[:, i])[1]
                            for i in range(gradients_seq.shape[1])]
                    acfs = list(map(abs, acfs))
                    acf_max = np.max(acfs)
                    acf_min = np.min(acfs)
                    acf_mean = np.mean(acfs)
                    acf_std = np.std(acfs)

                    results.append({'loss': loss,
                                    'learning_rate': learning_rate,
                                    'eta0': eta0,
                                    'auc': auc,
                                    'acf_max': acf_max, 'acf_min': acf_min,
                                    'acf_mean': acf_mean, 'acf_std': acf_std})

    df_results = pd.DataFrame.from_records(results)
    df_results.to_csv(f'{outpath}.csv', index=False)


def plot_results():
    df_results = pd.read_csv(f'{outpath}.csv')
    # we filter on the results around 10% of the max auc
    df_plot = df_results.loc[df_results.auc >= df_results.auc.max()*(1-0.1)]
    plt.scatter(df_plot.auc, df_plot.acf_max)
    plt.grid()
    plt.xlabel('auc')
    plt.ylabel('acf_max')
    plt.legend()
    plt.savefig(f'{outpath}.png', bbox_inches='tight')
    plt.close()


if __name__ == '__main__':
    run()
    plot_results()
