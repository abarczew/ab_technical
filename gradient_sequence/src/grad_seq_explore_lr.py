from sklearn.decomposition import PCA
import pandas as pd
import numpy as np
import statsmodels.api as sm
from conf import OUTDIR
from utils import get_data
from logistic_regression import gradient_descent
import matplotlib.pyplot as plt
import seaborn as sns
plt.style.use('seaborn-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)


epochs = 100
newton = True
seed = 42
experiment_space = {
    'clipping_norm_range': [None, 1000, 100, 10, 1],
    'sample_size_range': [100, 1000, 10000],
    'features_nb_range': [3, 10, 100]
}
outpath = f'{OUTDIR}/grad_seq_explore_lr'


def run():
    X, y = get_data('mnist')
    X = np.array(X, dtype=np.float32) / 255
    y = np.array(y, dtype=np.float32)

    results_acfs = []
    results_sample_nb = []
    results_features_nb = []
    results_clipping_norm = []

    for c in experiment_space['clipping_norm_range']:
        for sample_nb in experiment_space['sample_size_range']:
            for features_nb in experiment_space['features_nb_range']:
                # select the number of feature
                pca = PCA(n_components=features_nb)
                X_pca = pca.fit_transform(X)

                # select the number of rows
                np.random.seed(seed)
                sample_idx = np.random.choice(range(len(X_pca)),
                                              sample_nb,
                                              replace=False)
                X_train = X_pca[sample_idx, :]
                y_train = y[sample_idx, np.newaxis]
                try:
                    _, _, grad_seq = gradient_descent(X_train,
                                                      y_train,
                                                      newton=newton,
                                                      c=c,
                                                      epochs=epochs)
                    grad_seq = grad_seq[:-1]
                    # compute acf for lag=1 on all features of the gradient
                    acfs = [sm.tsa.stattools.acf(grad_seq[:, i])[1]
                            for i in range(grad_seq.shape[1])]
                    acfs = list(map(abs, acfs))
                except np.linalg.LinAlgError:
                    acfs = [np.nan]
                results_clipping_norm += [c] * len(acfs)
                results_sample_nb += [sample_nb] * len(acfs)
                results_features_nb += [features_nb] * len(acfs)
                results_acfs += acfs

    results = {
        'clipping_norm': results_clipping_norm,
        'sample_nb': results_sample_nb,
        'features_nb': results_features_nb,
        'acfs': results_acfs
    }

    df_results = pd.DataFrame.from_dict(results, orient='columns')
    df_results.to_csv(f'{outpath}.csv', index=False)


def plot_results():
    df_results = pd.read_csv(f'{outpath}.csv').dropna()
    # we filter on the results around 10% of the max auc
    g = sns.FacetGrid(df_results, row="sample_nb",
                      col="features_nb", margin_titles=True)
    g.map(sns.regplot, "clipping_norm", "acfs",
          fit_reg=False, x_jitter=.1)
    plt.savefig(f'{outpath}.png', bbox_inches='tight')
    plt.close()


if __name__ == '__main__':
    run()
    plot_results()
