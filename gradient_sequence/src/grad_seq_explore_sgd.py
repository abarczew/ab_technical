from sklearn.metrics import roc_auc_score
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
import pandas as pd
import numpy as np
import statsmodels.api as sm
from conf import LOSSES, OUTDIR
from utils import get_data, split_train_test
import matplotlib.pyplot as plt
plt.style.use('seaborn-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)


learning_rates = {'constant': [0.1, 0.01, 0.001], 'optimal': [0.1],
                  'invscaling': [0.1, 0.01, 0.001], 'adaptive': [0.1, 0.01, 0.001]}

max_iter = 1000
tol = 1e-4
early_stopping = True
results = []
outpath = f'{OUTDIR}/grad_seq_explore_sgd'


def run():
    X, y = get_data()
    X_train, y_train, X_test, y_test = split_train_test(X, y)
    for loss in LOSSES:
        for learning_rate in learning_rates.keys():
            for eta0 in learning_rates[learning_rate]:
                clf = make_pipeline(StandardScaler(),
                                    SGDClassifier(loss=loss,
                                    max_iter=max_iter,
                                    tol=tol, verbose=0,
                                    learning_rate=learning_rate, eta0=eta0))
                clf.fit(X_train, y_train)
                auc = roc_auc_score(y_test, clf.decision_function(X_test))

                gradients_seq = clf['sgdclassifier'].weights_sequence[:clf['sgdclassifier'].n_iter_*len(X), :] \
                    - clf['sgdclassifier'].weights_sequence[1:clf['sgdclassifier'].n_iter_ *
                                                            len(X)+1, :]
                gradients_seq = gradients_seq[:-1]
                # compute acf for lag=1 on all features of the gradient
                acfs = [sm.tsa.stattools.acf(gradients_seq[:, i])[1]
                        for i in range(gradients_seq.shape[1])]
                acfs = list(map(abs, acfs))
                acf_max = np.max(acfs)
                acf_min = np.min(acfs)
                acf_mean = np.mean(acfs)
                acf_std = np.std(acfs)

                results.append({'loss': loss, 'learning_rate': learning_rate, 'eta0': eta0,
                                'auc': auc, 'acf_max': acf_max, 'acf_min': acf_min,
                                'acf_mean': acf_mean, 'acf_std': acf_std})

    df_results = pd.DataFrame.from_records(results)
    df_results.to_csv(f'{outpath}.csv', index=False)


def plot_results():
    df_results = pd.read_csv(f'{outpath}.csv')
    # we filter on the results around 10% of the max auc
    df_plot = df_results.loc[df_results.auc >= df_results.auc.max()*(1-0.1)]
    plt.scatter(df_plot.auc, df_plot.acf_max)
    plt.grid()
    plt.xlabel('auc')
    plt.ylabel('acf_max')
    plt.legend()
    plt.savefig(f'{outpath}.png', bbox_inches='tight')
    plt.close()


if __name__ == '__main__':
    run()
    plot_results()
