\documentclass{article}
\usepackage{hyperref}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{amsmath}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]
\usepackage[linesnumbered,ruled]{algorithm2e}
\usepackage{enumitem}
\usepackage{graphicx}
\graphicspath{ {../src/results/} }
\usepackage[textsize=tiny]{todonotes}
\newcommand{\janfoot}[1]{\textcolor{blue}{\footnote{\textcolor{blue}{{{#1}}}}}}

\begin{document}

\title{Differentially private gradient descent based on gradient autocorrelation}
\author{PhD student: Antoine Barczewski\\Supervisor: Jan Ramon}
\date{2022}
\maketitle

\textbf{Abstract.}
Trained machine learning models can leak sensitive information about their underlying training dataset. To qualify this risk, differentially private versions of Empirical Risk Minimization (ERM) have arised to achieve privacy guarantee without underming overall performances. The most popular approach is the DP-SGD which basically adds random pertubations at each update of the gradient. However, these techniques have poor performances  when reasonnable guarantees of differential privacy is achieved. We propose a novel approach to reduce the added noise which leverages the autocorrelation of the gradients obtained during a descent.

\input{subfiles/preliminaries}

\section{Related Work}
\subsection{Differentially Private Empirical Risk Minimization}
Differentially Private Empirical Risk Minimization (DP-ERM) has first been studied by adding noise to the result of the optimization program along its objectives \cite{chaudhuri_differentially_nodate}. To provide a gradient descent already private rather than only its result, noise terms are added at each gradient update \cite{bassily_differentially_2014} (DP-SGD) as detailed in algorithm \ref{alg:dp-sgd}. Faster convergence of the latter have been developed \cite{wang_differentially_2017} building up differential privacy on the SVRG algorithm \cite{johnson_accelerating_2013}. Although DP-SGD has turned out to be the standard approach, competitive results have been achieved when challenging the protocol of the gradient descent towards ERM, such as differentially private coordinate-wise updates \cite{mangold_differentially_2022}.

\input{subfiles/dp_sgd}


\subsection{Publishing Correlated Time-Series Data via
Differential Privacy}
\cite{wang_cts-dp_2017} proposes a correlated time-series data publication thanks to correlated noise. It ensures privacy while lowering the mean squared error to true time-series. The sanitizing of time-series thanks to the autocorrelation factor compels to either model the time serie or to change the space of it if one wants to add i.i.d. noise terms for differential privacy. Whereas correlated noise prevents from using these techniques and eventually requires less cumulated noise. Algorithm \ref{alg:clm} details how to build correlated Laplace series\janfoot{I think calibration of $h(\tau)$ is wrong and should be $\frac{\sqrt{\mathcal{R}_{XX}(\tau)}}{N_0}$ with $N_0=4\lambda$}.

\input{subfiles/clm}

\section{Publishing gradient sequence with correlated noise}
In this section we propose an algorithm $\mathcal{A}_{\textsc{AutoCorrNoise-GD}}$ (Algorithm \ref{alg:gradseq-sgd}) for computing $\theta^{priv}$ using a variant of the classic DP-SGD \cite{bassily_differentially_2014} replacing i.i.d. $b_t$, where $b_t \thicksim \mathcal{N}(0,\mathbb{I}_p\sigma^2)$, by correlated noise terms.

\input{subfiles/gradseq_sgd}

\begin{theorem}
  (Privacy guarantee) Algorithm $\mathcal{A}_{\textsc{AutoCorrNoise-GD}}$ (Algorithm \ref{alg:gradseq-sgd}) is ($\epsilon$, $0$)-differentially private.
\end{theorem}

\section{Experiment}
\subsection{Measure gradient autocorrelation factors from plain gradient descent}
We implemented a gradient descent for logistic regressions based on a Newton-Raphson method. Exploding gradients are controlled thanks to a norm clipping hyper-parameter. Figure \ref{fig:acf_lr} shows the measure of the autocorrelation of each feature of each gradient update when the descent is applied on a sample of the MNIST dataset. The following parameters are explored:
\begin{itemize}
  \item the number of samples - from 100 to 10000
  \item the number of features (selected by PCA) - from 3 to 100
  \item the clipping norm - from 1 to 100 and none
\end{itemize}

\begin{figure}[h!]
  \includegraphics[scale=0.5]{grad_seq_explore_lr}
  \caption{Gradient autocorrelation factors distribution}
  \label{fig:acf_lr}
\end{figure}

Note that, when the clipping norm is too big (none or 100) gradients "explode", their corresponding autocorrelation factors are not reported in figure \ref{fig:acf_lr}. Gradients appear to be highly correlated (more than $0.95$) and even more for features with the most information - first features from the PCA. These results bode well for publishing gradient with correlated noise terms.

\bibliographystyle{IEEEtran}
\bibliography{biblio}
\end{document}
