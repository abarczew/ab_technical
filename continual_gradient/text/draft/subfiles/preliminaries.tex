\newcommand{\dspace}{\mathcal{Z}}
\newcommand{\dsetspace}{\mathcal{Z}^*}
\newcommand{\dset}{Z}
\newcommand{\dseta}{{Z^1}}
\newcommand{\dsetb}{{Z^2}}
\newcommand{\adjdsets}{{\mathcal{Z}^*_\sim}}
\newcommand{\exz}{z}
\newcommand{\exx}{x}
\newcommand{\exy}{y}
\newcommand{\yspace}{{\mathcal{Y}}}
\newcommand{\Fll}{{F^{ll}}}
\newcommand{\sLipschitz}{{L^g}}
\newcommand{\vLipschitz}{L}
\newcommand{\thetat}{{{\tilde{\theta}}^{(t)}}}
\newcommand{\thetaT}{{{\tilde{\theta}}^{(T)}}}
\newcommand{\thetaone}{{{\tilde{\theta}^{(1)}}}}
\newcommand{\thetatsucc}{{{\tilde{\theta}}^{(t+1)}}}
\newcommand{\noiset}{{b^{(t)}}}
\newcommand{\gradt}{{g^{(t)}}}
\newcommand{\gradtv}{{g^{(t)}_v}}
\newcommand{\batchset}{{V}}
\newcommand{\batchi}{{v_i}}
\newcommand{\batchsize}{s}


\section{Preliminaries}\label{sec:preliminaries}
Let's define $\mathcal{D}=\left\{\left(x_i, y_i\right)\right\}_{i=1}^n:$ training points drawn i.i.d. from distribution $\mu$ over $\mathcal{X} \times \mathcal{Y}$. We are trying to build a model $h_\theta: \mathcal{X} \rightarrow \mathcal{Y}$ parameterized by $\theta \in \Theta \subseteq \mathbb{R}^p$, so it minimizes
$\ell(\theta ; x, y)$, loss of model $h_\theta$ on data point $(x, y)$.
$\hat{R}(\theta ; \mathcal{D})=\frac{1}{n} \sum_{i=1}^n \ell\left(\theta ; x_i, y_i\right)$, empirical risk of model $h_\theta$ and
$\psi(\theta)$, regularizer on model parameters (e.g., $2$-norm).

\begin{definition}[Empirical Risk Minimization (ERM)]
$$
\hat{\theta} \in \underset{\theta \in \Theta}{\arg \min }[F(\theta ; \mathcal{D}):=\hat{R}(\theta ; \mathcal{D})+\lambda \psi(\theta)]
$$
where $\lambda \geq 0$ is a trade-off hyperparameter.
\end{definition}

\begin{definition}[DP-SGD]
To minimize $F(\theta,\dset)$
 one can use stochastic gradient descent (SGD).  The SGD algorithm iteratively for a number of time steps $t=1\ldots T$ computes a gradient $\gradt = \nabla F(\thetat,\dset)$ on the current model $\thetat$ and updates the model using $\gradt$, setting $\thetatsucc=\thetat - \eta(t) \gradt$ where $\eta(t)$ is a learning rate. In each iteration of the SGD algorithm, the data owners and the Aggregator collaboratively compute a noisy gradient ${\hat{G}}_t = \frac{1}{\batchsize} \left(\sum_{i=1}^{\batchsize} \nabla \mathcal{L}(\thetat,\batchi) + \noiset\right)+\gamma\psi(\theta)$ over a sample $\batchset=\{\batchi\}_{i=1}^{\batchsize}$  of the data set where $\noiset$ is appropriate noise.

\begin{algorithm}\label{alg:dp-sgd}
    \SetKwInOut{Input}{Input}
    \SetKwInOut{Output}{Output}

    \Input{Examples $\left\{x_1, \ldots, x_N\right\}$, loss function $\mathcal{L}(\theta)=$ $\frac{1}{N} \sum_i \mathcal{L}\left(\theta, x_i\right)$. Parameters: learning rate $\eta_t$, noise scale $\sigma$, group size $L$, gradient norm bound $C$.}
    Initialize $\theta_0$;
    \For{$t \leftarrow 1$ \KwTo $T$}{
      Take a random sample $L_t$ with sampling probability $L / N$\;
      For each $i \in L_t$, compute $\mathbf{g}_t\left(x_i\right) \leftarrow \nabla_{\theta_t} \mathcal{L}\left(\theta_t, x_i\right)$;
      $\overline{\mathbf{g}}_t\left(x_i\right) \leftarrow \mathbf{g}_t\left(x_i\right) / \max \left(1, \frac{\left\|\mathbf{g}_t\left(x_i\right)\right\|_2}{C}\right)$\;
      $\tilde{\mathbf{g}}_t \leftarrow \frac{1}{L}\left(\sum_i \overline{\mathbf{g}}_t\left(x_i\right)+\mathcal{N}\left(0, \sigma^2 C^2 \mathbf{I}\right)\right)$\;
      $\theta_{t+1} \leftarrow \theta_t-\eta_t \tilde{\mathbf{g}}_t$;
    }
    \Output{$\theta_T$ and compute the overall privacy $\operatorname{cost}(\varepsilon, \delta)$ using a privacy accounting method}
    \caption{$\mathcal{A}_{\textsc{DP-SGD}}$: Differentially Private Stochastic Gradient Descent.}

\end{algorithm}
\end{definition}

\begin{theorem}[Moments accountant]
There exist constants $c_1$ and $c_2$ so that given the sampling probability $q=L / N$ and the number of steps $T$, for any $\varepsilon<c_1 q^2 T$, Algorithm \ref{alg:dp-sgd} is $(\varepsilon, \delta)$-differentially private for any $\delta>0$ if we choose
$$
\sigma \geq c_2 \frac{q \sqrt{T \log (1 / \delta)}}{\varepsilon} .
$$
\end{theorem}
If one uses the strong composition theorem, one will then need to choose $\sigma=\Omega(q \sqrt{T \log (1 / \delta) \log (T / \delta)} / \varepsilon)$. Note that a factor of $\sqrt{\log (T / \delta)}$ is saved.


\begin{definition}[Event-level $(\epsilon, \delta)$-DP]
  An algorithm $\mathrm{A}(\cdot)$ satisfies $(\epsilon, \delta)$-differential privacy $((\epsilon, \delta)$-DP), if and only if for any two neighboring sequences $V$ and $V^{\prime}$ and for any possible output set $O$,
  $$
  \operatorname{Pr}[\mathbf{A}(V) \in O] \leq e^\epsilon \operatorname{Pr}\left[\mathbf{A}\left(V^{\prime}\right) \in O\right]+\delta,
  $$
  where two sequences $V=\left\langle v_1, v_2, \ldots\right\rangle$ and $V^{\prime}=\left\langle v_1^{\prime}, v_2^{\prime}, \ldots\right\rangle$ are neighbors, denoted by $V \simeq V^{\prime}$, when $v_i=v_i^{\prime}$ for all $i$ except one index.
\end{definition}

We introduce the continual observation mechanism as detailed in \cite{dwork_differential_2010} for the count function $F_{\text {cnt }}$ under event-DP in the context of finite streams. We construct a binary decomposition spanning all the $T$ time intervals, resulting in $\log T$ levels. On each level, the Laplace mechanism provides a noisy count based on the interval. Subsequently, any value for $\tilde{F}\left(D_t\right)$ is derived by summing up to $\log T$ of these perturbed counts, drawing one from each level. To apportion the privacy budgets over these intervals, each interval is allocated an amount of $\varepsilon / \log T$, taking into account both basic composition (across levels) and parallel composition (within a level).
