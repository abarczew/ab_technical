# Medical statistics and privacy

[[_TOC_]]

## Datatsets used in the paper - section 6.2.2:

- The 'Heart disease prediction' dataset can be downloaded at: https://www.kaggle.com/dileep070/heart-disease-prediction-using-logistic-regression/version/1

- The 'Bank Full' dataset can be downloaded at: https://www.kaggle.com/krantiswalke/bankfullcsv

- The 'Diabetes data set' can be downloaded at: https://archive.ics.uci.edu/ml/datasets/Diabetes+130-US+hospitals+for+years+1999-2008#

Those datasets should be integrated in a `data` folder as follows:
```
├── dp-cum
│       ├── code
│       │       ├── data
│       │       │       ├── bank-full.csv
│       │       │       ├── diabetic_data.csv
...    ...     ...      └── framingham.csv
```

## Experiments:

To run the experiments, change directory to `dp-cum/code/`.
Experiments shown in section 6 can be reproduced thanks to the code in `experiments/`. Datasets should be downloaded as per section 6.2.2 and put in `data/` as described previously. Corresponding results will be created in `results/`.

* Note: If you are in a virtual environment, the command should be `python` (default we will use hereafter). 
Otherwise, you will use `python3`.

### Smoothing - Section 6.3

From folder `/code/`, run
```
PYTHONPATH=./ python experiments/smooth/smooth_experiment.py
```
raw results are then created in `results` (from which Figures 2 and 3 are created).

To get aggregated results and visualizations, run
```
PYTHONPATH=./ python experiments/smooth/smooth_results.py
```

### Inverse ecdf - Section 6.4

From folder `/code/`, run
```
PYTHONPATH=./ python experiments/inverse_ecdf/inv_experiment.py
```
raw results are then created in `results` (from which Figure 4 is created).

To get aggregated results and visualizations, run
```
PYTHONPATH=./ python experiments/inverse_ecdf/inv_results.py
```

### ROC curve - Section 6.5

From folder `/code/`, run
```
PYTHONPATH=./ python experiments/roc/roc_experiment.py
```
aggregated results are created in `results` (from which Figure 5 is created) along with
visualizations of the roc curve for each value of parameters.

### Hosmer-Lemeshow - Section 6.6

From folder `/code/`, run
```
PYTHONPATH=./ python experiments/hl_test/hl_smooth_experiment.py 
```
results of the test are created in `results`.

To create Figure 6 in the paper run
```
PYTHONPATH=./ python experiments/hl_test/hl_results.py 
```
The figure is created in `results`.

### Runtime - Section 6.7

From folder `/code/`, run
```
PYTHONPATH=./ python experiments/runtime/runtime_experiment.py 
```
raw results are then created in `results`.

To get aggregated results and visualizations, run
```
PYTHONPATH=./ python experiments/runtime/runtime_results.py 
```

## Results in details:

In the folder `results/`, the following files are created:

- For the section *6.3. Smoothing*:
  - Files named `smooth_exp_results_explore_normX.csv` and Tables in .tex, following the naming `smooth_exp_results_rate_explore_normX.tex`, where `X` should be replaced by the order of the considered norm;
  - A png file `smooth_exp_results_rate_epsilon_lambda3_n32768.png` that matches the figure 2, plotting the MSE Ratio according to the variations of epsilon, based on the csv files named `smooth_exp_results_epsilon_normX.csv`;
  - Another png file `smooth_exp_results_rate_lambda_epsilon1_n32768.png` that matches the figure 3, plotting the MSE Ratio according to the variations of lambda, based on the csv files named `smooth_exp_results_lambda_normX.csv`.

- For the section *6.4 Evaluating an ECDF or its inverse*:
  - Several png file, depending on the considered norm, in the form `inv_exp_results_epsilon_lambda3_n30000_normX.png`, one of them matching the figure 4, plotting the MSE Ratio according to the variations of epsilon, and based on the csv files named `inv_exp_results_explore_normX.csv`.

- For the section *6.5 ROC Curve evaluation*:
  - For each dataset, several png files are produced, depending on various parameters such as the epsilon range - their names take the form: `roc_exp_summary_DATASETNAME_EPSILON.png`.
  - Matching tex files with the same name are also produced.

- For the section *6.6 Hosmer-Lemeshow*:
  - For the 'bank full' dataset, a png file called `hl_smooth_exp_bank-full` is produced and matches the figure 6, based on the computation stored in the csv file `hl_smooth_exp_bank-full.csv`.

- For the section *6.7 Runtime*:
  - A csv file is produced and several png files are based on it:
    - The files called `runtime_exp_results_smooth_X`, where X can be N, the number of "time steps", or n, the number of events during those time steps, matches the figure 7.
    - Similar files with a different type of graph (here, bins) are created under the name `runtime_exp_results_bin_X`.

## Backlog

### Critical

- [x] update imports to reflect the new tree map.
- [x] fix input and output paths in "results" files that may not correct
- [x] Review fix for reading+forming res. of bank-full.csv dataset in `experiments/hl_test/hltest_smooth_experiment.py` and `experiments/hl_test/hl_results.py` - looks like the dataset recently downloaded exhibited changes.
- [ ] Calling `Explore.py` fails at compilation - need fixing.

### Not critical

- [ ] Merge `utils/hltest.py` and `utils/hltest_smooth.py` together 
- [X] Use ustat function from `utils/ustat.py` across experiments (especially hl_test)
- [ ] In `utils/utils/roc.py` line 97, thresholds are computed straight from the predictions array which implies the server has it completely. probably this should be done otherwise so the data can be protected.
- [ ] Investigate `test_roc_ustat_smooth` in `test.py` which fails

- [ ] Complete all docstrings w/ reference to the equations/figures/sections of the paper
- [ ] Rename variables overshadowing others at different scopes
- [ ] Add print prompt w/ verbose option?
- [ ] Make roc, hl_test run in parallel on different datasets?
- [ ] To be added to the README file: How Fig. 8, 9, 10 are created?

#### In details:

- [ ] In file `smooth_experiment.py`:
  - [ ] Extract func. `experiment` from func. `run`
  - [ ] Write generic func. to save results
- [ ] In file `inv_experiment.py`:
  - [ ] Explicit short-named variables (`N`, `n`, `L`) w/ references to the paper
  - [ ] Extract func. `experiment` from func. `run`
- [ ] In file `roc_experiment.py`:
  - [X] Extract small UStat class from func. `get_roc_exp_score`
- [ ] In files `hl_test_experiment.py` and `hl_smooth_experiment`:
  - [ ] Explicit short-named variables
- [ ] In file `runtime_experiment.py`
  - [ ] Clean Up unused variables
