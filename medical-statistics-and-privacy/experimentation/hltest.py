from collections import namedtuple
import numpy as np
import scipy
from typing import Callable
from numpy.random import laplace


def hosmer_lemeshow_test(y_true: np.array, y_pred: np.array, n_groups: int = 10) -> namedtuple:
    """Performs a Hosmer–Lemeshow test.
    Parameters
    ----------
    y_true : array
        Observed labels, either 0 or 1.
    y_pred : array
        Predicted probabilities, floats on [0, 1].
    n_groups : int, optional
        The number of groups to create. The default value is 10, which
        corresponds to deciles of predicted probabilities.
    Returns
    -------
    hlstat : float
        The test statistic.
    df : int
        The degrees of freedom of the test.
    p : float
        The p-value of the test.
    """
    a = np.stack((y_true, y_pred), axis=-1)
    a = a[a[:, 1].argsort()]
    hltable = np.array_split(a, n_groups)

    def hlt_sub(y_true: np.array, y_pred: np.array) -> float:
        diff_success = (y_true.sum() - y_pred.sum())**2 / y_pred.sum()
        diff_fail = ((1 - y_true).sum() - (1 - y_pred).sum())**2 / (1 - y_pred).sum()
        return diff_success + diff_fail

    hlstat = np.sum(list(map(lambda x: hlt_sub(x[:, 0], x[:, 1]), hltable)))

    df = n_groups - 2
    p = scipy.stats.chi2.sf(hlstat, df)

    TestResult = namedtuple('HosmerLemeshowTest', ('hlstat', 'df', 'p'))
    return TestResult(hlstat, df, p)


def ustat(p, y_pred, lower, upper):
    mask = np.logical_and((lower <= y_pred), (y_pred < upper))
    return np.sum(p[mask]) / len(p)


def ustat_search(a: float, b: float, t: float, psi: float,
                 p: np.array, ustat: Callable[[np.array, float], float]) -> float:
    """Performs a binary search with ustat.
    Parameters
    ----------
    a : float
        Lower bound of the research interval.
    b : float
        upper bound of the research interval.
    t : float
        Target.
    psi : float
        Precision.
    p : array
        Array of probabilities on which the ustat is applied.
    ustat : function
        U-statistic.
    Returns
    -------
    The searched reversed value of the u-statistic.
    """
    if b < a:
        raise ValueError("Lower bound is supposed to be lower than the upper bound")

    indicator = np.ones_like(p)
    while b - a > psi:
        m = (a + b) / 2
        f_m = ustat(indicator, p, 0, m)
        if f_m < t:
            a = m
        else:
            b = m
    return (a + b) / 2


def hosmer_lemeshow_test_w_search(y_true: np.array, y_pred: np.array, psi: float,
                                  ustat: Callable[[np.array, float], float],
                                  n_groups: int = 10) -> namedtuple:
    """Performs a Hosmer–Lemeshow test.
    Parameters
    ----------
    y_true : array
        Observed labels, either 0 or 1.
    y_pred : array
        Predicted probabilities, floats on [0, 1].
    psi : float
        Precision.
    ustat : function
        U-statistic.
    n_groups : int, optional
        The number of groups to create. The default value is 10, which
        corresponds to deciles of predicted probabilities.
    Returns
    -------
    hlstat : float
        The test statistic.
    df : int
        The degrees of freedom of the test.
    p : float
        The p-value of the test.
    """
    t = np.empty(shape=(n_groups+1, ))
    t[0] = 0
    hlstat = 0
    n = len(y_true)
    for i in np.arange(1, n_groups+1):
        if i < n_groups:
            t[i] = ustat_search(t[i-1], 1, i/n_groups, psi, y_pred, ustat)
        else:
            t[i] = 1 + psi
        E_0 = n * ustat(1-y_pred, y_pred, t[i-1], t[i])
        O_0 = n * ustat(1-y_true, y_pred, t[i-1], t[i])
        E_1 = n * ustat(y_pred, y_pred, t[i-1], t[i])
        O_1 = n * ustat(y_true, y_pred, t[i-1], t[i])
        hlstat += ((O_0 - E_0)**2) / E_0 + ((O_1 - E_1)**2) / E_1

    df = n_groups - 2
    p = scipy.stats.chi2.sf(hlstat, df)

    TestResult = namedtuple('HosmerLemeshowTest', ('hlstat', 'df', 'p'))
    return TestResult(hlstat, df, p)


def ustat_search_noise(a: float, b: float, t: float, psi: float,
                       p: np.array, ustat: Callable[[np.array, float], float], epsilon: float, n_groups: int = 10) -> float:
    """Performs a binary search with ustat.
    Parameters
    ----------
    a : float
        Lower bound of the research interval.
    b : float
        upper bound of the research interval.
    t : float
        Target.
    psi : float
        Precision.
    p : array
        Array of probabilities on which the ustat is applied.
    ustat : function
        U-statistic
    n_groups : int, optional
        The number of groups to create. The default value is 10
    epsilon : The privacy budget.
    Returns
    -------
    The searched reversed value of the u-statistic.
    """
    L = int(np.ceil(len(p)))
    laplace_scale = (np.ceil(2*np.log2(1/psi))+2+4*n_groups)/(len(p)*epsilon)
    noise_dict = {}
    for i in range(1, len(p)+1):
        for j in range(1, L+1):
            if(noise_dict.get((int(np.ceil(i/2**j)), j)) is None):
                noise_dict[(int(np.ceil(i/2**j)), j)] \
                    = np.random.laplace(scale=laplace_scale)
    if b < a:
        raise ValueError("Lower bound is supposed to be lower than the upper bound")

    indicator = np.ones_like(p)
    while b - a > psi:
        m = (a + b) / 2
        i = t*n_groups
        select_noise = [(int(np.ceil(i/2**j)), j) for j in range(1, L+1)]
        f_m = ustat(indicator, p, 0, m)+sum(value for key,
                                            value in noise_dict.items()
                                            if key in select_noise)
        if f_m < t:
            a = m
        else:
            b = m
    return (a + b) / 2


def hosmer_lemeshow_test_w_search_noise(y_true: np.array, y_pred: np.array,
                                        psi: float,
                                        ustat: Callable[[np.array, float], float],
                                        epsilon: float,
                                        n_groups: int = 10) -> namedtuple:
    """Performs a Hosmer–Lemeshow test.
    Parameters
    ----------
    y_true : array
        Observed labels, either 0 or 1.
    y_pred : array
        Predicted probabilities, floats on [0, 1].
    psi : float
        Precision.
    ustat : function
        U-statistic.
    n_groups : int, optional
        The number of groups to create. The default value is 10, which
        corresponds to deciles of predicted probabilities.
    Returns
    -------
    hlstat : float
        The test statistic.
    df : int
        The degrees of freedom of the test.
    p : float
        The p-value of the test
    epsilon : The privacy budget
    """
    n = len(y_true)
    t = np.empty(shape=(n_groups+1, ))
    t[0] = 0
    hlstat = 0
    laplace_scale = (np.ceil(2*np.log2(1/psi))+2+4*n_groups)/(n*epsilon)
    for i in np.arange(1, n_groups+1):
        if i < n_groups:
            t[i] = ustat_search_noise(t[i-1], 1, i/n_groups, psi, y_pred, ustat, epsilon)
        else:
            t[i] = 1 + psi
        E_0 = n * ustat(1-y_pred, y_pred, t[i-1], t[i]) + np.random.laplace(scale=laplace_scale)
        O_0 = n * ustat(1-y_true, y_pred, t[i-1], t[i]) + np.random.laplace(scale=laplace_scale)
        E_1 = n * ustat(y_pred, y_pred, t[i-1], t[i]) + np.random.laplace(scale=laplace_scale)
        O_1 = n * ustat(y_true, y_pred, t[i-1], t[i]) + np.random.laplace(scale=laplace_scale)
        hlstat += ((O_0 - E_0)**2) / E_0 + ((O_1 - E_1)**2) / E_1

    df = n_groups - 2
    p = scipy.stats.chi2.sf(hlstat, df)

    TestResult = namedtuple('HosmerLemeshowTest_noise', ('hlstat', 'df', 'p'))
    return TestResult(hlstat, df, p)


if __name__ == '__main__':
    from sklearn.datasets import load_breast_cancer
    from sklearn.linear_model import LogisticRegression
    X, y = load_breast_cancer(return_X_y=True)
    clf = LogisticRegression(random_state=0).fit(X, y)
    y_pred = clf.predict_proba(X)[:, 1]

    print(hosmer_lemeshow_test(y, y_pred))
    print(hosmer_lemeshow_test_w_search(y, y_pred, 0.0001, ustat))
    print(hosmer_lemeshow_test_w_search_noise(y, y_pred, 0.0001, ustat, 0.1))
