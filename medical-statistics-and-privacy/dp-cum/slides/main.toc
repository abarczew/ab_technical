\beamer@sectionintoc {1}{Motivation and results}{3}{0}{1}
\beamer@sectionintoc {2}{Definitions}{6}{0}{2}
\beamer@sectionintoc {3}{Private ECDF}{8}{0}{3}
\beamer@sectionintoc {4}{Smooth private ECDF}{19}{0}{4}
\beamer@sectionintoc {5}{Algorithm}{23}{0}{5}
\beamer@sectionintoc {6}{Applications}{27}{0}{6}
\beamer@sectionintoc {7}{Conclusion}{35}{0}{7}
