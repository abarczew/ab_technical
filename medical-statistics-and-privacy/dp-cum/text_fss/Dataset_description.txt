﻿The methods described in this paper were tested on different data sets.
\begin{itemize}
	\item Breast cancer Wisconsen: This data set intends to predict if the breast cancer type is Malignent or Benign using features like its radius, area and texture (data set available in scikit-learn python package https://scikit-learn.org/stable/datasets/toy_dataset.html). 

	\item Heart disease prediction : This data set aims to pinpoint the most relevant/risk factors of heart disease as well as predict the 10 year risk of coronary heart disease using information about the individuals (e.g. age, gender, smoking habits and blood pressure).
 (https://www.kaggle.com/dileep070/heart-disease-prediction-using-logistic-regression)

	\item Bank full : This data set is realted with a marketing compaigns of a Portuguese banking institution. It intends to know if the client would subscribe or not to the product (bank term deposit), using features about the clients (e.g. age, job, education level, martial status, owning a house, having loans).
 (https://www.kaggle.com/krantiswalke/bankfullcsv)

	\item Diabetes data set: It studies the effets of diabetes to the readmission of patients to the hospital (https://archive.ics.uci.edu/ml/datasets/Diabetes+130-				US+hospitals+for+years+1999-2008#)

	\item Patient survival prediction: This data set aims to predict in-hospital mortality for all-cause admitted patients. It contains information about the patient (e.g age, ethnicity, gender) and information about the reason of admission to the hospital. (https://www.kaggle.com/mitishaagarwal/patient)




\begin{tabular}{llll}
Data set & \#instances & positives (fraction of instances which are 
positive) & number of features \\

Breast cancer Wisconsin & 569 & 0.627 & 30 \\

Heart disease prediction & 4328 & 0.152 & 15 \\

Bank-full & 45211 & 0.117 & 16 \\

Diabetes & 101766 & 0.46 & 49 \\

Patient survival prediction & 91713 & 0.086 & 84 
\end{tabular}
