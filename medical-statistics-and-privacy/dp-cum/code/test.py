import unittest
import numpy as np

from utils.ustat import UStat
from utils.roc import roc_curve, roc_curve_ustat, geo_score
# from search import search
# from hltest import hosmer_lemeshow_test, hosmer_lemeshow_test_w_search


class UStatTest(unittest.TestCase):
    def test_phi_ustat(self):
        """
        Test that ustat can use custom phi.
        """
        class UStatPhiTest(UStat):
            """docstring for UStatTest."""

            def __init__(self):
                self.x = np.array([3, 1, 2, 1.5])

            def get_instances(self, args):
                return self.x

        stat = UStatPhiTest()
        t = 3

        result = stat.ustat(lambda x: x**2 < t)
        self.assertEqual(result, 0.5)


# class TestSearch(unittest.TestCase):
#     def test_search(self):
#         """
#         Test that it can compute search on array.
#         """
#         class UStatSearchTest(UStat):
#             """docstring for UStatTest."""
#
#             def __init__(self):
#                 self.x = np.array([3, 1, 2, 1.5])
#
#             def get_instances(self, args):
#                 return self.x
#
#         stat = UStatSearchTest()
#         t = 0.5
#         a = 0
#         b = 3
#         psi = 0.01
#
#         result = search(a, b, t, psi, stat)
#         self.assertAlmostEqual(result, 1.50, 2)
#
#
# class TestHltest(unittest.TestCase):
#     def test_hosmer_w_search(self):
#         """
#         Test that it is close enough to standard method.
#         """
#         class UStatHltTest(UStat):
#             """docstring for UStatTest."""
#
#             def __init__(self):
#                 self.o = np.round(np.random.rand(100))
#                 self.p = np.random.rand(100)
#
#             def get_instances(self, args):
#                 return getattr(self, args)
#
#         stat = UStatHltTest()
#         n_groups = 10
#         psi = 0.01
#         result_true = hosmer_lemeshow_test(stat.o, stat.p, n_groups)
#
#         result_test = hosmer_lemeshow_test_w_search(stat, psi=psi,
#                                                     n_groups=n_groups)
#         self.assertAlmostEqual(getattr(result_true, 'hlstat'),
#                                getattr(result_test, 'hlstat'), 0)


class TestROC(unittest.TestCase):
    def test_roc(self):
        """
        Test that plain roc works
        """
        y_true = np.array([0, 0, 1, 1])
        y_score = np.array([0.1, 0.4, 0.35, 0.8])
        tpr_true = np.array([0., 0.5, 0.5, 1., 1., 1.])

        fpr, tpr = roc_curve(y_true, y_score)
        self.assertSequenceEqual(tpr.tolist(), tpr_true.tolist())

    def test_roc_ustat(self):
        """
        Test that roc can work with ustat
        """
        class UStatRocTest(UStat):
            """docstring for UStatTest."""

            def __init__(self):
                self.y_true = np.array([0, 0, 1, 1])
                self.y_score = np.array([0.1, 0.4, 0.35, 0.8])

            def get_instances(self, args):
                return getattr(self, args)

        stat = UStatRocTest()
        tpr_true = np.array([0., 0.5, 0.5, 1., 1., 1.])

        fpr, tpr = roc_curve_ustat(stat)
        self.assertSequenceEqual(tpr.tolist(), tpr_true.tolist())

    def test_roc_ustat_noise(self):
        """
        Test that roc can work with ustat and noise
        """
        class UStatRocNoiseTest(UStat):
            """docstring for UStatTest."""

            def __init__(self):
                self.y_true = np.array([0, 0, 1, 1])
                self.y_score = np.array([0.1, 0.4, 0.35, 0.8])

            def get_instances(self, args):
                return getattr(self, args)

        stat = UStatRocNoiseTest()
        tpr_true = np.array([0.5, 0.5, 1., 1.])
        epsilon = 0.9

        fpr, tpr = roc_curve_ustat(stat, epsilon=0.9)
        # first and last values are 0 and 1 for display purposes
        res = (tpr_true / tpr[1: -1]) < np.exp(epsilon)
        res_true = np.ones(len(res))
        self.assertSequenceEqual(res.tolist(), res_true.tolist())

    def test_roc_ustat_smooth(self):
        """
        Test that roc can work with ustat and noise
        """
        class UStatRocSmoothTest(UStat):
            """docstring for UStatTest."""

            def __init__(self):
                self.y_true = np.array([0, 0, 1, 1])
                self.y_score = np.array([0.1, 0.4, 0.35, 0.8])

            def get_instances(self, args):
                return getattr(self, args)

        stat = UStatRocSmoothTest()
        tpr_true = np.array([0.5, 0.5, 1., 1.])
        epsilon = 0.9
        smooth = True

        fpr, tpr = roc_curve_ustat(stat, epsilon=0.9, smooth=smooth)
        # first and last values are 0 and 1 for display purposes
        res = (tpr_true / tpr[1: -1]) < np.exp(epsilon)
        res_true = np.ones(len(res))
        self.assertSequenceEqual(res.tolist(), res_true.tolist())


class TestGeoScore(unittest.TestCase):
    def test_geo_score(self):
        """
        Test geo score
        """
        x_f = np.array([0, 1, 2])
        y_f = np.array([0, 1, 2])
        x_g = np.array([0, 1, 2])
        y_g = np.array([2, 1, 0])

        score_true = 2

        score = geo_score(x_f, y_f, x_g, y_g)

        self.assertEqual(score_true, score)


if __name__ == '__main__':
    unittest.main()
