from matplotlib import pyplot as plt
import seaborn as sns
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from time import time
from joblib import Parallel, delayed
from experiments.fss.pysrc.simulation_structures import *
from experiments.fss.pysrc.DistributedComparisonFunctionForPParties import DistributedComparisonFunctionForPParties
from experiments.fss.pysrc.utils import *
import numpy as np
import pandas as pd
from utils.dp_counter import generate_noise
from utils.smooth import rv_index, smooth_dense
import os


# fss lib

# utils


plt.style.use('seaborn-v0_8-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)


def generate_synthetic_data(
    distribution: object,
    distribution_params: dict,
    size: int
):
    """
    Generate synthetic data with scipy.stat distribution objects
    """
    return distribution.rvs(size=size, **distribution_params)


def discretize(data, domain, number_bins):
    """
    Create the bins for the input precision and use the numpy function digitize on the data and the created bins.

    params :
        data (numpy.array) The data to discretize
        precision : Number of decimals to keep

    return :
        tuple of size 2, the bins, and the return of np.digitize(data,bins)
    """
    # Avoir un tuple range ex: (0,100) découpé en un nombre de bins
    # Domaine F --> transforme en bins
    alpha = (domain[1] - domain[0]) / number_bins
    bins = np.array([domain[0] + alpha * i for i in range(number_bins)])
    #bins = np.array([10 ** -precision * i for i in range(1,int(max(data) * 10 ** precision) + 1)])
    return bins, np.digitize(data, bins, right=True)


def parse_results(
    data: np.array,
    bins: np.array,
    method: str
):
    """
    Parse the results from the input data, wich is assumed to correspond to the binned representation of the data with bins

    return : (np.array) An approximation of the results in the default representation
    """
    if method == 'mean':  # Moyenne entre bin[i] et bin[-1] ; attention aux 0
        return (bins[data] + bins[data - 1]) / 2
    if method == 'sup':  # bin[i]
        return bins[data]


def compute_every_key(fss, server, N):
    """
    Make the server evaluate all his key with the given fss instance
    Made for parallel computing.
    """
    nb_keys = server.count
    res = np.zeros((nb_keys, N))
    for k in range(nb_keys):
        # implement the mapping function here, see if N need to be updated, I think N here refers to the number of thresholds instead of the domain size
        res[k] = server.evaluate(fss, k)
    return res


def fss_experiment(p: int,
                   do: int,
                   N: int,
                   q: int,
                   l: int,
                   data: np.array,
                   thresholds: np.array,
                   verbose: bool = False) -> np.array:
    """
    Experiment the distributed comparison function secret sharing scheme.
    Run the experiments using the fss parameters and the data to share into the data owners.

    The experiment use parallel computing with default parameter 'n_jobs' set to -1.

    The experiment follow the following scheme :

        * Creation of the differents parties (data owners, and servers)
        * Generation of the key by the data owners, wich are sent to the different servers
        * Evaluation of their own shares by the servers
        * Summing of the shares, summing all keys and then output the results as a dict

    params :
        p : (int) Number of servers wich compute the comparison function
        do : (int) Number of data owners wich generate the function's shares
        N : (int) Domain of the function (number of values in the dataset)
        q : (int) Domain of G, the output of the comparison function
        l : (int) Lambda, the security parameter in number of bits
        data : (numpy.array) The data to share between data owner
        thresholds : (numpy.array) Values to compare the domain with

    return :
     (dict) : A python dictionary with 4 entries :
        'output' : The output array of the function
        'time' : The compute time of the entire function
        'key_size' : The size of the keys serialized
        'key_size_deserialized' : The size of the keys deserialized

    """

    servers = []
    data_owners = []

    # instantiation of the FSS DCF scheme
    fss = DistributedComparisonFunctionForPParties(p=p, N=N, q=q, lambd=l)

    data_shares = np.array_split(data, do)

    # Creation of the servers
    for _ in range(p):
        servers.append(Server())

    # Creation of the data owners
    for i in range(do):
        data_owners.append(DataOwner(data_shares[i], N=N, bins=thresholds))

    # Generation of keys and sending to the servers

    for i in range(do):
        for keys in data_owners[i].generate(fss):
            for k in range(p):
                servers[k].receive(keys[k])

    # Servers evaluate all of the function's shares on the domain, sums the result and output it (maybe applicate differential privacy)

    res_list = Parallel(n_jobs=-1)(
        delayed(compute_every_key)(fss, servers[i], N) for i in range(p)
    )

    # Servers sums their results to compute f(x_i)

    outputs = res_list[0]
    for res in res_list[1:]:
        outputs = (outputs + res) % q

    # Summing the results for each x_i in X

    output = np.sum(outputs, axis=0)

    return output


def agg_experiment(p: int,
                   do: int,
                   N: int,
                   q: int,
                   l: int,
                   data: np.array,
                   thresholds: np.array,
                   verbose: bool = False) -> np.array:

    servers = []
    data_owners = []

    data_shares = np.array_split(data, do)

    agg = DistributedPlainSharing(thresholds)

    # Creation of the servers
    for _ in range(p):
        servers.append(Server())

    # Creation of the data owners
    for i in range(do):
        data_owners.append(DataOwner(data_shares[i], N=N, bins=thresholds))

    if p <= do:
        dataowner_server_idx = np.array_split(np.arange(do), p)
    else:
        dataowner_server_idx = [[i] for i in range(do)]

    for i, dataowner_idx in enumerate(dataowner_server_idx):
        for j in dataowner_idx:
            servers[i].receive(data_owners[j])

    # Servers evaluate all shares on the domain, sums the result and output it
    res_list = Parallel(n_jobs=-1)(
        delayed(compute_every_key)(agg, servers[i], N) for i in range(p)
    )

    # Servers sums their results to compute f(x_i)

    outputs = res_list[0]
    for res in res_list[1:]:
        outputs = (outputs + res) % q

    # Summing the results for each x_i in X

    output = np.sum(outputs, axis=0)

    return output


def transform_eval_input(data, N):
    if data.min() < 0:
        data = data - data.min()
    return data * N / data.max()


def add_roc_dp(fpr_numerator, tpr_numerator,
               epsilon, smooth_norm=None, seed=42):
    N = len(tpr_numerator)
    rvidx = rv_index(N)
    scale = epsilon / rvidx.L
    rng = np.random.default_rng(seed)
    _, sumeta_fpr = generate_noise(rng, scale, rvidx)
    _, sumeta_tpr = generate_noise(rng, scale, rvidx)
    fpr = fpr_numerator + sumeta_fpr / fpr_numerator.max()
    tpr = tpr_numerator + sumeta_tpr / tpr_numerator.max()
    if smooth_norm is not None:
        fpr, _ = smooth_dense(fpr, rvidx, smooth_norm, 0, 1)
        tpr, _ = smooth_dense(tpr, rvidx, smooth_norm, 0, 1)
    return fpr, tpr


def get_distributed_roc(experiment, p, do, N, q, l,
                        data, ground_truth, precision=1e6,
                        epsilon=None, seed=42, smooth_norm=None, pos_label=1):
    data_f = data[ground_truth != pos_label]
    data_t = data[ground_truth == pos_label]

    data_f_input = transform_eval_input(data_f, precision)
    data_t_input = transform_eval_input(data_t, precision)

    thresholds = np.linspace(0, precision, N)

    data_f_results = experiment(p=p, do=do, N=N, q=q, l=l, data=data_f_input,
                                thresholds=thresholds)
    data_t_results = experiment(p=p, do=do, N=N, q=q, l=l, data=data_t_input,
                                thresholds=thresholds)

    if epsilon is not None:
        fpr, tpr = add_roc_dp(data_f_results,
                              data_t_results,
                              epsilon,
                              smooth_norm,
                              seed)
    else:
        fpr = data_f_results / data_f_results.max()
        tpr = data_t_results / data_t_results.max()

    fpr = np.r_[0, fpr, 1]
    tpr = np.r_[0, tpr, 1]

    return fpr, tpr


def auc(x: np.array,
        y: np.array):
    """
    Compute Area Under the Curve (AUC) using the trapezoidal rule.

    :param x : ndarray of shape (n,)
        x coordinates. These must be either monotonic increasing or monotonic
        decreasing.
    :param y : ndarray of shape, (n,)
        y coordinates.

    :return: float auc:
    """
    area = np.trapz(y, x)
    return area


def plot_roc(fpr, tpr):
    roc_auc = auc(fpr, tpr)

    import matplotlib.pyplot as plt
    plt.style.use('ggplot')

    fig, ax = plt.subplots()

    label = f"(AUC = {roc_auc:0.2f})"

    xlabel = "False Positive Rate"
    ylabel = "True Positive Rate"
    ax.set(xlabel=xlabel, ylabel=ylabel)

    ax.legend(loc="lower right")

    ax.plot(fpr, tpr, label=label)
    plt.show()


def get_pred(dataset_name='framingham', datadir='/data'):
    df_fram = pd.read_csv(f'{datadir}/{dataset_name}.csv').fillna(0)
    X = df_fram[[col for col in df_fram.columns if col != 'TenYearCHD']]
    y = df_fram.TenYearCHD

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33,
                                                        random_state=42)

    classif = LogisticRegression(solver='lbfgs')
    classif = make_pipeline(StandardScaler(with_mean=False),
                            classif)
    classif.fit(X_train, y_train)
    y_pred = classif.decision_function(X_test)

    return y_test, y_pred


def sample_roc(fpr, tpr, N, fpr_base, tpr_base, thresholds):
    sample_idx = np.random.choice(np.arange(len(tpr)),
                                  np.min([len(thresholds), N])-2,
                                  replace=False)
    if len(thresholds) <= N:
        fpr = np.r_[0, fpr[sample_idx], 1]
        tpr = np.r_[0, tpr[sample_idx], 1]
    else:
        fpr_base = np.r_[0, fpr_base[sample_idx], 1]
        tpr_base = np.r_[0, tpr_base[sample_idx], 1]
    return fpr, tpr, fpr_base, tpr_base


def get_geo_score(x_f,
                  y_f,
                  x_g,
                  y_g):
    """
    geo_score: ?
    :param x_f: array
        x coordinates of the f line to draw
    :param y_f: array
        y coordinates of the f line to draw
    :param x_g: array
        x coordinates of the g line to draw, g has same length as f
    :param y_g: array
        y coordinates of the g line to draw, g has same length as f

    :return float geo_score:
        sum of areas between f and g
    """
    idx = np.argwhere(np.diff(np.sign(y_f - y_g))).flatten()
    # make sure we get the last area
    idx = np.r_[idx, len(x_f)-1]
    # compute the difference of areas between each interception
    geo_score = 0
    for i, j in list(zip(idx[:-1], idx[1:])):
        area_f = np.trapz(y_f[i:j+1], x_f[i:j+1])
        area_g = np.trapz(y_g[i:j+1], x_g[i:j+1])
        geo_score += abs(area_f - area_g)
    return geo_score


def ecdf(data, N):
    ecdf_results = []
    for i in np.linspace(data.min(), data.max(), N):
        ecdf_results.append(len(data[data <= i]))
    return np.array(ecdf_results)


def get_centralized_roc(y_test, y_pred, N):
    fpr = ecdf(y_pred[y_test != 1], N)
    tpr = ecdf(y_pred[y_test == 1], N)

    fpr = np.r_[0, fpr / fpr.max(), 1]
    tpr = np.r_[0, tpr / tpr.max(), 1]

    return fpr, tpr


def main():
    directory = "results/fss"
    if not os.path.exists(directory):
        os.makedirs(directory)

    y_test, y_pred = get_pred()

    q = 2
    l = 10
    N = 100
    p = 2
    do = 3
    records = []
    outpath = f"{directory}/test.png"

    fpr, tpr = get_distributed_roc(fss_experiment,
                                   p, do, N, q, l, y_test, y_pred)
    import ipdb
    ipdb.set_trace()
    #
    # # parties and data owners
    # q = 2
    # l = 10
    # N = 100
    # parties_range = [2, 5, 10, 100]
    # do_range = [2, 5, 10, 100]
    # records = []
    # outpath = f"{directory}/parties_do_runtime.png"
    # for p in parties_range:
    #     for do in do_range:
    #         fpr, tpr = get_distributed_roc(p, do, N, q, l, y_test, y_pred)
    #         fpr_base, tpr_base = get_centralized_roc(y_test, y_pred, N)
    #         geo_score = get_geo_score(fpr, tpr, fpr_base, tpr_base)
    #         record = {'p': p, 'do': do, 'time': time, 'geo_score': geo_score}
    #         print(record)
    #         records.append(record)
    #
    # df = pd.DataFrame.from_records(records)
    # df_plot = pd.pivot_table(df, values='time', index=['p'],
    #                          columns=['do'], aggfunc="mean")
    #
    # sns.heatmap(df_plot, annot=True)
    # plt.savefig(outpath)
    # plt.close()
    #
    # # N points
    # p = 3
    # do = 4
    # q = 2
    # l = 10
    # N_range = [10, 100, 1000, 10000]
    # records = []
    # outpath = f"{directory}/n_runtime.png"
    # for N in N_range:
    #     fpr, tpr, time = get_distributed_roc(p, do, N, q, l, y_test, y_pred)
    #     fpr, tpr, fpr_base, tpr_base = sample_roc(fpr,
    #                                               tpr,
    #                                               N,
    #                                               fpr_base,
    #                                               tpr_base,
    #                                               thresholds)
    #     geo_score = get_geo_score(fpr, tpr, fpr_base, tpr_base)
    #     record = {'N': N, 'time': time, 'geo_score': geo_score}
    #     print(record)
    #     records.append(record)
    #
    # df = pd.DataFrame.from_records(records)
    # plt.plot(df['N'], df['time'])
    # plt.savefig(outpath)
    # plt.close()


if __name__ == '__main__':
    main()
