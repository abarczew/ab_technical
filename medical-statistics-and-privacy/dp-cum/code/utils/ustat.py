from typing import Callable
import abc
import numpy as np


class UStat(abc.ABC):
    @abc.abstractmethod
    def get_instances(self, *args: str) -> np.array:
        """
        Abstract class to be implemented by parties.

        :param string args:
            Instances names as defined by phi.

        :return: np.array
            The data over which we compute ustat.
        """
        pass

    def get_instances_nb(self, *args: str) -> int:
        """Get the number of instances held by all parties.

        :param string args:
            Arguments to get instances.

        :return: int
            The number of instances.
        """
        x = self.get_instances(*args)
        return x.size

    def ustat(self,
              phi: Callable[[np.array], np.array] = lambda x: x,
              **kwargs: str) -> float:
        """Computes u-statistic;

        :param Callable phi:
            Function applied on data over with we compute the U-statistic.
        :param string kwargs:
            Arguments to get instances.

        :return: float ustat:
            The u-statistic.
        """
        var_names = phi.__code__.co_varnames
        x = {}
        for var in var_names:
            x[var] = self.get_instances(var)
        # we suppose instances arrays have same lengths
        n = self.get_instances_nb(var_names[0])
        ustat = phi(**x).sum() / n
        return ustat


# From file roc_experiment.py
class UStat_smooth_ROC(UStat):
#class UStatRocSmoothTest(UStat):
    """docstring for UStatTest."""

    def __init__(self, y_test, y_pred):
        self.y_true = y_test
        self.y_score = y_pred

    def get_instances(self, args):
        return getattr(self, args)


# From file hltest
# class UStat_hosmer_lemeshow(UStat)
def ustat_hl(p: np.array,
          y_pred: np.array,
          lower,
          upper):
    """
    ustat ?

    :param p: ?
    :param y_pred: ?
    :param lower: ?
    :param upper: ?

    :return: ?
    """
    mask = np.logical_and((lower <= y_pred), (y_pred < upper))
    return np.sum(p[mask]) / len(p)


# From file hltest
def ustat_hl_search(a: float,
                 b: float,
                 t: float,
                 psi: float,
                 p: np.array,
                 ustat_hl: Callable[[np.array, float], float]) -> float:
    """
    Performs a binary search with ustat_hl.

    :param float a: Lower bound of the research interval.
    :param float b: Upper bound of the research interval.
    :param float t: Target.
    :param float psi: Precision.
    :param np.array p: Array of probabilities on which the ustat_hl is applied.
    :param function ustat_hl: U-statistic.

    :return: The searched reversed value of the u-statistic.
    """
    if b < a:
        raise ValueError(
            "Lower bound is supposed to be lower than the upper bound")

    indicator = np.ones_like(p)
    while b - a > psi:
        m = (a + b) / 2
        f_m = ustat_hl(indicator, p, 0, m)
        if f_m < t:
            a = m
        else:
            b = m
    return (a + b) / 2


# From file hltest
def ustat_hl_search_noise(a: float,
                       b: float,
                       t: float,
                       psi: float,
                       p: np.array,
                       ustat_hl: Callable[[np.array, float], float],
                       epsilon: float,
                       noise_dict: dict,
                       n_groups: int = 10) -> float:
    """
    Performs a binary search with ustat_hl.

    :param float a: Lower bound of the research interval.
    :param float b: Upper bound of the research interval.
    :param float t: Target.
    :param float psi: Precision.
    :param np.array p: Array of probabilities on which the ustat_hl is applied.
    :param function ustat_hl: U-statistic
    :param dict noise_dict: ?
    :param epsilon: The privacy budget.
    :param int n_groups: optional, The number of groups to create.
        The default value is 10

    :returns The searched reversed value of the u-statistic.
    """

    L = int(np.ceil(np.log2(len(p))))
    # Not in use?
    laplace_scale = (np.ceil(np.log2(1/psi))+1+4*n_groups)/(len(p)*epsilon)
    if b < a:
        raise ValueError("Lower bound is supposed to "
                         "be lower than the upper bound")
    indicator = np.ones_like(p)
    i = t*n_groups
    select_noise = [(int(np.ceil(pow(i/2, j))), j) for j in range(1, L+1)]
    sum_noise = sum(value for key,value in noise_dict.items()
                    if key in select_noise)
    while b - a > psi:
        m = (a + b) / 2
        f_m = ustat_hl(indicator, p, 0, m) + sum_noise
        if f_m < t:
            a = m
        else:
            b = m
    return (a + b) / 2