from collections import namedtuple
import numpy as np
import scipy
from typing import Callable
from utils.smooth import *
from utils.hltest import *
from utils.dp_counter import generate_noise
rng = np.random.RandomState()


def HL_smoothing(y_pred,
                 pre_psi,
                 n_groups=10,
                 epsilon=0.1):
    """
    This function takes an array and gives the smoothed count
    Dense version, takes space O(N) rather than O(n)

    :param y_pred: array
        Predicted probabilities, floats on [0, 1].
    :param pre_psi: float
        Precision.
    :param n_groups: int, optional
        The number of groups to create. The default value is 10, which
        corresponds to deciles of predicted probabilities.
    :param epsilon: The privacy budget.

    :return:
    """
    # Prepare parameters:
    n = len(y_pred)
    max_y = np.max(y_pred)
    min_y = np.min(y_pred)
    if max_y > 1:
        raise "max(y_pred) too high"
    if min_y < 0:
        raise "min(y_pred) too low"
    N = np.int(np.ceil(1/pre_psi)+1)
    psi = 1.0 / (N-1)  # so that psi*(N-1) = 1
    rvidx = rv_index(N)
    print("N=", N, ", n=", n, ", psi=", psi)

    # prepare noise :
    dp_budget_parts = rvidx.L + 8
    count_laplace_scale = dp_budget_parts / epsilon  # noise needed on a count
    ustat_laplace_scale = count_laplace_scale / n   # noise needed on a U-Stat
    (eta, sumeta) = generate_noise(rng, count_laplace_scale, rvidx)

    # prepare ecdf :
    y_pred_ecdf = np.zeros(N)
    for k in range(n):  # first put the EDF in y_pred_ecdf
        y_pred_k = np.int(np.round(y_pred[k]*(N-1)))  # after rounding in [0,N-1]
        y_pred_ecdf[y_pred_k] = y_pred_ecdf[y_pred_k] + 1
    for i in range(1, N):  # now make it cumulative
        y_pred_ecdf[i] = y_pred_ecdf[i] + y_pred_ecdf[i-1]

    # add DP + smooth
    y_pred_ecdf_dp = y_pred_ecdf + sumeta
    y_pred_ecdf = y_pred_ecdf / n        # from counter to rate
    y_pred_ecdf_dp = y_pred_ecdf_dp / n  # from counter to rate
    (y_pred_ecdf_sm, _) = smooth_dense(y_pred_ecdf_dp, rvidx, 1)

    return y_pred_ecdf_sm


def HL_smoothing_0124_18(y_pred,
                         psi,
                         n_groups=10,
                         epsilon=0.1):
    """
    This function takes an array and gives the smoothed count

        :param y_pred: array
        Predicted probabilities, floats on [0, 1].
    :param pre_psi: float
        Precision.
    :param n_groups: int, optional
        The number of groups to create. The default value is 10, which
        corresponds to deciles of predicted probabilities.
    :param epsilon: The privacy budget.

    :return:
    """
    n = len(y_pred)
    L = int(np.ceil(np.log2(n)))
    laplace_scale = (np.ceil(np.log2(1/psi))+1+4*n_groups)/(1/psi*epsilon)
    noise_dict = {}
    noise_values = rng.laplace(scale=laplace_scale, size=n*L)
    k = 0  # noise count
    for i in range(1, n+1):
        for j in range(1, L+1):
            if noise_dict.get((int(np.ceil(i/2**j)), j)) is None:
                noise_dict[(int(np.ceil(i/2**j)), j)] = noise_values[k]
                k = k+1

    indicator = np.ones_like(y_pred)

    f_M = np.empty(shape=(int(np.ceil(1/psi)), ))  # Contains the noisy counter
    k2 = 0
    for u in range(int(np.ceil(1/psi))):
        k2 = k2+psi
        select_noise = [(int(np.ceil(pow(u/2, j))), j) for j in range(1, L+1)]
        sum_noise = sum(value for key, value in noise_dict.items()
                        if key in select_noise)

        f_M[u] = ustat(indicator, y_pred, 0, k2) + sum_noise
    rvidx = rv_index(len(y_pred))
    (F_M_smoothed, eta_corr) = smooth_dense(f_M, rvidx)
    return F_M_smoothed


def ustat_search_smooth(a: float,
                        b: float,
                        t: float,
                        psi: float,
                        f_M_smooth: np.array,
                        ustat: Callable[[np.array, float], float],
                        n_groups: int = 10) -> float:
    """
    Performs a binary search with ustat.

    :param float a: Lower bound of the research interval.
    :param float b: Upper bound of the research interval.
    :param float t: Target.
    :param float psi: Precision.
    :param np.array f_M_smooth:
    :param function ustat: U-statistic
    :param int n_groups: optional, The number of groups to create.
        The default value is 10

    :returns The searched reversed value of the u-statistic.
    """

    if b < a:
        raise ValueError(
            "Lower bound is supposed to be lower than the upper bound")
    while b - a > psi:
        m = (a + b) / 2
        f_m = f_M_smooth[int(np.ceil(m))]
        if f_m < t:
            a = m
        else:
            b = m
    return (a + b) / 2


def hosmer_lemeshow_test_smooth(y_true: np.array,
                                y_pred: np.array,
                                psi: float,
                                ustat: Callable[[np.array, float], float],
                                epsilon: float,
                                n_groups: int = 10,
                                details: bool = False) -> namedtuple:
    """
    Performs a Hosmer–Lemeshow test.

    :param np.array y_true: Observed labels, either 0 or 1.
    :param np.array y_pred: Predicted probabilities, floats on [0, 1].
    :param float psi: Precision.
    :param function ustat: U-statistic.
    :param epsilon: The privacy budget.
    :param int n_groups: optional The number of groups to create.
        The default value is 10, which
        corresponds to deciles of predicted probabilities.
    :param bool details: optional, Add quantiles information to the saved file

    :return:
    """
    n = len(y_true)
    L = int(np.ceil(np.log2(n)))
    t = np.empty(shape=(n_groups+1, ))
    if details == True:
        E_0_byGroup = np.empty(shape=(n_groups, ))
        E_1_byGroup = np.empty(shape=(n_groups, ))
        O_0_byGroup = np.empty(shape=(n_groups, ))
        O_1_byGroup = np.empty(shape=(n_groups, ))

    t[0] = 0
    hlstat = 0
    hlstat_clipping = 0

    f_M_smooth = HL_smoothing(y_pred, psi, n_groups, epsilon)
    laplace_scale = (np.ceil(np.log2(1/psi))+1+4*n_groups) / \
        (len(y_true)*epsilon)

    noise_values_hlstat = rng.laplace(
        scale=laplace_scale, size=((n_groups, 4)))
    for i in np.arange(1, n_groups+1):
        if i < n_groups:
            t[i] = ustat_search_smooth(
                t[i-1], 1, i/n_groups, psi, f_M_smooth, ustat)
        else:
            t[i] = 1 + psi

        E_0 = n * ustat(1-y_pred, y_pred,
                        t[i-1], t[i]) + noise_values_hlstat[i-1, 0]
        O_0 = n * ustat(1-y_true, y_pred,
                        t[i-1], t[i]) + noise_values_hlstat[i-1, 1]
        E_1 = n * ustat(y_pred, y_pred, t[i-1],
                        t[i]) + noise_values_hlstat[i-1, 2]
        O_1 = n * ustat(y_true, y_pred, t[i-1],
                        t[i]) + noise_values_hlstat[i-1, 3]
        if details == True:
            E_0_byGroup[i-1] = E_0
            O_0_byGroup[i-1] = O_0
            E_1_byGroup[i-1] = E_1
            O_1_byGroup[i-1] = O_1
        hlstat += ((O_0 - E_0)**2) / E_0 + ((O_1 - E_1)**2) / E_1
    df = n_groups - 2
    p = scipy.stats.chi2.sf(hlstat, df)

    if details == True:
        TestResult = namedtuple('HosmerLemeshowTest', ('hlstat', 'df', 'p',
                                                       'E_0_byGroup', 'O_0_byGroup', 'E_1_byGroup', 'O_1_byGroup'))
        return TestResult(hlstat, df, p, E_0_byGroup, O_0_byGroup, E_1_byGroup, O_1_byGroup)
    else:
        TestResult = namedtuple(
            'HosmerLemeshowTest', ('hlstat', 'df', 'p'))
        return TestResult(hlstat, df, p)


if __name__ == '__main__':
    from sklearn.datasets import load_breast_cancer
    from sklearn.linear_model import LogisticRegression
    X, y = load_breast_cancer(return_X_y=True)
    clf = LogisticRegression(random_state=0).fit(X, y)
    y_pred = clf.predict_proba(X)[:, 1]

    print(hosmer_lemeshow_test(y, y_pred))
    print(hosmer_lemeshow_test_w_search(y, y_pred, 0.0001, ustat))
    print(hosmer_lemeshow_test_w_search_noise(y, y_pred, 0.0001, ustat, 0.1))
