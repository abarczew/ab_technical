import numpy as np
from utils.ustat import UStat, UStat_smooth_ROC
from typing import Tuple
from utils.smooth import rv_index, smooth_dense
from utils.dp_counter import generate_noise


def roc_curve(y_true: np.array,
              y_score: np.array,
              pos_label=None) -> Tuple:
    """
    Calculate true and false positives per binary classification threshold.

    :param y_true : ndarray of shape (n_samples,)
        True targets of binary classification.
    :param y_score : ndarray of shape (n_samples,)
        Estimated probabilities or output of a decision function.
    :param pos_label : int or str, default=None
        The label of the positive class.

    :return:
        - np.ndarray fpr: of shape (>2,)
    Increasing false positive rates such that element i is the false
    positive rate of predictions with score >= `thresholds[i]`.
        - np.ndarray tpr: of shape (>2,)
    Increasing true positive rates such that element `i` is the true
    positive rate of predictions with score >= `thresholds[i]`.
    """

    # make y_true a boolean vector
    if pos_label:
        y_true = y_true == pos_label

    # sort scores and corresponding truth values
    desc_score_indices = np.argsort(y_score, kind="mergesort")[::-1]
    y_score = y_score[desc_score_indices]
    y_true = y_true[desc_score_indices]

    # y_score typically has many tied values. Here we extract
    # the indices associated with the distinct values. We also
    # concatenate a value for the end of the curve.
    distinct_value_indices = np.where(np.diff(y_score))[0]
    threshold_idxs = np.r_[distinct_value_indices, y_true.size - 1]
    thresholds = y_score[threshold_idxs]

    # accumulate the true positives with decreasing threshold
    tpr = np.array(list(map(lambda x: (y_score >= x)[y_true == 1].sum()
                            / y_true.sum(), thresholds)))
    fpr = np.array(list(map(lambda x: (y_score >= x)[y_true != 1].sum()
                            / (1 - y_true).sum(), thresholds)))

    # Add an extra threshold position
    # to make sure that the curve starts at (0, 0)
    tpr = np.r_[0, tpr, 1]
    fpr = np.r_[0, fpr, 1]

    return fpr, tpr


def roc_curve_ustat(stat: UStat,
                    pos_label: str = None,
                    epsilon: float = None,
                    smooth: bool = None,
                    norm: int = None,
                    seed: int = 42) -> Tuple:
    """
    Calculate true and false positives per binary classification threshold.

    :param stat: Ustat instance
        Object to get data and call ustat.
    :param pos_label: int or str, default=None
        The label of the positive class.
    :param epsilon: float, default=None
        DP privacy budget if needed.
    :param smooth: bool, default=None
        qp smoothen strategy to avoid non increasing curve.
    :param norm: int, default=None
        norm (1 or 2) of the above qp.
    :param seed: int, default=42
        random seed.

    :return:
        - np.ndarray fpr: of shape (>2,)
    Increasing false positive rates such that element i is the false
    positive rate of predictions with score >= `thresholds[i]`.
        - np.ndarray tpr: of shape (>2,)
    Increasing true positive rates such that element `i` is the true
    positive rate of predictions with score >= `thresholds[i]`.
    """
    if not pos_label:
        pos_label = 1
    n = stat.get_instances_nb('y_true')
    tpr = []
    fpr = []
    # needs to be revamped to fit fss framework
    tpr_denominator = n * stat.ustat(lambda y_true: (y_true == pos_label))
    fpr_denominator = n * stat.ustat(lambda y_true: (y_true != pos_label))
    # dec_order = round(np.log10(n) / 2)
    dec_order = 2
    thresholds = np.unique(list(map(lambda x: round(x, dec_order), stat.y_score)))
    for threshold in thresholds:
        # ecdf on y_score==pos_label
        tpr.append(n / tpr_denominator * stat.ustat(lambda y_true, y_score:
                                                    (y_score >= threshold)
                                                    [(y_true == pos_label)]))
        fpr.append(n / fpr_denominator * stat.ustat(lambda y_true, y_score:
                                                    (y_score >= threshold)
                                                    [(y_true != pos_label)]))

    tpr = np.sort(tpr)
    fpr = np.sort(fpr)
    # Add an extra threshold position
    # to make sure that the curve starts at (0, 0)
    tpr = np.r_[0, tpr, 1]
    fpr = np.r_[0, fpr, 1]

    if epsilon is not None:
        N = len(tpr)
        rvidx = rv_index(N)
        scale = epsilon / rvidx.L
        rng = np.random.default_rng(seed)
        _, sumeta_tpr = generate_noise(rng, scale, rvidx)
        _, sumeta_fpr = generate_noise(rng, scale, rvidx)
        tpr = tpr + sumeta_tpr / tpr_denominator
        fpr = fpr + sumeta_fpr / fpr_denominator
        if smooth or norm:
            tpr, _ = smooth_dense(tpr, rvidx, norm, 0, 1)
            fpr, _ = smooth_dense(fpr, rvidx, norm, 0, 1)

    return fpr, tpr


def auc(x: np.array,
        y: np.array):
    """
    Compute Area Under the Curve (AUC) using the trapezoidal rule.

    :param x : ndarray of shape (n,)
        x coordinates. These must be either monotonic increasing or monotonic
        decreasing.
    :param y : ndarray of shape, (n,)
        y coordinates.

    :return: float auc:
    """
    area = np.trapz(y, x)
    return area


def plot_roc_curve(y_true,
                   y_score,
                   pos_label):
    """
    Plot Receiver operating characteristic (ROC) curve.
    Extra keyword arguments will be passed to matplotlib's `plot`.
    Read more in the :ref:`User Guide <visualizations>`.

    :param y_true : ndarray of shape (n_samples,)
        True targets of binary classification.
    :param y_score : ndarray of shape (n_samples,)
        Estimated probabilities or output of a decision function.
    :param pos_label : int or str, default=None
        The label of the positive class.

    :return: :class:`~sklearn.metrics.RocCurveDisplay` display:
        Object that stores computed values.
    """
    fpr, tpr = roc_curve(y_true,
                         y_score,
                         pos_label=pos_label)
    roc_auc = auc(fpr,
                  tpr)

    import matplotlib.pyplot as plt
    plt.style.use('ggplot')

    fig, ax = plt.subplots()

    label = f"(AUC = {roc_auc:0.2f})"

    info_pos_label = (f" (Positive label: {pos_label})")

    xlabel = "False Positive Rate" + info_pos_label
    ylabel = "True Positive Rate" + info_pos_label
    ax.set(xlabel=xlabel, ylabel=ylabel)

    ax.legend(loc="lower right")

    ax.plot(fpr, tpr, label=label)
    plt.show()


def plot_roc_curve_ustat(stat,
                         pos_label=None,
                         epsilon=None,
                         smooth=None,
                         norm=None):
    """
    Plot Receiver operating characteristic (ROC) curve.
    Extra keyword arguments will be passed to matplotlib's `plot`.
    Read more in the :ref:`User Guide <visualizations>`.

    :param stat: Ustat instance
        Object to get data and call ustat.
    :param pos_label: int or str, default=None
        The label of the positive class.
    :param epsilon: float, default=None
        DP privacy budget if needed.
    :param smooth: bool, default=None
        qp smoothen strategy to avoid non increasing curve.
    :param norm: int, default=None
        norm (1 or 2) of the above qp.

    :return: :class:`~sklearn.metrics.RocCurveDisplay` display:
        Object that stores computed values.
    """
    fpr, tpr = roc_curve_ustat(stat,
                               pos_label,
                               epsilon,
                               smooth,
                               norm)
    roc_auc = auc(fpr,
                  tpr)

    import matplotlib.pyplot as plt
    plt.style.use('ggplot')

    fig, ax = plt.subplots()

    label = f"(AUC = {roc_auc:0.2f})"

    info_pos_label = (f" (Positive label: {pos_label})")

    xlabel = "False Positive Rate" + info_pos_label
    ylabel = "True Positive Rate" + info_pos_label
    ax.set(xlabel=xlabel, ylabel=ylabel)

    ax.legend(loc="lower right")

    ax.plot(fpr, tpr, label=label)
    plt.show()


def geo_score(x_f,
              y_f,
              x_g,
              y_g):
    """
    geo_score: ?
    :param x_f: array
        x coordinates of the f line to draw
    :param y_f: array
        y coordinates of the f line to draw
    :param x_g: array
        x coordinates of the g line to draw, g has same length as f
    :param y_g: array
        y coordinates of the g line to draw, g has same length as f

    :return float geo_score:
        sum of areas between f and g
    """
    idx = np.argwhere(np.diff(np.sign(y_f - y_g))).flatten()
    # make sure we get the last area
    idx = np.r_[idx, len(x_f)-1]
    # compute the difference of areas between each interception
    geo_score = 0
    for i, j in list(zip(idx[:-1], idx[1:])):
        area_f = np.trapz(y_f[i:j+1], x_f[i:j+1])
        area_g = np.trapz(y_g[i:j+1], x_g[i:j+1])
        geo_score += abs(area_f - area_g)
    return geo_score
