from collections import namedtuple
import numpy as np
import scipy
from typing import Callable
from numpy.random import laplace
import pandas as pd
import random
from utils.smooth import *
from utils.dp_counter import generate_noise
from utils.ustat import ustat_hl, ustat_hl_search, ustat_hl_search_noise


def hosmer_lemeshow_test(y_true: np.array,
                         y_pred: np.array,
                         n_groups: int = 10,
                         details: bool = False) -> namedtuple:
    """
    Performs a Hosmer–Lemeshow test.

    :param array y_true: Observed labels, either 0 or 1.
    :param array y_pred: Predicted probabilities, floats on [0, 1].
    :param int n_groups: optional, The number of groups to create.
        The default value is 10, which
        corresponds to deciles of predicted probabilities.
    :param bool details: optional, Add quantiles information to the saved file

    :return:
        - float hlstat: The test statistic.
        - int df: The degrees of freedom of the test.
        - float p: The p-value of the test.
    """
    a = np.stack((y_true, y_pred), axis=-1)
    a = a[a[:, 1].argsort()]
    #hltable = np.array_split(a, n_groups)
    cut, bins = pd.qcut(a[:, 1],
                        n_groups,
                        retbins=True)
    hltable = []
    for i in range(n_groups):
        if (i < n_groups-1):
            hltable.append(a[np.logical_and(a[:, 1] >= bins[i],
                                            a[:, 1] < bins[i+1])])
        else:
            hltable.append(
                a[np.logical_and(a[:, 1] >= bins[i],
                                 a[:, 1] <= bins[i+1])])

    # Need to extract func. from inside the first func.
    def hlt_sub(y_true: np.array,
                y_pred: np.array) -> float:
        diff_success = (y_true.sum() - y_pred.sum())**2 / y_pred.sum()
        diff_fail = ((1 - y_true).sum() - (1 - y_pred).sum())**2 / (1 - y_pred).sum()
        return diff_success + diff_fail

    hlstat = np.sum(list(map(lambda x: hlt_sub(x[:, 0], x[:, 1]), hltable)))
    df = n_groups - 2
    p = scipy.stats.chi2.sf(hlstat,
                            df)

    if details:
        O_1 = np.array([])
        E_1 = np.array([])
        for i in range(n_groups):
            O_1 = np.append(O_1, np.sum(hltable[i][:, 0]))
            E_1 = np.append(E_1, np.sum(hltable[i][:, 1]))

        TestResult = namedtuple('HosmerLemeshowTest',
                                ('hlstat', 'df', 'p', 'E_1', 'O_1'))
        return TestResult(hlstat,
                          df,
                          p,
                          E_1,
                          O_1)
    else:
        TestResult = namedtuple('HosmerLemeshowTest',
                                ('hlstat', 'df', 'p'))
        return TestResult(hlstat,
                          df,
                          p)


def hosmer_lemeshow_test_w_search(y_true: np.array,
                                  y_pred: np.array,
                                  psi: float,
                                  ustat_hl: Callable[[np.array, float], float],
                                  n_groups: int = 10,
                                  details=False) -> namedtuple:
    """
    Performs a Hosmer–Lemeshow test.

    :param np.array y_true: Observed labels, either 0 or 1.
    :param np.array y_pred: Predicted probabilities, floats on [0, 1].
    :param float psi: Precision.
    :param function ustat_hl: U-statistic.
    :param int n_groups: optional The number of groups to create.
        The default value is 10, which
        corresponds to deciles of predicted probabilities.
    :param bool details: optional, Add quantiles information to the saved file

    :return:
    """
    t = np.empty(shape=(n_groups+1, ))
    if details:
        E_0_byGroup = np.empty(shape=(n_groups, ))
        E_1_byGroup = np.empty(shape=(n_groups, ))
        O_0_byGroup = np.empty(shape=(n_groups, ))
        O_1_byGroup = np.empty(shape=(n_groups, ))
    t[0] = 0
    hlstat = 0
    n = len(y_true)
    for i in np.arange(1, n_groups+1):
        if i < n_groups:
            t[i] = ustat_hl_search(t[i-1], 1, i/n_groups, psi, y_pred, ustat_hl)
        else:
            t[i] = 1 + psi
        E_0 = n * ustat_hl(1-y_pred, y_pred, t[i-1], t[i])
        O_0 = n * ustat_hl(1-y_true, y_pred, t[i-1], t[i])
        E_1 = n * ustat_hl(y_pred, y_pred, t[i-1], t[i])
        O_1 = n * ustat_hl(y_true, y_pred, t[i-1], t[i])
        hlstat += ((O_0 - E_0)**2) / E_0 + ((O_1 - E_1)**2) / E_1
        if details:
            E_0_byGroup[i-1] = E_0
            O_0_byGroup[i-1] = O_0
            E_1_byGroup[i-1] = E_1
            O_1_byGroup[i-1] = O_1

    df = n_groups - 2
    p = scipy.stats.chi2.sf(hlstat, df)

    if details:
        TestResult = namedtuple('HosmerLemeshowTest', ('hlstat', 'df', 'p',
                                'E_0_byGroup', 'O_0_byGroup', 'E_1_byGroup', 'O_1_byGroup'))
        return TestResult(hlstat, df, p, E_0_byGroup, O_0_byGroup, E_1_byGroup, O_1_byGroup)
    else:
        TestResult = namedtuple('HosmerLemeshowTest', ('hlstat', 'df', 'p'))
        return TestResult(hlstat, df, p)


class y_pred_ecdf:
    def __init__(self, y_pred, N):
        y_sort = np.array(sorted(y_pred))
        self.y_pred = np.array(np.round(y_sort*(N-1)), 'i')
        self.n = len(self.y_pred)

    def inv_eval(self, x, low=0, high=None):
        # t in range(n)
        # [low, high] can specify the sub-interval of [0,N]
        #             in which the answer is known to lie
        # return ecdf^{-1}(t) in range(N)
        return self.y_pred[x]

    def eval(self, t, low=0, high=None):
        # t in range(N)
        # [low, high] can specify the sub-interval of [0,N]
        #             in which the answer is known to lie
        # return ecdf(t) in range(n)
        if high is None:
            high = self.n
        while low < high:
            mid = (low + high)//2
            if self.y_pred[mid] <= t:
                low = mid+1
            else:
                high = mid
        return low


class y_pred_ecdf_dpsm:
    def inv_eval(self, x, low=0, high=None):
        # t in range(n)
        # [low, high] can specify the subinterval of [0,N]
        #             in which the answer is known to lie
        # return ecdf^{-1}(t) in range(N)
        if high is None:
            high = self.N
        while low < high:
            mid = (low + high)//2
            if self.eval(mid) <= x:
                low = mid+1
            else:
                high = mid
        return low


class y_pred_ecdf_dp(y_pred_ecdf_dpsm):
    def __init__(self, y_pred, L, random_gen, lapscale, N):
        y_sort = np.array(sorted(y_pred))
        self.y_pred = np.array(np.round(y_sort*(N-1)), 'i')
        self.L = L
        self.n = len(y_pred)
        self.N = N
        self.noise_dict = dict()
        self.random_gen = random_gen
        self.laplace_scale = lapscale

    def eval(self, t, low=0, high=None):
        # t in range(N)
        # [low, high] can specify the sub-interval of [0,N]
        #             in which the answer is known to lie
        # return ecdf(t) in range(n)
        if high is None:
            high = self.n
        while low < high:
            mid = (low + high)//2
            if self.y_pred[mid] <= t:
                low = mid+1
            else:
                high = mid
        val = low
        for l in range(self.L):
            key = (low // (2**l), l)
            if not(key in self.noise_dict):
                eta_il = self.random_gen.laplace(0, self.laplace_scale)
                self.noise_dict[key] = eta_il
            val = val + self.noise_dict[key]
        # print("eval(",t,")=",val)
        return val


class y_pred_ecdf_sm(y_pred_ecdf_dpsm):
    def __init__(self, y_ecdf_sm, n):
        self.y_ecdf_sm = y_ecdf_sm
        self.N = len(y_ecdf_sm)
        self.n = n

    def eval(self, t, low=0, high=None):
        # print("t=",t)
        # print(self.y_ecdf_sm)
        return self.y_ecdf_sm[t]


def hosmer_lemeshow_test_w_search_thresholds_1(n_groups: int,
                                               ecdf,
                                               t,
                                               low: int,
                                               high: int):
    """
    :param int n_groups: optional The number of groups to create.
        The default value is 10, which
        corresponds to deciles of predicted probabilities.
    :param ecdf
    :param float t:
    :param int low:
    :param int high:

    :return: t
    """
    if high-low < 2:
        for q in range(low, high):
            t[q] = ecdf.inv_eval(np.int(np.round(q*ecdf.n/n_groups))-1, t[low-1], t[high])
    else:
        mid = (low+high)//2
        t[mid] = ecdf.inv_eval(np.int(np.round(mid*ecdf.n/n_groups))-1, t[low-1], t[high])
        hosmer_lemeshow_test_w_search_thresholds_1(n_groups, ecdf, t, low, mid)
        hosmer_lemeshow_test_w_search_thresholds_1(n_groups, ecdf, t, mid+1, high)
    return t


def hosmer_lemeshow_test_w_search_thresholds(n_groups: int,
                                             N: int,
                                             ecdf):
    """
    :param int n_groups: optional The number of groups to create.
        The default value is 10, which
        corresponds to deciles of predicted probabilities.
    :param int N:
    :param ecdf

    :return:
    """
    t = np.empty(shape=n_groups+1, dtype='i')
    t[0] = 0
    t[n_groups] = N
    return hosmer_lemeshow_test_w_search_thresholds_1(n_groups, ecdf, t, 1, n_groups)


def hosmer_lemeshow_test_w_search_sens(y_true: np.array,
                                       y_pred: np.array,
                                       pre_psi: float,
                                       ustat_hl: Callable[[np.array, float], float],
                                       n_groups: int = 10,
                                       details: bool = False) -> namedtuple:
    """
    Performs a Hosmer–Lemeshow test.

    :param np.array y_true: Observed labels, either 0 or 1.
    :param np.array y_pred: Predicted probabilities, floats on [0, 1].
    :param float pre_psi: Precision.
    :param function ustat_hl: U-statistic.
    :param int n_groups: optional, The number of groups to create.
        The default value is 10, which
        corresponds to deciles of predicted probabilities.
    :param bool details: optional, Add quantiles information to the saved file

    :return:
        - float hlstat: The test statistic.
        - int df: The degrees of freedom of the test.
        - float p: The p-value of the test
        - float epsilon: The privacy budget
    """
    n = len(y_true)
    N = np.int(np.ceil(1/pre_psi)+1)
    psi = 1.0 / (N-1)

    ecdf = y_pred_ecdf(y_pred, N)

    t = hosmer_lemeshow_test_w_search_thresholds(n_groups, N, ecdf)
    # print("t=",t)

    hl = hosmer_lemeshow_test_expobs(n_groups, None, None, y_pred, y_true, n, t/(N-1), details)

    return hl


def repeat_hosmer_lemeshow_test(reps,
                                random_gen,
                                y_true: np.array,
                                y_pred: np.array,
                                pre_psi: float,
                                ustat_hl: Callable[[np.array, float], float],
                                epsilon: float,
                                n_groups: int = 10,
                                details: bool = False):
    """
    :param int reps:
    :param random_gen: the random number generator
    :param np.array y_true:
    :param np.array y_pred:
    :param float pre_psi: Precision.
    :param function ustat_hl:
    :param float epsilon:
    :param int n_groups: optional, The number of groups to create.
        The default value is 10, which
        corresponds to deciles of predicted probabilities.
    :param bool details: optional, Add quantiles information to the saved file

    :return:
        - mse_dp
        - mse_sm
        - res
    """
    print(f'Calling hosmer_lemeshow_test_w_search_sens')
    sens_res = hosmer_lemeshow_test_w_search_sens(y_true, y_pred, pre_psi, ustat_hl, n_groups, details)
    mse_dp = 0.0
    mse_sm = 0.0
    res = []
    for i in range(reps):
        print(f'Calling hosmer_lemeshow_test_w_search_noise for rep_i={i}')
        res_dp = hosmer_lemeshow_test_w_search_noise(random_gen,
                                                     y_true,
                                                     y_pred,
                                                     pre_psi,
                                                     ustat_hl,
                                                     epsilon,
                                                     n_groups,
                                                     details)
        err_dp = res_dp.hlstat - sens_res.hlstat
        mse_dp = mse_dp + err_dp**2
        print(f'Calling hosmer_lemeshow_test_w_search_sm for rep_i={i}')
        res_sm = hosmer_lemeshow_test_w_search_sm(random_gen,
                                                  y_true,
                                                  y_pred,
                                                  pre_psi,
                                                  ustat_hl,
                                                  epsilon,
                                                  n_groups,
                                                  details)
        err_sm = res_sm.hlstat - sens_res.hlstat
        mse_sm = mse_sm + err_sm**2
        res.append({'rep': i, 'epsilon': epsilon, 'mse_dp': mse_dp, 'mse_sm': mse_sm})
    mse_dp = mse_dp / reps
    mse_sm = mse_sm / reps
    return mse_dp, mse_sm, res


def hosmer_lemeshow_test_w_search_noise(random_gen,
                                        y_true: np.array,
                                        y_pred: np.array,
                                        pre_psi: float,
                                        ustat_hl: Callable[[np.array, float], float],
                                        epsilon: float,
                                        n_groups: int = 10,
                                        details: bool = False) -> namedtuple:
    """
    Performs a Hosmer–Lemeshow test.

    :param random_gen: random number generator to be used
    :param y_true: array
        Observed labels, either 0 or 1.
    :param y_pred: array
        Predicted probabilities, floats on [0, 1].
    :param pre_psi: float
        Precision.
    :param ustat_hl: function
        U-statistic.
    :param n_groups: int, optional
        The number of groups to create. The default value is 10, which
        corresponds to deciles of predicted probabilities.
    :param bool details: optional, Add quantiles information to the saved file

    :return:
        - float hlstat: The test statistic.
        - int df: The degrees of freedom of the test.
        - float p: The p-value of the test
        - float epsilon: The privacy budget
    """
    n = len(y_true)
    N = np.int(np.ceil(1/pre_psi)+1)
    psi = 1.0 / (N-1)
    L = int(np.ceil(np.log2(N)))
    count_laplace_scale = ((L+1)+8)/epsilon
    # print("count_laplace_scale=",count_laplace_scale)

    ecdf_dp = y_pred_ecdf_dp(y_pred,
                             L+1,
                             random_gen,
                             count_laplace_scale,
                             N)

    # for i in range(n) :
    #    print("inv_eval(",i,")=",ecdf_dp.inv_eval(i)," vs ",ecdf.inv_eval(i))

    t = hosmer_lemeshow_test_w_search_thresholds(n_groups,
                                                 N,
                                                 ecdf_dp)
    # print("t=",t)

    hl_dp = hosmer_lemeshow_test_expobs(n_groups,
                                        random_gen,
                                        count_laplace_scale,
                                        y_pred,
                                        y_true,
                                        n,
                                        t/(N-1),
                                        details)
    return hl_dp


def hosmer_lemeshow_test_w_search_sm(random_gen,
                                     y_true: np.array,
                                     y_pred: np.array,
                                     pre_psi: float,
                                     ustat_hl: Callable[[np.array, float], float],
                                     epsilon: float,
                                     n_groups: int = 10,
                                     details: bool = False) -> namedtuple:
    """Performs a Hosmer–Lemeshow test.

    :pram random_gen: random number generator to be used
    :param np.array y_true: Observed labels, either 0 or 1.
    :param np.array y_pred: Predicted probabilities, floats on [0, 1].
    :param float pre_psi: Precision.
    :param function ustat_hl: U-statistic.
    :param epsilon: The privacy budget.
    :param int n_groups: optional, The number of groups to create.
        The default value is 10, which
        corresponds to deciles of predicted probabilities.
    :param bool details: optional, Add quantiles information to the saved file

    :return:
        - float hlstat: The test statistic.
        - int df: The degrees of freedom of the test.
        - float p: The p-value of the test
        - float epsilon: The privacy budget
    """
    n = len(y_true)
    N = np.int(np.ceil(1/pre_psi)+1)
    psi = 1.0 / (N-1)
    L = int(np.ceil(np.log2(N)))
    count_laplace_scale = ((L+1)+8)/epsilon
    # print("count_laplace_scale=",count_laplace_scale)

    # print("N=",N)
    rvidx = rv_index(N)
    # prepare noise :
    ustat_hl_laplace_scale = count_laplace_scale / n   # noise needed on a U-Stat
    (eta, sumeta) = generate_noise(random_gen, 1/count_laplace_scale, rvidx)

    ecdf_dc = np.zeros(N)  # dense cumulative
    for k in range(n):  # first put the EDF in ecdf_dc
        y_pred_k = np.int(np.round(y_pred[k]*(N-1)))  # after rounding in [0,N-1
        ecdf_dc[y_pred_k] = ecdf_dc[y_pred_k] + 1
    for i in range(1, N):  # now make it cumulative
        ecdf_dc[i] = ecdf_dc[i] + ecdf_dc[i-1]
    ecdf_ddp = ecdf_dc + sumeta
    (ecdf_sm_array, _) = smooth_dense(ecdf_ddp, rvidx, 1)

    ecdf_sm = y_pred_ecdf_sm(ecdf_sm_array, n)

    # for i in range(n) :
    #    print("inv_eval(",i,")=",ecdf_dp.inv_eval(i)," vs ",ecdf.inv_eval(i))

    t = hosmer_lemeshow_test_w_search_thresholds(n_groups, N, ecdf_sm)
    # print("sm_t=",t)

    """
    ecdf_dp = y_pred_ecdf_dp(y_pred, L+1, random_gen, count_laplace_scale, N)
    dp_t = hosmer_lemeshow_test_w_search_thresholds(n_groups, N, ecdf_dp)
    print("dp_t=",dp_t)

    sens_ecdf = y_pred_ecdf(y_pred, N)
    sens_t = hosmer_lemeshow_test_w_search_thresholds(n_groups, N, sens_ecdf)
    print("sens_t=",sens_t)

    for i in range(n) :
        print("inv_eval(",i,")=",sens_ecdf.inv_eval(i)," vs ",ecdf_dp.inv_eval(i)," vs ",ecdf_sm.inv_eval(i))
    """

    hl_sm = hosmer_lemeshow_test_expobs(
        n_groups, random_gen, count_laplace_scale, y_pred, y_true, n, t/(N-1), details)
    return hl_sm


def hosmer_lemeshow_test_expobs(n_groups,
                                random_gen,
                                count_laplace_scale,
                                y_pred,
                                y_true,
                                n,
                                t,
                                details):
    """

    :param n_groups: int, optional
        The number of groups to create. The default value is 10, which
        corresponds to deciles of predicted probabilities.
    :param random_gen: random number generator to be used
    :param count_laplace_scale
    :param y_pred: array
        Predicted probabilities, floats on [0, 1].
    :param y_true: array
        Observed labels, either 0 or 1.
    :param n
    :param t
    :param bool details: optional, Add quantiles information to the saved file

    :return:

    """

    hlstat = 0
    # Not in use?
    hlstat_clipping = 0

    if details:
        E_0_byGroup = np.empty(shape=(n_groups, ))
        E_1_byGroup = np.empty(shape=(n_groups, ))
        O_0_byGroup = np.empty(shape=(n_groups, ))
        O_1_byGroup = np.empty(shape=(n_groups, ))

    if count_laplace_scale is None:
        noise_values_hlstat = np.zeros((n_groups, 4))
    else:
        rate_laplace_scale = count_laplace_scale / n
        # print("rate_laplace_scale=",rate_laplace_scale)
        noise_values_hlstat = random_gen.laplace(scale=rate_laplace_scale,
                                          size=(n_groups, 4))
    for i in range(1, n_groups+1):

        E_0 = n * ustat_hl(1-y_pred, y_pred,
                        t[i-1], t[i]) + noise_values_hlstat[i-1, 0]
        O_0 = n * ustat_hl(1-y_true, y_pred,
                        t[i-1], t[i]) + noise_values_hlstat[i-1, 1]
        E_1 = n * ustat_hl(y_pred, y_pred, t[i-1],
                        t[i]) + noise_values_hlstat[i-1, 2]
        O_1 = n * ustat_hl(y_true, y_pred, t[i-1],
                        t[i]) + noise_values_hlstat[i-1, 3]
        if details:
            E_0_byGroup[i-1] = E_0
            O_0_byGroup[i-1] = O_0
            E_1_byGroup[i-1] = E_1
            O_1_byGroup[i-1] = O_1
        hlstat += ((O_0 - E_0)**2) / E_0 + ((O_1 - E_1)**2) / E_1
    df = n_groups - 2
    p = scipy.stats.chi2.sf(hlstat, df)

    if details:
        TestResult = namedtuple('HosmerLemeshowTest', ('hlstat', 'df', 'p',
                                                       'E_0_byGroup', 'O_0_byGroup', 'E_1_byGroup', 'O_1_byGroup'))
        return TestResult(hlstat, df, p, E_0_byGroup, O_0_byGroup, E_1_byGroup, O_1_byGroup)
    else:
        TestResult = namedtuple(
            'HosmerLemeshowTest', ('hlstat', 'df', 'p'))
        return TestResult(hlstat, df, p)


def hosmer_lemeshow_test_w_search_noise_0125_12(y_true: np.array,
                                                y_pred: np.array,
                                                psi: float,
                                                ustat_hl: Callable[[np.array, float], float],
                                                epsilon: float,
                                                n_groups: int = 10,
                                                details: bool = False) -> namedtuple:
    """Performs a Hosmer–Lemeshow test.

    :param y_true: array
        Observed labels, either 0 or 1.
    :param y_pred: array
        Predicted probabilities, floats on [0, 1].
    :param psi: float
        Precision.
    :param ustat_hl: function
        U-statistic.
    :param epsilon: The privacy budget.
    :param n_groups: int, optional
        The number of groups to create. The default value is 10, which
        corresponds to deciles of predicted probabilities.
    :param bool details: optional, Add quantiles information to the saved file

    :return:
        - float hlstat: The test statistic.
        - int df: The degrees of freedom of the test.
        - float p: The p-value of the test
        - float epsilon: The privacy budget
    """
    n = len(y_true)
    L = int(np.ceil(np.log2(n)))
    t = np.empty(shape=(n_groups+1, ))
    if details:
        E_0_byGroup = np.empty(shape=(n_groups, ))
        E_1_byGroup = np.empty(shape=(n_groups, ))
        O_0_byGroup = np.empty(shape=(n_groups, ))
        O_1_byGroup = np.empty(shape=(n_groups, ))

    t[0] = 0
    hlstat = 0
    # Not in use?
    hlstat_clipping = 0
    laplace_scale = (np.ceil(np.log2(1/psi))+1+4*n_groups)/(1/psi*epsilon)
    #laplace_scale = 0.1
    noise_dict = {}
    # noise for the search function
    noise_values = random_gen.laplace(scale=laplace_scale,
                                      size=n*L)
    k = 0  # noise count
    for i in range(1, n+1):
        for j in range(1, L+1):
            if noise_dict.get((int(np.ceil(i/2**j)), j)) is None:
                noise_dict[(int(np.ceil(i/2**j)), j)] = noise_values[k]
                k = k+1
    noise_values_hlstat = random_gen.laplace(scale=laplace_scale,
                                             size=(n_groups, 4))
    for i in np.arange(1, n_groups+1):
        if i < n_groups:
            t[i] = ustat_hl_search_noise(
                t[i-1], 1, i/n_groups, psi, y_pred, ustat_hl, epsilon, noise_dict)
        else:
            t[i] = 1 + psi

        E_0 = n * ustat_hl(1-y_pred, y_pred,
                        t[i-1], t[i]) + noise_values_hlstat[i-1, 0]
        O_0 = n * ustat_hl(1-y_true, y_pred,
                        t[i-1], t[i]) + noise_values_hlstat[i-1, 1]
        E_1 = n * ustat_hl(y_pred, y_pred, t[i-1],
                        t[i]) + noise_values_hlstat[i-1, 2]
        O_1 = n * ustat_hl(y_true, y_pred, t[i-1],
                        t[i]) + noise_values_hlstat[i-1, 3]
        if details == True:
            E_0_byGroup[i-1] = E_0
            O_0_byGroup[i-1] = O_0
            E_1_byGroup[i-1] = E_1
            O_1_byGroup[i-1] = O_1
        hlstat += ((O_0 - E_0)**2) / E_0 + ((O_1 - E_1)**2) / E_1
    df = n_groups - 2
    p = scipy.stats.chi2.sf(hlstat, df)

    if details == True:
        TestResult = namedtuple('HosmerLemeshowTest', ('hlstat', 'df', 'p',
                                                       'E_0_byGroup', 'O_0_byGroup', 'E_1_byGroup', 'O_1_byGroup'))
        return TestResult(hlstat, df, p, E_0_byGroup, O_0_byGroup, E_1_byGroup, O_1_byGroup)
    else:
        TestResult = namedtuple(
            'HosmerLemeshowTest', ('hlstat', 'df', 'p'))
        return TestResult(hlstat, df, p)


def hosmer_lemeshow_test_w_search_middle_noise(y_true: np.array,
                                               y_pred: np.array,
                                               psi: float,
                                               ustat_hl: Callable[[np.array, float], float],
                                               epsilon: float,
                                               n_groups: int = 10,
                                               details: bool = False) -> namedtuple:
    """
    Performs a Hosmer–Lemeshow test.

    :param np.array y_true: Observed labels, either 0 or 1.
    :param np.array y_pred: Predicted probabilities, floats on [0, 1].
    :param float psi: Precision.
    :param function ustat_hl: U-statistic.
    :param epsilon: The privacy budget.
    :param int n_groups: optional, The number of groups to create.
        The default value is 10, which
        corresponds to deciles of predicted probabilities.
    :param bool details: optional, Add quantiles information to the saved file

    :return:
        - float hlstat: The test statistic.
        - int df: The degrees of freedom of the test.
        - float p: The p-value of the test
        - float epsilon: The privacy budget
    """
    n = len(y_true)
    L = int(np.ceil(np.log2(n)))
    t = np.empty(shape=(n_groups+1, ))
    if details:
        E_0_byGroup = np.empty(shape=(n_groups, ))
        E_1_byGroup = np.empty(shape=(n_groups, ))
        O_0_byGroup = np.empty(shape=(n_groups, ))
        O_1_byGroup = np.empty(shape=(n_groups, ))

    t[0] = 0
    hlstat = 0
    # Not in use?
    hlstat_clipping = 0
    laplace_scale = (np.ceil(np.log2(1/psi))+1+4*n_groups)/(n*epsilon)
    #laplace_scale = 0
    noise_dict = {}
    # noise for the search function
    noise_values = random_gen.laplace(scale=laplace_scale,
                               size=n*L)
    k = 0  # noise count
    for i in range(1, n+1):
        for j in range(1, L+1):
            if noise_dict.get((int(np.ceil(i/2**j)), j)) is None:
                noise_dict[(int(np.ceil(i/2**j)), j)] = noise_values[k]
                k = k+1
        noise_values_hlstat = random_gen.laplace(scale=laplace_scale,
                                          size=(n_groups, 4))

    middle = int(np.ceil((n_groups-1)/2))
    t[middle] = ustat_hl_search_noise(
        0, 1, middle/n_groups, psi, y_pred, ustat_hl, epsilon, noise_dict)
    for i in np.arange(1, n_groups+1):
        if i < middle:
            t[i] = ustat_hl_search_noise(
                t[i-1], t[middle], i/n_groups, psi, y_pred, ustat_hl, epsilon, noise_dict)
        elif middle < i < n_groups:
            t[i] = ustat_hl_search_noise(
                t[middle], 1, i/n_groups, psi, y_pred, ustat_hl, epsilon, noise_dict)
        elif i == middle:
            pass
        else:
            t[i] = 1+psi
        E_0 = n * ustat_hl(1-y_pred, y_pred,
                        t[i-1], t[i]) + noise_values_hlstat[i-1, 0]
        O_0 = n * ustat_hl(1-y_true, y_pred,
                        t[i-1], t[i]) + noise_values_hlstat[i-1, 1]
        E_1 = n * ustat_hl(y_pred, y_pred,
                        t[i-1], t[i]) + noise_values_hlstat[i-1, 2]
        O_1 = n * ustat_hl(y_true, y_pred,
                        t[i-1], t[i]) + noise_values_hlstat[i-1, 3]
        if details:
            E_0_byGroup[i-1] = E_0
            O_0_byGroup[i-1] = O_0
            E_1_byGroup[i-1] = E_1
            O_1_byGroup[i-1] = O_1
        hlstat += ((O_0 - E_0)**2) / E_0 + ((O_1 - E_1)**2) / E_1
        df = n_groups - 2
        p = scipy.stats.chi2.sf(hlstat, df)
        if details:
            TestResult = namedtuple('HosmerLemeshowTest', ('hlstat', 'df', 'p',
                                    'E_0_byGroup', 'O_0_byGroup', 'E_1_byGroup', 'O_1_byGroup'))
            return TestResult(hlstat, df, p, E_0_byGroup, O_0_byGroup, E_1_byGroup, O_1_byGroup)
        else:
            TestResult = namedtuple(
                'HosmerLemeshowTest', ('hlstat', 'df', 'p'))
            return TestResult(hlstat, df, p)


if __name__ == '__main__':
    from sklearn.datasets import load_breast_cancer
    from sklearn.linear_model import LogisticRegression
    X, y = load_breast_cancer(return_X_y=True)
    clf = LogisticRegression(random_state=0).fit(X, y)
    y_pred = clf.predict_proba(X)[:, 1]

    print(hosmer_lemeshow_test(y, y_pred))
    print(hosmer_lemeshow_test_w_search(y, y_pred, 0.0001, ustat_hl))
    print(hosmer_lemeshow_test_w_search_noise(y, y_pred, 0.0001, ustat_hl, 0.1))


def cmp_calib(y_true: np.array,
              y_pred: np.array,
              pre_psi: float,
              epsilon: float) -> namedtuple:

    """

    :param np.array y_true: Observed labels, either 0 or 1.
    :param np.array y_pred: Predicted probabilities, floats on [0, 1].
    :param float pre_psi: Precision.
    :param epsilon: The privacy budget.
    """

    n = len(y_true)
    N = np.int(np.ceil(1/pre_psi)+1)
    psi = 1.0 / (N-1)
    L = int(np.ceil(np.log2(N)))
    #laplace_scale = (np.ceil(np.log2(1/psi))+1+4*n_groups)/(1/psi*epsilon)
    count_laplace_scale = ((L+1)+8)/epsilon

    ecdf = y_pred_ecdf(y_pred)
    ecdf_dp = y_pred_ecdf_dp(y_pred, L+1, rng, count_laplace_scale, N)

    return 0
