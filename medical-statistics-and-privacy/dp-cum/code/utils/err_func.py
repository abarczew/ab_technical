import numpy as np


def mean_square_error(x: list,
                      y: list):
    return ((x-y)**2).sum()


def absolute_error(x: list,
                   y: list):
    return (np.abs(x-y)).sum()