import pandas as pd
from matplotlib import pyplot as plt
plt.style.use('seaborn-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)


def main(parameter_names):

    #######################################
    # Explore the space of all parameters #
    #######################################

    if 'explore' in parameter_names:
        for norm in [1, 2]:
            filepath = f'./results/smooth_exp_results_explore_norm{norm}.csv'
            outpath = f'./results/smooth_exp_results_rate_explore_norm{norm}.tex'
            df_res = pd.read_csv(filepath)
            df_plot = df_res[['lambda', 'epsilon', 'time_step_number',
                              'mse_true_dp', 'mse_true_smooth']]
            df_plot = df_plot.groupby(['time_step_number', 'lambda', 'epsilon'])\
                             .mean()
            df_plot['dp_smooth_rate'] = (df_plot.mse_true_smooth
                                         / df_plot.mse_true_dp)

            df_plot.drop(['mse_true_dp', 'mse_true_smooth'], axis=1, inplace=True)
            df_plot.unstack(level=2).to_latex(buf=outpath, index=True)

    ################################
    # Explore the space of epsilon #
    ################################

    if 'epsilon' in parameter_names:
        filepath_norm1 = './results/smooth_exp_results_epsilon_norm1.csv'
        filepath_norm2 = './results/smooth_exp_results_epsilon_norm2.csv'
        outpath = './results/smooth_exp_results_rate_epsilon_lambda3_n32768.png'
        df_eps_norm1 = pd.read_csv(filepath_norm1)
        df_eps_norm1['norm'] = 1
        df_eps_norm2 = pd.read_csv(filepath_norm2)
        df_eps_norm2['norm'] = 2
        df_eps = pd.concat([df_eps_norm1, df_eps_norm2])
        df_eps = df_eps.loc[df_eps['lambda'] > 1]

        df_eps['dp_smooth_rate'] = (df_eps.mse_true_smooth
                                    / df_eps.mse_true_dp)
        df_plot = df_eps.groupby(['norm', 'epsilon'])\
                        .agg(['mean', 'std'])\
                        .reset_index()

        for norm in [1, 2]:
            df = df_plot.loc[df_plot['norm'] == norm]
            plt.errorbar(df.epsilon, df.dp_smooth_rate['mean'],
                         yerr=df.dp_smooth_rate['std'],
                         label=f'smooth with {norm}norm', marker='o')
        plt.grid()
        plt.xlabel('Epsilon')
        plt.xscale('log')
        plt.ylabel('MSE ratio')
        plt.yscale('log')
        plt.legend()
        plt.savefig(outpath, bbox_inches='tight')
        plt.close()

    ###############################
    # Explore the space of lambda #
    ###############################

    if 'lambda' in parameter_names:
        filepath_norm1 = './results/smooth_exp_results_lambda_norm1.csv'
        filepath_norm2 = './results/smooth_exp_results_lambda_norm2.csv'
        outpath = './results/smooth_exp_results_rate_lambda_epsilon1_n32768.png'
        df_lam_norm1 = pd.read_csv(filepath_norm1)
        df_lam_norm1['norm'] = 1
        df_lam_norm2 = pd.read_csv(filepath_norm2)
        df_lam_norm2['norm'] = 2
        df_lam = pd.concat([df_lam_norm1, df_lam_norm2])

        df_lam['dp_smooth_rate'] = (df_lam.mse_true_smooth
                                    / df_lam.mse_true_dp)
        df_plot = df_lam.groupby(['norm', 'lambda'])\
                        .agg(['mean', 'std'])\
                        .reset_index()

        for norm in [1, 2]:
            df = df_plot.loc[df_plot['norm'] == norm]
            plt.errorbar(df['lambda'], df.dp_smooth_rate['mean'],
                         yerr=df.dp_smooth_rate['std'],
                         label=f'smooth with {norm}norm', marker='o')
        plt.grid()
        plt.xlabel('Lambda')
        plt.xscale('log')
        plt.ylabel('MSE ratio')
        plt.yscale('log')
        plt.legend()
        plt.savefig(outpath, bbox_inches='tight')
        plt.close()


if __name__ == '__main__':
    parameter_names = ['epsilon', 'lambda']
    main(parameter_names)
