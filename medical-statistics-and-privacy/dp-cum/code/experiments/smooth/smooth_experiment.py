import numpy as np
import pandas as pd
import datetime
from joblib import Parallel, delayed
from utils.dp_counter import generate_noise
from utils.smooth import smooth_dense, rv_index
from utils.err_func import mean_square_error, absolute_error
from experiments.smooth.smooth_parameters import params_explore, params_epsilon, params_lambda



#params_list = [params_explore, params_epsilon, params_lambda]
params_list = [params_lambda]
norms = [1, 2]


def run(time_step_range: list,
        poisson_lambda_range: list,
        epsilon_range: list,
        iter_number: int,
        n_jobs: int,
        parameters_name: str,
        norm: int):
    """
    Running Smooth experiment

    :param time_step_range: list, ?
    :param poisson_lambda_range: list, ?
    :param epsilon_range: list, ?
    :param iter_number: int, ?
    :param n_jobs: int, ?
    :param parameters_name: str to produce different namefiles for saving results
    :param norm: either 1 or 2, to choose different

    :return: results
    """

    seed = 42
    random_gen = np.random.default_rng(seed)
    # get the SeedSequence of the passed RaNdom Generator
    seed_seq = random_gen.bit_generator._seed_seq
    # create initial independent states
    child_states = seed_seq.spawn(iter_number)

    def experiment(time_step_range: list,
                   poisson_lambda_range: list,
                   epsilon_range: list,
                   iter_number: int,  # In use?
                   n_jobs: int,  # In use?
                   parameters_name: str,  # In use?
                   norm: int,
                   seed: int):
        """

        :param time_step_range: list, ?
        :param poisson_lambda_range: list, ?
        :param epsilon_range: list, ?
        :param iter_number: int, ?
        :param n_jobs: int, ?
        :param parameters_name: str to produce different namefiles for saving results
        :param norm: int, ?
        :param seed: int, ?

        :return: result, list, ?
        """

        result = []
        random_gen = np.random.default_rng(seed)

        for time_step_number in time_step_range:
            # from utils.smooth: pre-compute all indexation tables and similar info for
            #     binary tree organized random variables
            rvidx = rv_index(time_step_number)

            for poisson_lambda in poisson_lambda_range:

                for epsilon in epsilon_range:
                    # generate random streams
                    stream = random_gen.poisson(poisson_lambda,
                                                time_step_number)
                    stream = np.array(list(map(float, stream)))
                    # from numpy: Return the cumulative sum of the elements along a given axis
                    count_true = np.cumsum(stream)
                    # compute Diff Priv count
                    scale = epsilon / 8  # rvidx.L
                    _, sum_eta = generate_noise(random_gen,
                                                scale,
                                                rvidx)
                    count_diff_priv = count_true + sum_eta

                    try:
                        count_smooth, _ = smooth_dense(count_diff_priv,
                                                       rvidx,
                                                       norm)
                        # Register change in slope for the count (cumul=should always increase,
                        # except when adding negative noise)
                        is_decreasing = any((count_diff_priv[1:] - count_diff_priv[:-1]) < 0)

                        # Computation of Mean square error between:
                        # - Cumulative count and noisy cumulative count
                        mse_true_diff_priv = mean_square_error(count_true,
                                                        count_diff_priv)
                        # - Cumulative count and the smoothed noisy cumulative count
                        mse_true_smooth = mean_square_error(count_true,
                                                            count_smooth)

                        # Same as above with the absolute error
                        ae_true_diff_priv = absolute_error(count_true,
                                                    count_diff_priv)
                        ae_true_smooth = mean_square_error(count_true,
                                                           count_smooth)

                        res_item = {'lambda': poisson_lambda,
                                    'epsilon': epsilon,
                                    'time_step_number': time_step_number,
                                    'is_decreasing': is_decreasing,
                                    'mse_true_dp': mse_true_diff_priv,
                                    'mse_true_smooth': mse_true_smooth,
                                    'ae_true_dp': ae_true_diff_priv,
                                    'ae_true_smooth': ae_true_smooth}

                        print(res_item)

                        result.append(res_item)
                    except IndexError:
                        print(f'indexerror with smooth_1 with {poisson_lambda}\
                         and {time_step_number}')

        return result

    results = Parallel(n_jobs=n_jobs)(delayed(experiment)
                                      (time_step_range,
                                       poisson_lambda_range,
                                       epsilon_range,
                                       iter_number,
                                       n_jobs,
                                       parameters_name,
                                       norm,
                                       seed)
                                      for seed in child_states)

    df_res = pd.DataFrame.from_records(np.ravel(np.array(results)))
    df_res['timestamp'] = datetime.datetime.now()
    filepath = f'results/smooth_exp_results_{parameters_name}_norm{norm}.csv'
    print(f'Write {filepath}')
    df_res.to_csv(filepath)


if __name__ == '__main__':
    for params in params_list:
        for norm in norms:
            run(**params, norm=norm)
