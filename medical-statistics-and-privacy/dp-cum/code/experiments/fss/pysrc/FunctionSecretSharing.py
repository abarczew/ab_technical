from logging import raiseExceptions
import numpy as np
from pysrc.utils import *
from abc import ABC, abstractmethod


class FunctionSecretSharing(ABC):
    """
    Abstract class from which implementations of FSS algorithms can inherit.

    Notations:
    • Function to be secret shared:
        𝑓:{0,…,N-1} ⟶ 𝔾
    where  𝔾  is an abelian group. In what follows, 𝔾=ℤ_q with addition.
    • p is the number of parties/servers.
    • N is the size of the domain of 𝑓.
    • n is the number of bits needed to encode elements of {0,…,N-1}.
    • q is the size/order of 𝔾: |𝔾|=q.
    • m is the number of bits needed to encode elements of 𝔾=ℤ_q.
    """

    ### CONSTRUCTOR

    def __init__(self, p, N, q):
        self._p = p
        self._N = N
        self._n = int(np.ceil(np.log2(N)))
        self._q = q
        self._m = int(np.ceil(np.log2(q)))



    ### ATTRIBUTES

    @property
    def p(self):
        return self._p

    @p.setter
    def p(self, value):
        raise Exception('Attributes should not be changed after instantiation.')

    @property
    def N(self):
        return self._N

    @N.setter
    def N(self, value):
        raise Exception('Attributes should not be changed after instantiation.')

    @property
    def n(self):
        return self._n

    @n.setter
    def n(self, value):
        raise Exception('Attributes should not be changed after instantiation.')

    @property
    def m(self):
        return self._m

    @m.setter
    def m(self, value):
        raise Exception('Attributes should not be changed after instantiation.')

    @property
    def q(self):
        return self._q

    @q.setter
    def q(self, value):
        raise Exception('Attributes should not be changed after instantiation.')



    ### METHODS

    @staticmethod
    def _serialize_keys_for_gen(keys):
        """
        Serialize the keys so that they can be sent easily.

        Params:
            keys (list/array of int ∈ {0,1}): the keys to be serialized.

        Returns:
            keys_enc (list of bytestring): list of the encoded keys.
        """
        keys_enc = []
        for key in keys:
            keys_enc.append(serialize(key))
        return keys_enc



    # Abstract Methods

    @abstractmethod
    def generate(self, a, g=1):
        pass


    @abstractmethod
    def evaluate(self, key_enc, x):
        pass
