import numpy as np


class DistributedPlainSharing(object):
    """docstring for DistributedPlainSharing."""

    def __init__(self, kwargs):
        super(DistributedPlainSharing, self).__init__()
        self.thresholds = kwargs['thresholds']

    def evaluate(self, data):
        ecdf = []
        thresholds = self.thresholds
        for i in thresholds:
            ecdf.append(len(data[data <= i]))
        return ecdf


class Server():
    """
    Simulate a Server in order to achieve some experiments

    attributes :
        keys (list) : list of function secret shares the server received
        count (int) : The number of shares received
    """

    def __init__(self):

        self.keys = []
        self.count = 0

    def receive(self, key):
        """
        Add the key in parameter to the attribute 'keys' and increments the attribute 'count'

        params :
            key (binary string) : A function share
        """
        self.keys.append(key)
        self.count += 1

    def evaluate(self, fss, index, i=None):
        """
        Evaluate the share of index 'index', for x = 'i' with an instance of FSS with the same parameters used to generate the shares

        params :
            fss (object) : An instance of a function secret sharing scheme
            index (int) : The index of the share to evaluate
            i (int) : The x for wich evaluate the function
        """
        assert(index < self.count)

        key = self.keys[index]

        if i == None:
            return fss.evaluate(key)

        assert(i < fss.N)
        return fss.evaluate(key, i)

    def generate_dp_noise(self, value):
        pass


class DataOwner():
    """
    Simulate a DataOwner in order to achieve some experiments

    attributes :
        data (numpy.array) : The data to privately share
    """

    def __init__(self, data, N, bins=None):
        self.data = data
        if bins is None:
            self.bins = np.arange(N)
        else:
            self.bins = bins

    def generate(self, fss):
        for i, val in enumerate(np.digitize(self.data, self.bins, right=True)):
            keys = fss.generate(val, 1)
            yield keys

    def set_data(self, data):
        self.data = data
