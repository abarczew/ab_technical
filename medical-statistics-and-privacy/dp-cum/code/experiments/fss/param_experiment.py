from utils import generate_synthetic_data, experiment, plot_histograms_and_ecdf
from scipy.stats import poisson, norm, multinomial
import os
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
plt.style.use('seaborn-v0_8-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)


def main():
    directory = 'plot'
    if not os.path.exists(directory):
        os.makedirs(directory)

    # parties and data owners
    q = 2
    l = 10
    N = 100
    size = 2 ** 14
    data = generate_synthetic_data(norm, {'loc': 500, 'scale': 100}, size=size)
    parties_range = [2, 5, 10, 50, 100]
    do_range = [2 ** 2, 2 ** 5, 2 ** 10]
    records = []
    outpath = f"{directory}/parties_do_runtime.png"
    for p in parties_range:
        for do in do_range:
            results = experiment(p=p, do=do, N=N, q=q, l=l, data=data)
            records.append({'p': p, 'do': do, 'time': results['time']})

    df = pd.DataFrame.from_records(records)
    df_plot = pd.pivot_table(df, values='time', index=['p'],
                             columns=['do'], aggfunc="mean")

    sns.heatmap(df_plot, annot=True)
    plt.savefig(outpath)
    plt.close()

    # N points
    p = 3
    do = 4
    q = 2
    l = 10
    size = 2 ** 14
    data = generate_synthetic_data(norm, {'loc': 500, 'scale': 100}, size=size)
    N_range = [10, 100, 1000, 10000]
    records = []
    outpath = f"{directory}/n_runtime.png"
    for N in N_range:
        results = experiment(p=p, do=do, N=N, q=q, l=l, data=data)
        records.append({'N': N, 'time': results['time']})

    df = pd.DataFrame.from_records(records)
    plt.plot(df['N'], df['time'])
    plt.savefig(outpath)
    plt.close()

    # data size
    p = 3
    do = 4
    q = 2
    N = 1000
    l = 10
    size_range = [2 ** 5, 2 ** 10, 2 ** 20]
    records = []
    outpath = f"{directory}/size_runtime.png"
    for size in size_range:
        data = generate_synthetic_data(norm, {'loc': 500, 'scale': 100}, size=size)
        results = experiment(p=p, do=do, N=N, q=q, l=l, data=data)
        records.append({'size': size, 'time': results['time']})

    df = pd.DataFrame.from_records(records)
    plt.plot(df['size'], df['time'])
    plt.savefig(outpath)
    plt.close()

    # security parameter
    p = 3
    do = 4
    q = 2
    N = 1000
    size = 2 ** 14
    data = generate_synthetic_data(norm, {'loc': 500, 'scale': 100}, size=size)
    l_range = [1, 10, 100]
    records = []
    outpath = f"{directory}/l_runtime.png"
    for l in l_range:
        results = experiment(p=p, do=do, N=N, q=q, l=l, data=data)
        records.append({'l': l, 'time': results['time']})

    df = pd.DataFrame.from_records(records)
    plt.plot(df['l'], df['time'])
    plt.savefig(outpath)
    plt.close()


if __name__ == '__main__':
    main()
