import pandas as pd
from matplotlib import pyplot as plt
plt.style.use('seaborn-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)


def main():

    ################################
    # Explore the space of epsilon #
    ################################

    for norm in [2]:
        filepath_norm = f'results/inv_exp_results_explore_norm{norm}.csv'
        outpath = f'results/inv_exp_results_epsilon_lambda3_n30000_norm{norm}.png'
        df_eps = pd.read_csv(filepath_norm)

        df_plot = df_eps.rename(columns={'dp_err': 'DP error',
                                         'sm_err': 'smooth error',
                                         'ecdf_mse': 'ECDF MSE'})
        df_plot = df_plot.groupby('epsilon').agg(['mean', 'std']).reset_index()
        for col in ['DP error', 'smooth error', 'ECDF MSE']:
            plt.errorbar(df_plot.epsilon, df_plot[col]['mean'],
                         yerr=df_plot[col]['std'],
                         label=f'{col}', marker='o')
        plt.grid()
        plt.xlabel('Epsilon')
        plt.xscale('log')
        plt.ylabel('MSE ratio')
        plt.yscale('log')
        plt.legend()
        plt.savefig(outpath, bbox_inches='tight')
        plt.close()


if __name__ == '__main__':
    main()
