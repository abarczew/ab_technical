from experiments.inverse_ecdf.inv_parameters import params_explore, params_epsilon, params_lambda
from utils.smooth import smooth_dense, rv_index, binrv_print
import numpy as np
import pandas as pd
import datetime
from joblib import Parallel, delayed
from utils.dp_counter import generate_noise, generate_data_multinom, generate_data_poisson
from utils.err_func import mean_square_error, absolute_error




#params_list = [params_explore, params_epsilon, params_lambda]
params_list = [params_explore]
norms = [1, 2]


def bin_search(vector: list,
               N: int,
               threshold: int):
    """
    Binary search

    :param vector: vector
    :param N: len(vector)
    :param threshold: threshold

    :return: smallest index r with threshold < vector[r]
    (or N if no such index exists)
    """
    low = 0
    high = N
    while (low < high):
        mid = (low + high)//2
        if vector[mid] < threshold:
            low = mid+1
        else:
            high = mid
    return low


def one_exp(rvidx,
            n,
            epsilon,
            seed: int,
            norm: int):
    """
    Running one Experiment

    :param rvidx, an rv_index structure
    :param n, ? in particular, diff with N?
    :param epsilon, parameter of the Laplace distribution
    :param seed: int as fixed seed for random generator
    :param norm: either 1 or 2, to choose different

    :return: ?
    """
    random_gen = np.random.default_rng(seed)
    N = rvidx.N
    L = rvidx.L # What is L?
    # From utils.do_counter: generate DP noise
    (eta, sum_eta) = generate_noise(random_gen,
                                    epsilon/L,
                                    rvidx)
    # From utils.do_counter: generate data 
    # according to the multinomial model
    data = generate_data_multinom(random_gen,
                                  N,
                                  n)
    # make cumulative
    # Is it the dataset listed in table 1?
    for i in range(1, len(data)):
        data[i] = data[i] + data[i-1]
    # compute Diff Priv count
    diff_priv_data = data + sum_eta
    # Rescaling?
    data = data / n
    diff_priv_data = diff_priv_data / n
    (smooth_data, _) = smooth_dense(diff_priv_data,
                                    rvidx,
                                    norm)

    diff_priv_error = 0
    smooth_error = 0

    for i in range(N):
        diff_priv_i = bin_search(diff_priv_data,
                                 N,
                                 data[i])
        smooth_i = bin_search(smooth_data,
                              N,
                              data[i])
        # Computation of mean square error
        diff_priv_error = diff_priv_error + (i-diff_priv_i)**2
        smooth_error = smooth_error + (i-smooth_i)**2

    diff_priv_error = diff_priv_error / (N**3)
    smooth_error = smooth_error / (N**3)
    ecdf_mse = ((L**3)/(epsilon**2))/(n**2)
    return (diff_priv_error, smooth_error, ecdf_mse)


def run(time_step_range: list,
        num_event_range: list,
        epsilon_range: list,
        iter_number: int,
        n_jobs: int,
        parameters_name: str,
        norm=1):
    """
    Running inverse ECDF experiment

    :param time_step_range: list, ?
    :param num_event_range: list, ?
    :param epsilon_range: list, ?
    :param iter_number: int, ?
    :param n_jobs: int, ?
    :param parameters_name: str to produce different namefiles for saving results
    :param norm: either 1 or 2, to choose different

    :return: results
    """
    result = [] # In use?
    experiment_list = []
    experiment_id = 0

    for time_step_number in time_step_range:
        # from utils.smooth: pre-compute all indexation tables and similar info for
        #     binary tree organized random variables
        rvidx = rv_index(time_step_number)

        for num_event in num_event_range:

            for epsilon in epsilon_range:

                for repeat_iter in range(iter_number):
                    experiment_id = experiment_id + 1
                    experiment_list.append((rvidx,
                                            {'N': time_step_number,
                                            'n': num_event,
                                            'epsilon': epsilon,
                                            'iter': repeat_iter,
                                            'exp': experiment_id}))

    def experiment(params,
                   norm):

        print(params[1])
        (diff_priv_error, smooth_error, ecdf_mse) = one_exp(params[0],
                                                            params[1]['n'],
                                                            params[1]['epsilon'],
                                                            params[1]['iter'],
                                                            norm)
        res_item = params[1]
        res_item['diff_priv_error'] = diff_priv_error
        res_item['smooth_error'] = smooth_error
        res_item['ecdf_mse'] = ecdf_mse
        return res_item

    results = Parallel(n_jobs=n_jobs)(delayed(experiment)
                                      (params,
                                       norm)
                                      for params in experiment_list)

    sum_res = dict()

    for ri in results:
        k = (ri['N'], ri['n'], ri['epsilon'])
        if not(k in sum_res):
            sum_res[k] = {'#': 0,
                          'diff_priv_error': 0.0,
                          'smooth_error': 0.0,
                          'ecdf_mse': 0.0}
        sum_res[k]['#'] = sum_res[k]['#'] + 1
        sum_res[k]['diff_priv_error'] = sum_res[k]['diff_priv_error'] + ri['diff_priv_error']
        sum_res[k]['smooth_error'] = sum_res[k]['smooth_error'] + ri['smooth_error']
        sum_res[k]['ecdf_mse'] = sum_res[k]['ecdf_mse'] + ri['ecdf_mse']

    for ri in sum_res.items():
        print(ri[0], ",\t", ri[1]['diff_priv_error']/ri[1]['#'], ",\t", ri[1]
              ['smooth_error']/ri[1]['#'], ri[1]['ecdf_mse']/ri[1]['#'])

    df_res = pd.DataFrame.from_records(np.ravel(np.array(results)))
    df_res['timestamp'] = datetime.datetime.now()
    df_res.to_csv(f'results/inv_exp_results_{parameters_name}_norm{norm}.csv')


if __name__ == '__main__':
    for params in params_list:
        for norm in norms:
            run(**params, norm=norm)
