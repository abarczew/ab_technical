from sklearn.linear_model import LogisticRegression
from sklearn.datasets import load_breast_cancer
import numpy as np
import scipy
from typing import Callable
from numpy.random import laplace
import pandas as pd
import matplotlib.pyplot as plt
import random

from utils.hltest import *
from utils.ustat import ustat_hl


def hl_stat(y_true: np.array,
            y_pred: np.array,
            psi: float,
            ustat_hl: Callable[[np.array, float], float],
            epsilon: float,
            n_groups: int = 10,
            details: bool = False) -> namedtuple:
    """
    This function performs the three different types of HL test
    (Basic, With search, With search and noise )

    :param y_true: array
        Observed labels, either 0 or 1.
    :param y_pred: array
        Predicted probabilities, floats on [0, 1].
    :param psi: float
        Precision.
    :param ustat_hl: function
        U-statistic.
    :param n_groups: int, optional
        The number of groups to create. The default value is 10, which
        corresponds to deciles of predicted probabilities.
    :param details: bool, if true hosmer_lemeshow_test_w_search_noise will print
    the quantiles, E_0 and E_1

    :return:
        - hlstat: float, The test statistic.
        - df: int, The degrees of freedom of the test.
        - p: float, The p-value of the test
        - epsilon: float, The privacy budget
    """
    hl = hosmer_lemeshow_test(y_true, y_pred, n_groups)[0]
    hl_search = hosmer_lemeshow_test_w_search(y_true,
                                              y_pred,
                                              psi,
                                              ustat_hl,
                                              n_groups)[0]
    hl_noise = hosmer_lemeshow_test_w_search_middle_noise(y_true,
                                                          y_pred,
                                                          psi,
                                                          ustat_hl,
                                                          epsilon,
                                                          n_groups,
                                                          details)[0]
    TestResult = namedtuple('HosmerLemeshowTest',
                            ('hl', 'hl_Search', 'hl_noise'))
    return TestResult(hl,
                      hl_search,
                      hl_noise)


""" Use Example 1: Breast Cancer Dataset"""

X, y = load_breast_cancer(return_X_y=True)
clf = LogisticRegression(random_state=0).fit(X, y)
y_pred = clf.predict_proba(X)[:, 1]

print(hl_stat(y, y_pred, 0.0000001, ustat_hl, 0.1))


""" Use Example 2 : Heart dataset """

df = pd.read_csv('framingham.csv')
df.dropna(axis=0, inplace=True)
x = np.array(df[['prevalentHyp', 'cigsPerDay', 'male', 'heartRate',
             'totChol', 'BMI', 'diaBP', 'glucose', 'sysBP', 'age']])
y_true = np.array(df['TenYearCHD'])
clf = LogisticRegression(random_state=0).fit(x, y_true)
y_pred = clf.predict_proba(x)[:, 1]

print(hl_stat(y_true, y_pred, 0.0000001, ustat, 0.1))


""" Use Example 3: Bank data set """

df_bank = pd.read_csv('bank-full.csv', sep=";")
df_bank.dropna(axis=0, inplace=True)

bank_category = {"loan":    {"yes": 1, "no": 0},
                 "default": {"yes": 1, "no": 0},
                 "housing": {"yes": 1, "no": 0},
                 "marital": {"single": 0, "married": 1, "divorced": 2}}
df_bank = df_bank.replace(bank_category)

x = np.array(df_bank[['age', 'balance', 'day', 'duration', 'campaign',
             'pdays', 'previous', 'loan', 'marital', 'default', 'housing']])
y_true = (df_bank['y'] == "yes")
clf = LogisticRegression(random_state=0).fit(x, y_true)
y_pred = clf.predict_proba(x)[:, 1]
print(hl_stat(y_true, y_pred, 0.0000001, ustat, 0.1))
