import matplotlib.pyplot as plt
import pandas as pd
plt.style.use('seaborn-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)


def main():
    df_plot = pd.read_csv('results/hl_smooth_exp_bank-full.csv')
    df_plot = df_plot.groupby('epsilon').agg(['mean', 'std']).reset_index()

    df_plot.mse_dp = df_plot.mse_dp / (198.10**2)
    df_plot.mse_sm = df_plot.mse_sm / (198.10**2)
    
    dataset_name = 'bank'
    outpath = f'results/hl_smooth_exp_{dataset_name}'
    plt.errorbar(df_plot.epsilon,
                 df_plot.mse_dp['mean'],
                 yerr=df_plot.mse_dp['std'],
                 label='DP',
                 marker='o')
    plt.errorbar(df_plot.epsilon,
                 df_plot.mse_sm['mean'],
                 yerr=df_plot.mse_sm['std'],
                 label='Smooth',
                 marker='o')
    plt.grid()
    plt.yscale('log')
    plt.xlabel('Epsilon')
    plt.xscale('log')
    plt.ylabel('MSE')
    plt.legend()
    plt.savefig(f'{outpath}.png', bbox_inches='tight')
    plt.close()


if __name__ == '__main__':
    main()
