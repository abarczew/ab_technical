from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OneHotEncoder
from utils.hltest_smooth import *
from utils.hltest import *
from utils.smooth import *
import numpy as np
import pandas as pd
random_gen = np.random.default_rng(123)


dataset_names = ['framingham', 'bank-full', 'diabetic_data']
datadir = 'data'
outdir = 'results'
epsilon_range = [0.1, 0.2, 0.5, 1.0, 2.0, 5.0, 10.0, 20.0]


def hl_sample(y_true,
              y_pred,
              rate,
              random_gen):
    """
    hl_sample: ?

    :param y_true: ?
    :param y_pred: ?
    :param rate: ?
    :param random_gen: ?

    :return: ?
    """
    n = len(y_pred)
    s_true = [] # Variable for what?
    s_pred = [] # Variable for what?
    for i in range(n):
        if random_gen.uniform() < rate:
            s_true.append(y_true[i])
            s_pred.append(y_pred[i])
    return (s_true, s_pred)


def get_hl_test(y_true,
                y_pred,
                dataset_name):
    """
    get_hl_test: ?

    :param y_true: ?
    :param y_pred: ?
    :param dataset_name: ?

    :return: ?
    """
    res_final = []
    for epsilon in epsilon_range:
        print(f'Calling repeat_hosmer_lemeshow_test for epsilon={epsilon}')
        mse_dp, mse_sm, res = repeat_hosmer_lemeshow_test(100,
                                                          random_gen,
                                                          y_true,
                                                          y_pred,
                                                          0.0001,
                                                          ustat_hl,
                                                          epsilon,
                                                          details=False)
        res_final.append(res)
        print("eps = ", epsilon, "\tmse = ", (mse_dp, mse_sm))
    df = pd.DataFrame.from_records(np.ravel(np.array(res_final)))
    df.to_csv(f'{outdir}/hl_smooth_exp_{dataset_name}.csv')


def main():

    #####################################
    # Compute ROC on framingham dataset #
    #####################################

    if 'framingham' in dataset_names:
        dataset_name = 'framingham'
        print(f'Reading w/ dataset {dataset_name}')
        df = pd.read_csv(f'{datadir}/{dataset_name}.csv').fillna(0)
        print(f'Composing Arrays')
        x = np.array(df[['prevalentHyp', 'cigsPerDay', 'male', 'heartRate',
                         'totChol', 'BMI', 'diaBP', 'glucose', 'sysBP', 'age']])
        y_true = np.array(df['TenYearCHD'])
        print(f'Calling Logistic Regression')
        clf = LogisticRegression(random_state=0).fit(x, y_true)
        y_pred = clf.predict_proba(x)[:, 1]

        print(f'Calling func. get_hl_test')
        get_hl_test(y_true, y_pred, dataset_name)

    ###############################
    # Compute ROC on bank dataset #
    ###############################

    if 'bank-full' in dataset_names:
        dataset_name = 'bank-full'
        print(f'Reading w/ dataset {dataset_name}')
        df_bank = pd.read_csv(f'{datadir}/{dataset_name}.csv', sep=',')
        # df_bank = pd.read_csv(f'{datadir}/{dataset_name}.csv', sep=';')
        print(df_bank)
        bank_category = {"loan": {"yes": 1, "no": 0},
                         "default": {"yes": 1, "no": 0},
                         "housing": {"yes": 1, "no": 0},
                         "marital": {"single": 0, "married": 1, "divorced": 2}}
        print(f'Composing Arrays')
        df_bank = df_bank.replace(bank_category)
    #     # remove special character
        df_bank.columns = df_bank.columns.str.replace('\n', '')
        x = np.array(df_bank[['age', 'balance', 'day', 'duration', 'campaign', 'pdays', 'previous', 'loan', 'marital', 'default', 'housing']])
        y_true = (df_bank['Target'] == "yes")
        # y_true = (df_bank['y'] == "yes")
        print(f'Calling Logistic Regression')
        clf = LogisticRegression(random_state=0).fit(x, y_true)
        y_pred = clf.predict_proba(x)[:, 1]
    #
        print(f'Calling func. get_hl_test')
        get_hl_test(y_true, y_pred, dataset_name)

    ###################################
    # Compute ROC on diabetes dataset #
    ###################################

    if 'diabetic_data' in dataset_names:
        dataset_name = 'diabetic_data'
        print(f'Reading w/ dataset {dataset_name}')
        df_dia = pd.read_csv(f'{datadir}/{dataset_name}.csv')
        target_name = 'readmitted'

        print(f'Composing Arrays')
        enc = OneHotEncoder(drop='first')
        X = enc.fit_transform(df_dia[[col for col in df_dia.columns
                                      if col != target_name]])
        y_true = df_dia[target_name].map(lambda x: 0 if x == 'NO' else 1)
        X_train, X_test, y_train, y_test = train_test_split(X, y_true, test_size=0.6,
                                                            random_state=42)
        print(f'Calling Logistic Regression')
        clf = LogisticRegression(solver='lbfgs')
        clf = make_pipeline(StandardScaler(with_mean=False), clf)
        clf.fit(X_train, y_train)
        # we leak data to boost performance
        y_pred = clf.predict_proba(X)[:, 1]

        print(f'Calling func. get_hl_test')
        get_hl_test(y_true, y_pred, dataset_name)

if __name__ == '__main__':
    main()