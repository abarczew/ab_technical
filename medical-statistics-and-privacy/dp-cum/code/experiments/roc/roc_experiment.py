import numpy as np
import matplotlib.pyplot as plt
from joblib import Parallel, delayed
from utils.roc import roc_curve_ustat, geo_score, auc
from utils.ustat import UStat_smooth_ROC
from sklearn import metrics
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.svm import LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OneHotEncoder
import pandas as pd
plt.style.use('seaborn-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)


def get_roc_exp_score(X_train,
                      X_test,
                      y_train,
                      y_test,
                      model,
                      dataset_name,
                      epsilon,
                      outdir,
                      iter_number,
                      illustration):
    """
    get_roc_exp_score: ?

    :param X_train: ?
    :param X_test: ?
    :param y_train: ?
    :param y_test: ?
    :param model: ?
    :param dataset_name: ?
    :param epsilon: ?
    :param outdir: ?
    :param iter_number: ?
    :param illustration: ?

    :return: ?
    """
    res = []
    # define models to test
    models = {'logistic': LogisticRegression(solver='lbfgs'),
              'SVM': LinearSVC(dual=False, max_iter=1000),
              'random_forest': RandomForestClassifier(n_estimators=50),
              'extra_tree': ExtraTreesClassifier(n_estimators=50),
              'gboost': GradientBoostingClassifier(n_estimators=50)}

    # compute scores for logistic
    # regression, the best classifier here
    print('Compute ' + model)
    classif = models[model]

    if model in ['logistic', 'SVM']:
        classif = make_pipeline(StandardScaler(with_mean=False),
                                classif)
        classif.fit(X_train,
                    y_train)
        y_pred = classif.decision_function(X_test)
    else:
        classif.fit(X_train,
                    y_train)
        y_pred = classif.predict_proba(X_test)[:, 1]

    false_pos_rate, true_pos_rate, thresholds = metrics.roc_curve(y_test,
                                                                  y_pred)
    roc_auc = metrics.auc(false_pos_rate,
                          true_pos_rate)

    print(f'Model {model}: {roc_auc} AUC')

    stat = UStat_smooth_ROC(y_test, y_pred)

    print('Compute true roc')
    # Need to explicit what are the arguments of roc_curve_ustat here?
    false_pos_rate_ustat_plain, true_pos_rate_ustat_plain = roc_curve_ustat(stat,
                                                                            1)

    for seed in range(iter_number):
        print(f'Compute diff priv roc, epsilon={epsilon}')
        # No smoothing, no computed norm
        false_pos_rate_ustat_diff_priv, true_pos_rate_ustat_diff_priv = roc_curve_ustat(stat=stat,
                                                                                        pos_label=1,
                                                                                        epsilon=epsilon,
                                                                                        seed=seed)
        roc_auc_diff_priv = auc(false_pos_rate_ustat_diff_priv,
                                true_pos_rate_ustat_diff_priv)
        geo_score_true_diff_priv = geo_score(false_pos_rate_ustat_plain,
                                             true_pos_rate_ustat_plain,
                                             false_pos_rate_ustat_diff_priv,
                                             true_pos_rate_ustat_diff_priv)

        print(f'Compute smooth diff priv roc, 2norm, epsilon={epsilon}')
        # Need better name for variables + no hard coded values (1, 2)
        false_pos_rate_ustat_2norm, true_pos_rate_ustat_2norm = roc_curve_ustat(stat=stat,
                                                                                pos_label=1,
                                                                                epsilon=epsilon,
                                                                                smooth=True,
                                                                                norm=2,
                                                                                seed=seed)
        roc_auc_2norm = auc(false_pos_rate_ustat_2norm,
                            true_pos_rate_ustat_2norm)
        geo_score_true_2norm = geo_score(false_pos_rate_ustat_plain,
                                         true_pos_rate_ustat_plain,
                                         false_pos_rate_ustat_2norm,
                                         true_pos_rate_ustat_2norm)

        print(f'Compute smooth diff priv roc, 1norm, epsilon={epsilon}')
        false_pos_rate_ustat_1norm, true_pos_rate_ustat_1norm = roc_curve_ustat(stat=stat,
                                                                                pos_label=1,
                                                                                epsilon=epsilon,
                                                                                smooth=True,
                                                                                norm=1,
                                                                                seed=seed)
        roc_auc_1norm = auc(false_pos_rate_ustat_1norm,
                            true_pos_rate_ustat_1norm)
        geo_score_true_1norm = geo_score(false_pos_rate_ustat_plain,
                                         true_pos_rate_ustat_plain,
                                         false_pos_rate_ustat_1norm,
                                         true_pos_rate_ustat_1norm)

        res.append({'dataset': dataset_name,
                    'model': model,
                    'epsilon': epsilon,
                    'auc': roc_auc,
                    'auc_dp': roc_auc_diff_priv,
                    'auc_smooth_2norm': roc_auc_2norm,
                    'auc_smooth_1norm': roc_auc_1norm,
                    'geo_score_true_dp': geo_score_true_diff_priv,
                    'geo_score_true_2norm': geo_score_true_2norm,
                    'geo_score_true_1norm': geo_score_true_1norm})

    if illustration:
        outpath = f'{outdir}/roc_exp_results_{dataset_name}_{epsilon}.png'

        # plt.figure(figsize=(7, 7))
        plt.plot(false_pos_rate_ustat_diff_priv,
                 true_pos_rate_ustat_diff_priv,
                 label='DP ROC')
        plt.plot(false_pos_rate_ustat_2norm,
                 true_pos_rate_ustat_2norm,
                 label='2norm smooth ROC')
        plt.plot(false_pos_rate_ustat_1norm,
                 true_pos_rate_ustat_1norm,
                 label='1norm smooth ROC')
        plt.plot(false_pos_rate_ustat_plain,
                 true_pos_rate_ustat_plain,
                 label='True ROC')
        plt.grid()
        plt.xlabel('False positive rate')
        plt.ylabel('True positive rate')
        plt.legend()
        plt.savefig(outpath, bbox_inches='tight')
        plt.close()

    return res


def main():
    n_jobs = 8
    epsilon_range = [0.02, 0.05, 0.2, 0.5, 2., 5., 20., 50.]
    iter_number = 100
    illustration = True
    dataset_names = ['diabetic_data']  # 'framingham', 'bank-full',
    datadir = 'data'
    outdir = 'results'

    #####################################
    # Compute ROC on framingham dataset #
    #####################################

    if 'framingham' in dataset_names:
        dataset_name = 'framingham'
        df_fram = pd.read_csv(f'{datadir}/{dataset_name}.csv').fillna(0)
        X = df_fram[[col for col in df_fram.columns if col != 'TenYearCHD']]
        y = df_fram.TenYearCHD
        # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33,
        #                                                     random_state=42)

        model = 'logistic'
        res_fam = get_roc_exp_score(X, X, y, y,
                                    model, dataset_name, epsilon_range,
                                    outdir, iter_number, illustration)

        outpath = f'{outdir}/roc_exp_summary_{dataset_name}_{iter_number}'
        df_plot = pd.DataFrame.from_records(res_fam)
        df_plot = df_plot.groupby('epsilon').agg(['mean', 'std']).reset_index()
        df_plot.to_latex(buf=f'{outpath}.tex', index=True)

        plt.errorbar(df_plot.epsilon, df_plot.geo_score_true_1norm['mean'],
                     yerr=df_plot.geo_score_true_1norm['std'],
                     label='smooth with 1norm', marker='o')
        plt.errorbar(df_plot.epsilon, df_plot.geo_score_true_2norm['mean'],
                     yerr=df_plot.geo_score_true_2norm['std'],
                     label='smooth with 2norm', marker='o')
        plt.grid()
        plt.yscale('log')
        plt.xlabel('Epsilon')
        plt.xscale('log')
        plt.ylabel('Area between true ROC and smooth DP ROC')
        plt.legend()
        plt.savefig(f'{outpath}.png', bbox_inches='tight')
        plt.close()

    ###############################
    # Compute ROC on bank dataset #
    ###############################

    if 'bank-full' in dataset_names:
        dataset_name = 'bank-full'
        df_ban = pd.read_csv(f'{datadir}/{dataset_name}.csv', sep=';')
        enc = OneHotEncoder(drop='first')
        X = enc.fit_transform(df_ban[[col for col
                                      in df_ban.columns if col != 'y']])
        y = df_ban.y.map(lambda x: 1 if x == 'yes' else 0)
        # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33,
        #                                                     random_state=42)

        model = 'SVM'
        res_ban = get_roc_exp_score(X, X, y, y,
                                    model, dataset_name, epsilon_range,
                                    outdir, iter_number, illustration)

        outpath = f'{outdir}/roc_exp_summary_{dataset_name}_{iter_number}'
        df_plot = pd.DataFrame.from_records(res_ban)
        df_plot = df_plot.groupby('epsilon').agg(['mean', 'std']).reset_index()
        df_plot.to_latex(buf=f'{outpath}.tex', index=True)

        plt.errorbar(df_plot.epsilon, df_plot.geo_score_true_1norm['mean'],
                     yerr=df_plot.geo_score_true_1norm['std'],
                     label='smooth with 1norm', marker='o')
        plt.errorbar(df_plot.epsilon, df_plot.geo_score_true_2norm['mean'],
                     yerr=df_plot.geo_score_true_2norm['std'],
                     label='smooth with 2norm', marker='o')
        plt.grid()
        plt.yscale('log')
        plt.xlabel('Epsilon')
        plt.xscale('log')
        plt.ylabel('Area between true ROC and smooth DP ROC')
        plt.legend()
        plt.savefig(f'{outpath}.png', bbox_inches='tight')
        plt.close()

    ###################################
    # Compute ROC on diabetes dataset #
    ###################################

    if 'diabetic_data' in dataset_names:
        dataset_name = 'diabetic_data'
        df_dia = pd.read_csv(f'{datadir}/{dataset_name}.csv')
        target_name = 'readmitted'

        enc = OneHotEncoder(drop='first')
        X = enc.fit_transform(df_dia[[col for col in df_dia.columns
                                      if col != target_name]])
        y = df_dia[target_name].map(lambda x: 0 if x == 'NO' else 1)
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.6,
                                                            random_state=42)

        model = 'logistic'
        res_dia = Parallel(n_jobs=n_jobs)(delayed(get_roc_exp_score)
                                          (X_train, X, y_train, y,
                                           model, dataset_name, epsilon,
                                           outdir, iter_number, illustration)
                                          for epsilon in epsilon_range)

        outpath = f'{outdir}/roc_exp_summary_{dataset_name}_{iter_number}'
        df_plot = pd.DataFrame.from_records(np.ravel(np.array(res_dia)))
        # df_plot = pd.DataFrame.from_records(res_dia)
        df_plot = df_plot.groupby('epsilon').agg(['mean', 'std']).reset_index()
        df_plot.to_latex(buf=f'{outpath}.tex', index=True)

        plt.errorbar(df_plot.epsilon, df_plot.geo_score_true_1norm['mean'],
                     yerr=df_plot.geo_score_true_1norm['std'],
                     label='smooth with 1norm', marker='o')
        plt.errorbar(df_plot.epsilon, df_plot.geo_score_true_2norm['mean'],
                     yerr=df_plot.geo_score_true_2norm['std'],
                     label='smooth with 2norm', marker='o')
        plt.grid()
        plt.yscale('log')
        plt.xlabel('Epsilon')
        plt.xscale('log')
        plt.ylabel('Area between true ROC and smooth DP ROC')
        plt.legend()
        plt.savefig(f'{outpath}.png', bbox_inches='tight')
        plt.close()

        outpath = f'{outdir}/roc_exp_summary_{dataset_name}.csv'
        # res = res_fam + res_ban + res_dia
        df_plot = pd.DataFrame.from_records(res_dia)
        df_plot.to_csv(outpath)


if __name__ == '__main__':
    main()
