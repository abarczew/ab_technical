import pandas as pd
from utils.smooth import smooth_dense, rv_index
import numpy as np
from datetime import datetime
from joblib import Parallel, delayed
from utils.dp_counter import generate_noise, generate_data_multinom
from experiments.inverse_ecdf.inv_experiment import bin_search
from itertools import product

n_jobs = 3
epsilon = 0.5
seed = 42
N_range = [1000, 10000, 100000]
n_range = [1000, 10000, 100000]
norm_range = [1, 2]
outdir = 'results'


def one_exp(N, n, norm):
    rng = np.random.default_rng(seed=seed)
    rvidx = rv_index(N)
    L = rvidx.L
    (eta, sumeta) = generate_noise(rng, epsilon/L, rvidx)
    data = generate_data_multinom(rng, N, n)
    for i in range(1, len(data)):  # make cumulative
        data[i] = data[i] + data[i-1]
    dp_data = data + sumeta
    data = data / n
    dp_data = dp_data / n
    start = datetime.now()
    (sm_data, _) = smooth_dense(dp_data, rvidx, norm)
    end = datetime.now()
    runtime_smooth = end - start
    test_len = 100000
    worklist = rng.integers(N, size=test_len)
    start = datetime.now()
    for i in range(test_len):
        dp_i = bin_search(dp_data, N, data[worklist[i]])
    end = datetime.now()
    runtime_bin_search_dp = end - start
    start = datetime.now()
    for i in range(test_len):
        sm_i = bin_search(sm_data, N, data[worklist[i]])
    end = datetime.now()
    runtime_bin_search_smooth = end - start
    print(runtime_smooth, ',\t', runtime_bin_search_dp, ',\t', runtime_bin_search_smooth)
    return {'N': N, 'n': n, 'epsilon': epsilon, 'norm': norm,
            'runtime_smooth': runtime_smooth,
            'runtime_bin_search_dp': runtime_bin_search_dp,
            'runtime_bin_search_smooth': runtime_bin_search_smooth}


def run():
    exp_list = product(N_range, n_range, norm_range)
    exp_list = []
    for N in [10000]:
        for n in [1000, 3000, 10000, 30000, 100000]:
            for norm in norm_range:
                exp_list.append((N, n, norm))
    for N in [1000, 3000, 30000, 100000]:
        for n in [10000]:
            for norm in norm_range:
                exp_list.append((N, n, norm))
    for params in exp_list:
        print(params)
    results = Parallel(n_jobs=n_jobs)(delayed(one_exp)
                                      (*params)
                                      for params in exp_list)
    df = pd.DataFrame.from_records(results)
    df.to_csv(f'{outdir}/runtime_exp_results.csv')


if __name__ == '__main__':
    run()
