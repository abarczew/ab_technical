import matplotlib.pyplot as plt
import pandas as pd
plt.style.use('seaborn-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)


def main():
    df = pd.read_csv('results/runtime_exp_results.csv')
    for col in [c for c in df.columns if 'runtime' in c]:
        df[col] = pd.to_timedelta(df[col]).map(lambda dt: dt.total_seconds())

    xlabels = ['N', 'n']
    for i, xlabel in enumerate(xlabels):
        outpath = f'results/runtime_exp_results_smooth_{xlabel}'
        df_plot = df.loc[df[xlabels[-(i+1)]] == 10000]
        for norm in [1, 2]:
            df_sm = df_plot.loc[df_plot.norm == norm]
            plt.plot(df_sm[xlabel], df_sm['runtime_smooth'],
                     label=f'smooth with {norm}norm', marker='o')
        plt.grid()
        plt.xlabel(f'{xlabel}')
        plt.ylabel('Seconds')
        plt.legend()
        plt.savefig(f'{outpath}.png', bbox_inches='tight')
        plt.close()

    for i, xlabel in enumerate(xlabels):
        outpath = f'results/runtime_exp_results_bin_{xlabel}'
        df_plot = df.loc[df[xlabels[-(i+1)]] == 10000]
        df_plot = df_plot.groupby(xlabel).mean().reset_index()
        for bin in ['runtime_bin_search_dp', 'runtime_bin_search_smooth']:
            plt.plot(df_plot[xlabel], df_plot[bin],
                     label=f'{bin}', marker='o')
        plt.grid()
        plt.xlabel(f'{xlabel}')
        plt.ylabel('Seconds')
        plt.legend()
        plt.savefig(f'{outpath}.png', bbox_inches='tight')
        plt.close()


if __name__ == '__main__':
    main()
