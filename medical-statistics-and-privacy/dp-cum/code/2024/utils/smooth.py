
import cvxopt
import numpy as np
import copy


# "
# Tools for random variables organized in a binary tree for continual DP


def calc_il_to_rv(N, L):
    """
    il_to_rv[i,l] provides a linear index for the l-th random variable
    to be added to \ecdf(\tau_i) to obtain \ecdfdp(\tau_i)

    \ecdfdp(\tau_i) = \ecdf(\tau_i) + \sum_{l=0}^{L-1} \eta_{il_to_rv[i,l]}

    il_to_rv[i,l] is a linear number, while in the text random variables
    are indexed by a pair (floor(i/2^l),l)
    """
    il_to_rv = np.ndarray([N, L], 'i')
    rv_cnt = -1
    for l in range(L):
        for i in range(N):
            if np.mod(i, 2**l) == 0:
                rv_cnt = rv_cnt + 1
            il_to_rv[i, l] = rv_cnt
    return (il_to_rv, rv_cnt+1)


def calc_rv_range_by_l(N, L):
    """
    rv_range_by_l[0] is the linear index of the pair-indexed rv_{0,l}
    """
    rv_range_by_l = np.ndarray([L+1])
    rv_range_by_l[0] = 0
    for l in range(L):
        rv_range_by_l[l+1] = rv_range_by_l[l] + int(np.ceil(N/(2**l)))
    return rv_range_by_l


class rv_index:
    """
    class to pre-compute all indexation tables and similar info for
    binary tree organized random variables
    """

    def __init__(self, N):
        self.N = N
        self.L = int(np.ceil(np.log(N)/np.log(2)))+1
        (self.il_to_rv, self.rv_cnt) = calc_il_to_rv(self.N, self.L)
        self.rv_range_by_l = calc_rv_range_by_l(self.N, self.L)


def binrv_print(il_to_rv, N, L, rv):
    """
    Printing binary tree organized random variables
    """
    for l in range(L):
        for i in range(int(np.ceil(N/(2**l)))):
            print(l, "\t", i*(2**l), "..", (i+1)*(2**l)-1, "\t", rv[il_to_rv[i, l]])


def binrv_print_spinv(rvidx, rv_sp, rv_dict):
    """
    Printing binary tree organized random variables
    Spinv: rv_dict is a dict with (key,val) where key is the RV index and val is the index in rv_sp where the value can be found.
    """
    for l in range(rvidx.L):
        for i in range(int(np.ceil(rvidx.N/(2**l)))):
            if(rvidx.il_to_rv[i, l] in rv_dict):
                print(l, "\t", i*(2**l), "..", (i+1)*(2**l)-1,
                      "\t", rv_sp[rv_dict[rvidx.il_to_rv[i, l]]])


def rv_spinv_to_sparse(rv_sp, rvs):
    rv_x = np.ndarray([len(rv_sp)], "i")
    rv_y = np.ndarray([len(rv_sp)], "d")
    i = 0
    for xy in sorted(rvs.items(), key=lambda kv: kv[1]):
        rv_x[i] = xy[0]
        rv_y[i] = rv_sp[xy[1]]
        i = i+1
    return (rv_x, rv_y)


########################################################################
# Smoothing


def smooth_norm_1(x, y, rvidx, rvs, nrv, minval=None, maxval=None):
    """
    returns smooth version of y
    assuming y is a complete vector with one value for every \tau_i.
    Uses 1-norm
    """

    range_constr = 0
    if not(minval == None):
        range_constr = range_constr + 1
    if not(maxval == None):
        range_constr = range_constr + 1

    N = rvidx.N
    L = rvidx.L
    il_to_rv = rvidx.il_to_rv
    rv_cnt = rvidx.rv_cnt
    """
    x variables:
    *    0 .. nrv-1  : xnu : increase needed to eta
    *  nrv .. 2nrv-1 : xmu : decrease needed to eta

    s variables:
    *    0 .. nrv-1  : snu : increase needed to eta
    *  nrv .. 2nrv-1 : smu : decrease needed to eta
    * 2nrv .. 3nrv-2 : Delta : (x_i+1-x_i)+nu_i+1-nu_i-mu_i+1+mu_i

    Gsh equalities: Gx + s = h with
    .  G = [ -I  0                    ]     h = [ 0           ]
    .      [  0 -I                    ]         [ 0           ]
    .      [  nu_i-nu_i+1-mu_i+mu_i+1 ]         [ y_i+1 - y_i ]
    .      [  nu_1-mu_1               ]         [ 0 ]
    -      [  mu_end-nu_end           ]         [ max ]

    Objective: sum_i nu_i + mu_i
    . c = [1 1]
    """
    c = cvxopt.matrix(1.0, (2*nrv, 1), 'd')
    G = cvxopt.spmatrix([], [], [], (2*nrv+len(x)-1+range_constr, 2*nrv), 'd')
    h = cvxopt.matrix(0, (2*nrv+len(x)-1+range_constr, 1), 'd')
    for i in range(2*nrv):
        G[i, i] = -1.0
    for i in range(len(x)-1):
        h[i+2*nrv] = y[i+1] - y[i]
        for l in range(L):
            if il_to_rv[x[i], l] != il_to_rv[x[i+1], l]:
                G[i+2*nrv, rvs[il_to_rv[x[i], l]]] = 1
                G[i+2*nrv, rvs[il_to_rv[x[i+1], l]]] = -1
                G[i+2*nrv, nrv+rvs[il_to_rv[x[i], l]]] = -1
                G[i+2*nrv, nrv+rvs[il_to_rv[x[i+1], l]]] = 1

    range_idx = 2*nrv+len(x)-1
    if not(minval == None):
        for l in range(L):
            G[range_idx, rvs[il_to_rv[x[0], l]]] = -1
            G[range_idx, nrv+rvs[il_to_rv[x[0], l]]] = 1
        h[range_idx] = -minval + y[0]
        range_idx = range_idx + 1
        print("minval:", minval, ",y[0]=", y[0])
    if not(maxval == None):
        for l in range(L):
            G[range_idx, rvs[il_to_rv[x[-1], l]]] = 1
            G[range_idx, nrv+rvs[il_to_rv[x[-1], l]]] = -1
        h[range_idx] = maxval - y[-1]
        range_idx = range_idx + 1
        print("maxval:", maxval)

    # print("G=",G)
    # print("h=",h)

    cvxopt.solvers.options["show_progress"] = 0
    res = cvxopt.solvers.lp(c, G, h)
    numu_corr_sp = np.array(res['x'])
    # print("numu_corr_sp=",numu_corr_sp)
    eta_corr_sp = cvxopt.matrix(0, (nrv, 1), 'd')
    for i in range(nrv):
        # print("i=",i)
        # ipdb.set_trace()
        eta_corr_sp[i] = numu_corr_sp[i] - numu_corr_sp[i+nrv]
    ys = copy.deepcopy(y)
    for i in range(len(x)):
        for l in range(L):
            ys[i] = ys[i] + eta_corr_sp[rvs[il_to_rv[x[i], l]], 0]
    return (ys, eta_corr_sp)


def smooth_norm_2(x, y, rvidx, rvs, nrv, minval=None, maxval=None):
    """
    returns smooth version of y
    assuming y is a complete vector with one value for every \tau_i.
    Uses 2-norm
    """
    range_constr = 0
    if not(minval == None):
        range_constr = range_constr + 1
    if not(maxval == None):
        range_constr = range_constr + 1

    N = rvidx.N
    L = rvidx.L
    il_to_rv = rvidx.il_to_rv
    rv_cnt = rvidx.rv_cnt
    P = cvxopt.spmatrix([], [], [], (nrv, nrv), 'd')
    for j in range(nrv):
        P[j, j] = 1
    q = cvxopt.matrix(0, (nrv, 1), 'd')
    h = cvxopt.matrix(0, (range_constr+len(x)-1, 1), 'd')
    G = cvxopt.spmatrix([], [], [], (range_constr+len(x)-1, nrv), 'd')
    for i in range(len(x)-1):
        h[i] = y[i+1] - y[i]
        for l in range(L):
            if il_to_rv[x[i], l] != il_to_rv[x[i+1], l]:
                G[i, rvs[il_to_rv[x[i], l]]] = 1
                G[i, rvs[il_to_rv[x[i+1], l]]] = -1

    range_idx = len(x)-1
    if not(minval == None):
        for l in range(L):
            G[range_idx, rvs[il_to_rv[x[0], l]]] = -1
        h[range_idx] = -minval + y[0]
        range_idx = range_idx + 1
        print("minval:", minval, ",y[0]=", y[0])
    if not(maxval == None):
        for l in range(L):
            G[range_idx, rvs[il_to_rv[x[-1], l]]] = 1
        h[range_idx] = maxval - y[-1]
        range_idx = range_idx + 1
        print("maxval:", maxval)

    cvxopt.solvers.options["show_progress"] = 0
    res = cvxopt.solvers.qp(P, q, G, h)
    eta_corr_sp = np.array(res['x'])
    ys = copy.deepcopy(y)
    for i in range(len(x)):
        for l in range(L):
            ys[i] = ys[i] + eta_corr_sp[rvs[il_to_rv[x[i], l]], 0]
    return (ys, eta_corr_sp)


def smooth(x, y, rvidx, rvs, nrv, norm=None, minval=None, maxval=None):
    """
    returns smooth version of y
    assuming y is a complete vector with one value for every \tau_i.
    """
    if norm == None:
        norm = 2
    if norm == 2:
        return smooth_norm_2(x, y, rvidx, rvs, nrv, minval, maxval)
    if norm != 1:
        raise "smooth: unsupported norm"
    return smooth_norm_1(x, y, rvidx, rvs, nrv, minval, maxval)


def smooth_spinv(x, y, rvidx, norm=None, minval=None, maxval=None):
    rvs = dict()  # (key,val) with key the index of a RV relevant for this
    # problem and val the row/column index where this RV will go in P and G
    nrv = 0      # number of random variables relevant for this problem
    for i in x:
        for l in range(rvidx.L):
            if(not(rvidx.il_to_rv[i, l] in rvs)):
                rvs[rvidx.il_to_rv[i, l]] = nrv
                nrv = nrv + 1
    (ys, eta_corr_sp) = smooth(x, y, rvidx, rvs, nrv, norm, minval, maxval)
    return (ys, rvs, eta_corr_sp)


def smooth_sparse(x, y, rvidx, norm=None, minval=None, maxval=None):
    (ys, rvs, eta_corr_sp) = smooth_spinv(x, y, rvidx, norm)
    (eta_corr_x, eta_corr_y) = rv_spinv_to_sparse(eta_corr_sp, rvs, minval, maxval)
    return (ys, eta_corr_x, eta_corr_y)


def smooth_dense(y, rvidx, norm=None, minval=None, maxval=None):
    nrv = rvidx.rv_cnt
    rvs = range(rvidx.rv_cnt)
    x = range(rvidx.N)
    (ys, eta_corr) = smooth(x, y, rvidx, rvs, nrv, norm, minval, maxval)
    return (ys, eta_corr)


def smooth_demo():
    #y = [1.0, 2.0, 5.0, 3.5, 4.0, 4.0, 8.0]
    y = [1.0, 3.0, 2.0]
    rng = np.random.default_rng(123)
    #y = rng.random(10)

    print("Test smooth_dense ------------------------")
    N = len(y)
    rvidx = rv_index(N)
    (ys, eta_corr) = smooth_dense(y, rvidx, 2, 1.5, 2.5)
    print("y=", y)
    print("ys=", ys)

    """
    print("Test smooth_spinv ------------------------")
    x = [1, 4, 5, 6, 7]
    y = [5.0, 4.0, 3.0, 2.0, 1.0]
    (ys, rv_dict, eta_corr_sp) = smooth_spinv(x, y, rvidx, 1)
    binrv_print_spinv(rvidx, eta_corr_sp, rv_dict)
    print("y=", y)
    print("ys=", ys)

    print("Test smooth_sparse ------------------------")
    (ys, eta_corr_x, eta_corr_y) = smooth_sparse(x, y, rvidx, 1)
    print("eta_corr_x=", eta_corr_x)
    print("eta_corr_y=", eta_corr_y)
    """


if __name__ == '__main__':
    smooth_demo()
