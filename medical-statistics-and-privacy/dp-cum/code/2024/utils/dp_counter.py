import numpy as np


def generate_noise(random_gen,
                   epsilon,
                   rvidx):
    """
    generate DP noise

    :param random_gen: the random number generator
    :param epsilon: the epsilon parameter of the Laplace distribution
    :param rvidx: an rv_index structure

    :return:
        - eta:
        - sum_eta:
    """
    rv_cnt = rvidx.rv_cnt
    N = rvidx.N
    L = rvidx.L
    eta = random_gen.laplace(0, 1/epsilon, size=rv_cnt)
    sum_eta = np.zeros(N)
    for i in range(N):
        for j in range(L):
            sum_eta[i] = sum_eta[i] + eta[rvidx.il_to_rv[i, j]]
    return eta, sum_eta


def generate_data_poisson(random_gen,
                          N,
                          poisson_lambda):
    """
    generate data according to the Poisson model:

    :param random_gen: the random number generator to be used
    :param N: the number of "time steps"
    :param poisson_lambda: poisson process parameter

    :return:
    """
    return random_gen.poisson(poisson_lambda, N)


def generate_data_multinom(random_gen,
                           N,
                           n):
    """
    generate data according to the multinomial model:

    :param random_gen: the random number generator to be used
    :param N: the number of "time steps"
    :param n: number of events to happen in the N time steps

    :return:
    """
    s = np.zeros(N)
    for i in range(n):
        j = random_gen.integers(N)
        s[j] = s[j] + 1
    return s
