from matplotlib import pyplot as plt
import seaborn as sns
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from time import time
from joblib import Parallel, delayed
import stopit
from pysrc.simulation_structures import *
from pysrc.DistributedComparisonFunctionForPParties import DistributedComparisonFunctionForPParties
from pysrc.utils import *
import numpy as np
import pandas as pd
from utils.dp_counter import generate_noise
from utils.smooth import rv_index, smooth_dense
import os
from timeit import default_timer as timer
import collections
from tinysmpc import VirtualMachine, PrivateScalar, SharedScalar
from tinysmpc.fixed_point import fixed_point
import sys
from pympler import asizeof
# import asizeof220630 as asizeof
# fss lib

# utils


plt.style.use('seaborn-v0_8-paper')
plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.Set1.colors)


def generate_synthetic_data(
    distribution: object,
    distribution_params: dict,
    size: int
):
    """
    Generate synthetic data with scipy.stat distribution objects
    """
    return distribution.rvs(size=size, **distribution_params)


def discretize(data, domain, number_bins):
    """
    Create the bins for the input precision and use the numpy function digitize on the data and the created bins.

    params :
        data (numpy.array) The data to discretize
        precision : Number of decimals to keep

    return :
        tuple of size 2, the bins, and the return of np.digitize(data,bins)
    """
    # Avoir un tuple range ex: (0,100) découpé en un nombre de bins
    # Domaine F --> transforme en bins
    alpha = (domain[1] - domain[0]) / number_bins
    bins = np.array([domain[0] + alpha * i for i in range(number_bins)])
    #bins = np.array([10 ** -precision * i for i in range(1,int(max(data) * 10 ** precision) + 1)])
    return bins, np.digitize(data, bins, right=True)


def parse_results(
    data: np.array,
    bins: np.array,
    method: str
):
    """
    Parse the results from the input data, wich is assumed to correspond to the binned representation of the data with bins

    return : (np.array) An approximation of the results in the default representation
    """
    if method == 'mean':  # Moyenne entre bin[i] et bin[-1] ; attention aux 0
        return (bins[data] + bins[data - 1]) / 2
    if method == 'sup':  # bin[i]
        return bins[data]


def compute_every_key(fss, server, N):
    """
    Make the server evaluate all his key with the given fss instance
    Made for parallel computing.
    """
    nb_keys = server.count
    res = np.zeros((nb_keys, N))
    for k in range(nb_keys):
        # implement the mapping function here, see if N need to be updated, I think N here refers to the number of thresholds instead of the domain size
        res[k] = server.evaluate(fss, k)
    return res


def fss_experiment(p: int,
                   do: int,
                   N: int,
                   q: int,
                   l: int,
                   data: np.array,
                   thresholds: np.array,
                   verbose: bool = False) -> np.array:
    """
    Experiment the distributed comparison function secret sharing scheme.
    Run the experiments using the fss parameters and the data to share into the data owners.

    The experiment use parallel computing with default parameter 'n_jobs' set to -1.

    The experiment follow the following scheme :

        * Creation of the differents parties (data owners, and servers)
        * Generation of the key by the data owners, wich are sent to the different servers
        * Evaluation of their own shares by the servers
        * Summing of the shares, summing all keys and then output the results as a dict

    params :
        p : (int) Number of servers wich compute the comparison function
        do : (int) Number of data owners wich generate the function's shares
        N : (int) Domain of the function (number of values in the dataset)
        q : (int) Domain of G, the output of the comparison function
        l : (int) Lambda, the security parameter in number of bits
        data : (numpy.array) The data to share between data owner
        thresholds : (numpy.array) Values to compare the domain with

    return :
     (dict) : A python dictionary with 4 entries :
        'output' : The output array of the function
        'time' : The compute time of the entire function
        'key_size' : The size of the keys serialized
        'key_size_deserialized' : The size of the keys deserialized

    """

    servers = []
    data_owners = []
    com_cost_setup = 0
    com_cost_eval = 0

    # instantiation of the FSS DCF scheme
    fss = DistributedComparisonFunctionForPParties(p=p, N=N, q=q, lambd=l)

    data_shares = np.array_split(data, 10)

    # Creation of the servers
    for _ in range(p):
        servers.append(Server())

    # Creation of the data owners
    for i in range(do):
        data_owners.append(DataOwner(data_shares[i], N=N, bins=thresholds))

    # Generation of keys and sending to the servers
    start_setup = timer()
    for i in range(do):
        set_of_keys = data_owners[i].generate(fss)
        for keys in set_of_keys:
            # com_cost_setup += asizeof.asizeof(keys)
            for k in range(p):
                servers[k].receive(keys[k])
                com_cost_setup += len(keys[k])
    end_setup = timer()
    # Servers evaluate all of the function's shares on the domain, sums the result and output it (maybe applicate differential privacy)

    # to measure the runtime, computation is done sequentially
    # res_list = Parallel(n_jobs=-1)(
    #     delayed(compute_every_key)(fss, servers[i], N) for i in range(p)
    # )

    start_eval = timer()
    res_list = []
    for i in range(p):
        com_cost_eval += asizeof.asizeof(fss.evaluate)
        answer = compute_every_key(fss, servers[i], N)
        # Servers sums their results to compute f(x_i)
        output = answer.sum(0) % q
        res_list.append(output)
        com_cost_eval += asizeof.asizeof(output)

    # Summing the results for each x_i in X
    output = np.array(res_list).sum(0)
    end_eval = timer()
    return output, end_setup - start_setup, end_eval - start_eval, com_cost_setup, com_cost_eval


def secure_agg_experiment_private_eval(p: int,
                   do: int,
                   N: int,
                   q: int,
                   l: int,
                   data: list,
                   thresholds: np.array,
                   verbose: bool = False) -> np.array:
    """
    Experiment the comparison function with secure multi-party computation (SMPC).
    Run the experiments using the SMPC parameters and the data to share into the data owners.

    The experiment use parallel computing with default parameter 'n_jobs' set to -1.

    The experiment follow the following scheme :

        * Creation of the differents parties (data owners, and servers)
        * Generation of shared encrypted data between pairs of servers
        * Evaluation of each encryted data point against each public values
        * Sum on each public values the number of points
        * Return ecdf on previous vector

    params :
        p : (int) Number of servers wich compute the comparison function
        do : (int) Number of data owners wich generate the function's shares
        N : (int) Domain of the function (number of values in the dataset)
        q : (int) Domain of G, the output of the comparison function
        l : (int) Lambda, the security parameter in number of bits
        data : (numpy.array) The data to share between data owner
        thresholds : (numpy.array) Values to compare the domain with

    return :
     ('output' : The output array of the function

    """
    # create p computing servers
    servers = {}
    for i in range(p):
        servers[str(i)] = VirtualMachine(f'server_{i}')
    
    # create do data owners servers
    dataowners = {}
    for i in range(do):
        dataowners[str(i)] = VirtualMachine(f'do_{i}')

    # check data type
    if (type(data[0]) is np.float64) or (type(data[0]) is float):
        data = list(map(fixed_point, data))
        thresholds = list(map(lambda x: fixed_point(float(x)), thresholds))
    # split private data
    data_shares = list(map(list, np.array_split(data, do)))
    data_private = collections.defaultdict(dict)
    for i, data in enumerate(data_shares):
        for j, scalar in enumerate(data):
            data_private[str(i)][str(j)] = PrivateScalar(scalar, dataowners[str(i)])

    start_eval = timer()
    com_cost = 0
    # dataowners send shared encrypted data to pairs of servers
    shared_data_private = collections.defaultdict(dict)
    for i in range(do):
        for j in data_private[str(i)].keys():
            shared_data_private[str(i)][j] = data_private[str(i)][j].share([servers[str((p-i)%p)], servers[str((p-i+1)%p)]])
            # compute communication cost
            # n_shared_data * 2 * size(data) # already two private scalar in shared_private_data object
            com_cost += asizeof.asizeof(shared_data_private[str(i)][j])

    # Servers evaluate all of the function's shares on the domain
    count = 0
    ecdf = []
    def evaluate_shared(x_sh, thresholds):
        eval_sh = []
        for n in thresholds:
            eval_sh.append(1 - (x_sh > n).value)
        return eval_sh

    res_list = []
    for i in range(do):
        # sequential computation for fair comparaison of runtime
        # res = Parallel(n_jobs=-1)(
        #     delayed(evaluate_shared)(shared_data_private[str(i)][j], thresholds) for j in data_private[str(i)].keys()
        # )
        res = [evaluate_shared(shared_data_private[str(i)][j], thresholds) for j in data_private[str(i)].keys()]
        res_list.append(res)
        # for one dataownoer, every server must be queried N times
        com_cost += asizeof.asizeof(evaluate_shared) * len(thresholds) * p
        com_cost += asizeof.asizeof(list(map(list, res)))

    ecdf = np.concatenate(res_list).sum(0)
    end_eval = timer()
    return ecdf, 0, end_eval - start_eval, 0, com_cost
    

def secure_agg_experiment(p: int,
                   do: int,
                   N: int,
                   q: int,
                   l: int,
                   data: list,
                   thresholds: np.array,
                   verbose: bool = False) -> np.array:
    """
    Experiment the comparison function with secure multi-party computation (SMPC).
    Run the experiments using the SMPC parameters and the data to share into the data owners.

    The experiment use parallel computing with default parameter 'n_jobs' set to -1.

    The experiment follow the following scheme :

        * Creation of the differents parties (data owners, and servers)
        * Generation of shared encrypted data between pairs of servers
        * Evaluation of each encryted data point against each public values
        * Sum on each public values the number of points
        * Return ecdf on previous vector

    params :
        p : (int) Number of servers wich compute the comparison function
        do : (int) Number of data owners wich generate the function's shares
        N : (int) Domain of the function (number of values in the dataset)
        q : (int) Domain of G, the output of the comparison function
        l : (int) Lambda, the security parameter in number of bits
        data : (numpy.array) The data to share between data owner
        thresholds : (numpy.array) Values to compare the domain with

    return :
     ('output' : The output array of the function

    """
    # create p computing servers
    servers = {}
    for i in range(p):
        servers[str(i)] = VirtualMachine(f'server_{i}')
    
    # create do data owners servers
    dataowners = {}
    for i in range(do):
        dataowners[str(i)] = VirtualMachine(f'do_{i}')

    # check data type
    if (type(data[0]) is np.float64) or (type(data[0]) is float):
        data = list(map(fixed_point, data))
        thresholds = list(map(lambda x: fixed_point(float(x)), thresholds))
    # split private data
    data_shares = list(map(list, np.array_split(data, 10)))
    data_private = collections.defaultdict(dict)
    for i, data in enumerate(data_shares[:do]):
        for j, scalar in enumerate(data):
            data_private[str(i)][str(j)] = PrivateScalar(scalar, dataowners[str(i)])

    # dataowners send shared encrypted data to pairs of servers
    # shared_data_private = collections.defaultdict(dict)
    # for i in range(do):
    #     for j in data_private[str(i)].keys():
    #         shared_data_private[str(i)][j] = data_private[str(i)][j].share([servers[str((p-i)%p)], servers[str((p-i+1)%p)]])
    #         # compute communication cost
    #         # n_shared_data * 2 * size(data) # already two private scalar in shared_private_data object
    #         com_cost += asizeof.asizeof(shared_data_private[str(i)][j])

    # Servers evaluate all of the function's shares on the domain
    com_cost_setup = 0
    com_cost_eval = 0
    def eval(dataowner, threshold):
        count = 0
        objects = dataowner.objects
        for d in objects:
            count += int(d.value < threshold)
        return PrivateScalar(count, dataowner)
    start_eval = timer()
    # compare and share answer to servers
    shared_answer = collections.defaultdict(dict)
    for j, threshold in enumerate(thresholds):
        for i, dataowner in dataowners.items():
            # evaluation function and threshold are sent to do
            com_cost_eval += asizeof.asizeof(eval)
            com_cost_eval += asizeof.asizeof(threshold)
            # evaluation is performed by dataowner
            answer = eval(dataowner, threshold)
            # answer is shared to all servers
            shared_answer[i][str(j)] = answer.share([server for server in servers.values()])
            for share in shared_answer[i][str(j)].shares:
                com_cost_eval += asizeof.asizeof(share.value)

    # aggregate results on 1 server
    def share_result(server):
        return list(map(lambda x: x.value, server.objects))
    res_list = []
    for server in servers.values():
        com_cost_eval += asizeof.asizeof(share_result)
        res = share_result(server)
        res = np.array(res).reshape((-1, do)).sum(1)
        com_cost_eval += asizeof.asizeof(res)
        res_list.append(res)

    ecdf = np.array(res_list).sum(0)
    end_eval = timer()
    return ecdf, 0, end_eval - start_eval, com_cost_setup, com_cost_eval


def agg_experiment(p: int,
                   do: int,
                   N: int,
                   q: int,
                   l: int,
                   data: np.array,
                   thresholds: np.array,
                   verbose: bool = False) -> np.array:

    servers = []
    data_owners = []

    data_shares = np.array_split(data, do)

    agg = DistributedPlainSharing(thresholds)

    # Creation of the servers
    for _ in range(p):
        servers.append(Server())

    # Creation of the data owners
    for i in range(do):
        data_owners.append(DataOwner(data_shares[i], N=N, bins=thresholds))

    if p <= do:
        dataowner_server_idx = np.array_split(np.arange(do), p)
    else:
        dataowner_server_idx = [[i] for i in range(do)]

    for i, dataowner_idx in enumerate(dataowner_server_idx):
        for j in dataowner_idx:
            servers[i].receive(data_owners[j])

    # Servers evaluate all shares on the domain, sums the result and output it
    res_list = Parallel(n_jobs=-1)(
        delayed(compute_every_key)(agg, servers[i], N) for i in range(p)
    )

    # Servers sums their results to compute f(x_i)

    outputs = res_list[0]
    for res in res_list[1:]:
        outputs = (outputs + res) % q

    # Summing the results for each x_i in X

    output = np.sum(outputs, axis=0)

    return output


def transform_eval_input(data, N):
    if data.min() < 0:
        data = data - data.min()
    return data * N / data.max()


def add_roc_dp(fpr_numerator, tpr_numerator,
               epsilon, smooth_norm=None, seed=42):
    N = len(tpr_numerator)
    rvidx = rv_index(N)
    scale = epsilon / rvidx.L
    rng = np.random.default_rng(seed)
    _, sumeta_fpr = generate_noise(rng, scale, rvidx)
    _, sumeta_tpr = generate_noise(rng, scale, rvidx)
    fpr = fpr_numerator + sumeta_fpr / fpr_numerator.max()
    tpr = tpr_numerator + sumeta_tpr / tpr_numerator.max()
    if smooth_norm is not None:
        fpr, _ = smooth_dense(fpr, rvidx, smooth_norm, 0, 1)
        tpr, _ = smooth_dense(tpr, rvidx, smooth_norm, 0, 1)
    return fpr, tpr


def get_experiment_roc(experiment, p, do, N, q, l,
                        ground_truth, data, precision=1e6,
                        epsilon=None, seed=42, smooth_norm=None, pos_label=1):
    data_f = data[ground_truth != pos_label]
    data_t = data[ground_truth == pos_label]

    data_f_input = transform_eval_input(data_f, precision)
    data_t_input = transform_eval_input(data_t, precision)

    thresholds = np.linspace(0, precision, N)

    data_f_results, f_setup_time, f_eval_time, f_com_cost_setup, f_com_cost_eval = experiment(p=p, do=do, N=N, q=q, l=l, data=data_f_input,
                                thresholds=thresholds)
    data_t_results, t_setup_time, t_eval_time, t_com_cost_setup, t_com_cost_eval = experiment(p=p, do=do, N=N, q=q, l=l, data=data_t_input,
                                thresholds=thresholds)

    if epsilon is not None:
        fpr, tpr = add_roc_dp(data_f_results,
                              data_t_results,
                              epsilon,
                              smooth_norm,
                              seed)
    else:
        fpr = data_f_results / data_f_results.max()
        tpr = data_t_results / data_t_results.max()

    fpr = np.r_[0, fpr, 1]
    tpr = np.r_[0, tpr, 1]

    return fpr, tpr, f_setup_time + t_setup_time, f_eval_time + t_eval_time, f_com_cost_setup + t_com_cost_setup, f_com_cost_eval + t_com_cost_eval
    # return fpr, tpr, f_setup_time, f_eval_time, f_com_cost_setup, f_com_cost_eval


def auc(x: np.array,
        y: np.array):
    """
    Compute Area Under the Curve (AUC) using the trapezoidal rule.

    :param x : ndarray of shape (n,)
        x coordinates. These must be either monotonic increasing or monotonic
        decreasing.
    :param y : ndarray of shape, (n,)
        y coordinates.

    :return: float auc:
    """
    area = np.trapz(y, x)
    return area


def plot_roc(fpr, tpr):
    roc_auc = auc(fpr, tpr)

    import matplotlib.pyplot as plt
    plt.style.use('ggplot')

    fig, ax = plt.subplots()

    label = f"(AUC = {roc_auc:0.2f})"

    xlabel = "False Positive Rate"
    ylabel = "True Positive Rate"
    ax.set(xlabel=xlabel, ylabel=ylabel)

    ax.legend(loc="lower right")

    ax.plot(fpr, tpr, label=label)
    plt.show()


def get_pred(dataset_name='framingham', datadir='code/2024/data'):
    datadir = '/home/abarczew/ab_technical/medical-statistics-and-privacy/dp-cum/code/2024/data'
    df_fram = pd.read_csv(f'{datadir}/{dataset_name}.csv').fillna(0)
    X = df_fram[[col for col in df_fram.columns if col != 'TenYearCHD']]
    y = df_fram.TenYearCHD

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33,
                                                        random_state=42)

    classif = LogisticRegression(solver='lbfgs')
    classif = make_pipeline(StandardScaler(with_mean=False),
                            classif)
    classif.fit(X_train, y_train)
    y_pred = classif.decision_function(X_test)

    return y_test, y_pred


def sample_roc(fpr, tpr, N, fpr_base, tpr_base, thresholds):
    sample_idx = np.random.choice(np.arange(len(tpr)),
                                  np.min([len(thresholds), N])-2,
                                  replace=False)
    if len(thresholds) <= N:
        fpr = np.r_[0, fpr[sample_idx], 1]
        tpr = np.r_[0, tpr[sample_idx], 1]
    else:
        fpr_base = np.r_[0, fpr_base[sample_idx], 1]
        tpr_base = np.r_[0, tpr_base[sample_idx], 1]
    return fpr, tpr, fpr_base, tpr_base


def get_geo_score(x_f,
                  y_f,
                  x_g,
                  y_g):
    """
    geo_score: ?
    :param x_f: array
        x coordinates of the f line to draw
    :param y_f: array
        y coordinates of the f line to draw
    :param x_g: array
        x coordinates of the g line to draw, g has same length as f
    :param y_g: array
        y coordinates of the g line to draw, g has same length as f

    :return float geo_score:
        sum of areas between f and g
    """
    idx = np.argwhere(np.diff(np.sign(y_f - y_g))).flatten()
    # make sure we get the last area
    idx = np.r_[idx, len(x_f)-1]
    # compute the difference of areas between each interception
    geo_score = 0
    for i, j in list(zip(idx[:-1], idx[1:])):
        area_f = np.trapz(y_f[i:j+1], x_f[i:j+1])
        area_g = np.trapz(y_g[i:j+1], x_g[i:j+1])
        geo_score += abs(area_f - area_g)
    return geo_score


def ecdf(data, N):
    ecdf_results = []
    for i in np.linspace(data.min(), data.max(), N):
        ecdf_results.append(len(data[data <= i]))
    return np.array(ecdf_results)


def get_centralized_roc(y_test, y_pred, N):
    fpr = ecdf(y_pred[y_test != 1], N)
    tpr = ecdf(y_pred[y_test == 1], N)

    fpr = np.r_[0, fpr / fpr.max(), 1]
    tpr = np.r_[0, tpr / tpr.max(), 1]

    return fpr, tpr


def main():
    directory = "code/2024/results/roc"
    if not os.path.exists(directory):
        os.makedirs(directory)

    y_test, y_pred = get_pred()

    max_time = 7200 # time out after 2 hours
    
    # # parties and data owners
    # q = 2
    # l = 10
    # N = 100
    # parties_range = [2, 5, 10]
    # do_range = [2, 5, 10, 100]
    # records = []
    # start_central = timer()
    # fpr_base, tpr_base = get_centralized_roc(y_test, y_pred, N)
    # end_central = timer()
    # central_time = end_central - start_central
    # outpath = f"{directory}/parties_do_runtime_vs_agg_cost"
    # for p in parties_range:
    #     for do in do_range:
    #         with stopit.ThreadingTimeout(max_time) as context_manager:
    #             start_distributed = timer()
    #             fpr, tpr, com_cost_fss = get_experiment_roc(fss_experiment, p, do, N, q, l, y_test, y_pred)
    #             end_distributed = timer()
    #         if context_manager.state == context_manager.EXECUTED:
    #             start_agg = timer()
    #             fpr_agg, tpr_agg, com_cost_agg = get_experiment_roc(secure_agg_experiment, p, do, N, q, l, y_test, y_pred)
    #             end_agg = timer()
    #             geo_score = get_geo_score(fpr, tpr, fpr_base, tpr_base)
    #             dist_time = end_distributed - start_distributed
    #             agg_time = end_agg - start_agg
    #         elif context_manager.state == context_manager.TIMED_OUT:
    #             geo_score = None
    #             dist_time = f'timed_out (>{max_time//360}h)'
    #         record = {'N': N, 'l': l, 'q': q, 'p': p, 'do': do, 'dist_time': dist_time, 'agg_time': agg_time,
    #                     'central_time': central_time, 'geo_score': geo_score, 
    #                     'com_cost_agg': com_cost_agg, 'com_cost_fss': com_cost_fss}
    #         print(record)
    #         records.append(record)
    
    # df = pd.DataFrame.from_records(records)
    # df.to_csv(f'{outpath}.csv')

    # df_plot = pd.pivot_table(df, values='dist_time', index=['p'],
    #                          columns=['do'], aggfunc="mean")

    # sns.heatmap(df_plot, annot=True)
    # plt.savefig(outpath)
    # plt.close()

    # # servers
    # q = 2
    # l = 5
    # N = 100
    # do = 7
    # parties_range = list(np.arange(2, 9))
    # records = []
    # start_central = timer()
    # fpr_base, tpr_base = get_centralized_roc(y_test, y_pred, N)
    # end_central = timer()
    # central_time = end_central - start_central
    # outpath = f"{directory}/servers_runtime_vs_agg_cost"
    # for p in parties_range:
    #     with stopit.ThreadingTimeout(max_time) as context_manager:
    #         fpr, tpr, dist_setup_time, dist_eval_time, com_cost_fss_setup, com_cost_fss_eval = get_experiment_roc(fss_experiment, p, do, N, q, l, y_test, y_pred)
    #     if context_manager.state == context_manager.EXECUTED:
    #         fpr_agg, tpr_agg, agg_setup_time, agg_eval_time, com_cost_agg_setup, com_cost_agg_eval = get_experiment_roc(secure_agg_experiment, p, do, N, q, l, y_test, y_pred)
    #         geo_score = get_geo_score(fpr, tpr, fpr_base, tpr_base)
    #     elif context_manager.state == context_manager.TIMED_OUT:
    #         geo_score = None
    #         dist_eval_time = f'timed_out (>{max_time//3600}h)'
    #     record = {'N': N, 'l': l, 'q': q, 'p': p, 'do': do, 
    #                 'dist_setup_time': dist_setup_time, 'dist_eval_time': dist_eval_time,
    #                 'agg_setup_time': agg_setup_time, 'agg_eval_time': agg_eval_time, 
    #                 'central_time': central_time, 'geo_score': geo_score,
    #                 'com_cost_agg_setup': com_cost_agg_setup, 'com_cost_fss_setup': com_cost_fss_setup,
    #                 'com_cost_agg_eval': com_cost_agg_eval, 'com_cost_fss_eval': com_cost_fss_eval}
    #     print(record)
    #     records.append(record)
    
    # df = pd.DataFrame.from_records(records)
    # df.to_csv(f'{outpath}.csv')
    # # df = pd.read_csv(f'{outpath}.csv')
    # # # df = df.iloc[:-1]

    # df['dist_time'] = df['dist_setup_time'] + df['dist_eval_time']
    # df['agg_time'] = df['agg_setup_time'] + df['agg_eval_time']
    # df_plot = df[['p', 'dist_time', 'agg_time', 'central_time']].groupby('p').median().reset_index()
    # # outpath = f"{directory}/servers_runtime_vs_agg_cost"
    # # df = pd.read_csv(f'{outpath}.csv')
    # # df_plot['agg_eval_time'] = df[['do', 'agg_eval_time']].groupby('do').median().reset_index()['agg_eval_time']
    # outpath = f"{directory}/servers_runtime_vs_agg"
    # plt.plot(df_plot['p'], df_plot['dist_time'], label='FSS')
    # plt.plot(df_plot['p'], df_plot['agg_time'], label='Secure Aggregation')
    # plt.plot(df_plot['p'], df_plot['central_time'], label='Central Computation')
    # plt.yscale('log')
    # plt.legend()
    # plt.savefig(f'{outpath}.png')
    # plt.close()

    # # df = pd.read_csv(f'{outpath}.csv')
    # outpath = f"{directory}/servers_cost_tot_vs_agg"
    # df['com_cost_agg'] = df['com_cost_agg_setup'] + df['com_cost_agg_eval']
    # df['com_cost_fss'] = df['com_cost_fss_setup'] + df['com_cost_fss_eval']
    # df_plot = df[['p', 'com_cost_agg', 'com_cost_fss']].groupby('p').median().reset_index()
    # plt.plot(df_plot['p'], df_plot['com_cost_fss'], label='FSS')
    # plt.plot(df_plot['p'], df_plot['com_cost_agg'], label='Secure Aggregation')
    # plt.yscale('log')
    # plt.legend()
    # plt.savefig(f'{outpath}.png')
    # plt.close()


    # dataowner
    q = 2
    l = 5
    N = 547 # 87, 4
    p = 5
    do_range = list(np.arange(2, 9))
    records = []
    start_central = timer()
    fpr_base, tpr_base = get_centralized_roc(y_test, y_pred, N)
    end_central = timer()
    central_time = end_central - start_central
    outpath = f"{directory}/do_runtime_vs_agg_cost"
    for do in do_range:
        with stopit.ThreadingTimeout(max_time) as context_manager:
            fpr, tpr, dist_setup_time, dist_eval_time, com_cost_fss_setup, com_cost_fss_eval = get_experiment_roc(fss_experiment, p, do, N, q, l, y_test, y_pred)
        if context_manager.state == context_manager.EXECUTED:
            fpr_agg, tpr_agg, agg_setup_time, agg_eval_time, com_cost_agg_setup, com_cost_agg_eval = get_experiment_roc(secure_agg_experiment, p, do, N, q, l, y_test, y_pred)
            geo_score = get_geo_score(fpr, tpr, fpr_base, tpr_base)
        elif context_manager.state == context_manager.TIMED_OUT:
            geo_score = None
            dist_eval_time = f'timed_out (>{max_time//3600}h)'
        record = {'N': N, 'l': l, 'q': q, 'p': p, 'do': do, 
                    'dist_setup_time': dist_setup_time, 'dist_eval_time': dist_eval_time,
                    'agg_setup_time': agg_setup_time, 'agg_eval_time': agg_eval_time, 
                    'central_time': central_time, 'geo_score': geo_score,
                    'com_cost_agg_setup': com_cost_agg_setup, 'com_cost_fss_setup': com_cost_fss_setup,
                    'com_cost_agg_eval': com_cost_agg_eval, 'com_cost_fss_eval': com_cost_fss_eval}
        print(record)
        records.append(record)
    
    df = pd.DataFrame.from_records(records)
    df.to_csv(f'{outpath}.csv')
    # df = pd.read_csv(f'{outpath}.csv')
    # df = df.iloc[:-1]

    outpath = f"{directory}/do_runtime_vs_agg"
    df['dist_time'] = df['dist_setup_time'] + df['dist_eval_time']
    df['agg_time'] = df['agg_setup_time'] + df['agg_eval_time']
    df_plot = df[['do', 'dist_time', 'agg_time', 'central_time']].groupby('do').median().reset_index()
    plt.plot(df_plot['do'], df_plot['dist_time'], label='FSS')
    plt.plot(df_plot['do'], df_plot['agg_time'], label='Secure Aggregation')
    plt.plot(df_plot['do'], df_plot['central_time'], label='Central Computation')
    # plt.yscale('log')
    plt.legend()
    plt.savefig(f'{outpath}.png')
    plt.close()

    # df = pd.read_csv(f'{outpath}.csv')
    outpath = f"{directory}/do_cost_tot_vs_agg"
    df['com_cost_agg'] = df['com_cost_agg_setup'] + df['com_cost_agg_eval']
    df['com_cost_fss'] = df['com_cost_fss_setup'] + df['com_cost_fss_eval']
    df_plot = df[['do', 'com_cost_agg', 'com_cost_fss']].groupby('do').median().reset_index()
    plt.plot(df_plot['do'], df_plot['com_cost_fss'], label='FSS')
    plt.plot(df_plot['do'], df_plot['com_cost_agg'], label='Secure Aggregation')
    plt.yscale('log')
    plt.legend()
    plt.savefig(f'{outpath}.png')
    plt.close()
    
    # N points
    p = 5
    do = 7
    q = 2
    l = 5
    N_range = [50, 100, 1000, 2000] # 10,  + [(i+1)*1000 for i in range(10)]
    records = []
    outpath = f"{directory}/n_runtime_vs_agg_cost"
    for N in N_range:
        start_central = timer()
        fpr_base, tpr_base = get_centralized_roc(y_test, y_pred, N)
        end_central = timer()
        central_time = end_central - start_central
        with stopit.ThreadingTimeout(max_time) as context_manager:
            fpr, tpr, dist_setup_time, dist_eval_time, com_cost_fss_setup, com_cost_fss_eval = get_experiment_roc(fss_experiment, p, do, N, q, l, y_test, y_pred)
        if context_manager.state == context_manager.EXECUTED:
            fpr_agg, tpr_agg, agg_setup_time, agg_eval_time, com_cost_agg_setup, com_cost_agg_eval = get_experiment_roc(secure_agg_experiment, p, do, N, q, l, y_test, y_pred)
            geo_score = get_geo_score(fpr, tpr, fpr_base, tpr_base)
        elif context_manager.state == context_manager.TIMED_OUT:
            geo_score = None
            dist_eval_time = f'timed_out (>{max_time//3600}h)'
        record = {'N': N, 'l': l, 'q': q, 'p': p, 'do': do, 
                    'dist_setup_time': dist_setup_time, 'dist_eval_time': dist_eval_time,
                    'agg_setup_time': agg_setup_time, 'agg_eval_time': agg_eval_time, 
                    'central_time': central_time, 'geo_score': geo_score,
                    'com_cost_agg_setup': com_cost_agg_setup, 'com_cost_fss_setup': com_cost_fss_setup,
                    'com_cost_agg_eval': com_cost_agg_eval, 'com_cost_fss_eval': com_cost_fss_eval}
        print(record)
        records.append(record)
    
    df = pd.DataFrame.from_records(records)
    df.to_csv(f'{outpath}.csv')
    # df = pd.read_csv(f'{outpath}.csv')

    outpath = f"{directory}/n_runtime_vs_agg"
    df['dist_time'] = df['dist_setup_time'] + df['dist_eval_time']
    df['agg_time'] = df['agg_setup_time'] + df['agg_eval_time']
    plt.plot(df['N'], df['dist_time'], label='FSS')
    plt.plot(df['N'], df['agg_time'], label='Secure Aggregation')
    plt.plot(df['N'], df['central_time'], label='Central Computation')
    plt.yscale('log')
    plt.legend()
    plt.savefig(f'{outpath}.png')
    plt.close()    

    outpath = f"{directory}/n_cost_tot_vs_agg"
    df['com_cost_agg'] = df['com_cost_agg_setup'] + df['com_cost_agg_eval']
    df['com_cost_fss'] = df['com_cost_fss_setup'] + df['com_cost_fss_eval']
    plt.plot(df['N'], df['com_cost_fss'], label='FSS')
    plt.plot(df['N'], df['com_cost_agg'], label='Secure Aggregation')
    plt.yscale('log')
    plt.legend()
    plt.savefig(f'{outpath}.png')
    plt.close()  


if __name__ == '__main__':
    # data = [0., 2., 3.2]
    # N = 4
    # q = 2
    # l = 10
    # thresholds=[0, 1, 2, 3]
    # print(secure_agg_experiment(2, 2, N, 1, 3, data=data, thresholds=[0, 1, 2, 3]))
    # print(fss_experiment(2, 2, 2, q, l, data=data, thresholds=[0, 1]))
    # print(ecdf(np.array(data), N))
    main()
    # n_runtimes with levels of p