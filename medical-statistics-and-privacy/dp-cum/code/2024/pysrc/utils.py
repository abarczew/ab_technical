import numpy as np
import secrets


# Utilities
    
def binary_array_to_int(arr):
    """
    Returns the integer corresponding to the binary number in the binary numpy array (least significant bit to the right).
    e.g. np.array([0, 1, 0, 1]) ---> 5
    """
    return arr.dot(1 << np.arange(arr.size-1, -1, -1))


def int_to_binary_array(i, nbits):
    """
    Returns a numpy array of 'nbits' bits encoding the number 'i' (least significant bit to the right).
    e.g. i=5, nbits=5 ---> np.array([0, 0, 1, 0, 1])
    """
    return np.array(list(np.binary_repr(i, nbits))).astype(int)


def serialize(key_array):
    """
    Serializes the binary numpy array of the key into bytes.
    """
    return np.packbits(key_array.astype(bool)).tobytes()


def deserialize(encoded_key):
    """
    Deserializes the bytes into a numpy array of 0s and 1s.
    Warning: there will be trailing zeros at the end of the output array if the original key length was not a multiple of 8 bits.
    """
    return np.unpackbits(np.frombuffer(encoded_key, dtype=np.uint8))


def shuffle_columns(arr):
    """
    Returns the input array with a random permutation of its columns.
    """
    return arr[:,np.random.permutation(arr.shape[-1])]


def generate_random_2D_array(upper_bound, shape):
    """
    Returns a 2D numpy array with shape 'shape' and with (cryptographically secure) random entries between 0 and 'upper_bound'.
    """
    n = np.prod(shape)
    arr = np.zeros(n, dtype=int)
    for i in range(n):
        arr[i] = secrets.randbelow(upper_bound)
    return arr.reshape(shape)


def generate_random_2D_array_with_condition(upper_bound, shape, sum_of_rows):
    """
    Returns a 2D numpy array with shape 'shape' and with (cryptographically secure) random entries between 0 and 'upper_bound' that satisfy a condition:
    the sum of the rows must be equal to the array/list 'sum_of_rows'.
    """
    arr = np.zeros(shape, dtype=int)
    arr[:-1] = generate_random_2D_array(upper_bound, shape=(shape[0]-1, shape[1]))
    arr[-1] = (- np.sum(arr[:-1], axis=0) + sum_of_rows) % upper_bound
    return arr


def get_step_array(size, step_index, value):
    """
    Returns a 1D numpy array of 'size' elements, with 0s before step_index, and 'value' at and everywhere after step_index.
    """
    arr = np.ones(shape=size, dtype=int)
    arr *= value
    arr[:step_index] = 0
    return arr


def pseudorandom_generator(x, group_order, dim):
    """
    What they refer to as a 'pseudorandom generator' in the Function Secret Sharing paper is in fact a one-way function.
    To each x, the prg assigns a 'random' element of a group G. The order/cardinal of G is 'group-order'.
    The function returns 'dim' such elements in a numpy array.
    """
    rng = np.random.default_rng(seed=x)
    return rng.integers(group_order, size=dim)


