import numpy as np
from pysrc.FunctionSecretSharing import FunctionSecretSharing
from pysrc.utils import *


class DistributedComparisonFunctionForPParties(FunctionSecretSharing):
    """
    Implementation of Distributed Comparison Functions for p (≥2) parties.
    Implementation of the Algorithms 7 and 8 from the article Function Secret Sharing by E. Boyle et al.

    Note that a more efficient set of algorithms exists for p=2 (Algorithms 5 and 6 in the same paper).

    Notations:
    • Function to be secret shared:
        𝑓: {0,…,N-1} ⟶ 𝔾
        𝑓_{a,g}(x)  =   g if x ≥ a,
                        0 otherwise
    where  𝔾  is an abelian group. In what follows, 𝔾=ℤ_q with addition.
    • p is the number of parties/servers.
    • N is the size of the domain of 𝑓.
    • n is the number of bits needed to encode elements of {0,…,N-1}.
    • q is the size/order of 𝔾: |𝔾|=q.
    • m is the number of bits needed to encode elements of 𝔾=ℤ_q.
    • lambd or l (𝜆 in the paper) is the bit-length of the seeds.
    """

    # CONSTRUCTOR

    def __init__(self, p, N, q, lambd=128):
        """
        Params:
            p (int): number of parties/servers.

            N (int): size of the domain of the function.

            q (int): size of the group 𝔾.

            lambd (int): security parameter 𝜆 in the paper, bit-length of the seeds.
            Default value is 128.
        """
        super().__init__(p, N, q)

        self._lambd = lambd

        self._mu = self._compute_mu()
        self._nu = self._compute_nu()

    # ATTRIBUTES
    # Attributes should not be modified after instantiation.

    @property
    def lambd(self):
        return self._lambd

    @lambd.setter
    def lambd(self, value):
        raise Exception('Attributes should not be changed after instantiation.')

    def _compute_mu(self):
        """
        Used in line 2 of Algo. 7 and 8.
        Compute µ, the width of the table.
        """
        return int(np.ceil(2**(self.n/2) * self.q**((self.p-1)/2)))

    def _compute_nu(self):
        """
        Used in line 2 of Algo. 7 and 8.
        Compute ν, the height of the table.
        """
        return int(np.ceil(2**self.n/self.mu))

    @property
    def mu(self):
        return self._mu

    @mu.setter
    def mu(self, value):
        raise Exception('Attributes should not be changed after instantiation.')

    @property
    def nu(self):
        return self._nu

    @nu.setter
    def nu(self, value):
        raise Exception('Attributes should not be changed after instantiation.')

    # METHODS

    def _get_coords_in_grid(self, x):
        """
        Used in line 3 of Algo. 7 and 8.
        Return the coordinates of x in the ν * µ table.

        Params:
            x (int ∈ {0,...,N-1}): number of which we want to compute the coordinates.

        Returns:
            gamma (int): first coordinate.

            delta (int): second coordinate.
        """
        gamma = x // self.mu
        delta = x % self.mu
        return gamma, delta

    def _get_generic_Opq_and_Epq(self):
        """
        Used in the _get_As_for_gen method that follows.
        Return an (generic) example of O_{p,q} and E_{p,q} as they are described in the paper:
        "Let E_{p,q} (O_{p,q}) be the set of all (p, q**(p-1)) arrays over the set
        {0,...,q-1} such that the sum of elements in every column is 0 modulo q
        (1 modulo q) and every column appears exactly once in the array."
        """
        # For readability
        p = self.p
        q = self.q

        # Create the generic O_{p,q} and E_{p,q} structure
        O_arr = np.zeros((p, q**(p-1)), dtype=int)

        # Fill the p-1 first lines with all possible columns made of elements in {0,...,q-1}
        for i in range(p-1):
            O_arr[i] = np.tile(np.repeat(np.arange(q), q**i), q**(p-i-2)).astype(int)
        E_arr = np.copy(O_arr)

        # Fill the last row to have: sum on each column in E_{p,q} equal to 0 mod q, and 1 in O_{p,q}
        sums_on_columns = np.sum(O_arr[:-1], axis=0)
        O_arr[-1] = (1 - sums_on_columns) % q
        E_arr[-1] = - sums_on_columns % q

        return O_arr, E_arr

    def _get_As_for_gen(self, gamma):
        """
        Used in line 4 of Algo. 7.
        Return the appropriate arrays A.

        Params:
            gamma (int): first coordinate of a given number in the ν * µ table.

        Returns:
            As ((nu, p, q**(p-1)) numpy array of int ∈ {0,...,q-1}): the desired arrays, returned together in a single array.
        """
        # For readability
        p = self.p
        q = self.q
        nu = self.nu

        O_arr, E_arr = self._get_generic_Opq_and_Epq()
        # Sample from O_{p,q} or E_{p,q}, i.e. just take a columnwise permutation of O_arr or E_arr
        As = np.zeros((nu, p, q**(p-1)), dtype=int)
        for i in range(nu):
            if i == gamma:
                As[i] = shuffle_columns(O_arr)
            else:
                As[i] = shuffle_columns(E_arr)

        return As

    def _construct_keys_for_gen(self, seeds, As, vs, cws):
        """
        Corresponds to lines 8 to 11 of Algo. 7.
        Combines the seeds, arrays A, values v and correction words cw approriately to create the keys for the parties.

        Params:
            seeds ((nu, q**(p-1), l) numpy array of int ∈ {0,1}): the seeds described in line 5.

            As ((nu, p, q**(p-1)) numpy array of int ∈ {0,...,q-1}): the arrays A described in line 4.

            vs ((p, nu) numpy array of int ∈ {0,...,q-1}): the values v described in line 7.

            cws ((q**(p-1), mu) numpy array of int ∈ {0,...,q-1}): the correction words descbribed in line 6.

        Returns:
            keys ((p, key_length) numpy array of bool): the p keys returned in a single array,
            where key_length = nu*q**(p-1)*(l+m)+nu*m+mu*q**(p-1)*m.
        """
        # For readability
        p = self.p
        m = self.m
        q = self.q
        l = self.lambd
        mu = self.mu
        nu = self.nu

        # Flatten cws
        cws_flat = np.zeros(mu*q**(p-1)*m, dtype=bool)
        offset = 0
        for cw in cws.ravel():
            cws_flat[offset:offset+m] = int_to_binary_array(cw, m)
            offset += m

        # Construct the keys
        key_length = nu*q**(p-1)*(l+m) + nu*m + mu*q**(p-1)*m

        keys = np.zeros((p, key_length), dtype=bool)
        for i in range(p):
            offset = 0
            # sigma_i
            for j in range(nu):
                for k in range(q**(p-1)):
                    A_ = int_to_binary_array(As[j, i, k], m)
                    keys[i, offset:offset+l] = seeds[j, k]*np.any(A_)
                    keys[i, offset+l:offset+l+m] = A_
                    offset += l+m
            # v_i
            for j in range(nu):
                keys[i, offset:offset+m] = int_to_binary_array(vs[i, j], m)
                offset += m
            # cws
            keys[i, offset:] = cws_flat

        return keys

    def _parse_for_eval(self, key, gamma):
        """
        Corresponds to lines 4 and 5 of Algo. 8.
        Parses the key to retreive its different elements.

        Params:
            key ((key_length,) numpy array of uint8): the decoded key,
            where key_length is the first multiple of 8 larger than nu*q**(p-1)*(l+m)+nu*m+mu*q**(p-1)*m.

            gamma (int or None): the first coordinate of the number a on which evaluate has been called, in the ν * µ table.
            gamma is None in the case of a full domain evaluation.

        Returns:
            seeds ((nu, q**(p-1), l) numpy array of int ∈ {0,1}): the seeds s in line 5.

            As ((nu, q**(p-1)) numpy array of int ∈ {0,...,q-1}): the arrays A in line 5.

            v (int ∈ {0,...,q-1} if gamma is int, or (nu,) numpy array of int ∈ {0,...,q-1} if gamma is None): the value v in line 4.
            Rk: if gamma is int, v is actually v[delta] in the paper,
                otherwise v is the full v as in the paper.

            cws ((q**(p-1), mu) numpy array of int ∈ {0,...,q-1}): the correction words cw in line 4.
        """
        # For readability
        p = self.p
        n = self.n
        m = self.m
        q = self.q
        l = self.lambd
        mu = self.mu
        nu = self.nu

        # Retreive different parts of the key
        seeds = np.zeros((nu*q**(p-1), l), dtype=int)
        As = np.zeros(nu*q**(p-1), dtype=int)
        for i in range(nu*q**(p-1)):
            seeds[i] = key[i*(m+l):i*(m+l)+l]
            As[i] = binary_array_to_int(key[i*(m+l)+l:(i+1)*(m+l)])
        seeds = seeds.reshape((nu, q**(p-1), l))
        As = As.reshape((nu, q**(p-1)))

        offset = nu*(l+m)*q**(p-1)
        if gamma is None:
            v = np.array([binary_array_to_int(key[offset+gamma*m:offset+(gamma+1)*m])
                         for gamma in range(nu)])
        else:
            v = binary_array_to_int(key[offset+gamma*m:offset+(gamma+1)*m])

        offset += nu*m
        cws = np.zeros((q**(p-1), mu), dtype=int)
        for i in range(q**(p-1)):
            for j in range(mu):
                cws[i, j] = binary_array_to_int(key[offset+i*(m*mu)+j*m:offset+i*(m*mu)+(j+1)*m])

        return seeds, As, v, cws

    def _combine_for_eval(self, seeds, As, v, cws, gamma, delta):
        """
        Corresponds to line 6 in Algo. 8.
        Combine the different elements to get the final result of the evaluation.

        Params:
            seeds ((nu, q**(p-1), l) numpy array of int ∈ {0,1}): the seeds s in line 5.

            As ((nu, q**(p-1)) numpy array of int ∈ {0,...,q-1}): the arrays A in line 5.

            v (int ∈ {0,...,q-1} or (nu,) numpy array of int ∈ {0,...,q-1}): the value v in line 4.
            v is an array in the case of a full domain evaluation.
            Rk: int when gamma is int and delta is int,
                array when gamma is None and delta is None.

            cws ((q**(p-1), mu) numpy array of int ∈ {0,...,q-1}): the correction words cw in line 4.

            gamma (int or None): the first coordinate of the number a on which evaluate has been called, in the ν * µ table.
            gamma is none in the case of a full domain evaluation.
            Rk: int when delta is int and v is int,
                None when delta is None and v is an array.

            delta (int or None): the second coordinate of the number a on which evaluate has been called, in the ν * µ table.
            delta is none in the case of a full domain evaluation.
            Rk: int when gamma is int and v is int,
                None when gamma is None and v is an array.

        Returns:
            (int ∈ {0,...,q-1} or (N,) numpy array of int ∈ {0,...,q-1}): the result of the evaluation.
        """
        # For readability
        p = self.p
        N = self.N
        q = self.q
        nu = self.nu
        mu = self.mu

        if gamma is None or delta is None:
            res = np.zeros((nu, mu), dtype=int)
            for gamma in range(nu):
                res[gamma] = np.sum([(cws[i] + pseudorandom_generator(seeds[gamma, i], q, mu))
                                    * As[gamma, i] for i in range(q**(p-1))], axis=0)
                res[gamma] += v[gamma]
            res %= q
            return res.ravel()[:N]
        else:
            return (
                (v + np.sum([(cws[i] + pseudorandom_generator(seeds[gamma, i], q, mu))
                 * As[gamma, i] for i in range(q**(p-1))], axis=0)) % q
            )[delta]

    def generate(self, a, g=1):
        """
        Corresponds to Algo. 7.
        Generate the keys for the comparison function 𝑓_{a,g}.
            𝑓: {0,…,N-1} ⟶ 𝔾=ℤ_q
            𝑓_{a,g}(x)  =   g if x ≥ a,
                            0 otherwise

        Params:
            a (int ∈ {0,...,N-1}): see above.

            g (int ∈ {0,...,q-1}): see above.

        Returns:
            keys_enc (list of bytestring): the encoded keys.
        """
        # For readability
        p = self.p
        m = self.m
        q = self.q
        l = self.lambd
        mu = self.mu
        nu = self.nu

        # line 3
        gamma, delta = self._get_coords_in_grid(a)

        # line 4
        As = self._get_As_for_gen(gamma)

        # line 5
        seeds = generate_random_2D_array(2, shape=(nu, q**(p-1), l))

        # line 6
        cws = generate_random_2D_array_with_condition(
            upper_bound=q,
            shape=(q**(p-1), mu),
            sum_of_rows=-
            sum(pseudorandom_generator(seeds[gamma, i], q, mu) for i in range(q**(p-1)))
            + get_step_array(size=mu, step_index=delta, value=g)
        )

        # line 7
        vs = generate_random_2D_array_with_condition(
            upper_bound=q,
            shape=(p, nu),
            sum_of_rows=get_step_array(size=nu, step_index=gamma+1, value=g)
        )

        # lines 8 to 11
        keys = self._construct_keys_for_gen(seeds, As, vs, cws)

        # Serialize the keys
        keys_enc = self._serialize_keys_for_gen(keys)

        return keys_enc

    def evaluate(self, key_enc, x=None):
        """
        Corresponds to Algo 8.
        Evaluate the key at a specific x, or at all x's at once (full domain evaluation).

        Params:
            key_enc (bytestring): the encoded key received by the server.

            x (int ∈ {0,...,N-1} or None): the position at which to evaluate the key.
            x is None for a full domain evaluation.

        Returns:
            res (int ∈ {0,...,q-1} or (N,) numpy array of int ∈ {0,...,q-1}): the result of the evaluation.
            res is an array in the case of a full domain evaluation.
        """
        # For readability
        p = self.p
        q = self.q
        mu = self.mu

        # Deserialize the key
        key = deserialize(key_enc)

        # line 3
        if x is None:
            gamma, delta = None, None
        else:
            gamma, delta = self._get_coords_in_grid(x)

        # lines 4 and 5
        seeds, As, v, cws = self._parse_for_eval(key, gamma)

        # line 6
        res = self._combine_for_eval(seeds, As, v, cws, gamma, delta)

        return res


# Testing
if __name__ == '__main__':
    a = 7
    x = 5
    dcf = DistributedComparisonFunctionForPParties(p=3, N=15, q=4, lambd=3)
    keys = dcf.generate(7)
    print(len(keys[0]), 'bytes per key')
    print(sum(dcf.evaluate(key) for key in keys) % dcf.q)
