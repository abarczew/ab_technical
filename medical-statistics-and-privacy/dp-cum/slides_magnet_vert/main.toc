\beamer@sectionintoc {1}{Motivation and results}{3}{0}{1}
\beamer@sectionintoc {2}{Definitions}{6}{0}{2}
\beamer@sectionintoc {3}{Private ECDF}{8}{0}{3}
\beamer@sectionintoc {4}{Smooth private ECDF}{14}{0}{4}
\beamer@sectionintoc {5}{Algorithm}{18}{0}{5}
\beamer@sectionintoc {6}{Applications}{22}{0}{6}
\beamer@sectionintoc {7}{Conclusion}{30}{0}{7}
