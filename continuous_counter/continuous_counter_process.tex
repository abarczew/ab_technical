% This is samplepaper.tex, a sample chapter demonstrating the
% LLNCS macro package for Springer Computer Science proceedings;
% Version 2.20 of 2017/10/04
%
\documentclass[runningheads]{llncs}
%
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[linesnumbered,ruled]{algorithm2e}
\usepackage{enumitem}
% Used for displaying a sample figure. If possible, figure files should
% be included in EPS format.
%
% If you use the hyperref package, please uncomment the following line
% to display URLs in blue roman font according to Springer's eBook style:
% \renewcommand\UrlFont{\color{blue}\rmfamily}
%\SetKwRepeat{Do}{do}{while}
\begin{document}
%
\title{Differential privacy under continual observation with processes}
\author{Antoine Barczewski}
\institute{Inria}
%
\maketitle

\section{Preliminaries}
\subsection{Definitions and Theorems}

\begin{definition}[Differential Privacy]
  \cite{dwork:2014} A randomized algorithm $\mathcal{M}$ with domain
  $\mathbb{N}^{\vert\mathcal{X}\vert}$ is $(\epsilon, \delta)$-differentially
  private if for all $\mathcal{S} \subseteq Range(\mathcal{M})$
  and for all $x,y \in \mathbb{N}^{|\mathcal{X}|}$ such that $||x - y||_{1}$:
  $$\mathrm{Pr}[\mathcal{M}(x) \in \mathcal{S}] \leq \exp(\epsilon)\mathrm{Pr}[\mathcal{M}(y) \in \mathcal{S}] + \delta $$
  where the probability space is over the coin flips of the mechanism $\mathcal{M}$.
  If $\delta = 0$, we say that $\mathcal{M}$ is $\epsilon$-differentially private.
\end{definition}

\begin{definition}[$l_{1}$-sensitivity]
  \cite{dwork:2014} The $l_{1}$-sensitivity of a function
  $f$ : $\mathbb{N}^{|\mathcal{X}|} \to \mathbb{R}^{k}$ is:
  $$\Delta f = \max_{\substack{x,y \in \mathbb{N}^{|\mathcal{X}|} \\ \|x - y\|_{1}=1}} \|f(x) - f(y)\|_{1}$$
\end{definition}

\begin{definition}[The Laplace Distribution]
  \cite{dwork:2014} The Laplace Distribution (centered at 0) with scale $b$ is the distribution
  with probability density function:
  $$\mathrm{Lap}(x|b) = \frac{1}{2b}\exp\Bigl(-\frac{|x|}{b}\Bigr)$$.
\end{definition}

\begin{definition}[The Laplace Mechanism]
  \cite{dwork:2014} Given any function $f$ :
  $\mathbb{N}^{|\mathcal{X}|} \to \mathbb{R}^{k}$, the Laplace mechanism is defined
  as:
  $$\mathcal{M}_{L}(x, f(\cdot), \epsilon) = f(x) + (Y_{1},\ldots,Y_{k})$$
  where $Y_{i}$ are i.i.d. random variables drawn from $\mathrm{Lap}(\Delta f/b)$.
\end{definition}

\begin{theorem}\label{th:dp_lap}
  \cite{dwork:2014} The Laplace mechanism preserves $(\delta, 0)$-differential
  privacy.
\end{theorem}

\begin{definition}[Pan-privacy on streams]
  \cite{dwork:continual} Let $\mathrm{Alg}$ be an algorithm. Let $\mathrm{I}$ denote the set of internal states of the
  algorithm, and $\sigma$ the set of possible output sequences. Then algorithm $\mathrm{Alg}$
  mapping data stream prefixes to the range $\mathrm{T} \times \sigma$, is user-level pan-private
  (against a single intrusion) if for all sets $\mathrm{I}' \subseteq \mathrm{I}$
  and $\sigma' \subseteq \sigma$, and for all pairs of $X$-adjacent \cite{dwork:continual}
  data stream prefixes $S, S'$:
  $$\mathrm{Pr}[\mathrm{Alg}(S) \in (\mathrm{I}', \sigma')] \leq \exp(\epsilon)\mathrm{Pr}[\mathrm{Alg}(S) \in (\mathrm{I}', \sigma')]$$
  where the probability is over the coin flips of $\mathrm{Alg}$.
\end{definition}

\begin{definition}[Randomized streaming algorithm]
  \cite{dwork:continual} A randomized streaming algorithm yields a $(T, \alpha, \beta)$ counter if,
  in every execution, with probability at least $1 - \beta$ over the coin flips
  of the algorithm, for all $1 \leq t \leq T$, after processing
  a prefix of length $t$, the current output contains an estimate of the number of 1’s
  in the prefix that differs from the true count of the prefix by at most an
  additive $\alpha$ amount.
\end{definition}

\begin{theorem}
  \cite{dwork:continual} The counter algorithm of \cite{dwork:continual} run with
  parameters $T, \epsilon, \beta > 0$, yields a $(T, 4 \log(1/\beta)· \log^{2.5} T /\epsilon, \beta)$
  counter enjoying $\epsilon$-differential pan-privacy against a single intrusion.
\end{theorem}

\begin{definition}
  \cite{wiki:fsm} A finite-state machine (FSM) or finite-state automaton (FSA, plural: automata), finite automaton, or simply a state machine, is a mathematical model of computation. It is an abstract machine that can be in exactly one of a finite number of states at any given time. The FSM can change from one state to another in response to some inputs; the change from one state to another is called a transition. An FSM is defined by a list of its states, its initial state, and the inputs that trigger each transition.
\end{definition}

\section{Problem}
\subsection{Statement}
Suppose an hospital unit needs to count patients suffering from a specific disease on a regular basis. Suppose these patients can go in and out of the hospital and cannot stay longer than a certain amount of time $T_{max}$. What is the best strategy to implement such a counter?

\subsection{Proposed answer}
For $T_{max}$ the maximum length of time periods between the entrance and the exit of a patient, and for any $T \in \mathbb{N}$ the length of the observation, algorithm \ref{alg:binary_neg_panprivate} run with
parameters $T, T_{max}, \epsilon, \beta > 0$, yields a $(T, 4 \log(1/\beta) \cdot \log^{2.5} T_{max} /\epsilon, \beta)$
counter enjoying $\epsilon$-differential pan-privacy against a single
unannounced intrusion.

\begin{proof}\label{th:binary_neg_panprivate}
  We first argue about the accuracy of the algorithm, then prove event-level differential privacy and finally pan-privacy.\newline
  \textbf{Accuracy}. We add $1+\log T$ independently chosen Laplace noise variables to the true answer: the $\log T$ segment noises, and the noise with which we initialize $count$. Each segment contains 1 fresh, independent Laplace noise $\mathrm{Lap}(\log(T_{max}/\epsilon)$. Each of these variables has variance
  $4(\log T_{max} /\epsilon)^2$. Thus, in each round, with probability at least $1-\beta/T_{max}$ probability, the sum of noises is within magnitude $2 \log(1/\beta) + \log T_{max}$ standard deviations, so by a union bound, with probability at least $1-\beta$, in all rounds simultaneously, this sum of noises has magnitude smaller than $4 \log(1/\beta) \cdot \log^{2.5} T_{max} /\epsilon$.\newline
  \textbf{Differential Privacy}.  Consider an item arriving at $t \in \mathbb{N}$. We analyze which $\hat{\alpha_i}$ would be affected if $\sigma_{in}(t)$ is flipped. Note that if $\sigma_{in}(t)$ is flipped in one way, $\sigma_{out}(t)$ is necessarily flipped the other way in at most $T_{max}$ time periods. Hence, the number of $\hat{\alpha_i} \neq 0$ is at most $\log T_{max}$. Observe that each noisy $\hat{\alpha_i}$ maintains $\frac{\epsilon}{\log T_{max}}$-differential privacy by theorem \ref{th:dp_lap}. Hence, we can conclude the $\epsilon$-differential privacy of the algorithm. \ref{alg:binary_neg_panprivate}.\newline
  \textbf{Pan-Privacy}. Suppose the intrusion happens at time $t$ with $t_1 \leq t \leq t_2$ where $t_1$ and $t_2$ are the extremities of one segment one $\hat{\alpha_i}$. The adversary learns the internal state $\sum_{i=t_1}^{t}(\sigma_{in}(i)+\sigma_{out}(i)) + \mathrm{Lap}(\frac{\log T_{max}}{\epsilon})$ in addition to the noisy segment $\sum_{i=t_1}^{t_2}(\sigma_{in}(i)+\sigma_{out}(i) )+ \mathrm{Lap}(\frac{\log T_{max}}{\epsilon}) + \mathrm{Lap}(\frac{\log T_{max}}{\epsilon})$ output at time $t_2$. This is equivalent to revealing the noisy count $\sum_{i=t_1}^{t}(\sigma_{in}(i)+\sigma_{out}(i)) + \mathrm{Lap}(\frac{\log T_{max}}{\epsilon})$ and $\sum_{i=t+1}^{t_2}(\sigma_{in}(i)+\sigma_{out}(i)) + \mathrm{Lap}(\frac{\log T_{max}}{\epsilon})$ to the adversary. Note that changing any single position in the stream can only affect at most one of these two noisy counts. Hence, as its privacy has been proven above, this algorithm achieves $\epsilon$-pan privacy.$\square$
\end{proof}

\begin{algorithm}\label{alg:binary_neg_panprivate}
    \SetKwInOut{Input}{Input}
    \SetKwInOut{Output}{Output}

    \Input{A privacy parameter $\epsilon$, $T_{max}$ the maximum time
    period a patient can stay, a stream
    $\sigma_{in} \in \{-1, 0, 1\}^{\mathbb{N}}$.}
    \Output{At each time step $t$, output estimate $\mathcal{B}(t)$.}
    $\epsilon' \leftarrow \epsilon / \log(T_{max})$\;
    Each $\hat{\alpha_{i}}$ is (implicitly) initialized to $0$\;
    $count \leftarrow \mathrm{Lap}(\frac{1}{\epsilon'})$\;
    \For{$t \leftarrow 1$ \KwTo $T_{max}$}{
      return the estimate at time $t$:
      $$\mathcal{B}(t) = \textsc{BinMech}(t, \sigma, T_{max})$$\;
    }
    Express $T_{max}$ in binary form: $T_{max} = \sum_{j} \mathrm{Bin}_j(T_{max}) \cdot 2^j$\;
    Let $m := \max\{i: \mathrm{Bin}_i(T_{max}) \neq 0\}$\;
    Let $t_{bin} := T_{max}$\;
    \While{$t < \infty$}{
      Let $noise_{old} := \hat{\alpha_m}$\;
      $\hat{\alpha_m} \leftarrow 0$\;
      \For{$t_{sub} \leftarrow 1$ \KwTo $2^m$}{
          $$count \leftarrow count + \sigma(t)$$
          Let $i := \min\{j: \mathrm{Bin}_j(t_{bin}) \neq 0\}$\;
          \For{$j \leftarrow 0$ \KwTo $i-1$}{
            $$\hat{\alpha_{j}} \leftarrow 0$$
          }
          $$\hat{\alpha_{i}} \leftarrow \mathrm{Lap}(\frac{1}{\epsilon'})$$
          return the estimate at time $t$:
          $$\mathcal{B}(t) = count + \sum_{j: \mathrm{Bin}_j(t_{bin})=1} \hat{\alpha_{j}}
          + noise_{old}$$
          $t_{bin} \leftarrow t_{bin} + 1$\;
          $t \leftarrow t + 1$\;
      }
      $$t_{bin} \leftarrow t_{bin} - 2^m$$
    }
    \caption{\textsc{BinMechProccess}: Pan-private binary mechanism $\mathcal{B}$ with underlying process.}

\end{algorithm}


\section{Generalization}
Each patient can be seen as a finite state machine (FSM)\cite{wiki:fsm} where each
state is a service of the hospital the patient can be transferred to.
To exit a state, the patient has be transferred to another service or
leave the hospital. The decision of an exit is made by practitioners
either following a medical prescription or the hospital protocol.
The latter defines the maximum amount of time a patient can stay in a
state/service. The initial and final states are "outside the hospital".

\subsection{Problem statement}
Suppose an hospital unit needs to count patients suffering from a specific disease on a regular basis in each of its service. We define each patient as a FSM receiving inputs from the hospital staff so he or she changes state by changing service to eventually get back the initial state - the patient is outside of the hospital. Let $Q$ be the set of all states corresponding to all services of an hospital unit. Let $\sigma_{in,q}$ and $\sigma_{out,q}$ be respectively the incoming and outgoing streams of patients in state $q \in Q$. For each state $q \in Q$, we define $T_{max}$ be the maximum length of stay in the hospital. What is the best strategy to implement such counters bearing in mind an adversary has access to reports from all services?

\subsection{Proposed answer}
For $T_{max}$ the maximum length of stay in the hospital, for any $T \in \mathbb{N}$ the length of the observation, for any $q \in Q$ the set of states, $\mathcal{B}_q$ in algorithm \ref{alg:binary_fsm_panprivate} run with
parameters $T, T_{max}, \epsilon / |Q|, \beta > 0$, yields a $(T, 4 \log(1/\beta) \cdot \log^{2.5} T_{max} /(\epsilon/|Q|), \beta)$
counter enjoying $\epsilon / |Q|$-differential pan-privacy against a single
unannounced intrusion. \newline
Hence, $\mathcal{B}$ in algorithm  \ref{alg:binary_fsm_panprivate} run with
parameters $T, T_{max}, \epsilon / |Q|, \beta > 0$, yields a $(T, 4 \log(1/\beta) \cdot \log^{2.5} T_{max} /\epsilon, \beta)$
counter enjoying $\epsilon$-differential pan-privacy against a single
unannounced intrusion.

\begin{proof}
  As shown in proof \ref{th:binary_neg_panprivate}, $\mathcal{B}_q$ in algorithm \ref{alg:binary_fsm_panprivate} run with parameters $T, T_{max}, \epsilon / |Q|, \beta > 0$, yields a $(T, 4 \log(1/\beta) \cdot \log^{2.5} T_{max} /(\epsilon/|Q|), \beta)$
  counter enjoying $\epsilon / |Q|$-differential pan-privacy against a single
  unannounced intrusion. \newline
  Hence we can conclude the $\epsilon$-differential pan-privacy against a single
  unannounced intrusion of $\mathcal{B}$,  the sum over $Q$ of the previous algoritm.$\square$
\end{proof}

\begin{algorithm}\label{alg:binary_fsm_panprivate}
    \SetKwInOut{Input}{Input}
    \SetKwInOut{Output}{Output}

    \Input{$Q$ a set of states, a privacy parameter $\epsilon$, $\sigma_{q} \in \{-1, 0, 1\}^{\mathbb{N}}$ the incoming and outgoing stream of $q \in Q$ and $T_{max}$ the maximum length of stay in the hospital.}
    \Output{At each time step $t$, output estimate $\mathcal{B}(t)$.}
    \Return{$$\mathcal{B}(t) = \sum_{q \in Q} \textsc{BinMechProccess}(t, \sigma_{q},\epsilon, T_{max})$$}

    \caption{\textsc{BinMechFSM}: Pan-private binary mechanism $\mathcal{B}$ with final state machines.}

\end{algorithm}

\begin{algorithm}\label{alg:binary}
    \SetKwInOut{Input}{Input}
    \SetKwInOut{Output}{Output}

    \Input{A privacy parameter $\epsilon$, $T_{max}$ the maximum time
    period a patient can stay, a stream
    $\sigma_{in} \in \{-1, 0, 1\}^{\mathbb{N}}$ .}
    \Output{At each time step $t$, output estimate $\mathcal{B}(t)$.}
    $\epsilon' \leftarrow \epsilon / \log(T_{max})$\;
    Each $\hat{\alpha_{i}}$ is (implicitly) initialized to $0$\;
    $count \leftarrow \mathrm{Lap}(\frac{1}{\epsilon'})$\;
    \For{$t \leftarrow 1$ \KwTo $\infty$}{
      $$count \leftarrow count + \sigma(t)$$
      Let $i := \min\{j: \mathrm{Bin}_j(t) \neq 0\}$\;
      \For{$j \leftarrow 0$ \KwTo $i-1$}{
        $$\hat{\alpha_{j}} \leftarrow 0$$
      }
      $$\hat{\alpha_{i}} \leftarrow \mathrm{Lap}(\frac{1}{\epsilon'})$$
      return the estimate at time $t$:
      $$\mathcal{B}(t) = count + \sum_{j: \mathrm{Bin}_j(t)=1} \hat{\alpha_{j}}$$\;
    }
    \caption{\textsc{BinMech}: Binary mechanism $\mathcal{B}$}

\end{algorithm}

\begin{algorithm}\label{alg:binary_neg}
    \SetKwInOut{Input}{Input}
    \SetKwInOut{Output}{Output}

    \Input{A privacy parameter $\epsilon$, $T_{max}$ the maximum time
    period a patient can stay, a stream
    $\sigma_{in} \in \{0, 1\}^{\mathbb{N}}$ and a stream
    $\sigma_{out} \in \{0, 1\}^{\mathbb{N}}$.}
    \Output{At each time step $t$, output estimate $\mathcal{B}(t)$.}
    Each $\alpha_{i}$ and $\hat{\alpha_{i}}$ are (implicitly) initialized to $0$\;
    $\alpha_{old} \leftarrow 0$\;
    $\epsilon' \leftarrow \epsilon / \log(T_{max})$\;
    \For{$t \leftarrow 1$ \KwTo $\infty$}{
      Express $t$ in binary form: $t = \sum_{j} \mathrm{Bin}_j(t) \cdot 2^j$\;
      Let $i := \min\{j: \mathrm{Bin}_j(t) \neq 0\}$\;
      $$\alpha_{i} \leftarrow \sum_{j=0}^{i-1} \alpha_{j} + \sigma_{in}(t)$$
      \For{$j \leftarrow 0$ \KwTo $i-1$}{
        $$\alpha_{j} \leftarrow 0, \hat{\alpha_{j}} \leftarrow 0$$
      }
      $$\hat{\alpha_{i}} \leftarrow \alpha_{i} + \mathrm{Lap}(\frac{1}{\epsilon'})$$
      $$\alpha_{old} \leftarrow \alpha_{old} - \sigma_{out}(t)$$
      return the estimate at time $t$:
      $$\mathcal{B}(t) = \alpha_{old} + \sum_{j: \mathrm{Bin}_j(t)=1} \hat{\alpha_{j}}$$\;
      \If{ $\alpha_{old} < 0$}{
      Let $j := \max\{i: \hat{\alpha_i} \neq 0\}$\;
      $$\alpha_{old} \leftarrow \alpha_{old} + \hat{\alpha_j}$$
      $$\hat{\alpha_j} \leftarrow 0$$
      }
    }
    \caption{\textsc{OldBinMechProcess}: Binary mechanism $\mathcal{B}$ with underlying process.}

\end{algorithm}

\section{Generalization on the length of stay}
Suppose there is no limit in the length of stay of one patient but we know the
distribution of this length. Previous algoritms take as argument $T_{max}$, the maximum
amount of time a patient can stay in the hospital. But if $T_{max}$ follows a probability distribution, can we still ensure differential privacy?

\subsection{Problem statement}
Let's get back to the case where the hospital has only one service, we will generalize
afterwards based on previous results. Suppose an hospital unit needs to count patients suffering from a specific diseaseon a regular basis. Suppose these patients can go in and out of the hospital after an amount of time that follows a known probability distribution.
Let's say this distribution is a Poisson distribution of known parameter. What is the best strategy to implement such a counter?

\subsection{Proposed answer}
Let $\mathrm{P}_T$ be the probability distribution of the amount of time a patient
stays in one hospital. Suppose that only 1\% of patients can stay longer then a value $T_{max}$, i.e. $\forall X \in \Omega, \mathrm{P}_T(X \leq T_{max}) \geq 0.99$ with $\Omega$
the space of all possible outcomes.


\bibliographystyle{IEEEtran}
\bibliography{refs}
\end{document}
