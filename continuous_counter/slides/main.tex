\documentclass[slideopt,A4,showboxes,svgnames]{beamer}


%% list of packages here

\renewcommand{\sfdefault}{lmss}
\sffamily

\setbeamersize{text margin left=1cm,text margin right=1cm}
\setbeamerfont{alerted text}{series=\bfseries}
\setbeamerfont{example text}{series=\bfseries}

\usepackage[absolute,showboxes,overlay]{textpos}
\usepackage{caption}
\captionsetup{font=scriptsize,labelfont=scriptsize}

% For theorems and such
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{amsthm}
\usepackage{microtype}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{booktabs}
\usepackage{hyperref}
\usepackage{bbm}
\usepackage[linesnumbered,ruled]{algorithm2e}
\usepackage[noend]{algpseudocode}
\usepackage{pgf}
\usepackage{tikz}
\usepackage[latin1]{inputenc}
\usepackage{tikz-qtree}

\usetikzlibrary{matrix,arrows,decorations.pathmorphing, decorations.pathreplacing,calligraphy,positioning,calc, automata}

\tikzset{
    ncbar/.style={
        to path=(\tikztostart)
        -- ($(\tikztostart)!#1!90:(\tikztotarget)$)
        -- ($(\tikztotarget)!#1!-90:(\tikztostart)$)
        -- (\tikztotarget)
    },
    ncbar/.default=0.25cm
}

\tikzstyle{every picture}+=[remember picture]

\TPshowboxesfalse
\textblockorigin{0mm}{0mm}

\hypersetup{
allcolors=rouge_inria,
}

\newcommand{\GRAND}{\fontsize{100}{100}\selectfont}
\newcommand{\Grand}{\fontsize{80}{80}\selectfont}
\newcommand{\grand}{\fontsize{60}{60}\selectfont}
\newcommand{\ffeat}{\phi}
\newcommand{\ecdf}[1]{{F_{{#1}}}}
\newcommand{\ecdfdp}[1]{{{\hat{F}}_{{#1}}}}
\newcommand{\ecdfsm}[1]{{{\acute{F}}_{{#1}}}}
\newcommand{\clN}{L} % ceiling of log(N)
\newcommand{\phimin}{\phi^{\hbox{{\small{min}}}}}
\newcommand{\phimax}{\phi^{\hbox{{\small{max}}}}}
\newcommand{\rvseq}{\eta}
\newcommand{\rvidxs}{I}
\newcommand{\rvidxu}{\mathcal{I}}
\newcommand{\rvidxL}[1]{\mathcal{I}[{#1}]}
\newcommand{\rvidxLI}[2]{\mathcal{I}[{#1},{#2}]}
\newcommand{\subtau}{B}
\newcommand{\lossFunc}{\mathcal{L}}
\newcommand{\cmax}{c_{max}}
\newcommand{\trueclass}{c^*}
\newcommand{\estscore}{c_0}
\newcommand{\estclass}{{\hat{c}}}
\newcommand{\indicVal}[1]{\mathbbm{I}\left[{#1}\right]}
\newcommand{\indicSet}[1]{\mathbbm{1}_{{#1}}}
\newcommand{\HLModelFunc}{\mathcal{M}}
\newcommand{\HLObserveFunc}{\mathcal{Y}}
%% TODO : diminuer taille du titre dans la partie haute (cadre gris) : OK, on peut encore diminuer si besoin --> c'est Ok pour moi
%% Grossir le titre général sur slide 1 + le centrer (1 ou 2 lignes) : J'ai grossi et déplacé le texte sur la droite et un peu en haut. Faut-il encore le bouger ? --> C'est bien ainsi :-)

%% Le sous-titre en dessous puis l'auteur en dernier et en moins gros : Fait > VU
%% Slide 2 : on peut bien grossir le chiffre (01), slide 4 idem (02) et slide 6 aussi : Je viens de mettre trois tailles différentes, du plus petit au plus gros, sur les trois chapitres, dis-moi celle qui convient le mieux, ou s'il faut ajuster entre deux.
%% --> je ne vois qu'une taille mais c'est la bonne :-)
%% supprimer le texte dans la partie rouge en haut à gauche sur toutes les slides : OK
%% Logo seul sans baseline sur toutes les occurrences :OK
%% Réduire en taille ce logo : partout (sur la page de titre et en pied de page)? Ou juste en pied de page? --> juste en pied de page, en alignant horizontalement à droite, à l'aplomb du fond blanc

%% Un point rouge simple sans ombré pour les puces : OK
\title[FSM DP counter]{Finite State Machine\\ differentially private counter}
\subtitle{-}
\date[date]{date}
\author[A. Barczewski, J. Ramon]{Antoine Barczewski, Jan Ramon}

\usetheme{inria}
%\usetheme{inria2}
%\usetheme{inria3}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Titre de la présentation avec format Inria
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{frame}
    \titlepage
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%Plan de la présentation

%Chapitre 1
 \frame{\tocpage}

 \section{Motivation and results}
 \frame{\sectionpage}

\begin{frame}{Keep DP infinitely with underlying processes}
  \begin{alertblock}{}
    We propose strategies to \textcolor{rouge_inria}{build differentially private counter} with underlying processes.
  \end{alertblock}
  \begin{exampleblock}{Motivations}
  \begin{itemize}
    \item Suppose an hospital has to report on a daily basis the number of patient with a specific disease.
    \item Privacy can be preserved with limited amount of noise if queried few times, but querying multiple times \textcolor{rouge_inria}{on the same process involving the same patients} would require potentially \textcolor{rouge_inria}{an infinite amount of noise}.
    \item How can you \textcolor{rouge_inria}{preserve privacy without exhausting too quickly the privacy budget}?
  \end{itemize}
  \end{exampleblock}
\end{frame}
\begin{frame}{Our contributions are:}
  \begin{itemize}
    \item We build a differentially private counter on a process that runs for a long (possibly infinite) time and is \textcolor{rouge_inria}{queried after every step}.
    \item For this we use \textcolor{rouge_inria}{finite state machines}.
    \item We \textcolor{rouge_inria}{prove differential privacy guarantees} for every state of the Final State Machine.
    \item We \textcolor{rouge_inria}{measure empirically} these guarantees with synthetic data.
  \end{itemize}
\end{frame}


%Chapitre 2
\section{Definitions}
 \frame{\sectionpage}

 \begin{frame}{Preliminary definitions}
  \begin{alertblock}{Differential privacy}
    Let $\epsilon>0$.  Let $\mathcal{A}$ be an algorithm taking as input datasets from $\mathcal{X}^*$.  Two datasets $X^{(1)},X^{(2)}\in \mathcal{X}^*$ are adjacent if they differ in only one element.   The algorithm $\mathcal{A}$ is $\epsilon$-differentially private ($\epsilon$-DP) if for every pair of adjacent datasets $X^{(1)}$ and $X^{(2)}$, and every subset $S$ of possible outputs of $\mathcal{A}$, $$P(\mathcal{A}(X^{(1)})\subseteq S) \le e^\epsilon P(\mathcal{A}(X^{(2)})\subseteq S)$$
  \end{alertblock}
  \begin{alertblock}{Finite State Machine}
    A finite-state machine (FSM) is a mathematical model of computation. It is an abstract machine that can be in exactly one of a finite number of states at any given time. The FSM can change from one state to another in response to some inputs; the change from one state to another is called a transition.
  \end{alertblock}
\end{frame}

%Chapitre 3
% TODO: change terminologie
\section{One process DP counter}
 \frame{\sectionpage}

 \begin{frame}{How to count patients with DP?}
  \begin{exampleblock}{Problem statement}
    Suppose an hospital unit needs to count patients suffering from a specific disease on a regular basis. Suppose these patients can go in and out of the hospital and cannot stay longer than a certain amount of time $T_{max}$. What is the best strategy to implement such a counter?
  \end{exampleblock}
  \begin{exampleblock}{Issues to tackle}
    \begin{itemize}
      \item Sensitive data requiring privacy guarantees;
      \item Multiple querying;
      \item Infinite time of analysis but fixed duration of process.
    \end{itemize}
  \end{exampleblock}
 \end{frame}

 \begin{frame}{Binary mechanism}
  \scalebox{0.9}{
     \begin{algorithm}[H]
         \SetKwInOut{Input}{Input}
         \SetKwInOut{Output}{Output}
         \Input{A privacy parameter $\epsilon$, $T_{max}$ the maximum time
         period a patient can stay, a stream
         $\sigma_{in} \in \{-1, 0, 1\}^{\mathbb{N}}$ .}
         \Output{At each time step $t$, output estimate $\mathcal{B}(t)$.}
         $\epsilon' \leftarrow \epsilon / \log(T_{max})$; Each $\hat{\alpha_{i}}$ is (implicitly) initialized to $0$; $count \leftarrow \mathrm{Lap}(\frac{1}{\epsilon'})$\;
         \For{$t \leftarrow 1$ \KwTo $\infty$}{
           $count \leftarrow count + \sigma(t)$\;
           Let $i := \min\{j: \mathrm{Bin}_j(t) \neq 0\}$\;
           \For{$j \leftarrow 0$ \KwTo $i-1$}{
             $\hat{\alpha_{j}} \leftarrow 0$
           }
           $\hat{\alpha_{i}} \leftarrow \mathrm{Lap}(\frac{1}{\epsilon'})$\;
           return the estimate at time $t$:
           $\mathcal{B}(t) = count + \sum_{j: \mathrm{Bin}_j(t)=1} \hat{\alpha_{j}}$
         }
         \caption{\textsc{BinMech}.}
     \end{algorithm}
    }
 \end{frame}

 \begin{frame}{Binary mechanism with underlying process}
 \scalebox{0.9}{
   \begin{algorithm}[H]
       \SetKwInOut{Input}{Input}
       \SetKwInOut{Output}{Output}

       \Input{A privacy parameter $\epsilon$, $T_{max}$ the maximum time
       period a patient can stay, a stream
       $\sigma_{in} \in \{-1, 0, 1\}^{\mathbb{N}}$.}
       \Output{At each time step $t$, output estimate $\mathcal{B}(t)$.}
       \For{$t \leftarrow 1$ \KwTo $T_{max}$}{
         return $\mathcal{B}(t) = \textsc{BinMech}(t, \sigma,\epsilon, T_{max})$
       }
       Let $m := \max\{i: \mathrm{Bin}_i(T_{max}) \neq 0\}$;
       Let $t_{bin} := T_{max}$\;
       \While{$t < \infty$}{
         Let $noise_{old} := \hat{\alpha_m}$;
         $\hat{\alpha_m} \leftarrow 0$\;
         \For{$t_{sub} \leftarrow 1$ \KwTo $2^m$}{
             return $\mathcal{B}(t) = \textsc{BinMech}(t_{bin}, \sigma,\epsilon, 2^m) + noise_{old}$\;
             $t_{bin} \leftarrow t_{bin} + 1$\;
             $t \leftarrow t + 1$\;
         }
         $t_{bin} \leftarrow t_{bin} - 2^m$
       }
       \caption{\textsc{BinMechProccess}.}
   \end{algorithm}
  }
 \end{frame}

 \begin{frame}{The mechanism preserves $\epsilon$-differential privacy}
  \begin{alertblock}{Differential Privacy proof}
    \begin{itemize}
      \item Consider an item arriving at $t \in \mathbb{N}$. We analyze which $\hat{\alpha_i}$ would be affected if $\sigma_{in}(t)$ is flipped.
      \item If $\sigma_{in}(t)$ is flipped in one way, $\sigma_{out}(t)$ is flipped the other way in at most $T_{max}$.
      \item Each noisy $\hat{\alpha_i}$ maintains $\frac{\epsilon}{\log T_{max}}$-differential privacy.
      \item Hence, we can conclude the $\epsilon$-differential privacy of the algorithm.
    \end{itemize}
  \end{alertblock}
 \end{frame}

%Chapitre
\section{Finite State DP counter}
 \frame{\sectionpage}

\begin{frame}{How to count patients in several units?}
 \begin{exampleblock}{Problem statement}
   Suppose an hospital unit needs to count patients suffering from a specific disease on a regular basis in each of its service. We define each patient as a FSM receiving inputs from the hospital staff so he or she changes state by changing service. We define $T_{max}$ be the maximum length of stay in the hospital.\\
   What is the best strategy to implement such counters bearing in mind an adversary has access to reports from all services?
 \end{exampleblock}
 \begin{exampleblock}{Issues to tackle}
   \begin{itemize}
     \item Sensitive data requiring privacy guarantees;
     \item Multiple querying;
     \item Infinite time of analysis but fixed duration of process;
     \item Complex underlying processes.
   \end{itemize}
 \end{exampleblock}
\end{frame}

\begin{frame}{FSM can describe patient's journey in hospital}
  \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,
                    semithick]
  \tikzstyle{every state}=[fill=red,draw=none,text=white]

  \node[initial,state] (A)                    {reception};
  \node[state]         (D) [above right of=A] {surgery};
  \node[state]         (E) [below right of=A] {examination};
  \node[state]         (B) [right of=D]       {ward};
  \node[state]         (C) [right of=E]       {IC};
  \node[state]         (F) [above right of=C] {exit};

  \path (A) edge              node {} (E)
            edge              node {} (D)
            edge [loop above] node {} (A)
            edge              node {} (F)
        (B) edge              node {} (E)
            edge [loop above] node {} (B)
            edge              node {} (F)
        (C) edge              node {} (B)
            edge [loop below] node {} (C)
            edge              node {} (F)
        (D) edge              node {} (B)
            edge [loop above] node {} (D)
            edge              node {} (F)
        (E) edge              node {} (C)
            edge              node {} (D)
            edge [loop left]  node {} (E)
            edge              node {} (F);
  \end{tikzpicture}\\
  \footnotesize{This drawing is just meant for illustration purposes, we apology for the simplification.}
\end{frame}

\begin{frame}{Binary mechanism $\mathcal{B}$ with final state machines}
  \scalebox{0.9}{
    \begin{algorithm}[H]
      \SetKwInOut{Input}{Input}
      \SetKwInOut{Output}{Output}
      \Input{$Q$ a set of states, a privacy parameter $\epsilon$, $\sigma_{q} \in \{-1, 0, 1\}^{\mathbb{N}}$ the incoming and outgoing stream of $q \in Q$ and $T_{max}$ the maximum length of stay in the hospital.}
      \Output{At each time step $t$, output estimate $\mathcal{B}(t)$.}
      $\epsilon' \leftarrow \epsilon / |Q|$\;
      \Return{$$\mathcal{B}(t) = \sum_{q \in Q} \textsc{BinMechProccess}(t, \sigma_{q},\epsilon', T_{max})$$}
      \caption{\textsc{BinMechFSM}.}
    \end{algorithm}
  }
\end{frame}

%Chapitre
\section{Experiments}
 \frame{\sectionpage}

 \begin{frame}{Our algorithm works empirically}
\begin{columns}
% Column 1
\begin{column}{0.35\textwidth}
\begin{itemize}
  \item Synthetic data that replicates streams of patients in hospital units.
  \item Error grows with smaller $\epsilon$.
  \item Error increases logarithmically with $T_{max}$.
  \item Error is close to the theoretical one.
\end{itemize}
\end{column}
% Column 2
\begin{column}{0.65\textwidth}
  \begin{figure}
  \hskip -0.2in
  \centering
      \includegraphics[width=\columnwidth]{Figs/dp_counter_boundaries.png}
      \caption{MSE between DP and true count in simulated hospital units depending on $T_{max}$}
  \end{figure}
\end{column}
\end{columns}
\end{frame}

%Chapitre
\section{Future work}
 \frame{\sectionpage}

 \begin{frame}{}
   \begin{itemize}
     \item We can \textcolor{rouge_inria}{generalize on the length of the stay} with $T_{max}$ follows a poisson distribution.
     \item We need to build a \textcolor{rouge_inria}{more robust model} so it can cope with edge cases eg patient move units several times within the same day etc.
     \item We only focus on counting but we should be able to answer \textcolor{rouge_inria}{more complex statistical queries}.
   \end{itemize}
 \end{frame}

\end{document}
