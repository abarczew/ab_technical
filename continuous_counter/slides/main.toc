\beamer@sectionintoc {1}{Motivation and results}{3}{0}{1}
\beamer@sectionintoc {2}{Definitions}{6}{0}{2}
\beamer@sectionintoc {3}{One process DP counter}{8}{0}{3}
\beamer@sectionintoc {4}{Finite State DP counter}{13}{0}{4}
\beamer@sectionintoc {5}{Experiments}{17}{0}{5}
\beamer@sectionintoc {6}{Future work}{19}{0}{6}
