\subsection{Empirical DP optimum}\label{subsec:emp_opt}
\begingroup
\begin{frame}{Differentially private gradient descent with
empirically estimated sensitivity}
\begin{minipage}[t]{0.48\linewidth}
    \centering\textbf{Problem statement}\\
    \smallskip
    \begin{itemize}

      \item Differentially Private Empirical Risk Minimization (DP-ERM) has first been studied by \textcolor{red}{adding noise to the result} of the optimization program along its objectives (Chaudhuri et al.)
      \item To provide a gradient descent already private rather than only its result, noise terms are \textcolor{red}{added at each gradient update} (DP-SGD) \citep{bassily_differentially_2014}
      \item The latter is based on an upper bound of the sensitivity of the gradient that seems too conservative
      \item Can't we thighten this upper bound?
    \end{itemize}
    \pause
\end{minipage}\hfill
\begin{minipage}[t]{0.47\linewidth}
        \centering\textbf{Challenges}\\
        \smallskip
        \begin{itemize}
           \item Multiple querying
           \item Proof on differential privacy guarantees
        \end{itemize}
\end{minipage}

\end{frame}
\endgroup

\begingroup
\begin{frame}[t]{We replace the Lipschitz constant upper bound by and empirical estimation of the sensitivity}
\begin{minipage}[t]{0.48\linewidth}
  \vspace{0pt}
  \begin{tcolorbox}[title= Algorithm (DP-SGD) \citep{bassily_differentially_2014},size=title,boxrule=0.2pt]
  \begin{algorithm}[H]\label{alg:dp-sgd}
  \SetKwInOut{Input}{Input}
  \SetKwInOut{Output}{Output}

  \Input{Data set: $\mathcal{D}$, loss function $\ell$ (with Lipschitz constant $L$), privacy parameters ($\epsilon$, $\delta$), convex set $\mathcal{C}$, and learning rate function $\eta$: $[n^2] \rightarrow \mathbb{R}$}
  Set noise variance $\sigma^2 \leftarrow \frac{32L^2n^2\log(n/\delta)\log(1/\delta)}{\epsilon^2}$\;
  $\widetilde{\theta}_1$: Choose any point from $\mathcal{C}$\;
  \For{$t \leftarrow 1$ \KwTo $n^2-1$}{
    Pick $d \thicksim_u \mathcal{D}$ with replacement\;
    $\widetilde{\theta}_{t+1} = \widetilde{\theta}_t - \eta(t)[n \nabla \ell (\widetilde{\theta}_t;d)+b_t]$, where $b_t \thicksim \mathcal{N}(0,\mathbb{I}_p\sigma^2)$\;
  }
  \Output{$\theta^{priv} = \widetilde{\theta}_{n^2}$.}

\end{algorithm}

  \end{tcolorbox}
\end{minipage}\hfill
\pause
\begin{minipage}[t]{0.48\linewidth}
  \vspace{0pt}
  \begin{tcolorbox}[title= Algorithm (DP-EmpiricalSensitivity-SGD),size=title,boxrule=0pt,boxsep=1.9pt,left=4pt,right=0pt,top=0pt,bottom=0pt]
  \begin{algorithm}[H]\label{alg:emp-dp-sgd}
    \SetKwInOut{Input}{Input}
    \SetKwInOut{Output}{Output}

    \Input{$\mathcal{D}$, $\ell$, ($\epsilon$, $\delta$),  $\mathcal{C}$ with basis $\{e_i\}_{i \in [p]}$ and $\eta$}
    Set noise variance $\sigma^2 \leftarrow \frac{32n^2\log(n/\delta)\log(1/\delta)}{\epsilon^2}$\;
    $\widetilde{\theta}_1$: Choose any point from $\mathcal{C}$\;
    \For{$i \leftarrow 1$ \KwTo $p$}{
    $s_i = \|\nabla \ell ({\theta}_{1}; \mathcal{D} \cup \{e_i\}) - \nabla \ell ({\theta}_{1};\mathcal{D} \cup \{\vec{0}\})\|_2$\;
    }
    Set sensitivity: $S_1 = \max\limits_i(s_i)$\;
    \For{$t \leftarrow 1$ \KwTo $n^2-1$}{
      Pick $d \thicksim_u \mathcal{D}$ with replacement\;
      $\widetilde{\theta}_{t+1} = \widetilde{\theta}_t - \eta(t)[n \nabla \ell (\widetilde{\theta}_t;d)+b_t]$, where $b_t \thicksim \mathcal{N}(0,\mathbb{I}_p\sigma^2 S_{t}^2)$\;
      \For{$i \leftarrow 1$ \KwTo $p$}{
      $s_i = \|\nabla \ell ({\theta}_{t+1}; \mathcal{D} \cup \{e_i\}) - \nabla \ell ({\theta}_{t+1};\mathcal{D} \cup \{\vec{0}\})\|_2$\;
      }
      Set sensitivity: $S_{t+1} = \max\limits_i(s_i)$\;
    }
    \Output{$\theta^{priv} = \widetilde{\theta}_{n^2}$.}

\end{algorithm}
  \end{tcolorbox}
\end{minipage}

\end{frame}
\endgroup

\begin{frame}[c]{Future work}
\begin{itemize}
     \item We need to proof the \textcolor{red}{DP guarantees}. Basically we need to be able to prove the following:
     \vspace{1em}
     \begin{center}
       \begin{tcolorbox}\label{th:grad_ineq}
        Let $\{e_i\}_{i \in [p]}$ basis vectors of $\mathcal{D}$, $\Delta \nabla \ell$ the sensitivity of $\nabla \ell$.
        Then, for all $\theta \in \Theta$
        $$\Delta \nabla \ell \leq \max\limits_i(\|\nabla \ell ({\theta}; \mathcal{D} \cup \{e_i\}) - \nabla \ell ({\theta};\mathcal{D} \cup \{\vec{0}\})\|_2)$$
      \end{tcolorbox}
    \end{center}
     \item Along with the latter, we need to find a \textcolor{red}{lower bound of the utility} that proves the benefit of our methodology compared to the DP-SGD
     \item We will \textcolor{red}{assess the performance} of the methodology \textcolor{red}{against state of the art implementations} both in terms of machine learning performances and of resulting variance of the coefficients
   \end{itemize}
\end{frame}
