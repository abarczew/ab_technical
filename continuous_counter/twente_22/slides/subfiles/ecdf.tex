\subsection{ECDF}\label{subsec:ecdf}
\begingroup
\begin{frame}{Differentially Private Empirical Cumulative Distribution Functions}
\begin{minipage}[t]{0.48\linewidth}
    \centering\textbf{Problem statement}\\
    \begin{itemize}
      \item Empirical cumulative distribution functions (ECDF) play an \textcolor{red}{important role in statistics and machine learning}, e.g. ROC curves are based on ECDF
      \item Publishing a complete ECDF requires \textcolor{red}{more noise} than
      publishing a single aggregated value, but is also much \textcolor{red}{more informative}
      \item Let's assume several parties, e.g. hospitals, have sensitive data that can help computing an ECDF accross them. How can we compute it \textcolor{red}{securely and privately}?
    \end{itemize}
    \pause
\end{minipage}\hfill
\begin{minipage}[t]{0.47\linewidth}
        \centering\textbf{Challenges}\\
        \begin{itemize}
           \item Sensitive data requiring privacy guarantees
           \item Decentralized set up requiring security guarantees
           \item Continuous range of points to protect
        \end{itemize}
\end{minipage}

\end{frame}
\endgroup

\begingroup
\begin{frame}[t]{We propose strategies to compute securely and privately ECDF}
\begin{minipage}[t]{0.48\linewidth}
    \centering\textbf{Contributions}\\

    \begin{itemize}
    \item we \textcolor{red}{improve by a constant-factor the differential privacy guarantees} for continually observed statistics as shown by \cite{DBLP:journals/corr/abs-2103-16787}
    \end{itemize}
    \pause
\end{minipage}\hfill
\begin{figure}[!h]
  \centering
  \begin{tikzpicture}[every tree node/.style={draw,circle},
     level distance=1cm,sibling distance=0.7cm,
     edge from parent path={(\tikzparentnode) -- (\tikzchildnode)}]
    \Tree
    [.\node[style={draw,fill=red}, label={$[1,8]$}]{\textbf{\textcolor{white}{+}}};
        \edge;
        [.\node[style={draw,fill=black}, label={$[1,4]$}]{};
          \edge;
          [.\node[label={$[1,2]$}]{};
            \edge;
            [.\node (1) [label={$[1]$}]{};]
            \edge;
            [.\node[label={$[2]$}]{};]
            ]
          \edge;
          [.\node[label={$[3,4]$}]{};
            \edge;
            [.\node[label={$[3]$}]{};]
            \edge;
            [.\node[label={$[4]$}]{};]
            ]
          ]
        \edge;
        [.\node[label={$[5,8]$}]{};
          \edge;
          [.\node[style={draw,fill=black}, label={$[5,6]$}]{};
              \edge;
              [.\node[label={$[5]$}]{};]
              \edge;
              [.\node[label={$[6]$}]{};]
              ]
          \edge;
          [.\node[label={$[7,8]$}]{};
             \edge;
             [.\node (7) [style={draw,fill=black}, label={$[7]$}]{};]
             \edge;
             [.\node[style={draw, fill=red}, label={$[8]$}]{\textbf{\textcolor{white}{-}}};]
          ]
      ]
    ]
  \draw[ncbar=-.4cm] (1) to (7);
  \end{tikzpicture}
  \caption{It requires to change only $2$ noise terms (red) to increase the ecdf by $1$ in an interval from $1$ to $7$ against $3$ before (black).}
  \end{figure}
   \begin{itemize}
     \item \textcolor{red}{If negative terms} are allowed, then \textcolor{red}{at most $\lceil (\log_2N+1)/2\rceil$ noise terms} can be changed for any interval
     \item We prove this by induction
   \end{itemize}
\end{frame}
\endgroup

\begingroup
\begin{frame}[t]{We propose strategies to compute securely and privately ECDF}
\begin{minipage}[t]{0.48\linewidth}
    \centering\textbf{Contributions}\\

    \begin{itemize}
    \item we \textcolor{red}{improve by a constant-factor the differential privacy guarantees} for continually observed statistics as shown by \cite{DBLP:journals/corr/abs-2103-16787}
    \item we propose a \textcolor{red}{strategy to smooth differentially private functions}, which makes the function non-decreasing again, and in some cases reduce the error
    \end{itemize}
\end{minipage}\hfill
\pause
\begin{minipage}[t]{0.48\linewidth}
  \begin{figure}
    \hskip -0.2in
    \centering
    \includegraphics[width=1.\textwidth]{figures/roc_exp_results_framingham_0.5.png}
      \caption{ROC curve for logistic regression on the Heart disease dataset, and $\epsilon$-DP curves with $\epsilon=0.5$.}
  \end{figure}
  \begin{itemize}
    \item Noise adds horizontal and vertical deviations
    \item The smoothed curves resolve this problem
    \item They stay much closer to the true ROC curve
  \end{itemize}
\end{minipage}\hfill
\end{frame}
\endgroup

\begingroup
\begin{frame}[t]{We propose strategies to compute securely and privately ECDF}
\begin{minipage}[t]{0.48\linewidth}
    \centering\textbf{Contributions}\\

    \begin{itemize}
    \item we \textcolor{red}{improve by a constant-factor the differential privacy guarantees} for continually observed statistics as shown by \cite{DBLP:journals/corr/abs-2103-16787}
    \item we propose a \textcolor{red}{strategy to smooth differentially private functions}, which makes the function non-decreasing again, and in some cases reduce the error
    \item we propose both a generic algorithm which can start from any \textcolor{red}{secure aggregation operator} and a specific strategy based on \textcolor{red}{function secret sharing} \citep{Boyle2015} which has asymptotically better complexity
    \end{itemize}
\end{minipage}\hfill
\pause
\begin{minipage}[t]{0.48\linewidth}
\begin{center}
    \begin{tikzpicture}[align=center, node distance=1.7cm, auto]
      \node (X) {$X_1,$};
      \node (dots) [right of=X] {$\dots$};
      \node (XN) [right of=dots] {,$X_N$};
      \node (keys) [below=2cm of X] {$(k_{1,1}, \dots, k_{1, m})$,};
      \node (dots2) [right of=keys] {$\dots$};
      \node (keysN) [below=2cm of XN] {,$(k_{N,1}, \dots, k_{N, m})$};
      \node (eval1) [below=2cm of keys] {$f_1(t) = \sum_{i=1}^{N} eval(k_{i, 1}, t)$,};
      \node (dots3) [right of=eval1] {$\dots$};
      \node (evalm) [below=2cm of keysN] {,$f_m(t) = \sum_{i=1}^{N} eval(k_{i, m}, t)$};
      \draw[-to] (X) to node {\footnotesize{$gen(f, X_1)$}} (keys);
      \draw[-to] (XN) to node {\footnotesize{$gen(f, X_N)$}} (keysN);
      \draw[-to] (keys) to node {} (eval1);
      \draw[-to] (keysN) to node {} (eval1);
      \draw[-to] (keys) to node {} (evalm);
      \draw[-to] (keysN) to node {} (evalm);
      \draw[decorate,decoration={calligraphic brace,
      amplitude=7pt,raise=0.5ex},thick] (evalm.south east) -- (eval1.south west)
      node[midway,below=0.8em](ecdf){$ECDF(t) = \sum_{j=1}^{m} f_j(t)$};
    \end{tikzpicture}
  \end{center}
  \begin{itemize}
    \item Computation cost is linear in the key length
    \item Communication cost is linear in the number of servers
  \end{itemize}
\end{minipage}\hfill
\end{frame}
\endgroup

\begingroup
\begin{frame}[t]{We propose strategies to compute securely and privately ECDF}
\begin{minipage}[t]{0.48\linewidth}
    \centering\textbf{Contributions}\\

    \begin{itemize}
    \item we \textcolor{red}{improve by a constant-factor the differential privacy guarantees} for continually observed statistics as shown by \cite{DBLP:journals/corr/abs-2103-16787}
    \item we propose a \textcolor{red}{strategy to smooth differentially private functions}, which makes the function non-decreasing again, and in some cases reduce the error
    \item we propose both a generic algorithm which can start from any \textcolor{red}{secure aggregation operator} and a specific strategy based on \textcolor{red}{function secret sharing} \citep{Boyle2015} which has asymptotically better complexity
    \item we discuss how to make \textcolor{red}{$\epsilon$-differentially private ROC curves and Hosmer-Lemeshow statistic} using our technique
    \end{itemize}
\end{minipage}\hfill
\pause
\begin{minipage}[t]{0.48\linewidth}
\begin{figure}
\hskip -0.2in
\centering
    \includegraphics[width=\columnwidth]{figures/roc_exp_summary_bank-full_100.png}
    \caption{ROC curve estimation error}
\end{figure}
  \begin{itemize}
  \item{The error metric is the symmetric difference of the AUCs, which gives more information than comparing AUCs}
  \item{Error decreases with increasing $\epsilon$}
  \item{As the unsmoothed DP ROC curves cross themselves, it is hard to compute this metric}
  \end{itemize}
\end{minipage}\hfill
\end{frame}
\endgroup

\begin{frame}[c]{Future work}
\begin{itemize}
     \item Measure \textcolor{red}{running time with FSS} architecture
     \item Elaborate \textcolor{red}{more applications} of ECDF, to develop more efficient algorithms to securely compute private ECDF
     \item Get a \textcolor{red}{better understanding} of the various statistical processes affecting the error DP noise induces
   \end{itemize}
\end{frame}
