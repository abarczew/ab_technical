
\documentclass{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bbm}
\usepackage{algorithmicx}
\usepackage{algpseudocode}
\usepackage{xcolor}

\newcommand{\janfoot}[1]{\textcolor{blue}{\footnote{\textcolor{blue}{{{#1}}}}}}
\newcommand{\amalfoot}[1]{\textcolor{green}{\footnote{\textcolor{green}{{{#1}}}}}}
\newcommand{\antoinefoot}[1]{\textcolor{red}{\footnote{\textcolor{red}{{{#1}}}}}}
\begin{document}

\title{Differential privacy under continual observation revisited: the effects of smoothing and start time estimation}
\author{Jan Ramon et al}
\date{\today{}}
\maketitle

\section{Introduction}

Let $[n]=\{i\in \mathbb{N}\mid 1\le i \le n\}$
and $[n_1,n_1]=\{i\in\mathbb{N}\mid n_1\le i \le n_2$.
Let $T\in\mathbb{N}$ be the length of a time interval under consideration. Let $L=\lceil \log_2(T)\rceil$.   Given a sequence of arrival times $(t_1 \ldots t_m) \in [T]$ and partial counts $x_t = \sum_{i=1}^m \mathbbm{1}[t_i\le t]$, we would like to publish the partial counts $x_t$ (while keeping the arrival times $t_j$  ($j=1\ldots m$) $\epsilon$-differentially private.

Following \cite{dwork:continual}, we choose $\eta_{i,j}\sim Lap((\log_2(T)+1)/\epsilon)$ for $j\in [0,L-1]$ and $i\in [ \lceil N/2^j\rceil ]$, and publish ${\hat{x}}_i=x_i+\sum_{j=0}^{L-1} \eta_{\lceil i/2^j\rceil,j}$.  The result is $\epsilon$-differentially private.  

In this text, we study two remaining questions:
\begin{itemize}
\item We know that the sequence $x_1\ldots x_T$ is non-decreasing, i.e., for $1\le i_1<i_2\le T$, $x_{i_1}\le x_{i_2}$.  However, this does not necessarily hold for the noisy ${\hat{x}}_i$.  Can we smooth the sequence ${\hat{x}}_i$ such that it becomes non-decreasing again, and what is the effect in terms of error with respect to the original sequence $(x_i)_{i=1}^T$ ?
\item Suppose an adversary sees a part $({\hat{x}}_i)_{i=a}^b$ of the sequence.  Can this adversary estimate the noise alignment, i.e., the lowest bits of $a$?
\end{itemize}

\section{Smoothing the noisy sequence}

From a differential privacy point of view, any smoothing as post-processing doesn't weaken the privacy guarantee.

We may observe ${\hat{x}}_{i+1}<{\hat{x}}_i$ when there are only few items arriving at time $i+1$.  If a lot of items arrive, it is unlikely we will see the noise counts decrease.

More precisely, ${\hat{x}}_{i+1}<{\hat{x}}_i$ means that
\[
  x_{i+1}+\sum_{j=0}^{L-1} \eta_{\lceil (i+1)/2^j\rceil,j} <
  x_i+\sum_{j=0}^{L-1} \eta_{\lceil i/2^j\rceil,j}
\]
Let $l$ be the smallest non-negative integer for which $\lceil (i+1)/2^l\rceil =\lceil i/2^l\rceil$.  Then, we get
\[
  x_{i+1}-x_i <
  \sum_{j=0}^{l-1} \eta_{\lceil i/2^j\rceil,j} -  \eta_{\lceil (i+1)/2^j\rceil,j}
\]

\subsection{Experiments}

\textbf{Datasets.}  We consider synthetic datasets.  In particular,
\begin{itemize}
  \item \textbf{Poisson.}  The procedure $gen_{\hbox{pois},\lambda}(T)$ generates a dataset over an interval of length $T$ where the number of arrivals at time $t$ (for any $t\in [T]$) is Poisson distributed, i.e., $P(x_t-x_{t-1} = k)=\lambda^k e^{-\lambda}/k!$.  We will consider $\lambda=10^{i/3}$ with $i\in[-5,5]$.
\item \ldots
\end{itemize}

\textbf{Smoothing strategy.}  We will consider
\begin{itemize}
\item \textit{baseline}, i.e., no smoothing is performed and just $(y_i)_{i=1}^T = ({\hat{x}}_i)_{i=1}^T$ is returned.
\item \textit{qp}, which returns the sequence $(y_i)_{i=1}^T$ with $y_i={\hat{x}}_i+\sum_{j=0}^{L-1} s_{\lceil i/2^j\rceil,j}$ for which $\forall i:y_i\le y_{i+1}$ and $\sum_{j=0}^{L-1}\sum_{I=1}^{\lceil T/2^j\rceil} s_{I,j}^2$ is minimal.  This involves solving a quadratic programming problem.
\item \textit{simple}, which returns the sequence $(y_i)_{i=1}^T$ with $y_i={\hat{x}}_i+ s_i$ for which $\forall i:y_i\le y_{i+1}$ and $\sum_{i=1}^{T} s_i^2$ is minimal.  This involves solving a quadratic programming problem:
  \[
    \begin{array}{l}
      \min \sum_{i=1}^T s_i^2 \\
      \hbox{s.t. } \forall i\in [T-1] : {\hat{x}}_i+s_i \le {\hat{x}}_{i+1}+s_{i+1}
    \end{array}
  \]
\end{itemize}

\subsubsection{Experiment 1}
We compare for the several datasets of size $T\in\{2^5, 2^{10}, 2^{15}, 2^{20}, 2^{25}\}$ the mean squared error (MSE) $\sum_{i=1}^T (y_i-x_i)^2$ of the several smoothing strategies and the theoretical error $(L+1)^3/\epsilon^2$.\janfoot{exact error expression to be confirmed.}


\section{Estimating the noise alignment}

\bibliographystyle{plain}
\bibliography{../refs}

\end{document}
