import numpy as np
from utils import stream_item_reader


def min_bin(t):
    bin_t = bin(t).split('b')[1][::-1]
    bin_t_arr = np.array(list(map(int, list(bin_t))))
    return np.argmax(bin_t_arr)


def max_bin(t):
    return len(bin(t).split('b')[1])


def ind_bin(t):
    """
    Return the indexes of ones in the binary description of t.
    """
    bin_t = bin(t).split('b')[1][::-1]
    return list(map(lambda x: bool(int(x)), list(bin_t)))


def dp_counter(stream, epsilon, stream_item_reader=stream_item_reader):
    """

    """
    stream_length = len(stream)
    epsilon_prime = epsilon / np.log(stream_length)
    count = np.random.laplace(0, 1/epsilon_prime)
    alpha = np.zeros(len(stream))
    cum_sum_dp = []
    for t in np.arange(0, stream_length):
        count += stream_item_reader(stream[t])
        i = min_bin(t)
        alpha[:i] = np.zeros_like(alpha[:i])
        alpha[i] = np.random.laplace(0, 1/epsilon_prime)
        index_noise = ind_bin(t)
        noise = alpha[:len(index_noise)][index_noise].sum()
        cum_sum_dp.append(count + noise)
    return cum_sum_dp


def dp_counter_process(stream, epsilon, t_max):
    """

    """
    epsilon_prime = epsilon / np.log(t_max)
    count = np.random.laplace(0, 1/epsilon_prime)
    alpha = np.zeros(len(stream))
    cum_sum_dp = []
    for t in np.arange(0, min(len(stream), t_max)):
        count += stream_item_reader(stream[t])
        i = min_bin(t)
        alpha[:i] = np.zeros_like(alpha[:i])
        alpha[i] = np.random.laplace(0, 1/epsilon_prime)
        index_noise = ind_bin(t)
        noise = alpha[:len(index_noise)][index_noise].sum()
        cum_sum_dp.append(count + noise)
    t = t_max
    t_bin = t_max
    m = max_bin(t_max)
    length_max_noise = 2**m
    while t < len(stream):
        old_noise = alpha[m]
        alpha[m] = 0
        for sub_t in np.arange(1, length_max_noise):
            count += stream_item_reader(stream[t])
            i = min_bin(t_bin)
            alpha[:i] = np.zeros_like(alpha[:i])
            alpha[i] = np.random.laplace(0, 1/epsilon_prime)
            index_noise = ind_bin(t_bin)
            noise = alpha[:len(index_noise)][index_noise].sum()
            cum_sum_dp.append(count + noise + old_noise)
            t += 1
            t_bin += 1
            if t >= len(stream):
                break
        t_bin -= length_max_noise
    return cum_sum_dp


def generate_noise(rng, eps, rvidx):
    """
    generate DP noise
    * rng   : the random number generator
    * eps   : the epsilon parameter of the Laplace distribution
    * rvidx : an rv_index structure
    """
    rv_cnt = rvidx.rv_cnt
    N = rvidx.N
    L = rvidx.L
    eta = rng.laplace(0, 1/eps, size=rv_cnt)
    sumeta = np.zeros(N)
    for i in range(N):
        for j in range(L):
            sumeta[i] = sumeta[i] + eta[rvidx.il_to_rv[i, j]]
    return (eta, sumeta)


def generate_data_poisson(rng, N, poisson_lambda):
    """
    generate data according to the Poisson model:
    rng : the random number generator to be used
    N   : the number of "time steps"
    poisson_lambda : poisson process parameter
    """
    return rng.poisson(poisson_lambda, N)


def generate_data_multinom(rng, N, n):
    """
    generate data according to the multinomial model:
    rng : the random number generator to be used
    N   : the number of "time steps"
    n   : number of events to happen in the N time steps
    """
    s = np.zeros(N)
    for i in range(n):
        j = rng.integers(N)
        s[j] = s[j] + 1
    return s
