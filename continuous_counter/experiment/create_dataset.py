from random import random
from fysom import Fysom


class FysomError(Exception):
    """Raised when FysomError."""
    pass


patient_fsm_settings = {'initial': 'reception',
                        'final': 'exit',
                        'events': [
                            {'name': 'wait', 'src': '*', 'dst': '='},
                            {'name': 'needs_examination', 'src': [
                                'reception', 'ward'],
                             'dst': 'examination_unit'},
                            {'name': 'transfer_to_ic',
                             'src': 'examination_unit', 'dst': 'ic'},
                            {'name': 'transfer_to_surgery',
                             'src': ['reception', 'examination_unit'],
                             'dst': 'surgery'},
                            {'name': 'rest', 'src': ['ic', 'surgery'],
                             'dst': 'ward'},
                            {'name': 'healed', 'src': '*', 'dst': 'exit'}
                        ]}

units = [u['dst'] for u in patient_fsm_settings['events']
         if u['dst'] not in ['=', '*']] + [patient_fsm_settings['initial']]


def run_hospital(run_time: int, T_max: int,
                 patient_fsm_settings: dict = patient_fsm_settings) -> tuple:
    """Records entrance and exit in hospital services.
    Parameters
    ----------
    run_time : int
        Length observation.
    T_max : int
        Maximum amount of time a patient can stay in the hospital.
    patient_fsm_settings : dict
        Settings to init a patient as a FSM with Fysom.
    Returns
    -------
    hospital_stream : list
        List of atomic action (none, patient transition or creation)
        recorded in the hospital.
    patients_states : dict
        Dict of patients' ids, states and current FSM state
    """

    streams_keys = units + ['hospital']
    streams = {k: [] for k in streams_keys}
    patient_indexes = []  # list of patients currently in the hospital
    patient_ids = []  # list of all patients that came in the hospital
    patients_states = {}

    patient = Fysom(patient_fsm_settings)
    patient_index = 0
    patient_indexes.append(patient_index)
    patient_ids.append(patient_index)
    streams['hospital'].append((f'patient_{patient_index}', patient.current))
    streams[patient.current].append(f'+,patient_{patient_index}')
    patients_states[f'patient_{patient_index}'] = {'states': [patient.current],
                                                   'patient_fsm': patient}

    for t in range(run_time):
        # trigger an action or not
        if random() < 0.8:
            for s in streams_keys:
                streams[s].append(0)
            patient_index = -1
        # create a new patient or not
        elif random() < 0.8:
            patient = Fysom(patient_fsm_settings)
            patient_index = max(patient_ids)+1
            patient_indexes.append(patient_index)
            patient_ids.append(patient_index)
            streams['hospital'].append((f'patient_{patient_index}',
                                        patient.current))
            streams[patient.current].append(f'+,patient_{patient_index}')
            patients_states[f'patient_{patient_index}'] = \
                {'states': [patient.current], 'patient_fsm': patient}
        # pick a patient in the hospital and trigger transition
        elif len(patient_indexes) > 0:
            patient_index = patient_indexes[int(random()*len(patient_indexes))]
            patient = patients_states[f'patient_{patient_index}']['patient_fsm']
            previous_state = patient.current
            events = list(patient._map.keys())
            event = events[int(random()*len(events))]
            try:
                patient.trigger(event)
            except Exception:
                patient.trigger('wait')
            patients_states[f'patient_{patient_index}']['states'].append(patient.current)
            if previous_state == patient.current:
                streams['hospital'].append(0)
                streams[patient.current].append(0)
            elif patient.current == 'exit':
                streams['hospital'].append((f'patient_{patient_index}', patient.current))
                streams[previous_state].append(f'-,patient_{patient_index}')
                patient_indexes.remove(patient_index)
            else:
                streams['hospital'].append((f'patient_{patient_index}', patient.current))
                streams[patient.current].append(f'+,patient_{patient_index}')
                streams[previous_state].append(f'-,patient_{patient_index}')
        # if none of the above happened
        else:
            for s in streams_keys:
                streams[s].append(0)
            patient_index = -1
        # for every other patient, record inactivity
        for i in [p for p in patient_indexes if p != patient_index]:
            patient = patients_states[f'patient_{i}']['patient_fsm']
            patients_states[f'patient_{i}']['states'].append(patient.current)
            # push to exit if patient's stay is longer than T_max
            if len(patients_states[f'patient_{i}']['states']) >= T_max:
                previous_state = patient.current
                patient.trigger('healed')
                patients_states[f'patient_{i}']['states'].append(patient.current)
                streams['hospital'].append((f'patient_{i}', patient.current))
                streams[previous_state].append(f'-,patient_{i}')
                patient_indexes.remove(i)

    return streams, patients_states


if __name__ == '__main__':
    run_time = 500
    T_max = 20
    streams, patients_states = run_hospital(run_time, T_max)
    import ipdb
    ipdb.set_trace()
