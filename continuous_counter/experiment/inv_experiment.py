import numpy as np
import pandas as pd
import datetime
from joblib import Parallel, delayed
from dp_counter import dp_counter, generate_noise, generate_data_multinom, generate_data_poisson
import sys
sys.path.append("../smooth")
from smooth import smooth_dense, rv_index, binrv_print
#from smooth_parameters import params_explore, params_epsilon, params_lambda
from inv_parameters import params_explore, params_epsilon, params_lambda


def mse(x, y):
    return ((x-y)**2).sum()


#params_list = [params_explore, params_epsilon, params_lambda]
params_list = [params_explore]
norms = [1, 2]


def bin_search(v, N, t) :
    """
    Binary search
    v : vector
    N : len(v)
    t : threshold
    Returns : smallest index r with t \le v[r] (or N if no such index exists)
    """
    low = 0
    high = N
    while (low < high) :        
        mid = (low + high)//2
        if v[mid] < t :
            low = mid+1
        else :
            high = mid
    return low
            

    
def one_exp(rvidx, n, epsilon, seed) :  
    rng = np.random.default_rng(seed=seed)
    N = rvidx.N
    L = rvidx.L
    (eta, sumeta) = generate_noise(rng, epsilon/L, rvidx)
    data = generate_data_multinom(rng, N, n)
    for i in range(1, len(data)) : # make cumulative
        data[i] = data[i] + data[i-1]
    dp_data = data + sumeta
    data = data / n
    dp_data = dp_data / n    
    (sm_data, _) = smooth_dense(dp_data, rvidx, 1)
    dp_err = 0
    sm_err = 0
    for i in range(N):        
        dp_i = bin_search(dp_data, N, data[i])
        sm_i = bin_search(sm_data, N, data[i])
        dp_err = dp_err + (i-dp_i)**2
        sm_err = sm_err + (i-sm_i)**2
    dp_err = dp_err / (N**3)
    sm_err = sm_err / (N**3)
    return (dp_err,sm_err)
    

def run(time_step_range, num_event_range, epsilon_range,
        iter_number, n_jobs, parameters_name, norm=1):
    res = []
    exp_list = []
    exp_id = 0
    for time_step_number in time_step_range:
        rvidx = rv_index(time_step_number)
        for num_event in num_event_range:
            for epsilon in epsilon_range:
                for repeat_iter in range(iter_number):
                    exp_id = exp_id + 1    
                    exp_list.append((rvidx,
                                     {'N' : time_step_number,
                                      'n' : num_event,
                                      'epsilon': epsilon,
                                      'iter'   : repeat_iter,
                                      'exp'    : exp_id}))
                                                    
    def experiment(params):        
        print(params[1])
        (dp_err,sm_err) = one_exp(params[0], params[1]['n'], params[1]['epsilon'], params[1]['iter'])
        res_item = params[1]
        res_item['dp_err'] = dp_err
        res_item['sm_err'] = sm_err
        return res_item
    
    results = Parallel(n_jobs=n_jobs)(delayed(experiment)
                                      (params)
                                      for params in exp_list)

    sum_res = dict()
    for ri in results:
        k = (ri['N'],ri['n'],ri['epsilon'])
        if not(k in sum_res):
            sum_res[k] = {'#' : 0, 'dp_err' : 0.0, 'sm_err' : 0.0}
        sum_res[k]['#'] = sum_res[k]['#'] + 1
        sum_res[k]['dp_err'] = sum_res[k]['dp_err'] + ri['dp_err']
        sum_res[k]['sm_err'] = sum_res[k]['sm_err'] + ri['sm_err']
    for ri in sum_res.items() :
        print(ri[0],",\t",ri[1]['dp_err']/ri[1]['#'],",\t",ri[1]['sm_err']/ri[1]['#'])
    
    df_res = pd.DataFrame.from_records(np.ravel(np.array(results)))
    df_res['timestamp'] = datetime.datetime.now()
    df_res.to_csv(f'results/inv_exp_results_{parameters_name}_norm{norm}.csv')


if __name__ == '__main__':
    for params in params_list:
        run(**params)
    
