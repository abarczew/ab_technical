def stream_item_reader(stream_item):
    if stream_item == 0:
        return 0
    elif type(stream_item) == str:
        if stream_item.split(',')[0] == '+':
            return 1
        else:
            return -1
    else:
        raise('Input type error')
