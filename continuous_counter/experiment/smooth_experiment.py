import numpy as np
import pandas as pd
import datetime
from joblib import Parallel, delayed
from dp_counter import generate_noise
import sys
sys.path.append("../smooth")
from smooth import smooth_dense, rv_index
from smooth_parameters import params_explore, params_epsilon, params_lambda


def mse(x, y):
    return ((x-y)**2).sum()


params_list = [params_explore, params_epsilon, params_lambda]
norms = [1, 2]


def run(time_step_range, poisson_lambda_range, epsilon_range,
        iter_number, n_jobs, parameters_name, norm):
    seed = 42
    rng = np.random.default_rng(seed)
    # get the SeedSequence of the passed RNG
    ss = rng.bit_generator._seed_seq
    # create initial independent states
    child_states = ss.spawn(iter_number)

    def experiment(time_step_range, poisson_lambda_range, epsilon_range,
                   iter_number, n_jobs, parameters_name, norm, seed):
        res = []
        rng = np.random.default_rng(seed)
        for time_step_number in time_step_range:
            rvidx = rv_index(time_step_number)
            for poisson_lambda in poisson_lambda_range:
                for epsilon in epsilon_range:
                    # generate random streams
                    s = rng.poisson(poisson_lambda, time_step_number)
                    s = np.array(list(map(float, s)))
                    count_true = np.cumsum(s)
                    # compute DP count
                    scale = rvidx.L / epsilon
                    _, sumeta = generate_noise(rng, scale, rvidx)
                    count_dp = count_true + sumeta
                    try:
                        count_smooth, _ = smooth_dense(count_dp, rvidx, norm)
                        is_decreasing = any((count_dp[1:] - count_dp[:-1]) < 0)

                        mse_true_dp = mse(count_true, count_dp)
                        mse_true_smooth = mse(count_true, count_smooth)

                        res.append({'lambda': poisson_lambda,
                                    'epsilon': epsilon,
                                    'time_step_number': time_step_number,
                                    'is_decreasing': is_decreasing,
                                    'mse_true_dp': mse_true_dp,
                                    'mse_true_smooth': mse_true_smooth})
                    except IndexError:
                        print(f'indexerror with smooth_1 with {poisson_lambda}\
                         and {time_step_number}')

        return res

    results = Parallel(n_jobs=n_jobs)(delayed(experiment)
                                      (time_step_range, poisson_lambda_range,
                                      epsilon_range, iter_number, n_jobs,
                                      parameters_name, norm, seed)
                                      for seed in child_states)

    df_res = pd.DataFrame.from_records(np.ravel(np.array(results)))
    df_res['timestamp'] = datetime.datetime.now()
    filepath = f'smooth_exp_results_{parameters_name}_norm{norm}.csv'
    print(f'Write {filepath}')
    df_res.to_csv(filepath)


if __name__ == '__main__':
    for params in params_list:
        for norm in norms:
            run(**params, norm=norm)
