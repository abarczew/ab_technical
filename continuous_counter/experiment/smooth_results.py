import pandas as pd
from matplotlib import pyplot as plt


def main():
    #######################################
    # Explore the space of all parameters #
    #######################################

    filepath = 'results/smooth_exp_results_explore.csv'
    outpath = 'results/smooth_exp_results_explore.tex'
    df_res = pd.read_csv(filepath)
    df_plot = df_res[['lambda', 'epsilon', 'time_step_number',
                      'mse_true_dp', 'mse_true_smooth']]
    df_plot = df_plot.groupby(['time_step_number', 'lambda', 'epsilon']).mean()
    df_plot['dp_smooth_rate'] = (df_plot.mse_true_smooth - df_plot.mse_true_dp)\
        / df_plot.mse_true_dp

    df_plot.drop(['mse_true_dp', 'mse_true_smooth'], axis=1, inplace=True)
    df_plot.unstack(level=2).to_latex(buf=outpath, index=True)

    ################################
    # Explore the space of epsilon #
    ################################

    filepath = 'results/smooth_exp_results_epsilon.csv'
    outpath = 'results/smooth_exp_results_epsilon.png'
    df_eps = pd.read_csv(filepath)

    plt.figure(figsize=(10, 10))
    df_plot = df_eps.groupby(['lambda', 'epsilon']).mean().reset_index()
    df_plot['dp_smooth_rate'] = (df_plot.mse_true_smooth - df_plot.mse_true_dp)\
        / df_plot.mse_true_dp

    for lambda_poisson in df_plot['lambda'].drop_duplicates():
        df = df_plot.loc[df_plot['lambda'] == lambda_poisson]
        plt.plot(df.epsilon, df.dp_smooth_rate,
                 label=f'lambda={round(lambda_poisson, 2)}', marker='o')
    plt.xlabel('Epsilon')
    plt.ylabel('Relative difference of mse between dp and smooth')
    plt.title('Relative difference of mse between dp and smooth count over \
    epsilon values (N=1048576)')
    plt.legend()
    plt.savefig(outpath, bbox_inches='tight')
    plt.close()

    ###############################
    # Explore the space of lambda #
    ###############################

    filpath = 'results/smooth_exp_results_lambda.csv'
    outpath = 'results/smooth_exp_results_lambda.png'

    df_lam = pd.read_csv(filpath)
    plt.figure(figsize=(10, 10))
    df_plot = df_lam.groupby(['lambda', 'epsilon']).mean().reset_index()
    df_plot['dp_smooth_rate'] = (df_plot.mse_true_smooth - df_plot.mse_true_dp)\
        / df_plot.mse_true_dp

    plt.plot(df_plot['lambda'], df_plot.dp_smooth_rate, marker='o')
    plt.xlabel('Lambda')
    plt.ylabel('Relative difference of mse between dp and smooth')
    plt.title('Relative difference of mse between dp and smooth \
     count over lambda values (epsilon=1. and N=1048576)')
    plt.legend()
    plt.savefig(outpath, bbox_inches='tight')
    plt.close()


if __name__ == '__main__':
    main()
