import numpy as np

n_jobs = 8

###########################################################
# set of parameter to explore the space of all parameters #
###########################################################

iter_number = n_jobs * 6
lambda_number = 3
poisson_lambda_range = [10**(i/3) for i
                        in np.linspace(-5.0, 5.0, lambda_number)]
epsilon_number = 4
epsilon_range = [10**(1-i) for i in range(epsilon_number)][::-1]
time_step_range_length = 3
time_step_range = [2**(5*(i+1)) for i in range(time_step_range_length)]

params_explore = {'poisson_lambda_range': poisson_lambda_range,
                  'epsilon_range': epsilon_range,
                  'time_step_range': time_step_range,
                  'iter_number': iter_number,
                  'n_jobs': n_jobs,
                  'parameters_name': 'explore'}

####################################################
# set of parameter to explore the space of epsilon #
####################################################

iter_number = n_jobs * 11
lambda_number = 4
# there are two completely different behavior depending on lambda
poisson_lambda_range = [10**(i/3) for i
                        in np.linspace(-5.0, 5.0, lambda_number)]
poisson_lambda_range = [poisson_lambda_range[i] for i in [0, 2]]
epsilon_range = [0.02, 0.05, 0.2, 0.5, 2., 5., 20., 50.]
# the biggest number of time step increases the effect seen with lambda
time_step_range = [2**(5*(4))]

params_epsilon = {'poisson_lambda_range': poisson_lambda_range,
                  'epsilon_range': epsilon_range,
                  'time_step_range': time_step_range,
                  'iter_number': iter_number,
                  'n_jobs': n_jobs,
                  'parameters_name': 'epsilon'}

###################################################
# set of parameter to explore the space of lambda #
###################################################

iter_number = n_jobs * 11
lambda_number = 8
poisson_lambda_range = [10**(i/3) for i
                        in np.linspace(-5.0, 5.0, lambda_number)]
epsilon_range = [1.]
time_step_range = [2**(5*(4))]

params_lambda = {'poisson_lambda_range': poisson_lambda_range,
                 'epsilon_range': epsilon_range,
                 'time_step_range': time_step_range,
                 'iter_number': iter_number,
                 'n_jobs': n_jobs,
                 'parameters_name': 'lambda'}
