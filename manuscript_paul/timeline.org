#+TITLE: Important dates before defense

Assuming defense is on: September 27th (Wednesday).
Assuming manuscript is finished on: June 30th (Friday).

With these assumptions, there are 7 weeks remaining (as of May 12th).

And there is to do:
- Write background chapter (2 weeks total)
- Update DP-CD chapter (0.5 week total)
- Update DP-GCD chapter (0.5 week total)
- Update Privacy+Fairness chapter (1 week total)
- Write federated chapter (1 week + unknown time for finishing proof)
- Related works (1.5 week)

Total:
2 + 0.5 + 0.5 + 1 + 1 + 1.5 = 6.5 weeks

Priority:
First first draft of background: May 25th
