Bonjour Eric,

Merci pour ces précisions, ce postdoc m'intéresse toujours beaucoup. Le sujet correspond pleinement à mes intérêts, et l'environnement semble très stimulant.
Je réfléchis néanmoins encore à passer quelques mois à l'étranger avant de venir à Paris. Potentiellement aussi sur du FL, dans des thématiques proches de celles du postdoc. Bon, pour être tout à fait transparent, j'y réfléchis surtout car on m'a expliqué que c'était bien vu pour les candidatures CR... car personnellement ça me convient parfaitement de venir à Paris.
Est-ce que tu serais disponible dans les jours à venir pour discuter un peu plus en détails du projet et de la date de début du postdoc ?

Je profite également de ce mail pour te demander si tu accepterais d'être rapporteur de ma thèse ? Ca serait un réel honneur pour moi. Je suis en pleine rédaction, et le manuscrit devrait être finalisé d'ici mi/fin juin, pour une soutenance fin septembre. Ma thèse sera surtout centrée sur l'optimisation privée (autour de mes travaux sur private coordinate descent (ICML 2022) et greedy coordinate descent (AISTATS 2023)), ainsi que sur les liens entre privacy et fairness. Il y aura aussi un peu de FL, d'une part dans la médecine (avec FLamby et d'anciens travaux en collaboration avec le CHU de Lille), et (en principe) d'autre part quelques réflexions autour de la réduction des communications.

Amicalement,
Paul
