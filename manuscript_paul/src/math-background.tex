%\chapter{Mathematical Background}
%\label{cha:math-backgr}

\chapter{Background on Convex Optimization in Machine Learning}
\label{cha:convex-optim}

In supervised learning, models are often trained through empirical
risk minimization. The goal of this problem is to find a model that
minimizes the average of a \emph{loss function} (\ie a function that
evaluates the error of a model) on given training dataset. It is an
optimization problem over a space of models, that we call the
\emph{hypothesis class}. This hypothesis class is generally
parameterized by a real-valued vector, reducing the problem to finding
the parameters of the best model. Therefore, we turn to the study of
algorithms that solve (composite) finite-sum problems of the following
form:
\begin{align}
  \label{prelim:general-optim-pb}
  \tag{$\star$}
  \min_{w \in \cW} \bigg\{ F(w) := f(w) + \psi(w) \bigg\}
  \enspace,
  \enspace~
  \text{ where }
  f(w) = \frac{1}{n} \sum_{i=1}^n f_i(w)
  \enspace,
\end{align}
where $\cW \subset \RR^p$ is a set, and $f_i : \cW \rightarrow \RR$
(for $i \in [n]$) and $\psi: \cW \rightarrow \RR$ are functions. In
machine learning applications, $f_i$ is the loss function on the
$i$-th data record, and $\psi$ is a regularization term, that can be
used to enforce some structure on the model.

In this chapter, we give an overview of the optimization algorithms
that are generally used for solving machine learning problems that fit
in the framework of~\eqref{prelim:general-optim-pb}, under the
assumptions that $\cW$ is closed and convex, each $f_i$ (for
$i \in [n]$) is proper convex and smooth, and $\psi$ is convex (and
not necessarily differentiable). We describe these assumptions, as
well as their most important properties, in
\Cref{sec:describing-functions}. We then give in
\Cref{sec:convex-optimization} an overview of first order methods for
solving~\eqref{prelim:general-optim-pb} under these assumptions. We
choose to focus on first-order methods since these are the most usual
choice in machine learning applications, where at the number $n$ of
functions in the finite sum $f$ and the number $p$ of parameters in
the model are often large.


\input{src/bg/function-regularity.tex}
\input{src/bg/convex-optim.tex}

\chapter{Background on Differential Privacy in Machine Learning}
\label{cha:diff-priv-mach}

\input{src/bg/privacy.tex}
\input{src/bg/private-optim.tex}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
