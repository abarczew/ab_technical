\chapter{Appendix: Background Proofs}

\section{Convergence of \PGD}
\label{sec:convergence-pgd-proofs}


\subsubsection{Descent Lemma}
\label{sec:descent-lemma-1}

\begin{lemma}[Descent Lemma]
  \label{lem:prelim:descent-lemma-prox-gd}
  Let $w^{t}, w^{t+1}$ be two consecutive iterates of
  \Cref{algo:prelim:proximal-gd} with $\gamma \le \tfrac{2}{L}$. Then
  it holds that
  \begin{align}
    F(w^{t+1}) \le F(w^t)
    \enspace.
  \end{align}
\end{lemma}

\begin{proof}[Proof of \Cref{lem:prelim:descent-lemma-prox-gd}]
  \Cref{prop:prelim:implicit-grad-prox} gives a vector
  $\xi^t \in \partial \psi(w^{t+1})$ such that
  $w^{t+1} = w^t - \gamma (\nabla f(w^t) + \xi^t)$. The gradient of
  $f$ at $w^t$ can thus be expressed as
  \begin{align}
    \nabla f(w^t)
    & =
      \tfrac{1}{\gamma} (w^t - w^{t+1}) - \xi^t
      \enspace.
  \end{align}
  Using smoothness of $f$
  (\Cref{prop:prelim:smooth-quad-upper-bound}), we have
  \begin{align}
    f(w^{t+1})
    & \le
      f(w^t)
      + \scalar{ \nabla f(w^t) }{ w^{t+1} - w^t }
      + \tfrac{L}{2} \norm{ w^{t+1} - w^t }^2
    \\
    & =
      f(w^t)
      + \scalar{ \tfrac{1}{\gamma} (w^t - w^{t+1}) - \xi^t }{ w^{t+1} - w^t }
      + \tfrac{L}{2}  \norm{ w^{t+1} - w^t }^2
    \\
    & =
      f(w^t)
      - \scalar{ \xi^t }{ w^{t+1} - w^t }
      + (\tfrac{L}{2} - \tfrac{1}{\gamma})  \norm{ w^{t+1} - w^t }^2
      \enspace.
  \end{align}
  Then, since $\xi^t \in \partial\psi(w^{t+1})$, convexity of $\psi$
  yields (\Cref{prop:prelim:convex:diff-def}):
  \begin{align}
    \psi(w^{t+1})
    & \le
      \psi(w^t)
      + \scalar{ \xi^t }{ w^{t+1} - w^t }
      \enspace.
  \end{align}
  Summing the last two inequalities, and remarking that
  $\tfrac{L}{2} - \tfrac{1}{\gamma} \le 0$, gives the result.
\end{proof}


\begin{corollary}[Last iterate is good]
  \label{cor:prelim:prox-gd-last-vs-sum-iterates} Let $w_0, \dots, w^T$
  be the iterates of \Cref{algo:prelim:proximal-gd}. Then it holds that
  \begin{align}
    T ( F(w^T) - F(w^*))
    & \le
      \sum_{t=1}^T F(w^t) - F(w^*)
      \enspace.
  \end{align}
\end{corollary}

\begin{proof}[Proof of \Cref{cor:prelim:prox-gd-last-vs-sum-iterates}]
  We multiply the result of \Cref{lem:prelim:descent-lemma-prox-gd} for
  $t=0$ to $t=T-1$, multiplying the $t$-th inequality by $t$ to obtain
  \begin{align}
    0
    & \ge
      \sum_{t=0}^{T-1} t F(w^{t+1}) - t F(w^t)
      =
      \sum_{t=0}^{T-1} (t+1) F(w^{t+1}) - t F(w^t) - F(w^{t+1})
      \enspace.
  \end{align}
  After summing the telescoping sum, we obtain
  \begin{align}
    0
    & \ge
      T F(w^T) - \sum_{t=1}^T F(w^t)
      \enspace.
  \end{align}
  Rearranging the terms and subtracting $TF(w^*)$ on both sides gives
  the lemma.

\end{proof}


\subsection{Convex Case}

\begin{theorem}[Convergence of \Cref{algo:prelim:proximal-gd} (Convex functions)]
  \label{thm:prelim:convergence-proximal-gd-convex}
  Let $w^t$, for $t \in [T]$, be the iterates of
  \Cref{algo:prelim:proximal-gd} parameterized with
  $\gamma \le \sfrac{1}{L}$. Let $w^*$ be a minimizer of $F$. Then,
  for all $1 \le t \ge T$, $w^t$ satisfies
  \begin{align}
    \label{eq:prelim:convergence-iterates-proximal-gd-convex}
    F(w^t) - F(w^*)
    & \le
      \frac{1}{2 \gamma t}
      \norm{ w^0 - w^* }^2
      - \frac{1}{2 \gamma t}
      \norm{ w^{t} - w^* }^2
      \enspace.
  \end{align}
\end{theorem}

\begin{proof}[Proof of \Cref{thm:prelim:convergence-proximal-gd-convex}]
  We start by expanding the norm as follows
  \begin{align}
    \norm{ w^{t+1} - w^* }^2
    & \le
      \norm{ w^t - w^* }^2
      + 2 \scalar{ w^{t+1} - w^t }{ w^t - w^* }
      + \norm{ w^{t+1} - w^t }^2
    \\
    & =
      \norm{ w^t - w^* }^2
      + 2 \scalar{ w^{t+1} - w^t }{ w^{t+1} - w^* }
      - \norm{ w^{t+1} - w^t }^2
      \enspace.
  \end{align}
  Since $w^{t+1} - w^t = \gamma (\nabla f(w^t) + \xi^t )$, we obtain
  \begin{align}
    \label{proof:prelim:convergence-proximal-gd-convex:full-ineq}
    \norm{ w^{t+1} - w^* }^2
    & \le
      \norm{ w^t - w^* }^2
      - 2 \gamma \scalar{ \nabla f(w^t) + \xi^t }{ w^{t+1} - w^* }
      - \norm{ w^{t+1} - w^t }^2
    \\
    & \le
      \norm{ w^t - w^* }^2
      - 2 \gamma \scalar{ \xi^t }{ w^{t+1} - w^* }
      - 2 \gamma \scalar{ \nabla f(w^t) }{ w^{t} - w^* }
      \nnlq
      - 2 \gamma \scalar{ \nabla f(w^t) }{ w^{t+1} - w^t }
      - \norm{ w^{t+1} - w^t }^2
      \enspace.
  \end{align}
  Now, by convexity of $f$ and $\psi$, we have the two inequalities
  \begin{align}
    \label{proof:prelim:convergence-proximal-gd-convex:cvx-f}
    - 2 \gamma \scalar{ \nabla f(w^t) }{ w^t - w^* }
    & \le - 2 \gamma ( f(w^t) - f(w^*) )
      \enspace,
    \\
    \label{proof:prelim:convergence-proximal-gd-convex:cvx-psi}
    - 2 \gamma \scalar{ \xi^{t+1} }{ w^{t+1} - w^* }
    & \le - 2 \gamma ( \psi(w^{t+1}) - \psi(w^*) )
      \enspace.
  \end{align}
  Additionally, smoothness of $f$ gives
  \begin{align}
    \label{proof:prelim:convergence-proximal-gd-convex:smooth-f}
    - 2 \gamma \scalar{ \nabla f(w^{t}) }{ w^{t+1} - w^t }
    & \le
      - 2 \gamma ( f(w^{t+1}) - f(w^t) )
      + \gamma L \norm{ w^{t+1} - w^t }^2
      \enspace.
  \end{align}
  Plugging \eqref{proof:prelim:convergence-proximal-gd-convex:cvx-f},
  \eqref{proof:prelim:convergence-proximal-gd-convex:cvx-psi}, and
  \eqref{proof:prelim:convergence-proximal-gd-convex:smooth-f} in
  \Cref{proof:prelim:convergence-proximal-gd-convex:full-ineq} gives
  \begin{align}
    \norm{ w^{t+1} - w^* }^2
    & \le
      \norm{ w^t - w^* }^2
      - 2 \gamma (F( w^{t+1} ) - F( w^* ))
      + (\gamma L - 1) \norm{w^{t+1} - w^t}^2
    \\
    & \le
      \norm{ w^t - w^* }^2
      - 2\gamma (F( w^{t+1} ) - F( w^* ))
      \label{proof:prelim:convergence-proximal-gd-convex:last-ineq}
      \enspace,
  \end{align}
  since $\gamma \le \sfrac{1}{L}$. We now sum
  \eqref{proof:prelim:convergence-proximal-gd-convex:last-ineq} for
  $t=0$ to $T-1$ and use
  \Cref{cor:prelim:prox-gd-last-vs-sum-iterates} to conclude.
\end{proof}


\subsubsection{Convergence for Strongly-Convex Functions}
\label{sec:conv-strongly-conv}


\begin{theorem}[Convergence of \Cref{algo:prelim:proximal-gd} (Strongly-convex functions)]
  \label{thm:prelim:convergence-proximal-gd-strongly-convex}
  Let $w^t$, for $t \in [T]$, be the iterates of
  \Cref{algo:prelim:proximal-gd}. Then, for $t \ge 1$, the iterate
  $w^t$ satisfies
  \begin{align}
    \label{eq:prelim:convergence-iterates-proximal-gd-strongly-convex}
    \norm{ w^{t} - w^* }^2
    & \le
      \left(
      1 - \gamma \mu
      \right)^t
      \norm{ w^0 - w^* }^2
      \enspace.
  \end{align}
\end{theorem}

\begin{proof}[Proof of \Cref{thm:prelim:convergence-proximal-gd-strongly-convex}]
  We start back from the result of
  \Cref{thm:prelim:convergence-proximal-gd-convex}, with
  $w^0 = w^{kt'}$ for some $t' \ge 0$,
  \begin{align}
    F(w^{k(t'+1)}) - F(w^*)
    & \le
      \frac{1}{2 \gamma k}
      \norm{ w^{kt'} - w^* }^2
      - \frac{1}{2 \gamma k}
      \norm{ w^{k(t'+1)} - w^* }^2
      \enspace.
  \end{align}
  Strong convexity of $F$ gives
  $\tfrac{\mu}{2} \norm{ w^t - w^* }^2 \le F(w^t) - F(w^*)$.
  Combining with the previous equation, we obtain
  \begin{align}
    (1 + 2 \gamma \mu k)
    \norm{ w^{k(t'+1)} - w^* }^2
    & \le
      \norm{ w^{kt'} - w^* }^2
      \enspace.
  \end{align}
  We now divide by $1+2\gamma\mu k$ and remark that for $0 < x \le 1$,
  $\tfrac{1}{1+x} \le 1 - \tfrac{x}{2}$ to obtain
  \begin{align}
    \norm{ w^{k(t'+1)} - w^* }^2
    & \le
      \left( 1 - \gamma \mu k \right)
      \norm{ w^{kt'} - w^* }^2
      \enspace.
  \end{align}
  And taking $k=1$ gives the result. Nonetheless, we also note that
  $k = \lceil\sfrac{1}{2\gamma\mu}\rceil$ gives
  \begin{align}
    \norm{ w^{k(t'+1)} - w^* }^2
    & \le
      \tfrac{1}{2} \norm{ w^{kt'} - w^* }^2
      \enspace,
  \end{align}
  which is a technique that will arise in multiple proof in the next
  chapter.
\end{proof}


\section{Convergence of \PCD}
\label{sec:convergence-pcd}


\subsubsection{Descent Lemma}

\begin{lemma}[Descent Lemma]
%  \label{lem:prelim:descent-lemma-prox-gd}
  Let $w^{t}, w^{t+1}$ be two consecutive iterates of
  \Cref{algo:prelim:proximal-gd} with $\gamma \le \tfrac{2}{L}$. Then
  it holds that
  \begin{align}
    F(w^{t+1}) \le F(w^t)
    \enspace.
  \end{align}
\end{lemma}

\begin{proof}[Proof of \Cref{lem:prelim:descent-lemma-prox-gd}]
  By smoothness of $f$ (\Cref{prop:prelim:smooth-quad-upper-bound}),
  \begin{align}
    f(w^{t+1})
    & \le
      f(w^t)
      + \scalar{ \nabla_j f(w^t) }{ w_j^{t+1} - w_j^t }
      + \tfrac{1}{2} \norm{w_j^{t+1} - w_j^t}_L^2
      \enspace.
  \end{align}
  The implicit characterization of the proximity operator
  (\Cref{prop:prelim:implicit-grad-prox}) gives
  $\xi_j^t \in \partial \psi_j (w_j^{t+1})$ such that
  \begin{align}
    w_j^{t+1}
    & =
      w_j^t
      - \gamma_j (\nabla_j f(w^t) + \xi_j^t)
      \enspace.
  \end{align}
  We can thus replace the gradient above and obtain
  \begin{align}
    f(w^{t+1})
    & \le
      f(w^t)
      + \scalar{ \gamma_j (w_j^t - w_j^{t+1}) - \xi_j^t }{ w_j^{t+1} - w_j^t }
      + \tfrac{1}{2} \norm{w_j^{t+1} - w_j^t}_L^2
    \\
    & \le
      f(w^t)
      - \scalar{ \xi_j^t }{ w_j^{t+1} - w_j^t }
      \enspace.
  \end{align}
  Finally, convexity of $\psi_j$ gives
  $-\scalar{\xi_j^t}{w_j^{t+1} - w_j^t} \le - (\psi_j(w_j^{t+1}) -
  \psi_j(w_j^t))$, and we obtain
  \begin{align}
    f(w^{t+1}) + \psi(w^{t+1})
    & \le
      f(w^t) + \psi(w^{t+1})
      - (\psi_j(w_j^{t+1}) - \psi_j(w_j^t))
    \\
    & =
      f(w^t) + \psi(w^t)
      \enspace,
  \end{align}
  where the second equality comes from the separability of $\psi$.
\end{proof}


\begin{corollary}[Last iterate is good]
  \label{cor:prelim:prox-cd-last-vs-sum-iterates} Let $w_0, \dots, w^T$
  be the iterates of \Cref{algo:prelim:proximal-gd}. Then it holds that
  \begin{align}
    T ( F(w^T) - F(w^*))
    & \le
      \sum_{t=1}^T F(w^t) - F(w^*)
      \enspace.
  \end{align}
\end{corollary}

\begin{proof}[Proof of \Cref{cor:prelim:prox-cd-last-vs-sum-iterates}]
  See proof of \Cref{cor:prelim:prox-gd-last-vs-sum-iterates}.
\end{proof}


\subsubsection{Convergence for convex functions}


\begin{theorem}[Convergence of \Cref{algo:prelim:proximal-gd} (Convex functions)]
  \label{thm:prelim:convergence-proximal-gd-convex}
  Let $w^t$, for $t \in [T]$, be the iterates of
  \Cref{algo:prelim:proximal-cd} parameterized with
  $\gamma_j \le \sfrac{1}{L_j}$. Let $w^*$ be a minimizer of
  $F$. Then, for all $1 \le t \ge T$, $w^t$ satisfies
  \begin{align}
    \label{eq:prelim:convergence-iterates-proximal-cd-convex}
    F(w^t) - F(w^*)
    & \le
      \frac{p}{2t} \Big( \norm{ w^0 - w^* }_{\Gamma^{-1}}^2 + (1-\tfrac{1}{p})\big(F(w^0) - F(w^*)\big)\Big)
      \nnlq
      - \frac{p}{2t} \Big( \norm{ w^t - w^* }_{\Gamma^{-1}}^2 + (1-\tfrac{1}{p})\big(F(w^t) - F(w^*)\big)\Big)
      \enspace.
  \end{align}
\end{theorem}

\begin{proof}[Proof of \Cref{thm:prelim:convergence-proximal-gd-convex}]
  We measure convergence in the $\Gamma^{-1}$-norm $\norm{\cdot}_{\Gamma^{-1}}$,
  where $\Gamma^{-1} = \diag(\gamma_1^{-1}, \dots, \gamma_p^{-1})$.
  \begin{align}
    & \norm{ w^{t+1} - w^* }_{\Gamma^{-1}}^2
      \le
      \norm{ w^t - w^* }_{\Gamma^{-1}}^2
      + 2 \scalar{ w^{t+1} - w^t }{ w^t - w^* }_{\Gamma^{-1}}
      + \norm{ w^{t+1} - w^t }_{\Gamma^{-1}}^2
      \nonumber
    \\
    & \quad =
      \norm{ w^t - w^* }_{\Gamma^{-1}}^2
      + 2 \scalar{ w^{t+1} - w^t }{ w^{t+1} - w^* }_{\Gamma^{-1}}
      - \norm{ w^{t+1} - w^t }_{\Gamma^{-1}}^2
      \enspace.
  \end{align}
  Since $w^{t+1} - w^t = \gamma_j (\nabla_j f(w^t) + \xi_j^t )$, we obtain
  \begin{align}
    \label{proof:prelim:convergence-proximal-gd-convex:full-ineq}
    \norm{ w^{t+1} - w^* }_{\Gamma^{-1}}^2
    & \le
      \norm{ w^t - w^* }_{\Gamma^{-1}}^2
      - 2 \scalar{ \nabla_j f(w^t) + \xi_j^t }{ w^{t+1} - w^* }
      - \norm{ w^{t+1} - w^t }_{\Gamma^{-1}}^2
      \nonumber
    \\
    & \le
      \norm{ w^t - w^* }_{\Gamma^{-1}}^2
      - 2 \scalar{ \xi_j^t }{ w_j^{t+1} - w_j^* }
      - 2 \scalar{ \nabla_j f(w^t) }{ w_j^{t} - w_j^* }
      \nnlq
      - 2 \scalar{ \nabla_j f(w^t) }{ w_j^{t+1} - w_j^t }
      - \norm{ w_j^{t+1} - w_j^t }^2
      \enspace.
  \end{align}
  Now, by smoothness of $f$ and convexity of $\psi_j$, we have
  \begin{align}
    \label{proof:prelim:convergence-proximal-cd-convex:smooth-f}
    - 2 \scalar{ \nabla_j f(w^{t}) }{ w_j^{t+1} - w_j^t }
    & \le
      - 2 ( f(w^{t+1}) - f(w^t) )
      + \norm{ w_j^{t+1} - w_j^t }_{L}^2
      \nonumber
    \\
%    \label{proof:prelim:convergence-proximal-cd-convex:cvx-psi}
    - 2 \scalar{ \xi_j^t }{ w_j^{t+1} - w_j^* }
    & \le
      - 2 ( \psi_j(w_j^{t+1}) - \psi_j(w_j^*) )
      \enspace.
  \end{align}
  Using the fact that $\gamma_j \le \sfrac{1}{L_j}$ for all
  $j \in [p]$, and taking the expectation over $j$, we obtain
  \begin{align}
    \label{proof:prelim:convergence-proximal-gd-convex:full-ineq-after-smooth}
    & \expect \norm{ w^{t+1} - w^* }_{\Gamma^{-1}}^2
      - \norm{ w^t - w^* }_{\Gamma^{-1}}^2
      \nnl
    & \le
      - \tfrac{2}{p} \scalar{ \nabla f(w^t) }{ w^{t} - w^* }
      - 2 ( f(w^{t+1}) - f(w^t) )
      - \tfrac{2}{p} \sum_{j=1}^p ( \psi_j(w_j^{t+1}) - \psi_j(w_j^*) )
      \nonumber
    \\
    & \le
      - \tfrac{2}{p} (f(w^t) - f(w^*))
      - 2 ( f(w^{t+1}) - f(w^t) )
      - \tfrac{2}{p} \sum_{j=1}^p ( \psi_j(w_j^{t+1}) - \psi_j(w_j^*) )
      \enspace,
  \end{align}
  where the second inequality comes from the convexity of $f$.  The
  crucial remark here is that
  \begin{align}
    \expect \psi(w^{t+1}) - \psi(w^*)
    & =
      \tfrac{p-1}{p} (\psi(w^t) - \psi(w^*))
      + \tfrac{1}{p} \sum_{j=1}^p \psi_j(w_j^{t+1}) - \psi_j(w_j^*)
      \enspace.
  \end{align}
  Combining the two previous inequalities, we obtain
  \begin{align}
    & \expect \norm{ w^{t+1} - w^* }_{\Gamma^{-1}}^2
      - \norm{ w^t - w^* }_{\Gamma^{-1}}^2
      \nnlq
      \le
      - 2 (F(w^{t+1}) - F(w^*) )
      + \tfrac{2 (p-1)}{p} ( F(w^t) - F(w^*) )
      \enspace.
  \end{align}
  We then multiply this inequality by $\sfrac{p}{2}$ and sum it for $t=0$ to
  $T-1$ to obtain
  \begin{align}
    & \tfrac{p}{2} \expect \norm{ w^{T} - w^* }_{\Gamma^{-1}}^2
      - \tfrac{p}{2}  \norm{ w^0 - w^* }_{\Gamma^{-1}}^2
      \nnlq
      \le
      \sum_{t=0}^{T-1}
      (p-1) ( F(w^t) - F(w^*)
      - p (F(w^{t+1}) - F(w^*) )
    \\
    & \quad =
      (p-1) \sum_{t=0}^{T-1} F(w^t)- F(w^{t+1})
      - \sum_{t=0}^{T-1} F(w^{t+1}) - F(w^*)
      \enspace.
  \end{align}
  Summing the telescoping sum and applying
  \Cref{cor:prelim:prox-cd-last-vs-sum-iterates} gives
  \begin{align}
    & \tfrac{p}{2} \expect \norm{ w^{T} - w^* }_{\Gamma^{-1}}^2
      - \tfrac{p}{2}  \norm{ w^0 - w^* }_{\Gamma^{-1}}^2
      \nnlq
      \le
      (p-1) (F(w^0) - F(w^T))
      - T (F(w^T) - F(w^*))
      \enspace,
  \end{align}
  and rearranging the terms gives the result.
\end{proof}

\subsubsection{Convergence for strongly-convex functions}

\begin{theorem}[Convergence of \Cref{algo:prelim:proximal-gd} (Strongly-convex functions)]
  \label{thm:prelim:convergence-proximal-gd-strongly-convex}
  Let $w^t$, for $t \in [T]$, be the iterates of
  \Cref{algo:prelim:proximal-cd} parameterized with
  $\gamma_j \le \sfrac{1}{L_j}$. Let $w^*$ be the unique minimizer of
  $F$. Then, for all $1 \le t \ge T$, $w^t$ satisfies
  \begin{align}
    \label{eq:prelim:convergence-iterates-proximal-cd-strongly-convex}
    & \norm{ w^t - w^* }_{\Gamma^{-1}}^2 + (1-\tfrac{1}{p})\big(F(w^t) - F(w^*)\big)
      \nnlq
      \le
      \Big(1-\frac{\mu_{\Gamma^{-1}}}{4p}\Big)^t
      \Big(
      \norm{ w^0 - w^* }_{\Gamma^{-1}}^2 + (1-\tfrac{1}{p})\big(F(w^0) - F(w^*)\big)
      \Big)
      \enspace.
  \end{align}
\end{theorem}
\begin{proof}[Proof of \Cref{thm:prelim:convergence-proximal-gd-strongly-convex}]
  We start from the convex result
  \begin{align}
    F(w^t) - F(w^*)
    & \le
      \frac{p}{2t} \Big( \norm{ w^0 - w^* }_{\Gamma^{-1}}^2 + (1-\tfrac{1}{p})\big(F(w^0) - F(w^*)\big)\Big)
      \nnlq
      - \frac{p}{2t} \Big( \norm{ w^t - w^* }_{\Gamma^{-1}}^2 + (1-\tfrac{1}{p})\big(F(w^t) - F(w^*)\big)\Big)
      \enspace.
  \end{align}
  After dividing by $\tfrac{p}{2t}$, we obtain
  \begin{align}
    & \norm{ w^t - w^* }_{\Gamma^{-1}}^2
      + (1-\tfrac{1}{p})\big(F(w^t) - F(w^*)\big)
      + \tfrac{2t}{p} \big(F(w^t) - F(w^*)\big)
      \nnlq
      \le
      \norm{ w^0 - w^* }_{\Gamma^{-1}}^2 + (1-\tfrac{1}{p})\big(F(w^0) - F(w^*)\big)
      \enspace.
  \end{align}
  We now remark that, due to $F$'s strong convexity, we have
  \begin{align}
    \tfrac{t}{p} \big( F(w^t)-F(w^*) \big)
    & \ge
      \tfrac{\mu_{\Gamma^{-1}} t}{2p} \norm{ w^t - w^* }_{\Gamma^{-1}}^2
      \enspace.
  \end{align}
  Additionally, we remark that since $F(w^t) - F(w^*) \ge 0$,
  \begin{align}
    (1-\tfrac{1}{p} + \tfrac{t}{p})\big(F(w^t) - F(w^*)\big)
    & \ge
      (1 - \tfrac{1}{p}) (1 + \tfrac{t}{p})
      \big(F(w^t) - F(w^*)\big)
      \enspace,
  \end{align}
  and that since $\mu_{\Gamma^{-1}} \le 1$,
  $1+\tfrac{t}{p} \ge 1+\tfrac{\mu_{\Gamma^{-1}}t}{2p}$. Combining both
  inequalities gives that
  \begin{align}
    & \big( 1 + \tfrac{\mu_{\Gamma^{-1}}t}{2p} \big) \Big(
      \norm{ w^t - w^* }_{\Gamma^{-1}}^2 + (1-\tfrac{1}{p})\big(F(w^t) - F(w^*)\big)
      \Big)
      \nnlq
      \le
      \norm{ w^0 - w^* }_{\Gamma^{-1}}^2 + (1-\tfrac{1}{p})\big(F(w^0) - F(w^*)\big)
      \enspace.
  \end{align}
  Dividing by $1+\tfrac{\mu_{\Gamma^{-1}}t}{p}$, and remarking that for
  $0 < x \le 1$, $\tfrac{1}{1+x} \le 1-\tfrac{x}{2}$ gives
  \begin{align}
    & \norm{ w^t - w^* }_{\Gamma^{-1}}^2 + (1-\tfrac{1}{p})\big(F(w^t) - F(w^*)\big)
      \nnlq
      \le
      (1-\tfrac{\mu_{\Gamma^{-1}}t}{4p})
      \Big(
      \norm{ w^0 - w^* }_{\Gamma^{-1}}^2 + (1-\tfrac{1}{p})\big(F(w^0) - F(w^*)\big)
      \Big)
      \enspace.
  \end{align}
  Take $t=1$ and the result follows by recursion.
\end{proof}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../main"
%%% End:
