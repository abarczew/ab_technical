\section{Differentially Private Machine Learning}
\label{sec:diff-priv-optim-1}

One may think that, as machine learning works with aggregated quantities on possibly large datasets, it does not contain confidential
information. However, as any computation done on a set of data, they
are subject to the rule stated in \Cref{sec:defin-mean-diff}: any
useful computation done on a dataset leaks information on this
dataset. Therefore, special care has to be taken to train these models
while preserving data privacy.

In this section, we give in \Cref{sec:priv-leaks-mach} a quick tour of
existing attacks on machine learning models. These attacks highlight
the reality of privacy leaks, and thus, the necessity of addressing
them. To this end, we introduce the differentially private empirical
risk minimization problem (\DPERM)
\citep{chaudhuri2011Differentially}, which aims at training machine
learning models under differential privacy. Then, we describe some
classical approaches for solving it in \Cref{sec:solving-dperm}, and
discuss their usability in practice. Finally, we describe utility
lower bounds in \Cref{sec:lower-bounds}. Under the usual assumptions
on the objective, these worst-case lower bounds are nearly matched by
the methods proposed in \Cref{sec:solving-dperm}. Nonetheless, we will
see in \Cref{chap:dp-cd} that these lower bounds can be refined when
regularity is measured in a coordinate-wise manner.


\subsection{Privacy Leaks in Machine Learning}
\label{sec:priv-leaks-mach}

Although trained machine learning models aim at finding general
patterns that apply to the complete population, they still tend to
leak some information on their training data. This has been
demonstrated in practice through inference attacks, that try to
reconstruct (part of) the training data. We now describe two types of
such attacks: membership inference attacks, and reconstruction
attacks.

\paragraph{Membership Inference Attacks.}

% \begin{figure}
%   \centering
%   \includegraphics[width=0.8\linewidth,height=12em]{example-image-duck}
%   \caption{Membership Inference Attack with Shadow Models }
%   \label{prelim:fig:shadow-models}
% \end{figure}
% \TODO{ \Cref{prelim:fig:shadow-models}: maybe not very important
% compared to other things. }

Membership inference attacks aim at inferring, from the result of a
query, whether an individual was present in the data or not. These
attacks correspond to the point of view of the attacker that
differential privacy tries to protect against. They were introduced by
\citet{shokri2017Membership}, who proposed to attack a model as a
black box. In this setting, the adversary has a black-box access to
the model (\ie they can query the model with arbitrary, and obtain the
values predicted by the model). Their attack works in three steps: (i)
generate synthetic data that is somewhat similar to the training data,
then (ii) train multiple models on parts of this data, and (iii) train
another classifier on the \emph{prediction of the models from previous
  step}, using as label the membership of the record in the training
data of the model. The resulting model is expected to be able to
distinguish between a point used in training a model and another one.
% to
% distinguish between the data used in training of these models from the
% rest.
% When attribute inference attacks fail, one may still be able to
% identify whether an individual was used in the training of a model or
% not.\footnote{In some sense, this is the ``dual'' problem of
%   differential privacy.} These attacks are called membership inference
% attacks and were first studied by \citet{shokri2017Membership}. Their
% approach is based on training an ``attack classifier'' that takes the
% output of a model as input and predicts if it was used in its
% training \TODO{this sentence is not very clear.}. To train the attack classifier, they generate synthetic
% data and use a fraction of it to train another ``shadow'' model: the
% attack classifier then learns to distinguish between the training and
% test data for this shadow model.
This kind of approach have then notably been studied by
\citet{yeom2018Privacy,truex2019Demystifying,ye2022Enhanced}.  Other
settings, where adversary can do more than simply querying the model
have also been studied
\citep{nasr2019Comprehensive,sablayrolles2019Whitebox,melis2019Exploiting}.

We note that most of the aforementioned works assume that the attacker
possesses a set of candidate records that contains a large fraction of
true training records. This setting is in line with the guarantees of
differential privacy (where the adversary may \emph{know all records
  but one} and still be unable to re-identify anyone), but may not be
overly realistic. The works of
\citet{jayaraman2021Revisiting,carlini2021Membership} studied a harder
(but more realistic) setting where few of the records from the
candidate set are actual training records.

We refer to \citet{hu2022Membership} for a detailed overview of
membership inference attacks.


\paragraph{Reconstruction Attacks.}
Attribute inference attacks aim to reconstruct (part of) the training
data from a trained model or from intermediate computations like
gradients. This threatens individuals' privacy since all their
personal information (present in a private dataset) could be
reconstructed by malicious parties.

% \TODO{could you elaborate on why this is an issue? not
%   obvious to me why this is so bad. Giving a scenerio where it matters
%   would be nice.}

Multiple works have shown that such attacks are possible from
gradients. For instance, consider the federated learning setting,
where multiple agents learn a model collectively using \texttt{FedAvg}
with one local step \citep{mcmahan2017CommunicationEfficient}. The
server asks an agent to compute a gradient $\nabla f(w;d)$, for some
function $f$ that depends on the agent's local data $d$ and some
parameters $w \in \RR^p$. In such setting,
\citet{phong2017PrivacyPreserving,wang2019Inferring,zhu2019Deep}
showed that one can reconstruct the data by solving a problem similar
to this:
% \TODO{so maybe state that it requires the knowledge of the
%   network's architecture and the current parameters to be
%   implemented. When is this realistic?}
\begin{align*}
  \min_{d'} \norm{ \nabla f(w;d') - g }^2
  \enspace,
  \quad
  \text{ where } g = \nabla f(w; d)
  \enspace.
\end{align*}
This can notably be done by the server that orchestrates the training,
who knows everything but the data record. These approaches were
extended to mini-batch \texttt{FedAvg} by
\citet{geiping2020Inverting,wen2022Fishing}. Similarly,
\citet{fowl2022Robbing} proposed to modify models during training so
that the data is obtained completely without having to solve such a
problem.

Finally, some works have studied reconstruction attacks from trained
models. This was notably studied by
\citet{guo2022Bounding,balle2022Reconstructing} in a general
setting. More specific works considered high-dimensional linear models
\citep{paige2021Reconstructing}, generative models
\citep{wang2009Learning,carlini2023Extracting} and language models
\citet{carlini2019Secret,carlini2021Extracting}.

In the remainder of this Chapter, we give an overview of the methods
that can be set up to limit the possibility for an adversary to
perform the attacks we just described.


% \TODO{ sort out the references a bit and write the chapter }

% Membership inference attacks aim at determining, given a model (or
% intermediate computations), whether a given individual was part of the
% training set or not. These were introduced by
% \citet{shokri2017Membership}. They can be seen as the dual problem of
% differential privacy, which aims at preventing adversaries from
% inferring membership. These attacks have been widely studied, for
% instance by


% \citet{nasr2019Comprehensive} : white box attacks

% \citet{ye2022Enhanced} : shadow model and many other + try to understand factors
% \citet{yeom2018Privacy}: link with overfitting
% \citet{truex2019Demystifying}: trying to understand


% \citet{sablayrolles2019Whitebox} : bayesian framework

% \citet{jayaraman2021Revisiting} \citet{carlini2021Membership} : under realistic assumptions

% \citet{melis2019Exploiting}: membership inference in collaborative learning

% \citet{hu2022Membership}: survey

% \subsubsection{Is there any Hope ?}
% \label{sec:there-any-hope}



% \citet{dwork2015Preserving}

% \citet{bassily2016Algorithmic}

% \citet{jung2021New}





\subsection{Differentially Private Empirical Risk Minimization}
\label{sec:diff-priv-empir}

% One possibility to reduce the risk of data leakage described in the
% previous section is to adapt the training algorithms to be
% differentially private. There are many ways of doing that, either by
% modifying the problem itself, or by modifying the training
% procedure.

A general method for training machine learning models in a
differentially private way is differentially private empirical risk
minimization \citep{chaudhuri2011Differentially}. Let $\cX$ be a
feature space and $\cY$ a label space. Suppose that a trusted data
curator has access to a data set
$D = \{d_1, \dots, d_n\} \subseteq (\cX \times \cY)^n$ of $n$
records. To train a model privately, one aims at designing an
$(\epsilon, \delta)$-differentially private algorithm that computes an
approximation $w^{\text{priv}}$ of
\begin{align}
  w^* \in \argmin_{w \in \cW} \bigg\{ F(w) := f(w) + \psi(w)
  \bigg\}
  \enspace,
  \enspace
  \text{ with } f(w) = \frac{1}{n} \sum_{i=1}^n \ell(w; d_i)
  \enspace,
  \label{eq:prelim:dp-erm-eq}
  \tag{$\star'$}
\end{align}
which is a special instance of~\eqref{prelim:general-optim-pb} with
$f_i(w) = \ell(w; d_i)$, for some loss function
$\ell : \cW \times \cX \times \cY \rightarrow \RR$. We assume that
$\cW \subseteq \RR^p$ is a closed convex set, $\ell(\cdot; d)$ is
smooth and proper convex for all $d \in D$, and $\psi$ is proper
convex. The function $f$ is the only function that depends on the
data, and $\psi$ is a regularizer that controls the model's complexity
and structure. In the following, we call \emph{utility} the expected
suboptimality gap. Specifically, if an algorithm outputs $w^{\priv}$,
we measure its utility as $\expect(F(w^{\priv})) - F(w^*)$, where the
expectation is over the randomness of the algorithm.

% In this section, we describe the general privacy-preserving
% setting we consider in this thesis. We will call \emph{utility} a
% measure of ``how close the private solution is to the optimal
% (non-private) one?'' and discuss existing lower bounds on utility
% given a privacy budget. We then discuss a range of existing methods
% that achieve optimal utility in the sense that they match the lower
% bounds.

\subsection{Solving Differentially Private Empirical Risk Minimization}
\label{sec:solving-dperm}

Multiple approaches have been proposed for solving
\eqref{eq:prelim:dp-erm-eq} using differentially private
algorithms. In this section, we describe two these methods: output
perturbation and differentially private stochastic gradient descent
(\DPSGD). We then discuss some practical considerations that are
essential for the real-world use of these mechanisms.

% We then discuss
% the privacy-utility trade-off in differentially private machine
% learning by stating lower bounds on the utility under a fixed
% differential privacy budget.


\subsubsection{Output Perturbation}
\label{sec:output-perturbation-1}



% introduced in \citet{chaudhuri2011Differentially}


% ref for obj pert:
% \citet{kifer2012Private}: generalized obj-pert for sparse problems
% \citet{neel2020Oracle}

One very simple way of finding an $(\epsilon, \delta)$-differentially
private solution to \eqref{eq:prelim:dp-erm-eq} is to compute a solution
$w^*$ and release it using the Gaussian mechanism (see
\Cref{sec:laplace-mechanism}). This approach coined \emph{output perturbation} was studied by
\citet{chaudhuri2008Privacypreserving,chaudhuri2011Differentially} and
later by \citet{lowy2021Output}.

\begin{myalgorithm}
  {Output Pertubation}
  {dataset $D$, noise scale $\sigma > 0$}
  {$w^\priv = w^* + \cN(0, \sigma^2)^p$}
  \label{algo:prelim:output-perturbation}
  Compute $w^* \in \argmin_{w \in \cW} \big\{ F(w) \big\}$.
\end{myalgorithm}

To assess the privacy guarantees of this mechanism, we need to compute
the sensitivity of the function that maps a dataset to a solution
of~\eqref{eq:prelim:dp-erm-eq}. To do so, we need the solution to be
unique, and to derive a bound on how much it can change when one
element of the dataset changes.

\begin{theorem}
  \label{thm:prelim:output-pert-dp}
  Let $\epsilon \ge 0$, $\delta \in [0, 1]$. Assume $f$ is
  differentiable, $L$-Lipschitz, $\mu$-strongly convex, and has a
  finite minimum. Then, \Cref{algo:prelim:output-perturbation} with
  $\sigma^2 = \frac{ 2 \log(1.25/\delta) L^2 }{ \mu^2 n^2 \epsilon^2
    }$ is $(\epsilon, \delta)$-differentially private.
\end{theorem}

\begin{proof}
  To prove this theorem, we study the sensitivity of the function
  \begin{align*}
    g: D \mapsto \argmin_{w \in \cW} F(w)
    \enspace.
  \end{align*}
  First, since $F$ is $\mu$-strongly convex and has a finite minimum,
  it has a unique minimizer, and $g$ is well-defined.  We now let $D$
  and $D'$ be two datasets differing on their first element. Let
  $w_1^*$ and $w_2^*$ be the minimizers of $F(\cdot;D)$ and
  $F(\cdot;D')$ respectively. Since $f$ is differentiable and strongly
  convex, we can use \eqref{eq:prelim:convex:diff-def-upper-bound} to
  obtain
  \begin{align}
    F(w_1^*;D)
     & \le
    F(w_2^*;D) - \tfrac{\mu}{2} \norm{ w_1^* - w_2^* }^2
    \label{eq:prelim:st-cv-output-pert-1}
    \\
    F(w_2^*;D)
     & \le
    F(w_1^*;D) + \scalar{ \nabla F(w_2^*; D) }{ w_2^* - w_1^* }
    - \tfrac{\mu}{2} \norm{ w_1^* - w_2^* }^2
    \label{eq:prelim:st-cv-output-pert-2}
    \enspace.
  \end{align}
  Now we remark that
  $F(w_2^*;D) = F(w_2^*;D') + \tfrac{1}{n} ( \ell(w_2^*; d_1') -
    \ell(w_2^*; d_2')$. Therefore, since $w_1^*$ and $w_2^*$ are the
  minimum of $F(\cdot;D)$ and $F(\cdot;D')$, we have
  $\nabla F(w_2^*;D')=0$ and
  \begin{align}
    \nabla F(w_2^*; D)
     & =
    \tfrac{1}{n} ( \nabla \ell(w_2^*; d_1') - \nabla \ell(w_2^*; d_2') )
    \enspace.
  \end{align}
  Summing \eqref{eq:prelim:st-cv-output-pert-1} and
  \eqref{eq:prelim:st-cv-output-pert-2} and replacing the value of
  $\nabla F(w_2^*; D)$, we obtain
  \begin{align}
    \mu \norm{ w_1^* - w_2^* }^2
     & \le
    \tfrac{1}{n} \scalar{ \nabla \ell(w_2^*; d_1') - \nabla \ell( w_2^* ; d_1 ) }{ w_2^* - w_1^* }
    \le
    \tfrac{2L}{n} \norm{ w_2^* - w_1^* }
    \enspace,
  \end{align}
  which implies that the sensitivity of $g$ is
  $\Delta_2(g) \le \tfrac{2L}{\mu n}$. The theorem follows from the
  differential privacy guarantees of the Gaussian mechanism stated in
  \Cref{bg:priv:gaussian-priv-direct-dp}.
\end{proof}
\Cref{thm:prelim:output-pert-dp} proves that the output perturbation
mechanism is differentially private. Nonetheless, we stress that for
the theorem to hold, the computation of the minimizer of
the~\eqref{eq:prelim:dp-erm-eq} problem has to be \emph{computed
  exactly}. In general, it may be difficult to guarantee the
exactitude of this computation. In the next section, we discuss an
algorithm that directly computes a differentially private value
without relying on the exact computation of the solution.

\begin{remark}
  A very related method is \emph{objective perturbation}
  \citep{chaudhuri2011Differentially}. Instead of finding the true
  value, then perturbing it, the objective is augmented with an
  additive noise term, which guarantees the privacy of the
  solution. These type of algorithms have been studied by
  \citet{kifer2012Private} on sparse problems, and
  \citet{neel2020Oracle} studied it under various sets of assumptions.
\end{remark}






\subsubsection{Differentially Private Stochastic Gradient Descent}
\label{sec:diff-priv-stoch-1}

In this section, we describe the most widely used algorithm for
solving the~\ref{eq:prelim:dp-erm-eq} problem: \emph{differentially
  private stochastic gradient} (\DPSGD). \DPSGD is a variant of the
\SGD algorithm that we described in
\Cref{sec:prox-grad-desc}. Contrary to output perturbation, it can
solve~\eqref{eq:prelim:dp-erm-eq} even on non-strongly-convex losses.

\DPSGD was initially proposed by
\citet{song2013Stochastic,jain2012Differentially}. Then,
\citet{bassily2014Private} proved the optimality of \DPSGD's utility,
and \citet{wang2017Differentially} studied variance-reduced variants
of \DPSGD to improve the efficiency of the algorithm (although for the
same utility). \DPSGD has also been widely studied as a minimizer of
the population risk, see
\citet{duchi2013Local,bassily2019Private,feldman2020Private}.  We
describe the proximal variant of this algorithm, that can handle
non-smooth regularizers.
\begin{myalgorithm}
  {\DPSGD: Differentially Private \SPGD}
  {initial point $w^0$, step sizes $\gamma_0, \dots, \gamma_T > 0$, noise scale $\sigma > 0$}
  {$w^T$}
  \label{algo:prelim:dp-sgd}
  For $t = 0$ to $T-1$:
  \begin{itemize}
    \vspace{-0.5em}
    \item Sample index $i$ uniformly at random in $[n]$
    \item Update
          $\displaystyle w^{t+1} = \prox_{\gamma_t \psi} \big( w^t - \gamma_t
            ( \nabla f_i(w^t) + \eta^t \big)$, where $\eta^t \sim \cN(0, \sigma^2)$.
  \end{itemize}
    \vspace{-0.5em}
\end{myalgorithm}
At each iteration of \DPSGD, we sample some record from the data, and
add Gaussian noise. This allows to give differential privacy
guarantees for \DPSGD.
\begin{theorem}[Adapted from Theorem II.2 in \citet{bassily2014Private}]
  \label{prelim:guarantee-privacy-dp-sgd}
  Let $\epsilon \ge 0$ and $\delta \in [0, 1]$. There exist a value of
  $\sigma^2 = O( \frac{T L^2 \log(1/\delta)}{n^2 \epsilon^2} )$, that
  can easily be computed numerically, such that \DPSGD with parameter
  $\sigma^2$ is $(\epsilon, \delta)$-differentially private.
\end{theorem}

\begin{proof}
  To prove the theorem, we study the Rényi differential privacy of
  \DPSGD, and convert them back to the usual differential privacy.
  Let $\alpha > 0$. From \Cref{bg:priv:gaussian-priv}, each iteration
  of \DPSGD is $(\alpha, \frac{\alpha L^2}{2\sigma^2})$-Rényi
  differentially private. Since each iteration operates on a sample of
  size $\tfrac{1}{n}$ of the data, this guarantee is amplified by a
  factor $O(1/n^2)$ (see \Cref{prelim:sampling-rdp}). Using the
  composition result from \eqref{prelim:dp:composed-mec}, we obtain
  that \DPSGD is $(\alpha, \frac{\alpha T L^2}{2n^2\sigma^2})$-Rényi
  differentially private. Converting back to
  $(\epsilon, \delta)$-differential privacy using
  \Cref{bg:priv:rdp-to-dp} gives the result.
\end{proof}


\begin{remark}
  In \Cref{prelim:guarantee-privacy-dp-sgd}, we write
  $\sigma^2 = O( \frac{T L^2 \log(1/\delta)}{n^2 \epsilon^2} )$
  uniquely to give an intuition on the scale of the noise required for
  \DPSGD to satisfy differential privacy. In practice, this value is
  computed numerically by tuning the parameter $\alpha$ of Rényi
  differential privacy properly.
\end{remark}

% \begin{proof}
%   \DPSGD is a composition of $T$ Gaussian mechanism with parameter
%   $\sigma^2$. Each mechanism operates on a sample of size
%   $\tfrac{1}{n}$ of the data.

%   \TODO{ Write the rest of the proof. }
% \end{proof}

The utility of \DPSGD has first been formally studied by
\citet{bassily2014Private} under the assumption that $f$ is
differentiable (and not necessarily smooth), $L$-Lipschitz and
convex. They use an indicator function, $\psi = \iota_\cW$, so that the iterates are projected on the convex set $\cW$ at each iteration.
\begin{theorem}[Theorem II.4 in \citealp{bassily2014Private}]
  \label{prelim:dp-sgd:utility}
  Let $f$ be differentiable, $L$-Lipschitz and convex, and take
  $\psi = \iota_\cW$ so that $\prox_{\gamma_t \psi}$ is the projection
  on the set $\cW$. Define $w^* \in \argmin_{w \in \cW}$ a minimizer
  of $f$, and denote $\norm{ \cW }_2$ the diameter of $\cW$. Set the
  number of iterations to $T = n^2$, and
  $\sigma^2 = O ( \frac{ L^2 T \log(1/\delta) }{ n^2 \epsilon^2 }
    )$.
  \begin{itemize}
    \item If $F$ is convex, set the step size
          $\gamma_t = \frac{ \norm{ \cW }_2 }{ \sqrt{ t(n^2 L^2 + p \sigma^2
                ) } }$. Then the output of \DPSGD achieves the utility
          \begin{align*}
            \expect( F(w^\priv) ) - F(w^*)
            = O\left( \frac{ L \norm{ \cW }_2 \sqrt{ p \log(1/\delta) } }{ n \epsilon} \right)
            \enspace.
          \end{align*}
    \item If $F$ is $\mu$-strongly-convex with respect to the
          $\ell_2$-norm, set $\gamma_t = \frac{1}{\mu n t}$. Then the output
          of \DPSGD achieves the utility
          \begin{align*}
            \expect( F(w^\priv) ) - F(w^*)
            = O\left( \frac{ L^2 p \log(1/\delta) }{ \mu n^2 \epsilon^2 } \right)
            \enspace.
          \end{align*}
  \end{itemize}
\end{theorem}
\begin{proof}
  The proof relies on the following bounds on the gradients expected
  square norm, for any $w \in \cW$, and $i \sim \cU([n])$,
  \begin{align*}
    \expect( \norm{ \nabla \ell(w; d_i) + \eta^t }^2 )
     & =
    \expect( \norm{ \nabla \ell(w; d_i) }^2 )
    + 2 \expect( \scalar{ \nabla \ell(w; d_i) }{ \eta^t } )
    + \expect( \norm{ \eta^t }^2 )
    \\
     & \le
    L^2 + p \sigma^2
    \enspace,
  \end{align*}
  where the inequality comes from the uniform bound
  $\norm{ \nabla \ell(w; d_i) }^2 \le L$, the fact that
  $\expect( \eta^t ) = 0$ and the fact that $\sigma^2$ is the variance
  of each coordinate of $\eta^t$.

  The bounds then follow from the analysis of projected \SGD from
  \citet{shamir2013Stochastic}, that relies on the availability of a
  bound on the squared norm of stochastic gradients. With our choice
  of step sizes, their results state that, for convex functions (see
  their Theorem 2 with
  $c = {\norm{\cW}_2 \epsilon}/{L\sqrt{p \log(1/\delta)}}$), that
  \begin{align*}
    \expect( F(w^T)
     & - F(w^*) )
    \le
    \tfrac{ 8 \sqrt{ p \log(1/\delta) } L \norm{ \cW }_2 \log(T) }{ \epsilon \sqrt{T} }
    + \tfrac{ 4 \sqrt{p}  \epsilon \norm{ \cW }_2 \sigma^2 \log(T) }{ L \sqrt{T} }
    \\
     & =
    \tfrac{ 8 \sqrt{p \log(1/\delta) } L \norm{ \cW }_2 \log(T) }{ \epsilon \sqrt{T} }
    + O \left( \tfrac{ 4 \sqrt{p T \log(1/\delta)} L \norm{ \cW }_2\log(T) }{ n^2 \epsilon } \right)
    \enspace,
  \end{align*}
  and for $\mu$-strongly-convex functions (see their Theorem 1), that
  \begin{align*}
    \expect( F(w^T) - F(w^*) )
     & \le
    \tfrac{34 L^2 \log(T) }{\lambda T}
    + \tfrac{34 p \sigma^2 \log(T) }{\mu T}
    =
    \tfrac{34 L^2 \log(T) }{\lambda T}
    + O\left( \tfrac{ L^2 p \log(T) }{\mu n^2 \epsilon^2 } \right)
    \enspace.
  \end{align*}
  The result follows from setting $T = n^2$ in these two inequalities,
  which balances the two terms.
\end{proof}
This theorem gives a theoretical guarantee on the utility of \DPSGD.
% provided that its parameters are set correctly. While it is
% difficult, in practice, setting \DPSGD's parameters to these values
% gives an upper bound on the utility that can be achieved with
% \DPSGD, under proper tuning of its parameters.
The proof of these utility results highlights the tension between the
optimization error, which decreases with the number of iterations, and
the noise due to privacy, which increases with the number of
iterations. We stress that the latter increases, not because of noise
accumulation, but because of the composition of multiple queries over
the same database: this requires increasing the variance of the noise
to keep a constant privacy budget. We will observe the same phenomenon
in the utility analyses we carry in \Cref{chap:dp-cd,chap:greedy-cd}
for the \DPCD and \DPGCD algorithms.

We note that these utility upper bounds grow polynomially with the
dimension. This is due because, at each iteration, we add noise on
each of the gradient's coordinates. We will see in
\Cref{chap:greedy-cd} that, in some cases, doing coordinate-wise
updates on properly chosen coordinates can reduce this dependence from
polynomial to logarithmic. Nonetheless, under the general assumptions
of \Cref{prelim:dp-sgd:utility}, it is not possible to achieve better
utility (see \Cref{sec:lower-bounds}). Therefore, \emph{under the
  assumptions of \Cref{prelim:dp-sgd:utility}}, \DPSGD optimally
solves the \ref{eq:prelim:dp-erm-eq} problem.


\begin{remark}
  A result similar to \Cref{prelim:dp-sgd:utility} can be obtained for
  \DPSGD with general $\psi$ and fixed step size, based on the
  convergence result of \SPGD from \citet{khaled2020Unified}. This
  requires special attention, but the arguments are the same as in the
  proof of \Cref{prelim:dp-sgd:utility}.
\end{remark}

% Essentially, the noise term adds up with
% the variance term resulting from the convergence result of \SPGD. We
% state this result in the next lemma.

% \begin{lemma}
%   If $F$ is convex, let
%   $R_0^2 = \norm{ w^0 - w^* }^2 + 2\gamma(F(w^0) - F(w^*)) $, we have
%   \begin{align}
%     \expect( F(\bar w^t) - F(w^*) )
%     & \le
%       \frac{ R_0^2 }{ \gamma t }
%       + \frac{ 32 p \gamma T L^2 \log(1/\delta) }{ n^2 \epsilon^2 }
%       \enspace.
%   \end{align}
%   If $F$ is strongly-convex, we run it for $T=\sfrac{ 8 }{\gamma\mu}$
%   iterations, and average the iterates as $\hat w^k$. We do that for
%   $k=1$ to $K$, and obtain
%   \begin{align}
%     F(\hat w^k - w^*) - F(w^*)
%     & \le
%       \frac{1}{2^k} (F(w^0) - F(w^*)) + \frac{32 p \gamma T k L^2 \log(1/\delta)}{n^2\epsilon}
%       \enspace.
%   \end{align}

% \end{lemma}

% \begin{proof}
%   For the strongly convex case, strong convexity of $F$ gives
%   $\tfrac{1}{\gamma}R_0^2 \le (\tfrac{2}{\gamma\mu}+1) (F(w^0)-F(w^*))
%   \le (\tfrac{4}{\gamma\mu}) (F(w^0)-F(w^*))$. After
%   $k=\sfrac{8}{\gamma\mu}$ iterations, we therefore obtain
%   \begin{align}
%     F(\hat w^k - w^*) - F(w^*)
%     & \le
%       \frac{1}{2} (F(w^0) - F(w^*)) + \frac{32 p \gamma T L^2 \log(1/\delta)}{n^2\epsilon^2}
%       \enspace,
%   \end{align}
%   and the result follows by induction.
% \end{proof}
% When choosing the right number of iterations, this lemma allows to
% derive a utility result for \DPSGD.
% \begin{theorem}[Utility of \DPSGD]
%   If $F$ is convex, then set
%   $T = \frac{ R_0 n \epsilon }{ L \gamma \sqrt{ 32 p \log(1/\delta)
%     }}$ to obtain
%   \begin{align}
%     F(\bar w^T) - F(w^*)
%     & \le
%       \frac{ 2 L R_0 \sqrt{ 32 p \log(1/\delta)} }{ n \epsilon}
%       =
%       O \left( \frac{ L \sqrt{p} }{ n \epsilon } \right)
%       \enspace.
%   \end{align}
%   If $F$ is strongly-convex, then use the averaging scheme described
%   above and set $T=\sfrac{8}{\gamma\mu}$,
%   $K = \lceil \log (\frac{\mu n^2 \epsilon^2}{256 p L^2 \log(1/\delta)
%   }) \rceil $, to obtain
%   \begin{align}
%     F(\bar w^K) - F(w^*)
%     & \le
%       \frac{512 p L^2 \log(1/\delta) \log (\frac{\mu n^2 \epsilon^2}{256 p L^2 \log(1/\delta) }) }{ n^2 \epsilon^2 }
%       =
%       O \left( \frac{ L^2 p }{ \mu n^2 \epsilon^2 } \right)
%       \enspace.
%   \end{align}
% \end{theorem}
% Note that the rates match with the ones we described in
% \Cref{sec:lower-bounds}, which implies that \DPSGD achieves the best
% possible utility in general. We will see that under finer assumptions,
% we can derive algortihms that improve over that on favorable problems.

\subsubsection{Differentially Private Machine Learning in Practice}
\label{sec:gradient-clipping}

When trying to train a model privately, several practical challenges
arise. First, the sensitivity estimated from the Lipschitz constant of
the loss typically overestimate the actual norm of the gradient,
preventing algorithms like \DPSGD from finding a meaningful
model. Second, optimization algorithms often crucially depend on the
choice of their hyperparameters, which can be difficult to choose
privately. We give a brief overview of methods that have been proposed
to address these issues in practice.

\paragraph{Gradient Clipping.}
\label{sec:gradient-clipping-1}



% \begin{figure}
%   \TODO{ Make this figure. \\}
%   \centering
%   \includegraphics[width=0.4\linewidth]{example-image-duck}
%   \includegraphics[width=0.4\linewidth]{example-image-duck}
%   \caption{DP-SGD with and without clipping (left plot=convergence,
%     right plot=example in 2D.}
%   \label{fig:prelim:sgd-clip-noclip}
% \end{figure}

% Paragraph to add somewhere: Second, it depends on a global estimate of the sensitivity of the
% objective function, which we obtain by a uniform Lipschitz assumption
% on the objective function (see \Cref{sec:lipsch-sens}). This
% dependence can not be reduced due to the worst-case analysis required
% by differential privacy, but is generally an overestimation of the
% actual sensitivity of the objective function at hand.

In gradient-based algorithms like \DPSGD, we compute a differentially
private approximation of the gradient using, for instance, the
Gaussian mechanism. To do so, we need a bound on the sensitivity of
this gradient, which can be obtained through the Lipschitz assumption,
as we discussed in \Cref{sec:lipsch-sens}). Since this bound needs to
hold uniformly for all gradients of the loss, on all data records, it
can be very high compared to the actual value of the gradients. In
some problems (\eg deep neural networks), it may also be difficult to
compute this constant tightly to begin with. To mitigate these issues,
practical implementations of \DPSGD often use gradient clipping, as
described by \citet{abadi2016Deep}. We set a threshold $C > 0$, and
clip the gradients whose norm is higher than this threshold as
follows:
\begin{align}
  \clip(\nabla \ell(w; d); C)
   & = \min\left( 1, \frac{C}{\norm{\nabla \ell(w; d)}} \right) \nabla \ell(w; d).
\end{align}
This guarantees that the clipped gradient is bounded by $C$. This has
two important consequences in terms of privacy: (i) it guarantees that
\emph{for any record, the gradient will be bounded by $C$} (even if it
has an unexpectedly high value), and (ii) it reduces the sensitivity
of the gradient from $2L$ to $2C$, which can be much lower.

In practice, clipping is indispensable to obtain reasonable utility
while guaranteeing privacy. It is notably used in all implementations
of \DPSGD (see \eg PyTorch Opacus \citep{yousefpour2022Opacus}, and
TensorFlow Privacy \citep{abadi2016TensorFlow}).

Unfortunately, clipping introduces bias in the gradient, as not all
individual gradients are clipped the same. This can be interpreted as
a bias-variance trade-off \citep{amin2019Bounding}: low clipping
induces large bias, but small variance, whereas high clipping induces
little to no bias, but large variance. Nonetheless, this bias is not
always a problem. For instance, \citet{chen2020Understanding}
highlighted that when gradients follow a symmetric distribution,
clipping does not introduce that much bias. It may also be possible to
reduce this bias by setting the clipping threshold adaptively
\citep{pichapati2019AdaCliP,andrew2021Differentially}, although this
is difficult to do using only private information on the
gradients. Finally, we note that the recent work of
\citet{yang2022Normalized,koloskova2023Revisiting} analyzed clipping
in \DPSGD under a relative smoothness assumption, highlighting the
fact that the choice of the threshold $C$ indeed introduces bias.

\paragraph{Hyperparameter Tuning.}
\label{sec:hyperparameter-tuning}

The utility of differentially private optimization algorithms like
\DPSGD is highly dependent on the choice of their
hyperparameters. Classical methods for hyperparameter tuning (\eg
grid-search) require running the algorithm multiple times. Adapting
them to the differentially private setting is thus a challenging
task. A naive solution is to run the algorithm with different sets of
hyperparameters, and use composition results to guarantee privacy of
the complete procedure. While this preserves differential privacy, it
generally destroys utility.

In general, when selecting hyperparameters, we are only interested in
finding the best ones: it should thus be possible to improve the naive
solution by not releasing the outputs of runs that gave poor
results. This idea was first explored by
\citet{chaudhuri2013Stabilitybased}, who used a stability assumption
to reduce the overall budget of the tuning. Later on,
\citet{liu2019Private} proposed a more general method (\ie without the
stability assumption), based on private selection methods, that are
similar to the report noisy max mechanism (see
\Cref{sec:report-noisy-max}). Their work was further extended to Rényi
differential privacy by \citet{papernot2022Hyperparameter}.  Other
approaches have also been proposed, based on adaptive algorithms
\citep{mohapatra2022Role,priyanshu2021Efficient}, or on running
algorithms on subsets of the data to reduce the privacy loss
\citep{koskela2023Practical}.


\subsection{Utility Lower Bounds}
\label{sec:lower-bounds}

For a given privacy budget the problem~\eqref{eq:prelim:dp-erm-eq} can
not be solved up to arbitrary precision. This is due to the fact that
solving a problem too precisely could allow to infer the presence of
some individuals in the training data. For
$(\epsilon, \delta)$-differential privacy, the following theorem
states lower bounds on utility.
\begin{theorem}[Utility Lower Bounds for DP-ERM, see Theorems V.3 and
    V.5 in \citealp{bassily2014Private}]
  \label{prelim:thm:dp-erm-lower-bound}
  Let $n, p > 0$, $\epsilon > 0$ and $\delta = o(1/n)$, and assume
  that $\cW$ is bounded with diameter $\norm{\cW}_2$. For every
  $(\epsilon,\delta)$-differentially private algorithm $\cA$, there
  exists a dataset $D$ such that:

  If $F$ is convex, with probability at least $1/2$,
  \begin{align}
    F(\cA(D)) - F(w^*)
     & =
    \Omega\left( L \norm{ \cW }_2  \min\left(1, \frac{\sqrt{p}}{n \epsilon } \right) \right)
    \enspace.
  \end{align}

  If $F$ is $\mu$-strongly convex (\wrt $\ell_2$-norm), with
  probability at least $1/3$,
  \begin{align}
    F(\cA(D)) - F(w^*)
     & =
    \Omega\left( \frac{ L^2 }{ \mu } \min\left(1, \frac{p}{n^2 \epsilon^2 }\right)  \right)
    \enspace.
  \end{align}
\end{theorem}
These results are based on the results of
\citet{bun2014Fingerprinting}, who studied counting queries under
$(\epsilon,\delta)$-differential privacy. Their results are based on
the work of \citet{boneh1998Collusionsecure,tardos2008Optimal} about
fingerprinting codes. These codes were originally designed to protect
against piracy on proprietary software: each copy is equipped with a
hidden serial number, and pirates aim at making up fake serial
numbers. Since pirates do not know the location of all digits of this
serial number, they can only change some of them: examining the fake
number, they could trace it back to the original copies of the pirates. The
idea behind \citet{bun2014Fingerprinting}'s lower bounds is that
counting queries can be used as a way of finding back these pirates
(leading to reidentification). \citet{bassily2014Private} then further
reduced the DP-ERM problem to computing counting
queries. In \Cref{sec:utility-lower-bounds}, we provide a refined
version of these lower bounds, where Lipschitzness of the objective is
measured in a coordinate-wise manner rather than on the full function.

Note that \citet{bassily2014Private} give similar results for pure
$\epsilon$-differential privacy, based on the work of
\citet{hardt2010Geometry}. We do not state these results since all our
results will be stated in terms of approximate differential privacy.

Finally, we note that \citet{talwar2016Private} proved that the lower
bound from \Cref{prelim:thm:dp-erm-lower-bound} on convex objective
function does not hold when the $\ell_1$ diameter of $\cW$,
$\norm{\cW}_1$ is independent of the dimension. In this case, the
lower bound can be refined to
$F(w^{\priv}) - F(w^*) = \Omega( \tfrac{1}{n^{2/3}} )$. Notably, this
lower bound is matched (up to logarithmic factors) by differentially
private variants of the Frank-Wolfe algorithm
\citep{jaggi2013Revisiting,frank1956Algorithm}. In
\Cref{chap:greedy-cd}, we propose an algorithm that nearly matches
this lower bound even when $\cW$ is unbounded (or has a
$\ell_1$-diameter that depends on the dimension).




% \newpage


% \subsection{Differentially Private Frank-Wolfe}
% \label{sec:diff-priv-frank}


% \TODO{ I keep this there as notes. It should be (i) written and (ii)
%   put in Chapter 5, as it gives important insights on the DP-FW
%   algorithm. }

% \begin{myalgorithm}
%   {\DPFW: Differentially Private Frank-Wolfe}
%   {initial point $w^0$, noise scale $\lambda > 0$, convex set $\cC = \conv(\cS)$}
%   {$w^T$}
%   \label{algo:prelim:dp-sgd}
%   For $t = 0$ to $T-1$:
%   \begin{itemize}
%   \item Compute
%     $ \displaystyle w^+ = \argmin_{s \in S} \bigg\{
%     \scalar{ s }{ \nabla f(w^t) } + \Lap(\lambda) \bigg\} $
%   \item Update
%     $\displaystyle w^{t+1} = (1 - \gamma_t) w^t + \gamma_t w^+$, where
%     $\gamma_t = \frac{2}{t+2}$
%   \end{itemize}
% \end{myalgorithm}


% \citet{frank1956Algorithm}: initial FW paper

% \citet{jaggi2013Revisiting}: general analysis of FW under uncertainty

% \citet{talwar2015Nearly}: FW for LASSO

% \citet{talwar2016Private}, \citet{bassily2021NonEuclidean},
% \citet{asi2021Private}: FW in $\ell_1$ genometry --> mention DP-GCD
% and that we do not need this $\ell_1$ geometry.

% \citet{lacoste-julien2015Global}: block coordinate FW (see appendix
% for a DP variant of it)





%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../main"
%%% End:
