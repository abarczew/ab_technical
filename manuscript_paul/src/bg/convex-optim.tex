\section{Convex Optimization}
\label{sec:convex-optimization}

In this section, we describe the algorithms that are most widely used
when solving Problem~\eqref{prelim:general-optim-pb} in machine
learning applications. In these problems, datasets tend to be large,
and models to be high-dimensional. This has oriented the community
towards the use and study of first-order algorithms, and some of their
variants. Recall that we aim at studying problems of the following
form
\begin{align}
  \tag{$\star$}
  \min_{w \in \cW} \left\{ F(w) := f(w) + \psi(w) \right\}
  \enspace,
  \enspace~
  \text{ where }
  f(w) = \frac{1}{n} \sum_{i=1}^n f_i(w)
  \enspace,
\end{align}
When $F$ is differentiable, the most iconic algorithm for solving this
problem is \emph{gradient descent}, initially proposed by
\citet{cauchy1847Methode,hadamard1908Memoire}. This algorithm
iteratively refines an initial guess $w^0 \in \RR^p$ as follows
\begin{align}
  \label{prelim:algo:GD}
  w^{t+1}
   & = w^t - \gamma \nabla F(w^t)
  \qquad \text{ for $t \ge 0$ }
  \enspace,
  \tag{\GD}
\end{align}
where $\gamma > 0$ is a step size. The first analysis of this
algorithm has been done by \citet{curry1944Method}, who showed that
\GD asymptotically converges to a stationary point of $F$. The method
has then been extensively studied \citep[see for
instance][]{himmelblau1972Applied,kantorovich1982Functional,polyak1987Introduction}.
A prominent special case of this problem is when $\cW$ and $F$ are
convex. Numerous books have been dedicated to this special case. The
most classical ones date from late $20$th or early $21$st centuries
\citep{rockafellar1970Convex,nesterov1994InteriorPoint,nesterov2004Introductory,boyd2004Convex},
and more recent ones
\citep{bubeck2015Convex,nesterov2018Lectures,beck2017FirstOrder,wright2022Optimization,ryu2022LargeScale}.

In this thesis, we focus on these types of convex problems. We assume
that:
\begin{itemize}[leftmargin=5em]
  \item[\bfseries (A1)] The set $\cW$ is closed and convex.
  \item[\bfseries (A2)] The function $f$ is proper convex and smooth.
  \item[\bfseries (A3)] The function $\psi$ is convex (and not necessarily
    differentiable).
\end{itemize}
Note that we do not assume that $\psi$ is differentiable: \GD in
itself is therefore not applicable. Fortunately, this can be dealt
with proximity operators, that we introduced
in~\Cref{sec:proximal-operators}, using the proximal gradient descent
algorithm. In the remainder of this chapter, we will describe and
analyze this algorithm, as well as two important variants. The first
one is a stochastic algorithm, where the gradient $\nabla f$ is
estimated approximately. Such variants are widely used when training
models on very large datasets.  The second one is a coordinate-wise
variant, where we compute only one coordinate of the gradient at a
time: these algorithms will be at the core of this thesis.

% \citet{agarwal2009Informationtheoretic}




% \paragraph{Oracle Complexity}

% \citet{nemirovskij1983Problem}: oracle complexity of gd (check that better)

% \citet{agarwal2009Informationtheoretic}: oracle complexity of sgd for
% (strongly-)convex+lispschitz functions

\subsection{Proximal Gradient Descent}
\label{sec:prox-grad-desc-1}

When the function $\psi$ in \eqref{prelim:general-optim-pb} is not
differentiable, using \GD is naturally not possible. To remedy this,
it is tempting to replace the gradient with a \emph{subgradient} of
$\psi$ at the current point $w^t$. This is known as the
\emph{subgradient method}, and was proposed by
\citet{shor1962Application} (see \citet{polyak1977Subgradient} for
more details). Unfortunately, this method generally suffers from a
slow convergence rate, and tends to lose the structural properties we
aim to enforce using the regularizer (like sparsity when $\psi$ is the
$\ell_1$-norm of the parameters).

These rates can typically be improved if, instead of updating using a
subgradient of $\psi$ at $w^t$, we use a subgradient of $\psi$
\emph{at the next point $w^{t+1}$}. While this certainly seems crazy
at first, this is what proximal gradient descent does! It does so by
exploiting the implicit gradient update property of proximity
operators from \Cref{prop:prelim:implicit-grad-prox}.

The exact formulation of the proximal gradient algorithm builds on two
ideas. The first the use proximity operators for minimizing
functions. This is the idea of the proximal point algorithm, which was
proposed by \citet{martinet1970Egularisation,martinet1972D}, and
studied by \citet{rockafellar1976Monotone}. However, such methods may
be oblivious to the regularity of $f$. This has led
\citet{passty1979Ergodic,bruck1977Weak} to consider forward-backward
splitting schemes, that benefit from the best of both worlds. In these
methods, we do a gradient step, followed by an implicit update through
a proximity operator. This gives the \PGD algorithm, that we list here
as \Cref{algo:prelim:proximal-gd}.
\vspace{-0.5em}
\begin{myalgorithm}
  {\PGD: Proximal Gradient Descent}
  {initial point $w^0$, step size $\gamma$}
  {$w^T$}
  \label{algo:prelim:proximal-gd}
  For $t = 0$ to $T-1$:
  \begin{itemize}
    \item $\displaystyle w^{t+1} = \prox_{\gamma \psi} \big( w^t - \gamma \nabla f(w^t) \big)$
  \end{itemize}
  \vspace{-0.5em}
\end{myalgorithm}
Interestingly, \PGD converges at a rate that is similar to \GD on
smooth functions. \citet{beck2009Fast} proved that \PGD converges at a
rate $1/t$ on convex problems, and
\citet{schmidt2011Convergence,karimi2016Linear} showed that
under strong convexity, \PGD converges linearly. We state the results
more precisely in \Cref{prelim:convergence-proximal-gd}.
\begin{theorem}
  \label{prelim:convergence-proximal-gd}
  Assume $f$ is $M$-smooth, and let $w^*$ be a minimizer of $F$. Let
  $w^t$ be the iterates of \Cref{algo:prelim:proximal-gd} with step
  size $\gamma \le 1/M$. Then, for general convex objectives
  \citep[see Theorem 3.1 in][]{beck2009Fast}:
  \begin{align}
    F(w^t) - F(w^*)
     & \le
    \frac{ \norm{ w^0 - w^* }^2 }{ 2 \gamma t }
    \enspace.
  \end{align}
  If, additionally, $F$ is strongly convex, then (see Proposition 3 in
  \citealp{schmidt2011Convergence}; or Theorem 3.5 in
  \citealp{garrigos2023Handbook})
  \begin{align}
    \norm{ w^t - w^* }^2
     & \le (1 - \tfrac{\gamma\mu}{2})^t \norm{ w^0 - w^* }^2
    \enspace.
  \end{align}
\end{theorem}
\begin{proof}
  We start by expanding the norm
  \begin{align}
    \norm{ w^{t+1} - w^* }^2
    & =
      \norm{ w^{t} - w^* }^2
      + \scalar{w^{t+1} - w^t}{ w^{t} - w^* }
      + \norm{ w^{t+1} - w^t }^2
      \nonumber
    \\
    & =
      \norm{ w^{t} - w^* }^2
      + \scalar{w^{t+1} - w^t}{ w^{t+1} - w^* }
      - \norm{ w^{t+1} - w^t }^2
      \enspace.
      \label{eq:prelim:gd-proof:dev-sq-norm}
  \end{align}
  From \Cref{prop:prelim:implicit-grad-prox}, there exists
  $\xi^t \in \partial \psi(w^{t+1})$ (where $\partial\psi$ is the
  subdifferential of $\psi$, see \Cref{prelim:def:subdiff}) such that
  $w^{t+1} = w^t - \gamma (\nabla f(w^t) + \xi^t)$. We replace
  $w^{t+1}$ by its value in~\eqref{eq:prelim:gd-proof:dev-sq-norm} to
  obtain
  \begin{align}
    \norm{ w^{t+1} - w^* }^2
    & =
      \norm{ w^{t} - w^* }^2
      - 2 \gamma \scalar{ \nabla f(w^t) + \xi^t }{ w^{t+1} - w^* }
      - \norm{ w^{t+1} - w^t }^2
      \enspace.
  \end{align}
  Now, since $f$ is smooth, \Cref{prop:prelim:smooth-quad-upper-bound}
  gives
  \begin{align}
    - 2\gamma \scalar{\nabla f(w^t)}{w^{t+1}-w^t}
    \le
    -2\gamma(f(w^{t+1}) - f(w^t))
    + M\gamma \norm{ w^{t+1} - w^t }^2
      \label{eq:prelim:gd-proof:dev-sq-norm:smooth-f}
    \enspace.
  \end{align}
  And by convexity of $f$ and $\psi$, we have,
  from~\eqref{eq:prelim:convex:diff-def} and the definition of the
  subdifferential (\Cref{prelim:def:subdiff}),
  \begin{align}
    -2\gamma\scalar{ \nabla f(w^t) }{ w^t - w^* }
    & \le -2\gamma (f(w^t) - f(w^*))
      \label{eq:prelim:gd-proof:dev-sq-norm:convex-f}
      \enspace,
    \\
    -2\gamma\scalar{ \xi^t }{ w^{t+1} - w^* }
    & \le - 2\gamma (\psi(w^{t+1}) - \psi(w^*))
      \label{eq:prelim:gd-proof:dev-sq-norm:convex-psi}
      \enspace.
  \end{align}
  Summing~\eqref{eq:prelim:gd-proof:dev-sq-norm:smooth-f},
  \eqref{eq:prelim:gd-proof:dev-sq-norm:convex-f} and
  \eqref{eq:prelim:gd-proof:dev-sq-norm:convex-psi}, then replacing
  in~\eqref{eq:prelim:gd-proof:dev-sq-norm}, we obtain
  \begin{align}
    \norm{ w^{t+1} - w^* }^2
    & \le
      \norm{ w^{t} - w^* }^2
      - 2 \gamma (F(w^{t+1}) - F(w^*))
      + (M\gamma - 1) \norm{ w^{t+1} - w^t }^2
      \nonumber
    \\
    & \le
      \norm{ w^{t} - w^* }^2
      - 2 \gamma (F(w^{t+1}) - F(w^*))
      \enspace,
  \end{align}
  where the second inequality comes from $\gamma \le 1/M$. We now
  distinguish two cases:
  \begin{itemize}
  \item $F$ is convex, then we sum this inequality for $t=0$ to
    $t=T-1$ and sum the telescoping sum to obtain
    \begin{align}
      2\gamma \sum_{t=1}^T F(w^t) - F(w^*) \le \norm{w^0 - w^*}^2
      \enspace.
    \end{align}
    Then, remark that $F(w^t)$ is a decreasing function of $t$ (see
    \eg the proof of Theorem 3.1 in \citet{beck2009Fast}), therefore
    $F(w^t) \le F(w^T)$ for all $t \le T$, and the result follows.
  \item $F$ is $\mu$-strongly convex \wrt $\norm{\cdot}_2$, then
    by~\eqref{eq:prelim:min-global-bound}, we have
    $-2\gamma(F(w^{t+1}) - F(w^*)) \le -\gamma\mu \norm{ w^{t+1} - w^*
    }^2$. This gives the inequality
    \begin{align}
      \label{eq:10}
      (1 + \gamma\mu) \norm{ w^{t+1} - w^* }^2
      \le \norm{ w^{t} - w^* }^2
      \enspace.
    \end{align}
    The result follows from
    $\tfrac{1}{1+\gamma\mu} \le 1-\tfrac{\gamma\mu}{2}$, which holds
    since $\gamma\mu \le 1$.
  \end{itemize}
  % We refer to \Cref{sec:convergence-pgd-proofs} for the proof of the
  % sublinear convergence result for general convex objectives.
  % The convergence result for strongly-convex objectives is however
  % short and beautiful. It relies on the fact that if $w^*$ is the
  % (unique) minimizer of $F$, then there exists a vector
  % $\xi^* \in \partial \psi(w^*)$ such that
  % $w^* = \prox_{\gamma\psi} ( w^* - \gamma \xi^* )$. Therefore, we can
  % use the non-expansiveness of the proximity operator
  % \eqref{prop:prelim:firm-nonexp-prox} to obtain, for $t \ge 0$,
  % \begin{align}
  %   \norm{ w^{t+1} - w^* }^2
  %    & =
  %   \norm{ \prox_{\gamma\psi}(w^t - \gamma\nabla f(w^t)) - \prox_{\gamma\psi}(w^* - \gamma \xi^*)  }^2
  %   \\
  %    & \le
  %   \norm{ w^t - w^* - \gamma( \nabla f(w^t) + \xi^* )  }^2
  %   \\
  %    & =
  %   \norm{ w^t - w^* - \gamma( \nabla f(w^t) - \nabla f(w^*) )  }^2
  %   \enspace,
  % \end{align}
  % where the second inequality comes from the optimality condition
  % $0 = \nabla f(w^*) + \xi^*$. Expanding the square gives
  % \begin{align}
  %    & \norm{ w^{t+1} - w^* }^2
  %   - \norm{ w^t - w^* }^2
  %   \nnlq
  %   \le
  %   - 2 \gamma\scalar{ w^t - w^* }{ \nabla f(w^t) - \nabla f(w^*) }
  %   + \gamma^2 \norm{ \nabla f(w^t) - \nabla f(w^*) }^2
  %   \enspace.
  % \end{align}
  % The result then follows from
  % \eqref{eq:prelim:convex-smooth:grad-dist-ineq} with $w = w^t$ and
  % $w' = w^*$.
  \vspace{-2.3em}
\end{proof}

\Cref{prelim:convergence-proximal-gd} suggests that, both for convex
and strongly-convex functions, setting the step size to $1/M$ is the
best strategy. When the objective function is $\mu$-strongly-convex,
the convergence speed of \PGD is governed by the ratio
\begin{align}
  \label{eq:def:kappa}
  \kappa = \frac{M}{\mu}
  \enspace,
\end{align}
which is called the \emph{condition number} of the problem: \PGD
converges fast on problems with small condition number, and slow on
problems with large condition number.

\subsection{Proximal Stochastic Gradient Descent}
\label{sec:prox-grad-desc}


In many applications, computing the gradient of $f$ is expensive. This
is notably the case in machine learning applications, where $f$
depends on a large dataset. In such cases, it may be sufficient to
compute a stochastic estimate of the gradient, for instance by using
only the gradient of $f_i$ for some $i \in [n]$ instead of the
gradient of $f$. This is the idea of \emph{stochastic gradient
  descent} (\SGD), as introduced by \citet{robbins1951Stochastic}.  On
smooth functions, the non-asymptotic convergence of \SGD was studied
by \citet{bach2011Nonasymptotic}. They notably discuss rules for
choosing adapting step sizes over the iteration of \SGD to guarantee
the convergence of its iterates. Their analysis was refined by
\citet{needell2016Stochastic,gower2019SGD}, improving convergence rate
and relaxing assumptions on the objective function. When the objective
function is composite (\ie $\psi \neq 0$ in
\eqref{prelim:general-optim-pb}), we can consider a proximal variant
of \SGD, that we describe in \Cref{algo:prelim:proximal-gd}.
\begin{myalgorithm}
  {\SPGD: Proximal Stochastic Gradient Descent}
  {initial point $w^0$, step size $\gamma$}
  {$w^T$}
  \label{algo:prelim:proximal-sto-gd}
  \vspace{-0.5em}
  For $t = 0$ to $T-1$:
  \vspace{-0.5em}
  \begin{itemize}
    \item Sample one index $i$ uniformly randomly in $[n]$
    \item Update
      $\displaystyle w^{t+1} = \prox_{\gamma \psi} \big( w^t - \gamma
      \nabla f_i(w^t) \big)$
  \end{itemize}
  \vspace{-1em}
\end{myalgorithm}

The \SPGD algorithm and its variants have notably been studied by
\citet{nitanda2014Stochastic,atchade2017Perturbed,rosasco2020Convergence,cevher2019Linear,gorbunov2020Unified},
The convergence of \SPGD with constant step size can be described in
two different phases: (i) a convergence phase, where the iterates get
closer to a solution, and (ii) an oscillation phase, where iterates
oscillate around a solution due to the variance in the estimation of
the gradient. This is what we describe in the next theorem.
\begin{theorem}[Convergence of \SPGD]
  \label{prelim:convergence-proximal-sgd}
  Assume $f$ is $M$-smooth, and let $w^*$ be a minimizer of $F$. Let
  $w^t$ be the iterates of \Cref{algo:prelim:proximal-sto-gd} with
  step size $\gamma \le 1/8M$. We denote
  $\sigma_*^2 = \expect_s( \norm{ g_s(w^*) }^2 )$ the variance of the
  gradient estimate at the optimum. Then, for general convex
  objectives (see Corollary 11.6 in \citealp{garrigos2023Handbook},
  based the general results of \citealp{khaled2020Unified})
  \begin{align}
    \expect( F(\bar w^t) - F(w^*) )
     & \le
    \frac{ \norm{ w^0 - w^* }^2 + 2\gamma(F(w^0) - F(w^*)) }{ \gamma t }
    + 4 \gamma \sigma_*^2
    \enspace,
  \end{align}
  where $\bar w^t = \sum_{k=1}^t w^k$. If $f$ is $\mu$-strongly
  convex, then (see Corollary A.1 in \citealp{gorbunov2020Unified})
  \begin{align}
    \expect( \norm{ w^t - w^* }^2 )
     & \le
    (1 - \gamma\mu)^t \norm{ w^0 - w^* }^2 + \frac{2\gamma\sigma_*^2}{\mu}
    \enspace.
  \end{align}
\end{theorem}


In both results of \Cref{prelim:convergence-proximal-sgd}, a variance
term remains. This is due to the oscillation phase, where the noise in
the estimation of the gradient dominates, and the iterates remain in a
ball around the optimum. The radius of this ball is determined by the
variance at the optimum $\sigma_*^2$, and the step size. Therefore,
setting smaller step sizes allows finding better solutions, but slows
down the convergence of \SPGD.

We illustrate this phenomenon in \Cref{fig:prelim:gd-sgd}: \SPGD
eventually reaches a plateau, where it does not progress towards the
optimum anymore. The distance from this optimum is determined by the
step size, and as in \PGD, the convergence speed is determined by the
condition number $\kappa = M/\mu$.

We note that this oscillating phenomenon can be compensated for by
using variance reduction schemes. Many schemes have been proposed over
the ten past years (see for instance
\citealp{johnson2013Accelerating,defazio2014SAGA,xiao2014Proximal},
and many many others). We refer to
\citet{gower2020VarianceReduced,gorbunov2020Unified,khaled2020Unified}
for overviews and unified analyses of such methods.

\begin{figure}[t]
  \centering
  \captionsetup[subfigure]{justification=centering}
  \centering
  \begin{subfigure}{0.35\linewidth}
    \centering
    \includegraphics[width=\linewidth]{chap2/gd-vs-sgd-good-cond.pdf}
    \label{fig:prelim:gd-sgd-good-cond}
  \end{subfigure}~~~
  \begin{subfigure}{0.35\linewidth}
    \centering
    \includegraphics[width=\linewidth]{chap2/gd-vs-sgd-bad-cond.pdf}
    \label{fig:prelim:gd-sgd-bad-cond}
  \end{subfigure}~~%
  \begin{subfigure}{0.2\linewidth}
    \centering
    \includegraphics[width=\linewidth]{chap2/legend-gd-vs-sgd.pdf}
    \vspace{3em}
  \end{subfigure}%
  \vspace{-1em}
  \caption{Evolution of the suboptimality gap for \PGD and \SPGD with
    different step sizes. When step sizes are large, \SPGD converges
    faster than \GD in its first iterations, but quickly reaches a
    plateau. With smaller step sizes, it converges slower, but is able
    to find better solutions. For both algorithms, the condition
    number $\kappa = M/\mu$ (defined in~\eqref{eq:def:kappa})
    determines the convergence rate. }
  \label{fig:prelim:gd-sgd}
\end{figure}

In the following of this thesis, we will study differentially private
variants of these algorithms (see \Cref{sec:diff-priv-optim-1}). In
this setting, the variance in the estimation of $f$'s gradient is due
to the privacy constraints, and variance reduction does not allow us
to get rid of this additive term. We, therefore, do not discuss them
further.



% Stochastic methods:


% : non-asymptotic analysis of SGD in convex and strongly convex (probably first analysis?)

% \citet{needell2016Stochastic}: nice analysis (btw there seem to be nice discussions on choice of max Li vs averaged Lis (smoothness constants of individual functions in the finite sum) + importance sampling (linked with sampling of coordinate descent)

% % \citet{gower2019SGD}: improved rates for SGD

% Proximal and stochastic variants:

% \citet{nitanda2014Stochastic}: proximal SGD with acceleration techniques
% \citet{xiao2014Proximal}: SVRG with proximal updates

% \citet{atchade2017Perturbed}: study of proximal SGD
% \citet{rosasco2020Convergence}: convergence of proximal SGD

% \citet{cevher2019Linear}: proposition 2.8 gives linear convergence of
% proximal SGD with constant step size, that is probably the paper to cite


% \citet{gorbunov2020Unified}

\subsection{Proximal Coordinate Descent}
\label{sec:prox-stoch-grad}

In some problems, it may be interesting to update iterates only
\emph{one coordinate at a time}. This is the idea of \emph{coordinate
  descent}. It has two important advantages:
\begin{itemize}
\item In high-dimensional problems, computing one coordinate of the
  gradient is much cheaper than computing the full gradient, which can
  make the method very fast.
\item Updating coordinates one at a time can allow the use of larger
  step sizes.
\end{itemize}
Coordinate descent methods have encountered large success due to their
simplicity and effectiveness
\citep{liu2009Blockwise,friedman2010Regularization,chang2008Coordinate,sardy2000Block},
and have seen a surge of practical and theoretical interest in the
last decade \citep{wright2015Coordinate,shi2017Primer,
  richtarik2014Iteration,fercoq2014Accelerated,tappenden2016Inexact,
  hanzely2020Variance,nutini2015Coordinate,karimireddy2019Efficient}.
This theoretical study started with the works of
\citet{luo1992Convergence,tseng2001Convergence,tseng2009Coordinate},
who studied coordinate descent for non-smooth optimization
problems. Then, \citet{nesterov2010Efficiency} analyzed coordinate
descent with random selection of the updated coordinate for smooth
problems. They derived convergence results in expectation, showing
that coordinate descent algorithms can be extremely efficient on large
scale problems. In general, we refer to
\citet{wright2015Coordinate,shi2017Primer} for a general overview of
results on coordinate descent methods.


To design proximal variants of coordinate descent, we need to
assume that the non-smooth part $\psi$
of~\eqref{prelim:general-optim-pb} is \emph{separable}:
\begin{align}
  \label{prelim:eq:separability}
  \psi(w) = \sum_{j=1}^p \psi_j(w_j) \enspace.
\end{align}
This assumption means that each the function can be split in an
ensemble of functions, that each depend on only one of the
coordinates. This is notably the case of the $\ell_1$-norm and of the
characteristic function of a box-set.

The separability assumption allows to do coordinate-wise proximal
updates. This gives the following proximal stochastic coordinate
descent algorithm.
% \paragraph{Coordinate descent.}
% Coordinate descent (CD) algorithms have a long history in optimization.
% \citet{Luo_Tseng1992,Tseng01,Tseng_Yun09} have shown convergence results for
% (block) CD algorithms for nonsmooth optimization.
% \citet{Nesterov12} later proved a global non-asymptotic $1/k$ convergence
% rate for CD with random choice of coordinates for a convex, smooth objective.
% Parallel, proximal variants were developed by
% \citet{richtarik2014Iteration,fercoq2014Accelerated}, while
% \citet{hanzely2018SEGA} further considered non-separable non-smooth parts.
% \citet{shalev-shwartz2013Stochastic} introduced Dual CD algorithms
% for smooth ERM, showing performance similar to SVRG.
% We refer to \citet{wright2015Coordinate} and \citet{shi2017Primer} for
% detailed reviews on CD.
% Inexact CD was studied by \citet{tappenden2016Inexact}, but their analysis
% requires updates not to increase the objective, which is hardly compatible
% with DP.
% We obtain tighter results for inexact CD with noisy gradients
% (see Remark~\ref{rmq:improvement-inexact-coordinate-descent}).

% \citet{karimi2016Linear}: Theorem 6 gives linear convergence of
% proximal CD.
\begin{myalgorithm}
  {\PCD: Proximal Coordinate Descent}
  {initial point $w^0$, step sizes $\gamma_1, \dots, \gamma_p$}
  {$w^T$}
  \label{algo:prelim:proximal-cd}
  For $t = 0$ to $T-1$:
  \begin{itemize}
    \item Sample index $j$ uniformly randomly in $[p]$
    \item Set $\displaystyle w^{t+1} = w^t$
    \item Update
          $\displaystyle w_j^{t+1} = \prox_{\gamma_j \psi_j} \big( w_j^t - \gamma_j
            \nabla_j f(w^t) \big)$
  \end{itemize}
\end{myalgorithm}
The theoretical convergence properties of this algorithm were notably
studied by
\citet{richtarik2014Iteration,fercoq2014Accelerated,karimi2016Linear}. We
study the convergence rate of \PCD in the following theorem.
\begin{theorem}[Convergence of \PCD]
  \label{prelim:convergence-proximal-cd}
  Assume $f$ is $M$-coordinate-smooth, where
  $M=\diag(M_1, \dots, M_p) \in \RR^{p\times p}$ for
  $M_1, \dots, M_p > 0$, and let $w^*$ be a minimizer of $F$. Let
  $w^t$ be the iterates of \Cref{algo:prelim:proximal-cd} with step
  size $\gamma_j = 1/M_j$ for $j \in [p]$. Then for convex objectives
  (see \citet{richtarik2014Iteration}, Theorem~5)
  \begin{align}
    F(w^t) - F(w^*)
     & \le
    \frac{2p \max\big( F(w^0) - F(w^*), \norm{ w^0 - w^* }_M^2\big) }{ t }
    \enspace.
  \end{align}
  If additionally $F$ is $\mu_M$-strongly convex \wrt
  $\norm{\cdot}_M$, then (see \citet{richtarik2014Iteration}, Theorem
  7)
  \begin{align}
    F(w^t) - F(w^*)
     & \le
    \Big(1 - \frac{\mu_M}{p} \Big)^t (F(w^0) - F(w^*) )
    \enspace.
  \end{align}
\end{theorem}
\begin{figure}[t]
  \captionsetup[subfigure]{justification=centering}
  \centering
  \begin{subfigure}{0.5\linewidth}
    \centering
    \includegraphics[height=4.5cm]{chap2/high-cond.pdf}
    \caption{High condition number $\kappa \approx 37.6$.}
    \label{fig:prelim:convex-upbd-2}
  \end{subfigure}%
  ~
  \begin{subfigure}{0.5\linewidth}
    \centering
    \includegraphics[height=4.5cm]{chap2/low-cond.pdf}
    \caption{Low condition number $\kappa \approx 2.7$.}
    \label{fig:prelim:convex-upbd-1}
  \end{subfigure}
  %
  \caption{Trajectory of coordinate descent (purple~\texttt{+}) and
    gradient descent (black~\texttt{x}) for two quadratic problems
    $f = w^\top A w$ with different condition numbers $\kappa = M/\mu$
    (as defined in~\eqref{eq:def:kappa}), where $M$ and $\mu$ are the
    largest and smallest eigenvalues of the Hessian of $f$. \CD is
    much less sensitive to bad conditioning than \GD: on both
    problems, it finds good solutions fast, while \GD stalls after a
    few iterations. }
  \label{fig:prelim:cd-vs-gd-quadratic}
\end{figure}
\Cref{prelim:convergence-proximal-cd} shows that the convergence rate
of \PCD is determined by $\norm{ w^0 - w^* }_M^2$ and $\mu_M$, for
convex and strongly-convex objectives respectively. These values scale
with the \emph{coordinate-wise smoothness} of the objective
function. As such, if $f$ is $\beta$-smooth and $M$-coordinate-smooth,
$\norm{ w^0 - w^* }_M^2$ can be much smaller than
$\beta \norm{ w^0 - w^* }_2^2$, and $\mu_M$ can be much larger than
$\mu/\beta$. Therefore, \PCD is less sensitive to poor conditioning of
the problem at hand than \PGD and \SPGD. This is due to its ability to
make much larger step sizes on coordinates with small smoothness
constants. We illustrate this phenomenon in
\Cref{fig:prelim:cd-vs-gd-quadratic}. We will show in
\Cref{chap:dp-cd} that this property can be used to improve the
privacy-utility trade-off in differentially private optimization.


\subsection{Greedy Coordinate Descent}
\label{sec:greedy-coord-desc-1}

In some problems, it may be interesting not to choose the updated
coordinate uniformly randomly. One possibility is to choose it as the
one with the largest gradient entry. This strategy is sometimes named
the \emph{Gauss-Southwell rule}, and the corresponding algorithm is
called greedy coordinate descent. This algorithm was notably discussed
by
\citet{luo1992Convergence,tseng2009Coordinate,dhillon2011Nearest}. It
can also be seen as a special case of the steepest descent method (see
Section 9.4.3 in \citealp{boyd2004Convex}). We describe this algorithm
for optimizing the smooth variant of~\eqref{prelim:general-optim-pb}
(with $\psi=0$) in \Cref{algo:prelim:greedy-cd}.

The first theoretical analyses of greedy coordinate descent's
convergence did not show improvement over the stochastic greedy
coordinate descent we described in the previous section. These results
therefore suggest that it is no use to select the coordinate greedily
rather than randomly. But greedy updates do help in many cases. This
is what \citet{dhillon2011Nearest} and \citet{nutini2015Coordinate}
showed by proposing refined convergence results for convex and
strongly-convex objectives, that we state in the following theorem.

\begin{myalgorithm}
  {\GCD: Greedy Coordinate Descent}
  {initial point $w^0$, step sizes $\gamma_1, \dots, \gamma_p > 0$}
  {$w^T$}
  \label{algo:prelim:greedy-cd}
  For $t = 0$ to $T-1$:
  \begin{itemize}
    \item Compute $j = \argmax_{j \in [p]} \Big\{
            \tfrac{1}{M_j} \abs{\nabla_j f(w^t)}^2
            \Big\}$
    \item Set $\displaystyle w^{t+1} = w^t$
    \item Update
          $\displaystyle w_j^{t+1} = w_j^t - \gamma_j \nabla_j f(w^t)$
  \end{itemize}
\end{myalgorithm}

\vspace{0.5em}

\begin{theorem}[Convergence of \GCD]
  \label{prelim:convergence-greedy-cd}
  Assume $f$ is $M$-coordinate-smooth with
  $M = \diag(M_1,\dots,M_P) \in \RR^{p\times p}$ for some
  $M_1, \dots, M_p > 0$, and let $w^*$ be a minimizer of $F$. Let
  $w^t$ be the iterates of \Cref{algo:prelim:greedy-cd} with step size
  $\gamma_j = 1/M_j$. Then, let
  $\displaystyle R_{M,1} = \max_{w\in\RR^p} \min_{w^*\in \cW^*} \left\{
    \norm{w-w^*}_{M,1} \mid f(w) \le f(w^0) \right\}$. For general
  convex objectives (see Lemma 1 in \citealp{dhillon2011Nearest}, or
  Theorem 3 in \citealp{karimireddy2019Efficient})
  \begin{align}
    f(w^t) - f(w^0)
    & \le \frac{R_{M,1}^2}{2 t}
      \enspace.
  \end{align}
  If, additionally, $F$ is $\mu_{M,1}$-strongly convex \wrt the norm
  $\norm{\cdot}_{M,1}$, then (see \citealp{nutini2015Coordinate},
  Section 4)
  \begin{align}
    f(w^t) - f(w^*)
    & \le
      \Big(1 - \mu_{M,1} \Big)^t (F(w^0) - F(w^*) )
      \enspace.
  \end{align}
\end{theorem}
In these results, $R_{M,1}$ and $\mu_{M,1}$ are defined using the
(scaled) $\ell_1$-norm. This allows to get rid of the explicit
dependence on the dimension that appears in the analysis of stochastic
coordinate descent (see
\Cref{prelim:convergence-proximal-cd}). Importantly, since for any
vector $w \in \RR^p$,
$\norm{w}_2 \le \norm{w}_1 \le \sqrt{p} \norm{w}_2$, these result
imply that greedy coordinate descent is always better than stochastic
coordinate descent. Most interestingly, in the best case, \emph{greedy
  coordinate descent enjoys the same rate as gradient
  descent}.\footnote{We refer to the supplementary of
  \citet{nutini2015Coordinate} for examples of problems where gradient
  descent and greedy coordinate descent perform similarly.} This will
be at the core of \Cref{chap:greedy-cd}, where we propose a
differentially private greedy coordinate descent method and formally
analyze its privacy-utility trade-off.

Sometimes, it may still be interesting, theoretically, to use greedy
coordinate descent rather than gradient descent. Indeed, in some
specific settings, it is possible to approximate the greedy update
rule in sublinear time. This can notably be done using fast
nearest-neighbor schemes when fitting (generalized) linear models
\citep{dhillon2011Nearest,nutini2015Coordinate,karimireddy2019Efficient}.
In practice, however, greedy coordinate descent methods are often
slower (in wall-clock time) than their randomized or cyclic
counterparts \citep{massias2017Safe}. We will see in
\Cref{chap:greedy-cd} that the private variant of this algorithm can
obtain near-dimension independent utility, which may be worth the high
computational cost.


Note that the analysis of proximal extensions of greedy coordinate
descent for composite problems is challenging.
\citet{karimireddy2019Efficient} proved convergence rates for
$\ell_1$-regularized and box-constrained problems, using a modified
greedy coordinate algorithm. Nonetheless, we remark that proximal
variants of greedy coordinate (see \eg Section 2.3.3 in
\citealp{shi2017Primer}) seem to work well in practice, even without
such tricks.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../main"
%%% End:
