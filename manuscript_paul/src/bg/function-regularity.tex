\section{Functions Regularity}
\label{sec:describing-functions}

The type of algorithms we use for solving problems
like~\eqref{prelim:general-optim-pb} primarily depends on the
properties of the functions themselves. In this thesis, we
\emph{always} assume that, for $i \in [n]$, $f_i$ is convex and
smooth, and that $\psi$ is convex. This gives us a diverse set of
tools that we can exploit to design and analyze efficient
algorithms. This section is devoted to the description of these tools
and their uses.


\subsection{Differentiability, Gradient and Jacobian}
\label{sec:diff-lipsch}

The first indispensable tool that we need is differentiability. This
allows approximating the local behavior of a function with a linear
function. Such functions can be studied using their \emph{differential
  function}, which we define in this section. We refer to
\citet{fleming2012Functions,garling2014Course} for more details on
differentiable functions.

In all the following, $p,k > 0$ are two integers, and for all integers
$n > 0$, we denote $e_1, \dots, e_n$ the standard basis of $\RR^n$. We
also denote $\cW \subseteq \RR^p$ a subset of $\RR^p$. The
differential function $df$ of a function $f: \RR^p \rightarrow \RR^k$
is defined as follows.
\begin{definition}[Differentiable function]
  A function $f: \cW \rightarrow \RR^k$ is differentiable at a point
  $w \in \cW$ if there exists a linear function
  $df_w: \cW \rightarrow \RR^k$ such that
  \begin{align}
    \lim_{ h \rightarrow 0 }
    \frac{ \norm{ f(w + h) - f(w) - df_{w}(h) } }
    { \norm{h} }
    = 0
    \enspace.
  \end{align}
  When $f$ is differentiable on all its domain $\cW$, we say that $f$
  is differentiable, and we define its differential function as
  $df : w \rightarrow df_w$.
\end{definition}

For $j \in [p]$, the partial derivative of $f$ in the direction of
$e_j$ is $\frac{\partial f}{\partial x_j} : w \mapsto
  df(w)(e_j)$. These partial derivatives fully characterize the
differential of $f$. In particular, when a function
$f: \RR^p \rightarrow \RR$ is real-valued, its differential function
is a linear form.
% From Riesz representation
% theorem, the differential $df(w)$ of $f$ at $w \in \cW$ can
At any point $w \in \cW$, $df(w)$ can be expressed as a dot product
with a specific vector. We call this vector the gradient of $f$ at
$w$.
\begin{definition}[Gradient]
  Let $f : \cW \rightarrow \RR$ be a differentiable real-valued
  function. The gradient $\nabla f(w)$ of $f$ at $w \in \cW$ is the
  only vector such that, for all $h \in \RR^p$,
  $df(w)(h) = \scalar{ \nabla f(w) }{ h }$.  The coefficients of
  $\nabla f(w)$ are the partial derivatives of $f$, and we denote
  $\nabla_j f(w) = \frac{\partial f}{\partial w_j}(w)$ the $j$-th
  coefficient of $\nabla f(w)$:
  \begin{align}
    \nabla f(w)
     & =
    \left(
    \frac{\partial f}{w_1}(w),
    \dots,
    \frac{\partial f}{w_p}(w)
    \right)
    \in \RR^{p}
    \enspace.
  \end{align}
\end{definition}
For vector-valued functions, the notion of gradient can be naturally
extended by constructing a matrix whose lines are the gradients of
each coordinate of the function. This matrix is called the Jacobian
matrix.

\begin{definition}[Jacobian and Hessian Matrix]
  Let $f : \RR^p \rightarrow \RR^k$ be a differentiable function, and,
  for $i \in [k]$, we denote $f_i = e_i^\top f$ the $i$-th coordinate
  of $f$. For all $w \in \RR^p$, the Jacobian matrix $Jf(w)$ of $f$ is
  the matrix of the linear map $df(w)$ in the standard bases of
  $\RR^p$ and $\RR^k$. Its coefficients are
  $J_{i,j}f(w) = df_i(w)(e_j) = \frac{\partial f_i}{\partial x_j}(w)$.
  Specifically, { \everymath={\displaystyle}
      \begin{align}
        Jf(w)
         & =
        \left(
        \begin{matrix}
            \nabla f_1(w) \vphantom{\frac{\partial f_1}{\partial x_1}(w)} \\
            \vdots \vphantom{\frac{\partial f_1}{\partial x_1}(w)}        \\
            \nabla f_k(w) \vphantom{\frac{\partial f_1}{\partial x_1}(w)}
          \end{matrix}
        \right)
        =
        \left(
        \begin{matrix}
            \frac{\partial f_1}{\partial x_1}(w)
             & \frac{\partial f_1}{\partial x_2}(w)
             & \dots
             & \frac{\partial f_1}{\partial x_p}(w)
            \\
            \vdots\vphantom{\frac{\partial f_1}{\partial x_1}(w)}
             & \vdots\vphantom{\frac{\partial f_1}{\partial x_1}(w)}
             & \ddots\vphantom{\frac{\partial f_1}{\partial x_1}(w)}
             & \vdots\vphantom{\frac{\partial f_1}{\partial x_1}(w)}
            \\
            \frac{\partial f_k}{\partial x_1}(w)
             & \frac{\partial f_k}{\partial x_2}(w)
             & \dots
             & \frac{\partial f_k}{\partial x_p}(w)
          \end{matrix}
        \right)
        \in \RR^{k \times p}
        \enspace.
      \end{align}
    }%
  When $f$ is twice differentiable, we define the Hessian $\nabla^2 f$
  of $f$ as the transposed of the Jacobian of $\nabla f$: $\nabla^2 f = J(\nabla f)^\top$.
\end{definition}

Gradients lie at the core of first-order optimization algorithms. We
will see in \Cref{sec:convex-optimization} and throughout the thesis
that they are a key component of \emph{gradient descent},
\emph{coordinate descent} and their differentially private variants.

\subsection{Mahalanobis Norms}
\label{sec:norms}

% \begin{figure}[h]
%   \centering
%   \begin{subfigure}{0.32\linewidth}
%     \includegraphics[width=\linewidth]{example-image-duck}
%     \caption{Test}
%   \end{subfigure}~
%   %
%   \begin{subfigure}{0.32\linewidth}
%     \includegraphics[width=\linewidth]{example-image-duck}
%     \caption{Test}
%   \end{subfigure}

%   \caption{$\ell_1$ and $\ell_2$ unit balls in two dimensions.}
%   \label{fig:prelim:norms}
% \end{figure}

Before jumping to convexity and smoothness, which are the two essential
properties that we will use throughout this thesis, we need to define
a way of measuring the ambient space. The most usual way of doing so
is to use $\ell_q$-norms:
\begin{align}
  \norm{ w }_q = \bigg( { \sum_{j=1}^p} \abs{ w_j }^{q} \bigg)^{1/q}
  \enspace, \qquad
  \text{ for all } w \in \RR^p, \text{ and } q \ge 0
  \enspace.
\end{align}
The $\ell_1$, $\ell_\infty$, and $\ell_2$ norms will be at the core of
the theory we develop in this thesis: they will serve to measure
functions' regularity, and they will allow us to analyze the
convergence of all the algorithms we study. Note that these norms
measure each dimension of the space equally, but the functions we
study may have different properties along each of these dimensions. To
capture this, we define the following scaled norms, inspired by the
work of \citet{mahalanobis1936Generalised}.
\begin{definition}[Mahalanobis Norms]
  \label{prelim:mahalanobis}
  Let $M_1, \dots, M_p > 0$ be positive real numbers and
  $M = \diag(M_1, \dots, M_p) \in \RR^{p \times p}$ be a diagonal
  matrix. For $q \ge 0$, we define
  \begin{align}
    \norm{ w }_{M, q} = \norm{ M^{1/2} w }_q
    \enspace,
    \qquad \qquad
    \norm{ w }_{M^{-1}, q} = \norm{ M^{-1/2} w }_q
    \enspace.
  \end{align}

  % a positive definite matrix (\ie
  % $w^\top M w > 0$ for all non-zero $w \in \RR^p$), and
  % $\scalar{ \cdot }{ \cdot }$ be the usual euclidian dot product on
  % $\RR^p$. We define the two conjugate norms, for all $w \in \RR^p$,
  % where $M^{1/2}$ and $M^{-1/2}$ are matrices whose squares are $M$
  % and $M^{-1}$.
\end{definition}

\begin{figure}[t]
  \captionsetup[subfigure]{justification=centering}
  \centering
  \begin{subfigure}{0.4\linewidth}
    \includegraphics[width=\linewidth]{chap2/norm-1.pdf}
    \caption{$\ell_1$-norm.}
    \label{fig:prelim:norm-1}
  \end{subfigure}~
  %
  \begin{subfigure}{0.4\linewidth}
    \includegraphics[width=\linewidth]{chap2/norm-2.pdf}
    \caption{$\ell_2$-norm.}
    \label{fig:prelim:norm-2}
  \end{subfigure}
  \caption{$\ell_1$ and $\ell_2$ unit balls. In solid black lines, the
    usual unit ball (\ie $M=\Id_2$ in \Cref{prelim:mahalanobis}). In
    dashed purple lines, the unit balls for $M = \diag(0.1, 10)$. }
  \label{fig:prelim:norms}
\end{figure}



These norms account for each dimension differently, depending on the
value of the $M_j$'s. We give examples of the balls of radius $1$ for
various norms in \Cref{fig:prelim:norms}. These norms are at the core
of the analysis of coordinate descent algorithms, as we will discuss
in \Cref{sec:prox-stoch-grad}, \Cref{chap:dp-cd,chap:greedy-cd}.

Norms can be grouped by pairs, that we call conjugate (or dual)
norms. The conjugate norm of a norm $\norm{\cdot}$ is defined as
\begin{align}
  \label{prelim:def-conjugate-norms}
  \norm{ w }^*
   & =
  \sup \{ \scalar{ w }{ x } \mid \norm{ x } \le 1 \}
  \enspace,
\end{align}
where $\scalar{ \cdot }{ \cdot }$ is usual euclidean dot product.
An important special case is the (scaled) $\ell_q$-norms,
whose conjugate norm is
\begin{align}
  \norm{ \cdot}_{M,q}^*
   & =
  \norm{ \cdot }_{M^{-1},q'}
  \enspace,
  \qquad
  \text{ where } q' \text{ is such that } \tfrac{1}{q} + \tfrac{1}{q'} = 1
  \enspace.
\end{align}
Conjugate norms are related to the usual Euclidean dot product through
Hölder's inequality, which is a direct consequence of their definition
as \eqref{prelim:def-conjugate-norms}:
\begin{align}
  \label{prelim:holder}
  \scalar{ w }{ w' }
   & \le
  \norm{ w } \cdot \norm{ w' }^*
  \enspace,
  \qquad
  \text{ for all } w , w' \in \RR^p
  \enspace.
\end{align}
This inequality reduces to the Cauchy-Schwarz inequality when
$\norm{ \cdot } = \norm{ \cdot }^* = \norm{ \cdot }_2$. It will be
very useful in \Cref{sec:greedy-coord-desc-1} and
\Cref{chap:greedy-cd}, where we study greedy coordinate descent
algorithms.


% While these norms indeed measure distance, they give the same weight
% to each coordinate of the vector. In constrast, when studying a
% function $f$, it can have different properties along different
% directions. For instance, the function
% $f: (w_1, w_2) \rightarrow w_1^2 + 0.1 w_2$ has very different
% properties along its first and its second paramater. To account for
% this properly, we define a pair of conjugate norms, that are inspired
% by the work of \citet{mahalanobis1936Generalised}.


\subsection{Convex Sets and Convex Functions}
\label{sec:convexity}

We now turn to the study of a first type of regularity: convexity. In
the following, (strong) convexity will play two major roles. First, it
ensures that any extremal point of a function is a minimum. Second, it
provides global linear (or quadratic) lower bounds on the function,
which is a crucial property for the formal analysis of convex
optimization algorithms. In the rest of this section, we define convex
sets and convex functions, as well as the important properties that
will be used throughout the thesis.


\begin{figure}[t]
  \centering
  \includegraphics[width=0.4\linewidth]{chap2/projection-convex-set.pdf}
  \caption{Projection on a convex set. Black ``\texttt{+}'' are
    initial points, and purple ``\texttt{x}'' are their
    projections. Pairs of projected points are always closer than the
    initial ones.}
  \label{fig:prelim:proj-convex-set}
\end{figure}



\subsubsection{Convex Sets}
\label{sec:convex-sets}

Let $\cW \subseteq \RR^p$ be a subset of $\RR^p$. It is convex if all
segments between any two points of $\cW$ is included in
$\cW$. Formally, this means that for all $w, w' \in \cW$ and
$\lambda \in [0, 1]$, $(1 - \lambda) w + \lambda w' \in \cW$.

Whenever a point $w \in \RR^p$ is not in $\cW$, it can be projected on
$\cW$ using the following projection operator
\begin{align}
  \label{prelim:def-proj-convex-set}
  \Pi_\cW(w) = \argmin_{z \in \cW} \norm{ z - w }_2^2
  \enspace,
\end{align}
where $\norm{\cdot}_2$ is the usual $\ell_2$-norm. By definition, for
any $w \in \cW$, $\Pi_\cW(w)$ returns the element of $\cW$ that is the
closest to $w$, and this element is unique (see \eg Theorem~3.1.10 in
\citealp{nesterov2004Introductory}). These projection operators play a
central role in constrained convex optimization (\ie $\cW \neq \RR^p$
in~\eqref{prelim:general-optim-pb}). Most of the algorithms we will
study indeed use a projection step to meet to constraint.  The crucial
property of $\Pi_\cW$ that makes these methods work, is its
non-expansiveness.
\begin{proposition}[Non-Expansiveness of Convex Projection]
  Let $\cW \subseteq \RR^p$ be a closed convex set, and
  $w, w' \in \RR^p$, then
  \begin{align*}
    \norm{ \Pi_\cW(w) - \Pi_\cW(w') }
     & \le
    \norm{ w - w' }
    \enspace.
  \end{align*}
\end{proposition}
We will prove this property when we introduce the proximity operator
(see \Cref{sec:proximal-operators}), that can be seen as a
generalization of the convex projection. We also give several
geometric examples of this property
in~\Cref{fig:prelim:proj-convex-set}.


\begin{figure}[t]
  \captionsetup[subfigure]{justification=centering}
  \centering
  \begin{subfigure}{0.32\linewidth}
    \includegraphics[width=\linewidth]{chap2/ex-convex-1.pdf}
    \caption{Strongly-convex function \\
      with one minimizer.}
    \label{fig:prelim:convex-1}
  \end{subfigure}~
  %
  \begin{subfigure}{0.32\linewidth}
    \includegraphics[width=\linewidth]{chap2/ex-convex-2.pdf}
    \caption{Convex function \\
      without minimizers.}
    \label{fig:prelim:convex-2}
  \end{subfigure}~
  %
  \begin{subfigure}{0.32\linewidth}
    \includegraphics[width=\linewidth]{chap2/ex-convex-3.pdf}
    \caption{Convex function \\
      with multiple minimizers.}
    \label{fig:prelim:convex-3}
  \end{subfigure}

  \caption{Example of convex functions. The dotted purple line is a
    chord of $f$ and is always above $f$. The dashed black lines are
    lower bounds that follow from (strong) convexity of $f$. In
    \Cref{fig:prelim:convex-2,fig:prelim:convex-3}, the slope of these
    lower bounds are elements of the subdifferential $\partial f$ of
    $f$. }
  \label{fig:prelim:convex}
\end{figure}


\subsubsection{Convex Functions}
\label{sec:convex-functions-1}

Let $f : \RR^p \rightarrow \RR \cup \{\pm\infty\}$ be a
(extended-)real-valued function. The domain of $f$ is
\begin{align*}
  \dom(f)
   & = \{ w \in \RR^p \mid f(w) < +\infty \}
  \enspace.
\end{align*}
When $\dom(f)$ is not empty and $f$ does not take the value $-\infty$,
we say $f$ is \emph{proper}. We are now ready to introduce the notion
of \emph{convex functions}.
\begin{definition}[Convex and strongly-convex function]
  \label{def:prelim:convex:first-def}
  Let $\mu \ge 0$, and $\norm{\cdot}$ be any norm. A function
  $f: \RR^p \rightarrow \RR$ is proper $\mu$-strongly convex
  w.r.t. the norm $\norm{\cdot}$ if $f$ is proper, $\dom(f)$ is
  convex, and for all $w, w' \in \RR^p$ and $\lambda \in [0, 1]$,
  \begin{align}
    \label{eq:prelim:convex:first-def}
    f( (1 - \lambda) w + \lambda w' )
     & \le
    (1 - \lambda) f(w)
    + \lambda f(w')
    + \tfrac{\mu}{2} \lambda (1 - \lambda) \norm{ w - w' }^2
    \enspace,
  \end{align}
  If $f$ is differentiable, the above property is equivalent to
  \begin{align}
    \label{eq:prelim:convex:diff-def}
    f(w')
     & \ge
    f(w)
    + \scalar{ \nabla f(w) }{ w - w' }
    + \tfrac{\mu}{2} \norm{ w - w' }^2
    \enspace,
  \end{align}
  If $f$ is twice differentiable, it is also equivalent to
  \begin{align}
    \label{eq:prelim:convex-hessian}
    \nabla^2 f \succcurlyeq \mu \Id_p
    \enspace.
  \end{align}
  When $\mu = 0$, we simply say that $f$ is convex.
\end{definition}
We illustrate the first two definitions
\eqref{eq:prelim:convex:first-def} and
\eqref{eq:prelim:convex:diff-def} in \Cref{fig:prelim:convex}. In
\Cref{fig:prelim:convex-1}, the function is twice differentiable and
strongly-convex, and satisfies all three definitions with $\mu >
0$. In \Cref{fig:prelim:convex-2,fig:prelim:convex-3} the functions
are convex ($\mu = 0$) but not differentiable, and have respectively
no minimums and an infinite number of minimums.

In the following, we will essentially use
inequality~\eqref{eq:prelim:convex:diff-def}, which provides a linear
(or quadratic if $\mu \neq 0$) lower bound on the value of $f$. Sadly,
this requires $f$ to be \emph{differentiable}, which will not always
be the case (consider \eg the $\ell_1$-norm). We can circumvent this
limitation by defining a proxy for the gradient
in~\eqref{eq:prelim:convex:diff-def} as follows.
\begin{definition}[Subdifferential]
  \label{prelim:def:subdiff}
  Let $f: \RR^p \rightarrow \RR$ be a proper convex function. The
  subdifferential of $f$ at a point $w \in \dom(f)$ is
  \begin{align*}
    \partial f(w)
     & =
    \{ g \in \RR^p \mid f(w') \ge f(w) + \scalar{ g }{ w' - w } \text{ for all } w' \in \RR^p \}
    \enspace.
  \end{align*}
\end{definition}
The subdifferential of a ($\mu$-strongly) convex function $f$ is a
(strongly) monotone operator: let $x, y \in \RR^p$ and
$g_x \in \partial f(x) , g_y \in \partial f(y)$, then
\begin{align}
  \label{prelim:eq-monotone}
  \scalar{ g_x - g_y }{ x - y } \ge \mu \norm{ x - y }^2
  \enspace.
\end{align}

We give examples of elements of the subdifferential of $f$ in
\Cref{fig:prelim:convex-2,fig:prelim:convex-3} (see the slope of the
dotted black lines). Even on points where $f$ is not differentiable,
the subdifferential gives a linear lower bounds of $f$. This property
will be extremely useful when working with proximal operators for
composite problems (\ie when $\psi$ is not differentiable in
\eqref{prelim:general-optim-pb}). We discuss this in more detail in
\Cref{sec:proximal-operators}.

More generally, convexity guarantees that whenever one finds a local
extremum of $f$, it is guaranteed to be a global minimum of $f$.
% The following classes of functions are convex
% \begin{itemize}
% \item Linear functions $w \mapsto Aw$ for some matrix $A$, as
%   they satisfy
%   \begin{align*}
%     A((1-\lambda) w + \lambda w')
%     = (1-\lambda) Aw + \lambda Aw'
%     \enspace.
%   \end{align*}

% \item Norms $\norm{\cdot}: w \mapsto \norm{w}$, as by homogeneity
%   and triangle inequality,
%   \begin{align*}
%     \norm{(1-\lambda) w + \lambda w'}
%     \le (1-\lambda) \norm{w} + \lambda \norm{w'}
%     \enspace.
%   \end{align*}
% \item Quadratic functions $f : w \mapsto w^\top A w + b^\top w$ for
%   some positive definite matrix $A$ and a vector $b$, since
%   \begin{align*}
%     \nabla^2 f
%     & = \tfrac{1}{2} (A + A^\top)
%       \succcurlyeq
%       0
%       \enspace,
%   \end{align*}
%   in particular, the quadratic loss $w \mapsto (x^\top w - y)^2$ for
%   $x \in \RR^p$, $y \in \RR$ is convex.

% \item Exponential function $f: w \mapsto \exp(w)$ is convex since
%   for all $w \in \RR$, $\nabla^2 f(w) = \exp(w) > 0$.
% \end{itemize}

% Moreover, in some circumstances, convex functions can be
% combined. Typically, the sum of two (or more) convex functions is
% still convex, and the composition $h \circ f$ of a nondecreasing
% convex function $h$ and a convex function $f$ is convex.

\begin{proposition}[Minimums are Global]
  Let $f: \cW \rightarrow \RR$ be a convex function. Then the set
  $\argmin(f)$ of minimizers of $f$ is convex, and any local minimum
  of $f$ is a global minimum. If $f$ is strongly convex, then it has
  at most one minimum.

  Moreover, when $f$ is $\mu$-strongly convex, and has a minimizer
  $w^* \in \argmin(f)$, $f$ is uniformly lower bounded by
  \begin{align}
    \label{eq:prelim:min-global-bound}
    f(w)
    \ge
    f(w^*)
    +
    \tfrac{\mu}{2} \norm{ w - w^* }^2
    \enspace,
    \qquad \text{ for all } w \in \RR^p
    \enspace.
  \end{align}
\end{proposition}


% \TODO{ add a remark on Polyak-Lojasiewicz assumptions
%   \citet{polyak1963Gradient,lojasiewicz1963Topological}: introduction
%   of the Polyak-Lojasiewicz condition.  }
% Finally, strong convexity of a function $f$ allows to give an upper
% bound on the distance between
% \begin{proposition}[Sensitivity]
%   Let $f$ be a $\mu$-strongly function, and $g$ a function with a
%   unique minimizer. Let $w_f^*$ and $w_f^*$ be the respective
%   minimizers of $f, g$. Then
%   \begin{align}
%     \label{eq:9}
%     \norm{ w_f^* - w_g^* }
%     & \le
%       \tfrac{1}{\mu} \norm{ \nabla f(w_f^*) - \nabla f(w_g^*) }
%       \enspace.
%   \end{align}
% \end{proposition}
% \begin{proof}
%   The proof follows from \eqref{eq:prelim:convex:diff-def} applied
%   with $w, w' = w_f^*, w_g^*$ and $w, w' = w_g^*, w_f^*$, which
%   respectively give
%   \begin{align*}
%     f(w_f^*)
%     \ge
%     f(w_g^*) + \scalar{ \nabla f(w_g^*) }{ w_f^* - w_g^* } + \tfrac{\mu}{2} \norm{ w_f^* - w_g^* }^2
%     \enspace,
%     \\
%     f(w_g^*)
%     \ge
%     f(w_f^*) + \scalar{ \nabla f(w_f^*) }{ w_g^* - w_f^* } + \tfrac{\mu}{2} \norm{ w_f^* - w_g^* }^2
%     \enspace.
%   \end{align*}
%   Summing these two inequalities gives the result.
% \end{proof}
\begin{figure}[t]
  \captionsetup[subfigure]{justification=centering}
  \centering
  \begin{subfigure}{0.32\linewidth}
    \includegraphics[width=\linewidth]{chap2/ex-convex-upbd-1.pdf}
    \caption{Strongly-convex function \\
      with one minimizer.}
    \label{fig:prelim:convex-upbd-1}
  \end{subfigure}~
  %
  \begin{subfigure}{0.32\linewidth}
    \includegraphics[width=\linewidth]{chap2/ex-convex-upbd-2.pdf}
    \caption{Convex function \\
      without minimizers.}
    \label{fig:prelim:convex-upbd-2}
  \end{subfigure}~
  %
  \begin{subfigure}{0.32\linewidth}
    \includegraphics[width=\linewidth]{chap2/ex-convex-upbd-3.pdf}
    \caption{Convex function \\
      with multiple minimizers.}
    \label{fig:prelim:convex-upbd-3}
  \end{subfigure}

  \caption{ Illustration
    of~\eqref{eq:prelim:convex:diff-def-upper-bound}, where $w$ is
    represented as a star and $w'$ as a circle. When $\nabla f(w)$ is
    known, we can upper bound $f(w)$ using the value of $f$ at $w'$
    and the tangent of $f$ at $w$. }


  % When the gradient at a given point (here represented with a star)
  % is known, translating the tangent at this point to cross another
  % point (here represented by a circle) }

  % Example of convex functions together with the upper bounds (purple
  % dashed line) we obtain
  % using~\eqref{eq:prelim:convex:diff-def-upper-bound} with $w'$
  % being the black dot. }
  \label{fig:prelim:convex-upbd}
\end{figure}

Finally, we remark that convexity also allows deriving upper bounds on
functions, by reformulating \eqref{eq:prelim:convex:diff-def} as
follows, possibly with $\mu = 0$,
\begin{align}
  \label{eq:prelim:convex:diff-def-upper-bound}
  f(w)
   & \le
  f(w')
  + \scalar{ \nabla f(w) }{ w' - w }
  - \tfrac{\mu}{2} \norm{ w - w' }^2
  \enspace, \qquad
  \text{ for all } w, w' \in \RR^p
  \enspace.
\end{align}
We give examples of the upper bounds we obtain in this way in
\Cref{fig:prelim:convex-upbd}. Unfortunately, using these upper bounds
to bound $f(w)$ for some $w \in \RR^p$ requires computing
$\nabla f(w)$, which is generally not directly available. We will see
in~\Cref{sec:proximal-operators} that this inequality can be used to
analyze proximity operators computed on $\psi$, the non-smooth part
of~\eqref{prelim:general-optim-pb}. To analyze algorithms that work on
the full composite problem, we will need more assumptions on $f$, the
differentiable part of~\eqref{prelim:general-optim-pb}.

\subsection{Lipschitzness and Smoothness}
\label{sec:lipschitzness}

\subsubsection{Lipschitzness and Sensitivity}
\label{sec:lipsch-sens}

A simple assumption to obtain an upper bound on a function $f$ is to
use linear upper bounds. This is commonly called Lipschitzness and is
defined as follows.
\begin{definition}[Lipschitzness]
  A function $f: \RR^p \rightarrow \RR^k$ is $L$-Lipschitz with
  respect to two norm $\norm{\cdot}$ and $\norm{\cdot}_f$ on $\RR^p$
  and $\RR^k$ if for all $w, w' \in \dom(f)$,
  \begin{align}
    \norm{ f(w) - f(w') }_f \le L \norm{ w - w' }
    \enspace.
  \end{align}
  We can also measure the Lipschitzness of $f$ along each of its
  parameters. We say $f$ is $(L_1, \dots, L_p)$-coordinate-Lipschitz
  for $L_1, \dots, L_p > 0$ if for all $j \in [p]$, and $w \in \cW$,
  \begin{align}
    \abs{ f(w + te_j) - f(w) } \le L_j \abs{t}
    \enspace.
  \end{align}
\end{definition}
When the function $f$ is differentiable, this Lipschitz property
directly gives an upper bound on the gradient of $f$.
\begin{proposition}[Lemma 2.6 in \citealp{shalev-shwartz2011Online}]
  \label{prelim:lipschitz-and-bounded-gradient}
  Let $f : \cW \subseteq \RR^p \rightarrow \RR^k$ be a differentiable
  convex function. Then $f$ is $L$-Lipschitz with respect to a norm
  $\norm{\cdot}$ if and only for all $w \in \cW$,
  $\norm{\nabla f(w)}^* \le L$, where $\norm{\cdot}^*$ is the dual
  norm of $\norm{\cdot}$.

  Similarly, if $f$ is $(L_1, \dots, L_p)$-coordinate-Lipschitz, then
  for $w \in \cW$, $\abs{ \nabla_j f(w) } \le L_j$.
\end{proposition}
% \begin{proof}
%   $(ii) \Rightarrow (i)$: for all $w,w' \in \RR^p$, the mean value
%   theorem gives
%   \begin{align}
%     \norm{ f(w) - f(w') }'
%     & \le
%       \sup_{t \in [0, 1]} \norm{ \nabla f(w + t(w'-w)) }
%       \norm{w - w'}
%       \le
%       L \norm{w - w'}
%       \enspace.
%   \end{align}


%   $(i) \Rightarrow (ii)$: \TODO{proof}
% \end{proof}
This upper bound will be particularly useful in the design of
differentially private optimization algorithms, that typically require
a bound on the difference between two gradients (see
\Cref{sec:differential-privacy}). Such a bound directly follows from
the Lipschitz property, since if $f: \cW \rightarrow \RR^k$ is
$L$-Lipschitz w.r.t. $\norm{\cdot}$, then for all $w, w' \in \cW$,
\begin{align}
  \label{eq:2}
  \norm{ \nabla f(w) - \nabla f(w') }^*
  & \le \norm{ \nabla f(w) }^* + \norm{ \nabla f(w') }^*
    \le 2L
    \enspace.
\end{align}

\begin{figure}[t]
  \captionsetup[subfigure]{justification=centering}
  \centering
  \begin{subfigure}{0.32\linewidth}
    \includegraphics[width=\linewidth]{chap2/ex-convex-smooth-1.pdf}
    \caption{Quadratic smooth.}
    \label{fig:prelim:convex-upbd-1}
  \end{subfigure}~
  %
  \begin{subfigure}{0.32\linewidth}
    \includegraphics[width=\linewidth]{chap2/ex-convex-smooth-2.pdf}
    \caption{Convex smooth.}
    \label{fig:prelim:convex-upbd-2}
  \end{subfigure}~
  %
  \begin{subfigure}{0.32\linewidth}
    \includegraphics[width=\linewidth]{chap2/ex-convex-smooth-3.pdf}
    \caption{Non-convex smooth.}
    \label{fig:prelim:convex-upbd-3}
  \end{subfigure}

  \caption{Example of convex functions together with the upper bounds
    (purple dashed line) we obtain
    using~\eqref{eq:prelim:convex:diff-def-upper-bound} with $w'$
    being the black dot. }
  \label{fig:prelim:convex-upbd}
\end{figure}

\subsubsection{Smoothness and Coordinate-wise Smoothness}
\label{sec:smoothn-grad-step}

In general, the Lipschitz assumption is too restrictive and does not
provide enough information about the function. Instead of assuming the
\emph{function} $f$ to be Lipschitz, we may assume that its
\emph{gradient} is.
\begin{definition}[Smooth function]
  A differentiable function $f: \RR^p \rightarrow \RR$ is $M$-smooth
  w.r.t. a norm $\norm{\cdot}$ if its gradient is $M$-Lipschitz, \ie
  for all $w, w' \in \RR^p$,
  \begin{align}
    \norm{ \nabla f(w) - \nabla f(w') }
     & \le
    M \norm{ w - w' }
    \enspace.
  \end{align}
  If $f$ is twice differentiable, this is equivalent to
  $\nabla^2 f \preccurlyeq M\Id_p$.
\end{definition}
The most useful consequence of this assumption is that it gives a
quadratic upper bound on the function $f$, that can be computed
globally from the knowledge of the gradient of $f$ at one point and
the smoothness constant.
\begin{proposition}[Quadratic Upper Bound]
  \label{prop:prelim:smooth-quad-upper-bound}
  Let $f: \RR^p \rightarrow \RR$ a $M$-smooth function. Then for all
  $w, w' \in \RR^p$,
  \begin{align}
    \label{eq:prelim:quad-upbd}
    f(w')
     & \le
    f(w)
    + \scalar{ \nabla f(w) }{ w' - w }
    + \tfrac{M}{2} \norm{ w - w' }^2
    \enspace.
  \end{align}
\end{proposition}
% \begin{proof}[Proof of \Cref{prop:prelim:smooth-quad-upper-bound}]
%   \TODO{Write the proof}
% \end{proof}
This property has a very important role in smooth first-order
optimization. Indeed, a natural idea for finding a minimum of $f$ is
to iteratively minimize this quadratic upper bound: take a fixed
$w \in \cW$, then the quadratic upper bound from
\eqref{eq:prelim:quad-upbd} is minimal when its gradient is zero, that is
\begin{align*}
  \nabla f(w) + M(w' - w) = 0
  \enspace,
\end{align*}
which implies that this quadratic upper bound is minimal when
$w' = w - \tfrac{1}{M} \nabla f(w)$. This is exactly the gradient step
that we will use in gradient descent for smooth functions (see
\Cref{sec:prox-grad-desc-1}).

Interestingly, the smoothness of $f$ can be captured more tightly by
measuring it on vectors that differ on only one coordinate.
\begin{definition}[Coordinate-smooth function]
  Let $M_1, \dots, M_p > 0$ and define
  $M = \diag(M_1, \dots, M_p) \in \RR^{p \times p}$.  A differentiable
  function $f: \RR^p \rightarrow \RR$ is $M$-coordinate-smooth, if for
  $j \in [p]$, the $j$-th coordinate of its gradient is
  $M_j$-Lipschitz, meaning that for all $w \in \RR^p$ and $t \in \RR$,
  \begin{align}
    \norm{ \nabla f(w) - \nabla f(w + t e_j) }
     & \le
    M_j \abs{t}
    \enspace.
  \end{align}
\end{definition}
Note that this assumption is in fact the same as smoothness, as
Lipschitzness of the gradient directly implies Lipschitzness of its
coordinates. But the coordinate wise constants $M_j$'s can be much
smaller than the global one. Coordinate-wise smoothness therefore
simply measure the smoothness more finely along each of the
coordinates of $f$. This allows to refine the quadratic upper
bound~\eqref{eq:prelim:quad-upbd} to the following, for $w \in \cW$
and $t \in \RR$,
\begin{align}
  \label{eq:prelim:coord-quad-upbd}
  f(w + te_j)
  & \le f(w) + \nabla_j f(w^t) \cdot t  + \tfrac{M_j}{2} \abs{ t }^2
    \enspace.
\end{align}
This upper bound will play a crucial role in the analysis of
coordinate descent methods, as we will see in
\Cref{sec:prox-stoch-grad}.

% \TODO{ probably make a subsection for convex + smooth, where we
%   introduce the two following inequalities and the condition number }

% \TODO{ the next two inequalities can be refined a little bit, but i am
%   not sure it is useful to present that }

% Interestingly if a convex function $f$ is smooth,
% inequality~\eqref{eq:prelim:convex:diff-def-upper-bound} can be
% refined into
% \begin{align}
%   \label{eq:prelim:convex-smooth:diff-def-upper-bound}
%   f(w)
%    & \le
%   f(w')
%   + \scalar{ \nabla f(w) }{ w' - w }
%   - \tfrac{\mu}{2} \norm{ w - w' }^2
%   - \tfrac{1}{2L} \norm{ \nabla f(w) - \nabla f(w') }^2
%   \enspace,
% \end{align}
% which itself leads to the inequality
% \begin{align}
%   \label{eq:prelim:convex-smooth:grad-dist-ineq}
%   \tfrac{\mu}{2} \norm{ w - w' }^2
%   + \tfrac{1}{2L} \norm{ \nabla f(w) - \nabla f(w') }^2
%    & \le
%   \scalar{ \nabla f(w) - \nabla f(w') }{ w - w' }
%   \enspace.
% \end{align}


\subsection{Proximal Operators}
\label{sec:proximal-operators}

When the problem~\eqref{prelim:general-optim-pb} has both a smooth
part $f$ and a non-smooth part $\psi$, the inequality from
\eqref{eq:prelim:quad-upbd} does not give an upper bound on $f+\psi$
anymore. To fix this, we may simply add $\psi$ to each side
of~\eqref{eq:prelim:quad-upbd}, which gives
\begin{align*}
  \label{eq:prelim:quad-upbd-composite}
  f(w') + \psi(w')
  & \le
    f(w)
    + \scalar{ \nabla f(w) }{ w' - w }
    + \tfrac{L}{2} \norm{ w - w' }^2
    + \psi(w')
    \enspace.
\end{align*}
Proceeding as above, we may want to minimize the right hand size of
this inequality in $w'$, which is minimal when
$w' = \argmin_{v \in \RR^p} \Big\{ \tfrac{1}{2} \norm{ v - (w -
  \tfrac{1}{L} \nabla f(w) ) }^2 + \psi(v) \Big\}$.  This motivates
the study of \emph{proximity operators}.
\begin{definition}[Proximity Operator]
  Let $\psi : \RR^p \rightarrow \RR$ be a closed proper convex
  function. For all $w \in \RR^p$, the proximal operator of $\psi$ is
  \begin{align}
    \prox_{\psi} (w)
     & =
    \argmin_{v \in \RR^p}
    \Big\{
    \tfrac{1}{2} \norm{ v - w }_2^2 + f(v)
    \Big\}
    \enspace.
  \end{align}
\end{definition}
Two usual examples of proximal operators are
\begin{itemize}
\item $\psi = \iota_{\cW}$ where $\cW$ is a convex set and $\iota_\cW$
  its characteristic function. Then $\prox_{\iota_\cW} = \Pi_\cW$ is
  the projection operator on the set $\cW$, as defined
  in~\eqref{prelim:def-proj-convex-set}.
\item $\psi = \lambda \norm{ \cdot }_1$ for some $\lambda > 0$. Then
  $\prox_{\lambda \norm{\cdot}_1}$ is the soft thresholding operator,
  which, for each coordinate $j$, gives
  $e_j^\top \prox_{\lambda \norm{\cdot}_1}(w) = \sign(w_j) \cdot \max(
  0, \abs{ w_j } - \lambda )$.
\end{itemize}

\begin{figure}[t]
  \captionsetup[subfigure]{justification=centering}
  \centering
  \begin{subfigure}{0.24\linewidth}
    \includegraphics[width=\linewidth]{chap2/prox-l1-0.pdf}
  \end{subfigure}~
  \begin{subfigure}{0.24\linewidth}
    \includegraphics[width=\linewidth]{chap2/prox-l1-1.pdf}
  \end{subfigure}~
  \begin{subfigure}{0.24\linewidth}
    \includegraphics[width=\linewidth]{chap2/prox-l1-2.pdf}
  \end{subfigure}~
  \begin{subfigure}{0.24\linewidth}
    \includegraphics[width=\linewidth]{chap2/prox-l1-3.pdf}
  \end{subfigure}
  \caption{ Objective function and solution (black point) of the
    optimization problem defined by the proximity operator of the
    $\ell_1$-norm. Purple point is the initial point. Each coordinate
    of the point is shrinked, and is put to zero if it is small
    enough: this is why the $\ell_1$-norm regularizer promotes
    sparsity. }
  \label{fig:prelim:prox-l1-norm}
\end{figure}



The projection operator is the same as illustrated in
\Cref{fig:prelim:proj-convex-set}, and we illustrate the loss function
solved by $\prox_{\lambda \norm{\cdot}_1}$ in
\Cref{fig:prelim:prox-l1-norm}. We refer to the
\href{http://proximity-operator.net/}{Prox Repository}\footnote{See
  \url{http://proximity-operator.net/}.} for more examples of
proximity operators.

We now state the mystical property of proximity operators: they do an
implicit gradient update, finding an element of the (sub)differential
of a function at a point we do not know yet. This property will be
crucial in every analysis of proximal algorithms, as it will allow
using convexity to obtain \emph{upper bounds}
using~\eqref{eq:prelim:convex:diff-def-upper-bound}.
\begin{proposition}[Implicit gradient step]
  \label{prop:prelim:implicit-grad-prox}
  Let $x \in \RR^p$, and $f: \RR^p \rightarrow \RR$ a convex
  function. There exists
  $\xi \in \partial \psi( \prox_{f}(x) )$ such that
  \begin{align}
    \prox_{f} (x)
     & =
    x - \xi
    \enspace.
  \end{align}
\end{proposition}
\begin{proof}[Proof of \Cref{prop:prelim:implicit-grad-prox}]
  Let $x \in \RR^p$ and
  $g: w \mapsto \tfrac{1}{2} \norm{ x - w }^2 + f(w)$. Note that $g$
  is strongly-convex, and has a unique minimizer $\prox_{f}(x)$, which
  satisfies
  $0 \in \prox_{\alpha f}(x) - x + \partial f(\prox_{f}(x))$.  This
  guarantees the existence of a unique
  $\xi \in \partial f(\prox_{f}(x))$ such that
  $\prox_{f}(x) = x - \xi$, which is the result.
\end{proof}

% \TODO{reformulate end of this section, in particular, firm
%   nonexpansiveness is stated in\citet{rosasco2020Convergence}, or
%   Lemma 2.4 of \citet{combettes2005Signal} or Definition 2.1 and next
%   in \citet{bauschke2021Generalized}}


\begin{proposition}[Lemma 2.4 in \citealp{combettes2005Signal}]
  \label{prop:prelim:firm-nonexp-prox}
  Let $x, y \in \RR^p$, and $f: \RR^p \rightarrow \RR$ a convex
  function. The proximal operator of $f$ is firmly non-expansive,
  meaning that
  \begin{align}
    \norm{ \prox_{f} (x) - \prox_{f} (y) }^2
     & \le
    \scalar{ \prox_f (x) - \prox_f (y) }{ x - y }
    \enspace.
  \end{align}
  Which implies the usual non-expansiveness property
  \begin{align}
    \norm{ \prox_{f} (x) - \prox_{f} (y) }^2
     & \le
    \norm{ x - y }^2
    \enspace.
  \end{align}
\end{proposition}

\begin{proof}[Proof of \Cref{prop:prelim:firm-nonexp-prox}]
  By \Cref{prop:prelim:implicit-grad-prox}, we have that, for all
  $x, y \in \RR^p$, $x - \prox_{f}(x) \in \partial f(\prox_{f}(x))$
  and $y - \prox_{f}(y) \in \partial f(\prox_{f}(y))$. Since $f$ is
  convex, it is monotone, and~\eqref{prelim:eq-monotone} gives
  \begin{align}
    \label{eq:prelim:prox-non-exp:convexity}
    \scalar{ (x - \prox_f(x)) - (y - \prox_f(y)) }{ \prox_f(x) - \prox_f(y) }
    \ge 0
    \enspace,
  \end{align}
  which gives the first inequality. The second one follows from the
  Cauchy-Schwarz inequality.
  % We can go further and obtain
  % \begin{align}
  %    & \norm{ \prox_f(x) - \prox_f(y) }^2
  %   \le
  %   \scalar{ x - y }{ \prox_f(x) - \prox_f(y) }
  %   \\
  %    & \quad =
  %   \norm{ x - y }^2
  %   + \scalar{ x - y }{ \prox_f (x) - x - \prox_f (y) + y }
  %   \\
  %    & \quad =
  %   \norm{ x - y }^2 - \norm{ \prox_f (x) - x - \prox_f (y) + y }^2
  %   \nnlqq
  %   + \scalar{ \prox_f (x) - \prox_f(y) }{ \prox_f (x) - x - \prox_f (y) + y }
  %   \enspace,
  % \end{align}
  % and the result follows from
  % \eqref{eq:prelim:prox-non-exp:convexity}.
\end{proof}

% \TODO{Write this paragraph}
% \begin{proposition}[Fixed-point]
%   \label{prop:prelim:fixed-point-prox}
%   Let $w^* \in \RR^p$ be the minimizer of $f$. Then
%   \begin{align}
%     \prox_{f} (w^*) = w^*
%     \enspace,
%   \end{align}
%   and $w^*$ is a fixed point of $\prox_f$.
% \end{proposition}

% \begin{proof}[Proof of \Cref{prop:prelim:fixed-point-prox}]
%   \TODO{Write the proof, see \citet{parikh2014Proximal}}
% \end{proof}

% \begin{corollary}[Fixed-point in composite settings]
%   \label{prop:prelim:fixed-point-composite-prox}
% \end{corollary}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../main"
%%% End:
