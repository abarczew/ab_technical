\chapter{Conclusion and Perspectives}
\label{chap:conclusion}

\section{Conclusion}
\label{sec:conclusion-1}

In this thesis, we investigated the role of problem structure in
differentially private machine learning. We proposed two new
differentially private optimization algorithms for empirical risk
minimization. These algorithms can exploit structural properties of
the problem to provably achieve a better privacy-utility trade-off
than existing algorithms .We also studied how differential privacy
impacts fairness in classification problems, and highlighted the role
of the confidence of the model across sub-groups of the population.


% Then, we studied how differential privacy impacts
% fairness in classification problems. We derived an upper bound on the
% difference of fairness between private and non-private models. This
% bound highlights the importance of the confidence of the model in its
% prediction and of the distribution of this confidence across the
% sub-groups of the population.

% We started with the study of
% two new differentially private optimization algorithms that can
% exploit some structural properties of a problem (like imbalanced
% gradients, or sparse solutions) to achieve better privacy-utility
% trade-off. We then investigated the impact of privacy on fairness by
% deriving a bound on the difference in fairness between private and
% non-private models. This bound highlights the importance of the
% distribution of the model's confidence among the population.


% We proposed two differentially private coordinate descent algorithms,
% and theoretically studied their utility. These methods can improve the
% privacy-utility trade-off by exploiting structural properties of the
% problem at hand (like imbalanced gradients or sparsity). In essence,
% our results highlight that the properties that make coordinate descent
% efficient in non-private problems (\eg large step sizes) also help to
% improve utility in differentially private optimization.


We proposed in \Cref{chap:dp-cd} a differentially private stochastic
coordinate descent algorithm. At each iteration of this algorithm, one
coordinate is sampled uniformly at random. This coordinate is then
updated with a noisy proximal gradient step. Noise addition allows to
guarantee differential privacy, but does not prevent from using large
coordinate-wise step sizes (like in non-private proximal coordinate
descent). Our differentially private coordinate descent algorithm can
therefore adapt to the imbalance in gradient's coordinates scales,
outperforming existing algorithms in terms of privacy-utility
trade-off when the problem at hand is imbalanced. We showed this
through a careful analysis of its convergence properties and derived
corresponding lower bounds.


% where the updated coordinates are chosen uniformly
% randomly. We analyzed the convergence properties of this algorithm,
% and stated its privacy-utility trade-off. Coordinate descent is
% notably able to exploit imbalance in gradients' coordinates by using
% larger step sizes than (stochastic) gradient descent algorithm. This
% makes the algorithm converge fast, allowing to improve utility in
% these kind of problems. We also proposed a refined lower bound on the
% privacy-utility trade-off under a coordinate-wise Lipschitzness
% assumption, that is matched by differentially private coordinate
% descent.

We then proposed in \Cref{chap:greedy-cd} a greedy variant of the
differentially private coordinate descent algorithm. This algorithm
can further improve utility by choosing the updated coordinates
greedily using the report noisy max mechanism. Thanks to these greedy
updates, the algorithm can naturally exploit structural properties
like the sparsity of the solution. Under favorable structural
assumptions, we proved that the dependence of our algorithm's utility
on the dimension is reduced from polynomial to logarithmic. We
demonstrated that this algorithm can find good (and sparse) parameters
in very few iterations, even when the dimension is large.


Finally we investigated in \Cref{chap:fair-privacy}, for
classification tasks, the impact of differential privacy on the level
of fairness of the learned model. To this end, we derived a bound on
the difference of fairness between a private model and its non-private
counterpart. This bound follows from the fact that many group fairness
notions are pointwise Lipschitz when the decision function is
Lipcshitz in its parameters. Our results highlight the key role of the
confidence margin in this problem, and in particular of its
distribution among the different sub-groups of the population.



\section{Perspectives}
\label{sec:perspectives}

\paragraph{Non-uniform Sampling of Coordinates.}

In \Cref{chap:dp-cd}, we studied differentially private coordinate
descent with \emph{uniform sampling} of the coordinates. In some
problems, it may be relevant to sample them non-uniformly (\eg
proportionally to the coordinate-wise smoothness constants, or
adaptively). This was studied by
\citet{nesterov2010Efficiency,richtarik2014Iteration,richtarik2016Optimal}
in the non-private setting. In particular, sampling coordinates with
large smoothness constants more often can help find an approximate
solution faster (although the algorithm tends to stall after a certain
number of iterations). This could be beneficial in differentially
private optimization. Due to privacy, we are necessarily finding an
approximate solution, yet performing fewer iterations helps reducing
the amount of injected noise, possibly improving the precision of this
approximation.



\paragraph{Clipping.}

In practice, the differentially private coordinate descent algorithms
we proposed heavily rely on the use of gradient clipping. However,
this is not covered by our theory, which makes it difficult to choose
the value of this threshold. Recently, \citet{koloskova2023Revisiting}
proposed a theoretical study of clipped (stochastic) gradient descent
under an $(L_0,L_1)$-smoothness assumption. Using a coordinate-wise
variant of this assumption could lead to improve theoretical
understanding of these clipped coordinate descent algorithms. Such
theoretical analysis could help in defining rules of thumb for setting
these clipping thresholds.

Adaptive strategies have also been proposed for setting the clipping
thresholds in an adaptive way
\citep{pichapati2019AdaCliP,andrew2021Differentially}. Developing
adaptive clipping strategies for differentially private coordinate
methods could also help to alleviate the difficulty of setting the
value of these thresholds appropriately.

\paragraph{Hyperparameter-Free Methods.}

Most of the existing differentially private optimization algorithms
heavily rely on one or more hyperparameters (\eg number of iterations,
step size, clipping thresholds, etc.). In particular, the differentially
private coordinate descent algorithms we developed in this thesis use
multiple parameters per coordinate. Although we have proposed methods
to adapt these from one global hyperparameter (some procedures
exist for tuning them privately) it would be more practical to avoid
setting them all together. Recently, hyperparameter-free
optimization algorithms have regained in popularity, for instance
through the works of
\citet{defazio2023LearningRateFree,mishchenko2023Prodigy,khaled2023DoWG}. Extending
these ideas to the differentially private setting could yield
important improvements in the performance and practical usability of
differentially private optimization algorithms.




\paragraph{Screening and Support Recovery.}

Practical solvers for sparse learning problems are often based on
coordinate descent, combined with screening methods
\citep{fercoq2015Mind,massias2017Safe,massias2018Celer,bertrand2022L1}. These
methods aim at identifying coordinates that have already converged,
and stop updating them to accelerate the convergence. In some sparse
problems, this can result in massive performance gains, which could
translate into better utility in differentially private settings.

More generally, (greedy) coordinate descent tend to identify the
support of the model fast in the non-private setting
\citep{klopfenstein2020Model,fang2020Greed}. The algorithms developed
in this thesis could thus be a promising starting point towards
defining differentially private algorithms that can identify the
support of a model.


\paragraph{Vertical Federated Learning.}

In federated learning, multiple agents aim at collaboratively training
a model without sharing their data. The vertical flavor of federated
learning covers the case where each agent holds a subset of the
features. These problems have not been studied very extensively
(contrary to other federated learning settings), but some approaches
are based on coordinate descent methods
\citep{liu2020Communication}. Differentially private vertical
federated learning could therefore be an interesting application of
the results we developed in this thesis.

% \paragraph{Hyperparameter-free private optimization.}

\paragraph{Efficient Greedy Updates.}

In \Cref{chap:greedy-cd}, we proposed a differentially private greedy
coordinate descent algorithm. Although this algorithm can reduce the
dependence on the dimension from polynomial to logarithmic, its
iterations have an important computational cost. In non-private
settings, multiple approaches have been proposed for reducing this
cost \citep{dhillon2011Nearest,karimireddy2019Efficient}. Their
approaches cast the greedy selection rule as a nearest neighbors
search, and use methods like locality-sensitive hashing to compute an
approximation of the greedy rule, reducing the computation cost. In
the differentially private greedy coordinate descent algorithm, the
greedy rule is always computed approximately (due to the privacy
requirement). Therefore, using these approximate greedy selection
rules could lead to reducing significantly the computational cost of
our algorithm, possibly without altering its convergence properties
too much. A promising perspective to solve this problem is to use the
differentially private locality-sensitive methods that were developed
by \citet{fernandes2021Locality}.


\paragraph{Proximal greedy coordinate descent.}

We proposed a proximal variant of differentially private greedy
coordinate descent in \Cref{sec:proximal-dp-gcd}, which achieves good
empirical performance. Unfortunately, theoretically analyzing the
convergence of this algorithm is very difficult. In the non-private
setting, \citet{karimireddy2019Efficient} proposed a modified variant
of proximal greedy coordinate descent for $\ell_1$-regularized and box-constrained problems, but their approach does not seem to be
applicable in the differentially private setting. Developing a proper
theory for this algorithm is a challenging open problem.



\paragraph{Logarithmic dependence on dimension: non-greedy algorithms.}

All differentially private algorithms whose utility can depend
logarithmically on the dimension are based on greedy algorithms (\ie differentially private Frank-Wolfe
\citet{talwar2015Nearly,bassily2021NonEuclidean,asi2021Private} and
our differentially private greedy coordinate descent algorithm). These
algorithms all leverage the report noisy max mechanism. They
require computing full gradients but only use one coordinate in the
final update. To this date, it is not clear whether it is possible to
achieve such utility without relying on greedy updates with the report
noisy max mechanism.

More generally, developing algorithms that can adapt to the structure
of the problem (like differentially private greedy coordinate descent
does) without relying on greedy updates is an important research
problem. Such algorithms would be interesting to achieve the best
possible utility on the problem at hand, without requiring too much
computation when greedy updates fail to exploit the structure of the
problem.

\paragraph{Achieving fairness and privacy.}

The theoretical study we proposed in \Cref{chap:fair-privacy} shows
that, in classification problems, differential privacy has a bounded
impact on fairness. It does not, however, guarantee that the learned
model is fair. This could be achieved by using
fairness-promoting regularization strategies like the one proposed by
\citet{lohaus2020Too}. In general, our results provide some insights
on the study of fairness, that could guide further developments of
differentially private mechanisms that foster fairness. One possible
direction would be to guarantee differential privacy with
non-uniform noise addition. If done properly, this could bias
the models learned this way toward fairer ones.


\paragraph{Large margin classifiers.}

Our results from \Cref{chap:fair-privacy} also highlight the key role
of the confidence margin on the fairness of the learned
models. Therefore, adapting training methods, so that trained models
have larger margins, could help in finding more fairness models. Note
that large-margin models also tend to achieve better privacy-utility
trade-offs, although few results exist on this question
\citep{bassily2022Differentially,bassily2022Open}. Nonetheless, this
suggests that this direction is promising for training models that
achieve good fairness, utility and privacy all at once.






%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
