% !TEX root = main.tex

\section{Experimental Details for \Cref{chap:greedy-cd}}
\label{sec:experimental-details}

In this section, we provide more information about the experiments, such as
details on implementation, datasets and the hyperparameter grid we
use for each algorithm. We then
give the full results on our L1-regularized, non-smooth, problems,
with the three greedy rules (as opposed to \Cref{sec:experiments-1} where
we only plotted results for the
\GSr rule). Finally, we provide runtime plots.

\paragraph{Code and setup.}
\label{sec:code-setup}

The algorithms are implemented in C++ for efficiency, together with a
Python wrapper for simple use. It is provided as supplementary.
Experiments are run on a computer with a Intel
(R) Xeon(R) Silver 4114
CPU @ 2.20GHz and 64GB of RAM, and took about 10 hours in total to run
(this includes all
hyperparameter tuning).

\paragraph{Datasets.}
\label{sec:datasets}

The datasets we use are described in \Cref{tab:datasets-desc}. In
\Cref{fig:solution-nice}, we plot the histograms of the absolute value
of each problem solution's parameters. The purple line indicates the
value of $\alpha$ that ensures that the parameters of the solution are
$(\alpha,5)$-quasi-sparse. Note the logarithmic scale on the $y$-axis.
On the \logg, \loggg, \madelon, \sparse, \california and \dorothea
datasets, the solutions are very imbalanced. In these problems, a very
limited number of parameters stand out, and DP-GCD is able to exploit
this property. This illustrates the results from
\Cref{sec:fast-init-conv}, since DP-GCD can exploit this structure
even in quasi-sparse problems, where $\alpha$ is non zero. Conversely,
the \mtp solution is more balanced: the structural properties of this
dataset are not strong enough for DP-GCD to outperform its competitors.


% On the \logg, \loggg, \mtp and
% \madelon (l2) problems, the histograms show that many of the
% solution's parameters are small. On \logg and \mtp, this does not
% ensure a high enough level of quasi-sparsity for DP-GCD to really
% outperform random selection. On \loggg and \madelon (l2), solutions
% tend to be sparser (\ie $(\alpha,\tau)$-quasi-sparse for some $\tau$
% and smaller $\alpha$), which DP-GCD manages to exploit. On \sparse,
% \california, \dorothea and \madelon (l1), the problems' solutions are
% actually sparse: our proximal DP-GCD algorithm uses this property to
% improve utility.


\begin{figure*}[t]
  \captionsetup[subfigure]{justification=centering}
  \centering
  % \begin{subfigure}{0.046\linewidth}
  %   \centering
  %   \includegraphics[width=\linewidth]{plots/xlegend.pdf}
  %   \begin{minipage}{.1cm}
  %     \vfill
  %   \end{minipage}
  % \end{subfigure}%
  \begin{subfigure}{0.235\linewidth}
    \centering
    \includegraphics[width=\linewidth]{imbalance_plots/lognormal_opt_1_Logistic_L2Regularizer.pdf}
    \caption{\texttt{log1} \\ Logistic + L2 \\  ($\lambda=1\text{e-}3$)}
    \label{fig:solution-skewed-1}
  \end{subfigure}%
  \begin{subfigure}{0.235\linewidth}
    \centering
    \includegraphics[width=\linewidth]{imbalance_plots/lognormal_opt_2_Logistic_L2Regularizer.pdf}
    \caption{\texttt{log2} \\ Logistic + L2 \\  ($\lambda=1\text{e-}3$)}
    \label{fig:solution-skewed-2}
  \end{subfigure}%
  \begin{subfigure}{0.235\linewidth}
    \centering
    \includegraphics[width=\linewidth]{imbalance_plots/mtp_LeastSquares_L2Regularizer.pdf}
    \caption{\texttt{mtp}\\ Least Squares + L2 \\  ($\lambda=5\text{e-}8$)}
    \label{fig:solution-mtp}
  \end{subfigure}%
  \begin{subfigure}{0.235\linewidth}
    \centering
    \includegraphics[width=\linewidth]{imbalance_plots/madelon_Logistic_L2Regularizer.pdf}
    \caption{\texttt{madelon} \\ Logistic + L2 \\  ($\lambda=1$)}
    \label{fig:solution-madelon-l2}
  \end{subfigure}%
  % \begin{subfigure}{0.22\linewidth}
  %   \centering
  %   \includegraphics[width=\linewidth]{plot_solutions/solution_lasso.pdf}
  %   \caption{Sparse \\ LASSO, $\lambda=30$.}
  %   \label{fig:solution-lasso-sparse-raw}
  % \end{subfigure}%

  %   \caption{Relative error to non-private optimal for DP-CD, DP-GCD
  %     and DP-SGD. Number of iterations, clipping thresholds and step
  %     sizes are tuned separately. We report min/mean/max values over
  %     10~runs.}
  %   \label{fig:solution-nice}
  % \end{figure*}
  % \begin{figure*}[t]
  \captionsetup[subfigure]{justification=centering}
  \centering
  % \begin{subfigure}{0.046\linewidth}
  %   \centering
  %   \includegraphics[width=\linewidth]{plots/xlegend.pdf}
  %   \begin{minipage}{.1cm}
  %     \vfill
  %   \end{minipage}
  % \end{subfigure}%
  \begin{subfigure}{0.235\linewidth}
    \centering
    \includegraphics[width=\linewidth]{imbalance_plots/lasso_LeastSquares_L1Regularizer.pdf}
    \caption{\texttt{square} \\ LASSO \\ ($\lambda=30$)}
    \label{fig:solution-sparse-lasso}
  \end{subfigure}%
  \begin{subfigure}{0.235\linewidth}
    \centering
    \includegraphics[width=\linewidth]{imbalance_plots/california_LeastSquares_L1Regularizer.pdf}
    \caption{\texttt{california}\\ LASSO \\  ($\lambda=0.1$)}
    \label{fig:solution-california}
  \end{subfigure}%
  \begin{subfigure}{0.235\linewidth}
    \centering
    \includegraphics[width=\linewidth]{imbalance_plots/dorothea_SparseLogistic_L1Regularizer.pdf}
    \caption{\texttt{dorothea}\\ Logistic + L1 \\  ($\lambda=0.01$)}
    \label{fig:solution-dexter}
  \end{subfigure}%
  \begin{subfigure}{0.235\linewidth}
    \centering
    \includegraphics[width=\linewidth]{imbalance_plots/madelon_Logistic_L1Regularizer.pdf}
    \caption{\texttt{madelon}\\ Logistic + L1 \\  ($\lambda=0.05$)}
    \label{fig:solution-madelon-l1}
  \end{subfigure}%
  % \begin{subfigure}{0.22\linewidth}
  %   \centering
  %   \includegraphics[width=\linewidth]{plots/mtp_l1_norm.pdf}
  %   \caption{\texttt{mtp}\\ LASSO \\  ($\lambda=0.05$)}
  %   \label{fig:solution-skewed-2}
  % \end{subfigure}%
  % % \begin{subfigure}{0.22\linewidth}
  %   \centering
  %   \includegraphics[width=\linewidth]{plots/lasso.pdf}
  %   \caption{Sparse \\ LASSO, $\lambda=30$.}
  %   \label{fig:solution-lasso-sparse-raw}
  % \end{subfigure}%

  \caption{Histograms of the absolute value of each problem solution's
    parameters. Purple line indicates the $\alpha$ for which the
    plotted vector is $(\alpha,5)$-quasi-sparse. Y-axis is
    logarithmic.}
  \label{fig:solution-nice}
\end{figure*}

\paragraph{Hyperparameters.}
\label{sec:hyperparameters}

On all datasets, we use the same hyperparameter grid. For each
algorithm, we choose between roughly the same number of
hyperparameters. The number of passes on data represents $p$
iterations of DP-CD, $n$ iterations of DP-SGD, and $1$ iteration of
DP-GCD. The complete grid is described in
\Cref{tab:hyperparameter-grid}, and the chosen hyperparameters for
each problem and algorithm are given in
\Cref{table:chosen-hyperparameters}.

\begingroup

\renewcommand*{\arraystretch}{1.2}
\begin{table}
  \centering
  \caption{Hyperparameter grid used in our experiments.}
  \label{tab:hyperparameter-grid}
  \begin{tabular}{ccc}
    \toprule
    Algorithm & Parameter & Values \\
    \midrule
              & Passes on data & \texttt{[0.001, 0.01, 0.1, 1, 2, 3, 5, 10, 20]} \\
    DP-CD     & Step sizes & \texttt{np.logspace(-2, 1, 10)} \\
              & Clipping threshold & \texttt{np.logspace(-4, 6, 50)}  \\
    \midrule
              & Passes on data & \texttt{[0.001, 0.01, 0.1, 1, 2, 3, 5, 10, 20]} \\
    DP-SGD    & Step sizes & \texttt{np.logspace(-6, 0, 10)} \\
              & Clipping threshold & \texttt{np.logspace(-4, 6, 50)}  \\
    \midrule
              & Passes on data & \texttt{[1, 2, 4, 7, 10, 15, 20]} \\
    DP-GCD    & Step sizes & \texttt{np.logspace(-2, 1, 10)} \\
              & Clipping threshold & \texttt{np.logspace(-4, 6, 50)}  \\
    \bottomrule
  \end{tabular}
\end{table}
\endgroup

\paragraph{Recovery of the support.}

In \Cref{tab:finding-of-support}, we report the number of coordinates
that are correctly/incorrectly identified as non-zero on $\ell_1$
regularized problems. Contrary to DP-SGD and DP-CD, DP-GCD never
incorrectly identifies a coordinate as non-zero. Additionally, the
suboptimality gap is lower for DP-GCD: its updates thus lead to better
solutions.

\begin{table}
  \centering
  \caption{ Coordinates correctly/incorrectly identified as non-zeros
    by each algorithm, and relative suboptimality gap
    $(f(w^{priv})-f^*)/f^*$ (averaged over 5~runs).}
  \label{tab:finding-of-support}
  \begin{tabularx}{\textwidth}{XXXXX}%{ccccc}
    \toprule
    & \texttt{square} & \texttt{california} & \texttt{dorothea} & \texttt{madelon}  \\
    \midrule
    $\|w^*\|_0$ & 7 & 3 & 72 & 3 \\
    DP-CD & 0 / 0 (0.75) & 3 / 2 (0.0024) & 1 / 1 (0.77) & 0 / 0 (0.0085) \\
    DP-SGD & 0 / 3 (0.75) & 3 / 5 (0.020) & 0 / 0 (0.78) & 0 / 0 (0.012) \\
    DP-GCD & 2 / 0 (0.35) & 2 / 0 (0.00056) & 1 / 0 (0.64) & 1 / 0 (0.0015) \\
    \bottomrule
  \end{tabularx}
\end{table}

\paragraph{Additional experiments on proximal DP-GCD.}
\label{sec:addit-exper-prox}


In \Cref{fig:expe-prox}, we show the results of the proximal DP-GCD
algorithm, after tuning the hyperparameters with the grid described above for
each of the \GSs, \GSr and
\GSq rules.

The three rules seem to behave qualitatively the same on \sparse,
\dorothea and \madelon, our three high-dimensional non-smooth
problems. There, most coordinates are chosen about one time. Thus, as
described by \citet{karimireddy2019Efficient}, all the steps are
``\good'' steps (along their terminology): and on such good steps, the
three rules coincide. On the lower-dimensional dataset \california,
coordinates can be chosen more than one time, and ``\bad'' steps are
likely to happen. On these steps, the three rules differ.

\paragraph{Runtime.}
Finally, we report the runtime of DP-GCD, in comparison with DP-CD and
DP-SGD in \Cref{fig:expe-time}, that is the counterpart of
\Cref{fig:expe-nice}, except with runtime on the $x$-axis. These
results confirm the fact that DP-GCD can be efficient, although its
iterations are expensive to compute. Indeed, in imbalanced problems,
the small number of iterations of DP-GCD enables it to run faster than
DP-SGD, and in roughly the same time as DP-CD, while improving
utility.

\begin{table}
  \footnotesize
  \centering
  \caption{
    Selected hyperparameters for every dataset and algorithm.
  }
  \label{table:chosen-hyperparameters}
  \csvautobooktabular{src/greedy-cd/csvs/best_params_treated.csv}
\end{table}

% \begin{figure*}[t]
%   \captionsetup[subfigure]{justification=centering}
%   \centering
%   \begin{subfigure}{0.046\linewidth}
%     \centering
%     \includegraphics[width=\linewidth]{plots/xlegend.pdf}
%     \begin{minipage}{.1cm}
%       \vfill
%     \end{minipage}
%   \end{subfigure}%
%   \begin{subfigure}{0.235\linewidth}
%     \centering
%     \includegraphics[width=\linewidth]{plots/sparse_lasso_prox.pdf}
%     \caption{\texttt{sparse} \\ LASSO \\ ($\lambda=30$)}
%     \label{fig:expe-prox-sparse-lasso}
%   \end{subfigure}%
%   \begin{subfigure}{0.235\linewidth}
%     \centering
%     \includegraphics[width=\linewidth]{plots/california_l1_norm_prox.pdf}
%     \caption{\texttt{california}\\ LASSO \\  ($\lambda=0.1$)}
%     \label{fig:expe-prox-california}
%   \end{subfigure}%
%   \begin{subfigure}{0.235\linewidth}
%     \centering
%     \includegraphics[width=\linewidth]{plots/dexter_norm_prox.pdf}
%     \caption{\texttt{dexter}\\ Logistic + L1 \\  ($\lambda=0.1$)}
%     \label{fig:expe-prox-dexter}
%   \end{subfigure}%
%   \begin{subfigure}{0.235\linewidth}
%     \centering
%     \includegraphics[width=\linewidth]{plots/madelon_l1_norm_prox.pdf}
%     \caption{\texttt{madelon}\\ Logistic + L1 \\  ($\lambda=0.05$)}
%     \label{fig:expe-prox-madelon-l1}
%   \end{subfigure}%
%   % \begin{subfigure}{0.22\linewidth}
%   %   \centering
%   %   \includegraphics[width=\linewidth]{plots/mtp_l1_norm.pdf}
%   %   \caption{\texttt{mtp}\\ LASSO \\  ($\lambda=0.05$)}
%   %   \label{fig:expe-skewed-2}
%   % \end{subfigure}%
%   % % \begin{subfigure}{0.22\linewidth}
%   %   \centering
%   %   \includegraphics[width=\linewidth]{plots/lasso.pdf}
%   %   \caption{Sparse \\ LASSO, $\lambda=30$.}
%   %   \label{fig:expe-lasso-sparse-raw}
%   % \end{subfigure}%

%   \caption{Relative error to non-private optimal for DP-CD, proximal
%     DP-GCD (with \GSr, \GSs and \GSq rules) and DP-SGD on different
%     problems. On the x-axis, $1$ tick represents a full access to the
%     data: $p$ iterations of DP-CD, $n$ iterations of DP-SGD and $1$
%     iteration of DP-GCD. Number of iterations, clipping thresholds and
%     step sizes are tuned simultaneously for each algorithm. We report
%     min/mean/max values over 10~runs.}
%   \label{fig:expe-prox}
% \end{figure*}

\begin{figure*}[t]
  \captionsetup[subfigure]{justification=centering}
  \centering
  \begin{subfigure}{0.046\linewidth}
    \centering
    \vspace{-1.5em}
    \includegraphics[width=\linewidth]{plots/xlegend.pdf}
    \begin{minipage}{.1cm}
      \vfill
    \end{minipage}
  \end{subfigure}%
  \begin{subfigure}{0.235\linewidth}
    \centering
    \includegraphics[width=\linewidth]{plots/lasso_LeastSquares_L1Regularizer_gs.pdf}
    \caption{\texttt{sparse} \\ LASSO \\ ($\lambda=30$)}
    \label{fig:expe-sparse-lasso-gs}
  \end{subfigure}%
  \begin{subfigure}{0.235\linewidth}
    \centering
    \includegraphics[width=\linewidth]{plots/california_LeastSquares_L1Regularizer_gs.pdf}
    \caption{\texttt{california}\\ LASSO \\  ($\lambda=0.1$)}
    \label{fig:expe-california-gs}
  \end{subfigure}%
  \begin{subfigure}{0.235\linewidth}
    \centering
    \includegraphics[width=\linewidth]{plots/dorothea_SparseLogistic_L1Regularizer_gs.pdf}
    \caption{\texttt{dorothea}\\ Logistic + L1 \\  ($\lambda=0.01$)}
    \label{fig:expe-dorothea-gs}
  \end{subfigure}%
  \begin{subfigure}{0.235\linewidth}
    \centering
    \includegraphics[width=\linewidth]{plots/madelon_Logistic_L1Regularizer_gs.pdf}
    \caption{\texttt{madelon}\\ Logistic + L1 \\  ($\lambda=0.05$)}
    \label{fig:expe-madelon-l1-gs}
  \end{subfigure}%

  \caption{Relative error to non-private optimal for DP-CD, proximal
    DP-GCD (with \GSr, \GSs and \GSq rules) and DP-SGD on different
    problems. On the x-axis, $1$ tick represents a full access to the
    data: $p$ iterations of DP-CD, $n$ iterations of DP-SGD and $1$
    iteration of DP-GCD. Number of iterations, clipping thresholds and
    step sizes are tuned simultaneously for each algorithm. We report
    min/mean/max values over 5~runs.}
  \label{fig:expe-prox}
\end{figure*}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
