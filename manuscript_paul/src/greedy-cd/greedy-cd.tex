% !TEX root = main.tex

\section{Private Greedy Coordinate Descent}
\label{sec:priv-greedy-coord}

In this section, we present the contribution of this Chapter: the
differentially private greedy coordinate descent algorithm
(\DPGCD). As described in Section~\ref{sec:algorithm}, \DPGCD updates
only one coordinate per iteration, which is selected greedily as the
(approximately) largest entry of the gradient so as to maximize the
improvement in utility at each iteration. We establish privacy
(Section~\ref{sec:privacy}) and utility (Section~\ref{sec:utility})
guarantees for \DPGCD. We further show in \Cref{sec:fast-init-conv}
that \DPGCD enjoys improved utility for high-dimensional problems with
a \emph{quasi-sparse} solution (\ie with a fraction of the parameters
dominating the others). We then provide a proximal extension of \DPGCD
to non-smooth problems (\Cref{sec:proximal-dp-gcd}) and conclude with
a discussion of \DPGCD's computational complexity in
\Cref{sec:computational-cost}.

% show that it is
% well-suited for high-dimensional problems with a \emph{quasi-sparse}
% solution (\ie with a fraction of the parameters dominating
% the others).

% In \Cref{sec:privacy} we describe how to set $\lambda_j,\lambda_j'$
% (for $j\in[p]$) to ensure $(\epsilon,\delta)$-differential privacy. We
% then give high-probability utility results in \Cref{sec:utility}. We
% further show in \Cref{sec:fast-init-conv} that \DPGCD can progress
% very fast in its first iterations when the solutions are
% quasi-sparse.

\subsection{The Algorithm}
\label{sec:algorithm}

At each iteration, \DPGCD
(\Cref{algo:private-greedy-coordinate-descent}) updates the parameter
with the greatest gradient value (rescaled by the inverse square root
of the coordinate-wise smoothness constant). This corresponds to the
Gauss-Southwell-Lipschitz rule \citep{nutini2015Coordinate}. We
describe this algorithm in
\Cref{algo:private-greedy-coordinate-descent}.

% \begin{algorithm*}[t]
%   \caption{DP-GCD: Differentially Private Greedy Coordinate Descent}

%   \begin{algorithmic}[1]
%     \State \textbf{Input:} initial $w^0 \in \RR^p$, iteration count $T > 0, \forall j \in [p],$ noise scales $\lambda_j, \lambda_j'$, step sizes $\gamma_j > 0$.
%     \For{$t = 0$ to $T-1$}
%     \State $\displaystyle j_t = \argmax_{j' \in [p]} \tfrac{\abs{\nabla_{j'} f(w^t) + \chi_{j'}^t}}{\sqrt{M_{j'}}}$, \hfill with $ \chi_{j'}^t \sim \Lap(\lambda'_{j'})$. \label{algo:private-greedy-coordinate-descent:choice_coord}\algorithmiccomment{Choose $j_t$ using report-noisy-max.}
%     \State $\displaystyle w^{t+1} = w^t - \gamma_{j_t} (\nabla_{j_t} f(w^t) + \eta_{j_t}^t) e_{j_t}$, \hfill  with $\eta_{j_t}^t \sim \Lap(\lambda_{j_t})$. \algorithmiccomment{Update the chosen coordinate.}
%     \label{algo:private-greedy-coordinate-descent:update}
%     \EndFor
%     \State \Return $w^T$.
%   \end{algorithmic}
% \end{algorithm*}

\begin{myalgorithm}
  {\DPGCD: Differentially Private Greedy Coordinate Descent}
  {initial point $w^0$, noise scales $\lambda_1, \dots, \lambda_p, > 0$, $\lambda_1', \dots, \lambda_p', > 0$, step sizes $\gamma_1, \dots, \gamma_p > 0$, number of iteration $T > 0$}
  {$w^T$}
  \setlist[itemize]{align=parleft,left=0pt..1.5em}
  \label{algo:private-greedy-coordinate-descent}
  For $t = 0$ to $T-1$:
  \vspace{-0.5em}
  \begin{itemize}
  \item Select
    $\displaystyle j = \argmax_{j' \in [p]} \tfrac{\abs{\nabla_{j'}
        f(w^t) + \chi_{j'}^t}}{\sqrt{M_{j'}}}$, with
    $ \chi_{j'}^t \sim \Lap(\lambda'_{j'})$
  \item Set $w^{t+1} = w^t$
  \item Update
    $\displaystyle w_j^{t+1} = w_j^t - \gamma_{j} (\nabla_{j} f(w^t) + \eta_{j}^t)$,  with $\eta_{j}^t \sim \Lap(\lambda_{j})$
  \end{itemize}
  \vspace{-0.5em}
\end{myalgorithm}


To guarantee privacy, this selection is done using the
report-noisy-max mechanism \citep{dwork2014Algorithmic} with noise
scales $\lambda_j'$ along $j$-th entry ($j\in[p]$). \DPGCD then
performs a gradient step with step size $\gamma_j > 0$ along this
direction. The gradient is privatized using the Laplace mechanism
\citep{dwork2014Algorithmic} with scale $\lambda_j$.
% \marc{Lier un
%   peu plus ce paragraphe avec l'algo en précisant $\lambda_j$ et
%   $\lambda'_j$ après Laplace mechanism et report-noisy-max et préciser
%   $\gamma_j$ le per componenent step size}

\begin{remark}[Sparsity of iterates]
  \label{rem:sparse_iterates}
  When initialized at $w^0=0$, \DPGCD generates sparse iterates. Since it
  chooses its updates greedily, this gives a screening ability to the
  algorithm \citep{fang2020Greed}. We discuss the implications of this
  property in Section~\ref{sec:fast-init-conv}, where we show that \DPGCD's
  utility is improved when the problem's solution is (quasi-)sparse.
\end{remark}

\subsection{Privacy Guarantees}
\label{sec:privacy}

The privacy guarantees of \DPGCD depends on the noise scales $\lambda_j$ and
$\lambda_j'$. In
\Cref{thm:greedy-cd-privacy}, we describe how to set these values so
as to ensure that \DPGCD is
$(\epsilon,\delta)$-differentially private.

\begin{theorem}
  \label{thm:greedy-cd-privacy}
  Let $\epsilon, \delta \in (0,
  1]$. \Cref{algo:private-greedy-coordinate-descent} with
  $\lambda_j = \lambda_j' = \frac{8L_j}{n\epsilon} \sqrt{ T
    \log(1/\delta)} $ is $(\epsilon,\delta)$-differentially private.
\end{theorem}

\begin{proofsketch}
  (Detailed proof in \Cref{sec-app:proof-privacy})  Let
  $\epsilon' = \epsilon/\sqrt{16T\log(1/\delta)}$. At an iteration
  $t$, data is accessed twice. First, to compute the index $j$ of the
  coordinate to update. It is obtained as the index of the largest
  noisy entry of $f$'s gradient, with noise $\Lap(\lambda_j')$. By the
  report-noisy-argmax mechanism, $j$ is $\epsilon'$-DP. Second, to
  compute the gradient's $j$'s entry, which is released with noise
  $\Lap(\lambda_j)$.% \marc{C'est pas le contraire entre les
  %    $\lambda_j$ et $\lambda'_j$?}
  The Laplace mechanism ensures that this computation is also
  $\epsilon'$-DP. \Cref{algo:private-greedy-coordinate-descent} is
  thus the $2T$-fold composition of $\epsilon'$-DP mechanisms, and the
  result follows from DP's advanced composition theorem
  \citep{dwork2014Algorithmic}.
\end{proofsketch}

% \aurelien{does this remark hold? Mironov gives RDP analysis of Laplace but I
% am not sure if it gives better bounds. I think here you probably just mean
% with the full advanced composition formula. If so, you could clarify. Also
% note that for $\epsilon$ not very small and $T$ not so large you may be
% better off with simple composition...}
\begin{remark}
  The assumption $\epsilon\in(0,1]$ is only used to give a closed-form
  expression for the noise scales $\lambda, \lambda$'s. In practice,
  we tune them numerically to obtain the desired value of $\epsilon>0$
  by the advanced composition theorem (see
  \cref{thm:greedy-cd-privacy:general-epsilon-value} in
  \Cref{sec-app:proof-privacy}), removing the assumption
  $\epsilon \le 1$.
\end{remark}

Computing the greedy update requires injecting Laplace noise that
scales with the coordinate-wise Lipschitz constants $L_1,\dots,L_p$ of
the loss. These constants are typically smaller than their global
counterpart. This allows \DPGCD to inject less noise on smaller-scaled
coordinates.

%This will be crucial for \DPGCD to achieve its nearly
%dimension-independent utility.

\subsection{Utility Guarantees}
\label{sec:utility}

% \aurelien{$M_{\min}$ and $M_{\max}$ do not seem to be explicitly defined.
% perhaps do this in Sec 2?}

We now prove utility upper bounds for \DPGCD.
We show that in
favorable settings (see discussion below), \DPGCD decreases the
dependence on the dimension from polynomial to logarithmic.
% In such
% problems, \DPGCD can thus improve on the general worst-case
% utility.
\Cref{thm:greedy-cd-utility} gives asymptotic utility
upper bounds, where~$\widetilde O$ ignores non-significant logarithmic
terms. Complete non-asymptotic results can be found in
\Cref{sec-app:proof-utility}.

% We now state utility results for \DPGCD on smooth \DPERM. For some
% problems, \DPGCD can provide better utility than the worst-case lower
% bounds. For clarity, we state asymptotic results in
% \Cref{thm:greedy-cd-utility}, where~$\widetilde O$ ignores
% non-significant logarithmic terms.
% In the rest of this paper,
% we denote $f(w) = \frac{1}{n}\sum_{i=1}^n \ell(w;d_i)$.\marc{déjà dit en (1)}
\begin{theorem}
  \label{thm:greedy-cd-utility}
  Let $\epsilon, \delta \in (0,1]$. Assume $\ell(\cdot; d)$ is a
  convex and $L$-coordinate-Lipschitz loss function for all
  $d \in \cX$, and $f$ is $M$-coordinate-smooth. Define $\cW^*$ the set
  of minimizers of $f$, and $f^*$ the minimum of $f$. Let
  $w_{priv}\in\mathbb{R}^p$ be the output of
  \Cref{algo:private-greedy-coordinate-descent} with step sizes
  $\gamma_j = {1}/{M_j}$, and noise scales
  $\lambda_1,\dots,\lambda_p$, $\lambda'_1,\dots,\lambda'_p$ set as in
  Theorem~\ref{thm:greedy-cd-privacy} (with $T$ chosen below) to
  ensure $(\epsilon,\delta)$-DP. Then, the following holds for any
  $\zeta\in(0,1]$:
  \begin{enumerate}[leftmargin=12pt]
  \item When $f$ is convex, let
    $R_{M,1}=\max_{\scriptstyle w\in\RR^p~}\!\!\max_{\scriptstyle
      w^*\in \cW^*}\! \left\{ \norm{w-w^*}_{M,1} \!\mid\! f(w) \le
      f(w^0) \right\}$.  Assume the initial optimality gap is
    $f(w^0) - f^* \ge \tfrac{16 L_{\max} \sqrt{T\log(1/\delta)
        \log(2Tp/\zeta)}}{M_{\min} n \epsilon}$, and set
    $T = O(n^{2/3} \epsilon^{2/3} R_{M,1}^{2/3} M_{\min}^{1/3} /
    L_{\max}^{2/3} \log(1/\delta)^{1/3})$. Then with probability at
    least $1-\zeta$,
          \begin{align*}
            % \label{eq:utility_convex}
            f(w_{priv}) - f^*
            = \widetilde O\bigg(\frac{R_{M,1}^{4/3} L_{\max}^{2/3} \log(1/\delta) \log(p/\zeta)}{n^{2/3}\epsilon^{2/3}M_{\min}^{1/3}}\bigg)\enspace.
          \end{align*}
        \item When $f$ is $\mu_{M,1}$-strongly convex
          w.r.t. $\smash{\norm{\cdot}_{M,1}}$, set the number of
          iterations to
          $T = O\left(\frac{1}{\mu_{M,1}} \log(\frac{M_{\min}
              \mu_{M,1} n \epsilon (f(w^0)-f(w^*))}{L_{\max}
              \log(1/\delta) \log(2p/\zeta)})\right)$. Then with
          probability at least $1-\zeta$,
          \begin{align*}
            % \label{eq:utility_strongconvex}
            f(w_{priv}) - f^*
            = \widetilde O\bigg(
            \frac{L_{\max}^2 \log(1/\delta) \log({2p}/{\mu_M\zeta})}{M_{\min} \mu_{M,1}^2 n^2 \epsilon^2}
            \bigg)\enspace.
          \end{align*}
  \end{enumerate}%
\end{theorem}
% \paul{sketch proof}
% \aurelien{right now it is very short and uninformative. either complete it,
% or remove it for now.}
\begin{proofsketch}(Detailed proof in \Cref{sec-app:proof-utility}). %\marc{cas convex}
  % \paul{this proof does not correspond to appendices anymore}
  We start by proving a noisy ``descent lemma''.  Since $f$ is smooth,
  we have
  $f(w^{t+1}) \le f(w^t) - \frac{1}{2M_j} \nabla_j f(w^t)^2 +
  \frac{1}{2M_j} (\eta_j^t)^2$. The greedy selection of $j$ gives that
  $- \tfrac{1}{M_j}(\nabla_j f(w^t) + \chi_j)^2 \le - \norm{\nabla
    f(w^t) + \chi}_{M^{-1},\infty}^2$. We then use the inequality
  $(a+b)^2 \le 2a^2 + 2b^2$ for $a,b\in\RR$, and convexity arguments
  to prove the lemma. When $f$ is convex, we have%
  \begin{align*}
     & f(w^{t+1}) - f(w^*)
    \le f(w^t) - f(w^*)                                             \\
     & \quad - \frac{(f(w^t) - f(w^*))^2}{8 \norm{w^t-w^*}_{M,1}^2}
    + \frac{\abs{\eta_j^t}^2}{2M_j}
    + \frac{ \abs{\chi^t_{j}}^2 }{2M_{j}}
    + \frac{ \abs{\chi^t_{j^*}}^2 }{4M_{j^*}} \enspace.
  \end{align*}
  There, we observe that, at each iteration, either (i) $w^t$ is far
  enough from the optimum, and the value of the objective decreases
  with high probability, either (ii) $w^t$ is close to the optimum,
  then all future iterates remain in a ball whose radius depends on
  the scale of the noise. We prove this key property rigorously in
  \Cref{sec:key-lemma-behavior}.

  When $f$ is $\mu_{M,1}$-strongly-convex \wrt $\norm{\cdot}_{M,1}$,
  we obtain
  \begin{align*}
    f(w^{t+1}) - f(w^*)
     & \le \Big( 1 - \frac{\mu_{M,1}}{4} \Big)
    (f(w^t) - f(w^*))                          \\
     & \qquad + \frac{\abs{\eta_j^t}^2}{2M_j}
    + \frac{ \abs{\chi^t_{j}}^2 }{2M_{j}}
    + \frac{ \abs{\chi^t_{j^*}}^2 }{4M_{j^*}} \enspace,
  \end{align*}
  and the result follows by induction. In both settings, we use
  Chernoff bounds to obtain a high-probability result.
\end{proofsketch}

\begin{remark}
  The lower bound on
  $f(w^0) - f^*$ in Theorem~\ref{thm:greedy-cd-utility}
  %  \ge 16 L_{\max} \sqrt{T\log(1/\delta)
  % \log(2Tp/\zeta)}/M_{\min} n \epsilon$
  is a standard assumption in the analysis
  of inexact coordinate descent methods: it ensures that sufficient
  decrease is possible despite the noise. A similar assumption is made
  by \citet{tappenden2016Inexact}, see Theorem 5.1 therein.
\end{remark}

\paragraph{Discussion of the utility bounds}
One of the key properties of \DPGCD is that its utility is dictated by
$\ell_1$-norm quantities ($R_{M,1}$ and $\mu_{M,1}$). Remarkably, this
arises without enforcing any $\ell_1$ constraint in the problem, which is in
stark contrast with
private Frank-Wolfe algorithms (\DPFW) that
require such constraints
\citep{talwar2015Nearly,asi2021Private,bassily2021NonEuclidean}. To better
grasp the implications of this, we discuss our results in two
regimes considered in previous work (see Section~\ref{sec:related-works}): (i) when these
$\ell_1$-norm quantities are bounded (similarly to \DPFW algorithms),
and (ii) when their
$\ell_2$-norm counterparts are bounded (similarly to \DPSGD-style
algorithms).
% \paul{mention dim-independent in (i) and "interpolation"
%   in (ii)} \paul{attention: interpolation means something very
%   specific in stochastic optim}

\textit{Bounded in $\ell_1$-norm.} When $R_{M,1}$ and $\mu_{M,1}$ are
$O(1)$, as assumed in prior work on \DPFW
\citep{talwar2015Nearly,asi2021Private,bassily2021NonEuclidean},
\DPGCD's dependence on the dimension is \textit{logarithmic}. For
convex objectives, its utility is $O(\log(p)/n^{2/3}\epsilon^{2/3})$,
matching that of \DPFW and known lower bounds
\citep{talwar2015Nearly}. For strongly-convex problems,
\DPGCD is the first algorithm to achieve a $O(\log(p)/n^2\epsilon^2)$
utility. Indeed, the only competing result in this setting, due to
\citet{asi2021Private}, obtains a worse
 utility of $O(\log(p)^{4/3}/n^{4/3}\epsilon^{4/3})$ by using an
impractical reduction of \DPFW to the convex case. \DPGCD outperforms
this prior result without suffering the extra complexity due to the
reduction.

\textit{Bounded in $\ell_2$-norm.} Consider $R_{M,2}$ and
$\mu_{M,2}$, the $\ell_2$-norm counterparts of $R_{M,1}$ and
$\mu_{M,1}$. Assume that $R_{M,2}$ and
$\mu_{M,2}$ are both $O(1)$, as considered in \DPSGD and its variants \citep{bassily2014Differentially,wang2017Differentially}.
We compare these quantities
using the following inequalities
\citep[see][]{stich2017Approximate,nutini2015Coordinate}:
\begin{align*}
  R_{M,2}^2 \le R_{M,1}^2 \le p R_{M,2}^2\enspace,~~~~
  \tfrac{1}{p}\mu_{M,2} \le \mu_{M,1} \le
  \mu_{M,2}\enspace.
\end{align*}
In the best case of these inequalities, the $O(\log p)$ utility bounds
of the bounded $\ell_1$ norm regime are preserved in the bounded
$\ell_2$ scenario. In the worst case, the utility of \DPGCD becomes
$\widetilde O (p^{2/3}/n^ {2/3}\epsilon^{2/3})$ and
$\widetilde O(p^2/n^2\epsilon^2)$ for convex and strongly-convex
objectives respectively. These worst-case results match \DPFW's
utility in the convex setting (see \eg
\Citet{asi2021Private}), but they do not match \DPSGD's utility.
%of $O(\sqrt{p}/n\epsilon)$ (resp. $O(p/n^2\epsilon^2)$),
However, this sheds light on an interesting phenomenon: \DPGCD
\emph{interpolates between $\ell_1$- and $\ell_2$-norm
  regimes}. Indeed, it lies somewhere between the two extreme cases we
just described, depending on how the $\ell_1$- and $\ell_2$-norm
constants compare. Most interestingly, it does so without \textit{a
  priori} knowledge of the problem or explicit constraint on the
domain. Whether there exists an algorithm that yields optimal utility
in all regimes is an interesting open question.

\paragraph{Coordinate-wise regularity} Due to its use of
coordinate-wise step sizes, \DPGCD can adapt to coordinate-wise
imbalance of the objective in the same way as its randomized counterpart,
\DPCD, where coordinates are chosen uniformly at random
\citep{mangold2021Differentially}. This adaptivity notably appears in \Cref
{thm:greedy-cd-utility}
through the measurement of $R_{M,1}$ and $\mu_{M,1}$ relatively to the
scaled norm $\norm{\cdot}_{M,1}$ (as defined in
\Cref{sec:preliminaries}). We refer to
\citep{mangold2021Differentially} for detailed discussion of these
quantities and the associated gains compared to full gradient methods like
\DPSGD.





% \textbf{Discussion of the rates.} We now analyze the utility guarantees
% obtained in
% Theorem~\ref{thm:greedy-cd-utility}. The
% explicit dependence on the dimension
% $p$ in the utility of \DPGCD is only \emph{logarithmic}. Furthermore, note
% that
% $R_{M,2}^2 \le R_{M,1}^2 \le p R_{M,2}^2$, where
% $R_{M,2}$ is the counterpart of $R_{M,1}$ \wrt the
% $\ell_2$-norm \citep[see][]{stich2017Approximate}, and that
% $\tfrac{\mu_2}{pM_{\max}} \le \mu_{M,1} \le
% \tfrac{\mu_2}{M_{\min}}$ \citep[see][]{nutini2015Coordinate}.
% $R_{M,1}$ and
% $\mu_{M,1}$ can thus be independent of the dimension. In such favorable
% settings, \emph{\DPGCD achieves nearly dimension-independent
% utility}. This is notably the case when the
% $M_j$'s are imbalanced \citep[see][]{mangold2021Differentially}, and when most
% of the error comes from a
% constant
% number of coordinates.\aurelien{maybe recall that \DPSGD always suffers a $
% \sqrt{p}$ dependence because it updates all coordinates? ``In contrast, recall
% that the popular \DPSGD algorithm...''}

% \aurelien{it would also be nice to emphasize that, like \DPCD, \DPGCD
% performs better than full gradient approaches on problems with imbalanced
% coordinate-wise smoothness constants due to
% its ability to scale the step size in a coordinate-wise fashion (something
% like that). This is captured by the norms that depend on $M$.}

% More importantly, this happens with sparse solutions (when
% initializing \DPGCD with $w^0=0$): then,
% $R_{M,1}$ scales with the number of non-zeros in the problem's
% solutions.

% Furthermore, we will show in \Cref{sec:fast-init-conv} that \DPGCD can
% achieve dimension-independent utility (for strongly-convex losses)
% even when the solutions are not sparse.

% In the convex case, \DPGCD thus achieves nearly
% dimension-independent utility when $R_M=O(1)$. This notably happens
% when \DPGCD is initialized with $w^0=0$ and there exist sparse
% solutions to \eqref{eq:dp-erm}, or when the $M_j$'s are
% . For strongly-convex functions, $\mu_1=O(1)$


% when
% coordinates are very . \citet{fang2020Greed}'s work suggest
% that sparsity


% Thus, as soon as $R_M = O(1)$ or $\mu_{M} = O(1)$, \DPGCD
% achieves nearly dimension-independent utility. Importantly, this is
% the case when the algorithm is initialized with $w^0=0$ and there
% exist sparse solutions to \eqref{eq:dp-erm}.\aurelien{true for $R_M$, but what
% about $\mu_M$?}

% In the convex setting, when $R_M=O(1)$, \DPGCD matches the utility
% of \DPFW \citep{talwar2015Nearly} for \DPERM with
% $\ell_1$-constraints. Importantly, \DPGCD can achieve this $O(\log p)$ rate for
% unconstrained, smooth ERM problems: we are not aware of previously
% proposed algorithms achieving such utility. We also propose and
% empirically evaluate a natural proximal extension of \DPGCD to handle
% nonsmooth regularization (such as $\ell_1$) in
% Section~\ref{sec:experiments-1}.
% % In problems where the solution is very
% %  (typically $\ell_2$-regularized problems with some dominant
% % parameters), it can also be the case that $R_M=O(1)$ without $\ell_1$
% % regularization. \paul{bad formulation}\aurelien{also you have already said
% % this in the previous paragraph. remove?}

% In the strongly-convex setting, \DPGCD is the first algorithm (to our
% knowledge) that can exploit strong-convexity \wrt the
% $\ell_1$-norm. Its error can be as low as $O(\log(p)/n^2\epsilon^2)$
% when $\mu_{M,1}=O(1)$. The closest result (although in a different
% setting) is the one of \citet{asi2021Private}, who achieve
% $O(\log(p)^2/n^{4/3}\epsilon^{4/3})$ utility using a Frank-Wolfe
% algorithm. We also note that their algorithm is built on a reduction to the
% convex setting which, unlike \DPGCD, can be intractable.
% %\aurelien{and algorithm is impractical, no?}




% \subsection{Optimality of \DPGCD}
% \label{sec:optimality-dp-gcd}

% We now argue that \DPGCD is nearly optimal both in convex and
% strongly-convex settings. To this end we compare the utility bounds
% stated in \Cref{thm:greedy-cd-utility} with existing lower bounds.
% For convex losses, \citet{talwar2015Nearly} proved that utility is
% $\widetilde \Omega(1/n^{2/3}\epsilon^{2/3})$. \DPGCD matches it up to
% logarithmic factors. For $\mu_1$-strongly-convex \wrt $\norm{\cdot}_1$
% losses, we extend the results of \citet{bassily2014Differentially}
% (\Cref{sec:lower-bounds-strongl}).\aurelien{missing appendix?} This shows that
% utility is
% $\widetilde O (L^2/\mu_1n^2\epsilon^2)$, which \DPGCD matches up to
% $1/\mu_1$ and other logarithmic factors. We conjecture that the
% $1/\mu_1$ factor is an artefact of our proof, and that this
% strongly-convex lower bound is tight.




\subsection{Better Utility on Quasi-Sparse Problems}
\label{sec:fast-init-conv}

% \aurelien{c'est une conséquence directe des résultats précédents? une
% intuition? une observation empirique?}
In addition to the general utility results presented above, we now exhibit a
specific setting where \DPGCD performs especially
well, namely strongly-convex problems whose solutions
are dominated by a few parameters. We call such vectors quasi-sparse.
% (see \Cref{gcd:quasi-sparsity}).
%
% In this section, we show that in the strongly convex setting, \DPGCD
% progresses very fast during its first
% iterations when the solution to problem~\eqref{eq:dp-erm} has a small number
% of large entries.
% is strongly-convex, .
% For non-private greedy coordinate descent,
% \citet{fang2020Greed} established that, for strongly-convex
% objectives, sparse solutions induce fast initial convergence.
% We generalize their results to the private (noisy) setting and to
% solutions that are not necessarily sparse, but have few large
% entries.


\begin{definition}[$(\alpha,\tau)$-quasi-sparsity]
  \label{gcd:quasi-sparsity}
  A vector $w\in\RR^p$ is $(\alpha,\tau)$-quasi-sparse if it has at
  most $\tau$ entries superior to $\alpha$ (in modulus). When
  $\alpha=0$, the vector is called $\tau$-sparse.
\end{definition}
Note that any vector in $\RR^p$ is $
(0,p)$-quasi-sparse, and for any $\tau$ there exists $\alpha>0$ such that the
vector is $(\alpha,\tau)$-quasi-sparse. In fact, $\alpha$ and $\tau$ are linked, and
$\tau(\alpha)$ can be seen as a function of $\alpha$. Of course,
quasi-sparsity will only yield meaningful improvements when $\alpha$
and $\tau$ are small simultaneously.

We now state the main result of
this section, which shows that \DPGCD
(initialized with $w^0=0$) converges to a good approximate solution in few
iterations for problems with quasi-sparse solutions.
% This is essential in \DPERM, as it reduces the amount of
% noise injected into the algorithm, thereby improving utility.
% \Cref
% {gcd:fast-initial-convergence} shows that \DPGCD progresses fast on
% strongly-convex problems with a quasi-sparse solution.

\begin{theorem}[Proof in \Cref{sec:fast-init-conv-sup}]
  \label{gcd:fast-initial-convergence}
  Consider $f$ satisfying the hypotheses of \Cref{thm:greedy-cd-utility}, with
  \Cref{algo:private-greedy-coordinate-descent} initialized at
  $w^0=0$. We denote its output $w^T$, and assume that its iterates
  remain $s$-sparse for some $s \le p$.  Assume that $f$ is
  $\mu_{M,2}$-strongly-convex \wrt $\norm{\cdot}_{M,2}$, and that the
  (unique) solution of problem~\eqref{eq:prelim:dp-erm-eq} is
  $(\alpha,\tau)$-quasi-sparse for some $\alpha,\tau\ge 0$.  Let
  $0\le T \le p-\tau$ and $\zeta\in[0,1]$. Then with probability at
  least $1 - \zeta$:
  \begin{align*}
    % \label{gcd:fast-initial-convergence:eq}
    f(w^T) - f^*
    % & \le \prod_{t=1}^T \Big(1 - \frac{\mu_{M,1}^{(t+\tau)}}{4}\Big) (f(w^0) - f^*) \\
    % & \mkern-18mu + \widetilde O \Big((T+\tau)(p - \tau)\alpha^2
    % + \frac{L_{\max}^2T(T+\tau)}{M_{\min} \mu_{M,2} n^2 \epsilon^2}\Big)             \\
     \le \prod_{t=1}^T
& \Big(1 - \tfrac{\mu_{M,2}}{4(\tau+\min(t,s))}\Big) (f(w^0) - f^*)
    \\
       & + \widetilde O \Big((T+\tau)(p - \tau)\alpha^2
    + \tfrac{L_{\max}^2T(T+\tau)}{M_{\min} \mu_{M,2} n^2 \epsilon^2}\Big)
    \enspace.
  \end{align*}
  We assume that
  $\alpha^2 = O\left( L_{\max}^2 (s+\tau) / M_{\min}\mu_{M,2}^2
    pn^2\epsilon^2 \right)$, and set the number of iterations to
  $T = \frac{s+\tau}{\mu_{M,2}} \log((f(w^0)-f^*)
  M_{\min}\mu_{M,2}n^2\epsilon^2/L^2)$. Then, we have that, with
  probability at least $1-\zeta$,
  \begin{align*}
    f(w^T) - f^* = \widetilde O\left( \frac{L_{\max}^2}{M_{\min}}
    \frac{(s+\tau)^2 \log(2p/\zeta)}{\mu_{M,2} n^2\epsilon^2 } \right)
    \enspace.
  \end{align*}
\end{theorem}
Here, strong convexity is measured in $\ell_2$ norm but the dependence
on the dimension is reduced from $p$, the ambient space dimension, to
$(s+\tau)^2$, the \emph{effective dimension of the space where the
  optimization actually takes place}. For high-dimensional sparse
problems, the latter is typically much smaller and yields a large
improvement in utility. Note that it is not necessary for the solution
to be perfectly sparse: it suffices that most of its mass is
concentrated in a fraction of the coordinates. Notably, when
$\alpha^2 = O(L_{\max}^2T/M_{\min}\mu_{M,2}pn^2\epsilon^2)$, the lack
of sparsity is smaller than the noise, and does not affect the
rate. It generalizes the results by \citet{fang2020Greed} for
non-private and sparse settings, that we recover when~$\alpha=0$
and~$\epsilon\rightarrow +\infty$.

In practice, the assumption over the iterates' sparsity is often met with
$s\ll
p$. In
the non-private setting, greedy coordinate descent is known to focus on
coordinates that are non-zero in the solution
\citep{massias2017Safe}: this keeps iterates' sparsity
close to the one of the solution. Furthermore, due to privacy constraints,
\DPGCD will often run for $T \ll p$ iterations. This is especially true in
high-dimensional problems, where the amount of noise required to
guarantee privacy does not allow many iterations (\lcf experiments in
\Cref{sec:experiments-1}).

% Importantly, it is not necessary for the solution to be perfectly
% sparse: it suffices that most of its mass is concentrated in a
% fraction of the parameters. Typically, when
% $\alpha = O(\sqrt{L_{\max}^2T/M_{\min}\mu_{M,2}pn^2\epsilon^2})$, this
% lack of sparsity is smaller than the noise required for privacy.
% Then, \DPGCD will choose the most relevant coordinates. In terms of
% utility the dimension is replaced by the solution's sparsity level,
% which can be much smaller. This result generalizes the results of
% \citet{fang2020Greed}, that we recover when~$\alpha=0$
% and~$\epsilon\rightarrow +\infty$.

% to the case of private (\emph{noisy}) GCD and to
% solutions that are not necessarily sparse but only quasi-sparse. For problems
% with
% $\tau$-sparse solutions (\ie $\alpha=0$),
% \Cref{gcd:fast-initial-convergence} recovers the non-private results
% of \citet{fang2020Greed}, up to an additive term due to privacy.
% Importantly, we show that improvements are still possible in regimes
% where the solution is not sparse (\ie, $\alpha>0$) but $\alpha$ is
% small.
% (\ie,
% $w^*$ is not perfectly sparse), important progress happens in the
% first iterations.
% Typically, when
% $\alpha = O(\sqrt{L_{\max}^2T/M_{\min}\mu_{M,2}pn^2\epsilon^2})$, the
% residual term from the quasi-sparsity is smaller than the term due to
% the noise. For such $\alpha$'s, the result is the same for
% $\tau$-sparse and $(\alpha,\tau)$-quasi-sparse vectors.

% Note that $\mu_{M,1}^{(t+\tau)}$ grows as $t + \tau$ gets smaller.
% Consequently, $1 - {\mu_{M,1}^{(t+\tau)}}/{4}$ is closer to
% zero in the first iterations (where $t$ is
% small). \Cref{gcd:fast-initial-convergence} thus proves that, for
% problems with quasi-sparse solutions, \DPGCD \emph{provably} makes
% important progress in its first iterations. The second inequality in
% \Cref{gcd:fast-initial-convergence} links the result to the
% objective's global strong-convexity constant \wrt
% $\norm{\cdot}_{M,2}$.  It shows that in this setting, the progress
% made by \DPGCD's first iterations is independent of the dimension.


% Performing many updates is very costly in private optimization, since the
% scale of the noise grows with the number of released gradients. Thus, in high
% dimensional problems where other private algorithms are unable to make any
% progress, \DPGCD's fast initial convergence may yield better solutions. We
% will illustrate this in our experiments.

% In
% \Cref{gcd:fast-initial-convergence:corollary}, we describe a setting
% where \DPGCD's utility only depends on the quasi-sparsity of the
% solution.

% \begin{corollary}
%   \label{gcd:fast-initial-convergence:corollary}
%   \paul{todo}
% \end{corollary}

% Finally,
% \Cref{gcd:fast-initial-convergence:corollary} states that on problems
% with quasi-sparse solutions, \DPGCD's does not depend on the dimension
% (up to logarithmic factors).
% \begin{corollary}
%   \paul{todo}
%   \label{gcd:fast-initial-convergence:corollary}
%   Under the hypothesis of \Cref{gcd:fast-initial-convergence}, assume
%   $p \ge 2\tau$ and take $T=\tau$, it holds that
%   \begin{align}
%     \label{gcd:fast-initial-convergence:eq}
%     f(w^T) - f^*
%     & \le \Big(1 - \frac{\mu_{M,2}}{4(T+\tau)}\Big)^T (f(w^0) - f^*)
%       + \widetilde O \Big( 2 \tau p \alpha^2
%       + \frac{T+\tau}{\mu_{M,2} n^2 \epsilon^2}\Big) \enspace.
%   \end{align}
% \end{corollary}


% \subsection{Towards Composite Problems}
% \label{sec:towards-comp-probl}

% \paul{this section still have to be proof-read}
% Our results from previous section suggest adding sparsity-inducing
% regularization terms to the objective of~\eqref{eq:prelim:dp-erm-eq}, such as
% $\lambda\norm{w}_1$, for some $\lambda>0$
% \citep{tibshirani1996Regression}. This makes the objective non-smooth,
% which is not covered by \Cref{algo:private-greedy-coordinate-descent}
% nor our analysis. Multiple greedy rules have been proposed to adapt to
% this setting (see \eg,
% \citet{tseng2009Coordinate,nutini2015Coordinate}). We describe them,
% together with a proximal version of \DPGCD, in
% \Cref{sec:greedy-coord-desc}, and we evaluate them numerically in
% \Cref{sec:experiments-1}.

% Theoretical analysis of proximal greedy coordinate descent is known to
% be difficult. The best existing approach is the one of
% \citet{karimireddy2019Efficient}, that is restricted to
% $\ell_1$-regularized and box-constrained problems. Their analysis
% relies on splitting updates between \good steps, that makes provable
% progress, and \bad steps, that do not increase the objective. They
% then modify the algorithm so that after each \bad step, a \good one is
% guaranteed. In private optimization, this trick does not work. The
% noise required by privacy causes two major problems: first, \bad steps
% can increase the objective and second, \good steps are no longer
% guaranteed after the \bad ones.

\subsection{Proximal \DPGCD}
\label{sec:proximal-dp-gcd}

In \Cref{sec:fast-init-conv}, we proved that \DPGCD's utility is
improved when problem's solution is (quasi-)sparse. This motivates us
to consider problems with sparsity-inducing regularization (\ie when
$\psi \neq 0$ in \eqref{eq:prelim:dp-erm-eq}), such as the $\ell_1$
norm of $w$ \citep{tibshirani1996Regression}.  To tackle such
non-smooth terms, we propose a proximal version of \DPGCD (for which
the same privacy guarantees hold), building upon the multiple greedy
rules that have been proposed for the nonsmooth setting \citep[see
\eg][]{tseng2009Coordinate,nutini2015Coordinate}.  We describe this
algorithm in \Cref{algo:proximal-dp-gcd}.

% \TODO{ Put in normal algo environment. }

% \begin{algorithm}[h]
%   \caption{\DPGCD (Proximal Version): Private Proximal Greedy CD}
%   \label{algo:private-prox-greedy-coordinate-descent}
%   \begin{algorithmic}[1]
%     \State \textbf{Input:} initial $w^0 \in \RR^p$, iteration count $T > 0, \forall j \in [p],$ noise scales $\lambda_j, \lambda_j'$, step sizes $\gamma_j > 0$.
%     \For{$t = 0$ to $T-1$}
%       \State Select $j$ by the noisy \GSs, \GSr or \GSq rule.
%       \State $\displaystyle w^{t+1} = w^t + (\prox{\gamma_j\psi_j} (w^t - \gamma_j (\nabla_j f(w^t) + \eta_{j}^t)) - w_j^t) e_j$, \hfill  $\eta_{j}^t \sim \Lap(\lambda_{j})$.
%     \label{algo:private-prox-greedy-coordinate-descent:update}
%     \EndFor
%     \State \Return $w^T$.
%   \end{algorithmic}
% \end{algorithm}

\begin{myalgorithm}
  {Proximal \DPGCD: Differentially Private Proximal Greedy Coordinate Descent}
  {initial point $w^0$, noise scales $\lambda_1, \dots, \lambda_p, > 0$, $\lambda_1', \dots, \lambda_p', > 0$, step sizes $\gamma_1, \dots, \gamma_p > 0$, number of iteration $T > 0$}
  {$w^T$}
  \setlist[itemize]{align=parleft,left=0pt..1.5em}
  \label{algo:proximal-dp-gcd}
  For $t = 0$ to $T-1$:
  \vspace{-1em}
  \begin{itemize}
  \item Select $j$ by the noisy \GSs, \GSr or \GSq rule with noise
    scales $\lambda_1', \dots \lambda_p'$
  \item Set $w^{t+1} = w^t$
  \item Update
    $w_j^{t+1} = \prox{\gamma_j\psi_j} (w^t - \gamma_j (\nabla_j
    f(w^t) + \eta_{j}^t))$, with
    $\eta_{j}^t \sim \Lap(\lambda_{j})$
  \end{itemize}
  \vspace{-1em}
\end{myalgorithm}
The same privacy guarantees as for the smooth \DPGCD algorithm hold
since, privacy-wise, the proximal step is a post-processing step.  We
also adapt the greedy selection rule to incorporate the non-smooth
term. We can use one of the following three rules
\begin{align}
  j & = \argmax_{j\in[p]} \min_{\xi_j\in\partial\psi_j(w_j)} \frac{1}{\sqrt{M_j}}\abs{\nabla_j f(w^t) + \eta_j^t + \xi_j}\tag{\texttt{GS-s}} \enspace, \\
  j & = \argmax_{j\in[p]} \sqrt{M_j} \abs{\prox_{\frac{1}{M_j}\psi_j}(w_j^t - \frac{1}{M_j}(\nabla_j f(w^t) + \eta_j^t) - w_j^t} \tag{\texttt{GS-r}} \enspace, \\
  j & = \argmax_{j\in[p]} \min_{\alpha\in\RR} \nabla_j f(w^t) \alpha + \frac{M_j}{2} \alpha^2 + \psi_j(w_j^t + \alpha) - \psi_j(w_j^t) \tag{\texttt{GS-q}} \enspace.
\end{align}
These rules are commonly considered in the non-private GCD literature
\citep[see \eg][]{tseng2009Coordinate,shi2017Primer,karimireddy2019Efficient},
except for the noise $\eta_j^t$ and the rescaling in the \GSs and \GSr
rules.


\subsection{Computational Cost}
\label{sec:computational-cost}

Each iteration of \DPGCD requires computing a full gradient, but
only uses one of its coordinates. In non-private optimization, one would
generally be better off performing the full update to avoid wasting
computation. This is not
the case when gradients are private. Indeed, using the full gradient
requires privatizing $p$ coordinates, even when only a few of
them may be needed. Conversely, the report noisy max
mechanism
\citep{dwork2014Algorithmic} allows to select these entries
\emph{without paying the full privacy cost of dimension}. Hence, the
greedy updates of \DPGCD reduce the noise needed at the cost of more
computation.

In practice, the higher computational cost of each iteration may not always
translate in a significantly larger cost overall: as
shown by our theoretical results, \DPGCD is able to exploit the
\emph{quasi-sparsity} of the solution to progress fast and only a
handful of iterations may be needed to reach a good private
solution. In contrast, most updates of classic private optimization algorithms
(like \DPSGD) may not be worth doing, and lead to unnecessary injection
of noise. We illustrate this phenomenon numerically in
\Cref{sec:experiments-1}.

% However, in non-sparse and balanced problems, the
% greedy update does not improve over random selection. In these cases,
% we suggest using \DPCD with random coordinate selection
% \citep{mangold2021Differentially}.

% Note that the greedy update can sometimes be computed more
% efficiently. Multiple works tackled this problem, either through a
% nearest-neighbors based approach \citep{dhillon2011Nearest}, active
% sets \citep{stich2017Approximate} or by casting the greedy rule to a
% MIPS problem \citep{karimireddy2019Efficient}. All these approaches
% compute an approximation of the greedy coordinate. Since the private
% greedy selection already induces some error, we can expect that these
% approximation schemes could come without extra cost on utility
% in our setting.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../main"
%%% End:
