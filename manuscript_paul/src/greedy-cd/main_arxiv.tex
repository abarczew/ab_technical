\documentclass[twoside]{article}

\usepackage{fullpage}
\usepackage{authblk}
%\usepackage[accepted]{aistats2023}
% If your paper is accepted, change the options for the package
% aistats2023 as follows:
%
%\usepackage[accepted]{aistats2023}
%
% This option will print headings for the title of your paper and
% headings for the authors names, plus a copyright note at the end of
% the first column of the first page.

% If you set papersize explicitly, activate the following three lines:
%\special{papersize = 8.5in, 11in}
%\setlength{\pdfpageheight}{11in}
%\setlength{\pdfpagewidth}{8.5in}

% If you use natbib package, activate the following three lines:
%\usepackage[round]{natbib}
%\renewcommand{\bibname}{References}
%\renewcommand{\bibsection}{\subsubsection*{\bibname}}

% If you use BibTeX in apalike style, activate the following line:
%\bibliographystyle{apalike}


\usepackage[backend=biber,natbib=true,style=authoryear,doi=false,isbn=false,url=false,eprint=false,giveninits=true,maxbibnames=200,maxcitenames=2,mincitenames=1,uniquename=false,uniquelist=false,dashed=false]{biblatex}

\newcommand{\noopsort}[1]{#1}
\newcommand{\bm}[1]{#1}



\addbibresource{references.bib}% Syntax for version >= 1.2

\DeclareSourcemap{
  \maps[datatype=bibtex]{
    \map{
      \step[fieldsource=url, match=\regexp{http://(dx.doi.org/|dl.acm.org/)}, final]
      \step[fieldset=url, null]
      \step[fieldset=urldate, null]
    }
  }
}
\setlength\bibitemsep{1.5\itemsep}

% % For theorems and such
\usepackage{amsmath}
\usepackage{amssymb}
% \usepackage{mathtools}
\usepackage{amsthm}


\usepackage{subcaption}

\usepackage{algorithm}
\usepackage[noend]{algpseudocode}

\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{cleveref}

\usepackage{shortcuts}
\usepackage{shortcuts_neurips2022}

\usepackage{tabularx,booktabs}
\usepackage{csvsimple}


\definecolor{linkcolor}{RGB}{83,83,182}

\hypersetup{
  colorlinks=true,
  citecolor=linkcolor,
  linkcolor=linkcolor,
  urlcolor=linkcolor
}

\begin{document}

% If your paper is accepted and the title of your paper is very long,
% the style will print as headings an error message. Use the following
% command to supply a shorter title of your paper so that it can be
% used as headings.
%
% \runningtitle{High-Dimensional Private Empirical Risk Minimization by Greedy Coordinate Descent}

% If your paper is accepted and the number of authors is large, the
% style will print as headings an error message. Use the following
% command to supply a shorter version of the authors names so that
% they can be used as headings (for example, use only the surnames)
%
%\runningauthor{Surname 1, Surname 2, Surname 3, ...., Surname n}

% \twocolumn[

%   \aistatstitle{High-Dimensional Private Empirical Risk Minimization\\ by Greedy
%     Coordinate Descent}

%   \aistatsauthor{ Paul Mangold
%     \And Aurélien Bellet
%     \And Joseph Salmon
%     \And Marc Tommasi}

%   % \aistatsaddress{ Inria Lille
%   %   \And Inria Lille
%   %   \And Univ. Montpellier
%   %   \And Univ. Lille} ]

%   \aistatsaddress{
%     Univ. Lille, Inria, \\
%     CNRS, Centrale Lille, \\
%     UMR 9189 - CRIStAL, \\
%     F-59000 Lille, France
%     \And
%     Univ. Lille, Inria, \\
%     CNRS, Centrale Lille, \\
%     UMR 9189 - CRIStAL, \\
%     F-59000 Lille, France
%     \And
%     IMAG, Univ Montpellier, \\
%     CNRS, Montpellier, France \\
%     Institut Universitaire \\
%     de France (IUF)
%     \And
%     Univ. Lille, CNRS, \\
%     Inria, Centrale Lille, \\
%     UMR 9189 - CRIStAL, \\
%     F-59000 Lille, France
%   } ]


\title{High-Dimensional Private Empirical Risk Minimization\\ by Greedy Coordinate Descent}
\date{}

\author[1]{Paul Mangold}
\author[1]{Aurélien Bellet}
\author[2,3]{Joseph Salmon}
\author[4]{Marc Tommasi}

\affil[1]{Univ. Lille, Inria,  CNRS, Centrale Lille, UMR 9189 - CRIStAL, F-59000 Lille, France}
\affil[2]{IMAG, Univ Montpellier, CNRS, Montpellier, France}
\affil[3]{Institut Universitaire de France (IUF)}
\affil[4]{Univ. Lille, CNRS, Inria, Centrale Lille,  UMR 9189 - CRIStAL, F-59000 Lille, France}

\maketitle

\begin{abstract}
  In this paper, we study differentially private empirical risk
  minimization (DP-ERM). It has been shown that the worst-case
  utility of DP-ERM reduces polynomially as the dimension increases. This is a
  major obstacle to privately learning large machine learning
  models. In high dimension, it is common for some model's parameters
  to carry more information than others. To exploit this, we propose a
  differentially private greedy coordinate descent (DP-GCD)
  algorithm. At each iteration, DP-GCD privately performs a
  coordinate-wise gradient step along the gradients' (approximately)
  greatest entry. We show theoretically that DP-GCD can achieve a
  logarithmic dependence on the dimension for a wide range of problems by
  naturally exploiting their structural properties (such as
  quasi-sparse solutions). We
  illustrate this behavior numerically, both on synthetic and real datasets.
  % Finally,
  % we describe promising directions for future work.
\end{abstract}


\input{intro}
\input{related-works}
\input{prelim}
\input{greedy-cd}
\input{expe}
\input{conclu}

\section*{Acknowledgments}

The authors would like to thank the anonymous reviewers who provided
useful feedback on previous versions of this work, which helped to improve
the paper.

\looseness=-1 This work was supported by the Inria Exploratory Action FLAMED
and by the French National Research Agency (ANR) through grant ANR-20-CE23-0015
(Project PRIDE), ANR-20-CHIA-0001-01 (Chaire IA CaMeLOt) and ANR 22-PECY-0002 IPOP
(Interdisciplinary Project on Privacy) project of the Cybersecurity PEPR.


\printbibliography


% \subsubsection*{Acknowledgements}
% All acknowledgments go at the end of the paper, including thanks to reviewers who gave useful comments, to colleagues who contributed to the ideas, and to funding agencies and corporate sponsors that provided financial support.
% To preserve the anonymity, please include acknowledgments \emph{only} in the camera-ready papers.
\appendix
\onecolumn

% {\color{red}\textbf{Warning}: we have re-organized the appendix after the
%   submission of the main text, so the references in the main text
%   pointing to sections of the appendix may be wrong.
%   Please use this supplementary version to get the right references.}

\input{sup-proof-privacy}
\input{sup-proof-utility}
\input{sup-fast-initial}
\input{sup-proximal-gcd}
\input{sup-expe-details}

%\setcounter{page}{24}
%\setcounter{figure}{3}

\input{sup-expe-time}


\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
