% !TEX root = main.tex


\section{Introduction}

In this Chapter, we propose the differentially private greedy
coordinate descent algorithm (\DPGCD). This algorithm extends the \GCD
algorithm that we presented in \Cref{sec:greedy-coord-desc-1} to the
differentially private setting. Similarly to \DPCD, \DPGCD updates one
coordinate at a time, but instead of choosing the coordinate randomly,
it chooses the one with the largest gradient entry. We describe \DPGCD
and analyze it both theoretically and empirically as a solver of the
unconstrained smooth \DPERM problem. We recall that $\cX$ denotes a
feature space and $\cY$ a label space, and that we have a dataset
$D = \{d_1, \dots, d_n\} \subseteq (\cX \times \cY)^n$ of $n$
records. We aim at solving the following smooth empirical risk
minimization problem:
\begin{align}
  w^* \in \argmin_{w \in \RR^p} \big\{ f(w)
  \big\}
  \enspace,
  \enspace
  \text{ with } f(w) = \frac{1}{n} \sum_{i=1}^n \ell(w; d_i)
  \enspace,
  \label{eq:prelim:dp-erm-eq-chap-cd}
  \tag{$\star'$}
\end{align}
where $p$ can be large,
$\ell: \cW \times \cX \times \cY \rightarrow \RR$ is a loss function
which is convex and coordinate-wise smooth in its first parameter.

As we discussed in \Cref{cha:diff-priv-mach}, solving
\eqref{eq:prelim:dp-erm-eq-chap-cd} using a differentially private
algorithm necessarily decreases the utility of the trained
model. Specifically, existing lower bounds on utility for a fixed
privacy budget (see \Cref{sec:lower-bounds}) show that utility
decreases polynomially with the dimension $p$. Since machine learning
models are often high-dimensional (\eg $n \approx p$ or even
$n \ll p$), this is a massive drawback for the practical use of
differentially private empirical risk minimization. To go beyond this
negative result, one can leverage the fact that high-dimensional
problems often exhibit some \emph{structure}. In particular, some
parameters are typically more significant than others: it is notably
(but not only) the case when models are sparse, which is often desired
in high dimension \citep{tibshirani1996Regression}. Private learning
algorithms could thus be designed to exploit this by focusing on the
most significant parameters of the problem.
% maximizing the improvement in utility at each iteration.
Several works have tried to exploit such high-dimensional problems' structure
to reduce the dependence on the dimension, \eg from polynomial to
logarithmic.
\citet{talwar2015Nearly,bassily2021NonEuclidean,asi2021Private}
proposed a DP Frank-Wolfe algorithm (\DPFW) that exploits the solution's
sparsity. However, their algorithm only works on $\ell_1$-constrained
\DPERM, restricting its range of application.
For sparse linear regression,
% \citet{wang2021Sparse} developed a truncated variant of \DPSGD
% (that only keeps the largest gradient entries), that also exploits
% sparsity.
\citet{kifer2012Private} proposed to first identify some support and
then solve the \DPERM problem on the restricted support.
Unfortunately, their approach requires implicit knowledge of the
solution's sparsity.
% their approach relies either on prior
% knowledge on the solution's sparsity, or on the tuning of an
% additional hyperparameter.
Finally, \citet{kairouz2021Nearly,zhou2021Bypassing} used public data
to estimate lower-dimensional subspaces, where the gradient can be
computed at a reduced privacy cost. A key limitation is that such
public data set, from the same domain as the private data, is
typically not available in learning scenarios involving sensitive
data.

The differentially private greedy coordinate descent algorithm
(\DPGCD), that we propose in this Chapter, does not have these
pitfalls. At each iteration, \DPGCD privately determines the
gradient's greatest coordinate, and performs a gradient step in its
direction. It can thus focus on the most useful parameters, avoiding
to waste privacy budget on updating non-significant ones.  Formally,
we show that \DPGCD reduces the dependence on the dimension from
polynomial to logarithmic for a wide range of unconstrained
problems. This is the first algorithm to obtain such gains without
relying on $\ell_1$ or $\ell_0$ constraints. In fact, \DPGCD's utility
naturally depends on $\ell_1$-norm quantities (\ie distance from
initialization to optimal or strong-convexity parameter) and spans two
different regimes. When these $\ell_1$-norm quantities are $O(1)$ as
assumed in \DPFW, \DPGCD attains $O(\log(p)/n^{2/3}\epsilon^{2/3})$
and $O(\log(p)/n^2\epsilon^2)$ utility on convex and strongly-convex
problems respectively, outperforming existing \DPFW algorithms without
solving a constrained problem. In the second regime, when the
$\ell_2$-norm counterpart of the above quantities are $O(1)$ as
assumed for \DPSGD and its variants, we show that \DPGCD adapts to the
problem's underlying structure. Specifically, it is able to
\emph{interpolate between logarithmic and polynomial dependence on the
  dimension}.  In addition to these general utility results, we prove
that for strongly convex problems with quasi-sparse solutions
(including but not limited to sparse problems), \DPGCD converges to a
good approximate solution in few iterations. This improves utility in
the $\ell_2$-norm setting, replacing the polynomial dependence on the
ambient space's dimension by the quasi-sparsity level of the solution.
% This property is very appealing in private
% optimization, where performing more iterations requires increasing the
% noise added at each iteration to guarantee privacy.
% \paul{without analysis is a bit of a strong formulation...}
% Motivated by this property, we propose (without analysis) a proximal
% variant of \DPGCD, allowing the use of sparsity-inducing regularizers
% such as the $\ell_1$-norm of the parameters.
We evaluate both our
algorithms numerically on real and synthetic datasets, validating our
theoretical observations.

% These theoretical observations are then confirmed by
% our experiments on real and synthetic datasets.


% We show formally that \DPGCD achieves an excess risk of
% $\widetilde O(\log(p)/\mu_1^2n^2\epsilon^2)$ on problems with
% $\mu_1$-strongly-convex objective \wrt the $\ell_1$-norm. When
% $\mu_1=O(1)$, \DPGCD is thus the first algorithm (to our knowledge)
% whose utility scales with $\log(p)/n^2$. We derive rates for the
% convex case as well. Furthermore, we prove that on problems with
% quasi-sparse solutions (including but not limited to sparse problems,
% see \Cref{gcd:quasi-sparsity}), \DPGCD progresses particularly fast in
% the first iterations. This property is very appealing in private
% optimization, where performing more iterations requires increasing the
% noise added at each iteration to guarantee privacy. These theoretical
% observations are then
% confirmed by our experiments on real and synthetic datasets.
% \aurelien{perhaps we could also mention the proximal version, even if we do
% not have theory?}

The contributions of this Chapter can be summarized as follows:
% although with key differences. It operates on non-constained
% optimization problems, and can \emph{always} generate sparse
% solutions, while \DPFW only does so in LASSO problems. Other works
% have tried exploiting similar ideas, either by using public data to
% compute gradient on a restricted subspace \citep{kairouz2021Nearly},
% or ... \paul{complete list, maybe AdaClip?}. Contrary to these works,
% our algorithm do not require public data (which can be hard to
% collect). Our contributions can be summarized as follows
\begin{enumerate}
  \item We propose differentially private greedy coordinate descent
        (\DPGCD), a method that performs
        updates along the (approximately) greatest entry of the gradient. We
        formally establish
        its privacy guarantees, and derive high
        probability utility upper bounds.
  \item We prove that \DPGCD exploits structural
        properties of the problem (\eg quasi-sparse solutions) to
        improve utility. Importantly, \DPGCD
        does not require prior knowledge of this structure to exploit
        it.
  \item We empirically validate our theoretical results on a variety of
        synthetic and real datasets, showing that \DPGCD outperforms
        existing private algorithms on high-dimensional problems with
        quasi-sparse solutions.
\end{enumerate}

The rest of the Chapter is organized as follows. First, we discuss
related work in more details in
\Cref{sec:related-works}. \Cref{sec:priv-greedy-coord} then introduces
\DPGCD, and formally analyzes its privacy and utility. We validate our
theoretical results numerically in \Cref{sec:experiments-1}. Finally,
we conclude and discuss the limitations of our results in
\Cref{sec:conclusion-and-discussion}.


% \paul{probably move this discussion in a separated section} First, the
% setting is different: we consider non-constrained, smooth
% optimization, while \DPFW is restricted to constrained
% problems. Second, our algorithm \emph{always} operates on one
% coordinate at a time, which allows finding sparse approximations even
% in non-sparse problems, while \DPFW is limited to the LASSO.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../main"
%%% End:
