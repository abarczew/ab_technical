% !TEX root = ../../main.tex

\section{Lemmas on Sensitivity}
\label{sec:lemma-sensitivity}

% \aurelien{in practice we will use this on $\ell$, which takes as input a
% single data point. I think it would be simpler to write this
% part with respect to $d\in\cX$ rather than for $D\in\cD$}

In this section, we let $\cX$ be the universe where the data is drawn from.
To upper bound the sensitivities of a function's gradient, we start by
recalling in \Cref{lemma:gradient-upper-bound} that (coordinate) gradients are
bounded by (coordinate-wise-)Lipschitz constants.
We then link this upper bound with gradients' sensitivities in
\Cref{lemma:gradient-sensitivity-upper-bound}.

\begin{lemma}
  \label{lemma:gradient-upper-bound}
  Let $\ell : \RR^p \times \cX \rightarrow \RR$ be convex and differentiable
  in its first argument,
  $\Lambda > 0$ and $L_1, \dots, L_p > 0$.
  \begin{enumerate}
  \item If $\ell(\cdot; d)$ is $\Lambda$-Lipschitz for all $d \in \cX$, then
    $\norm{\nabla \ell(w; d)}_2 \le \Lambda$ for all $w \in \RR^p$ and $d \in \cX$.
  \item If $\ell(\cdot; d)$ is $L$-coordinate-Lipschitz for all $d \in \cX$, then
    $\abs{\nabla_j \ell(w; d)} \le L_j$ for all $w \in \RR^p$, $d \in \cX$
    and $j \in [p]$.
  \end{enumerate}
\end{lemma}

\begin{proof}
  % \jo{todo giving both proof is welcome! }
  Let $d \in \cX$.
  % For readability, we omit in this proof the second argument $D$ of $\ell(\cdot;
  % D)$.
  We start by proving the first statement.
  First, if $\nabla \ell(w;d) = 0$, $\norm{\nabla \ell(w;d)}_2 = 0 \le \Lambda$ and
  the
  result holds.
  Second, we focus on the case where $\nabla \ell(w;d) \not= 0$.
  The convexity of $\ell$ gives, for $w \in \RR^p$, $d \in \cX$:
  \begin{align}
    \ell(w + \nabla \ell(w;d);d)
    & \ge \ell(w;d) + \scalar{\nabla \ell(w;d)}{\nabla \ell(w;d)}
      \nnl
    & = \ell(w;d) + \norm{\nabla \ell(w;d)}_2^2\enspace,
  \end{align}
  then, reorganizing the terms and using $\Lambda$-Lipschitzness of $\ell$
  yields
  \begin{align}
    \norm{\nabla \ell(w;d)}_2^2
    & \le \ell(w + \nabla \ell(w;d);d) - \ell(w;d)
      \nnl
    & \le \abs{\ell(w + \nabla \ell(w;d);d) - \ell(w;d)}
      \nnl
    & \le \Lambda \norm{\nabla \ell(w;d)}_2\enspace,
  \end{align}
  and the result follows after dividing by $\norm{\nabla \ell(w;d)}_2$.
  To prove the second statement, we set $j \in [p]$, and $w \in \RR^p$,
  and remark that if $\nabla_j \ell(w;d)=0$, then $\abs{\nabla_j \ell(w;d)} \le
  L_j$.
  When $\nabla_j \ell(w;d) \neq 0$, the convexity of $\ell$ yields
  \begin{align}
    \ell(w + \nabla_j \ell(w;d) e_j;d)
    &\ge \ell(w;d) + \scalar{\nabla \ell(w;d)}{\nabla_j \ell(w;d) e_j}
      \nnl
    &= \ell(w;d) + \nabla_j \ell(w;d)^2\enspace.
  \end{align}
  Reorganizing the terms and using $L$-coordinate-Lipschitzness of $\ell$ gives
  \begin{align}
    \nabla_j \ell(w;d)^2
    & \le \ell(w + \nabla_j \ell(w;d) e_j;d) - \ell(w;d)
      \nnl
    & \le \abs{\ell(w + \nabla_j \ell(w;d) e_j;d) - \ell(w;d)}
      \nnl
    & \le L_j \abs{\nabla_j\ell(w;d)}\enspace,
  \end{align}
  and we get the result after dividing by $\abs{\nabla_j \ell(w;d)}$.
%  \jo{that requires you can divide the partial gradient, so it must be non zero. Might get into trouble when this is not the case...say constant functions ?}
\end{proof}

\begin{lemma}
  \label{lemma:gradient-sensitivity-upper-bound}
  Let $\ell : \RR^p \times \cX \rightarrow \RR$ be convex and differentiable
  in its 1st argument,
  $\Lambda > 0$ and $L_1, \dots, L_p > 0$.
  \begin{enumerate}
  \item If $\ell(\cdot; d)$ is $\Lambda$-Lipschitz for all $d \in \cX$, then
    $\Delta(\nabla \ell) \le 2\Lambda$.
  \item If $\ell(\cdot; d)$ is $L$-coordinate-Lipschitz for all $d \in \cX$, then
    $\Delta(\nabla_j \ell) \le L_j$ for all $j \in [p]$.
  \end{enumerate}
\end{lemma}

\begin{proof}
%  \jo{todo giving both proof is welcome! }
  We start by proving the first statement.
  Let $w, w' \in \RR^p$, $d, d' \in \cX$.
  From the triangle inequality and \Cref{lemma:gradient-upper-bound}, we get
  the following
  upper bounds:
  \begin{align}
    \norm{\nabla \ell(w; d) - \nabla \ell(w'; d')}_2
    \le \abs{\nabla \ell(w; d)} + \abs{\nabla \ell(w'; d')}
    \le 2\Lambda\enspace,
  \end{align}
  which is the claim of the first statement.
  To prove the second statement, we proceed similarly: the triangle inequality
  and \Cref{lemma:gradient-upper-bound} give the following upper bounds:
  \begin{align}
    \abs{\nabla_j \ell(w; d) - \nabla_j \ell(w'; d')}
    \le \abs{\nabla_j \ell(w; d)} + \abs{\nabla_j \ell(w'; d')}
    \le 2L_j\enspace,
  \end{align}
  which is the desired result.
\end{proof}

% We obtain the inequality \eqref{eq:delta-lipschitz-norm} stated in
% Section~\ref{sec:preliminaries} as a corollary.

We can therefore obtain a bound on the sum of the sensitivities of the
functions $d \mapsto 1/M_j \nabla_j \ell(w; d)^2$ for $j \in [p]$,
$w \in \RR^p$. We denote this sum $\Delta_{M^{-1}}(\nabla \ell)^2$.
\begin{corollary}
  Let $L_1, \dots, L_p > 0$.
  Let $\ell(\cdot; d) : \RR^p \rightarrow \RR$ be a convex,
  $L$-coordinate-Lipschitz function for all $d \in \cX$.
  Then
  \begin{align}
    \Delta_{M^{-1}}(\nabla \ell)
    = \Big(\sum_{j=1}^p \frac{1}{M_j} \Delta (\nabla_j\ell)^2\Big)^{\frac{1}
    {2}}
    \le \Big(\sum_{j=1}^p \frac{4}{M_j} L_j^2\Big)^{\frac{1}{2}}
    = 2 \norm{L}_{M^{-1}}\enspace.
  \end{align}
\end{corollary}

% \begin{lemma}
%   \label{lemma:sensitivity-of-1n-func}
%   Let $\alpha > 0$ and $f : \RR^p \times \cD \rightarrow \RR$. Define the function
%   $h : (w, D) \mapsto \alpha \ell(w; d)$.
%   Then for all $w, w' \in \RR^p$ and $D, D' \in \cD$, it holds that
%   \begin{align}
%     \norm{h(w; d) - h(w'; d')}_2 =
%     \alpha \norm{\ell(w; d) - \ell(w'; d')}_2,
%   \end{align}
%   therefore $\Delta(h) = \alpha \Delta(f)$.
% \end{lemma}
% \aurelien{not sure this lemma is needed, this is really elementary!}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../main"
%%% End:
