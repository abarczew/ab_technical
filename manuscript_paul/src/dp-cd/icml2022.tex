%%%%%%%% ICML 2022 EXAMPLE LATEX SUBMISSION FILE %%%%%%%%%%%%%%%%%

\documentclass[accepted,nohyperref]{article}

% Recommended, but optional, packages for figures and better typesetting:
\usepackage{microtype}
\usepackage{graphicx}
% \usepackage{subfigure}
\usepackage{subcaption}
\usepackage{booktabs} % for professional tables

% hyperref makes hyperlinks in the resulting PDF.
% If your build breaks (sometimes temporarily if a hyperlink spans a page)
% please comment out the following usepackage line and replace
% \usepackage{../../main} with \usepackage[nohyperref]{../../main} above.
\usepackage{hyperref}


% Attempt to make hyperref and algorithmic work together better:
\newcommand{\theHalgorithm}{\arabic{algorithm}}

% Use the following line for the initial blind version submitted for review:
% \usepackage{../../main}

% If accepted, instead use the following line for the camera-ready submission:
\usepackage[accepted]{../../main}

% For theorems and such
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{amsthm}

% if you use cleveref..
\usepackage[capitalize,noabbrev]{cleveref}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% THEOREMS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{assumption}[theorem]{Assumption}
% \theoremstyle{remark}
\newtheorem{remark}[theorem]{Remark}

% Todonotes is useful during development; simply uncomment the next line
%    and comment out the line below the next line to turn off comments
%\usepackage[disable,textsize=tiny]{todonotes}
%\usepackage[textsize=tiny]{todonotes}


% The \icmltitle you define below is probably too long as a header.
% Therefore, a short form for the running title is supplied here:
\icmltitlerunning{Differentially Private Coordinate Descent for
  Composite Empirical Risk Minimization}

\input{preamble}
\begin{document}

\twocolumn[ \icmltitle{Differentially Private Coordinate Descent \\for
    Composite Empirical Risk Minimization}

  % It is OKAY to include author information, even for blind
  % submissions: the style file will automatically remove it for you
  % unless you've provided the [accepted] option to the ../../main
  % package.

  % List of affiliations: The first argument should be a (short)
  % identifier you will use later to specify author affiliations
  % Academic affiliations should list Department, University, City, Region, Country
  % Industry affiliations should list Company, City, Region, Country

  % You can specify symbols, otherwise they are numbered in order.
  % Ideally, you should not use this facility. Affiliations will be numbered
  % in order of appearance and this is the preferred way.
  \icmlsetsymbol{equal}{*}

  \begin{icmlauthorlist}
    \icmlauthor{Paul Mangold}{inria}
    \icmlauthor{Aurélien Bellet}{inria}
    \icmlauthor{Joseph Salmon}{montpellier,iuf}
    \icmlauthor{Marc Tommasi}{univlille}
  \end{icmlauthorlist}

  \icmlaffiliation{inria}{Univ. Lille, Inria,  CNRS, Centrale Lille, UMR 9189 - CRIStAL, F-59000 Lille, France}
  \icmlaffiliation{montpellier}{IMAG, Univ Montpellier, CNRS, Montpellier, France}
  \icmlaffiliation{iuf}{Institut Universitaire de France (IUF)}
  \icmlaffiliation{univlille}{Univ. Lille, CNRS, Inria, Centrale Lille,  UMR 9189 - CRIStAL, F-59000 Lille, France}

  \icmlcorrespondingauthor{Paul Mangold}{paul.mangold@inria.fr}

  % You may provide any keywords that you
  % find helpful for describing your paper; these are used to populate
  % the "keywords" metadata in the PDF but will not be shown in the document
  \icmlkeywords{Optimization, Privacy, Coordinate Descent, Machine Learning, ICML}

  \vskip 0.3in
]

% this must go after the closing bracket ] following \twocolumn[ ...

% This command actually creates the footnote in the first column
% listing the affiliations and the copyright notice.
% The command takes one argument, which is text to display at the start of the footnote.
% The \icmlEqualContribution command is standard text for equal contribution.
% Remove it (just {}) if you do not need this facility.

\printAffiliationsAndNotice{}  % leave blank if no need to mention equal contribution
%\printAffiliationsAndNotice{\icmlEqualContribution} % otherwise use the standard text.

\begin{abstract}
  Machine learning models can leak information about the data used to
  train them. To mitigate this issue, Differentially Private (DP) variants of
  optimization
  algorithms like Stochastic Gradient Descent (DP-SGD) have been
  designed to trade-off utility for privacy in Empirical Risk
  Minimization (ERM) problems. In this paper, we propose
  Differentially Private proximal Coordinate Descent (DP-CD), a new
  method to solve composite DP-ERM problems. We derive utility
  guarantees through a novel theoretical analysis of inexact
  coordinate descent. Our results show that, thanks to larger step
  sizes, DP-CD can exploit imbalance in gradient coordinates to
  outperform DP-SGD. We also prove new lower bounds for composite
  DP-ERM under coordinate-wise regularity assumptions, that are nearly
  matched by DP-CD. For practical implementations, we propose to clip
  gradients using coordinate-wise thresholds that emerge
  from our theory, avoiding costly hyperparameter tuning. Experiments
  on real and synthetic data support our results, and show that DP-CD
  compares favorably with DP-SGD.

  % Machine learning models can leak information about the data used to
  % train them. Differentially Private (DP) variants of optimization
  % algorithms like Stochastic Gradient Descent (DP-SGD) have been
  % designed to mitigate this, inducing a trade-off between privacy and
  % utility.  In this paper, we propose a new method for composite
  % Differentially Private Empirical Risk Minimization (DP-ERM):
  % Differentially Private proximal Coordinate Descent (DP-CD).  We
  % analyze its utility through a novel theoretical analysis of inexact
  % coordinate descent, and highlight some regimes where DP-CD
  % outperforms DP-SGD, thanks to the possibility of using larger step
  % sizes.  We also prove new lower bounds for composite DP-ERM under
  % coordinate-wise regularity assumptions, that are, in some settings,
  % nearly matched by our algorithm.In practical implementations, the
  % coordinate-wise nature of DP-CD updates demands special care in
  % choosing the clipping thresholds used to bound individual
  % contributions to the gradients.  A natural parameterization of these
  % thresholds emerges from our theory, limiting the addition of
  % unnecessarily large noise without requiring coordinate-wise
  % hyperparameter tuning or extra computational cost.
\end{abstract}

\input{intro}
\input{prelim}
\input{dp-cd}
\input{lower-bounds}
\input{practice}
\input{exp}
\input{related_work}
\input{conclu}

\section*{Acknowledgments}

The authors would like to thank the anonymous reviewers who provided
useful feedback on previous versions of this work, which helped to improve
the paper.

This work was supported in part by the Inria Exploratory Action FLAMED and
by the French National Research Agency (ANR) through grant ANR-20-CE23-0015
(Project PRIDE) and ANR-20-CHIA-0001-01 (Chaire IA CaMeLOt).


\bibliography{references}
\bibliographystyle{../../main}
\input{supplement}

\end{document}


% This document was modified from the file originally made available by
% Pat Langley and Andrea Danyluk for ICML-2K. This version was created
% by Iain Murray in 2018, and modified by Alexandre Bouchard in
% 2019 and 2021 and by Csaba Szepesvari, Gang Niu and Sivan Sabato in 2022.
% Previous contributors include Dan Roy, Lise Getoor and Tobias
% Scheffer, which was slightly modified from the 2010 version by
% Thorsten Joachims & Johannes Fuernkranz, slightly modified from the
% 2009 version by Kiri Wagstaff and Sam Roweis's 2008 version, which is
% slightly modified from Prasad Tadepalli's 2007 version which is a
% lightly changed version of the previous year's version by Andrew
% Moore, which was in turn edited from those of Kristian Kersting and
% Codrina Lauth. Alex Smola contributed to the algorithmic style files.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
