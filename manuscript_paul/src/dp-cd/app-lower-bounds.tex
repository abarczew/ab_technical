% !TEX root = ../../main.tex

\section{Proof of Lower Bounds}
\label{sec:utility-lower-bounds-1}

To prove lower bounds on the utility of $L$-coordinate-Lipschitz functions, we
extend the proof of \citet{bassily2014Private} to our setting
(that is, $L$-coordinate-Lipschitz functions and unconstrained
composite optimization).
There are three main difficulties in adapting their proof:
\begin{itemize}
\item First, the optimization problem stated in
  \Cref{eq:prelim:dp-erm-eq-chap-cd} is not constrained.  We stress
  that while convex constraints can be enforced using the regularizer
  $\psi$ (using the characteristic function of a convex set), its
  separable nature only allows box constraints. In contrast,
  \citet{bassily2014Private} rely on an $\ell_2$-norm constraint to
  obtain their lower bounds.
  \item Second, Lemma 5.1 of \citet{bassily2014Private} must be extended to
        our $L$-coordinate-Lipschitz setting.
        To do so, we consider datasets with points in $\prod_{j=1}^p \{-L_j, L_j\}$
        rather than $\{-1/\sqrt{p}, 1/\sqrt{p}\}^p$, and carefully adapt the
        construction of
        the dataset $D$ so that $\norm{\sum_{i=1}^n d_i}_2 = \Omega(\min(n\norm{L}_2, {\sqrt{p} \norm{L}_2}/{\epsilon}))$,
        which is essential to prove our lower bounds.
  \item Third, the lower bounds of \citet{bassily2014Private} rely on fingerprinting
        codes, and in particular on the result of \citet{bun2014Fingerprinting} which
        uses such codes to prove that (when $n$ is smaller than
        some $n^*$ we describe later) differential privacy is incompatible with
        precisely and simultaneously estimating \emph{all} $p$ counting
        queries defined over the columns of the dataset $D$.
        In our construction, since all columns of $D$ now have different scales, we
        need an additional
        hypothesis on the repartition of the $L_j$'s, (\ie that
        $\sum_{j\in\cJ} L_j^2 = \Omega(\norm{L}_2)$ for all $\cJ \subseteq [p]$ of a
        given size), which is not
        required in existing lower bounds (where all columns have equal scale).
\end{itemize}


% \subsection{Fingerprinting codes}
% \label{sec:fingerprinting-codes}

% \begin{definition}
%   For any $n, d \in \NN$, $\xi, \beta \in [0, 1]$, a pair of randomized
%   algorithms $(Gen, Trace)$ is a $(n, d)$-fingerprinting code with security
%   $\xi$ robust to a fraction $\beta$ of errors if $Gen$ outputs a codebook
%   $C \in \{0, 1\}^{n\times d}$ and for every (randomized) adversary $\cA_{FP}$,
%   and every coalition $S \subseteq [n]$, if we set $c' = \cA_{FP}(C_S)$, then
%   \begin{enumerate}
%   \item $\prob{(Trace(C, c') = \bot) \land (c' \in F_{\beta}(C_S))} \le \xi$,
%   \item $\prob{Trace(C, c') \in [n] \ S} \le \xi$,
%   \end{enumerate}
%   where the probability is taken over the coins of $Gen$, $Trace$ and $\cA_{FP}$.
%   The algorithms $Gen$ and $Trace$ may share a common state.
% \end{definition}

% \begin{theorem}
%   For every $n \in \NN$ and $\xi \in (0, 1]$, there exists an
%   $(n, d)$-fingerprinting code with security $\xi$ robust to a $1/75$ fraction
%   of errors for
%   \begin{align}
%     n = n(d, \xi) = \widetilde \Omega(\sqrt{d / \log(1/\xi)}).
%   \end{align}
% \end{theorem}


\subsection{Counting Queries and Accuracy}
\label{sec:accur-re-ident}

We start our proof by recalling and extending to our setting the notions of
counting queries (\Cref{def:counting-query}) and accuracy (\Cref{def:accuracy}),
as described by \citet{bun2014Fingerprinting}.
The main feature of our definitions is that we allow the set $\cX$ to have
different scales for each of its coordinates, and that we account for this scale
in the definition of accuracy.
We denote by $\conv(\cX)$ the convex hull of a set $\cX$.

\begin{definition}[Counting query]
  \label{def:counting-query}
  Let $n > 0$.
  A counting query on $\cX$ is a function $q : \cX^n \rightarrow \conv(\cX)$ defined
  using a predicate   $q : \cX \rightarrow \cX$.
  The evaluation of the query $q$ over a dataset $\cD \in \cX^n$ is defined as
  the arithmetic mean of $q$ on $\cD$:
  \begin{align}
    q(\cD) = \frac{1}{n} \sum_{i=1}^n q(d_i)\enspace.
  \end{align}
\end{definition}

\begin{definition}[Accuracy]
  \label{def:accuracy}
  Let $n, p \in \NN$, $\alpha, \beta \in [0, 1]$, $L_1, \dots, L_p > 0$,
  and $\cX = \prod_{j=1}^p \{-L_j; L_j\}$ or $\cX = \{0, L_j\}^p$.
  Let $\cQ = \{q_1, \dots, q_p\}$ be a set of $p$ counting queries on $\cX$ and
  $D \in \cX^n$ a dataset of $n$ elements.
  A sequence of answers $a = (a_1, \dots, a_p)$ is said $(\alpha, \beta)$-accurate
  for $\cQ$ if $\abs{q_j(D) - a_j} \le L_j\alpha$ for at least a
  $1 - \beta$ fraction of indices $j \in [p]$.
  A randomized algorithm $\cA : \cX^n \rightarrow \RR^{\card{\cQ}}$ is said
  $(\alpha, \beta)$-accurate for $\cQ$ on $\cX$ if for every $D \in \cX^n$,
  \begin{align}
    \prob{\cA(D) \text{ is } (\alpha, \beta)\text{-accurate for } \cQ} \ge 2/3\enspace.
  \end{align}
\end{definition}

In our proof, we will use a specific class of queries: one-way marginals
(\Cref{def:one-way-marginals}), that compute the arithmetic mean of a dataset
along one of its column.
\begin{definition}[One-way marginals]
  \label{def:one-way-marginals}
  Let $\cX = \prod_{j=1}^p \{-L_j; L_j\}$ or $\cX = \{0, L_j\}^p$.
  The family of one-way marginals on $\cX$ is defined by queries with predicates
  $q_j(x) = x_j$ for $x \in \cX$.
  For a dataset $D \in \cX^n$ of size $n$, we thus have
  $q_j(D) = \frac{1}{n} \sum_{i=1}^n d_{i,j}$.
\end{definition}



\subsection{Lower Bound for One-Way Marginals}
\label{sec:lower-bounds-1-1}


We can now restate a key result from \citet{bun2014Fingerprinting}, which shows
that there exists a minimal number $n^*$ of records needed in a dataset to allow
achieving both accuracy and privacy on the estimation of one-way marginals on
$\cX=(\{0, 1\}^p)^n$.
This lemma relies on the construction of re-identifiable distribution
(see \citealt[Definition~2.10]{bun2014Fingerprinting}). One can then use this
distribution to find a dataset on which a private algorithm can not be accurate
(see \citealt[Lemma~2.11]{bun2014Fingerprinting}).
\begin{lemma}[{\citealt[Corollary~3.6]{bun2014Fingerprinting}}]
  \label{lemma:bun-fingerprint-1way-marginal}
  For $\epsilon > 0$ and $p > 0$, there exists a number $n^* = \Omega(\frac{\sqrt{p}}{\epsilon})$
  such that for all $n \le n^*$, there exists no algorithm that is both
  $(1/3, 1/75)$-accurate and $(\epsilon, o\left(\frac{1}{n}\right))$-differentially
  private for the estimation of one-way marginals on $(\{0, 1\}^p)^n$.
  % the family of $1$-way marginals on $\{0, 1\}^p$
  % has sample complexity $\Omega(\frac{\sqrt{p}}{\epsilon})$ for
  % $(1/3, 1/75)$-accuracy and $(\epsilon, o\left(\frac{1}{n}\right))$-differential
  % privacy.
\end{lemma}

To leverage this result in our setting of private empirical risk minimization,
we start by extending it to queries on $\cX = \prod_{j=1}^p \{-L_j; L_j\}$.
Before stating the main theorem of this section (\Cref{thm:lower-bound-one-way-marginals}),
we describe a procedure $\chi_L : (\{0, 1\}^p)^n \rightarrow \cX^{3n}$
(with $L_1, \dots, L_p > 0$), that takes as input a dataset $D \in (\{0,
  1\}^p)^n$ and outputs an augmented and rescaled version. This procedure
is crucial to our proof and is defined as follows.
First, it adds $2n$ rows filled with $1$'s to $D$, which ensures that the sum
of each column of $D$ is $\Theta(n)$ (which gives the lower bound on $M$ in
\Cref{thm:lower-bound-one-way-marginals}).
Then it rescales each of these columns by subtracting $1/2$ to each coefficient
and multiplying the $j$-th column of $D$ ($j \in [p]$) by $2L_j$.
The resulting dataset $D_L^{aug} = \chi_L(D)$ is a set of $3n$ points with values
in $\cX = \prod_{j=1}^p \{-L_j, L_j\}$, with the property that, for all
$j \in [p]$, $3nL_j \ge \sum_{i=1}^n (D_L^{aug})_{i,j} \ge nL_j$.
For $D \in (\{0, 1\}^p)^n$, we show how to reconstruct $q_j(\chi_L(D))$
from $q_j(D)$
in \Cref{claim:qj-qjaug}.

\begin{lemma}
  \label{claim:qj-qjaug}
  Let $n \in \NN$, $j \in [p]$, $L_j > 0$ and $q_j$ the $j$-th one-way marginal
  on datasets $D$ with $p$ columns such that for $d_i \in D$, $q_j(d_i) = d_{i,j}$.
  Let $D_L^{aug} = \chi_L(D)$.
  It holds that
  \begin{align}
    q_j(D_L^{aug}) = \frac{2L_j}{3} q_j(D) + \frac{L_j}{3}\enspace,
  \end{align}
  where we use the slight abuse of notation by denoting the one-way marginals
  $q_j : \cX^{3n} \rightarrow \conv(\cX)$
  and $q_j : (\{0, 1\}^p)^n \rightarrow [0, 1]^p$ in the same way.
\end{lemma}

\begin{proof}
  Let $D \in (\{0, 1\}^p)^n$, and let $D^{aug} \in (\{0, 1\}^p)^{3n}$
  constructed by
  adding $2n$ rows of $1$'s at the end of $D$. Let $D_L^{aug} = \chi_L(D)$.
  We remark that
  \begin{align}
    \label{claim:qj-qjaug-eq1}
    q_j(D^{aug})
    & = \frac{1}{3n} \sum_{i=1}^{3n} D^{aug}_{i,j}
      \nnl
    & = \frac{1}{3} \left(\frac{1}{n} \sum_{i=1}^n D^{aug}_{i,j} \right)
      + \frac{1}{3n} \sum_{i=n+1}^{3n} 1
      \nnl
    & = \frac{1}{3} q_j(D) + \frac{2}{3} \in [0, 1]\enspace.
  \end{align}
  Then, we link $q_j(D^{aug})$ with $q_j(D^{aug}_L)$:
  \begin{align}
    \label{claim:qj-qjaug-eq2}
    q_j(D^{aug}_L)
    & = \frac{1}{3n} \sum_{i=1}^{3n} (D_L^{aug})_{i,j}
      \nnl
    & = \frac{1}{3n} \sum_{i=1}^{3n} 2L_j((D^{aug})_{i,j} - 1/2)
      \nnl
    & = 2L_j (q_j(D^{aug}) - 1/2) \in [-L_j, L_j]\enspace,
  \end{align}
  combining \eqref{claim:qj-qjaug-eq1} and \eqref{claim:qj-qjaug-eq2} gives the
  result.
\end{proof}

\begin{theorem}
  \label{thm:lower-bound-one-way-marginals}
  Let $n, p \in \NN$, and $L_1, \dots, L_p > 0$.
  Assume that for all subsets $\cJ \subseteq [p]$ of size at least $\lceil \frac{p}{75} \rceil$,
  $\sqrt{\sum_{j\in\cJ} L_j^2} = \Omega(\norm{L}_2)$.
  Define $\cX = \prod_{j=1}^p \{-L_j; +L_j\}$, and let $q_j : \cX \rightarrow \{-L_j, L_j\}$
  be the predicate of the $j$-th one-way marginal on $\cX$.
  Take $\epsilon > 0$ and $\delta = o(\frac{1}{n})$.
  There exists a number $M = \Omega\left(\min\left(n \norm{L}_2, \frac{\sqrt{p} \norm{L}_2}{\epsilon}\right)\right)$
  such that for every $(\epsilon,\delta)$-differentially private algorithm $\cA$,
  there exists a dataset $D = \{d_1, \dots, d_n\} \in \cX^n$ with $\norm{\sum_{i=1}^n d_i}_2 \in [M-1, M+1]$
  such that, with probability at least $1/3$ over the randomness of $\cA$:
  \begin{align}
    \norm{\cA(D) - q(D)}_2 = \Omega\left(\min\left(\norm{L}_2, \frac{\sqrt{p} \norm{L}_2}{n \epsilon}\right)\right)\enspace.
  \end{align}
\end{theorem}

\begin{proof}
  Let $M = \Omega\left(\min\left(n \norm{L}_2, \frac{\sqrt{p} \norm{L}_2}{\epsilon}\right)\right)$,
  and define the set of queries $\cQ$ composed of $p$ queries $q_j(D) = \frac{1}{n}\sum_{i=1}^n d_{i,j}$
  for $j \in [p]$.
  Let $\cA$ be a $(\epsilon, \delta)$-differentially-private randomized
  algorithm.
  Let $\alpha, \beta \in [0, 1]$.
  We will show that there exists a dataset $D$ such that
  $\norm{\sum_{i=1}^n d_i}_2 \in [M-1, M+1]$ for which $\cA(D)$ is not
  $(\alpha, \beta)$-accurate.



  \paragraph{When \boldmath$n \le n^*$.}
  Assume, for the sake of contradiction, that $\cA : \cX^{3n} \rightarrow \conv(\cX)$
  is $(\tfrac{1}{3}\alpha, \beta)$-accurate for $\cQ$.
  Then, for each dataset $D' \in \cX^{3n}$, we have
  \begin{align}
    \label{thm:lower-bound-one-way-marginals:accuracy-A}
    \prob\left( \exists \cJ \subseteq [p]~\text{with }\abs{\cJ}
      \ge (1 - \beta)p \text{ and }\forall j \in \cJ,~
      \abs{\cA_j(D') - q_j(D')} < \frac{2L_j}{3} \alpha \right)
    \ge 2/3\enspace.
  \end{align}
  Importantly, for all $D \in (\{0,1\})^p)^n$, the randomized algorithm $\cA$
  satisfies~\eqref{thm:lower-bound-one-way-marginals:accuracy-A} for the
  dataset $D_L^{aug} = \chi_L(D) \in \cX^{3n}$.
  We now construct the mechanism $\widetilde \cA : (\{0, 1\}^p)^n \rightarrow [0, 1]^p$
  that takes a dataset $D \in (\{0, 1\}^p)^n$, constructs $D_L^{aug} = \chi_L(D)$
  and runs $\cA$ on it.
  It then outputs $\widetilde \cA(D)$ such that, for $j \in [p]$,
  $\widetilde \cA_j(D) = \frac{3}{2L_j} \cA_j(D_L^{aug}) - \frac{L_j}{3}$.
  Using \Cref{claim:qj-qjaug}, the results of $\widetilde \cA$ and be linked to
  the ones of $\cA$, as
  \begin{align}
    \label{thm:lower-bound-one-way-marginals:rewrite-wdA}
    \abs{\widetilde \cA(D) - q_j(D)}
    & = \abs{\frac{3}{2L_j} \cA_j(D_L^{aug})- \frac{L_j}{3} - \frac{3}{2L_j} q_j(D_L^{aug}) + \frac{L_j}{3}}
      \nnl
    & = \frac{3}{2L_j} \abs{\cA_j(D_L^{aug}) - q_j(D_L^{aug})}\enspace.
  \end{align}

  Therefore, if $\cA$ satisfies~\eqref{thm:lower-bound-one-way-marginals:accuracy-A}
  and \eqref{thm:lower-bound-one-way-marginals:rewrite-wdA}, then
  $\widetilde \cA : (\{0, 1\}^p)^n \rightarrow [0, 1]^p$ satisfies,
  for all $D \in (\{0, 1\}^p)^n$,
  \begin{align}
    \label{thm:lower-bound-one-way-marginals:accuracy-widetildeA}
    \prob\left( \exists \cJ \subseteq [p]~\text{with }\abs{\cJ}
      \ge (1 - \beta)p \text{ and }\forall j \in \cJ,~
      \abs{\widetilde \cA_j(D) - q_j(D)} < \alpha \right)
    \ge 2/3\enspace,
  \end{align}
  which is exactly the definition of $(\alpha, \beta)$-accuracy for $\widetilde\cA$.
  Remark that since $\widetilde\cA$ is only a post-processing of $\cA$, without
  additional access to the dataset itself, $\widetilde\cA$ is itself
  $(\epsilon, \delta)$-differentially-private.
  We have thus constructed an algorithm that is both accurate and private for
  $n \le n^*$, which contradicts the result of
  \Cref{lemma:bun-fingerprint-1way-marginal} when $\beta = \frac{1}{75}$.
  This proves the existence of a dataset $D \in (\{0, 1\}^p)^n$ such that
  for $D_L^{aug} = \chi_L(D)$, $\cA(D_L^{aug})$ is not
  $(\tfrac{1}{3}\alpha, \beta)$-accurate on $\cQ$, which means that with
  probability at least $1/3$, there exists a subset $\cJ \subseteq [p]$ of
  cardinal $\card{\cJ} \ge \lceil \beta p \rceil$ such that
  \begin{align}
    \label{eq:thm:lower-bound-one-way-marginals:upper-bound-normL}
    \norm{\cA(D_L^{aug}) - q(D_L^{aug})}_2
    \overset{\eqref{thm:lower-bound-one-way-marginals:accuracy-A}}{\ge}
    \sqrt{\sum_{j \in \cJ} \frac{4L_j^2}{9}}
    \ge \Omega( \norm{L}_2 )\enspace,
  \end{align}
  where the second inequality comes from the fact that
  $\card{\cJ} \ge  \lceil \beta p \rceil = \lceil \frac{p}{75} \rceil$ and
  our hypothesis on $\sum_{j\in\cJ} L_j^2$.
  Notice that when $L_1 = \cdots = L_p = \frac{1}{\sqrt{p}}$, we recover the
  result of \citet{bassily2014Private}, since $\norm{L}_2 = 1$ it holds
  with probability at least $1/3$ that
  \begin{align}
    \norm{\cA(D_L^{aug}) - q(D_L^{aug})}_2
    \overset{\eqref{thm:lower-bound-one-way-marginals:accuracy-A}}{\ge}
    \sqrt{\sum_{j \in \cJ} \frac{4L_j^2}{9}}
    \ge \sqrt{\frac{4}{9 \times 75}} \norm{L}_2
    \ge \frac{2}{27}\enspace,
  \end{align}
  and in that case, since all $L_j$'s are equal, it indeed holds that
  $\sqrt{\sum_{j \in \cJ} L_j^2} = \Omega(\norm{L}_2)$.
  Finally, we remark that the sum of each column of $D_L^{aug}$ is
  $\sum_{i=1}^n d_{i,j} \ge n L_j$, and as such, we have
  $\norm{\sum_{i=1}^n d_i}_2 = \sqrt{\sum_{j=1}^p (\sum_{i=1}^n d_{i,j})^2}
    \ge \sqrt{\sum_{j=1}^p n^2 L_j^2} = n \norm{L}_2$.

  \paragraph{When \boldmath$n > n^*$.}
  We get the result in that case by augmenting the dataset $D^*$ that we
  constructed in the first part of this proof.
  To do so, we follow the steps described by \citet{bassily2014Private} in the
  proof of their Lemma 5.1.
  The construction consists in choosing a vector $c \in \cX$, and adding
  $\lceil \frac{n-n^*}{2} \rceil$ rows with $c$, and
  $\lfloor \frac{n-n^*}{2} \rfloor$ rows with $-c$ to the dataset $D^*$.
  This results in a dataset $D'$ such that
  $\norm{\sum_{i=1}^n d_i} = \Omega(n^* \norm{L}_2) = \Omega(\frac{\sqrt{p} \norm{L}_2}{\epsilon})$,
  since the contributions of rows $-c$ and $c$ (almost) cancel out.
  The theorem follows from observing that $(\frac{n^*}{n} \alpha,
    \beta)$-accuracy
  on this augmented dataset implies $(\alpha, \beta)$-accuracy on the original
  dataset.
  As such, if an algorithm is both private and $(\frac{n^*}{n} \alpha, \beta)$-accurate
  on the dataset $D'$, we get a contradiction, which gives the theorem as
  $\frac{n^*}{n} = \frac{\sqrt{p}}{n\epsilon}$.
\end{proof}

\begin{remark}
  \label{rmq:lower-bound-one-way-marginals-no-hyp-Lj}
  Without the assumption on the distribution of the $L_j$'s, we can still get
  an inequality that resembles~\eqref{eq:thm:lower-bound-one-way-marginals:upper-bound-normL}:
  $\norm{\cA(D_L^{aug}) - q(D_L^{aug})}_2
    \overset{\eqref{thm:lower-bound-one-way-marginals:accuracy-A}}{\ge}
    \sqrt{\sum_{j \in \cJ} \frac{4L_j^2}{9}}
    \ge \frac{2}{27} \frac{L_{\min}}{L_{\max}} \norm{L}_2$,
  with probability at least $1/3$, and we get a result similar to
  \Cref{thm:lower-bound-one-way-marginals}, except
  with an additional multiplicative factor $L_{\min}/L_{\max}$.
\end{remark}



\subsection{Lower Bound for Convex Functions}
\label{sec:convex-functions}

To prove a lower bound for our problem in the convex case, we let
$L_1, \cdots, L_p > 0$ and define a dataset $D = \{d_1, \dots, d_n\}$
taking its values in a set $\cX = \prod_{j=1}^p \{\pm L_j\}$.  For
$\beta > 0$, we consider the problem \eqref{eq:prelim:dp-erm-eq} with
$\cW = \RR^p$, the convex, smooth and $L$-coordinate-Lipschitz loss
function $\ell(w; d) = - \scalar{w}{d}$ and the convex, separable
regularizer
$\psi(w) = \frac{\norm{\sum_{i=1}^n d_i}_2}{\beta n} \norm{w}_2^2$:
\begin{align}
  \label{eq:dp-erm-lower-bound-convex}
  w^* = \argmin_{w\in\RR^p} \left\{
  F(w; D) = - \frac{1}{n}\scalar{w}{\textstyle{\sum_{i=1}^n d_i}}
  + \frac{\norm{\sum_{i=1}^n d_i}_2}{\beta n} \norm{w}_2^2 \right\}\enspace,
\end{align}
%where $M$ is such that $\norm{\sum_{i=1}^n d_i}_2 \in [M-1; M+1]$.
To find the solution of \eqref{eq:dp-erm-lower-bound-convex}, we look for $w^*$
so that the objective's gradient is zero, that is
\begin{align}
  \label{eq:dp-erm-lower-bound-convex-grad-obj-zero}
  w^* = \frac{\beta}{\norm{\sum_{i=1}^n d_i}_2} \sum_{i=1}^n d_i\enspace,
\end{align}
so that $\norm{w^*}_2 =  \frac{\beta}{\norm{\sum_{i=1}^n d_i}_2} \norm{\sum_{i=1}^n d_i}_2 = \beta$.
%We can now apply our DP-CD algorithm to problem \eqref{eq:dp-erm-lower-bound-convex}.
% which yields the same solution as \eqref{eq:dp-erm-lower-bound-convex}:
% \begin{align}
%   \label{eq:dp-erm-lower-bound-convex-unconstrained}
%   \min_{w\in\RR^p}  F(w;D) = - \frac{1}{n} \scalar{w}{\textstyle{\sum_{i=1}^n d_i}}
%   + \frac{ \norm{ \textstyle{\sum_{i=1}^n d_i}}}{2\beta n}
%   \left( \norm{w}_2^2 - \norm{w^*}_2^2 \right).
% \end{align}
To prove the lower bound, we remark that
\begin{align}
  F(w; D) - F(w^*; D)
  & = - \frac{1}{n} \scalar{w - w^*}{\textstyle{\sum_{i=1}^n d_i}}
  + \frac{\norm{\textstyle{\sum_{i=1}^n d_i}}}{2 \beta n} (\norm{w}^2_2 - \norm{w^*}_2^2)      \nnl
   & = - \frac{1}{n} \scalar{w - w^*}{\frac{\norm{\textstyle{\sum_{i=1}^n d_i}}}{\beta} w^*}
  + \frac{\norm{\textstyle{\sum_{i=1}^n d_i}}}{2\beta n} (\norm{w}^2_2 - \norm{w^*}_2^2)       \nnl
   & = \frac{\norm{\textstyle{\textstyle{\sum_{i=1}^n d_i}}}}{\beta n}
  \left( \scalar{w^* - w}{w^*} + \frac{1}{2} \norm{w}_2^2 - \frac{1}{2} \norm{w^*}_2^2 \right) \nnl
   & = \frac{\norm{\textstyle{\textstyle{\sum_{i=1}^n d_i}}}}{\beta n}
  \left( - \scalar{w}{w^*} + \frac{1}{2} \norm{w}_2^2 + \frac{1}{2} \norm{w^*}_2^2 \right)     \nnl
   & = \frac{\norm{\textstyle{\textstyle{\sum_{i=1}^n d_i}}}}{2 \beta n}
  \norm{w - w^*}_2^2\enspace.
\end{align}

At this point, we can proceed similarly to \citet{bassily2014Private}
to relate this quantity to private estimation of one-way marginals.
We let $M = \Omega(\min(n\norm{L}_2, \norm{L}_2\sqrt{p}/\epsilon))$
and $\cA$ be an $(\epsilon,\delta)$-differentially private mechanism
that outputs a private solution $w^{priv}$ to
\eqref{eq:dp-erm-lower-bound-convex}.  Suppose, for the sake of
contradiction, that for every dataset $D$ with
$\norm{\textstyle{\sum_{i=1}^n d_i}}_2 \in [M-1; M+1]$, it holds with
probability at least $2/3$ that
\begin{align}
  \label{lower-bound-convex-function-inequality-beta}
  \norm{w^{priv} - w^*} \neq \Omega(\beta)\enspace.
\end{align}

We now derive from $\cA$ a mechanism $\widetilde \cA$ to estimate one-way
marginals. To do this, $\widetilde \cA$ runs $\cA$ to obtain $w^{priv}$ and
outputs $\frac{M}{n\beta} w^{priv}$.
We obtain that with probability at least $2/3$,
\begin{align}
  \label{lower-bound-convex-function-inequality-contradiction-with-beta}
  \norm{\widetilde \cA(D) - q(D)}_2
  & = \frac{M}{n\beta} \norm{w^{priv} - \frac{\beta}{M} \textstyle{\sum_{i=1}^n d_i}}_2
    \nnl
  &  \not= \Omega\left(\frac{M}{n}\right)
   = \Omega\left(\min\left(\norm{L}_2, \frac{\norm{L}_2\sqrt{p}}{n\epsilon}\right)\right)\enspace.
\end{align}
where $q(D) = \frac{1}{n} \sum_{i=1}^n d_i$.
This is in contradiction with \Cref{thm:lower-bound-one-way-marginals}.
We thus proved that $\norm{w^{priv} - w^*} = \Omega(\beta)$, with probability at
least $1/3$.
As a consequence, we now obtain that with probability at least $1/3$,
\begin{align}
  F(w^{priv}; D) - F(w^*; D)
  & = \frac{\norm{\textstyle{\textstyle{\sum_{i=1}^n d_i}}}}{2 \beta n} \norm{w^{priv} - w^*}_2^2
    \nnl
  & = \Omega\left(\min\left(\norm{L}_2\beta, \frac{\beta\norm{L}_2\sqrt{p}}
    {n\epsilon}\right)\right)\enspace,
\end{align}
which gives the desired result on the expectation of $F(w^{priv}; D) - F(w^*; D)$.

Finally, if we do not make any hypothesis on the $L_j$'s distribution, we
can directly use
the non-augmented dataset constructed by \citet{bun2014Fingerprinting} to prove
\Cref{lemma:bun-fingerprint-1way-marginal} (that is the dataset from
\Cref{thm:lower-bound-one-way-marginals}, rescaled but not augmented).
The $\ell_2$-norm of the sum of this dataset is
$\norm{\sum_{i=1}^n d_j}_2 = [M' - 1, M' + 1]$ with
$M' = \Omega\left(\min\left(\frac{L_{\min}}{L_{\max}} n \norm{L}_2, \frac{L_{\min}}{L_{\max}} \frac{\sqrt{p}\norm{L}_2}{\epsilon} \right)\right)$.
This holds since four columns of this dataset out of five have sum of
$\pm n L_j$ (for some $j$'s), but no lower bound on the sum of the remaining
columns can be derived.
Thus, assuming~\eqref{lower-bound-convex-function-inequality-beta} holds,
then~\eqref{lower-bound-convex-function-inequality-contradiction-with-beta} can
be rewritten as
\begin{align}
  \norm{\widetilde \cA(D) - q(D)}_2
  & = \frac{M'}{n\beta} \norm{w^{priv} - \frac{\beta}{M} \textstyle{\sum_{i=1}^n d_i}}_2
    \nnl
  & \not= \Omega\left(\frac{M'}{n}\right)
  = \Omega\left(\min\left(\frac{L_{\min}}{L_{\max}} \norm{L}_2, \frac{L_{\min}}{L_{\max}} \frac{\norm{L}_2\sqrt{p}}{n\epsilon}\right)\right)\enspace,
\end{align}
with probability at least $1/3$, which is in contradiction with
Remark~\ref{rmq:lower-bound-one-way-marginals-no-hyp-Lj}.
We thus get an additional factor of $L_{\min}/L_{\max}$ in the lower bound:
\begin{align}
  F(w^{priv}; D) - F(w^*; D)
  & = \frac{\norm{\textstyle{\textstyle{\sum_{i=1}^n d_i}}}}{2 \beta n} \norm{w^{priv} - w^*}_2^2
    \nnl
  &= \Omega\left(\min\left(\frac{L_{\min}}{L_{\max}}\norm{L}_2\beta, \frac{L_{\min}}{L_{\max}}\frac{\beta\norm{L}_2\sqrt{p}}{n\epsilon}\right)\right)\enspace.
\end{align}
%probability at least $2/3$, then $\norm{\widetilde \cA(D) - q(D)}_2 \neq \frac{L_{\min}}{L_{\max}} \Omega(\frac{M}{n})$.

\subsection{Lower Bound for Strongly-Convex Functions}
\label{sec:strongly-conv-funct}

To prove a lower bound for strongly-convex functions, we let $\mu_I > 0$,
$L_1, \dots, L_p > 0$, $\cW = \prod_{j=1}^p [-\frac{L_j}{2\mu_I}, +\frac{L_j}{2\mu_I}]$ and
$D = \{ d_1, \dots, d_n \} \in \prod_{j=1}^p \{\pm \frac{L_j}{2\mu_I} \}$.
We consider the following problem, which fits in our setting:
\begin{align}
  \label{eq:dp-erm-lower-bound-strongly-convex}
  w^* = \argmin_{w\in\RR^p} \left\{
  F(w;D) = \frac{\mu_I}{2n} \sum_{i=1}^n \norm{w - d_i}_2^2
  + i_{\cW}(w) \right\}\enspace.
\end{align}
where $i_{\cW}$ is the (separable) characteristic function of the set
$\cW$. Since $\psi = i_{\cW}$ is the characteristic function of a
box-set, the proximal operator is equal to the projection on $\cW$ and
DP-CD iterates are thus guaranteed to remain in $\cW$. Therefore,
regularity assumptions on $f$ only need to hold on $\cW$.  The loss
function $\ell(w;d_i)=\frac{\mu_I}{2} \norm{w - d_i}_2^2$ is
$L$-coordinate-Lipschitz on $\cW$ since, for $w \in \cW$ and
$j \in [p]$, the triangle inequality gives:
\begin{align}
  \abs{\nabla_j \ell(w;d_i)}
  \le \mu_I (\abs{w_j} + \abs{d_{i,j}})
  \le \mu_I \left(\frac{L_j}{2\mu_I} + \frac{L_j}{2\mu_I}\right)
  \le L_j\enspace.
\end{align}
This loss is also $\mu_I$-strongly convex \wrt $\ell_2$-norm since
for $w, w' \in \cW$,
\begin{align}
  \ell(w; d_i)
  & = \frac{\mu_I}{2} \norm{w - d_i}_2^2
    \nnl
  & = \frac{\mu_I}{2} \norm{w' - d_i + w - w'}_2^2
    \nnl
  & = \frac{\mu_I}{2} \left(\norm{w' - d_i}_2^2 + 2\scalar{w' - d_i}{w - w'} + \norm{w - w'}_2^2\right)\enspace,
\end{align}
which is exactly $\mu_I$-strong convexity since
$\ell(w';d_i) = \frac{\mu_I}{2} \norm{w' - d_i}_2^2$ and
$\nabla \ell(w'; d_i) = \mu_I(w - d_i)$.
The minimum of the objective function in~\eqref
{eq:dp-erm-lower-bound-strongly-convex} is attained
at
\begin{align*}
  w^* = \frac{1}{n} \sum_{i=1}^n d_i = q(D) \in \cW\enspace.
\end{align*}
The excess risk of $F$ is thus
\begin{align}
  F(w; D) - F(w^*)
   & = \frac{\mu_I}{2n} \sum_{i=1}^n \norm{w - d_i}_2^2 - \norm{w^* - d_i}_2^2           \\
   & = \frac{\mu_I}{2n} \sum_{i=1}^n \norm{w}^2 - \norm{w^*}^2 + 2 \scalar{d_i}{w^* - w} \\
   & = \frac{\mu_I}{2}\norm{w}^2 - \frac{1}{2}\norm{w^*}^2 + \scalar{w^*}{w^* - w}       \\
   & = \frac{\mu_I}{2} \norm{w - q(D)}_2^2\enspace.
\end{align}

It remains to apply \Cref{thm:lower-bound-one-way-marginals} to obtain that, with
probability at least $1/3$,
\begin{align}
  F(w^{priv}; D) - F(w^*)
   & = \Omega\left( \min\left(\frac{\norm{L}_2^2}{\mu_I}, \frac{\norm{L}_2^2 p}{\mu_I n^2\epsilon^2}\right)\right)\enspace,
\end{align}
which gives the lower bound on the expected value of $F(w^{priv}; D) - F(w^*)$.
Note that without the additional assumption on the distribution of the $L_j$'s,
Remark~\ref{rmq:lower-bound-one-way-marginals-no-hyp-Lj} directly gives the
result with an additional multiplicative factor $(L_{\min} / L_{\max})^2$:
\begin{align}
  F(w^{priv}; D) - F(w^*)
   & = \Omega\left( \min\left(\frac{L_{\min}^2}{L_{\max}^2}\frac{\norm{L}_2^2}{\mu_I},
    \frac{L_{\min}^2}{L_{\max}^2}\frac{\norm{L}_2^2 p}{\mu_I n^2\epsilon^2}\right)\right)\enspace,
\end{align}
with probability at least $1/3$.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../main"
%%% End:
