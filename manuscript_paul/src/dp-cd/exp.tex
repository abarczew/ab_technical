% !TEX root = ../../main.tex

%______________________________________________________________________________
%______________________________________________________________________________
%
% EXPERIMENTS
%______________________________________________________________________________
%______________________________________________________________________________

\begin{figure}[t]
%  \captionsetup[subfigure]{justification=centering}
  \centering
  \begin{subfigure}{0.05\linewidth}
    \centering
    \includegraphics[width=\linewidth]{plots/xlegend.pdf}
    \begin{minipage}{.1cm}
      \vfill
    \end{minipage}
  \end{subfigure}%
  \begin{subfigure}{0.3\linewidth}
    \centering
    \includegraphics[width=\linewidth]{plots/optimization_electricity_raw.pdf}
    \caption{Electricity\\ Logistic, $\lambda=1/n$.}
    \label{fig:expe-logreg-constant}
  \end{subfigure}%
  \begin{subfigure}{0.3\linewidth}
    \centering
    \includegraphics[width=\linewidth]{plots/optimization_california_raw.pdf}
    \caption{California\\ LASSO, $\lambda=1.0$.}
    \label{fig:expe-logreg-power-law-sparse}
  \end{subfigure}

  \caption{ Relative error to non-private optimal for \DPCD (blue), \DPCD with
  privately estimated coordinate-wise smoothness
    constants (green), \DPSGD (orange) and \DPSCD (red, only applicable to the
    smooth case) on
    two \emph{imbalanced} problems. The number of passes is
    tuned separately for each algorithm to achieve lowest error. We report
    min/mean/max values over 10~runs.}
  \label{fig:expe-raw}
\end{figure}

\section{Numerical Experiments}
\label{sec:numerical-experiments}

%\aurelien{keep only \DPSGD batch 1 (and remove batch 1 from the name)}

In this section, we assess the practical performance of \DPCD against
(proximal) \DPSGD on LASSO\footnote{\ie
  $\ell(w, (x,y)) = (w^\top x - y)^2$, $\psi(w) = \lambda\norm{w}_1$.}
and $\ell_2$-regularized logistic regression\footnote{\ie
  $\ell(w, (x, y)) = \log(1 + \exp(-y w^\top x))$,
  $\psi(w) = \!\frac{\lambda}{2}\!\norm{w}_2^2$.}. On the latter
problem, we also consider the dual private coordinate
descent algorithm of \citet{damaskinos2021Differentially}
(\DPSCD). For LASSO, we use the California dataset
\citep{kelleypace1997Sparse}, with $n=20,640$ records and $p=8$
features as well as a synthetic dataset (coined ``Sparse LASSO'') with
$n=1,000$ records and $p=1,000$ independent features that follow a
standard normal distribution.  The labels are then computed as a noisy
sparse linear combination of a subset of $10$ active features.  For
logistic regression, we consider the Electricity dataset
\citep{Electricity} with $45,312$ records and $8$ features.  On
California and Electricity, we set $\epsilon=1$ and $\delta=1/n^2$,
which is generally seen as a rather high privacy regime. The Sparse
LASSO dataset corresponds to a challenging setting for privacy
($n=p$), so we consider a low privacy regime with $\epsilon=10$,
$\delta=1/n^2$.  Privacy accounting for \DPSGD is done by numerically
evaluating the Rényi DP formula given by the sampled Gaussian
mechanism \citep{mironov2019Renyi}. Similarly for \DPCD, we do not use
the closed-form formula of Theorem~\ref{thm:dp-cd-privacy} but rather
numerically evaluate the tighter Rényi DP formula given in
Appendix~\ref{sec:proof-privacy}.

For \DPSGD, we use constant step sizes and standard gradient
clipping. For \DPCD, we adapt the coordinate-wise clipping thresholds
from one hyperparameter, as described in
\Cref{sub:clipping}. Similarly, coordinate-wise step sizes are set to
$\gamma_j = \gamma / M_j$, where $\gamma$ is a hyperparameter. When
the coordinate-wise smoothness constants are not all equal, we also
consider \DPCD with privately computed $M_j$'s, as described in
\Cref{sec:priv-smoothn}.  For each dataset and each algorithm, we
simultaneously tune the clipping threshold, the number of passes over
the dataset and, for \DPCD and \DPSGD, the step sizes.  After tuning
these parameters, we report the relative error to the (non-private)
optimal objective value.  The complete tuning procedure is described
in \Cref{sec:hyperp-tuning}, where we also give the best error for
various numbers of passes for each algorithm and dataset.  The code
used to obtain all our results is available in a public repository\footnote{
\url{https://gitlab.inria.fr/pmangold1/private-coordinate-descent/}}
and in the supplementary material.
% \aurelien{explain hyperparam of SGD, merge in one place, say explicitly that
% the number of hyperparam is the same for both.}

%\aurelien{subsections: use balance/imbalanced rather than raw/standardized}
\subsection{Imbalanced Datasets}
\label{sec:raw-datasets}

% \aurelien{Regarding Fig 1-2: it looks like you could fit the 3 plots on one
% line if you put only only one "Relative Error.." on the left. also,
% xlabel/ylabel text are quite huge compared to the rest (could be reduced),
% while the legend is quite small (could be increased)}

In the Electricity and California datasets, features are naturally
imbalanced. \DPCD can exploit this through the use of coordinate-wise
smoothness constants. We also consider a variant of \DPCD
(\texttt{DP-CD-P}) which dedicates $10\%$ of the privacy
budget~$\epsilon$ to estimate these constants (see
\Cref{sec:priv-smoothn}) from a crude upper bound on each feature
(twice their maximal absolute value). It then uses the resulting
private smoothness constants in step sizes and clipping
thresholds. \Cref{fig:expe-raw} shows that \DPCD outperforms \DPSGD
and \DPSCD by an order of magnitude on both datasets, even when the
smoothness constants are estimated privately.



\subsection{Balanced Datasets}
\label{sec:stand-datas}

To assess the performance of \DPCD when coordinate-wise smoothness
constants are balanced, we standardize the Electricity and California
datasets (see Section~\ref{sec:data-standardization}).  As
standardization is done for all algorithms, we do not account for it
in the privacy budget. On standardized datasets, coordinate-wise
smoothness constants are all equal, removing the need of estimating
them privately. We report the results in \Cref{fig:expe-standardized}.
Although our theory suggests that \DPCD may do worse than \DPSGD in
balanced regimes, we observe that it still improves over \DPSGD (and
\DPSCD) in practice. Similar observations hold in our challenging
Sparse LASSO problem, where \DPSGD is barely able to make any
progress. We believe these results are in part due to the beneficial
effect of clipping in \DPCD, and the fact that \DPSGD relies on
amplification by subsampling, for which privacy accounting is not
perfectly tight. Additionally, CD methods are known to perform well on
fitting linear models: our results show that this transfers well to
private optimization.

\begin{figure*}[t]
  \captionsetup[subfigure]{justification=centering}
  \centering
  \begin{subfigure}{0.05\linewidth}
    \centering
    \includegraphics[width=\linewidth]{plots/xlegend.pdf}
    \begin{minipage}{.1cm}
      \vfill
    \end{minipage}
  \end{subfigure}%
  \begin{subfigure}{0.3\linewidth}
    \centering
    \includegraphics[width=\linewidth]{plots/optimization_electricity_norm.pdf}
    \caption{Electricity\\ Logistic, $\lambda=1/n$.}
    \label{subfig:expe-logreg-constant}
  \end{subfigure}%
  \begin{subfigure}{0.3\linewidth}
    \centering
    \includegraphics[width=\linewidth]{plots/optimization_california_norm.pdf}
    \caption{California\\ LASSO, $\lambda=0.1$.}
    \label{subfig:expe-logreg-power-law}
  \end{subfigure}
  \begin{subfigure}{0.3\linewidth}
    \centering
    \includegraphics[width=\linewidth]{plots/optimization_lasso.pdf}
    \caption{Sparse\\ LASSO, $\lambda=30$.}
    \label{fig:expe-logreg-power-law}
  \end{subfigure}

  \caption{
    Relative error to non-private optimal for  \DPCD (blue), \DPSGD (orange)
    and \DPSCD (red, only applicable to the smooth case) on three
    \emph{balanced} problems. The number of passes is tuned separately for
    each algorithm to achieve lowest error. We report min/mean/max values over
    10~runs.
  }
  \label{fig:expe-standardized}
\end{figure*}

\subsection{Running Time}
\label{sec:running-time}

The results above showed that \DPCD yields better
utility than \DPSGD. We also observe that \DPCD tends to reach
these
results in up to $10$~times fewer passes on the data than \DPSGD (see
\Cref{sec:hyperp-tuning} for detailed results). Additionally, when accounting
for running time, \DPCD
significantly outperforms \DPSGD: we refer to \Cref{sec:running-time-in} for
the counterparts of \Cref{fig:expe-raw} and \ref{fig:expe-standardized} as a
function of the running time instead of the number of passes.
% In~\Cref{sec:hyperp-tuning}, we report the best relative
% suboptimality gap with different total passes on the data for each of
% the above algorithms and problems. This confirms that \DPCD converges
% not only better but also faster than \DPSGD on the raw Electricity and
% California datasets. On the standardized California dataset, \DPCD
% still requires around $10$ times less iterations than the two variants
% of \DPSGD, while remaining competitive on the standardized Electricity
% dataset. We also report the absolute running time of these algorithms
% in~\Cref{sec:running-time-in}, to show that in our implementation,
% \DPCD tends to run significantly faster than \DPSGD.

% In this section, we illustrate the practical performance of our \DPCD
% algorithm by comparing it with \DPSGD (with minibatches of $10$
% records) on LASSO \citep{tibshirani1996Regression} and
% $\ell_2$-regularized logistic regression.  To ensure privacy, both
% algorithms use the Gaussian mechanism with appropriate noise, as
% described in \Cref{sec:privacy-guarantees}.  For \DPSGD, we also use
% amplification for the sampled Gaussian mechanism
% \citep{mironov2019Enyi}.
% % , as done by \citet{bassily2014Private}.  \aurelien{But Bassily does
% % not keep track of privacy loss with zCDP.}  \paul{maybe in this one
% % \cite{balle2020Privacy}?}
% The privacy budget is fixed to $\epsilon = 1$, $\delta = 1/n^2$, which
% is generally regarded as providing good privacy guarantees.
% % \aurelien{maybe mention once and for all that SGD uses batch 10 in
% % all experiments and remove from figs.}  \aurelien{we probably need a
% % few words to explain how privacy accounting is done for SGD. You
% % probably use amp by subsampling. Do you also keep track of
% % everything through zCDP as for \DPCD?}  We note that our
% % implementations slightly depart from theory, as we report the loss
% % for each iterate without averaging.\jo{here you have to state why
% % you did that (you don't win much not averagig hopefully). Otherwise
% % a reader could feel like ``hum... there is a catch here...''}
% We report the loss for each iterate without periodic averaging, as we
% observe that this performs slightly better empirically.
% Hyperparameters are tuned by grid search, choosing the values that
% yield the lowest average loss on the training set.  As we aim to
% compare both algorithms at their best, we do not account for tuning in
% our privacy budget.  We also stress the fact that while smoothness
% constants could leak information, they can be estimated using global,
% non-confidential bounds/statistics on the data features.
% % Experiments are run on a computer running Debian 11, with 16GB RAM
% % and Intel Core i7-10610U CPU.
% Algorithms are implemented in C++, with Python bindings\footnote{We
%   provide the code as supplementary material.}.%
% % differentially private way,
% % To ensure strict differential privacy guarantees, one should dedicate a
% % part of the privacy budget to hyperparameters tuning \paul{ref on this} and
% % computation of private estimates of smoothness constants.

% \begin{figure*}[t]
%   \captionsetup[subfigure]{justification=centering}
%   \centering
%   \begin{subfigure}[b]{0.22\textwidth}
%     \centering
% %    \includegraphics[width=\textwidth]{plots/reg_balanced.pdf}
%     \includegraphics[width=\textwidth]{plots/lasso_balanced.pdf}
%     \includegraphics[width=\textwidth]{plots/smoothness_regression_balanced.pdf}
%     \caption{LASSO, $\lambda = 50$, balanced smoothness.}
%     \label{fig:expe-lasso-constant}
%   \end{subfigure}
%   \hfill
%   \begin{subfigure}[b]{0.22\textwidth}
%     \centering
% %    \includegraphics[width=\textwidth]{plots/reg_lognormal.pdf}
%     \includegraphics[width=\textwidth]{plots/lasso_lognormal.pdf}
%     \includegraphics[width=\textwidth]{plots/smoothness_regression_lognormal.pdf}
%     \caption{LASSO, $\lambda = 100$, unbalanced smoothness.}
%     \label{fig:expe-lasso-power-law}
%   \end{subfigure}
%   \hfill
%   \begin{subfigure}[b]{0.22\textwidth}
%     \centering
%     \includegraphics[width=\textwidth]{plots/logl2_balanced.pdf}
%     \includegraphics[width=\textwidth]{plots/smoothness_classif_balanced.pdf}
%     \caption{Logistic with $\ell_2$-reg, balanced smoothness.}
%     \label{fig:expe-logreg-constant}
%   \end{subfigure}
%   \hfill
%   \begin{subfigure}[b]{0.22\textwidth}
%     \centering
%     \includegraphics[width=\textwidth]{plots/logl2_lognormal.pdf}
%     \includegraphics[width=\textwidth]{plots/smoothness_classif_lognormal.pdf}
%     \caption{Logistic with $\ell_2$-reg, unbalanced smoothness.}
%     \label{fig:expe-logreg-power-law}
%   \end{subfigure}


%   \caption{ Comparison of \DPCD and \DPSGD on different problems and
%     datasets.  The top row reports the evolution of the relative error
%     (with respect to the optimal non-private loss) across iterations,
%     with average, minimum and maximum values over 10 runs. The bottom
%     row reports coordinate-wise smoothness constants.  }
%   \label{fig:expe-lasso}
% \end{figure*}

% \paragraph{LASSO.}

% Let $\cX = \RR^p\times \RR$,
% $\ell(w; (x,y)) = \frac{1}{2} (x^\top w - y)^2$ and
% $\psi = \lambda \norm{w}_1 = \lambda \sum_{j=1}^p \abs{w_j}$, where
% $\lambda$ is high enough to enforce sparsity of the optimal solution
% (values of $\lambda$ are given in \Cref{fig:expe-lasso}).
% % \aurelien{value of $\lambda?$ is it $1/n$ as in logistic case?}
% For \DPSGD, we use standard gradient clipping, as described in
% \eqref{eq:standard_clipping_rule}.  For \DPCD we use our
% coordinate-wise rule described in \Cref{sub:clipping} with
% $C_j = \sqrt{M_j / \tr(M)}C$ (referred to as \CDsmooth), as well as
% the uniform clipping rule $C_j = \frac{C}{\sqrt{p}}$ (\CDunif), with
% $C > 0$ to be tuned.  Each algorithm is run for $30$ passes over the
% full dataset and the experiment is repeated over $10$ different random
% seeds.
% % As described above, we tune the learning rate and clipping parameter
% % $C$ by grid search.  \aurelien{how? is the metric the loss on
% % separate validation set? or on the training set?}
% We report the mean, minimum and maximum of the relative error to the
% optimal (non-private) solution over the $10$ random runs with chosen
% hyperparameters.
% % \paul{put correct repo} \aurelien{this should be anonymized. at
% % submission time it is easier to submit the code as part of the
% % supplementary}
% % \url{https://gitlab.inria.fr/magnet/thesepaulmangold/pcoptim}.

% % The next two sections show the results of the experiments we
% % conducted for linear regression (without regularization and with
% % $\ell_1$ regularization) and logistic regression (with $\ell_2$
% % regularization).  Regularization parameters, when relevant, are set
% % to the standard value of $\frac{1}{n}$, since the effect of this
% % parameter does not appear to be essential in the behaviour of these
% % algorithms. \marc{un peu contre intuitif si CD marche mieux que GD
% % quand on a de forts écarts de variation entre composantes}

% % \aurelien{missing a lot of information about the setup: hyperparam
% % tuning, privacy budget, etc}

% We generate two datasets of $n=10,000$ records with $p=100$ features
% independently drawn from a standard Gaussian
% distribution. % \jo{so the features are with correlation=0
% % I guess?}
% We then generate noisy labels from a linear predictor perturbed with small
% Gaussian noise.
% In the first dataset, we keep the features balanced, so that the loss has
% balanced smoothness constants.
% In the second one, we rescale features so that smoothness constants
% follow a lognormal distribution, which we expect to resemble
% real-world datasets \citep{chmiel2020Neural} while remaining close to the case
% discussed in \Cref{sec:comparison-with-dp-sgd}.
% % \paul{i explained that this setting resembles the one of 3.4, while being closer
% %   to reality. Choice of loss is not too important, as long as smoothness constants
% %   are similar, but regularization has an impact on the term
% %   $R_M = \max(\sqrt{F(w^0) - F(w^*)}, \norm{w^0 - w^*}_M)$: $F(w^0) - F(w^*)$ does
% %   not change too much when changing $M$ if regularization is important, and this
% %   is essentially the reason we have this $\max$ instead of just $\norm{w^0 - w^*}_M$
% %   when adding regularization.}
% % \aurelien{is the relation with the conditioning discussed in Sec
% %   3.4 direct? how does it depend on the choice of loss?}
% \Cref{fig:expe-lasso-constant} shows results on the first
% dataset. Here, the two clipping rules for \DPCD are equivalent and
% perform slightly worse than \DPSGD.
% % Results for the second dataset are in
% % \Cref{fig:expe-lasso-power-law}.
% \Cref{fig:expe-lasso-power-law} reports results on the second dataset:
% there, \DPCD with uniform clipping suffers from its large learning
% rates, as these also amplify the final amount of noise.  Remarkably,
% our clipping rule from \Cref{sub:clipping} avoids this pitfall and
% improves over \DPSGD.

% % \marc{Dans les figures, il y a un point par passe sur les données
% % pour GD mais des points intermédiaires pour CD ce qui donne une
% % impression plus lisse pour GD. Je trouve qu'on pourrait uniformiser}

% % In the first one, they are constant and equal to one, in the second
% % one we draw them from a power law, and for the last one, one
% % constant is set to a substantially larger value than the others.

% % \aurelien{in figs, difficult to see where the different curves are
% % when they overlap} \aurelien{shoudn't the power law be in the other
% % direction: most things are small but a few ones can be big?}
% % \aurelien{why is GD still beating us in imbalanced setting?}
% % \paul{it was l2 regularized linear regression, it is better now!}

% % \paul{on the figure: legends are too small:

% % - should i remove legends in the figure and say ``blue = blabla,
% % oragne = blabla'' in the caption ?

% % - is there a trick to avoid size of labels to do weird stuff like
% % this?

% % - figures are super heavy-weight file. should i keep the ribbon
% % around the mean curves?}  \aurelien{for figures, you can increase
% % the overall textsize}


% \paragraph{Logistic regression.}

% We now set $\ell(w; (x,y)) = \log(1 + \exp(-y x^\top w))$ and
% $\psi = \lambda \norm{w}_2^2$.  The design matrix $X$ is generated as
% in the LASSO case.
% % , with $n = 10000$ records and $p=100$ features from centred unit
% % Gaussian distribution.
% Labels $y$ are computed from a linear predictor, and assigned to
% $\pm 1$ depending on a fixed threshold.  They are then independently
% flipped with probability $0.2$.  We set $\lambda$ to $\frac{1}{n}$,
% following classical machine learning guidelines.  Results are shown in
% \Cref{fig:expe-logreg-constant,fig:expe-logreg-power-law}, and are
% similar to the ones of the LASSO: in the balanced setting, the two
% clipping rules for \DPCD are equivalent, and a little worse than
% \DPSGD.  In the unbalanced setting, choosing the appropriate clipping
% rule improves convergence, and \DPCD largely outperforms \DPSGD.

% % \begin{figure}
% %   \captionsetup[subfigure]{justification=centering}
% %   \centering
% %   \begin{subfigure}[b]{0.22\textwidth}
% %     \centering
% %     \includegraphics[width=\textwidth]{plots/logl2_balanced.pdf}
% %     \includegraphics[width=\textwidth]{plots/smoothness_classif_balanced.pdf}
% %     \caption{Balanced smooth. constants, $\lambda = \frac{1}{n}$.}
% %     \label{fig:expe-logreg-constant}
% %   \end{subfigure}
% %   \hfill
% %   \begin{subfigure}[b]{0.22\textwidth}
% %     \centering
% %     \includegraphics[width=\textwidth]{plots/logl2_lognormal.pdf}
% %     \includegraphics[width=\textwidth]{plots/smoothness_classif_lognormal.pdf}
% %     \caption{Unbalanced smooth. constants, $\lambda = \frac{1}{n}$.}
% %     \label{fig:expe-logreg-power-law}
% %   \end{subfigure}

% %   \caption{
% %     Comparison of \DPCD and \DPSGD for $\ell_2$-regularized logistic regression.
% %     The first line reports the evolution of the loss over iterations of both
% %     algorithms, with average, minimal and maximal values over 10 runs, and the
% %     second one reports coordinate-wise smoothness constants.
% %   }
% %   \label{fig:expe-logreg}
% % \end{figure}

% % \begin{figure}[t]
% %   \includegraphics[width=\linewidth]{plots/varying_dim_mmax.pdf}

% %   \caption{
% %     Comparison of \DPCD (with clipping rule $C_j = \sqrt{M_j/\tr(M)}$) and \DPSGD
% %     on linear regression in function of the dimension and the ratio
% %     ${M_{max}}/{M_{min}}$, with tuned hyperparameters.
% %     Circle sizes give the ratio of the optimization error of \DPSGD over the one
% %     of \DPCD, averaged over $10$ random runs.
% %   }
% %   \label{fig:cd-vs-sgd-cond-dim}
% % \end{figure}

% % \paragraph{Dimension and conditioning.}

% % In this experiment, we aim to numerically reproduce the results from
% % \Cref{sec:comparison-with-dp-sgd}, in the setting where one
% % parameter dominates.  To this end, we generate multiple datasets of
% % $n=10,000$ records, with one larger smoothness constant, where we
% % vary the dimension $p$ and the ratio $\frac{M_{max}}{M_{min}}$.  We
% % consider a linear regression problem (\ie quadratic loss and
% % $\psi = 0$) on each dataset and~run \DPSGD and \DPCD on each of
% % them, tuning hyperparameters and reporting the mean optimization
% % error for the last iterate over $10$ runs.  In
% % \Cref{fig:cd-vs-sgd-cond-dim}, we plot the ratio of the error of
% % \DPSGD over that of \DPCD.
% % % \aurelien{perhaps not so easy to visually distinguish
% % % dot sizes for 500, 1000 and 2500.}
% % As predicted by our theoretical analysis, the advantage of \DPCD
% % increases as $\frac{M_{max}}{M_{min}}$ increases. When the latter is
% % large, \DPSGD suffers from small learning rates and is unable to get
% % close to the optimal.  This effect is mitigated as the dimension
% % increases, which is due to the fact that coordinate-wise clipping is
% % oblivious to correlated gradient coordinates, which makes the amount
% % of noise added by \DPCD increase faster with the dimension than the
% % one added by \DPSGDy.
% % % \paul{there either we have adaptive clipping and it works, either
% % % we have not and we say that proper adaptive clipping should allow
% % % somehow estimating the full Lipschitz constant in comparison with
% % % the coordinate Lipschitz one, making clipping aware of the
% % % interactions between parameters.}  \aurelien{i remember this is
% % % slow in practice, but implementing \DPCD with mini-batch 10 could
% % % help us counter potential criticism that CD does not scale well
% % % with $n$.}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../main"
%%% End:
