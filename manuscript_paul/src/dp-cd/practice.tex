% !TEX root = ../../main.tex
%______________________________________________________________________________
%______________________________________________________________________________
%
% \DPCD in Practice
%______________________________________________________________________________
%______________________________________________________________________________


\section{\DPCD in Practice}
\label{sec:dp-cd-practice}

We now discuss practical questions related to \DPCD. First, we show how
to implement coordinate-wise gradient clipping using a single hyperparameter.
% to achieve
% non-trivial utility, existing algorithms rely on gradient clipping. In
% \DPCD, gradients must be clipped coordinate-wise, requiring to choose
% one threshold per coordinate. Since tuning these parameters is way too
% costly, we propose a strategy to adapt them from a single
% hyperparameter.
Second, we explain how to privately estimate the smoothness constants.
% uses component-wise smoothness constants
% to operate: we explain how to compute them privately.
Finally, we
discuss the possibility of standardizing the features and how this relates
to estimating smoothness constants for the important problem of fitting
generalized linear models.


% When learning from private data, some practical questions
% arise. First, the practitioner who uses SGD-like algorithms will
% generally standardize its dataset before proceeding. This requires
% publishing a ``standardization model'' together with the learned
% model. With \DPCD, one can skip this standardization step, and learn a
% ready-to-use model. To do so efficiently, \DPCD requires knowledge of
% the component-wise smoothness constants. Computing these constants
% often resembles standardization, but these need not to be
% released. Second, practical implementation of \DPSGD heavily rely on
% gradient clipping. When using \DPCD, gradients shall be clipped one
% coordinate at a time, which requires careful attention. We discuss
% these two questions in this section.
\subsection{Coordinate-wise Gradient Clipping}
\label{sub:clipping}

To bound the sensitivity of coordinate-wise gradients, our analysis of
Section~\ref{sec:diff-priv-coord} relies on the knowledge of Lipschitz
constants for the loss function $\ell(\cdot;d)$ that must hold for all
possible data points
$d\in \cX$.
% see
% inequality \eqref{eq:delta-lipschitz-norm} and the discussion above
% it.
This is classic in the analysis of DP optimization algorithms \citep[see
  e.g.,][]
{bassily2014Private,wang2017Differentially}. In practice however, these
Lipschitz constants can be difficult to bound tightly and often
give largely pessimistic estimates of sensitivities, thereby making gradients
overly noisy. To overcome this problem, the common practice in concrete
deployments of \DPSGD algorithms is to \emph{clip per-sample gradients}
so that their norm does not exceed a fixed threshold parameter $C > 0$
\citep{abadi2016Deep}:
% Let's go back to inequality \eqref{eq:delta-lipschitz-norm} and
% discuss gradient sensitivities directly. Lipschitz constants can
% indeed give pessimistic estimates of sensitivities, making gradients
% overly noisy, which severly hurts convergence. Gradients are thus
% generally clipped at a fixed threshold $C > 0$ \citep{abadi2016Deep}
% as follows:
\begin{align}
%  \label{eq:standard_clipping_rule}
  \clip(\nabla \ell(w), C)
  = \min\Big(1, \frac{C}{\norm{\nabla \ell(w)}}_2\Big)  \nabla \ell(w)\enspace.
\end{align}
This effectively ensures that the sensitivity $\Delta(\clip(\nabla \ell, C))$
of the clipped gradient is bounded by $2C$.

In \DPCD, gradients are released one coordinate at a time and should
thus be clipped in a coordinate-wise fashion. Using the same threshold for
each coordinate would ruin the ability of \DPCD to account for imbalance
across gradient coordinates, whereas tuning coordinate-wise thresholds as $p$
individual
hyperparameters $\{C_j\}_{j=1}^p$
is impractical.

Instead, we leverage the results of \Cref{thm:cd-utility} to adapt
them from a single hyperparameter.  We first remark that our utility
guarantees are invariant to the scale of the matrix $M$. After
rescaling $M$ to $\widetilde M = \frac{p}{\tr(M)} M$ so that
$\tr(\widetilde M) = \tr(I) = p$, as proposed by
\citet{richtarik2014Iteration}, the key quantity $\norm{L}_{M^{-1}}$
in our our utility bounds is replaced by
$\norm{L}_{\widetilde M^{-1}}$. This suggests to parameterize the
$j$-th threshold as $C_j = \sqrt{{M_j}/{\tr(M)}} C$ for some $C > 0$,
ensuring that $\norm{\{C_j\}_{j=1}^{p}}_{\widetilde M^{-1}} \leq 2C$.
The parameter $C$ thus controls the overall sensitivity, allowing
clipped \DPCD to perform $p$ iterations for the same privacy budget as
one iteration of clipped \DPSGD.

\subsection{Private Smoothness Constants}
\label{sec:priv-smoothn}

\DPCD requires the knowledge of the coordinate-wise smoothness
constants $M_1,\dots,M_p$ of $f$ to set appropriate step sizes (see
\Cref{thm:cd-utility}) and clipping thresholds (see
above).\footnote{In fact, only $\smash{M_j/\sum_{j'} M_{j'}}$ is needed, as we
  tune the clipping threshold and scaling factor for the step sizes.
  See \Cref{sec:numerical-experiments}.}  In most problems, the
$M_j$'s depend on the dataset $D$ and must thus be estimated privately
using a fraction $\epsilon'$ of the overall privacy budget $\epsilon$.
% Since $f$ is an
% average of loss terms, its coordinate-wise smoothness constants are
% the average of those of $\ell(\cdot, d)$ over $d\in D$.  These per-sample
% quantities are easy to get for typical losses (see
% \Cref{sec:data-standardization} for the case of linear models).
% Privately estimating $M_1,\dots,M_p$ thus reduces to a classic private
% mean estimation problem for which many methods exist.  For instance,
% assuming that the practitioner knows a crude upper bound on per-sample
% smoothness constants, he/she can compute the smoothness constants of
% the $\ell(\cdot, d)$'s, clip them to the pre-defined upper bounds, and
% privately estimate their mean using the Laplace mechanism
% (see \Cref{sec:priv-estim-smoothn} for
% details).
Recall that $f$ is the average loss over the dataset $D$ (see the
definition of~\eqref{eq:prelim:dp-erm-eq}). We can thus denote by
$M_j^{(i)}$ the $j$-th coordinate-smoothness constant of
$\ell(\cdot, d_i)$, where $d_i$ is the $i$-th point in $D$. The $j$-th
smoothness constant of the function $f$ is thus the average of all
these constants: $M_j = \frac{1}{n} \sum_{i=1}^n M_j^{(i)}$.

Assuming that the practitioner knows an approximate upper bound $b_j$
over the $M_j^{(i)}$'s, they can enforce it by clipping $M_j^{(i)}$ to
$b_j$ for each $i\in[n]$. The sensitivity of the average of the clipped $M_j^{
(i)}$'s is
thus $2b_j/n$. One can then compute an estimate of $M_1,\dots,M_p$ under $\epsilon$-DP using the Laplace mechanism as follows:
\begin{align}
  M_j^{priv} = \frac{1}{n} \sum_{i=1}^n \clip(M_j^{(i)}, b_j) + \text{Lap}\left
  (\frac{2b_jp}{n\epsilon'}\right),\quad\text{for each }j\in[p]\enspace,
\end{align}
where the factor $p$ in noise scale comes from using the simple
composition theorem \cite{dwork2014Algorithmic}, and
$\text{Lap}(\lambda)$ is a sample drawn in a Laplace distribution of
mean zero and scale $\lambda$.  The computed constant can then
directly be used in \DPCD, allocating the remaining budget
$\epsilon-\epsilon'$ to the optimization procedure.  We show
numerically in \Cref{sec:numerical-experiments} that dedicating $10\%$
of the total budget $\epsilon$ to this strategy allows \DPCD to
effectively exploit the imbalance across gradients' coordinates.

% When the component-smoothness constants are unknown to the
% practitioner, they can still dedicate some privacy budget to computing
% them. This is a private mean estimation problem. One can thus simply
% clip individual contributions to a reasonable upper bound, and make
% the result private with the Laplace mechanism
% \citep{dwork2013Algorithmic}. Since computing these constants is
% similar to computing a gradient, a small fraction of the privacy
% budget generally suffices to obtain a good estimation of these
% constants. This issue is in fact no specific to \DPCD. As pointed
% above, these constants are needed to standardize the data, and must be
% published with learned model when standardizing. Nonetheless, we
% provide some experiments with privately learned component-smoothness
% constants in \Cref{sec:numerical-experiments}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Feature Standardization}
\label{sec:data-standardization}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

CD algorithms are very popular to solve generalized linear
models \citep{friedman2010Regularization} and their regularized version (\eg
LASSO, logistic regression).
For these problems, the coordinate-wise smoothness constants are
$M_j \propto \frac 1n \norm{X_{:,j}}_2^2$, where $X_{:,j} \in \RR^{n}$
is the vector containing the value of the $j$-th feature. Therefore, standardizing the features to have zero mean and
unit variance (a standard preprocessing step) makes coordinate-wise smoothness
constants equal. However, this requires to compute the
mean and variance of each feature in $D$, which is more
costly than the smoothness
constants to estimate privately.\footnote{We note that the privacy cost of
standardization is rarely accounted for in practical evaluations.}
Moreover, while our theory suggests that \DPCD may not be superior to \DPSGD
when smoothness constants are all equal (see
Section~\ref{sec:comparison-with-dp-sgd}), the
numerical results of \Cref{sec:numerical-experiments} show that \DPCD often
outperforms \DPSGD even when features are standardized.

Finally, we emphasize that standardization is not always possible.
This notably happens when solving the problem at hand is a subroutine of another algorithm.
For instance, the Iteratively Reweighted Least Squares (IRLS) algorithm
\citep{holland1977Robust} finds the maximum likelihood estimate of a
generalized linear model by solving a sequence of linear
regression problems with reweighted features, proscribing standardization.
Similar situations happen when using reweighted $\ell_1$ methods for
non-convex sparse regression \citep{candes2008Enhancing}, relying on convex (LASSO) solvers for the inner loop.
\DPCD is thus a method of choice to serve as subroutine in private versions of these algorithms.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../main"
%%% End:
