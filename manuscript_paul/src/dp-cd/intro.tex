% !TEX root = ../../main.tex
%______________________________________________________________________________
%______________________________________________________________________________
%
% INTRODUCTION
%______________________________________________________________________________
%______________________________________________________________________________

\section{Introduction}




% Machine learning fundamentally relies on the availability of data, which can
% be sensitive or confidential.
% It is now well-known that preventing learned models from leaking information
% about individual training points requires particular attention
% \citep{shokri2017Membership}.
% A standard approach for training models while provably controlling the amount of
% leakage is to solve an empirical risk minimization (ERM) problem
% under a differential privacy (DP) constraint \citep{chaudhuri2011Differentially}.
% In this work, we aim to design a differentially private algorithm which
% approximates the solution to a composite ERM problem of the form:
% % to learn
% % a differentially private approximate solution $w_{priv}$ to the problem
% \begin{align}
%   \label{eq:dp-erm}
%   w^* \in
%   \argmin_{w \in \mathbb{R}^p}
%   \frac{1}{n} \sum_{i=1}^n \ell(w; d_i) + \psi(w)\enspace,
% \end{align}
% where $D = (d_1, \dots, d_n)$
% % \jo{What is the mathematical nature of D? a
% %matrix? sizes?}\aurelien{introduce data domain explicitly and use it in def
% %of $\ell$. instantiate in expe;}
% is a dataset of $n$ samples drawn from a universe $\cX$,
% $\ell: \RR^p \times \cX \rightarrow \RR$ is a loss function which is convex
% and smooth in $w$, and
% $\psi: \RR^p \rightarrow \RR$ is a convex regularizer which is separable (\ie
% $\psi(w) = \sum_{j=1}^p \psi_j(w_j)$) and typically nonsmooth (\eg
% $\ell_1$-norm).

% Differential privacy constraints induce a trade-off between the privacy and
% the utility (i.e., optimization error) of the solution of~\eqref{eq:dp-erm}.
% This trade-off was made explicit by \citet{bassily2014Private}, who derived
% lower bounds on the achievable error given a fixed privacy budget.
% To solve the DP-ERM problem in practice, the most popular approaches are based
% on Differentially Private variants of Stochastic Gradient Descent (\DPSGD)
% \citep{bassily2014Private,abadi2016Deep,wang2017Differentially}, in which
% random perturbations are added to the (stochastic) gradients.
% \citet{bassily2014Private} analyzed \DPSGD in the non-smooth DP-ERM setting,
% and \citet{wang2017Differentially} then proposed an efficient DP-SVRG
% algorithm for composite DP-ERM.
% Both algorithms match known lower bounds.
% %\paul{mention output perturbation and other approaches? at least need
% %more references here.} \aurelien{no, this breaks the flow of the
% %introduction. related work is the right place}
% %A few other algorithms have been considered for DP-ERM with good results,
% %but all of them solve a narrower problem than \eqref{eq:dp-erm}.
% %They either rely on a specific geometry of the constraint set
% %\citep{talwar2015Nearly,bassily2021NonEuclidean,asi2021Private}, or restrict
% %their analysis to generalized linear models \citep{damaskinos2020Differentially}.
% % Additionally, they all rely on the computation of full (stochastic) gradients.
% %\paul{all methods rely on computations of full (stochastic) gradients: is this
% %really convincing?}\aurelien{also it is not clear what you mean by
% %"strong assumptions"?}
% % Moreover, all these method need to compute full (stochastic) gradients, and may
% % therefore be
% SGD-style algorithms perform well in a wide variety of settings, but
% also have some flaws: they either require small (or decreasing) step
% sizes or variance reduction schemes to guarantee convergence, and they
% can be slow when gradients' coordinates are imbalanced.
% %sensitive to bad coordinate-wise conditioning.\aurelien{terme à ajuster si on trouve mieux}
% %[[REF?]]\aurelien{are we better on this ground?},
% %and they are highly sensitive to parameters scales\aurelien{``parameters
% %scales is quite vague. can't we say something about conditioning?'}
% %\citep{ruder2017Overview}.
% These flaws propagate to the private counterparts of these
% algorithms.
% %
% Despite a few attempts at designing other differentially private solvers for
% ERM under different setups
% \citep{talwar2015Nearly,damaskinos2021Differentially}, the differentially
% private optimization toolbox remains limited, which undoubtedly restricts the
% resolution of practical problems.
% % we firmly believe that broadening the spectrum of private algorithms is essential
% % to the development of efficient private optimization in a wide variety of
% % practical settings.


% % Other approaches to this DP-ERM problem have been developed in some specific
% % settings.
% % For constrained DP-ERM, Frank-Wolfe-style \citep{frank1956Algorithm,jaggi2013Revisiting}
% % algorithms have recently been proven useful \citep{talwar2015Nearly,talwar2016Private,bassily2021NonEuclidean,asi2021Private}.
% % For generalized linear models (GLM), which is the $\ell_2$-regularized ERM
% % problem, dual coordinate descent algorithms have been proposed by
% % \citep{damaskinos2020Differentially}.

% % These algorithms have all been analyzed theoretically, and have been shown to
% % match the utility lower bound in their respective settings.
% % However, they all rely on the computation of a full (stochastic) gradient at
% % each iteration.
% % Thus, the differentially private optimization toolbox remains limited, and we
% % firmly believe that the development of new algorithms is crucial in the
% % practical use of these algorithms.
% % \aurelien{pour éviter l'effet ``on le fait car ca n'a pas été fait'', je
% % pense qu'il serait préférable d'inverser l'ordre du propos: dans ce papier, on
% % propose un DP-CD, ce qui est intéressant car CD est populaire et a tels
% % avantages sur SGD. Marc: +1.}
In this Chapter, we propose the differentially private proximal
coordinate descent algorithm (\DPCD). This algorithm is based on the
proximal coordinate descent method, that we described in
\Cref{sec:prox-stoch-grad}. Like \DPSGD (see
\Cref{sec:diff-priv-stoch-1}), \DPCD preserves differential privacy by
performing updates based on perturbed gradients. At each iteration, it
does a proximal coordinate update using a coordinate-wise gradient,
that was computed under differential privacy using the Gaussian
mechanism.
% Coordinate Descent (CD) methods have encountered a large success in
% non-private machine learning due to their simplicity and effectiveness
% \citep{liu2009Blockwise,friedman2010Regularization,chang2008Coordinate,sardy2000Block},
% and have seen a surge of practical and theoretical interest in the
% last decade
% \citep{nesterov2010Efficiency,wright2015Coordinate,shi2017Primer,
%   richtarik2014Iteration,fercoq2014Accelerated,tappenden2016Inexact,
%   hanzely2020Variance,nutini2015Coordinate,karimireddy2019Efficient}.
%CD algorithms use partial information on the gradient (only one coordinate),
%allowing cheaper updates than full gradient methods.

We propose \DPCD and analyze its theoretical and empirical convergence
properties as a differentially private solver for the composite
empirical risk minimization problem. Let $\cX$ be a feature space and
$\cY$ a label space, and suppose that we have a dataset
$D = \{d_1, \dots, d_n\} \subseteq (\cX \times \cY)^n$ of $n$
records. We study the following unconstrained composite problem:
\begin{align}
  w^* \in \argmin_{w \in \RR^p} \bigg\{ F(w) := f(w) + \psi(w)
  \bigg\}
  \enspace,
  \enspace
  \text{ with } f(w) = \frac{1}{n} \sum_{i=1}^n \ell(w; d_i)
  \enspace,
  \label{eq:prelim:dp-erm-eq-chap-cd}
  \tag{$\star'$}
\end{align}
where $\ell: \RR^p \times \cX \times \cY \rightarrow \RR$ is a loss
function which is convex and coordinate-wise smooth in its first
parameter, and $\psi: \RR^p \rightarrow \RR$ is a separable convex
regularizer (\ie $\psi(w) = \sum_{j=1}^p \psi_j(w_j)$) that is
typically nonsmooth (\eg $\ell_1$-norm).

% In contrast to stochastic gradient descent, coordinate descent methods
% converge with constant step sizes, that adapt to the coordinate-wise
% smoothness of the objective. Additionally, coordinate-wise updates
% naturally tend to have a lower sensitivity. Operating with partial
% gradients thus enables our private algorithm to reduce the
% perturbation required to guarantee privacy without resorting to
% privacy amplification by sampling (see \Cref{sec:amplification}).


% than
% full gradient descent, making it less sensitive to bad problem conditioning,
% overcoming SGD's principal flaw.
% \aurelien{mention don't need amp by subsampling?}
% Despite these advantages, it
% is not obvious whether achieving a good
% privacy-utility trade-off is possible with CD methods.
% Indeed, they typically require more iterations than full gradient descent,
% but coordinate-wise updates do not systematically reduce the amount of
% perturbation needed to guarantee differential privacy.
% and prevents the
% use of privacy amplification by subsampling Balle et al.
% [2018], as argued by Damaskinos et al. [2020].
% Ensuring efficiency of a differentially private counterpart of coordinate
% descent is however not straightforward.
% This essentially comes from the fact that repeated queries over the same database
% leak more private information.
% In gradient algorithms, this means that gradients' perturbation scale required
% for guaranteeing privacy grows with the total number of iterations.
% And coordinate descent requires more iterations than gradient descent.
% Nonetheless, through


We start by showing that the proposed algorithm satisfies differential
privacy. Importantly, we note that the coordinates of the gradient
generally have a much lower sensitivity than the full gradient. This
allows \DPCD to compensate its larger number of iterations (which
induces larger noise) with smaller sensitivity.\footnote{Contrarily to
  \DPSGD, \DPCD does not rely on privacy amplification by sampling
  (see \Cref{sec:amplification}), which is not applicable in this
  setting.} We then theoretically analyze the properties of \DPCD by
developing a novel analysis of proximal coordinate descent with
perturbed (but unbiased) gradients. This allows to derive upper bounds
on the privacy-utility trade-off achieved by \DPCD.
% Inspired by analyses of SGD methods
% \citep{shamir2013Stochastic,johnson2013Accelerating},
To this end, we prove a recursion on distances of \DPCD's iterates to
an optimal point. Our analysis keeps track of coordinate-wise
regularity constants all along, which tightly captures the importance
of using large constant step sizes to obtain high utility. Our results
highlight the fact that \DPCD can exploit imbalanced gradient
coordinates to outperform \DPSGD.
%and small dependence on the gradients' perturbation.
% The proposed analysis also improves upon known convergence rates for
% inexact coordinate descent in the non-private setting
% \citep{tappenden2016Inexact} by exploiting the fact that, in our
% setting, gradients are unbiased.
% in settings where inexactness results
% from noisy, unbiased gradients.
% \aurelien{probably to remove}
We assess the optimality of \DPCD by deriving lower bounds
that capture coordinate-wise Lipschitz regularity measures, and show that
\DPCD matches those bounds up to logarithmic factors.
% Even though \DPCD already achieves good utility when most coordinates have small
% sensitivity,
Our lower bounds also suggest interesting perspectives for future work on
\DPCD algorithms.
% by putting more privacy budget on the coordinates with largest
% sensitivities.\aurelien{I would keep this for later, to avoid overclouding
% the list of contributions}
% It also keeps track of the learning rates up until the end.

Our theoretical results also have important consequences for practical
implementations, which heavily rely on gradient clipping to achieve
good utility.
% Efficient practical use of our algorithm indeed depends, as \DPSGD does,
% on the use of tuned clipping thresholds.
In contrast to \DPSGD, \DPCD requires to set \emph{coordinate-wise} clipping
thresholds, which can lead to impractical coordinate-wise hyperparameter tuning.
% However, it departs from \DPSGD in that these thresholds can be different for
% each coordinate.
% Fortunately, our analysis suggests
%Based on our analysis, we instead
We instead propose a simple rule for adapting these thresholds from a
single hyperparameter. We also show how the coordinate-wise smoothness
constants used by \DPCD can be
estimated privately. We validate our theory with numerical
experiments on real and synthetic datasets. These experiments further
show that even in balanced problems, \DPCD can still improve over
\DPSGD, confirming the relevance of \DPCD for DP-ERM.
\\

The main contributions of this Chapter can be summarized as follows:
% \aurelien{these do not
%really sound to me like our main contributions. What about introducing a new
%algorithm?..}
\begin{enumerate}
\item We propose the first differentially private proximal coordinate
  descent method for composite \DPERM, formally prove its utility, and
  highlight regimes where it outperforms \DPSGD.
\item We show matching lower bounds under coordinate-wise regularity
  assumptions.
\item We give practical guidelines to use \DPCD, and show its
  relevance through numerical experiments.
\end{enumerate}

%\aurelien{the description of what we do is not detailed enough: you should
%give more details on the algorithm, the analysis and the results you obtain.
%Then, the concise list should be a way to summarize the main contributions.}

The rest of this Chapter is organized as follows.  We briefly describe
some related work in \Cref{sec:dp-cd:related-works}.  In
\Cref{sec:diff-priv-coord}, we present our \DPCD algorithm, show that
it satisfies differential privacy, establish utility guarantees, and
%that nearly match known lower bounds.
% if possible: we show that these bounds are optimal for M-comp-smooth functions
compare these guarantees with those of \DPSGD.
In \Cref{sec:utility-lower-bounds}, we derive lower bounds under
coordinate-wise regularity assumptions, and
show that \DPCD can match them. \Cref{sec:dp-cd-practice} discusses practical
questions related to gradient clipping and the private estimation of
smoothness constants.
\Cref{sec:numerical-experiments} presents our numerical experiments,
comparing \DPCD and \DPSGD on LASSO and $\ell_2$-regularized
logistic regression problems. %\aurelien{possibly to update later}
% Finally, we review existing work in
% \Cref{sec:related-works}, and conclude with promising lines of future work in
% \Cref{sec:conclusion-and-discussion}.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../main"
%%% End:
