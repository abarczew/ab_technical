% !TEX root = ../../main.tex

%______________________________________________________________________________
%______________________________________________________________________________
%
% PRIVACY
%______________________________________________________________________________
%______________________________________________________________________________

\section{Proof of \Cref{thm:dp-cd-privacy}}
\label{sec:proof-privacy}

% \aurelien{in the main text we have used the term ``randomized
% algorithm'' (with notation
% $\cA$) instead of ``random mechanism'' ($\cM$). it would be better to adopt
% the same convention in this appendix. (Of course we should keep the term
% ``Gaussian mechanism'' though)}

To track the privacy loss of an adaptive composition of $K$ Gaussian
mechanisms, we use Rényi Differential Privacy
\citep[RDP]{mironov2017Renyi}. We note that similar results are
obtained with zero Concentrated Differential Privacy
\citep{bun2016Concentrated}. This flavor of differential privacy,
gives tighter privacy guarantees in that setting, as it reduces the
noise variance by a multiplicative factor of $\log(K/\delta)$ in
comparison to the usual advanced composition theorem of differential
privacy \citep{dwork2006Calibrating}.  Importantly, RDP can be
translated back to differential privacy.

In this section, we recall the definition and main properties of zCDP.
We denote by $\cD$ the set of all datasets over a universe $\cX$
and by $\cF$ the set of possible outcomes of the randomized
algorithms we consider.

\subsection{Rényi Differential Privacy}

We will use the Rényi divergence (\Cref{def:renyi-div}), which gives
a distribution-oriented vision of privacy.

\begin{definition}[Rényi divergence, \citealt{vanerven2014Renyi}]
  \label{def:renyi-div}
  For two random variables $Y$ and $Z$ with values in the same domain $\cC$,
  the Rényi divergence is, for $\alpha > 1$,
  \begin{align}
    D_\alpha(Y||Z)
    = \frac{1}{\alpha - 1} \log \int_{\cC} \prob{Y=z}^\alpha \prob{Z=z}^{1 - \alpha} dz\enspace.
  \end{align}
\end{definition}

%\jo{not sure you have the correct def...should refer to a book for that,
%  or a survey say \url{https://arxiv.org/pdf/1206.2459.pdf}}


We now define RDP in \Cref{def:rdp}. RDP provides a strong privacy
guarantee that can be converted to classical differential privacy
(\Cref{lemma:rdp-to-dp} and \Cref{cor:rdp-to-dp}).

\begin{definition}[Rényi Differential Privacy,
\citealt{mironov2017Renyi}]
  \label{def:rdp}
  A randomized algorithm $\cA : \mathcal D \rightarrow \mathcal F$ is
  $(\alpha, \epsilon)$-Rényi-differentially private (RDP) if, for all
  all datasets $D, D' \in \mathcal D$ differing on at most one
  element,
  \begin{align}
    D_\alpha(\cA(D) || \cA(D')) \le \epsilon\enspace.
  \end{align}
\end{definition}

\begin{lemma}[{\citealt[Proposition~3]{mironov2017Renyi}}]
  \label{lemma:rdp-to-dp}
  If a randomized algorithm $\cA : \mathcal D \rightarrow \mathcal F$
  is $(\alpha, \epsilon)$-RDP, then it is
  $(\epsilon + \frac{\log(1/\delta)}{\alpha - 1}, \delta)$-differentially
  private for all $0 < \delta < 1$.
\end{lemma}

\begin{remark}
  \label{rmq:rdp-to-dp}
  The above $(\alpha,\epsilon)$-RDP guarantees hold for multiple
  values of $\alpha,\epsilon$. As such, $\epsilon = \epsilon(\alpha)$
  can be seen as a function of $\alpha$, and \Cref{lemma:rdp-to-dp}
  ensures that the algorithm is $(\epsilon', \delta)$-DP for
  \begin{align}
    \epsilon' = \min_{\alpha > 1} \left\{ \epsilon(\alpha) + \frac{\log(1/\delta)}{\alpha - 1} \right\}\enspace.
  \end{align}
\end{remark}

We can now restate in \Cref{thm:rdp-composition} the composition
theorem of RDP, which is key in designing private iterative
algorithms.

\begin{theorem}[{\citealt[Proposition~1]{mironov2017Renyi}}]
  \label{thm:rdp-composition}
  Let $\cA_1, \dots, \cA_K : \cD \rightarrow \cF$ be $K > 0$
  randomized algorithms, such that for $1 \le k \le K$, $\cA_k$
  is $(\alpha, \epsilon_k(\alpha))$-RDP,
  where these algorithms can be chosen adaptively (i.e., $\cA_k$ can use
  to the output of $\cA_{k'}$ for all $k' < k$).
  Let $\cA : \mathcal D \rightarrow \mathcal F^K$ such that for $D \in \cD$,
  $\cA(D) = (\cA_1(D), \dots, \cA_K(D))$.
  Then $\cA$ is $\left(\alpha, \sum_{k=1}^K \epsilon_k(\alpha)\right)$-RDP.
\end{theorem}


% \subsection{Gaussian mechanism}

Finally, we define the Gaussian mechanism (\Cref{def:gaussian-mechanism}), as used
in \Cref{algo:dp-cd}, and restate in \Cref{lemma:gaussian-rdp-private} the
privacy guarantees that it satisfies in terms of RDP.

\begin{definition}[Gaussian mechanism]
  \label{def:gaussian-mechanism}
  Let $f: \mathcal D \rightarrow \RR^p$, $\sigma > 0$, and $D \in \cD$.
  The Gaussian mechanism for answering the query $f$ is defined as:
  \begin{align}
    \cM_f^{Gauss}(D; \sigma) = f(D) + \mathcal N\left( 0, \sigma^2 I_p
    \right)\enspace.
  \end{align}
%  \aurelien{it would be better to find a notation where the parameter
%  $\sigma$ appears explicitly}
\end{definition}

\begin{lemma}[{\citealt[Corollary 3]{mironov2017Renyi}}]
  \label{lemma:gaussian-rdp-private}
  The Gaussian mechanism with noise $\sigma^2$ is
  $(\alpha, \frac{\Delta(f)^2 \alpha}{2 \sigma^2})$-RDP, where
  $\Delta(f) = \sup_{D,D'} \norm{f(D) - f(D')}_2$ (for neighboring
  $D, D'$) is the sensitivity of $f$.
\end{lemma}
\begin{proof}
  The function $h = \frac{f}{\Delta(f)}$ has sensitivity $1$, thus for
  any $s > 0$, the Gaussian mechanism $\cM_h^{Gauss}(\cdot;s)$ is
  $(\alpha, \frac{\alpha}{2\sigma^2})$-RDP
  \citep[Corollary~1]{mironov2017Renyi}. As $f = \Delta(f) \times h$,
  we have
  $\cM_f^{Gauss}(\cdot;\sigma) = \Delta(f) \times
  \cM_h^{Gauss}(\cdot;\frac{\sigma}{\Delta(f)})$.
  This mechanism is thus
  $(\alpha, \frac{\Delta(f)^2\alpha}{2\sigma^2})$-RDP.
\end{proof}


\begin{corollary}
  \label{cor:rdp-to-dp}
  Let $0 < \epsilon \le 1, 0 < \delta < \tfrac{1}{3}$. If a randomized
  algorithm $\cA : \mathcal D \rightarrow \mathcal F$ is
  $(\alpha, \frac{\gamma \alpha}{2 \sigma^2})$-RDP with $\gamma > 0$ and
  $\sigma = \frac{\sqrt{3 \gamma \log(1/\delta)}}{\epsilon}$ for all
  $\alpha > 1$, it is also $(\epsilon, \delta)$-DP.
\end{corollary}

\begin{proof}
  From \Cref{rmq:rdp-to-dp} it holds that $\cA$ is
  $(\epsilon',\delta)$-DP with
  \begin{align*}
    \epsilon' = \min_{\alpha > 1} \left\{ \frac{\gamma \alpha}{2 \sigma^2} +
    \frac{\log(1/\delta)}{\alpha - 1} \right\}
    \enspace.
  \end{align*}
  This minimum is
  attained when the derivative of the objective is zero, which is the
  case when
  $\frac{\gamma}{2\sigma^2} = \frac{\log(1/\delta)}{(\alpha - 1)^2}$,
  resulting in $\alpha = 1 + \sqrt{\frac{2\log(1/\delta)\sigma^2}{\gamma}}$.
  $\cA$ is thus $(\epsilon', \delta)$-DP with
  \begin{align}
  \label{eq:full_privacy_formula}
    \epsilon' = \frac{\gamma}{2\sigma^2} + \frac{\sqrt{\gamma \log(1/\delta)}}{\sqrt{2} \sigma}
    + \frac{\sqrt{\gamma \log(1/\delta)}}{\sqrt{2} \sigma}
     = \frac{\gamma}{2\sigma^2} + \frac{\sqrt{2\gamma \log(1/\delta)}}{\sigma}\enspace.
  \end{align}

  Choosing $\sigma = \frac{\sqrt{3 \gamma \log(1/\delta)}}{\epsilon}$
  now gives
  \begin{align}
    \epsilon'
    = \frac{\epsilon^2}{6 \log(1/\delta)} + \sqrt{{2}/{3}} \epsilon
    \le (1/6  + \sqrt{2/3}) \epsilon
    \le \epsilon\enspace,
  \end{align}
  where the first inequality comes from $\epsilon \leq 1$, thus $\epsilon^2
  \le \epsilon$
  and $\delta < 1/3$ thus $\frac{1}{\log(1/\delta)} \le 1$.
  The second inequality follows from $1/6  + \sqrt{2/3} \approx 0.983 < 1$.
\end{proof}


\subsection{Proof of \Cref{thm:dp-cd-privacy}}

We are now ready to prove \Cref{thm:dp-cd-privacy}.
From the privacy perspective, \Cref{algo:dp-cd} adaptively releases
and post-processes a series of gradient coordinates protected by the Gaussian
mechanism. We thus start by proving
\Cref{lemma:rdp-composition-gaussian-k}, which gives an $
(\epsilon,\delta)$-differential privacy
guarantee for the adaptive composition of $K$ Gaussian mechanisms.

\begin{lemma}
  \label{lemma:rdp-composition-gaussian-k}
%  \aurelien{say again here that the mechanisms can be chosen adaptively}
  Let $0 < \epsilon \le 1$, $\delta < 1/3$, $K > 0$, $p > 0$, and
  $\{f_k : \RR^p \rightarrow \RR\}_{k=1}^{k=K}$ a family of $K$ functions.
  The adaptive composition of $K$ Gaussian mechanisms,
  with the $k$-th mechanism releasing
  $f_k$ with noise scale $\sigma_k = \frac{\Delta(f_k) \sqrt{3K \log(1/\delta)}}{\epsilon}$
  is $(\epsilon, \delta)$-differentially private.
\end{lemma}
\begin{proof}
  Let $\sigma > 0$. \Cref{lemma:gaussian-rdp-private} guarantees that
  the $k$-th Gaussian mechanism with noise scale
  $\sigma_k = \Delta(f_k) \sigma > 0$ is
  $(\alpha, \frac{\alpha}{2 \sigma^2})$-RDP.  Then, the composition of
  these $K$ mechanisms is, according to \Cref{thm:rdp-composition},
  $(\alpha, \frac{k\alpha}{2 \sigma^2})$-RDP.  This can be converted
  to $(\epsilon,\delta)$-DP via \Cref{cor:rdp-to-dp} with
  $\gamma = K$, which gives
  $\sigma_k = \frac{\Delta(f_k)\sqrt{3 k \log(1/\delta)}}{\epsilon}$
  for $k\in[K]$.
  \end{proof}

We now restate \Cref{thm:dp-cd-privacy} and prove it.

\begin{theorem}{\ref{thm:dp-cd-privacy}}
%  \label{thm:dp-cd-privacy}
  Assume $\ell(\cdot;d)$ is $L$-coordinate-Lipschitz $\forall d\in\cX$.
  Let $\epsilon < 1$ and $\delta < 1/3$.
  If $\sigma_j^2 = \frac{12L_j^2 TK \log(1/\delta)}{n^2\epsilon^2}$
  for all $j \in [p]$,
  then \Cref{algo:dp-cd} satisfies $(\epsilon, \delta)$-DP.
\end{theorem}

\begin{proof}
  For $j \in [1, p]$, $\nabla_j f$ in \Cref{algo:dp-cd} is released using the
  Gaussian mechanism with noise variance $\sigma_j^2$.
  The sensitivity of $\nabla_j f$
  is $\Delta(\nabla_j f) = \frac{\Delta (\nabla_j \ell)}{n} \le \frac{2L_j}
  {n}$. Note that $TK$ gradients are released, and
  \begin{align*}
    \sigma_j^2 = \frac{12L_j^2 TK \log(1/\delta)}{n^2\epsilon^2} \text{ for } j \in [1, p]\enspace,
  \end{align*}
  thus by \Cref{lemma:rdp-composition-gaussian-k} and the post-processing
  property of DP,
  \Cref{algo:dp-cd}
  is  $(\epsilon, \delta)$-differentially private.
\end{proof}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../main"
%%% End:
