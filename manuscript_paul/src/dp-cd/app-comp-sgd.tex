% !TEX root = ../../main.tex

\section{Comparison with DP-SGD}
\label{sec-app:comparison-with-dp}

In this section, we provide more details on the arguments of
\Cref{sec:comparison-with-dp-sgd}, where we suppose that $\ell$ is $L$-coordinate-Lipschitz
and $\Lambda$-Lipschitz.
To ease the comparison, we assume that $R_M = \norm{w^0 - w^*}_M$, which is
notably the case in the smooth setting with $\psi = 0$
(see Remark~\ref{rmq:expec-first-upper-bound-without-non-smooth}).

\paragraph{Balanced.}
We start by the scenario where coordinate-wise smoothness constants are balanced
and all equal to $M = M_1 = \cdots = M_p$.
We observe that
\begin{align}
  \norm{L}_{M^{-1}}
  = \sqrt{\sum_{j=1}^p \frac{1}{M_j} L_j^2}
  = \sqrt{\frac{1}{M} \sum_{j=1}^p L_j^2}
  = \frac{1}{\sqrt{M}} \norm{L}_2\enspace.
\end{align}
We then consider the convex and strongly-convex functions separately:
\begin{itemize}
  \item \textit{Convex functions:}
        it holds that $R_M = \sqrt{M} R_I$, which yields the equality
        $\norm{L}_{M^{-1}} R_{M} = \norm{L}_2 R_I$.
  \item \textit{Strongly convex functions:}
        if $f$ is $\mu_M$-strongly-convex with respect to $\norm{\cdot}_M$, then for
        any $x, y \in \RR^p$,
        \begin{align}
          f(y)
          & \ge f(x) + \scalar{\nabla f(x)}{y - x} + \frac{\mu_M}{2} \norm{y - x}_M^2
            \nnl
          & = f(x) + \scalar{\nabla f(x)}{y - x} + \frac{M\mu_M}{2} \norm{y - x}^2_2\enspace,
        \end{align}
        which means that $f$ is $M\mu_M$-strongly-convex with respect to $\norm{\cdot}_2$.
        This gives
        $\frac{\norm{L}_{M^{-1}}^2}{\mu_M}
          = \frac{\norm{L}_2^2 / M}{\mu_I / M}
          = \frac{\norm{L}_2^2}{\mu_I}$.
\end{itemize}

In light of the results summarized in \Cref{table:utility-cd-sgd}, it remains to
compare $\norm{L}_2 = \sqrt{\sum_{j=1}^p L_j^2}$ with $\Lambda$, for which it
holds that $\Lambda \le \sqrt{\sum_{j=1}^p L_j^2} \le \sqrt{p}\Lambda$, which is
our result.

\paragraph{Unbalanced.}
When smoothness constants are disparate, we discuss the case where
\begin{itemize}
  \item \textit{one coordinate of the gradient dominates the others:}
        we assume without loss of generality that the dominating coordinate is the first one.
        It holds that $M_1 =: M_{\max} \gg M_{\min} =: M_j$, for all $j \neq 1$ and
        $L_1 =: L_{\max} \gg L_{\min} =: L_j$, for all $j \neq 1$ such that
        $\frac{L_1^2}{M_1} \gg \sum_{j\neq 1} \frac{L_j^2}{M_j}$.
        As $L_1$ dominates the other coordinate-Lipschitz constants, most of the variation of the loss comes from its first coordinate.
        This implies that $L_1$ is close to the global Lipschitz constant $\Lambda$ of $\ell$.
        As such, it holds that
        \begin{align}
          \norm{L}_{M^{-1}}^2
          =
          \sum_{j=1}^p \frac{L_j^2}{M_j}
          \approx \frac{L_1^2}{M_1}
          \approx \frac{\Lambda^2}{M_{\max}}\enspace.
        \end{align}
  \item \textit{the first coordinate of $\bar w^0$ is already very close to
          its optimal value}
        so that $M_1\abs{\bar w^0_1 - w^*_1} \ll \sum_{j \neq 1} M_j \abs{\bar w^0_j - w^*_j}$.
        Under this hypothesis,
        \begin{align}
          R_M^2
          \approx \sum_{j\neq 1}M_j\abs{w_j^0 - w_j^*}^2
          = M_{\min} \sum_{j\neq 1}\abs{w_j^0 - w_j^*}^2
          \approx M_{\min} R_I^2\enspace.
        \end{align}
\end{itemize}
We can now easily compare DP-CD with DP-SGD in this scenario.
First, if $\ell$ is convex, then
$\norm{L}_{M^{-1}} R_M \approx \sqrt{\frac{M_{\min}}{M_{\max}}} \Lambda R_I$.
Second, when $\ell$ is strongly-convex, we observe that for $x, y \in \RR^p$,
\begin{align}
  f(y)
  & \ge f(x) + \scalar{\nabla f(x)}{y - x} + \frac{\mu_M}{2} \norm{y - x}_M^2
    \nnl
  & \ge f(x) + \scalar{\nabla f(x)}{y - x} + \frac{M_{\min} \mu_M}{2} \norm{y - x}_2^2\enspace,
\end{align}
which implies that when $f$ is $\mu_M$ strongly-convex with respect to
$\norm{\cdot}_M$, it is $M_{\min}\mu_M$ strongly-convex with respect
to $\norm{\cdot}_2$.  This yields, under our hypotheses,
$\frac{\norm{L}_{M^{-1}}^2}{\mu_M} \approx \frac{\Lambda^2 /
  M_{\max}}{\mu_I / M_{\min}} = \frac{M_{\min}}{M_{\max}}
\frac{\Lambda^2}{\mu_I}$.
In both cases, DP-CD can get arbitrarily better than DP-SGD, and gets
better as the ratio ${M_{\max}}/{M_{\min}}$ increases.

% We illustrate
% this by comparing the two algorithms on synthetic linear regression
% problems (quadratic loss, $\psi=0$) with $n = 10, 000$ records and one
% larger smoothness constant. In each problem, we vary the dimension $p$
% and the ratio $M_{\max}/M_{\min}$.  On each dataset, we tune clipping
% and step sizes for each algorithm, and computethe relative error on
% the last iterate, averaged over 10 runs. In
% \Cref{fig:expe-conditioning}, we plot the ratio of the relative error
% of DP-SGD over that of DP-CD. As predicted by our theoretical
% analysis, the advantage of DP-CD increases as $M_{\max}/ M_{\min}$
% increases. When the latter is large, DP-SGD suffers from small
% learning rates and is unable to get close to the optimal.

% This effect is mitigated as the dimension increases, which is
% due to the fact that coordinate-wise clipping is oblivious to
% correlated gradient coordinates, which makes the amount of noise added
% by DP-CD increase faster with the dimension than the one added by DP

% \begin{figure}
%   \centering
%   \includegraphics[width=0.5\textwidth]{plots/varying_dim_mmax.pdf}
%   \caption{Comparison of DP-CD and DP-SGD on linear regression in
%     function of the dimension and the ratio $M_{\max}/M_{\min}$, with
%     tuned hyperparameters. Circle sizes give the ratio of the
%     optimization error of DP-SGD over the one of DP-CD, averaged over
%     10 random runs.}
%   \label{fig:expe-conditioning}
% \end{figure}

The two hypotheses we describe above are of course very restrictive.
However, it gives some insight about when and why DP-CD can outperform
DP-SGD.
Our numerical experiments in \Cref{sec:numerical-experiments} confirm this
analysis, even in less favorable
cases.
% In practice, our approximation $\norm{L}_{M^{-1}}^2 \approx \frac{\Lambda^2}{M_{\max}}$
% can be quite loose, particularly as the dimension grows.
% Indeed, the accumulation of smaller scaled coordinates, where DP-CD uses the
% large step size $1/M_{\min}$, causes it to add large amounts of noise on
% many potentially insignificant coordinates.
% In contrast, DP-SGD almost ignores these coordinates, due to its
% small step size, which can improve utility when these coordinates have a
% negligible impact on the objective.
% We show numerically in \Cref{sec:numerical-experiments} that the rule we
% proposed to set coordinate-wise clipping thresholds for DP-CD helps to
% mitigate this effect.



% To support our claims, we run DP-CD and DP-SGD on a range of datasets
% corresponding to the unbalanced case described above.
% We let two elements vary: the ratio $\frac{M_{\max}}{M_{\min}}$ and the
% dimension $p$.
% For each value of $p$, we generate a dataset of $n=10000$ random draws from a
% normal distribution of unit variance $\mathcal N(0, I_p)$.
% We then rescale the first feature of the dataset, so as to make the smoothness
% constant of the first variable bigger, controlling the
% ratio $\frac{M_{\max}}{M_{\min}}$.

% For each algorithm and dataset, we tune the learning rates and clipping values
% appropriately, and compute the mean optimization gap $f(w_{priv}) - f(w^*)$ for
% the last iterate over $10$ independent runs.
% We report in \Cref{fig:cd-vs-sgd-cond-dim} the ratio
% $\frac{f(w_{priv}^{(SGD)}) - f(w^*)}{f(w_{priv}^{(CD)}) - f(w^*)}$, which gives a
% sense of how many times more precise DP-CD is in comparison with DP-SGD.

% This experiment confirms our theoretical analysis, as:
% \begin{itemize}
% \item DP-CD improves on DP-SGD as the ratio $\frac{M_{\max}}{M_{\min}}$ grows, which
%   is what we expected to happen from the theoretical comparison above.
% \item DP-CD improves less on DP-SGD as the dimension increases. This is due to
%   the fact that sensitivities (and clipping thresholds) are evaluated in a
%   coordinate-wise fashion.
%   Indeed, while evaluating the gradient globally allow coordinates to balance
%   each other without changing the gradient norm, this phenomenon has to be dealt
%   with coordinate per coordinate for DP-CD.
%   As such, the approximation
%   $\Delta_{M^{-1}} (\nabla\ell) \approx \frac{\Lambda}{M_{\max}}$
%   is looser and looser as the dimension increases.
% \end{itemize}


% \subsection{Experiments on Linear Regression}
% \label{sec:exper-line-regr}

% In this section, we run experiments on non-regularized linear regression, that
% is $\ell(w; (x,y)) = \frac{1}{2} (x^Tw - y)^2$ and $\psi = 0$.
% We use the same datasets as the ones used in \Cref{sec:numerical-experiments} for the
% LASSO, and observe similar results, that are shown in \Cref{fig:expe-linreg}.

% \begin{figure}[H]
%   \captionsetup[subfigure]{justification=centering}
%   \centering
%   \begin{subfigure}[b]{0.22\textwidth}
%     \centering
%     \includegraphics[width=\textwidth]{plots/reg_balanced.pdf}
%     \includegraphics[width=\textwidth]{plots/smoothness_regression_balanced.pdf}
%     \caption{Balanced smoothness constants}
%     \label{fig:expe-linreg-constant}
%   \end{subfigure}
%   ~~
%   \begin{subfigure}[b]{0.22\textwidth}
%     \centering
%     \includegraphics[width=\textwidth]{plots/reg_lognormal.pdf}
%     \includegraphics[width=\textwidth]{plots/smoothness_regression_lognormal.pdf}
%     \caption{Unbalanced smoothness constants}
%     \label{fig:expe-linreg-power-law}
%   \end{subfigure}


%   \caption{
%     Comparison of DP-CD and DP-SGD for linear regression.
%     The first line reports the evolution of the loss over iterations of both
%     algorithms, with average, minimal and maximal values over 10 runs, and the
%     second one reports coordinate-wise smoothness constants.
%   }
%   \label{fig:expe-linreg}
% \end{figure}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../main"
%%% End:
