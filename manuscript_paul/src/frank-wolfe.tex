\chapter{Private Block-Coordinate Frank-Wolfe Algorithm}
\label{cha:priv-block-coord}



\section{Preliminary}

\subsection{Definitions}

In this document, for $P \ge p > 0$, $\cM \subseteq \RR^P$ will refer to a set $\cM_1 \times \cdots \times \cM_p$ separable in $p$ blocks.
This allows the definition of block-coordinate-wise curvature and lipschitz constants.

Matrices $U_1, \dots, U_p$ form a partition of $I_P$ so that $I_p = [U_1, \dots, U_p]$, and are viewed as operators that project an element of $\cM_i$ in $\cM$.

\begin{definition}%[Curvature Constant]
  For $f : \cM \rightarrow \RR$, the curvature constant $C_f$ is
  \begin{align}
    C_f = \sup_{\substack{\alpha, s \in \cM, \\ \gamma \in [0, 1], \\ \alpha' = \alpha + \gamma(s - \alpha) }} \frac{2}{\gamma^2} \left( f(\alpha') - f(\alpha) - \scalar{\alpha' - \alpha}{\nabla f(\alpha)} \right),
  \end{align}

  and the block-wise curvature constants, for $i \in [p]$,
  \begin{align}
    C_k^i = \sup_{\substack{\alpha \in \cM, s_i \in \cM_i, \\ \gamma \in [0, 1], \\ \alpha' = \alpha + \gamma U_i (s_i - \alpha_i) }}  \frac{2}{\gamma^2} \left( f(\alpha') - f(\alpha) - \scalar{\alpha'_i - \alpha_i}{\nabla_i f(\alpha)} \right).
  \end{align}
\end{definition}

\begin{definition}%[Lipschitz Constant]
  For $f : \cM \rightarrow \RR$ this lipschitz constant $L$ with respect to a norm $\norm{\cdot}$ is a constant such that for all $x, y \in \cM$,
  \begin{align}
    \norm{f(x) - f(y)} \le L \norm{x - y},
  \end{align}

  and the block-wise lipschitz constant $L_i$, for $i \in [p]$, is a constant such that for all $x \in \cM$ and $y \in \cM_i$,
  \begin{align}
    \norm{f(x + U_i v) - f(x)} \le L_i \norm{v}_i,
  \end{align}
  where $\norm{\cdot}_i$ is the norm defined as $\norm{v}_i = \norm{U_i v}$ for all $v \in \cM_i$.
\end{definition}

\subsection{Laplace Distributions}

\begin{definition}
  The Laplace distribution is the probability distribution with density function $f_\lambda(x) = \frac{1}{2\lambda} e^{- \frac{\abs{x}}{\lambda}}$.
\end{definition}

\begin{lemma}
  \label{lemma:laplace-tail-property}
  Let $x, y \ge 0$, then the Laplace density function satisfies $\displaystyle f_\lambda(x+y) = \frac{1}{2\lambda} e^{-\frac{x + y}{\lambda}} = e^{-\frac{y}{\lambda}} f_\lambda(x)$.
\end{lemma}

\begin{lemma}
  \label{lemma:diff-laplace}
  Let $b, b'$ be to Laplace variables drawn from $Lap(\lambda)$ for some scale $\lambda > 0$.
  Let $a > 0$, then
  \begin{align}
    \label{lemma:diff-laplace:1}
    \prob{b-b' = a} = \frac{1}{4\lambda}\left( 1 + \frac{a}{\lambda}\right) \exp\left(-\frac{a}{\lambda}\right) ;
  \end{align}
  \begin{align}
    \label{lemma:diff-laplace:2}
    \prob{b-b' \ge a} = \left( \frac{1}{2} + \frac{a}{4\lambda}\right) \exp\left(-\frac{a}{\lambda}\right).
  \end{align}
\end{lemma}

\begin{proof}
  For equality \ref{lemma:diff-laplace:1}, we have
  \begin{align}
    \prob{b - b' = a}
    & = \int_{-\infty}^\infty \frac{1}{4\lambda^2} e^{-\frac{\abs{x}}{\lambda}} e^{-\frac{\abs{x-a}}{\lambda}} dx \\
    & = \int_{-\infty}^0 \frac{1}{4\lambda^2} e^{\frac{x}{\lambda}} e^{\frac{x-a}{\lambda}} dx
      + \int_{0}^a \frac{1}{4\lambda^2} e^{-\frac{x}{\lambda}} e^{\frac{x-a}{\lambda}} dx
      + \int_a^\infty \frac{1}{4\lambda^2} e^{-\frac{x}{\lambda}} e^{-\frac{x-a}{\lambda}} dx \\
    & = \frac{1}{4\lambda^2} \Eval{\frac{\lambda}{2} e^{\frac{2x-a}{\lambda}}}{-\infty}{0}
      + \frac{1}{4\lambda^2} \Eval{x e^{\frac{-a}{\lambda}}}{0}{a}
      - \frac{1}{4\lambda^2} \Eval{\frac{\lambda}{2} e^{-\frac{2x-a}{\lambda}}}{a}{\infty} \\
    & = \frac{1}{8\lambda} e^{-\frac{a}{\lambda}}
      + \frac{a}{4\lambda^2} e^{-\frac{a}{\lambda}}
      + \frac{1}{8\lambda} e^{-\frac{a}{\lambda}} \\
    & = \frac{1}{4\lambda} \left( 1 + \frac{a}{\lambda} \right) e^{-\frac{a}{\lambda}}.
  \end{align}

  Using this, we get, for equality \ref{lemma:diff-laplace:2},
  \begin{align}
    \prob{b - b' \ge a}
    & = \frac{1}{4\lambda} \int_a^{\infty} \left( 1 + \frac{x}{\lambda} \right) e^{-\frac{x}{\lambda}} dx \\
    & = \frac{1}{4\lambda} \int_a^{\infty} e^{-\frac{x}{\lambda}} dx
      + \frac{1}{4\lambda^2} \int_a^{\infty} x e^{-\frac{x}{\lambda}} dx \\
    & = - \frac{1}{4\lambda} \Eval{\lambda e^{-\frac{x}{\lambda}}}{a}{\infty}
      + \frac{1}{4\lambda^2} \left[
      - \Eval{x \lambda e^{- \frac{x}{\lambda}}}{a}{\infty}
      + \int_a^\infty \lambda e^{-\frac{x}{\lambda}} dx
      \right] \\
    & = - \frac{1}{4\lambda} \Eval{\lambda e^{-\frac{x}{\lambda}}}{a}{\infty}
      + \frac{1}{4\lambda^2} \left[
      - \Eval{x \lambda e^{- \frac{x}{\lambda}}}{a}{\infty}
      - \Eval{\lambda^2 e^{- \frac{x}{\lambda}}}{a}{\infty}
      \right] \\
    & = \frac{1}{4} e^{-\frac{a}{\lambda}}
      + \frac{a}{4 \lambda} e^{-\frac{a}{\lambda}}
      + \frac{1}{4} e^{-\frac{a}{\lambda}} \\
    & = \left( \frac 12 + \frac{a}{4\lambda} \right) e^{-\frac{a}{\lambda}}.
  \end{align}
\end{proof}

\section{Inexact Block-Coordinate Frank-Wolfe}

To prove convergence of the differentially private block-coordinate Frank-Wolfe algorithm, we first prove a more general result on inexact block-coordinate Frank-Wolfe algorithm.

The block-coordinate Frank-Wolfe algorithm is described in \cite{lacoste-julien_block-coordinate_2013}, and the idea of the inexact update and its analysis is taken from \cite{jaggi_revisiting_2013}.

\begin{algorithm}[H]
  \caption{Inexact Block-Coordinate Frank-Wolfe}
  \label{algo:inexact-bc-fw}
  \hspace*{\algorithmicindent} \textbf{Input} : Let $\alpha \in \cM = \cM_1 \times \cdots \times \cM_p$. Let $\delta^1, \dots, \delta^p > 0$.
  \begin{algorithmic}[1]
    \For{$k = 0 \dots K$}
    \State Pick $i \sim_u [p]$.
    \State Find $s_i^k$ such that
    $\displaystyle \scalar{s_i^k}{\nabla_i f(\alpha^k)} \le \min_{s'_i \in \cM_i} \scalar{s'_i}{\nabla_i f(\alpha^k)} + \frac 12 \gamma_k \delta^i C_f^i$
    \State Let $\gamma_k = \frac{2p}{k+2p}$.
    \State Update $\alpha^k_i = \alpha^k_i + \gamma_k \left( s_i^k - \alpha^k_i \right)$.
    \EndFor
  \end{algorithmic}
\end{algorithm}

We first prove \Cref{lemma:iterate-progression} that bounds the progress made by at each iteration of the algorithm.

\begin{lemma}
  \label{lemma:iterate-progression}
  With $\alpha^k$ the iterates of \Cref{algo:inexact-bc-fw},
  \begin{align}
    \expec{}{f(\alpha^{k+1})}
    & \le f(\alpha^k) - \frac{\gamma_k}{p} g(\alpha^k) + \frac {\gamma_k^2}{2p} \sum_{i=1}^p C_f^i (1 + \delta^i),
  \end{align}
  where $\displaystyle g(\alpha) = \scalar{\alpha}{\nabla f(\alpha)} - \min_{s \in \cM} \scalar{s}{\nabla f(\alpha)} $.
\end{lemma}

\begin{proof}
  For $\alpha^+ = \alpha + \gamma U_i(s_i - \alpha_i)$ we have
  \begin{align}
    f(\alpha^+)
    & = f(\alpha + \gamma U_i(s_i - \alpha_i)) \\
    & \le f(\alpha) + \gamma \scalar{s_i - \alpha_i}{\nabla_i f(\alpha)} + \frac{\gamma^2}{2} C_f^i.
  \end{align}

  Taking $\alpha = \alpha^k$ and $\alpha^+ = \alpha^{k+1} = \alpha + \gamma_k U_i(s_i - \alpha_i^k)$ gives
  \begin{align}
    \expec{}{f(\alpha^{k+1})}
    & \le \expec{}{f(\alpha^k + \gamma_k \scalar{s_i - \alpha_i^k}{\nabla_i f(\alpha^k)} + \frac{\gamma_k^2}{2} C_f^i} \\
    & = f(\alpha^k) + \frac {\gamma_k}{p} \sum_{i=1}^p \gamma_k \scalar{s_i - \alpha_i^k}{\nabla_i f(\alpha^k)} + \frac{\gamma_k^2}{2} C_f^i \\
    & = f(\alpha^k) + \frac {\gamma_k}{p} \scalar{s - \alpha^k}{\nabla f(x^k)} + \frac{\gamma_k^2}{2p} \sum_{i=1}^p C_f^i.
  \end{align}

  The definition of $s_i^k$ gives
  \begin{align}
    \scalar{s_i^k - \alpha_k^i}{\nabla_i f(\alpha^k)}
    & \le \min_{s'_i \in \cM_i} \scalar{s'_i}{\nabla_i f(\alpha^k)} - \scalar{\alpha^k_i}{\nabla_i f(\alpha^k)} + \frac 12 \gamma_k \delta^i C_f^i.
  \end{align}

  Taking the expectation from above expression yields
  \begin{align}
    \expec{}{\scalar{s_i^k - \alpha_k^i}{\nabla_i f(\alpha^k)}}
    & \le \expec{}{\min_{s'_i \in \cM_i} \scalar{s'_i}{\nabla_i f(\alpha^k)} - \scalar{\alpha^k_i}{\nabla_i f(\alpha^k)} + \frac 12 \gamma_k \delta^i C_f^i} \\
    \frac 1p \scalar{s^k - \alpha^k}{\nabla f(\alpha^k)}
    & \le  \frac 1p \sum_{i=1}^p \min_{s_i' \in \cM_i} \scalar{s_i'}{\nabla_i f(\alpha^k)} - \scalar{\alpha_i^k}{\nabla_i f(\alpha^k)} + \frac 12 \gamma_k \delta^i C_f^i \\
    & = \frac 1p \left(\min_{s' \in \cM} \scalar{s}{\nabla f(\alpha^k)} - \scalar{\alpha^k}{\nabla f(\alpha^k)} + \frac 12 \gamma_k \sum_{i=1}^p \delta^i C_f^i\right).
  \end{align}

  And it follows that
  \begin{align}
    \scalar{s^k - \alpha^k}{\nabla f(\alpha^k)}
    & = \min_{s' \in \cM} \scalar{s}{\nabla f(\alpha^k)} - \scalar{\alpha^k}{\nabla f(\alpha^k)} + \frac 12 \gamma_k \sum_{i=1}^p \delta^i C_f^i.
  \end{align}

  Plugging this in [ref]
  \begin{align}
    \expec{}{f(\alpha^{k+1})}
    & \le f(\alpha^k) + \frac{\gamma_k}{p}  \min_{s' \in \cM} \left(\scalar{s}{\nabla f(\alpha^k)} - \scalar{\alpha^k}{\nabla f(\alpha^k)}\right) + \frac {\gamma_k^2}{2p} \sum_{i=1}^p C_f^i (1 + \delta^i).
  \end{align}

\end{proof}

It remains to prove \Cref{thm:general_inexact_convergence}.

\begin{theorem}
  \label{thm:general_inexact_convergence}
  Let $\alpha^k$ be the iterates of \Cref{algo:inexact-bc-fw} and define $\displaystyle C = \sum_{i=1}^p C_f^i (1 + \delta^i)$, it holds that
  \begin{align}
    \expec{}{f(\alpha^k) - f^*} \le \frac{2p}{k + 2p} (C + (f(\alpha^0) - f^*)).
  \end{align}
\end{theorem}

\begin{proof}
  \Cref{lemma:iterate-progression} gives
  \begin{align}
    \phi^{k+1}
    & \le \phi^k - \frac{\gamma_k}{p} g(\alpha^k) + \frac{\gamma_k^2}{2p} C \\
    & \le \phi^k - \frac{\gamma_k}{p} \phi^k + \frac{\gamma_k^2}{2p} C \\
    & = \left( 1 - \frac{\gamma_k}{p} \right) \phi^k + \frac{\gamma_k^2}{2p} C,
  \end{align}

  where $\phi^k = \expec{}{f(\alpha^k) - f^*}$ and $C = \sum_{i=1}^p C_f^i (1 + \delta^i)$.

  Remark that we used the convexity of $f$ which gives:
  \begin{align}
    f(\alpha) - f^* \ge \scalar{\nabla f(\alpha)}{\alpha - \alpha^*} \ge \scalar{\nabla f(\alpha)}{\alpha} - \min_{s \in \cM} \scalar{\nabla f(\alpha)}{s} = g(\alpha).
  \end{align}

  The theorem can then be proven by induction. For $k=0$, $\gamma_0 = 1$ gives
  \begin{align}
    \phi^1
    \le \left( 1 - \frac 1 p \right )\phi^0 + \frac {1}{2p} C
    \le C + \phi^0.
  \end{align}

  Now, assume that the result holds for $k \ge 1$, then
  \begin{align}
    \phi^{k+1}
    & \le \left( 1 - \frac{\gamma_k}{p} \right) \phi^k + \frac{\gamma_k^2}{2p} C \\
    & \le \left( 1 - \frac{2}{k + 2p} \right) \left( \frac{2p}{k+2p} (C + \phi^0) \right) + \frac{2p}{(k+2p)^2} C \\
    & = \left( \frac{2p}{k+2p} - \frac{4p}{(k+2p)^2} \right) (C + \phi^0) + \frac{2p}{(k+2p)^2} C \\
    & \le \left( \frac{2p}{k+2p} - \frac{2p}{(k+2p)^2} \right) (C + \phi^0) \\
    & = \frac{2p}{k+2p} \left( 1 - \frac{1}{k+2p} \right) (C + \phi^0) \\
    & = \frac{2p}{k+2p} \frac{k+2p-1}{k+2p}  (C + \phi^0) \\
    & = \frac{2p}{k+2p} \frac{k+2p}{k+2p+1}  (C + \phi^0) \\
    & = \frac{2p}{k+1+2p} (C + \phi^0).
  \end{align}
\end{proof}

\section{Differentially Private Block-Coordinate Frank-Wolfe}

In this section, we consider the problem of DP-ERM, which is the problem of solving the ERM problem under differential-privacy constraints on the data:
\begin{align}
  \min_{\alpha \in \cM} f(\alpha; D) = \frac 1n \sum_{i=1}^n \ell(\alpha; d_i),
\end{align}
where elements of $D$ live in $\RR^P \times \RR$ and $\ell$ is convex and block-coordinate wise $L_1$-Lipschitz with constants $L_i$.

The definition of a differentially private version of \Cref{algo:inexact-bc-fw} can be done similarly to \cite{talwar_nearly_2015}, under the rather strong assumption that every $\cM_i$ is the convex hull of a finite set $S^i$.
In this setting, the Frank-Wolfe update necessarily involves a point of $S^i$, due to \Cref{rmk:compact_set_fw}.

\begin{remark}
  \label{rmk:compact_set_fw}
  Let $n \in \bbN$ and $\cM \subseteq \RR^n$ be the convex hull of a compact set $S \subseteq \RR^n$.
  For any vector $v \in \RR^n$, $\displaystyle \argmin_s \scalar{v}{s} \cap S \not= \emptyset$.
\end{remark}

The Frank-Wolfe update thus boils down to finding the vertex that minimizes the linear approximation of the optimized function in a finite set.
Making it differentially private thus heavily relies on the report-noisy-max mechanism.

\begin{lemma}
  \label{lemma:sensitivity-scalar-product}
  The sensitivity of $h_i(v; D) := \scalar{v}{\nabla_i f(\alpha; D)}$ is
  \begin{align}
    \Delta h_i
    & = \sup_{D, D'} \abs{ \scalar{v}{\nabla_i f(\alpha; D)} -  \scalar{v}{\nabla_i f(\alpha; D')} } \\
    & \le \sup_{D, D'} \norm{v}_1 ( \norm{\nabla_i f(\alpha; D)}_\infty +  \norm{\nabla_i f(\alpha; D')}_\infty ) \\
    & \le \frac 2n \norm{\cM_i}_1 L_i,
  \end{align}
  where $\displaystyle \norm{\cM_i}_1 = \sup_{v \in \cM_i} \norm{v}_1$ and $L_i$ is the $\ell_1$-coordinate wise Lipschitz constant of $f$.
\end{lemma}

Note in \Cref{lemma:sensitivity-scalar-product} that the infinite norm of the gradient is upper bounded by the $\ell_1$ Lipschitz constant of $f$.
For this see for instance Lemma 2.6 in \cite{shalev_online_2011}.

\begin{algorithm}
  \caption{Differentially Private Block-Coordinate Frank-Wolfe}
  \label{algo:dp-bc-fw}
  \hspace*{\algorithmicindent} \textbf{Input} : Let $\alpha \in \cM = \cM_1 \times \cdot \times \cM_p$, privacy parameters $(\epsilon, \delta)$.
  \begin{algorithmic}[1]
    \For{$k = 0\dots K$}
    \State Pick $i \sim_u [p]$.
    \State $\displaystyle \forall v \in S^i, \tau_i^v = \scalar{v}{\nabla_i f(\alpha)} + Lap\left( \frac{L_i \norm{\cM_i}_1 \sqrt{8K \log(1/\delta)}}{n \epsilon} \right)$.
    \State $\displaystyle s_i^k = \argmin_{v \in S^i} \tau_i^v$.
    \State Let $\gamma_k = \frac{2p}{k+2p}$.
    \State Update $\alpha^k_i = \alpha^k_i + \gamma_k \left( s_i^k - \alpha^k_i \right)$.
    \EndFor
  \end{algorithmic}
\end{algorithm}

\begin{theorem}
  \Cref{algo:dp-bc-fw} is $(\epsilon, \delta)$-differentially private.
\end{theorem}

\begin{proof}
  Each iteration of the algorithm uses the report noisy max mechanism.
  The $\ell_1$-sensitivity of the function $h_i(v; D) = \scalar{v}{\nabla_i f(\alpha; D)}$ is $\Delta h_i \le \frac 2n \norm{\cM_i}_1 L_i$.
  Thus adding a Laplace noise with scale $\frac{\Delta h_i}{\epsilon}$ makes the iteration $\left(\epsilon' = \frac{\epsilon}{\sqrt{8K\log(1/\delta)}}, 0\right)$-differentially private.

  The advanced composition theorem then states that the algorithm is $(\epsilon, \delta)$-differentially private.
\end{proof}

\begin{lemma}
  \label{lemma:high-prob-fw}
  Let $(\alpha_1, \dots, \alpha_K)$ be the iterates of \Cref{algo:dp-bc-fw}, $(s_1, \dots, s_K)$ the choosen vectors at each step and $s_k^*$ the optimal vectors in the non-noisy setting.
  Then, for $\zeta \in [0, 1]$,
  \begin{align}
    \prob{\forall k \le K, \scalar{s_k}{\nabla_i f(\alpha^k)} \le \scalar{s^*_k}{\nabla_i f(\alpha^k)} + \frac 12 \delta^i \gamma_k C_f^i} \ge 1 - \zeta,
  \end{align}
  for $\displaystyle \delta^i = \frac{4 L_i \norm{\cM_i}_1 \sqrt{8K\log(1/\delta)}\log(\abs{S}K/\zeta)}{n\epsilon\gamma C_f^i}$, where $\gamma = \frac{2p}{2p+K}$.

\end{lemma}
\begin{proof}
  To prove the result, we find an upper bound on the probability of the complementary event.
  For this calculus, we define the event ``choose $s_k=s$ at step $k$'' as
  \begin{align}
    "s_k = s": \forall s'\in S, \scalar{s}{\nabla_i f(\alpha^k)} + b_{s} \le \scalar{s'}{\nabla_i f(\alpha^k)} + b_{s'}.
  \end{align}

  Then, with $s_k$ the vectors choosen at step $k$, $\displaystyle s_k^* = \argmin_{s\in \cM_i} \scalar{s}{\nabla_i f(\alpha^k)}$ and $\lambda_i = \frac{L_i \norm{\cM_i}_1\sqrt{8K\log(1/\delta)}}{n\epsilon}$,
  \begin{align}
    & \prob{\forall k \le K, \scalar{s_k}{\nabla_{i_k} f(\alpha^k)} \ge \scalar{s_k^*}{\nabla_{i_k} f(\alpha^k)} + \frac 12 \delta^{i_k} \gamma_k C_f^{i_k}} \\
    \overset{\substack{union\\bound}}&{\le} \sum_{k \le K} \prob{\scalar{s_k}{\nabla_{i_k} f(\alpha^k)} \ge \scalar{s_k^*}{\nabla_{i_k} f(\alpha^k)} + \frac 12 \delta^{i_k} \gamma_k C_f^{i_k}} \\
    & \hspace{-2em}
      \text{as $"s_k = s"$ events for $s \in S$ is a partition of the sample space, law of total probability gives:} \nonumber \\
    & \le \sum_{k \le K} \sum_{s\in S} \prob{\scalar{s}{\nabla_{i_k} f(\alpha^k)} \ge \scalar{s^*_k}{\nabla f(\alpha^k)} + \frac 12 \delta^{i_k} \gamma_k C_f^{i_k} \land "s_k = s"} \\
    & = \sum_{k \le K} \sum_{s\in S} \prob{\scalar{s - s^*_k}{\nabla_{i_k} f(\alpha^k)} \ge \frac 12 \delta^{i_k} \gamma_k C_f^{i_k} \land "s_k = s"} \\
    & \hspace{-2em}
      \text{using $\scalar{s - s^*}{\nabla_{i_k} f(\alpha^k)} \le b_{s^*} - b_s$ and $\gamma_k \ge \gamma$:} \nonumber \\
    & \le \sum_{k \le K} \sum_{s\in S} \prob{b_{s^*} - b_s \ge \frac 12 \delta^{i_k} \gamma_k C_f^{i_k} \land "s_k = s"} \\
    & \le \sum_{k \le K} \sum_{s\in S} \prob{b_{s^*} - b_s \ge \frac 12 \delta^{i_k} \gamma C_f^{i_k}} \\
    & \hspace{-2em}
      \text{leveraging $b_s, b_{s^*} \sim Lap(\lambda_{i_k})$ and \Cref{lemma:diff-laplace:2}:} \nonumber \\
    & = \sum_{k \le K} \sum_{s\in S} \left( \frac 12 + \frac{\delta^{i_k} \gamma C_f^{i_k}}{8\lambda_{i_k}} \right) e^{- \frac{\delta^{i_k} \gamma C_f^{i_k}}{\lambda_{i_k}}} \\
    & \le \sum_{k \le K} \sum_{s\in S} \left( \frac 12 + \frac{\delta^{i_k} C_f^{i_k}}{8\lambda_{i_k}} \right) e^{- \frac{\delta^{i_k} \gamma C_f^{i_k}}{\lambda_{i_k}}}.
  \end{align}

  Now set $\zeta' \in [0, 1]$, then for all $i \in [p]$, one can choose $\frac 12\delta^i \gamma C_f^i = 2 \lambda_i \log(1/\zeta')$, which yields
  \begin{align}
    \left( \frac 12 + \frac{\delta^i \gamma C_f^i}{8\lambda_i} \right) e^{- \frac{\delta^i \gamma C_f^i}{\lambda_i}}
    = \frac 12 \zeta'^2 + \frac{\zeta' \log(1/\zeta')}{2} \zeta'
    \le \frac 12 \zeta' + \frac{1}{2} \zeta' = \zeta',
  \end{align}

  giving the result with $\zeta' = \frac{\zeta}{\abs{S} K} \le 1$.
\end{proof}

\begin{theorem}
  \label{thm:dp-cv-bc-fw}
  With $\alpha^k$ the iterates from \Cref{algo:dp-bc-fw} and $\zeta \in [0, 1]$, it holds that, with probability at least $1 - \zeta$,
  \begin{align}
    \expec{i}{f(\alpha^K) - f^*}
    & \le \frac{2p}{2p+K} \left( \sum_{i=1}^p C_f^i + f(\alpha^0) - f^* \right)
      + \frac{\sqrt{32K\log(1/\delta)}\log(\abs{S}K/\zeta)}{n\epsilon} \sum_{i=1}^p L_i \norm{\cM_i}_1,
  \end{align}

  which gives the result in expectation:
  \begin{align}
    \expec{}{f(\alpha^K) - f^*}
    & \le \frac{2p}{2p+K} \left( \sum_{i=1}^p C_f^i + f(\alpha^0) - f^* \right) \\
    & \qquad + \frac{\sqrt{32K\log(1/\delta)}\log(\abs{S}KL_1\norm{\cM}_1)}{n\epsilon} \sum_{i=1}^p L_i \norm{\cM_i}_1
      + \frac 1n.
  \end{align}
\end{theorem}

\begin{proof}
  \Cref{lemma:high-prob-fw}, coupled with \Cref{thm:general_inexact_convergence} gives that with probability at least $1 - \zeta$, with $\gamma = \frac{2p}{2p + K}$,
  \begin{align}
    \expec{i}{f(\alpha^K) - f^*}
    & \le \frac{2p}{2p+K} \left( \sum_{i=1}^p C_f^i \left( 1 + \frac{4L_i\norm{\cM_i}_1\sqrt{8K\log(1/\delta)}\log(\abs{S}K/\zeta)}{n\epsilon\gamma C_f^i} \right) + f(\alpha^0) - f^* \right) \\
    & = \frac{2p}{2p+K} \left( \sum_{i=1}^p C_f^i + f(\alpha^0) - f^* \right) \\
    & \quad  + \frac{2p}{2p+K} \frac{4\sqrt{8K\log(1/\delta)}\log(\abs{S}K/\zeta)}{n\epsilon\gamma} \sum_{i=1}^p L_i \norm{\cM_i}_1 \\
    & = \frac{2p}{2p+K} \left( \sum_{i=1}^p C_f^i + f(\alpha^0) - f^* \right)
      + \frac{4\sqrt{8K\log(1/\delta)}\log(\abs{S}K/\zeta)}{n\epsilon} \sum_{i=1}^p L_i \norm{\cM_i}_1.
  \end{align}

  Now, remark that for all $\alpha \in \cM, f(\alpha) - f^* \le \frac {L_1 \norm{\alpha - \alpha^*}_1}{n} \le \frac{L_1 \norm{\cM}_1}{n}$, which yields
  \begin{align}
    \expec{}{f(\alpha^K) - f^*}
    & \le z \prob{f(\alpha^K) - f^* \le z} + \frac{L_1\norm{\cM}_1}{n} \prob{f(\alpha^K) - f^* \ge z} \\
    & \le z + \frac{L_1\norm{\cM}_1}{n} \zeta,
  \end{align}

  where $z = \frac{2p}{2p+K} \left( \sum_{i=1}^p C_f^i + f(\alpha^0) - f^* \right)
  + \frac{\sqrt{32K\log(1/\delta)}\log(\abs{S}K/\zeta)}{n\epsilon} \sum_{i=1}^p L_i \norm{\cM_i}_1$.

  Thus giving the result by setting $\zeta = \frac{1}{L_1\norm{\cM}_1} \le 1$.
\end{proof}

\begin{remark}
  In \Cref{thm:dp-cv-bc-fw}, the result depends on the sum of $\ell_1$-lipschitz constants of the function that is minimized.
  This sum also appears when analyzing coordinate descent methods, and it seems that it appears in all DP block-coordinate alogrithms.

  Listing some problems and seeing how it compares with global $\ell_1$-lipschitz constant might give some insight on how bad it is, and on which problems it is reasonnible to use block coordinate methods.
\end{remark}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
