\documentclass[12pt,oneside]{memoir}
\nouppercaseheads

% formatting of pages and paragraphs
%\usepackage{fullpage}
%\usepackage[margin=1.1in]{geometry}
%\usepackage[textwidth=157.75mm, textheight=234mm, marginratio={4:6,5:7}, nomarginpar]{geometry}
\usepackage{geometry}
\newgeometry{
    top=1.5in,
    bottom=1.25in,
    outer=1.25in, % to set to 1in in the end
    inner=1.25in, % to set to 1.5in in the end
}

\usepackage{parskip}
\setlength{\parindent}{0pt}
\setlength{\parskip}{0.5em}

% to include the first page
\usepackage{pdfpages}

% language
\usepackage[english]{babel}

% math packages
\usepackage{amsmath}
\usepackage{amssymb}

% colors and boxes
\input{style/general}
\input{style/links}
\input{style/colors}
\input{style/theorems}
\input{style/citations}
\input{style/tables}
\input{style/algorithms}
\input{style/figures}

\input{macros/names}
\input{macros/math-operators}
\input{macros/letters}
\input{macros/reviewing}

%\includeonly{src/intro,src/math-background}



\begin{document}

% include title page
\includepdf[pages=-]{first-page/these_pagegarde.pdf}


\begin{KeepFromToc}
\chapter*{Abstract}
\label{sec:thesis-abstract}

\vspace{-1em}

In the past decades, concerns about the societal impact of machine
learning have been growing. Indeed, if machine learning has proven its
usefulness in science, day-to-day applications, and many other
domains, its success is principally due to the availability of large
datasets. This raises two concerns, the first about the
confidentiality of the training data, and the second, about possible
discrimination in a model's predictions. Trustworthy machine learning
aims at providing technical answers to these concerns.

Unfortunately, guaranteeing the privacy of the training data and the
fairness of the predictions often decreases the utility of the learned
model. This problem has drawn significant interest in the past years,
but most of existing methods (usually based on stochastic gradient
descent) tend to fail in some common scenarios, like training of
high-dimensional models.  In this thesis, we study how structural
properties of machine learning problems can be exploited to improve
the trade-off between privacy and utility, and how this can impact the
fairness of the predictions.

The first two contributions of this thesis are two new differentially
private optimization algorithms, that are both based on coordinate
descent. They aim at exploiting different structural properties of the
problem at hand. The first algorithm is based on stochastic coordinate
descent, and can exploit imbalance in the scale of the gradient's
coordinates by using large step sizes. This allows our algorithm to
obtain useful models in difficult problems, where stochastic gradient
descent quickly stalls. The second algorithm is based on greedy
coordinate descent. Its greedy updates allow to focus on the most
important coordinates of the problem, which can sometimes drastically
improve utility (\eg when the solution of the problem is sparse).

The third contribution of this thesis studies the interplay of
differential privacy and fairness in machine learning. These two
notions have rarely been studied simultaneously, and there are growing
concerns that differential privacy may exacerbate unfairness. We show
that group fairness measures have interesting regularity properties,
provided that the predictions of the model are Lipschitz-continuous in
its parameters. This result allows to derive a bound on the difference
in fairness levels between a private model and its non-private
counterpart.


\chapter*{Résumé}
\label{cha:resume}

Au cours des dernières décennies, les préoccupations quant à l'impact sociétal de l'apprentissage automatique se sont multipliées. En effet, si l'apprentissage automatique a prouvé son utilité dans la science, dans la vie quotidienne, ainsi que dans de nombreux autres domaines, son succès est principalement dû à la disponibilité de grands ensembles de données. Cela soulève deux préoccupations : la première concerne la confidentialité des données d'entraînement et la seconde, la possibilité de discrimination dans les prédictions d'un modèle. Le domaine de l'apprentissage automatique fiable vise à apporter des réponses techniques à ces préoccupations.

Malheureusement, garantir la confidentialité des données d'entraînement, ainsi que l'équité des prédictions, diminue souvent l'utilité du modèle appris. Ce problème a suscité un grand intérêt au cours des dernières années. Cependant, la plupart des méthodes existantes (généralement basées sur la descente de gradient stochastique) ont tendance à échouer dans des scénarios courants, tels que l'entraînement de modèles en grande dimension. Dans cette thèse, nous étudions comment les propriétés structurelles des problèmes d'apprentissage automatique peuvent être exploitées pour améliorer le compromis entre la confidentialité et l'utilité, et comment cela peut affecter l'équité des prédictions.

Les deux premières contributions de cette thèse sont deux nouveaux algorithmes d'optimisation respectant la confidentialité différentielle, tous deux basés sur la descente par coordonnées, visant à exploiter les propriétés structurelles du problème. Le premier algorithme est basé sur la descente par coordonnées stochastique et est en mesure d'exploiter le déséquilibre dans l'échelle des coordonnées du gradient en utilisant des grands pas d'apprentissage. Cela lui permet de trouver des modèles pertinents dans des scénarios difficiles, où la descente de gradient stochastique échoue. Le deuxième algorithme est basé sur la descente par coordonnées gloutonne. Les mises à jour gloutonnes permettent de se concentrer sur les coordonnées les plus importantes du problème, ce qui peut parfois améliorer considérablement l'utilité (par exemple, lorsque la solution du problème est parcimonieuse).

La troisième contribution de cette thèse étudie les interactions entre confidentialité différentielle et équité en apprentissage automatique. Ces deux notions ont rarement été étudiées simultanément, et il existe des inquiétudes croissantes selon lesquelles la confidentialité différentielle pourrait nuire à l'équité des prédictions. Nous montrons que quand les prédictions du modèle sont lipschitziennes (par rapport à ses paramètres), les mesures d'équité de groupe présentent des propriétés de régularité intéressantes, que nous caractérisons. Ce résultat permet d'obtenir une borne sur la différence de niveaux d'équité entre un modèle privé et le modèle non-privé correspondant.


\end{KeepFromToc}
\newpage

% include table of contents
\settocdepth{subsection}
\begin{KeepFromToc}
  \hypersetup{linkcolor=black}
  \tableofcontents
\end{KeepFromToc}
\newpage

\include{src/contrib}

% actual thesis
%\part{Introduction}
\include{src/intro}

\include{src/publications}

%\part{Mathematical Background}
\include{src/math-background}

%\part{Contribution: Private Coordinate Descent Methods}
\include{src/random-cd}
\include{src/greedy-methods}


%\part{Contribution: Impact of Privacy on Fairness}
\include{src/fairness}


%\part{Contribution: Federated}
%\include{src/federated}

%\part{Conclusion}
\include{src/conclusion}

% appendices

\printbibliography


\appendix

% \part*{Appendix: Background Proofs}

% \addcontentsline{toc}{part}{\protect\numberline{}\hspace{-1cm}Appendix: Background Proofs}%
% \include{src/bg/supplementary-proofs}

% \part*{Appendix: Contributed Proofs}
% \addcontentsline{toc}{part}{\protect\numberline{}\hspace{-1cm}Appendix: Contributed Proofs}%

\include{src/dp-cd/supplement}
\include{src/greedy-cd/supplement}
\include{src/fairness-lipschitz/supplement}

% \part*{Appendix: Additional Results}
% \addcontentsline{toc}{part}{\protect\numberline{}\hspace{-1cm}Appendix: Additional Results}%


\include{src/experimental-details}

%\include{src/lipschitz}
%\include{src/federated-healthcare}
%\include{src/frank-wolfe}

% references


\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
