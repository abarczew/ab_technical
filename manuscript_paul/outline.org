#+TITLE: PhD Manuscript Outline
* Raw summary of the contributions
- Private block coordinate algorithms
  - Private coordinate descent
  - Greedy private coordinate descent
  - Inexact and private block-coordinate Frank-Wolfe
- Coordinate-wise prox-skipping algorithm
- Applications to federated and decentralized learning
  - Local training with efficient algorithms
  - A vertical federated learning CD algorithm?
  - Personalized federated learning?
- (Old work at CHU) Federated logistic regression with confidence intervals
- Interplay between fairness and privacy
- Federated learning datasets
- Scaled gaussian mechanism
- Federated imputation?

* Fil directeur de la thèse
Private optimization in a collaborative settings, where we study the
properties of the resulting model.

Potential title: Privacy-preserving and federated optimization for
trustworthy machine learning

Or maybe something better: Leveraging Problem Structure in Private
Optimization for Trustable Machine Learning

By structure I mean: (i) exploiting component-wise regularity, (ii)
exploiting unbalancedness by greedy methods, and (iii) apply these
algorithms to federated learning through local training or vertical
federated learning. We also (iv) study the relation between model and
data structures on the resulting fairness of the obtained models.

I think it is also interesting to present things as studying
optimization algorithms under noise addition. Some existing analyses
do not give optimal results in such settings: in some way, it may help
build a deeper understanding of these analyses.
* Table of Content (tentative)
1. Introduction
   a. Leakage in Machine Learning Models
   b. Differential Privacy as a solution
   c. Private Optimization

2. Exploiting Coordinate-Wise Regularity through Randomized CD

3. Exploiting High Dimensional Structure through Greedy Algorithms
   a. Greedy CD
   b. Block-Coordinate Frank-Wolfe?

4. Federated Machine Learning
   a. Local Training with Efficient Local Algorithms + Impact on Privacy
   b. Vertical Federated Learning?

5. A Step Back: Trustworthiness of the Models
   a. Fairness
   b. Properties of Models Fairness and Accuracy Levels
   c. Assessing Impact of Privacy on Fairness

6. Federated Logistic Regression with Confidence Intervals (based on work at CHU)
   a. Estimating the Parameters
   b. Giving Confidence Intervals

7. Conclusion and Perspectives
   1. Summary
   2. Perspectives in Private Machine Learning
      - Building algorithms that adapt to the problem's structure
      - Building more efficient "greedy-like" algorithms
      - Building hyperparameter-free algorithms (based on polyak step size schemes?)
   3. Perspectives in Federated Learning
      - Improving decentralized ML by leveraging some kind of
        similarity (note that proxskip does not use any information on
        such similarity)
      - Greedy client selection in vertical FL (reducing communucations)
      - Towards more trustworthy FL: privacy with relevant confidence intervals
      - Towards more practical FL: imputations schemes (cf. appendix?)
8. Appendix
   1. A Federated Imputation Scheme?
   2. A Scaled Gaussian Mechanism for Multiplicative Queries
   3. Cheat Sheet: Usual Lipschitz Constants

* Resources to thank in the manuscript
- Fabian Pedregosa : [[https://fa.bianp.net/blog/2017/optimization-inequalities-cheatsheet/][optimization inequalities cheat sheet]]
- László Kozma : [[https://www.lkozma.net/inequalities_cheat_sheet/ineq.pdf][useful inequalities cheat sheet]]
- Guillaume Garrigos an Robert M. Gower : [[https://gowerrobert.github.io/pdf/M2_statistique_optimisation/grad_conv.pdf][handbook of convergence of gradient methods]]
- Yurii Nesterov : [[cite:&nesterov2004Introductory]]
